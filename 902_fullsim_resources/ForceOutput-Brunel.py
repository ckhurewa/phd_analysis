# This file must be given as last argument to gaudirun.py, after Brunel<conf>.py

# Syntax is:
#   gaudirun.py Brunel<conf>.py MC09-Files.py
#
from Gaudi.Configuration import *
# from Configurables import Brunel, LHCbApp

#-- File catalogs. First one is read-write
# FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml",
                           # "xmlcatalog_file:$BRUNELROOT/job/NewCatalog.xml" ]

# EventSelector().Input = ["DATAFILE='PFN:./Moore.dst' TYP='POOL_ROOTTREE' OPT='READ'"]

# Default output files names are set up using value Brunel().DatasetName property
# datasetName = 'Brunel'
# Brunel().DatasetName = datasetName

#-- Dst or rDst file

FileCatalog().Catalogs = [ "xmlcatalog_file:Catalog.xml" ]

OutputStream("DstWriter").Output = "DATAFILE='PFN:Brunel.dst' TYP='POOL_ROOTTREE' OPT='REC'"

