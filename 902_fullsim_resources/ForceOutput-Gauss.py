#--Set name of output files for given job (uncomment the lines)
#  Note that if you do not set it Gauss will make a name based on event type,
#  number of events and the date

from Gaudi.Configuration import *

idFile = 'Gauss'
HistogramPersistencySvc().OutputFile = idFile+'-histos.root'

OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile