#!/usr/bin/env python

from PythonCK import ioutils

def env_injector(**kwargs):
  """
  This will write a temp file, where inside there'll be call on os.environ,
  and kwargs of this function will be injected into that env. The function then 
  return a path to that temp file, to be used in gaudirun.
  """
  msg = "import os\n"
  for key,val in kwargs.iteritems():
    msg += 'os.environ[%r] = %r\n'%(key,val)
  fpath = ioutils.dump_to_file( msg, suffix='.py' )
  return fpath
