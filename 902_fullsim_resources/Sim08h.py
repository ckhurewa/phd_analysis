#!/usr/bin/env python

"""

Model Request 10886
Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged

If you want it more customized, then copy the module and edit it ad-hoc.

Usage:
  ## These are already prepared Ganga-Application
  >> import Sim08h
  >> j = Job()
  >> j.application = Sim08h.Gauss( 42311012, 100 )  # Only this one need args
  >> j.application = Sim08h.Gauss( '12345678.py', 100 )  # Local file
  >> j.application = Sim08h.Boole()
  >> j.application = Sim08h.DaVinci( r'.*Z02TauTau_.*' )

"""

import os
from SimUtils import env_injector
from Ganga import GPI

## Not recommend using `from Sim08h import *` since the app name collide.
## Use rather `import Sim08h` `app = Sim08h.Boole()`
__all__ = []

extraopts_dddb = """
from Configurables import LHCbApp
LHCbApp().DDDBtag    = 'dddb-20130929-1'
LHCbApp().CondDBtag  = 'sim-20130522-1-vc-md100'
"""

#-------#
# GAUSS #
#-------#

# ## For 8TeV condition: 
# # app = Gauss(version='v48r3')  # Allow P8 Pileup???
def Gauss(src, numevt):
  """
  Args:
    src: If int, 8-digit decay file ID 
         If str, path to local gen python file.
    numevt (int): Number of event to gen per (sub)job.
  """
  files = [ 
    '$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py',
    None,
    '$LBPYTHIA8ROOT/options/Pythia8.py',
    '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py',
    '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
    '$XMLSUMMARYKERNELROOT/options/add-XMLSummary.py',
  ]
  if isinstance( src, int ):
    files[1] = '$DECFILESROOT/options/%i.py'%decid
  else:
    files[1] = src

  app = GPI.Gauss(version='v45r10p1', platform='x86_64-slc5-gcc43-opt')
  app.optsfile = files

  ## Control numbers
  app.extraopts = extraopts_dddb + """
from Gauss.Configuration import *
GenInit('GaussGen').RunNumber =  1
GenInit('GaussGen').FirstEventNumber = 1
LHCbApp().EvtMax = %i
  """%numevt
  return app


#-------#
# BOOLE # mem < 1500
#-------#

def Boole():
  app = GPI.Boole(version='v26r3', platform='x86_64-slc5-gcc43-opt' )
  app.optsfile = [
    '$APPCONFIGOPTS/Boole/Default.py',
    '$APPCONFIGOPTS/Boole/DataType-2012.py',
    '$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py',
    '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
  ]
  app.extraopts = extraopts_dddb
  return app

#-------#
# MOORE #
#-------#

def Moore():
  app = GPI.Moore(version='v14r8p1', platform='x86_64-slc5-gcc43-opt')
  app.optsfile = [
    '$APPCONFIGOPTS/Moore/MooreSimProductionWithL0Emulation.py',
    '$APPCONFIGOPTS/Conditions/TCK-0x409f0045.py',
    '$APPCONFIGOPTS/Moore/DataType-2012.py',
    '$APPCONFIGOPTS/L0/L0TCK-0x0045.py',
  ] 
  app.extraopts = extraopts_dddb + """
from Configurables import Moore
Moore().outputFile = 'Moore.dst'  # Force output
  """
  return app

#--------#
# BRUNEL #
#--------#

def Brunel():
  app = GPI.Brunel(version='v43r2p11', platform='x86_64-slc5-gcc46-opt')
  app.optsfile = [
    '$APPCONFIGOPTS/Brunel/DataType-2012.py',
    '$APPCONFIGOPTS/Brunel/MC-WithTruth.py',
    '$APPCONFIGOPTS/Persistency/DST-multipleTCK-2012.py',
    '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
  ]
  app.extraopts = extraopts_dddb + """
from Gaudi.Configuration import OutputStream
OutputStream("DstWriter").Output = "DATAFILE='PFN:Brunel.dst' TYP='POOL_ROOTTREE' OPT='REC'"
  """
  return app

#---------#
# DAVINCI #
#---------#

def DaVinci(stripping_regex=''):
  """
  Usage:
  >> app = Sim08h.DaVinci( r'.*Z02TauTau_.*' )

  Args:
    stripping_regex (rawstr): If provided, the list of stripping line names will 
                              be regex against this string. Only those pass (via
                              re.search ) will be used inside DaVinci.
  """
  app   = GPI.DaVinci(version='v32r2p1', platform='x86_64-slc5-gcc46-opt')  # For S20
  app.optsfile = [
    env_injector( TMP_STRIPPING_REGEX=stripping_regex ),
    os.path.join( os.path.split(__file__)[0], 'DV_stripping_regex.py'),
    '$APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py',
    '$APPCONFIGOPTS/DaVinci/DataType-2012.py',
    '$APPCONFIGOPTS/DaVinci/InputType-DST.py',
    '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
  ]
  app.extraopts = extraopts_dddb
  return app


