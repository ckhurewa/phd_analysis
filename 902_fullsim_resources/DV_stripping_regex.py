
"""

Use environmental variable `TMP_STRIPPING_REGEX` to apply regex
patching onto the `StrippingStream.appendLines` method.

"""

import os
import re
import functools

def whitelisting(func, regex=None):
  def wrap( self, lines ):
    if not regex:
      queue = lines
    else:
      print 'Whitelist requesting lines'
      queue = []
      for line in lines:
        name = line.name()
        if re.search( regex, name ):
          print 'Allow: ', name
          queue.append( line )
    return func( self, queue )
  return wrap

## Need injection from earlier script.
stripping_regex = os.environ['TMP_STRIPPING_REGEX']
if stripping_regex:
  from StrippingConf.Configuration import StrippingStream
  StrippingStream.appendLines = whitelisting( StrippingStream.appendLines, stripping_regex )
