# Example data files for Boole.
# This file must be given as last argument to gaudirun.py, after Boole<conf>.py

# Syntax is:
#   gaudirun.py Boole<conf>.py MC09-Files.py
#

from Gaudi.Configuration import *

# -- File catalogs
# FileCatalog().Catalogs = [ "xmlcatalog_file:NewCatalog.xml" ]
EventSelector().Input = ["DATAFILE='PFN:./Gauss.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
