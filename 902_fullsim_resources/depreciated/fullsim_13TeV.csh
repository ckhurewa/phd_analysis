#!/bin/csh -f
#SBATCH -c 1
#SBATCH -J fullgen_bbtautau
#SBATCH -N 1
#SBATCH -A batch
#SBATCH -p batch
#SBATCH --mem=10240
#SBATCH -t 1-0:00:00


# Usage:
#     sbatch fullsim_13TeV.csh /path/to/options/43900080.py -n 100 -r 1
#
# Args:
#   (string): Required, option file to produce
#   -n (int): Number of event
#   -r (int): Starting run number
#   --fix1  : Apply PileUpTool = 'FixedNInteractions'

# Default!
set nevents=3
set runnumber=0
set FixedNInteractions=false

set GAUSS_DEC_OPTION=$argv[1]

# Carry my env here
source /home/khurewat/.login

echo $IOCHAINROOT
echo $DECFILESDUMMY

# set IOCHAINROOT $HOME/PhD_Analysis/902_fullsim_resources
# set DECFILESDUMMY $HOME/PhD_Analysis/009_bbtautau_generation/H2AA

# set GAUSS_DEC_OPTION="/home/khurewat/PhD_Analysis/009_bbtautau_generation/H2AA/options/43900080.py"

#==============================================================================

echo START JOB: `date`

#http://www.unix.com/shell-programming-and-scripting/144827-help-convert-ksh-script-getopts-tcsh.html
while ( $#argv > 1)
  switch($argv[2])
    case -n: 
      shift
      set nevents=$argv[2]
      breaksw
    case -r: 
      shift
      set runnumber=$argv[2]
      breaksw
    case --fix1:
      set FixedNInteractions=true
      breaksw
    default:
      shift
      echo Unknown parameter $argv[2]
    endif
  endsw
  shift
end

echo "----------------------------------------"
echo "option    :" $GAUSS_DEC_OPTION              
echo "nevents   :" $nevents                   
echo "runnumber :" $runnumber
echo "fix1      :" $FixedNInteractions                
echo "----------------------------------------"


# Push into new *.py input
set ARGS_GAUSS_JOB="temp_gauss_job.py"
rm $ARGS_GAUSS_JOB
touch $ARGS_GAUSS_JOB
echo "from Gauss.Configuration import *"            >> $ARGS_GAUSS_JOB
echo "LHCbApp().EvtMax = " $nevents                 >> $ARGS_GAUSS_JOB
echo "GenInit('GaussGen').RunNumber = " $runnumber  >> $ARGS_GAUSS_JOB
echo "GenInit('GaussGen').FirstEventNumber = 1"     >> $ARGS_GAUSS_JOB

if ($FixedNInteractions == true) then
  echo "Generation('Generation').PileUpTool = 'FixedNInteractions'" >> $ARGS_GAUSS_JOB
endif

#==============================================================================

# Login!
source /cvmfs/lhcb.cern.ch/lib/group_login.csh
LbLogin -c x86_64-slc5-gcc43-opt # Enforce the older model

#-------------# 
#   GAUSS     #
#-------------#


# SetupProject Gauss v46r7p2
SetupProject Gauss v45r6

# Choose the magnet config
# set GAUSS_MAG=$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py 
# set GAUSS_MAG=$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu3.8.py

set GAUSS_MAG=$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-nu1.5.py
set CONDITION=$APPCONFIGOPTS/Conditions/Sim08-2012-md100.py

echo "---------------------------------------------------------"
echo GAUSS_MAG: $GAUSS_MAG
echo CONDITION: $CONDITION
echo "---------------------------------------------------------"


# gaudirun.py \
#   $GAUSS_DEC_OPTION \
#   ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
#   ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py \
#   $GAUSS_MAG \
#   $CONDITION \
#   $ARGS_GAUSS_JOB \
#   ${LBPYTHIA8ROOT}/options/Pythia8.py \
#   $IOCHAINROOT/IOchain-Gauss.py \


  # $GAUSSOPTS/GenStandAlone.py \
  # $APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py \
  # ${APPCONFIGOPTS}/Gauss/xsim.py \


# Order requirement
# - Pythia8: To enforce generator
# - ARGS_GAUSS_JOB: In case of having FixedNInteractions

# #-------------------------
# echo END JOB: `date`
# #-------------------------

# exit 0

#-------------# 
#   BOOLE     #
#-------------#

SetupProject Boole v26r3
# SetupProject Boole v28r2
gaudirun.py \
  $APPCONFIGOPTS/Boole/Default.py \
  $APPCONFIGOPTS/Boole/DataType-2012.py \
  $APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py \
  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
  $CONDITION \
  $APPCONFIGOPTS/Boole/EnableSpillover.py \
  $IOCHAINROOT/IOchain-Boole.py \

  # ${APPCONFIGOPTS}/Boole/xdigi.py


#-------------# 
#   MOORE     #
#-------------#

SetupProject Moore v14r8p1
# SetupProject Moore v22r2
gaudirun.py \
  $APPCONFIGOPTS/Moore/MooreSimProductionWithL0Emulation.py \
  $APPCONFIGOPTS/Conditions/TCK-0x409f0045.py \
  $APPCONFIGOPTS/Moore/DataType-2012.py \
  $APPCONFIGOPTS/L0/L0TCK-0x0045.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-Moore.py


#-------------# 
#   BRUNEL    #
#-------------#

SetupProject Brunel v43r2p7
# SetupProject Brunel v46r1
gaudirun.py \
  $APPCONFIGOPTS/Brunel/DataType-2012.py \
  $APPCONFIGOPTS/Brunel/MC-WithTruth.py \
  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-Brunel.py


#------------------------# 
#   DAVINCI/STRIPPING    #
#------------------------#

SetupProject DaVinci v32r2p1
# SetupProject DaVinci v35r1
gaudirun.py \
  $APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py \
  $APPCONFIGOPTS/DaVinci/DataType-2012.py \
  $APPCONFIGOPTS/DaVinci/InputType-DST.py \
  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-DaVinci.py


# #------------# 
# #   BENDER   #
# #------------#

# # IF REQUESTS

# # TMPFILE=$(mktemp)
# # echo '
# # SetupProject Bender v22r11
# # /home/khurewat/PhD_Analysis/005_ParticleGun/p8jp_v2/p8jp_v2_bender.py
# # ' >> $TMPFILE
# # csh $TMPFILE


#-------------------------
echo END JOB: `date`
#-------------------------

