
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py")
importOptions("$APPCONFIGOPTS/DaVinci/DataType-2012.py")
importOptions("$APPCONFIGOPTS/DaVinci/InputType-DST.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")
