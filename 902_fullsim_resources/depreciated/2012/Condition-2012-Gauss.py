##############################################################################
# File for running Gauss with Sim08a configuration and beam conditions as in
# production for 2012 data (4.0 TeV beams, nu=2.5, no spill-over)
#
##############################################################################


#--Pick beam conditions as set in AppConfig
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py")
importOptions("$LBPYTHIAROOT/options/Pythia.py")      # empty placeholder
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")    # Turn off hadronLevel
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

