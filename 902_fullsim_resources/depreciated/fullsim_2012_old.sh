#!/bin/sh
# Prefer sh over csh for better variety
#
# Usage:
#     ./fullsim_2012.sh path/to/options/30000000.py
#
# When operating on Ganga, be sure to take care of these envvar!
#
# export IOCHAINROOT=$HOME/PhD_Analysis/902_fullsim_resources
#

echo START JOB: `date`

# env

# HARD-BORROW
export APPCONFIGOPTS=$VO_LHCB_SW_DIR/lib/lhcb/DBASE/AppConfig/v3r194/options


#==============================================================================

#----------------------------------------------
# ArgParse
#----------------------------------------------
#
# --nevents: Number of events   (default=100)
# --magnet : Magnet "up"/"down" (deafult=down)
# --seed   : Seed               (default=0)
#
#----------------------------------------------

# Default!
nevents=10
magnet=up
runnumber=0

while getopts 'n:m:r:' option
do
    case $option in
        n  ) nevents=$OPTARG;;
        m  ) magnet=$OPTARG;;
        r  ) runnumber=$OPTARG;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done

export GAUSS_DEC_OPTION=${@:$OPTIND:1}

[ -z "$GAUSS_DEC_OPTION" ] && (echo "Missing obligatory option file!!"; exit;)
[ -z "$IOCHAINROOT"      ] && (echo "Missing var \$IOCHAINROOT !!"; exit;)
[ -z "$APPCONFIGOPTS"    ] && (echo "Missing var \$APPCONFIGOPTS !!"; exit;)


echo "----------------------------------------"
echo "option    :" $GAUSS_DEC_OPTION              
echo "nevents   :" $nevents                       
echo "magnet    :" $magnet                        
echo "runnumber :" $runnumber                     
echo "----------------------------------------"


# Push into new *.py input
export ARGS_GAUSS_JOB="temp_gauss_job.py"
rm $ARGS_GAUSS_JOB
touch $ARGS_GAUSS_JOB
echo "from Gauss.Configuration import *"            >> $ARGS_GAUSS_JOB
echo "LHCbApp().EvtMax = " $nevents                 >> $ARGS_GAUSS_JOB
echo "GenInit('GaussGen').RunNumber = " $runnumber  >> $ARGS_GAUSS_JOB
echo "GenInit('GaussGen').FirstEventNumber = 1"     >> $ARGS_GAUSS_JOB

export LOGFILE="log.txt"
rm $LOGFILE

#==============================================================================

# Login!
# source /sw/lib/group_shell.sh

# Try both!
LbLoginc x86_64-slc5-gcc43-opt
# /sw/lib/LbLogin.sh -c x86_64-slc5-gcc43-opt
# $VO_LHCB_SW_DIR/lib/LbLogin.sh -c x86_64-slc5-gcc43-opt
# env

# $VO_LHCB_SW_DIR/lib/LbLogin.sh -c x86_64-slc5-gcc43-opt

#-------------# 
#   GAUSS     #
#-------------#

# Choose the magnet config
case $magnet in
  up )
    export GAUSS_MAG=$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py 
    export CONDITION=$APPCONFIGOPTS/Conditions/Sim08-2012-mu100.py
    ;;
  down )
    export GAUSS_MAG=$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py 
    export CONDITION=$APPCONFIGOPTS/Conditions/Sim08-2012-md100.py
    ;;
esac

echo "---------------------------------------------------------"
echo GAUSS_MAG: $GAUSS_MAG
echo CONDITION: $CONDITION
echo "---------------------------------------------------------"




# TMPFILE=$(mktemp)
# echo '
# $VO_LHCB_SW_DIR/lib/group_login.sh
# $VO_LHCB_SW_DIR/lib/LbLogin.sh -c x86_64-slc5-gcc43-opt
# env
# export PATH=${PATH}:$VO_LHCB_SW_DIR/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v7r10p4/InstallArea/scripts/
# $VO_LHCB_SW_DIR/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v7r10p4/InstallArea/scripts/SetupProject.sh Gauss v45r6
# # $LBSCRIPTS_HOME/InstallArea/scripts/SetupProject.sh Gauss v45r6
# gaudirun.py \
#   $ARGS_GAUSS_JOB \
#   $GAUSS_DEC_OPTION \
#   ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
#   ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py \
#   $GAUSS_MAG \
#   $CONDITION \
#   # ${LBPYTHIA8ROOT}/options/Pythia8.py \
#   $GAUSSOPTS/GenStandAlone.py \
#   $IOCHAINROOT/IOchain-Gauss.py \
#   ${APPCONFIGOPTS}/Gauss/xsim.py
# ' >> $TMPFILE
# sh $TMPFILE


TMPFILE=$(mktemp)
echo '
SetupProject Gauss v45r6
gaudirun.py \
  $ARGS_GAUSS_JOB \
  $GAUSS_DEC_OPTION \
  ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
  ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py \
  $GAUSS_MAG \
  $CONDITION \
  # ${LBPYTHIA8ROOT}/options/Pythia8.py \
  # $GAUSSOPTS/GenStandAlone.py \
  $IOCHAINROOT/IOchain-Gauss.py \
  ${APPCONFIGOPTS}/Gauss/xsim.py
' >> $TMPFILE
csh $TMPFILE


#-------------# 
#   BOOLE     #
#-------------#

TMPFILE=$(mktemp)
echo '
SetupProject Boole v26r3
gaudirun.py \
  $APPCONFIGOPTS/Boole/Default.py \
  $APPCONFIGOPTS/Boole/DataType-2012.py \
  $APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py \
  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-Boole.py \
  ${APPCONFIGOPTS}/Boole/xdigi.py
' >> $TMPFILE
csh $TMPFILE >> $LOGFILE

#-------------# 
#   MOORE     #
#-------------#

TMPFILE=$(mktemp)
echo '
SetupProject Moore v14r8p1
gaudirun.py \
  $APPCONFIGOPTS/Moore/MooreSimProductionWithL0Emulation.py \
  $APPCONFIGOPTS/Conditions/TCK-0x409f0045.py \
  $APPCONFIGOPTS/Moore/DataType-2012.py \
  $APPCONFIGOPTS/L0/L0TCK-0x0045.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-Moore.py
' >> $TMPFILE
csh $TMPFILE >> $LOGFILE


#-------------# 
#   BRUNEL    #
#-------------#

TMPFILE=$(mktemp)
echo '
SetupProject Brunel v43r2p7
gaudirun.py \
  $APPCONFIGOPTS/Brunel/DataType-2012.py \
  $APPCONFIGOPTS/Brunel/MC-WithTruth.py \
  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-Brunel.py
' >> $TMPFILE
csh $TMPFILE >> $LOGFILE

#------------------------# 
#   DAVINCI/STRIPPING    #
#------------------------#

TMPFILE=$(mktemp)
echo '
SetupProject DaVinci v32r2p1
gaudirun.py \
  $APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py \
  $APPCONFIGOPTS/DaVinci/DataType-2012.py \
  $APPCONFIGOPTS/DaVinci/InputType-DST.py \
  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
  $CONDITION \
  $IOCHAINROOT/IOchain-DaVinci.py
' >> $TMPFILE
csh $TMPFILE >> $LOGFILE

#------------# 
#   BENDER   #
#------------#

# IF REQUESTS

# TMPFILE=$(mktemp)
# echo '
# SetupProject Bender v22r11
# /home/khurewat/PhD_Analysis/005_ParticleGun/p8jp_v2/p8jp_v2_bender.py
# ' >> $TMPFILE
# csh $TMPFILE


#-------------------------
echo END JOB: `date`
#-------------------------

