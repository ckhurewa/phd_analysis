from Gaudi.Configuration import *
from Configurables import Moore

# Capture both Boole.digi, Boole-Extended.digi
EventSelector().Input = [ "DATAFILE='PFN:./Boole.digi' TYP='POOL_ROOTTREE' OPT='READ'" ]
                        # ,"DATAFILE='PFN:./Boole-Extended.digi' TYP='POOL_ROOTTREE' OPT='READ'"]

Moore().outputFile = 'Moore.dst'
