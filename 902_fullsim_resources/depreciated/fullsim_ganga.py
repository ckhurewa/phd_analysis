import os
from glob import glob

"""
NSUBJOBS: Number of subjobs to enrich 
NEVENTS : Number of events per subjobs
OPTION  : Main Gauss job file, can be the offical one (30000000.py) or non-official one.
EXE     : Full-simulation-wrapped script.
"""


j = Job(name='p8jp-Zgun-fix',
        # comment="Test fullgen on Dirac",
        comment='*',
        application=Executable(),
        # application=GaudiPython(),
        # backend=Interactive(),
        backend=PBS(), 
        # backend=Dirac(),
        )

NSUBJOBS = 100
NEVENTS  = 1000

# OPTION   = os.environ['DIR09'] + "/H2AA/options/43900080.py"
OPTION   = "/home/khurewat/PhD_Analysis/005_ParticleGun/p8jp_v2/p8jp_v2_gauss.py"
EXE      = os.environ['IOCHAINROOT'] + '/fullsim_2012.sh'

#==============================================================================

#----------------
# SPLIT & MERGE
#----------------

# Provide all the flags conformed with the EXE above
# Note1: the *.sh script is a little buggy. All flags should preceed positional arg
# Note2: Ship OPTION locally next to the EXE file, for the sake of portability

# MAGNET   = "up"

arg0 = ['-n', NEVENTS, os.path.split(OPTION)[-1]]
args = [['-r', str(i), '-m', m ] + arg0 for i in xrange(int(NSUBJOBS)) for m in ('up', 'down')]

if NSUBJOBS == 1:
  j.application.args = args[0]
else:
  j.splitter = ArgSplitter(args=args)

j.postprocessors = RootMerger(files=['tuple.root'], ignorefailed=True, overwrite=True)

#-----------------
# INPUT / OUTPUT
#-----------------

j.application.exe = EXE
# j.application.exe = os.path.split(EXE)[-1]
j.inputsandbox    = [ EXE, OPTION ] + glob(os.environ['IOCHAINROOT']+'/IOchain-*')
# j.outputfiles     = [ '*' ]
# j.outputfiles  = [ '*AllStreams.dst', 'GeneratorLog.xml', '*.py' ] # , '*log'
j.outputfiles  = [ 'tuple.root', 'temp_gauss_job.py', 'summary.xml' ]


j.application.env   = dict(os.environ) # Only for PBS?


if isinstance(j.backend, PBS):
  j.backend.extraopts = "-l walltime=8:00:00"

if isinstance(j.backend, Dirac):
  d = {'IOCHAINROOT': '.'} # Ship IO-chain locally
  j.application.env = d  

#======================
queues.add(j.submit)
#======================
