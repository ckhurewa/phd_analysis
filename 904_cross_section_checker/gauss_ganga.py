"""
Ganga job to run the cross-section checking.
"""

import os
import re

DECID = 42321000


#===============================================================================

## Define application, version and master package
## http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/decfiles/releases/
app = Gauss()
app.platform  = 'x86_64-slc6-gcc48-opt'
# app.version   = "v48r0p1"  # Sim08e
app.version   = "v48r3"


## Define job-options files
## Using GangaCK patched method to support envvar
app.optsfile = [
  '$GAUSSOPTS/Gauss-2012.py',
  '$GAUSSOPTS/GenStandAlone.py',
  #
  '$DECFILESROOT/options/%i.py'%DECID,
  # '~/analysis/013_Tau_Identification/generation/we20jet/42321011.py',
  #
  '$LBPYTHIA8ROOT/options/Pythia8.py',  # Enforce Pythia8 in most case
  # '$LBPOWHEGROOT/options/pwhg_tt.py', # FOR 41900010 only!
  '$XMLSUMMARYKERNELROOT/options/add-XMLSummary.py',
]

## Extraopts to Gauss jobs
app.extraopts = \
"""
from Gauss.Configuration import *
GenInit('GaussGen').RunNumber =  1
GenInit('GaussGen').FirstEventNumber = 0
Gauss().OutputType  = 'NONE'   # Disable GaussTape output
Gauss().Histograms  = 'NONE'   # Disable histo
LHCbApp().EvtMax    = 10000     # Need anyway otherwise it'll crash (?)

## P8PileUp
from Configurables import Generation, Pythia8Production
Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
Generation().PileUpTool = "FixedLuminosityForRareProcess"
Generation().Special.addTool( Pythia8Production, name = "Pythia8PileUp" )
Generation().Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
Generation().Special.ReinitializePileUpGenerator  = False
"""

## Create and submit job
j = Job() 
j.name        = "Gauss_CSC_Checker"
j.comment     = str(DECID) + ': ' + app.nickname(DECID) + '. P8PileUp.'
# j.comment     = '42321011: we20jet'
j.application = app
j.backend     = PBS(extraopts='--mem=3200 -t 1-0:0:0')
j.outputfiles = [ '*.root', '*.xml' ]

queues.add( j.submit )


