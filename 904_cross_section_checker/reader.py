#!/usr/bin/env python

"""

Helper script to read gen-eff from `GeneratorLog.xml` for splitted jobs.

Usage:
    ./reader.py 2369 1234 1235

where each number is JID in gangadir

"""

import re
import sys
from glob import glob

# JID = 3539

pattern = r'<efficiency name = "%s">\n\s+<after> (\d+) </after>\n\s+<before> (\d+) </before>'

def eff_fraction( dat, name ):
  res = re.findall( pattern%name, dat )
  if res:
    return int(res[0][0]), int(res[0][1])
  else:
    return 0.,0.

def read(fpath):
  with open(fpath) as fin:
    dat = fin.read()
  res1 = eff_fraction( dat, 'generator level cut' )
  res2 = eff_fraction( dat, 'full event cut'      )
  return res1, res2

def evtype( fpath ):
  with open(fpath) as fin:
    dat = fin.read()
  return re.findall( r'eventType>(\d+)', dat )[0]

def main(jid):
  path = '/home/khurewat/gangadir/workspace/khurewat/LocalXML/%s/*/output/GeneratorLog.xml'
  N1 = D1 = 0.
  N2 = D2 = 0.
  for x in glob(path%jid):
    (n1,d1),(n2,d2) = read(x)
    N1 += n1 
    N2 += n2 
    D1 += d1 
    D2 += d2
  print 'PATH      : ', path 
  print 'EVTYPE    : ', evtype(x)
  print 'GenCut    : ', N1, ' / ', D1, ' = ', N1/D1
  if N2 and D2:
    print 'FullEvtCut: ', N2, ' / ', D2, ' = ', N2/D2
  print 

if __name__ == '__main__':
  for jid in sys.argv[1:]:
    main(jid)
