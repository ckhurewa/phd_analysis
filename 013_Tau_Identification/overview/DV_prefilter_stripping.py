
import os
import sys
sys.path.append(os.path.expandvars('$DIR13/overview'))
from whitelist import LIST_STRIPPING

from PhysConf.Filters import LoKi_Filters
filt = LoKi_Filters(STRIP_Code = "|".join("HLT_PASS('%s')"%s for s in LIST_STRIPPING))

from Configurables import DaVinci
DaVinci().EventPreFilters += [ filt.sequence('Stripping') ]

