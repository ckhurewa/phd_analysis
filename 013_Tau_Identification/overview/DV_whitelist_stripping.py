#!/usr/bin/env python

"""
Running only subset of stripping line which looks relevant to ditau analysis.
"""

from Configurables import DaVinci

whitelist = (
  'Ditau',
  'DisplVertices',
  'Wmu',
  'WMu',
  'We',
  'DY',
)

def pass_whitelist(s):
  return any(w in s for w in whitelist)

## It's deep...
l = DaVinci().mainSeq.Members[0].Members[0].Members[0].Members
l = [ x for x in l if pass_whitelist(x.name()) ]
DaVinci().mainSeq.Members[0].Members[0].Members[0].Members = l

print DaVinci().mainSeq.Members[0].Members[0].Members[0].Members