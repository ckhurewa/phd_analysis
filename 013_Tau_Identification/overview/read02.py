#!/usr/bin/env python

from MyPythonLib.PyROOT import import_tree
from ROOT import *
# gROOT.SetBatch(True)

# from rootpy.io import root_open
from root_numpy import tree2rec

import pandas as pd
import numpy as np

pd.options.display.width        = 400
pd.options.display.precision    = 4
pd.options.display.float_format = '{:8,.3f}'.format


from trigger_whitelist import LIST_L0, LIST_HLT1, LIST_HLT2

LIST_L0   = [ 'L0%sDecision'%s for s in LIST_L0   ]
LIST_HLT1 = [ '%sDecision'%s   for s in LIST_HLT1 ]
LIST_HLT2 = [ '%sDecision'%s   for s in LIST_HLT2 ]

def main():
  tree = import_tree(2532, 'MyTest/ditau')
  df   = pd.DataFrame(tree2rec(tree))

  dfout = df[df.itype==9]

  dfout['L0Phys']          = dfout[LIST_L0].any(axis=1)
  dfout['Hlt1MyPhys']      = dfout[LIST_HLT1].any(axis=1)
  dfout['Hlt2MyPhys']      = dfout[LIST_HLT2].any(axis=1)
  dfout['TriggeredMyPhys'] = dfout.L0Phys & dfout.Hlt1MyPhys & dfout.Hlt2MyPhys

  print 'Type: h1_mu'
  print 'PreTrigger (MC)                : ', 1.0
  print '\=> Ditau fully inacc          : ', dfout.ditau_fully_inacc.mean()
  # print '[PreTrigger] Ditau fully reconstructible: ', ...
  print '    \=> fully reconstructed    : ', dfout[dfout.ditau_fully_inacc].ditau_fully_recond.mean()
  print 

  S01 = dfout[dfout.TriggeredMyPhys & dfout.ditau_fully_recond]
  print 'L0Phys                         :', dfout.L0Phys.mean()
  print '\=> Hlt1MyPhys                 :', dfout[dfout.L0Phys].Hlt1MyPhys.mean()
  print '    \=> Hlt2MyPhys             :', dfout[dfout.L0Phys & dfout.Hlt1MyPhys].Hlt2MyPhys.mean()
  print '    |   \=> Strip MuX          :', dfout[dfout.TriggeredMyPhys].StrippingDitau_MuXLineDecision.mean()
  print '    |       \=> Ditau recoed   :', dfout[dfout.TriggeredMyPhys & dfout.StrippingDitau_MuXLineDecision].ditau_fully_recond.mean()
  print '    |   \=> Ditau fully recond :', dfout[dfout.TriggeredMyPhys].ditau_fully_recond.mean()
  print '    |       \=> Strip MuX      :', S01.StrippingDitau_MuXLineDecision.mean()
  print '    |       \=> Strip WmuJ     :', S01.StrippingWmuAKTJetsLineDecision.mean()
  print '    |       \=> Strip MuX|WmuJ :', S01[['StrippingDitau_MuXLineDecision','StrippingWmuAKTJetsLineDecision']].any(axis=1).mean()

  S02 = dfout[dfout.L0Phys & dfout.Hlt1MyPhys & dfout.Hlt2SingleMuonHighPTDecision & dfout.ditau_fully_recond]
  print '    \=> Hlt2SingleMuonHighPT   :', dfout[dfout.L0Phys & dfout.Hlt1MyPhys].Hlt2SingleMuonHighPTDecision.mean()
  print '        \=> Ditau fully recond :', dfout[dfout.L0Phys & dfout.Hlt1MyPhys & dfout.Hlt2SingleMuonHighPTDecision].ditau_fully_recond.mean()
  print '            \=> Strip MuX      :', S02.StrippingDitau_MuXLineDecision.mean()
  print '            \=> Strip Wmu      :', S02.StrippingWMuLineDecision.mean()
  print '            \=> Strip WmuJ     :', S02.StrippingWmuAKTJetsLineDecision.mean()
  print '            \=> Strip MuX|WmuJ :', S02[['StrippingDitau_MuXLineDecision', 'StrippingWMuLineDecision', 'StrippingWmuAKTJetsLineDecision']].any(axis=1).mean()



if __name__ == '__main__':
  main()
