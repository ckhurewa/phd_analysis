
#-----------#
# PREFILTER #
#-----------#

import os
import sys
sys.path.append(os.path.expandvars('$DIR13/overview'))
from whitelist import LIST_HLT1, LIST_HLT2

### PRE-FILTER
from PhysConf.Filters import LoKi_Filters
filt = LoKi_Filters(
  L0DU_Code = "L0_CHANNEL('Electron')|L0_CHANNEL('Photon')|L0_CHANNEL('Hadron')|L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')|L0_CHANNEL('Muon,lowMult')|L0_CHANNEL('DiMuon,lowMult')|L0_CHANNEL('Electron,lowMult')|L0_CHANNEL('Photon,lowMult')|L0_CHANNEL('DiEM,lowMult')|L0_CHANNEL('DiHadron,lowMult')",
  HLT1_Code = "|".join("HLT_PASS('%s')"%s for s in LIST_HLT1),
  HLT2_Code = '|'.join("HLT_PASS('%s')"%s for s in LIST_HLT2),
)

from Configurables import DaVinci
seq = filt.sequence('Trigger')
seq.RequireObjects += [ 'Hlt1/SelReports/Candidates' ]  # To evade non-triggered MCdev samples
# seq.RequireObjects = [ 'Trig/L0/L0DUReport' ]
# seq.RequireObjects = [ 'Hlt1/DecReports', 'Hlt2/DecReports' ]

DaVinci().EventPreFilters = [ seq ]
# DaVinci().EventPreFilters = filt.filters("Filters")
