#!/usr/bin/env python

from MyPythonLib.PyROOT import import_tree
from ROOT import *
# gROOT.SetBatch(True)

# from rootpy.io import root_open
from root_numpy import tree2rec

import pandas as pd
import numpy as np

pd.options.display.width        = 400
pd.options.display.precision    = 4
pd.options.display.float_format = '{:8,.3f}'.format



TYPES = ( 
  'e_e' , 'e_h1' , 'e_h3' , 'e_h5' , 'e_mu' , 'e_other', 
          'h1_h1', 'h1_h3', 'h1_h5', 'h1_mu', 'h1_other',
                   'h3_h3', 'h3_h5', 'h3_mu', 'h3_other',
                            'h5_h5', 'h5_mu', 'h5_other',
  'mu_mu', 'mu_other', 'other_other',
)

def main():

  tree  = TChain()
  tree.Add('01-10_tuple.root/MyTest/ditau')
  tree.Add('11-20_tuple.root/MyTest/ditau')
  tree.Add('21-30_tuple.root/MyTest/ditau')
  tree.Add('31-40_tuple.root/MyTest/ditau')
  df    = pd.DataFrame(tree2rec(tree))
  
  # 
  rows = {}
  for itype,tname in enumerate(TYPES):
    if 'h5' not in tname and 'other' not in tname:
      dfout  = df[df.itype==itype]
      size   = len(dfout)
      se     = dfout.sum(axis=0)
      se     = { key.replace('Decision','').replace('Stripping',''):val/size for key,val in se.iteritems() if key.startswith('Stripping') and val>0}
      se['[EVT]'] = size
      rows[tname] = se 
  dfout = pd.DataFrame(rows)
  dfout.fillna('', inplace=True)
  print dfout.to_string()
  dfout.to_csv('stripping_eff.csv')


if __name__ == '__main__':
  main()
