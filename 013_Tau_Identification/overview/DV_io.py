
#--- IOHelper

from Configurables import DaVinci
DaVinci().EvtMax  = 1000     # Number of events

from GaudiConf import IOHelper
from glob import glob
IOHelper().inputFiles(
  ## H2AA 8TeV
  # glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Brunel/*/Brunel.dst'),
  
  ## H2AA 13TeV
  # glob('/panfs/khurewat/MC/H2AA_H125_A30/13TeV_Nu1.6_25ns_100k/2361/*_Bender.dst')

  ## Z02TauTau
  # ['root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/MC/2012/ALLSTREAMS.DST/00033128/0000/00033128_00000103_1.allstreams.dst?svcClass=lhcbDst']

  ## Z02TauTau (not S21 yet!)
  # ['/panfs/khurewat/MC/official_Ztautau_42100000/00033128_00000112_1.allstreams.dst']

  ## WMu S20
  [ 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/MC/2012/ALLSTREAMS.DST/00021703/0000/00021703_00000001_1.allstreams.dst?svcClass=lhcbDst' ]

  ##2012 MCMB Triggered
  # ['root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/MC/2012/ALLSTREAMS.DST/00026814/0000/00026814_00000056_1.allstreams.dst']

  ## 2012 RealData (magDown) for S21 filtering
  # [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00041836/0000/00041836_00000008_1.ew.dst',
  #   'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/LHCb/Collision12/EW.DST/00041836/0000/00041836_00000022_1.ew.dst?svcClass=lhcbDst',
  #   "root://tbit00.nipne.ro:1094//dpm/nipne.ro/home/lhcb/LHCb/Collision12/EW.DST/00041836/0000/00041836_00000036_1.ew.dst",
  #   "root://lhcb-sdpd8.t1.grid.kiae.ru.:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/LHCb/Collision12/EW.DST/00041836/0000/00041836_00000050_1.ew.dst",
  # ]
)

