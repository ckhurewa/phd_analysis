#!/usr/bin/env python

### DaVinci
from Configurables import DaVinci
DaVinci().TupleFile       = 'tuple.root'
DaVinci().HistogramFile   = 'hist.root'
DaVinci().InputType       = 'DST'
DaVinci().DataType        = '2012'
DaVinci().Lumi            = False
DaVinci().Simulation      = True

# from Configurables import FilterInTrees, FilterDesktop
# from PhysSelPython.Wrappers import SimpleSelection, MergedSelection, AutomaticData



# tau_e   = 'tau- -> e- ...'
# tau_mu  = 'tau- -> mu- ...'
# tau_h1  = '(tau- -> X- Nu {X0} {X0} {X0} {X0}) & !(tau- -> l- ...)'
# tau_h3  = 'tau- -> X- X- X+ Nu {X0} {X0} {X0} {X0}'

tau_e   = ' X0 --> (tau- -> e-  ...) ...'
tau_mu  = ' X0 --> (tau- -> mu- ...) ...'
tau_h1  = ' X0 --> (tau- -> [pi- ,K- ] Nu {X0} {X0} {X0} {X0} {X0} {X0} {X0}) ...' # || (X0 --> (tau- -> K- {X0} {X0} {X0} {X0}) ...)'
tau_h3  = ' X0 --> (tau- -> X- X- X+ Nu ...) ...'

def conj(s):
  return s.replace('- ', '@').replace('+ ', '- ').replace('@', '+ ')

def desc_tau(s):
  return "(MCSOURCE('MC/Particles', '({})') >> ~MCEMPTY)".format(s)

from Configurables import LoKi__VoidFilter as Filter
def make_ditau_filter(name, desc1, desc2):
  code1 = "({} & {})".format(desc_tau(desc1), desc_tau(conj(desc2)))
  code2 = "({} & {})".format(desc_tau(desc2), desc_tau(conj(desc1)))
  return Filter(name,
    Code       = code1 + '|' + code2,
    Preambulo = [ "from LoKiMC.decorators import *" ],
  )

def make_tauminus_NOT_filter(name, desc):
  return Filter(name, 
    Code = desc_tau('!(%s)'%desc),
    Preambulo = [ "from LoKiMC.decorators import *" ],    
  )


filt = Filter('filt',
  Code      = "(MCSOURCE('MC/Particles', '(!(X0 --> (tau- -> e- ...) ...))') >> ~MCEMPTY)",
  # Code      = "(MCSOURCE('MC/Particles', ' X0 --> (tau- -> e- ...) ... ') >> ~MCEMPTY) & (MCSOURCE('MC/Particles', ' X0 --> (tau+ -> e+ ...) ... ') >> ~MCEMPTY)" , 
  # Code      = "MCSOURCE('MC/Particles', ' X0 --> (tau- -> pi- ...) (tau+ -> pi+ ...) ') >> ~MCEMPTY " , 
  # Code      = "MCSOURCE('MC/Particles', ' X0 -> (tau- -> e- ...) (tau+ -> mu+ ...) ') >> ~MCEMPTY " , 
  # Code      = "MCSOURCE('MC/Particles', ' X0 -> (tau- -> e- ...) (tau+ -> e+ ...) ') >> ~MCEMPTY " , 
  Preambulo = [ "from LoKiMC.decorators import *" ],
)


from Configurables import GaudiSequencer
seq = GaudiSequencer('MyDitau')
# seq.ModeOR = True
# seq.ShortCircuit = False
seq.Members = [ 
  make_tauminus_NOT_filter('tau_e' , tau_e),
  make_tauminus_NOT_filter('tau_h1', tau_h1),
  make_tauminus_NOT_filter('tau_h3', tau_h3),
  make_tauminus_NOT_filter('tau_mu', tau_mu),
  #
  # make_ditau_filter('ditau_e_e', tau_e, tau_e),
  # make_ditau_filter('ditau_e_h1', tau_e, tau_h1),
  # make_ditau_filter('ditau_e_h3', tau_e, tau_h3),
  # make_ditau_filter('ditau_e_mu', tau_e, tau_mu),
  # make_ditau_filter('ditau_h1_h1', tau_h1, tau_h1),
  # make_ditau_filter('ditau_h1_h3', tau_h1, tau_h3),
  # make_ditau_filter('ditau_h1_mu', tau_h1, tau_mu),
  # make_ditau_filter('ditau_h3_h3', tau_h3, tau_h3),
  # make_ditau_filter('ditau_h3_mu', tau_h3, tau_mu),
  # make_ditau_filter('ditau_mu_mu', tau_mu, tau_mu),
]


# DaVinci().EventPreFilters = [ seq ]
DaVinci().EventPreFilters = [ filt ]
# DaVinci().EventPreFilters = filt.filters("Filters")

from Configurables import PrintMCTree
DaVinci().UserAlgorithms = [PrintMCTree(ParticleNames=['tau-'], Depth=1)]

