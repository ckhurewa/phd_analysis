
from Configurables import DaVinci

# Remove the MyDSTWriterMainSeq
DaVinci().mainSeq.Members = [ seq for seq in DaVinci().mainSeq.Members if 'MyDSTWriter' not in seq.name() ]
print DaVinci().mainSeq.Members