
from glob import glob

## Re-Stripping21
opts_restrip21 = [
  # 'DV-Stripping21-Stripping-MC-QEE.py',
  'DV-Stripping21-WMuControl10.py',
  '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r207/options/DaVinci/DataType-2012.py',
  '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r207/options/DaVinci/InputType-DST.py',
  # '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r207/options/DaVinci/StrippingDST-DecReports.py', # not working
  '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r207/options/Persistency/Compression-ZLIB-1.py',
  'DV_xml.py',
  ## CAREFUL MAGUP/MAGDOWN
  # '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r207/options/Conditions/Sim08a-2012-md100.py',
  '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r213/options/Conditions/Sim08a-2012-mu100.py',
]

# ## Filter stripped S21 from real data
# opts_filter_S21 = [
#  'DV_prefilter_stripping.py',
#   '/home/khurewat/analysis/902_fullsim_resources/ForceOutput-DaVinci.py',
# ]

j = Job(
  name        = 'WMu 8TeV S21',
  # application = Bender(module='bender.py'), 
  application = DaVinci(version='v36r1p1', optsfile=opts_restrip21),
  backend     = LSF(extraopts='--mem=3200 -t 1-0:0:0'), 
  # comment     = 'Running S21QEE on 8TeV Z02TauTau MagUp.',
  comment     = 'Running S21QEE on 8TeV WMu MagUp. (prescale=0.05, WMu+DitauLines)',
  # comment     = 'Test Z02TauTau 8TeV S21 magDown',
)

j.inputdata = LHCbDataset.new(
  ## H2AA 8TeV S21
  # glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Brunel/*/Brunel.dst')

  ## Z02TauTau 8TeV
  # 'evt://MC/2012/42100000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  # 'evt://MC/2012/42100000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',

  ## W_munumu=PHOTOS (42311002)
  # 'sim+std://MC/2012/Beam4000GeV-JulSep2012-MagDown-Nu2.5-EmNoCuts/Sim06b/Trig0x40990042Flagged/Reco14/Stripping20NoPrescalingFlagged/42311002/ALLSTREAMS.DST',
  'sim+std://MC/2012/Beam4000GeV-JulSep2012-MagUp-Nu2.5-EmNoCuts/Sim06b/Trig0x40990042Flagged/Reco14/Stripping20NoPrescalingFlagged/42311002/ALLSTREAMS.DST',
)

# j.application.events = 100  # Per subjobs
# j.application.extraopts = "from Configurables import DaVinci; DaVinci().EvtMax = 1000"

#---------------------------------------------

j.splitter = SplitByFiles(filesPerJob=1, bulksubmit=True, ignoremissing=True)
j.outputfiles = [ MassStorageFile('*.xml'), MassStorageFile('*root'),  MassStorageFile('*dst') ]
j.submit()
