#!/usr/bin/env python

from BenderCK import *
from BenderCK.Tau import *

# class TauTriggerStrippingStatsAlgoMC(BaseTauAlgoMC):

#   REPORT_LOCS = (
#     'HltLikeL0/DecReports', 
#     'Hlt1/DecReports', 
#     'Hlt2/DecReports', 
#     'Strip/Phys/DecReports',
#   )

#   def gen_trigger_stripping(self):
#     for loc in self.REPORT_LOCS:
#       reports = self.get(loc)
#       if reports:
#         for name, rep in reports.decReports().iteritems():
#           name = name.replace(',','_')  # better compat
#           val  = rep.decision()==1
#           yield name, val

#   #--------------------------------------------

#   def analyse(self):

#     print self.event_break

#     ## Loop over ditau (Full event)
#     for ditau in self.list_ditau:
#       with self.enhancedTuple('ditau') as tup:
#         tup.column('itype'        , ditau.itype)
#         tup.column('is_type_MuX'  , ditau.is_type_MuX)
#         tup.column('is_type_EX'   , ditau.is_type_EX)
#         tup.fill_gen( self.gen_trigger_stripping() )
#         tup.EventInfo.fill()

#     self.setFilterPassed(True)
#     return SUCCESS


#==============================================================

class Z02MuMu_fiducial_eff(BaseTauAlgoMC):
  """
  Aim to help resolving efficiency embedded in Ronan's Z0 csc result.
  """

  def good(self, mu):
    eta = MCETA(mu)
    if (eta < 2.0) or (eta>4.5):
      return False 
    if MCPT(mu) < 20*GeV:
      return False 
    return True

  @pass_success
  def analyse(self):
    for ditau in self.list_ditau:
      valid = False
      if ditau.type.mu_mu:
        mu1 = ditau.tau1.children(CHARGED)[0]
        mu2 = ditau.tau2.children(CHARGED)[0]
        if self.good(mu1) and self.good(mu2):
          valid = True
      counter = self.Counter('valid_fiducial')
      counter += int(valid)




#==============================================================

class Z02MuMu_fiducial_eff(BaseTauAlgoMC):
  """
  Aim to help resolving efficiency embedded in Ronan's Z0 csc result.
  """

  def good(self, mu):
    eta = MCETA(mu)
    if (eta < 2.0) or (eta>4.5):
      return False 
    if MCPT(mu) < 20*GeV:
      return False 
    return True

  @pass_success
  def analyse(self):
    for ditau in self.list_ditau:
      valid = False
      if ditau.type.mu_mu:
        mu1 = ditau.tau1.children(CHARGED)[0]
        mu2 = ditau.tau2.children(CHARGED)[0]
        if self.good(mu1) and self.good(mu2):
          valid = True
      counter = self.Counter('valid_fiducial')
      counter += int(valid)




#==============================================================

class Z02MuMu_fiducial_eff(BaseTauAlgoMC):
  """
  Aim to help resolving efficiency embedded in Ronan's Z0 csc result.
  """

  def good(self, mu):
    eta = MCETA(mu)
    if (eta < 2.0) or (eta>4.5):
      return False 
    if MCPT(mu) < 20*GeV:
      return False 
    return True

  @pass_success
  def analyse(self):
    for ditau in self.list_ditau:
      valid = False
      if ditau.type.mu_mu:
        mu1 = ditau.tau1.children(CHARGED)[0]
        mu2 = ditau.tau2.children(CHARGED)[0]
        if self.good(mu1) and self.good(mu2):
          valid = True
      counter = self.Counter('valid_fiducial')
      counter += int(valid)




#==============================================================

def configure ( inputdata, catalogs = [], castor = True ) :

  seq = setdata_and_configure( inputdata , catalogs , castor ,

    ## Filter trigger line + Reconstructible
    # '$DIR13/overview/DV_prefilter_trigger.py',   # Optional if trigger available
    # '$DIR11/S23/DV_prefilter_reco.py',           # Optional if is MC signal

    ## Derived recipe from S21,
    # '$APPCONFIGOPTS/DaVinci/DV-Stripping21-Stripping-MC.py',
    # '$DIR13/overview/DV-Stripping21-Stripping-MC-QEE.py',  # Save DST from QEE WG
    # '$APPCONFIGOPTS/DaVinci/DataType-2012.py',
    # '$APPCONFIGOPTS/DaVinci/InputType-DST.py',

    ## L0 + Hlt1 + Hlt2 filter
    # '$DIR13/overview/DV_exclude_DSTWriter.py',
    # '$DIR13/overview/DV_whitelist_stripping.py',  # Run only subset of stripping (performance reason)

  )

  seq.Members += [
    # EchoTypeAlgoMC().name(),
    # TauRecoStatsAlgoMC().name(),
    Z02MuMu_fiducial_eff().name(),
  ]

  return SUCCESS

#--------------------------------------------------------------

if __name__ == '__main__':

  # uri = glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Brunel/*/Brunel.dst')
  # uri = glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Stripping21/*/*.dst')

  ## Z0 (not S21 yet!)
  uri = '/panfs/khurewat/MC/official_Ztautau_42100000/00033128_00000112_1.allstreams.dst'

  configure(uri)
  run(1000)
