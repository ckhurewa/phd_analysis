
from PhysSelPython.Wrappers import *
# from Configurables import LoKi__VoidFilter as VoidFilter
# from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, MultiSelectionSequence


from PhysConf.Filters import LoKi_Filters
algo = LoKi_Filters(VOID_Code="CONTAINS('Rec/Vertex/Primary') > 0").filters('filt')[0]
sel  = EventSelection('Sel', Algorithm=algo)
seq  = SelectionSequence('Seq', TopSelection = sel)


from Configurables import SelDSTWriter
writer = SelDSTWriter("MyDSTWriter", SelectionSequences=[ seq ])

from Configurables import DaVinci
DaVinci().UserAlgorithms = [ writer.sequence() ]

