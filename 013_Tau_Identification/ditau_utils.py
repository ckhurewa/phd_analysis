#!/usr/bin/env python

import re
import os
import sys
import math
import functools
import itertools
import numpy as np
import pandas as pd
import uncertainties
import __main__

from tabulate import tabulate
from PythonCK import logger
from PythonCK.decorators import memorized, optional_arg_decorator, report_info
from PythonCK.stringutils import try_int_else_float, remove_vertical_duplicate

from uncertainties import ufloat, nominal_value, std_dev

pd.options.display.width = 320 
Idx = pd.IndexSlice

## factorized local tools
import drawers
from decorators import *

## EXPERIMENTAL for concurrency
# https://github.com/alex-sherman/deco
import deco

#===============================================================================

def get_current_dtype_dt():
  """
  Optionally take from sys.argv
  """
  dtype = 'h3_mu'
  if len(sys.argv) > 1:
    for arg in sys.argv:
      if arg in DTYPES:
        dtype = arg 
        break
      if arg in DT_TO_DTYPE:
        dtype = DT_TO_DTYPE[arg]
        break
      if arg in ['mu_mu_withz', 'mumu_withz']:  # pseudo-channel
        dtype = 'mu_mu_withz'
        break
  logger.info('Study dtype: %s'%dtype)
  return dtype, DTYPE_TO_DT[dtype]


#===============================================================================
# CONSTANTS
#===============================================================================

## Order follows the latex note
DTYPES     = ['mu_mu', 'h1_mu', 'h3_mu', 'e_e', 'e_h1', 'e_h3', 'e_mu']
DTYPES_ALL = DTYPES + ['h1_h1', 'h1_h3', 'h3_h3']

CHANNELS     = ['mumu', 'h1mu', 'h3mu', 'ee', 'eh1', 'eh3', 'emu']
CHANNELS_ALL = CHANNELS + ['h1h1', 'h1h3', 'h3h3']

# For HMT
TTYPES = ['mu', 'h1', 'h3', 'e']

## Map dt to dtype
DT_TO_DTYPE = {
  'mumu': 'mu_mu',
  'h1mu': 'h1_mu',
  'h3mu': 'h3_mu',
  'ee'  : 'e_e',
  'eh1' : 'e_h1',
  'eh3' : 'e_h3',
  'emu' : 'e_mu',
}

## inverse
DTYPE_TO_DT = {
  'e_e'  : 'ee',
  'e_h1' : 'eh1',
  'e_h3' : 'eh3',
  'e_mu' : 'emu',
  'h1_h1': 'h1h1',
  'h1_h3': 'h1h3',
  'h1_mu': 'h1mu',
  'h3_h3': 'h3h3',
  'h3_mu': 'h3mu',
  'mu_mu': 'mumu',
}

## For Higgs-> mutau convention
TT_TO_DT = {
  'e' : 'emu',
  'mu': 'mumu',
  'h1': 'h1mu',
  'h3': 'h3mu',
}

## Helper grouper
DTYPE_TO_DGROUP = {
  'e_e'  : 'll',
  'mu_mu': 'll',
  'e_mu' : 'll',
  'h1_mu': 'lh1',
  'e_h1' : 'lh1',
  'h3_mu': 'lh3',
  'e_h3' : 'lh3',
}

## 
DT_TO_LATEX = {
  'mumu': '\\dmumu',
  'h1mu': '\\dmuh1',
  'h3mu': '\\dmuh3',
  'emu' : '\\dmue',
  'ee'  : '\\dee',
  'eh1' : '\\deh1',
  'eh3' : '\\deh3',
  'mue' : '\\dmue',
  #
  'h1h1': '\\ditauhh',
  'h1h3': '\\ditauhH',
  'h3h3': '\\ditauHH',
}

## Prefixes used as name of the branches
# Choose the higher-PT one up front, except for emu, prefer muon first.
# this will be consistent with Higgs->mutau analysis where first object is 
# always a muon.
PREFIXES = {
  'ee'   : ( 'e1' , 'e2'  ),
  'eh1'  : ( 'e'  , 'pi'  ),
  'eh3'  : ( 'e'  , 'tau' ),
  'emu'  : ( 'mu' , 'e'   ),
  'h1mu' : ( 'mu' , 'pi'  ),
  'h3mu' : ( 'mu' , 'tau' ),
  'mumu' : ( 'mu1', 'mu2' ),
}

## Use for exporting tree to subtree
PREFIXES_ALL = {
  'mumu': ( 'mu1', 'mu2'),
  'ee'  : ( 'e1' , 'e2' ),
  'emu' : ( 'e'  , 'mu' ),
  'h1mu': ( 'mu' , 'pi' ),
  'eh1' : ( 'e'  , 'pi' ),
  'h3mu': ( 'mu' , 'tau', 'pr1', 'pr2', 'pr3' ),
  'eh3' : ( 'e'  , 'tau', 'pr1', 'pr2', 'pr3' ),
}

## Labels suitable for x-axis, follow the order above
LABELS = {
  'ee'   : ( 'e1'  , 'e2'  ),
  'eh1'  : ( 'e'   , 'h1'  ),
  'eh3'  : ( 'e'   , 'h3'  ),
  'emu'  : ( '#mu' , 'e'   ),
  'h1mu' : ( '#mu' , 'h1'  ),
  'h3mu' : ( '#mu' , 'h3'  ),
  'mumu' : ( '#mu1', '#mu2'),  
}
LABELS['mumu_withz'] = LABELS['mumu']

# Single tau
# Try to have same vertical level
LABELS_TAU = {
  'e' : '#it{#tau}#scale[0.7]{#lower[0.6]{#it{e}}}',
  'h1': '#it{#tau}#scale[0.7]{#lower[0.4]{#it{h}1}}',
  'h3': '#it{#tau}#scale[0.7]{#lower[0.4]{#it{h}3}}',
  'mu': '#it{#tau}#scale[0.7]{#lower[0.4]{#it{#mu}}}',
  'l' : '#it{#tau}#scale[0.7]{#lower[0.4]{#it{l}}}', # generic
}

# +/- superscript (charge)
_POW_NEG = '#lower[-0.8]{#kern[-1.0]{-}}'
_POW_POS = '^{#kern[-0.7]{+}}'

## Separated, used in 2D plot (e.g., acceptance). 
# Also with explicit charge for same particle.
LABELS_DITAU_SEP = {
  'ee'  : (LABELS_TAU['e'] +_POW_NEG, LABELS_TAU['e'] +_POW_POS),
  'mumu': (LABELS_TAU['mu']+_POW_NEG, LABELS_TAU['mu']+_POW_POS),
  'emu' : (LABELS_TAU['e'] , LABELS_TAU['mu']), # draw as x=e, y=mu, to show drag horizontally
  'h1mu': (LABELS_TAU['mu'], LABELS_TAU['h1']),
  'eh1' : (LABELS_TAU['e'] , LABELS_TAU['h1']),
  'h3mu': (LABELS_TAU['mu'], LABELS_TAU['h3']),
  'eh3' : (LABELS_TAU['e'] , LABELS_TAU['h3']),
}

## slight different, asymmetric
# Used for unbiased same-sign plot
LABELS_DITAU_SEP2 = {
  'ee'  : (LABELS_TAU['e'] +'#scale[0.5]{#lower[0.35]{1}}', LABELS_TAU['e']+'#scale[0.5]{#lower[0.35]{2}}'),
  'mumu': (LABELS_TAU['mu']+'#scale[0.5]{#lower[0.5]{1}}', LABELS_TAU['mu']+'#scale[0.5]{#lower[0.5]{2}}'),
  'emu' : (LABELS_TAU['mu'], LABELS_TAU['e']), # follow standard convention of muon first
  'h1mu': (LABELS_TAU['mu'], LABELS_TAU['h1']),
  'eh1' : (LABELS_TAU['e'] , LABELS_TAU['h1']),
  'h3mu': (LABELS_TAU['mu'], LABELS_TAU['h3']),
  'eh3' : (LABELS_TAU['e'] , LABELS_TAU['h3']),  
}

## Suitable for ROOT label
LABELS_DITAU = {
  'ee'   : LABELS_TAU['e']+LABELS_TAU['e'],
  'eh1'  : LABELS_TAU['e']+LABELS_TAU['h1'],
  'eh3'  : LABELS_TAU['e']+LABELS_TAU['h3'],
  'emu'  : LABELS_TAU['mu']+LABELS_TAU['e'],
  'h1h1' : LABELS_TAU['h1']+LABELS_TAU['h1'],
  'h1h3' : LABELS_TAU['h1']+LABELS_TAU['h3'],
  'h1mu' : LABELS_TAU['mu']+LABELS_TAU['h1'],
  'h3h3' : LABELS_TAU['h3']+LABELS_TAU['h3'],
  'h3mu' : LABELS_TAU['mu']+LABELS_TAU['h3'],
  'mumu' : LABELS_TAU['mu']+LABELS_TAU['mu'],
}

## From process name to pretty ROOT labels
PROCESS_ROOTLABELS = {
  'OS'   : 'Data',
  'Data' : 'Data',
  'Ztau' : '#it{Z}#rightarrow#it{#tau}^{+}#it{#tau}#lower[-0.55]{-}',
  'Zll'  : '#it{Z}#rightarrow#it{l^{+}l#lower[-0.85]{-}}',
  'Zmu'  : '#it{Z}#rightarrow#mu#mu',
  'Ze'   : '#it{Z}#rightarrow#it{ee}',
  'QCD'  : 'QCD',
  'EWK'  : '#it{Vj}',
  'EWKmu': '#it{Vj}',
  'EWKe' : '#it{Vj}',
  'WW+WZ': '#it{WW}+#it{WZ}',
  'VV'   : '#it{WW}+#it{WZ}',  # alternative name
  'ttbar': 'ttbar',
  'Zbb'  : '#it{Z}#rightarrow b#bar{b}',
  'ZXC'  : 'Cross-feed',
  'Other': 'Other',
}

## For gallery
TREENAME_TO_ROOTLABELS = {
  'ztautau'  : PROCESS_ROOTLABELS['Ztau'],
  'dymu'     : PROCESS_ROOTLABELS['Zmu'],
  'cc_hardmu': 'c#bar{c}',
  'wmu17jet' : 'W+jet',
}


## From process name to pretty latex name
PROCESS_LATEX = {
  'OS'    : 'Observed',
  'BKG'   : 'Backgrounds',
  'Zll'   : '\\zll',
  'Ztau'  : '\\ztautau',
  'Zmu'   : '\\zmumu',
  'Ze'    : '\\zee',
  'QCD'   : 'QCD',
  'EWK'   : '\\Vj',
  'Vj'    : '\\Vj',
  'SS'    : 'QCD+\\Vj',
  'VV'    : 'VV',
  'ttbar' : '\\ttbar',
  'Zbb'   : '\\zbb',
  'ZXC'   : 'Cross-feed',
  'equiv' : 'Equivalent',
}

## Treename to pretty latex alias
TREENAME_TO_LATEX = {
  'ztautau0'      : '\\ztautau', #Central version, flat egen
  'dymu'          : '\\zmumu',
  'dye10'         : '\\zee',
  'dye40'         : '\\zee',
  'zbb'           : '\\zbb',
  'wtaujet'       : '\\wtaujet',
  # 'wmujet'        : None                 ,
  'wmu17jet'      : '\\wmujet',
  'we'            : '\\we',
  'wejet'         : '\\wejet',
  'we20jet'       : 'WE20JET',
  'wmumujet'      : '\\wmumujet',
  # 'wtaubb'        : None                 ,
  'ztaujet'       : '\\ztaujet',
  'zmu17jet'      : '\\zmujet',
  'zmu17cjet'     : 'ZMU17CJET',
  'zmu17bjet'     : 'ZMU17BJET',
  'hardqcd'       : 'HARDQCD',
  'cc_harde'      : '\\ccbare',
  'cc_hardmu'     : '\\ccbarmu',
  'bb_harde'      : '\\bbbare',
  'bb_hardmu'     : '\\bbbarmu',
  'mcmb_mux'      : 'MCMBMUX',
  'ttbar_41900006': '\\ttbar',
  'ttbar_41900007': '\\ttbar',
  'ttbar_41900010': '\\ttbar',
  'WW_ll'         : '\\wwll',
  'WW_lx'         : '\\wwlx',
  'WZ_lx'         : '\\wzlx',
}

TREENAME_TO_DECID = {
  'ztautau0'      : 42100000, #Central version, flat egen
  'dymu'          : 42112011,
  'dye10'         : 42122011,
  'dye40'         : 42122001,
  'zbb'           : 42150000,
  'wtaujet'       : 42300010,
  'wmu17jet'      : 42311011,
  'we'            : 42321000,
  'wejet'         : 42321010,
  'we20jet'       : None,
  'wmumujet'      : 42311012,
  'ztaujet'       : 42100020,
  'zmu17jet'      : 42112022,
  'zmu17cjet'     : 42112052,
  'zmu17bjet'     : 42112053,
  'hardqcd'       : 49001000,
  'cc_harde'      : 49021004,
  'cc_hardmu'     : 49011004,
  'bb_harde'      : 49021005,
  'bb_hardmu'     : 49011005,
  # 'mcmb_mux'      : 'MCMBMUX'             ,
  'ttbar_41900006': 41900006,
  'ttbar_41900007': 41900007,
  'ttbar_41900010': 41900010,
  'WW_ll'         : 41922002,
  'WW_lx'         : 42021000,
  'WZ_lx'         : 42021001,
}

## Computation of csc, for nice latex label
COMPUTE_LABELS = {
  'acc'   : '\\eacc',
  'beam'  : 'Beam energy',
  'br'    : '\\BR',
  'eacc'  : '\\eacc',
  'etrack': '\\etrack',
  'ekine' : '\\ekine',
  'epid'  : '\\ePID',
  'egec'  : '\\egec',
  'etrig' : '\\etrig',
  'erec'  : '\\erec',
  'esel'  : '\\esel',
  'lumi'  : 'Luminosity',
  'nsig'  : '\\nsig',
  'nexp'  : '\\nexp',
  'purity': '\\purity',
  'rec'   : '\\erec',
  'sel'   : '\\esel',
  'stat'  : 'Statistical',
  'syst'  : 'Systematic',
  'val'   : 'val', 
  'efftot': '\\etot'
}

#===============================================================================

@memorized
def Lumi(with_beam=True):
  """
  Constant Run-I 8TeV luminosity
  """
  ##
  # lumi0   = ufloat( 1990.99102712, 23.0954959136, 'lumi' )
  # run1    = 4.52  # {'runNumber < 111802', '111890 < runNumber'} # excluded in Zee 8TeV
  # run2    = 8.94  # {'runNumber < 126124', '126160 < runNumber'} # excluded in Zee 8TeV, Zmumu 8TeV
  # run3    = 1.32  # {'runNumber < 129530', '129539 < runNumber'} # excluded in ?
  # lumi_e  = lumi0 * ( lumi0.n -run1-run2-run3 )/lumi0.n
  # lumi_mu = lumi0 * ( lumi0.n -run2 )/lumi0.n
  # lumi = lumi0 * ( lumi0.n -run1-run2-run3 )/lumi0.n

  ## v161022
  lumi0 = ufloat(1989.63256943, 23.0797378053, 'lumi')
  run2  = 13.45371967 # exclude [126124, 126160]
  # run2  = 0.
  lumi  = lumi0 * ( lumi0.n -run2 )/lumi0.n  # it seems that run1,run3 have already been excluded.

  ## Add also fully correlated 1.15% beam uncertainty
  if with_beam:
    # f_beam = ufloat( 1, 0.0115   , 'beam' ) # Original version: 1.15%
    f_beam = ufloat( 1, 0.18/100., 'beam')  # New ebeam suggested by Roger, 0.18% 
    lumi *= f_beam
  return lumi


@memorized
def Br_tau():
  """
  Branching ratio of tau lepton.
  Watch out for the correlation.
  """
  br_tau = {
    'e' : ufloat( .1783, .0004, 'br' ),
    'mu': ufloat( .1741, .0004, 'br' ),
    'h3': ufloat( .1457, .0007, 'br' ),
  }
  br_tauh1 = ufloat( .8535, .0007, 'br' ) - br_tau['e'] - br_tau['mu']
  br_tau['h1'] = ufloat( br_tauh1.n, br_tauh1.s, 'br' ) # decorrelate from e,mu
  se = pd.Series(br_tau)
  se.name = 'brtau'
  se.index.name = 'tt'
  return se

@memorized
def Br_ditau():
  """
  Branching ratio for 2 tau leptons.
  """
  ## Grab the single-tau branching ratio
  br_tau = Br_tau()
  return pd.Series({
    'mumu': br_tau['mu'] * br_tau['mu'],
    'h1mu': br_tau['h1'] * br_tau['mu'] * 2.,
    'h3mu': br_tau['h3'] * br_tau['mu'] * 2.,
    'ee'  : br_tau['e']  * br_tau['e'],
    'eh1' : br_tau['e']  * br_tau['h1'] * 2.,
    'eh3' : br_tau['e']  * br_tau['h3'] * 2.,
    'emu' : br_tau['e']  * br_tau['mu'] * 2.,
  })

#===============================================================================

def sum_fullcorr(se):
  """
  Perform the sum of ufloats, adding syst linearly (full correlation)
  """
  nom = se.apply(nominal_value).sum()
  dev = se.apply(std_dev).sum()
  return ufloat(nom, dev)

def completely_correlate(se):
  """
  Given a series of ufloat, make it into a series of ufloat, such that they
  are correlated in uncertainties given by Poissonian error on the sum.
  Useful for binning counting exp.
  """
  def resolve(x):
    if x is None:
      return 0., 0.
    return uncertainties.nominal_value(x), uncertainties.std_dev(x)
  ones    = np.ones([len(se),len(se)])
  inputs  = [ resolve(x) for x in se ] 
  res     = uncertainties.correlated_values_norm(inputs, ones)
  if isinstance(se, pd.Series): 
    se2     = pd.Series(res)
    se2.index = se.index
    res = se2
  return res


#===============================================================================
# PRETTY PRINTER
#===============================================================================

def fmt2cols(var, decimal=1):
  """
  format the 2-columns cell compat with \ditautabular latex.
  """
  if var is None:
    return '\\bicol{---}  '
  if isinstance(var, basestring):
    return '\\bicol{%s}'%var
  n = uncertainties.nominal_value(var)
  s = uncertainties.std_dev(var)
  if pd.np.isnan(n):
    return '\\bicol{---}  '
  if n==1. and s < 1e-3:  # unity case
    return '\\bicol{1}'
  if n==100. and s < 1e-3:  # unity case (percent)
    return '\\bicol{100}'
  tmp = '{:5.%if} & {:4.%if}'%(decimal, decimal)
  return tmp.format(n,s)

fmt2cols2 = lambda x: fmt2cols(x, 2) 
fmt2cols_percent = lambda x: fmt2cols(x*100.)

## Force sf printer
def _fmtnf(n, percent=False):
  @ignore_string
  def fmt(x):
    if (x is None or isinstance(x, (bool, list, tuple))):
      return x
    if pd.np.isnan(nominal_value(x)): # cannot handle arbitary input
      return x
    if percent:
      x *= 100.
    N = min(max(n,1)*3, 5) # entire length
    return ('{:%i.%if}'%(N, n)).format(x)
  return fmt

@ignore_string
def _fmtint(x):
  x = uncertainties.nominal_value(x)
  if pd.np.isnan(x):
    return x
  return str(int(x))

pd.Series.fmt1f    = property(lambda x: x.apply(_fmtnf(1)))
pd.Series.fmt2f    = property(lambda x: x.apply(_fmtnf(2)))
pd.Series.fmt3f    = property(lambda x: x.apply(_fmtnf(3)))
pd.Series.fmt1p    = property(lambda x: x.apply(_fmtnf(1, True)))
pd.Series.fmt2p    = property(lambda x: x.apply(_fmtnf(2, True)))
pd.Series.fmt3p    = property(lambda x: x.apply(_fmtnf(3, True)))
pd.Series.fmt4p    = property(lambda x: x.apply(_fmtnf(4, True)))
pd.DataFrame.fmt0f = property(lambda x: x.applymap(_fmtnf(0)))
pd.DataFrame.fmt1f = property(lambda x: x.applymap(_fmtnf(1)))
pd.DataFrame.fmt2f = property(lambda x: x.applymap(_fmtnf(2)))
pd.DataFrame.fmt3f = property(lambda x: x.applymap(_fmtnf(3)))
pd.DataFrame.fmt4f = property(lambda x: x.applymap(_fmtnf(4)))
pd.DataFrame.fmt1p = property(lambda x: x.applymap(_fmtnf(1, True)))
pd.DataFrame.fmt2p = property(lambda x: x.applymap(_fmtnf(2, True)))
pd.DataFrame.fmt3p = property(lambda x: x.applymap(_fmtnf(3, True)))
pd.DataFrame.fmtint = property(lambda x: x.applymap(_fmtint))

@ignore_string
def asymvar_to_latex(avar, decimals=1):
  """
  Based on \tol{nom}{hi}{low} latex function.
  """
  s0  = '\\tol{{%s}}{{%s}}{{%s}}'
  v1  = float(avar)
  v2  = avar.ehigh
  v3  = abs(avar.elow)
  if v1 > 1E3: v1 = int(v1)
  if v2 > 1E3: v2 = int(v2)
  if v3 > 1E3: v3 = int(v3)
  fm1 = '{:.%if}'%decimals if v1<1E3 else '{}'
  fm2 = '{:.%if}'%decimals if v2<1E3 else '{}'
  fm3 = '{:.%if}'%decimals if v3<1E3 else '{}'
  s0  = s0%(fm1,fm2,fm3)
  return s0.format(v1, v2, v3)


@ignore_string
def ufloat_to_latex(x, decimals=1):
  """
  My writing of ufloat in latex.
  If value if null in any non-number kind (None, ''), return null string
  """
  if x is None or x == '':
    return ''
  if isinstance(x, float): # If already float, no extra formatting, just decimals
    # guard in $$ to force math mode
    return '${:.{}f}$'.format(x, decimals)
  return '\\U{{{:.{}f}}}'.format(x, decimals).replace('+/-','}{')

ufloat_to_latex1 = lambda x: ufloat_to_latex(x, 1)
ufloat_to_latex2 = lambda x: ufloat_to_latex(x, 2)
ufloat_to_latex3 = lambda x: ufloat_to_latex(x, 3)

def var_to_latex(x, decimals=1):
  """
  Joint compat for ufloat and asymvar
  """
  if x.__class__.__name__ == 'asymvar': # avoid import PyrootCK yet
    return asymvar_to_latex(x, decimals)
  return ufloat_to_latex(x, decimals)


# often-used adapter for 2 decimals
var_to_latex1 = lambda x: var_to_latex(x, 1)
var_to_latex2 = lambda x: var_to_latex(x, 2)
var_to_latex3 = lambda x: var_to_latex(x, 3)
var_to_latex_percent1 = lambda x: var_to_latex(x if isinstance(x, basestring) else x*100, 1)
var_to_latex_percent2 = lambda x: var_to_latex(x if isinstance(x, basestring) else x*100, 2)
var_to_latex_percent3 = lambda x: var_to_latex(x if isinstance(x, basestring) else x*100, 3)

## bind to pandas
pd.DataFrame.fmt1l  = property(lambda x: x.applymap(var_to_latex1))
pd.DataFrame.fmt2l  = property(lambda x: x.applymap(var_to_latex2))
pd.DataFrame.fmt3l  = property(lambda x: x.applymap(var_to_latex3))
pd.DataFrame.fmt1lp = property(lambda x: x.applymap(var_to_latex_percent1))
pd.DataFrame.fmt2lp = property(lambda x: x.applymap(var_to_latex_percent2))
pd.DataFrame.fmt3lp = property(lambda x: x.applymap(var_to_latex_percent3))
pd.Series.fmt1lp    = property(lambda x: x.apply(var_to_latex_percent1))
pd.Series.fmt2lp    = property(lambda x: x.apply(var_to_latex_percent2))
pd.Series.fmt3lp    = property(lambda x: x.apply(var_to_latex_percent3))
pd.Series.fmt1l     = property(lambda x: x.apply(var_to_latex1))
pd.Series.fmt2l     = property(lambda x: x.apply(var_to_latex2))
pd.Series.fmt3l     = property(lambda x: x.apply(var_to_latex3))


#===============================================================================

def se_to_markdown(se):
  raise NotImplementedError
  return tabulate(se)
  pass

pd.Series.to_markdown = se_to_markdown

#-------------------------------------------------------------------------------

def df_to_markdown(df0, formatters=None, floatfmt='.2f', rename_columns=None, drop_index=False, stralign='left', aligns=None):
  """
  Chose rename column after formetter because oftenly the original name is simple,
  but the printing name is verbose.
  """
  ## Make independent copy, nullify the index name
  df = df0.copy()
  if df.index.name is None:
    df.index.name = ''

  ## Before drop index, inject midrule if multiindex
  if isinstance(df0.index, pd.MultiIndex):
    marks = [i for i,dup in enumerate(df0.index.get_level_values(0).duplicated()) if not dup]
    inject_midrule(df, *marks[1:])

  ## Drop the index down
  df = df.reset_index(drop=drop_index)
  
  ## Apply formatter before rename
  if formatters:
    for key, func in formatters.iteritems():
      if key in df:
        df[key] = df[key].apply(func)
      else:
        print "Warning: Given key doesn't match any columns:", key
  
  ## Prepare list of columns explicitly
  columns = df.columns.tolist()
  if rename_columns:
    columns = [ rename_columns.get(s,s) for s in columns ]

  ## Call tabulate
  msg = tabulate(df.values, columns, floatfmt=floatfmt, stralign=stralign)

  ## In case of multiindex, iterate alongside with duplicate index, and
  # replace it to empty string.
  if isinstance(df0.index, pd.MultiIndex):
    indices = df0.index.get_level_values(0)
    dups    = indices.duplicated()
    lines   = msg.split('\n')
    for i, (line, tag, dup) in enumerate(zip(lines[2:], indices, dups)):
      if dup:
        lines[i+2] = line.replace(tag, ' '*len(tag))
    msg = '\n'.join(lines)

  ## Manually adjust the align if correctly given
  templates = {'l': '{:%i}', 'r':'{:>%i}'}
  header    = msg.split('\n')[0]
  col_marks = [len(token) for token in msg.split('\n')[1].split()]
  if isinstance(aligns, basestring) and len(aligns)==len(col_marks):
    cursor = 0
    acc    = []
    for width, spec in zip(col_marks, aligns):
      cname0 = header[cursor:cursor+width]
      tmp    = templates[spec]%width
      cname  = tmp.format(cname0.strip())
      acc.append(cname)
      cursor += (2+width)
    header = '  '.join(acc)
    msg = '\n'.join([header] + msg.split('\n')[1:])

  ## finally, add the final line
  lwidth = max(len(line) for line in msg.split('\n'))
  msg += '\n'+'-'*lwidth
  return msg

## Bind
pd.DataFrame.to_markdown = df_to_markdown

#===============================================================================

SPEC_NOM = 'r@{\scriptsize$\,\pm\,$}'
SPEC_ERR = '>{\scriptsize}r'

## Helper function
_is_ufloat = lambda x: x.__class__.__name__ == 'AffineScalarFunc'

def _get_spec(need_bicols, nlv, stralign):
  """
  Return the list of spec to be used for each column of latex table.

  nlv        : number of levels of the index.
  need_bicols: List of bools of length equals to columns. 
               True if that column needs a bicol structure.
  stralign   : Default align for non-bicol string
  """
  specs = ['l']*nlv # index are left-aligned
  for need in need_bicols:
    if need:
      specs.extend([SPEC_NOM, SPEC_ERR])
    else:
      specs.append(stralign)
  return specs


def _get_decimals(df):
  """
  Given the dataframe, return an estimate of decimal figures to be used,
  """
  def get_decimal_column(se):
    ## If there is no ufloat at all, return None for free places
    if not any(_is_ufloat(x) for x in se):
      return None
    ## Make estimate
    nmax = max(x.n for x in se if _is_ufloat(x))
    smax = max(x.s for x in se if _is_ufloat(x))
    vmax = ufloat(nmax, smax) # this is the fattest arg
    _, nmag, smag = vmax.rounding_PDG()
    return 2 if nmag <= 1 else 1 # my aesthetic decimal places
  ## Return result for each columns.
  return df.apply(get_decimal_column).tolist()


def to_bicol_latex(df0, decimals=[], stralign='r'):
  ## Init
  df       = df0.copy() # Make a copy
  is_multi = isinstance(df.index, pd.MultiIndex)
  nlv      = len(df.index.levels) if is_multi else 1 # number of levels, important vars

  ## Scan each column and pre-determine first whether bicol structure is needed
  # or not, or simple column is sufficient.
  # It's needed if at least one cell in the column is ufloat
  need_bicols = [any(_is_ufloat(x) for x in se) for _,se in df.iteritems()]

  ## Determine the spec from "need_bicols", no dependency on decimal places
  specs = _get_spec(need_bicols, nlv, stralign)

  ## List of decimal places: Use overriding values if correctly given, or estimate
  if decimals:
    assert len(decimals) == len(need_bicols), "Invalid overriding decimals."
  else:
    decimals = _get_decimals(df)

  ## apply fmt2cols only to the necessary columns
  # also reused the fetched decimal places from above.
  for cname, need, dec in zip(df.columns, need_bicols, decimals):
    if need:
      df[cname] = df[cname].apply(lambda u: fmt2cols(u, dec))

  ## convert to latex
  msg = df.to_latex(escape=False)
  
  ## I don't like the bloating multi-columns rows, collapse them
  if any(df.index.names):
    lines  = msg.split('\n')
    tokens = lines[3].split('&')[:nlv] + lines[2].split('&')[nlv:] 
    del lines[3]
    lines[2] = '&'.join(tokens)
    msg      = '\n'.join(lines)

  ## inject midrule at the outer-index marks
  if is_multi:
    skip  = 4  # number of boilerplate header lines
    marks = [i+skip for i,dup in enumerate(df.index.get_level_values(0).duplicated()) if not dup]
    msg   = inject_midrule(msg, *marks[1:])

  ## make the header line texts wrapped in bicol
  # skip first few as they belong to index
  lines    = msg.split('\n')
  line     = lines[2].rstrip('\\\\') # to be outside fmt2cols
  tokens   = [x.strip() for x in line.split('&')]
  tokens1  = tokens[:nlv]  # indices header
  tokens2  = tokens[nlv:]  # columns header
  tokens   = tokens1+[fmt2cols(tk) if need else tk for (tk,need) in zip(tokens2,need_bicols)]
  lines[2] = ' & '.join(tokens) + '\\\\'
  msg      = '\n'.join(lines)

  ## Inflate the column spec
  if any('{' in spec for spec in specs):
    lines    = msg.split('\n')
    spec0    = re.findall(r'\\begin{tabular}{(.*)}', lines[0])[0]
    spec     = '\n'.join(specs) # from above
    lines[0] = lines[0].replace(spec0, '\n'+spec+'\n')
    msg      = '\n'.join(lines)

  ## finishing touch
  msg = '\\centering\n'+msg
  return msg


## Bind
pd.DataFrame.to_bicol_latex = to_bicol_latex

#===============================================================================

def to_fast_csc(se, tag):
  """
  Special printer to print pd.Series having values from each channel to pretty
  string for fast_csc module, ready for copy & paste

  The uncertainty is by default overridden by the given tag, 
  """
  tem0 = "\nDF['TAG'] = pd.Series({{\n{}}})\n".format
  tem1 = "  {0!r:6}: ufloat( {1.n:9.7f}, {1.s:9.7f}, 'TAG' ),\n".format
  msg  = tem0(''.join(tem1(*x) for x in se[CHANNELS].iteritems()))
  msg  = msg.replace('TAG', tag)
  return msg

## Bind
pd.Series.to_fast_csc = to_fast_csc

#===============================================================================
# DATAFRAME
#===============================================================================

@memorized
def df_from_path(tag, dpath):
  """
  Load multiple dataframe of given tag at the given path.
  This if intended for multiple ditau channels.
  """
  ## careful, don't use splitext as some dirname have '.'
  # If I really want to use splittex, check first if it's dir or file.
  dpath = os.path.abspath(os.path.expandvars(dpath))
  if os.path.isfile(dpath):
    dpath = os.path.splitext(dpath)[0]
  logger.info('[%s] %s'%(tag, collapse_env(dpath)))
  ## if main one exists, return single item
  target = os.path.join(dpath, tag+'.df')
  if os.path.exists(target):
    return pd.read_pickle(target)
  ## If not, search for all dataframes starting with given tag.
  acc = {}
  for fname in os.listdir(dpath):
    # subitem
    if fname.startswith(tag+'_') and fname.endswith('.df'):
      target = os.path.join(dpath, fname)
      suffix = fname.replace(tag+'_', '').replace('.df', '') # extract suffix
      logger.debug(fname)
      acc[suffix] = pd.read_pickle(target)
  return pd.concat(acc)


#===============================================================================

def inject_midrule(df, *irows, **kwargs):
  """
  Suitable for dataframe aimed for markdown: Insert the string '\\midrule'
  in front of the index at given irow.

  Hack: If kwargs[replace] is given, it'll override the text
  """
  TAG = kwargs.get('replace', '\\midrule')

  if isinstance(df, basestring): # already a text paragraph
    lines = df.split('\n')
    for i, irow in enumerate(irows):
      lines.insert(irow+i, TAG)
    return '\n'.join(lines)

  if isinstance(df.index, pd.MultiIndex):
    l = list(df.index)
    for irow in irows:
      l[irow] = (TAG+'{}'+l[irow][0],) + l[irow][1:]
    df.index = pd.MultiIndex.from_tuples(l, names=df.index.names)
    return df
  else:
    iname = df.index.name
    l = list(df.index)
    for irow in irows:
      l[irow] = TAG+'{}'+l[irow]
    df.index = l
    df.index.name = iname
    return df

#===============================================================================
