#!/usr/bin/env python

"""

Decorators useful for analysis

"""
import os
import sys
import functools
import pandas as pd
import ROOT

from PythonCK import logger
from PythonCK.decorators import memorized, optional_arg_decorator
from uncertainties import nominal_value

## Local
import drawers

#===============================================================================

def ignore_string(func):
  """
  Helper decorator to do nothing if string is passed, 
  useful for many formatters.
  """
  @functools.wraps(func)
  def wrap(*args):
    if isinstance(args[0], basestring):
      return args[0]
    return func(*args)
  return wrap

#===============================================================================

def _target(func, args, subdir=True):
  """
  Helper method to consistently define output file name base on function's.
  It return the fullpath to destination file, WITHOUT extension.
  """
  blacklist = [
    'draw_',
    'latex_',
    'main_',
    'export_',
  ]
  # By default, use filename that the given func reside as a dir name.
  # unless subdir=False, then save to same dir as the script
  # dpath = func.func_globals['__file__']
  # dname = os.path.splitext(os.path.basename(dpath))[0]

  ## nondynamic, same snippet as pickle_dataframe_nondynamic
  file    = func.func_globals['__file__']
  dpath   = os.path.dirname(file)
  dname   = os.path.splitext(os.path.basename(file))[0]
  if not subdir:
    dname = os.path.dirname(dname)
  ## Get a clean function name use as file name
  fname = func.__name__
  for s in blacklist:
    fname = fname.replace(s, '')
  ## If args is provided, this is also included into naming scheme
  if args:
    fname = '_'.join((fname,)+args)
  ## finall, return the file path with extension
  return os.path.join(dpath, dname, fname)

#===============================================================================

@optional_arg_decorator
def gpad_save(func, subdir=True):
  """
  helper decorator to save gPad using current function's name.
  """
  @functools.wraps(func)
  def wrap(*args):
    ROOT.gROOT.SetBatch(True)
    func(*args)
    # use filename that the given func reside as a dir name.
    if ROOT.gPad:
      target = _target(func, args, subdir=subdir)+'.pdf'
      dpath  = os.path.split(target)[0]
      if not os.path.exists(dpath):
        logger.info('Directory %s/ not existed, create it.'%dpath)
        os.makedirs(dpath)
      ROOT.gPad.SaveAs(target)
    else:
      logger.warning('No active gPad found (missing ownership?), skip me.')
  return wrap

#===============================================================================

def text_save(func):
  """
  To faciliate saving the snippet of text (latex) to file
  - Choose location as subdir of same name.
  - Support filename by *args
  - Report
  """
  @functools.wraps(func)
  def wrap(*args):
    msg  = func(*args)
    dest = _target(func, args)+'.txt'
    with open(dest, 'w') as fout:
      fout.write(msg)
    print 'Written:', dest
  return wrap

#===============================================================================

def auto_draw(func):
  """
  Another aux function that, upon receiving DataFrame returned from decorated func,
  draw the TH2F from that dataframe. The dimension should be matched.

  Nice to use in conjunction with pickle_dataframe.
  """
  @functools.wraps(func)
  def wrap(*args):
    ## Force to batch mode
    ROOT.gROOT.batch = True
    ## Grab the result
    df0    = func(*args)
    target = _target(func, args)
    if getattr(df0, '_from_pickle', True): # existing pickle, already drawn.
      if os.path.exists(target+'_TH2.pdf') and os.path.exists(target+'_TGraphs.pdf'): # already drawn
        logger.info('Old pickle & drawn existed. Skip.')
        return df0 
    ## Start drawing
    logger.info('New result, start drawing.')
    ## Series -> Dataframe
    if isinstance(df0, pd.DataFrame):
      pass
    elif isinstance(df0, pd.Series):
      df0 = pd.DataFrame(df0)
    else:
      raise ValueError('Need DataFrame/Series')
    df = pd.DataFrame.copy(df0, deep=True) # make a copy
    ## TH2F
    name    = target+'_TH2.pdf'
    c1      = ROOT.TCanvas(name)
    h       = ROOT.TH2F.from_uframe(df)
    h.name  = name
    h.title = name
    h.markerSize = 1.0
    h.Draw('texte colz')
    ROOT.gPad.SaveAs(name)
    ## TGraphs
    name    = target+'_TGraphs.pdf'
    c2      = ROOT.TCanvas(name)
    gr,leg  = drawers.graphs_eff(df)
    gr.name = name
    ROOT.gPad.SaveAs(name)
    return df0
  return wrap

#===============================================================================

def collapse_env(s):
  """
  For logger, allow pretty-short-path hack
  """
  ## For lphe
  s = s.replace('/share/lphe/home', '/home')
  for envname in ['DIR13', 'DIR15']:
    if envname in os.environ:
      path = os.environ[envname]
      if s.startswith(path):
        s = s.replace(path, '$'+envname)
  return s

@optional_arg_decorator
def pickle_dataframe(func, dynamic_location=True):
  """
  Decorator on function that return a dataframe, from usually slow calculation.
  This will pickle the resultant dataframe into given destination,
  and the call to this function, if cache existed, will read pickle and return.

  In other word, it's a caching system specific to dataframe.

  Func support no kwargs for simplicity. (Ordered) args is allowed.

  Usage:
  >> @pickle_dataframe
  >> def some_slow_func():
  ..   return df
  
  Provide flag `--force` to ignore the cache and force rerun the computation.

  """
  logextra = logger.extra(func)

  @functools.wraps(func)
  def wrap(*args):

    ## Prep target
    if dynamic_location:
      ## dpath should be the path to calling script, which may borrow the decorated
      # function from different path. We don't need that
      dpath   = os.getcwd()
      dname   = os.path.split(__main__.__file__)[-1].replace('.py','')
    else:
      ## use filename that the given func resides as a dir name.
      file    = func.func_globals['__file__']
      dpath   = os.path.abspath(os.path.dirname(file))
      dname   = os.path.splitext(os.path.basename(file))[0]
    fname   = '_'.join([func.__name__,]+[str(a) for a in args])
    target  = os.path.join(dpath, dname, fname)+'.df'

    ## Consume --force flag to be non-recursive
    do_force = '--force' in sys.argv
    if do_force:
      sys.argv.remove('--force')
      logger.info('Consume --force: %s'%collapse_env(target))

    ## Actual computation
    if os.path.exists(target) and not do_force:
      # for logger, allow pretty-short-path hack
      logger.info('Loading dataframe: '+ collapse_env(target), extra=logextra)
      df = pd.read_pickle(target)
      ## ducktyping, to be used with auto_draw
      df._from_pickle = True
    else:
      df = func(*args)
      if df is not None:
        logger.info('Pickling dataframe: '+ collapse_env(target), extra=logextra)
        ## make sure the dir exists
        fulldpath = os.path.join(dpath, dname)
        if not os.path.exists(fulldpath):
          os.makedirs(fulldpath)
        ## backup existing
        target0 = target+'0'
        if os.path.exists(target0):
          os.remove(target0)
        if os.path.exists(target):
          os.rename(target, target0)
          logger.info('Backup: %s --> %s'%(target, target0))
        ## Ready, dump
        df.to_pickle(target)
      else:
        logger.info('None. No pickling.')
    if '-v' in sys.argv: # verbose
      print df
    return df
  wrap.func_globals['__file__'] = func.func_globals['__file__']
  return memorized(wrap)

## specialized version
pickle_dataframe_nondynamic = functools.partial(pickle_dataframe, dynamic_location=False)

#===============================================================================

def df_clean_unan(func=None):
  """
  Clean the hard-to-characterize "nan+-nan" ufloat instance in the dataframe.

  Hack to spit out cleaner function when invoke directly.
  """
  def cleaner(u):
    nom = nominal_value(u)
    if nom is None or pd.np.isnan(nom):
      return pd.np.nan
    return u
  # @functools.wraps(func)
  def wrap(*args, **kwargs):
    df = func(*args, **kwargs)
    if isinstance(df, pd.DataFrame): # invoke cleaner only if it's dataframe.
      return df.applymap(cleaner)
    return df
  return cleaner if func is None else (functools.wraps(func))(wrap)
