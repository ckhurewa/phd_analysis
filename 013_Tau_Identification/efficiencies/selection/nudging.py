#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

For Zmumu DPHI correction & systematics.

"""

from math import pi
from uncertainties import ufloat

from PyrootCK import *
from PyrootCK.mathutils import EffU

import trees # tethered
from ditau_utils import pd, CHANNELS, pickle_dataframe_nondynamic, gpad_save


## The scanned threshold such that applying this cut on MC yield the same
# efficiency on data.
NUDGED_DPHI = 2.683616

#===============================================================================

@gpad_save
def draw_zmumu_dphi():

  ## Histogram template
  xmin = 0
  xbin = 50
  xmax = pi * 1.01
  # linear bin
  h = ROOT.TH1F('h','h', xbin, xmin, xmax)
  # varbin histo
  # xarr = list(xmax-pd.np.geomspace(xmax, 1e-3, xbin+1))
  # h = ROOT.TH1F('h','h', xarr)

  ## Shared cut for data & MC
  cut = 'Z02mumu & (APT>0.1) & (ISO1>0.9) & (ISO2>0.9)' # APT to mimic actual selection

  ## Data
  t_data = trees.get_tree_data1()
  t_data.Draw('DPHI >> h', cut, 'goff')
  h_data = h.Clone('h_data')
  h_data.Scale(1./h_data.Integral())
  h.Reset()

  ## MC: before & after
  t_mc = trees.t_zmu()
  t_mc.Draw('DPHI >> h', cut, 'goff')
  h_mc = h.Clone('h_mc')
  h_mc.Scale(1./h_mc.Integral())
  h.Reset()

  ## title
  h_data.title = 'Z#rightarrow#mu#mu Data'
  h_mc.title   = 'Z#rightarrow#mu#mu MC'

  ## Prep canvas
  c = ROOT.TCanvas()
  c.ownership = False
  h_data.ownership   = False
  h_mc.ownership = False

  ## Start draw
  h_data.Draw()
  h_data.xaxis.title = '#Delta#Phi'
  # h_data.maximum = 0.4 # ymax in linear
  # h_data.maximum = 0.2 # ymax in log

  h_mc.Draw('same hist')
  h_mc.lineColor = ROOT.kRed
  h_mc.markerSize = 0

  ## legend
  leg = c.BuildLegend()
  leg.ownership = False
  leg.header = 'LHCb 8TeV'
  leg.x1 = 0.12
  leg.x2 = 0.50
  leg.y1 = 0.69
  leg.y2 = 0.94
  leg.Draw()

  ## Vertical threshold
  ymax = h_data.maximum
  line_data = ROOT.TLine(2.7, 0, 2.7, ymax)
  line_data.ownership = False
  line_data.lineStyle = 2
  line_data.lineColorAlpha = ROOT.kBlack, 0.5
  line_data.Draw()
  #
  line_mc = ROOT.TLine(NUDGED_DPHI, 0, NUDGED_DPHI, ymax)
  line_mc.ownership = False
  line_mc.lineStyle = 2
  line_mc.lineColorAlpha = ROOT.kRed, 0.5
  line_mc.Draw()
  #
  text_thres_data = ROOT.TText(2.7*1.02, 0.12, '2.7')
  text_thres_data.ownership = False
  text_thres_data.textAngle = 270
  text_thres_data.Draw()
  text_thres_MC = ROOT.TText(NUDGED_DPHI*0.95, 0.12, '%.3f'%NUDGED_DPHI)
  text_thres_MC.ownership = False
  text_thres_MC.textAngle = 270
  text_thres_MC.textColor = ROOT.kRed
  text_thres_MC.Draw()

  ## finally
  # c.logy = True 
  c.rightMargin = 0.01
  c.leftMargin  = 0.1
  c.RedrawAxis()
  c.Update()

#===============================================================================

def _esel_dphi(tree, threshold):
  """
  with all ztautau selection in place, except DPHI of course.
  """
  n0 = tree.GetEntries('Sel_nodphi')
  n1 = tree.GetEntries('Sel_nodphi & (DPHI > %f)'%threshold)
  return 1.*n1/n0

@pickle_dataframe_nondynamic
def results():
  """
  Cached processing. Tuned for consistent interface.
  """
  acc = {}
  for dt in CHANNELS:
    tree  = trees.t_ztautau(dt)
    esel0 = _esel_dphi(tree, 2.7)
    esel  = _esel_dphi(tree, NUDGED_DPHI)
    acc[dt] = pd.Series({
      'esel_before': esel0,
      'esel_after' : esel,
      'corr_DPHI'  : esel/esel0,
      'syst_DPHI'  : ((esel-esel0)/esel0) * 100., # in percent
    })
  se = pd.concat(acc)
  se.index.names = 'channel', 'field'
  return se.unstack('channel')[list(CHANNELS)]

#===============================================================================

if __name__ == '__main__':
  ## Apply LHCb style
  ROOT.gROOT.batch = True
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## DEV
  # draw_zmumu_dphi()
  print results().fmt2p
