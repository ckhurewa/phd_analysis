#!/usr/bin/env python

"""

MISC DRAW

"""

from PyrootCK import *
import trees

#===============================================================================

def IP():
  """
  Aim: Show a nice figure that IP of MC is slightly underestimated than data
  """
  t_real = trees.get_tree_data2()
  t_mc   = trees.t_zmu()

  QHist.trees   = t_real, t_mc 
  QHist.filters = [
    'Z02mumu', # strip, trig, pt, eta, mass
    'DPHI > 2.7',
    'DPHI < 3.1',
    'APT  < 0.3',
    'nPVs == 1',
    'mu1_0.50_cc_IT > 0.9',
    'mu2_0.50_cc_IT > 0.9',
  ]

  h = QHist()
  # h.name      = 'BPVIP'
  h.params    = 'mu2_BPVIP'
  h.xmin      = 1e-3
  h.xmax      = 0.3
  h.xbin      = 50
  # h.ymin      = 3e-4
  # h.ymax      = 3e-1
  h.xlog      = True
  # h.ylog      = True
  # h.normalize = True
  h.xlabel    = 'IP [mm]'
  # h.st_code   = 2
  h.draw()

  exit()

#===============================================================================

def DPHI():
  t_mc   = trees.t_zmu()
  t_real = trees.get_tree_data1()

  h = QHist()
  h.trees   = t_real, t_mc 
  h.filters = [
    'Z02mumu', 
    'APT  < 0.05',
    # 'nPVs == 1', # not available
    '_Iso',
  ]
  # h.name    = 'DPHI'
  h.params  = '3.141592653589793 - DPHI'
  h.xmin    = 1e-3
  h.xmax    = 3
  h.xbin    = 30
  # h.ymin    = 1e-3
  # h.ymax    = 0.3
  # h.ylog    = True
  # h.normalize = True
  h.xlog      = True
  h.xlabel    = '#pi - #Delta#phi'
  # h.st_code   = 2
  h.draw()

  exit()

#===============================================================================

def APT():
  t_mc   = trees.t_zmu()
  t_real = trees.get_tree_data1()

  h = QHist()
  h.trees   = t_real, t_mc 
  h.filters = [
    'Z02mumu', 
    # 'APT  < 0.3',
    # 'nPVs == 1',
    '_Iso',
    'DPHI < 2.9',
    'DPHI > 2.2',
  ]
  h.name      = 'APT'
  h.params    = 'APT'
  h.xmin      = 0
  h.xmax      = 0.7
  h.xbin      = 30
  h.ymin      = 1e-3
  # h.ymax      = 0.3
  # h.ylog      = True
  h.normalize = True
  h.xlabel    = 'A_{PT}'
  h.st_code   = 2
  h.draw()

#===============================================================================

def ISO():
  ## Grab the tree, guaranteed to be from mumu
  t_zmu  = trees.t_zmu()
  t_ztau = trees.t_ztau()
  t_real = trees.get_tree_data2()

  ## The systematics
  ## Zmumu (MC) / Ztaumutaumu (MC)
  ## Show that Z->mumu ~ Z->taumutaumu (has the same topology), and use for syst
  # Using both from MC. 
  h = QHist()
  h.name    = 'ISO_syst'
  h.filters = 'mu1_0.50_cc_IT > 0.9', 'Presel_truechan', 'DPHI < 2.9'
  h.trees   = t_zmu, t_ztau
  h.params  = 'mu2_0.50_cc_IT > 0.9: mu2_PT/1e3'
  h.xmin    = 5
  h.xmax    = 40
  h.xbin    = 12
  h.xlog    = True
  h.ymin    = 0.0
  h.ymax    = 1.0
  h.options = 'prof'
  h.xlabel  = 'p_{T}(#mu) [GeV]'
  h.ylabel  = '#hat{I}_{pT}(#mu) > 0.9'
  # h.draw()

  ## The correction
  ## Zmumu (DD) / Zmumu (MC)
  h = QHist()
  h.name    = 'ISO'
  h.filters = [
    # 'DPHI < 3.0',
    'DPHI > 2.7',
    'mu1_BPVIP < 0.01',
    'mu2_BPVIP < 0.01',
    'mu1_0.50_cc_IT > 0.9',
    # 'mu1_0.50_cc_vPT > 0.9',
    'mu2_0.50_cc_vPT < 10e3',
    # 'APT > 0.1',
    'APT < 0.5',
  ]
  h.trees   = t_real, t_zmu
  h.params  = 'mu2_0.50_cc_IT > 0.9: mu2_PT/1e3'
  h.xbin    = 10
  h.xmin    = 5
  h.xmax    = 60
  # h.xlog    = True
  h.ymin    = 0.4
  h.ymax    = 1.0
  h.options = 'prof'
  h.xlabel  = 'p_{T}(#mu) [GeV]'
  h.ylabel  = '#hat{I}_{pT}(#mu) > 0.9'
  h.st_code = 2
  h.draw()

  # ## As a func of event's vars
  # h = QHist()
  # h.name    = 'ISO_ntracks'
  # h.filters = [
  #   # 'DPHI < 3.0',
  #   'DPHI > 2.7',
  #   'mu1_BPVIP < 0.01',
  #   'mu2_BPVIP < 0.01',
  #   'mu1_0.50_cc_IT > 0.9',
  #   # 'mu1_0.50_cc_vPT > 0.9',
  #   'mu2_0.50_cc_vPT < 10e3',
  #   # 'APT > 0.1',
  #   'APT < 0.5',
  # ]
  # h.trees   = t_real, t_zmu
  # h.params  = 'mu2_0.50_cc_IT > 0.9: nTracks'
  # h.options = 'prof'
  # h.draw()


# #===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.SetBatch(True)

  # IP()
  # DPHI()
  # APT()
  ISO()

  # exit()
