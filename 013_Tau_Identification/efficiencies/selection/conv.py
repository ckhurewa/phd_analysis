#!/usr/bin/env python

from PyrootCK import *
import pandas as pd
pd.options.display.width = 320 

## local handle
import trees

#===============================================================================

# TH2* deriveTransformationMatrix( const TH1& h1in, const TH1& h1out)
def derive_transformation_matrix(h1in, h1out):
  """
  Author: Wouter H.
  Builds a monotonic function that transforms h1in into h1out,
  where h1in and h1out are histograms.  The function is
  represented by a matrix M such that for the histograms
  
      h1out[ibin] = sum_j M_ij h1in[jbin]
  
  where the indices i runs from 1 to N, where N is equal to the
  number of bins plus 2. (Underflow and overflow bin are included.)
  The coefficients M_ij are between 0 and 1 and normalized such that
  the integrals of h1out and h1in are identical.  The matrix is
  encoded in a TH2D, such that we can also draw it.
  """

  ## step 1. normalize the histograms, just in case
  N = h1in.nbinsX+2
  sumin      = h1in.Integral()
  sumout     = h1out.Integral()
  h1inarray  = [h1in.GetBinContent(ibin)/sumin   for ibin in xrange(N)]
  h1outarray = [h1out.GetBinContent(ibin)/sumout for ibin in xrange(N)]
  
  ## Init the 2D unfolding matrix  
  # allowing the non-uniform bin
  M = ROOT.TH2D("transformation", "",
                h1in.nbinsX, h1in.xaxis.bins,
                h1in.nbinsX, h1in.xaxis.bins)

  ## we basically do two loops in paralel to match the integrals
  sumout = sumin = 0
  ibin   = jbin  = 0
  fleft  = 1.

  while (ibin<N and jbin<N):
    c = h1outarray[ibin]
    d = h1inarray[jbin]
    sumout += c
    while ((sumin + d*fleft <= sumout) and (jbin < N)):
      sumin += fleft*d
      M.SetBinContent( M.GetBin(ibin,jbin), fleft )
      M.SetBinError  ( M.GetBin(ibin,jbin), 0) # FIXME
      fleft = 1
      jbin += 1
      if jbin < N:  
        d = h1inarray[jbin]
    #
    frac = (sumout - sumin) / d
    M.SetBinContent( M.GetBin(ibin,jbin), frac );
    M.SetBinError  ( M.GetBin(ibin,jbin), 0) # FIXME
    sumin += frac*d
    fleft = 1-frac
    ibin += 1

  return M

#-------------------------------------------------------------------------------

# TH1* transform( const TH1& h1in, const TH2& M)
def transform(h1in, M):
  """
  transform the histogram h1in such that
  
    h1out[ibin] = sum_j M_ij h1in[jbin]
  
  where M_ij is the folding matrix encoded in the histogram M. 
  """

  N = h1in.nbinsX+2
  h1out = h1in.Clone(h1in.name + "transformed")
  for ibin in xrange(N):
    vsum = err2 = 0.
    for jbin in xrange(N):
      Mbin = M.GetBin(ibin,jbin)
      w    = M.GetBinContent(Mbin)
      x    = h1in.GetBinContent(jbin)
      vsum += w * x
      werr = M.GetBinError(Mbin)
      xerr = h1in.GetBinError(jbin)
      err2 += w*w*xerr*xerr + werr*werr*x*x
    #
    h1out.SetBinContent(ibin, vsum)
    h1out.SetBinError(ibin, err2**0.5)
  return h1out


#===============================================================================

def main():
  ## Distribution of sample (Ztautau) from MC, to be convoluted.
  dt = 'h1mu'
  h_sample_mc = ROOT.TH1F('h', 'h', 50, pd.np.geomspace(1e-3, 10, 50+1))
  t_sample_mc = trees.t_ztautau(dt)
  t_sample_mc.Draw('pi_BPVIP >> h', '', 'goff')
  
  ## Control sample (Zmumu) from data & MC
  fin = ROOT.TFile('weights.root')
  h_control_dd = fin['IP_geo50']['IP_geo50'].hists[0]
  h_control_mc = fin['IP_geo50']['IP_geo50'].hists[1]

  ## Apply transformation
  M = derive_transformation_matrix(h1in=h_control_mc, h1out=h_control_dd)
  c1 = ROOT.TCanvas('matrix')
  M.Draw('colz')
  c1.logx = True
  c1.logy = True
  c1.Update()

  # print M.dataframe().applymap('{:.2f}'.format)

  hout = transform(h_sample_mc, M)

  ## scale before drawing
  hout.Scale(1./hout.Integral())
  h_sample_mc.Scale(1./h_sample_mc.Integral())

  c2 = ROOT.TCanvas('after_convoluted')
  hout.Draw()
  h_sample_mc.Draw('same')
  hout.markerColor = ROOT.kRed
  hout.lineColor = ROOT.kRed
  c2.logx = True
  c2.Update()

  exit()



if __name__ == '__main__':
  main()