#!/usr/bin/env python

from PyrootCK import *

#===============================================================================

def _draw_weightfit(h_real, h_mc):
  ## Enforce LHCb style
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Titles
  h_real.ownership = False
  h_mc.ownership = False
  h_real.title = 'Z#rightarrow#mu#mu (Data)'
  h_mc.title = 'Z#rightarrow#mu#mu (MC)'

  ## Start drawing
  c = ROOT.TCanvas()
  c.ownership = False
  c.Draw()

  ## Decorate
  h_real.markerSize     = 1.0
  h_real.markerStyle    = 20
  h_real.fillColorAlpha = 1, 0.
  h_mc.fillStyle        = 3254
  h_mc.fillColorAlpha   = ROOT.kRed, 0.8

  ## Use the new RatioPlot
  rp = ROOT.TRatioPlot(h_real, h_mc)
  rp.ownership = False
  rp.separationMargin = 0.01
  rp.h1DrawOpt = 'PE'
  rp.h2DrawOpt = 'hist'
  rp.Draw()

  ## Legends
  leg = rp.upperPad.BuildLegend()
  leg.ConvertNDCtoPad()
  leg.header    = 'LHCb preliminary'
  leg.fillStyle = 0
  # leg.fillStyle = 4000 # interfering

  ## Workaround for splitFraction (not working)
  frac = 0.5
  rp.upperPad.pad = 1., frac, 0., 1.-frac
  rp.lowerPad.pad = 0., 0., 1., frac

  ## Axis
  rp.upperRefYaxis.labelFont = 133
  rp.upperRefYaxis.labelSize = 24
  rp.lowerRefYaxis.title = 'ratio'
  rp.lowerRefYaxis.titleSize = 0.06
  rp.lowerRefYaxis.titleOffset = 0.85
  rp.lowerRefYaxis.labelSize = 0.05
  rp.lowerPad.logx = True

  ## Fine-tune margin
  rp.upTopMargin = 0.01
  rp.lowBottomMargin = 0.3
  rp.rightMargin = 0.01

  ## finally
  return c, rp, leg


def draw_weightfit_DPHI():
  """
  Note, don't use fit in DPHI because 
  (1) non-physical (DPHI is non-linear quantity, 
  (2) range is well-compat with Z->tautau
  """
  ## Get
  src    = 'DPHI_anti20'
  fin    = ROOT.TFile('weights.root')
  c2     = fin.Get(src).FindObject(src).hists
  h_real = c2.FindObject('h_real')
  h_mc   = c2.FindObject('h_mc')

  ## Discard first bin (next-to-zero) to be able to draw xlog
  hmin = h_real.GetBinLowEdge(2)
  h_real.xaxis.rangeUser = hmin, 4.
  h_mc.xaxis.rangeUser = hmin, 4.

  ## Actual draw
  h_real.xaxis.title = '#pi - #Delta#phi'
  c, rp, leg = _draw_weightfit(h_real, h_mc)
  leg.x1NDC = 0.12
  leg.x2NDC = 0.55
  leg.y1NDC = 0.60
  leg.y2NDC = 0.95

  ## Show fit
  gr = rp.lowerRefGraph
  func = ROOT.TF1('func', '[0] + [1]*log(x)', 1e-3, 3)
  # gr.Fit(func, 'N', '', 2e-3, 6e-2) # manual statful range
  # gr.Fit(func, 'N', '', 6e-3, 3e-2) # manual statful range
  gr.Fit(func, 'N') # all range
  func.lineColor = ROOT.kMagenta
  func.lineWidth = 3
  func.markerSize = 0.
  rp.lowerPad.cd()
  func.Draw('same')

  ## fit params
  text = ROOT.TPaveText(0.1, 0.38, 0.6, 0.50, 'NDC')
  text.textAlign = 13
  text.fillColor = 4000
  text.AddText('f(x) = {p[0]:.4f} {p[1]:+.4f} * log(x)'.format(p=func.parameters).replace('-', '- '))
  text.Draw('NB')

  ## Finally
  c.SaveAs('reweight/DPHI.pdf')


def draw_weightfit_IP():
  ## Get
  src    = 'IP_geo50'
  fin    = ROOT.TFile('weights.root')
  c2     = fin.Get(src).FindObject(src).hists
  h_real = c2.FindObject('h_real')
  h_mc   = c2.FindObject('h_mc')

  ## xmax ~ 0.5
  h_mc.xaxis.rangeUser = h_real.xaxis.rangeUser = 1e-3, 0.3

  ## Actual draw
  h_real.xaxis.title = 'IP [mm]'
  c, rp, leg = _draw_weightfit(h_real, h_mc)
  leg.x1NDC = 0.65
  leg.x2NDC = 1.20
  leg.y1NDC = 0.60
  leg.y2NDC = 0.95

  ## Show fit
  gr = rp.lowerRefGraph
  func = ROOT.TF1('func', '[0] + [1]*log(x)', 1e-3, 1e0)
  gr.Fit(func, 'N', '', 2e-3, 6e-2) # manual statful range
  # gr.Fit(func, 'N', '', 6e-3, 3e-2) # manual statful range
  # gr.Fit(func, 'N') # all range
  func.lineColor = ROOT.kMagenta
  func.lineWidth = 3
  func.markerSize = 0.
  rp.lowerPad.cd()
  func.Draw('same')

  ## fit params
  text = ROOT.TPaveText(0.1, 0.38, 0.6, 0.50, 'NDC')
  text.textAlign = 13
  text.fillColor = 4000
  text.AddText('f(x) = {p[0]:.4f} + {p[1]:.4f} * log(x)'.format(p=func.parameters))
  text.Draw('NB')

  ## Finally
  c.SaveAs('reweight/IP.pdf')

#===============================================================================

if __name__ == '__main__':

  ## Drawer
  # draw_weightfit_IP()
  draw_weightfit_DPHI()
