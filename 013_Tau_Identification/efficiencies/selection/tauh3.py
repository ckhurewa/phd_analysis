#!/usr/bin/env python

import pandas as pd

def results():
  """
  Return the tauh3 additional systematics, 
  calculated by Aurelio.
  """
  corr = pd.Series({
    'syst_VCHI2PDOF': 0.5,
    'syst_DRoPT'    : 1.5,
    'syst_BPVCORRM' : 0.7,
    'syst_BPVLTIME' : 2.7,
  })
  return pd.DataFrame({'h3mu': corr, 'eh3': corr})

if __name__ == '__main__':
  print results()
