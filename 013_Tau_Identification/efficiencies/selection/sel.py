#!/usr/bin/env python

from PyrootCK import * 
from PyrootCK.mathutils import EffU, sumq, covariance_matrix
from uncertainties import nominal_value, correlated_values, correlation_matrix
from uncertainties.unumpy import matrix

## Load local tools
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import pd, CHANNELS, pickle_dataframe_nondynamic, memorized, df_from_path

## Profile of uncertainties correlation 
# only in mumu, h1mu, ee, eh1
CORR_IP2 = [
  [1, 1, 0, 1, 1, 0, 0],
  [1, 1, 0, 1, 1, 0, 0],
  [0, 0, 1, 0, 0, 0, 0],
  [1, 1, 0, 1, 1, 0, 0],
  [1, 1, 0, 1, 1, 0, 0],
  [0, 0, 0, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 1],
]

## only in mumu, ee, emu
CORR_APT = [
  [1, 0, 0, 1, 0, 0, 1],
  [0, 1, 0, 0, 0, 0, 0],
  [0, 0, 1, 0, 0, 0, 0],
  [1, 0, 0, 1, 0, 0, 1],
  [0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0, 1, 0],
  [1, 0, 0, 1, 0, 0, 1],
]

## For h3mu, eh3 variables
CORR_TAUH3 = [
  [1, 0, 0, 0, 0, 0, 0],
  [0, 1, 0, 0, 0, 0, 0],
  [0, 0, 1, 0, 0, 1, 0],
  [0, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 1],
]

## mumu, ee cut is based on the same method. Consider the uncer correlated
CORR_LL_MASS = [
  [1, 0, 0, 1, 0, 0, 0],
  [0, 1, 0, 0, 0, 0, 0],
  [0, 0, 1, 0, 0, 0, 0],
  [1, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0, 1],
]

#===============================================================================
# FINAL RESULTS
#===============================================================================

@memorized
def get_ztau_seleff():
  """
  Helper to load the full contextual result.
  """
  path = '$DIR13/identification/plot/bkg_context'
  df = df_from_path('check_ztau_seleff', path)
  df.index.names = 'tt', 'tree', 'field'
  df.columns.name = 'count'
  return df


@memorized
def results_uncorrected_raw():
  """
  The raw version with components. Useful for latex & cross-check.
  Contains 3 kinds of trees, 7 channels, and all cut levels
  """
  def exe(df):
    ## prep
    df = df.xs(df.name, level='tree')
    deno = df['presel']
    eff  = df.apply(lambda se: se/deno).T
    ## add back for assertion
    eff.loc['npresel']  = df['presel']
    eff.loc['nsel_wmw'] = df['sel_wmw']
    ## change meaning to IP
    eff.loc['displ', 'h3mu'] = pd.np.nan
    eff.loc['displ', 'eh3']  = pd.np.nan
    eff.loc['displ', 'emu']  = pd.np.nan
    eff = eff.rename({'displ':'IP'})
    ## discard
    eff.loc['apt', 'h1mu'] = pd.np.nan
    eff.loc['apt', 'h3mu'] = pd.np.nan
    eff.loc['apt', 'eh1']  = pd.np.nan
    eff.loc['apt', 'eh3']  = pd.np.nan
    ## statistical uncer, in percent
    eff.loc['syst_stat'] = df.apply(lambda se: EffU(int(se.presel), int(se.sel_wmw)).rerr * 100., axis=1)
    ## finally
    return eff

  ## Fetch, aggregate, sort, return
  df0 = get_ztau_seleff()['evt'].unstack('field')
  eff = df0.groupby(level='tree').apply(exe)
  return eff[list(CHANNELS)]


def check_mult_bug():
  """
  Report the (uncorrected) esel after fixing the bug of mult-cand-per-event
  """
  df0 = get_ztau_seleff().unstack('field').stack('count')
  df  = df0.apply(lambda se: se.sel_wmw/se.presel, axis=1)
  return df.unstack('count')

#-------------------------------------------------------------------------------

@memorized
def results_raw():
  """
  Just combine all things together, no important processing
  """
  ## APT
  # There's a good agreement for muon APT, so only syst is need.
  # The final pd.Series should be prep from the module below.
  # NOTE: The result is cached manually.
  import direct_effdiv
  # print direct_effdiv.results()

  ## ISO
  # It required the PT-distribution of the Z->tautau candiate after selection
  import isolation
  # print isolation.results()

  # ## DPHI & IP2
  # # The weight is usually based on Zmumu DD/MC, so it usually won't change.
  # # The distribution of Z->tautau candidate during Sel/Presel will changes when
  # # the selection is modified, as well as the schema of variable affected,
  # # so this will always need a re-run. (SLOW & CACHE)
  # import reweight
  # print reweight.results()

  # ## Some assertion (uncorrected esel from 2 methods should be the same).
  # df1 = reweight.results().T
  # df2 = results_uncorrected_raw().T
  # if not df1.npresel.equals(df2.t_ztautau.npresel):
  #   print df1.npresel
  #   print df2.npresel
  #   assert False, 'Mismatched nPresel.'
  # if not df1.nsel.equals(df2.t_ztautau.nsel_wmw):
  #   print df1.nsel
  #   print df2.nsel_wmw
  #   assert False, 'Mismached nSelection.'

  ## Smearing: IP2
  # Apply the additional resolution from VELO performance paper into MC IP dist.
  import smearing
  # print smearing.results()

  ## Scaling: DPHI
  # Apply an linear transformation to MC distribution to match data
  # import scaling # to be replaced by nudging
  # print scaling.results()

  # ## Nudging: DPHI
  # # nudge the selection threshold around certain value
  # import nudging

  ## DPHI suggested by Roger
  # Use simple correction factor from Zmumu Data/MC, apply independent of ztau channel.
  import dphi_roger

  ## Mass bary-center
  # Use the shift in bary-centre between obs/exp to nudge the selection threshold
  import masscut

  ## Tauh3 exclusive variables
  # Calculated by Aurelio, I only have the final results
  import tauh3

  ## Combine together
  df = pd.concat([
    # results_uncorrected_raw().loc['t_ztautau'].loc[['npresel']],
    # reweight.results(),
    #
    direct_effdiv.results(),
    isolation.results().loc[['corr_ISO', 'syst_ISO1', 'syst_ISO2']],
    smearing.results(),
    dphi_roger.results(),
    masscut.results(),
    tauh3.results(),
  ])[CHANNELS]

  ## Collect the total correction factor. Be careful not to double-count it
  # corr_rew is total correction factor from reweighting technique, 
  # not necessary the same as product of correction from each var
  # df.loc['corr_total'] = (df.loc['corr_rew'] * df.loc['corr_ISO']).apply(nominal_value)
  df.loc['corr_total'] = df.filter(regex='corr_', axis=0).apply(lambda se: se.fillna(1.).prod())
  df.loc['corr_ISO1']  = isolation.results().loc['corr_ISO1'] 
  df.loc['corr_ISO2']  = isolation.results().loc['corr_ISO2'] 

  ## Identify the before & after
  res_ztau_raw      = results_uncorrected_raw().loc['t_ztautau']
  df.loc['npresel'] = res_ztau_raw.loc['npresel' ]
  df.loc['nsel']    = res_ztau_raw.loc['nsel_wmw']
  df.loc['before']  = res_ztau_raw.loc['sel_wmw' ]
  df.loc['after']   = df.loc['before'] * df.loc['corr_total']

  ## Show syst_stat explicitly
  df.loc['syst_stat'] = res_ztau_raw.loc['syst_stat']

  ## Recompute syst_total, from all rows that has index starts with "syst_"
  df.loc['syst_tot'] = df.filter(regex='syst_', axis=0).apply(lambda se: sumq(se.fillna(0.).values))

  ## finally
  return df

#-------------------------------------------------------------------------------

@memorized
def results():
  """
  Collapse the raw results into final usable selection efficiency with 
  systematic uncertainty correlation accounted for.
  """
  ## Grab it, throwaway unused, transpose channels away
  df = results_raw().filter(regex='syst.*|after|before', axis=0).T
  df = df.fillna(0.) # fill missing systimatics rerr to zero.

  ## Repact for the correct correlation
  N = 7
  O = pd.np.ones((N,N))  # square of ones , for completely correlated values
  I = pd.np.identity(N)  # identity matrix, for completely uncorrelated values

  # get absolute errors from percentage relative error,
  # pack into covariance matrix
  covar = sum([
    covariance_matrix(df.after, df.after * df.syst_IP2       / 100, CORR_IP2),
    covariance_matrix(df.after, df.after * df.syst_APT       / 100, CORR_APT),
    covariance_matrix(df.after, df.after * df.syst_BPVCORRM  / 100, CORR_TAUH3),
    covariance_matrix(df.after, df.after * df.syst_BPVLTIME  / 100, CORR_TAUH3),
    covariance_matrix(df.after, df.after * df.syst_DRoPT     / 100, CORR_TAUH3),
    covariance_matrix(df.after, df.after * df.syst_VCHI2PDOF / 100, CORR_TAUH3),
    covariance_matrix(df.after, df.after * df.syst_MASS      / 100, CORR_LL_MASS),
    covariance_matrix(df.after, df.after * df.syst_DPHI      / 100, O),
    covariance_matrix(df.after, df.after * df.syst_ISO1      / 100, O),
    covariance_matrix(df.after, df.after * df.syst_ISO2      / 100, O),
    covariance_matrix(df.after, df.after * df.syst_stat      / 100, I),
  ])

  ## Ready for the final value, no separation, just correlation
  df['after'] = correlated_values(list(df.after.values), covar.tolist())

  ## Repack before (no correction), and add the statistical uncer.
  df['before'] = df.apply(lambda se: ufloat(se.before, se.before*se.syst_stat/100), axis=1)

  ## Finally
  return df.T


#===============================================================================
# EXPORTERS
#===============================================================================

def to_fast_csc():
  return results().loc['after'][CHANNELS].to_fast_csc('sel')

#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def export_corr_matrix():
  """
  Export correlation matrix, which used in the computation in comb.
  """
  df   = results()
  corr = pd.DataFrame(correlation_matrix(df.loc['after']))
  corr.index   = CHANNELS
  corr.columns = CHANNELS
  return corr

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print export_corr_matrix()
    print to_fast_csc()
    sys.exit()

  ## Main computation
  # print get_ztau_seleff()
  # print results_uncorrected_raw()
  # print results_uncorrected_raw().loc['t_ztautau','dphi'].fmt1p
  # print check_mult_bug().fmt2p
  # print results_raw()
  # print results().loc['syst_tot']
  # print results().loc['after'].fmt1p
  # print to_fast_csc()
  # print export_corr_matrix()

  print results_raw()
