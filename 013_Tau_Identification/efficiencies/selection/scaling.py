#!/usr/bin/env python
"""

This script is used to attempt to answer Roger on how the DPHI calibration 
should be handle, by applying linear transformation on MC to make it matches data.

"""

from math import pi
from uncertainties import ufloat

from PyrootCK import *
from PyrootCK.mathutils import EffU

import trees # tethered
from ditau_utils import pd, CHANNELS, pickle_dataframe_nondynamic, gpad_save

## resultant scale scafor, from `find_dphi_scale`
# SCALE_FACTOR = 0.912997882223 # diff^2
SCALE_FACTOR = 0.917725905365 # diff^2/data

#===============================================================================

@pickle_dataframe_nondynamic
def scan_dphi_scale():
  """
  Simply run the scan over range of scale factor, keep the result.
  """
  ## slice the dataframe: data & mc
  cut   = 'Z02mumu & (APT < 0.2)'
  tree  = trees.get_tree_data1()
  tree.Draw('DPHI: APT', cut, 'goff para')
  df_data = tree.sliced_dataframe()

  ## MC tree
  tree  = trees.t_zmu()
  param = 'DPHI: mu1_PHI: mu2_PHI'
  tree.Draw(param, cut, 'goff para')
  df_mc = tree.sliced_dataframe()

  ## define the histogram, for chisquare computation
  from math import pi
  # xmax = pi * 3.15???
  xbin = 50
  ## anti
  xarr = list(pd.np.geomspace(1e-3, xmax, xbin+1))
  xarr[0] = 0 # force start at 0
  ## linear
  # xarr = pd.np.linspace(0., xmax, xbin+1)

  # ## Apply additional smearing on the MC
  # Arr = lambda: Normal(size=len(df_mc.index))

  ## round dphi
  rounder1 = lambda x: x+2*pi if x < 0 else x
  rounder2 = lambda x: 2*pi-x if x > pi else x

  # ## binning: MC before
  # df   = pi - df_mc.DPHI
  # g1   = pd.cut(df, xarr)
  # h_before = df.groupby([g1]).count()
  # h_before /= h_before.sum()
  # # print h_before

  ## binning: MC data
  df = pi - df_data.DPHI
  g1 = pd.cut(df, xarr)
  h_data = df.groupby([g1]).count()
  h_data /= h_data.sum()

  acc = {}
  for mult in pd.np.linspace(0.8, 1.0, 200+1):
    ## scale factor
    df = (pi - df_mc.DPHI)*mult
    g1 = pd.cut(df, xarr)
    h_after = df.groupby([g1]).count()
    h_after /= h_after.sum()
    ## calculate chi2 ratio
    # chisq_ratio = ((h_data-h_after)**2).sum()/((h_data-h_before)**2).sum()
    # print mult, chisq_ratio
    # acc[mult] = chisq_ratio
    chisq = (((h_after-h_data)**2)/h_data).sum()
    print mult, chisq
    acc[mult] = chisq

  return pd.Series(acc)

#-------------------------------------------------------------------------------

@gpad_save
def draw_minz_chisq():
  """
  Use the scanned result to plot for minimized chi2, and fit best scale factor.
  """
  ROOT.TGaxis.SetMaxDigits(1)
  ## Get the scanned result
  se = scan_dphi_scale()

  ## Draw
  c  = ROOT.TCanvas()
  c.ownership = False

  gr = ROOT.TGraph(len(se), se.index, se.values)
  gr.ownership = False
  gr.Draw('AP')
  gr.markerStyle = 2
  gr.markerSize  = 1.
  gr.xaxis.title = 'f_{scale}'
  gr.yaxis.title = '#chi^{2}_{#Delta#phi}'

  ## Fit, fetch result, draw
  gr.Fit('pol2', '', '', 0.88, 0.95)
  fit = gr.GetFunction('pol2')
  fit.lineColor = ROOT.kRed
  p0 = fit.GetParameter(0)
  p1 = fit.GetParameter(1)
  p2 = fit.GetParameter(2)
  print 'scale factor =', -p1/(2*p2)

  ## Finally save the chi2
  c.rightMargin = 0.01
  c.topMargin   = 0.07
  c.leftMargin  = 0.15
  c.Update()

#===============================================================================

@gpad_save
def draw_zmumu_dphi():
  ## Histogram in the 
  h = ROOT.TH1F('h','h', 50, 0, pi*1.01)

  # ## varbin histo
  # xbin = 50
  # xmax = pi * 1.001
  # xarr = list(xmax-pd.np.geomspace(xmax, 1e-3, xbin+1))
  # h = ROOT.TH1F('h','h', xarr)

  ## Shared cut for data & MC
  cut = 'Z02mumu & (APT>0.1) & (ISO1>0.9) & (ISO2>0.9)' # APT to mimic actual selection

  ## Data
  # cut    = 'Z02mumu & (APT < 0.3)'
  t_data = trees.get_tree_data1()
  t_data.Draw('DPHI >> h', cut, 'goff')
  h_data = h.Clone('h_data')
  h_data.Scale(1./h_data.Integral())
  h.Reset()

  ## MC: before & after
  t_mc = trees.t_zmu()
  t_mc.SetAlias('DPHI_', '3.141592653589793 - %f*(3.141592653589793-DPHI)'%SCALE_FACTOR)
  t_mc.Draw('DPHI >> h', cut, 'goff')
  h_before = h.Clone('h_before')
  h_before.Scale(1./h_before.Integral())
  h.Reset()
  t_mc.Draw('DPHI_ >> h', cut, 'goff')
  h_after = h.Clone('h_after')
  h_after.Scale(1./h_after.Integral())
  h.Reset()

  ## title
  h_data.title   = 'Data'
  h_before.title = 'MC'
  # h_before.title = 'MC: before scaling'
  # h_after.title  = 'MC: after scaling'

  ## Prep canvas
  c = ROOT.TCanvas()
  c.ownership = False
  h_data.ownership   = False
  h_before.ownership = False
  h_after.ownership  = False

  ## Start draw
  h_data.Draw()
  # h_data.maximum = 0.4 # ymax in linear
  # h_data.maximum = 0.2 # ymax in log
  h_data.xaxis.title = '#Delta#Phi'

  h_before.Draw('same hist')
  h_before.lineColor = ROOT.kRed
  h_before.markerSize = 0
  
  ## disable for observation
  # h_after.Draw('same hist')
  # h_after.lineColor = ROOT.kGreen
  # h_after.markerSize = 0

  # ## move stat box
  # c.Update()
  # stat = h_data.functions.FindObject('stats')
  # stat.x1NDC = 0.15
  # stat.x2NDC = 0.30

  ## legend
  leg = c.BuildLegend()
  leg.ownership = False
  leg.header = 'Z#rightarrow#mu#mu'
  leg.x1 = 0.12
  leg.x2 = 0.50
  leg.y1 = 0.69
  leg.y2 = 0.94
  leg.Draw()

  ## finally
  # c.logy = True 
  c.rightMargin = 0.01
  c.leftMargin  = 0.1
  c.RedrawAxis()
  c.Update()

  ## Print esel
  def check_esel(tree, threshold=2.7):
    n0 = tree.GetEntries(cut)
    n1 = tree.GetEntries(utils.join(cut, 'DPHI>%f'%threshold))
    return EffU(n0, n1)
  print 'data'     , '{:.6f}'.format(check_esel(t_data))
  print 'MC'       , '{:.6f}'.format(check_esel(t_mc))
  print 'MC2.683610', '{:.6f}'.format(check_esel(t_mc, 2.683610))
  print 'MC2.683612', '{:.6f}'.format(check_esel(t_mc, 2.683612))
  print 'MC2.683614', '{:.6f}'.format(check_esel(t_mc, 2.683614))
  print 'MC2.683616', '{:.6f}'.format(check_esel(t_mc, 2.683616))
  print 'MC2.683618', '{:.6f}'.format(check_esel(t_mc, 2.683618))
  print 'MC2.683620', '{:.6f}'.format(check_esel(t_mc, 2.683620))

#===============================================================================
# APPLY CALIBRATION TO ZTAUTAU
#===============================================================================

def calc_dphi_ztautau_single(dt):
  ## get the tree, make alias, slice for df
  tree = trees.t_ztautau(dt)
  tree.SetAlias('DPHI_', '3.141592653589793 - %f*(3.141592653589793-DPHI)'%SCALE_FACTOR)
  n1,e1 = tree.count_ent_evt('Presel_truechan')
  n2,e2 = tree.count_ent_evt('Presel_truechan & (DPHI > 2.7)')
  n3,e3 = tree.count_ent_evt('Presel_truechan & (DPHI_ > 2.7)')

  esel_before = EffU(e1, e2)
  esel_after  = EffU(e1, e3)
  factor      = esel_after/esel_before
  print '{:<4} {:.1f}% --> {:.1f}%. scale factor of {:.10f}'.format(dt, esel_before*100, esel_after*100, factor.n)
  return factor.n


@pickle_dataframe_nondynamic
def calc_dphi_ztautau():
  ## Obtained scale factor on pi-dphi
  return pd.Series({dt:calc_dphi_ztautau_single(dt) for dt in CHANNELS})


def results():
  corr = calc_dphi_ztautau()
  return pd.DataFrame({
    'corr_DPHI': corr,
    'syst_DPHI': (corr-1.)*100.,
  }).T

#===============================================================================

if __name__ == '__main__':
  ## Apply LHCb style
  ROOT.gROOT.batch = True
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  # print scan_dphi_scale()
  # draw_minz_chisq()
  draw_zmumu_dphi()
  # print calc_dphi_ztautau()
  # print results()
