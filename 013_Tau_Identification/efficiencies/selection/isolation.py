#!/usr/bin/env python

from PyrootCK import *
from PyrootCK.mathutils import EffU

## Load local tools
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import (
  pd, get_current_dtype_dt, df_from_path, CHANNELS,
  pickle_dataframe_nondynamic, sum_fullcorr, nominal_value
)
import trees

#===============================================================================

def prelim():
  """
  - Get esel_iso, obtain from cutting at some same starting point, then add the
    isolation cut, and divide for the changes.
  - The presel cut should select enough Zmumu from data, but not too bias toward
    Zmumu as the iso will be too good.
  """

  from ditau_prep import dt, t_real, t_ztautau, t_dymu
  assert dt == 'mumu'

  filt_zmudd = utils.join(
    'Presel_withz',
    'mu1_PT > 20E3',
    'mu2_PT > 20E3',
    'mu1_PT < 30E3',
    'mu2_PT < 30E3',
    'DPHI   > 2.7',
    'APT    < 0.5',
  )

  filt_ztau = utils.join(
    'Presel_truechan',
    'mu1_PT > 20e3',
    'mu2_PT > 20e3',
    'mu1_PT < 30E3',
    'mu2_PT < 30E3',
    'DPHI   > 2.7',
    'APT    < 0.5',
  )

  filt_zmumc = utils.join(
    'Preselection',
    'mu1_PT > 20E3',
    'mu2_PT > 20E3',
    'mu1_PT < 30E3',
    'mu2_PT < 30E3',
    'DPHI   > 2.7',
    'APT    < 0.5',
  )

  iso_alt = '_Iso'
  # iso_alt = '(mu1_0.50_cc_vPT < 2e3) & (mu2_0.50_cc_vPT < 2e3)'
  # iso_alt = '(mu1_0.50_cc_vPT < 2e3)'

  n0 = t_real.GetEntries(filt_zmudd)
  n1 = t_real.GetEntries(filt_zmudd + '&' + iso_alt)
  rData = EffU(n0, n1)
  print 'DD Zmu:', n1, ' / ', n0, ' = ', rData

  n0 = t_ztautau.GetEntries(filt_ztau)
  n1 = t_ztautau.GetEntries(utils.join(filt_ztau, iso_alt))
  rZtau = EffU(n0, n1)
  print 'MC Ztau:', n1, ' / ', n0, ' = ', rZtau

  n0 = t_dymu.GetEntries(filt_zmumc)
  n1 = t_dymu.GetEntries(filt_zmumc + '&' + iso_alt)
  rZmuMC = EffU(n0, n1)
  print 'MC Zmu:', n1, ' / ', n0, ' = ', rZmuMC

  print 'CORRECTION: DD Zmu / MC Ztau (expect < 1): ', rData/rZtau
  print 'SYST[%]   : MC Zmu / MC Ztau: ', abs(1-(rZmuMC/rZtau).n)*100.


#===============================================================================

def prep_correction():

  ## Get the trees & prep output
  name    = 'ISO'
  fout    = ROOT.TFile('corr_isolation.root', 'RECREATE')
  t_zmu   = trees.t_zmu()
  t_real  = trees.get_tree_data2()
  param   = 'mu2_0.50_cc_IT > 0.9: mu2_PT/1e3'

  ## Spec
  xbin = 8
  xmin = 5
  xmax = 60
  # xarr = pd.np.geomspace(xmin, xmax, xbin+1)
  # xarr = (xmax+xmin) - pd.np.geomspace(xmax, xmin, xbin+1)
  xarr = pd.np.linspace(xmin, xmax, xbin+1)
  h = ROOT.TProfile('htemp', 'htemp', xbin, xarr)
  p = param + '>> htemp'
  c = utils.join(  # Aim for low-PT Z0mumu from data
    'DPHI > 2.7',
    'mu1_BPVIP < 0.1',
    'mu2_BPVIP < 0.1',
    'mu1_0.50_cc_vPT < 5e3',
    'mu2_0.50_cc_vPT < 5e3',
    'APT < 0.2',
    # 'M > 80e3',
    # 'M < 100e3',
  )
  #
  t_real.Draw(p, c, 'goff prof')
  h_real = h.Clone('h_real')
  t_zmu.Draw(p, c, 'goff prof')
  h_mc   = h.Clone('h_mc')

  se_real = h_real.series()
  se_mc   = h_mc.series()
  w = se_real / se_mc.replace(0, 1)
  print pd.DataFrame({
    'Data': se_real,
    'MC'  : se_mc,
    'D/M' : w,
  })

  ## Prep
  h_real.title   = 'data'
  h_mc.title     = 'MC'
  h_mc.lineColor = ROOT.kRed

  ## Stack draw
  st = ROOT.THStack(name, name)
  st.Add(h_real)
  st.Add(h_mc)
  st.Draw('nostack')
  fout.cd()
  ROOT.gPad.SetName(name)
  ROOT.gPad.BuildLegend()
  # ROOT.gPad.Write(name, ROOT.TObject.kOverwrite)

  ## Divide for weight, manually
  title    = 'corr_%s'%name
  h_weight = h_real.ProjectionX(title)
  h_weight.Reset()
  h_weight.title = title
  for i, (_,u) in enumerate(w.iteritems()):
    h_weight.SetBinContent(i+1, u.n)
    h_weight.SetBinError(i+1, u.s)
  h_weight.Draw('texte colz')
  fout.cd()
  # h_weight.Write('', ROOT.TObject.kOverwrite)


  ## Finally
  fout.Close()

#===============================================================================

def prep_syst():
  ## Zmumu (MC) / Ztaumutaumu (MC)
  ## The choice of binning here is used for the systematics, be careful.
  ## Between MC only, allowing low-PT
  h = QHist()
  h.name    = 'iso_mumu_pt'
  h.filters = 'Preselection' #, 'DPHI > 2.7', 'mu1_0.50_cc_IT>0.9'
  h.trees   = t_dymu, t_ztautau
  h.params  = 'mu2_0.50_cc_IT>0.9: mu2_PT/1e3'
  h.xlog    = True
  h.xmin    = 5
  h.xmax    = 40
  h.xbin    = 12
  h.ymin    = 0.0
  h.ymax    = 1.02
  h.options = 'prof'
  h.xlabel  = 'p_{T}(#mu) [GeV]'
  h.ylabel  = '#hat{I}_{pT}(#mu) > 0.9'
  h.draw()


#===============================================================================

@pickle_dataframe_nondynamic
def calc_corr_syst(dt):

  def calc_syst(se):
    """
    Return relative systematics in percent.
    The denominator is midpoint between 2 value
    The numerator is the distance/2 + residual syst from each value
    """
    d = (se.zmu.n + se.ztau.n)/2
    n = abs(se.zmu.n-se.ztau.n)/2 + se.zmu.s + se.ztau.s
    return n/d*100.

  ## Prepare syst as a func of PT
  ## Syst from QHist `draw_isolation` function above
  # fin1 = ROOT.TFile('isolation/isolation.root') # move to drawer
  name = 'iso_mumu_pt'
  cv   = fin1.Get(name)
  df   = pd.DataFrame({
    'zmu' : cv[name+'_hc_00'].series(),
    'ztau': cv[name+'_hc_01'].series(),
  })
  syst = cv[name+'_hc_00'].ProjectionX('syst')
  syst.ownership = False
  syst.Reset()
  for i, x in enumerate(df.apply(calc_syst, axis=1)):
    syst.SetBinContent(i+1, x)

  ## 0. Load & prepare the tree
  from ditau_prep import t_ztautau

  ## 1. Slice the dataframe for PT
  param = ':'.join(['PT1', 'PT2'])
  t_ztautau.Draw(param, 'Selection', 'goff para')
  df = t_ztautau.sliced_dataframe()
  n  = len(df.index)

  ## 2. Vlookup correction & syst
  fin2  = ROOT.TFile('corr_isolation.root')
  hist  = fin2.Get('corr_ISO')
  corr1 = hist.vlookup(df['PT1'])
  corr2 = hist.vlookup(df['PT2'])
  fin1.cd()
  syst1 = syst.vlookup(df['PT1'])
  syst2 = syst.vlookup(df['PT2'])

  ## 3. Return the summary
  return pd.Series({
    'nsel'     : n,
    'corr_ISO1': sum_fullcorr(corr1)/n,
    'corr_ISO2': sum_fullcorr(corr2)/n,
    'syst_ISO1': sum_fullcorr(syst1).n/n,
    'syst_ISO2': sum_fullcorr(syst2).n/n,
    'corr_ISO' : sum_fullcorr(corr1*corr2)/n,
  })

#===============================================================================

def results():
  df = df_from_path('calc_corr_syst', '$DIR13/efficiencies/selection/isolation').unstack().T
  df = df.drop('nsel')
  return df[list(CHANNELS)].applymap(nominal_value)

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print results()
    sys.exit()

  ## 1. Prepare ingredients
  # draw_isolation()
  # prelim()
  # prep_correction()

  # ## 2. Calc correction & systematics per channel
  # dt = get_current_dtype_dt()[1]
  # print calc_corr_syst(dt)
  print results()
