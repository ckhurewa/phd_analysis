#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Implements the correction method for DPHI suggested by Roger.

The Delta phi correction, where the data looks better than the Monte Carlo - 
a somewhat unusual situation.  This could be because the errors on the simulated 
measurements are too big, or  that the Pt of the Z has a wider distribution 
in real life than in the simulation. Which is quite interesting but I don't 
think its relevant here. The Z -> mu mu analysis paper makes no mention of a 
delta phi cut so that's no guide. What the figure in reply170920 is telling us 
is that if you apply a cut at 2.7 (or anywhere else)  to the mu mu sample the 
Monte Carlo will give you an efficiency which is too low (by a small amount).

I don't follow the logic of introducing a cut at 2.684.  The distributions in 
Fig 11 of the ANA note are not that different from each other and from the mu mu data, 
so the contribution to Delta phi from the tau decay , though significant, is not 
the most dominant.  I think you can use the Z to mu mu data to say that the 
efficiency estimated from the MC for a cut at 2.7 is too low by a factor 1-F, 
so you correct the cross section  estimated from the MC for each Z -> tau tau 
channel by a factor 1/(1-F) and attach a systematic uncertainty F to that correction factor.   
This is a minor contribution to the overall systematic uncertainty so there is 
no need to make it complicated.

"""

from PyrootCK import *
from PyrootCK.mathutils import EffU
from qhist import QHist
import trees
from ditau_utils import pd, CHANNELS, LABELS_DITAU, pickle_dataframe_nondynamic

@pickle_dataframe_nondynamic
def results():

  ## Shared cut for data & MC
  cut = 'Z02mumu & (APT>0.1) & (ISO1>0.9) & (ISO2>0.9)' # APT to mimic actual selection

  ## Data
  tree = trees.get_tree_data1()
  n0 = tree.GetEntries(cut)
  n1 = tree.GetEntries(utils.join(cut, 'DPHI > 2.7'))
  eff_data = EffU(n0, n1)
  print eff_data

  tree = trees.t_zmu()
  n0 = tree.GetEntries(cut)
  n1 = tree.GetEntries(utils.join(cut, 'DPHI > 2.7'))
  eff_mc = EffU(n0, n1)
  print eff_mc

  ## MC to data correction
  corr_upscale = (eff_data/eff_mc).n

  ## Relative systematics
  syst_rerr = 1. - 1./corr_upscale

  ## Package into final results
  res = pd.Series({
    'corr_DPHI': corr_upscale,
    'syst_DPHI': syst_rerr * 100., # in percent
  })
  se = pd.concat({dt:res for dt in CHANNELS}) # same corr&syst for all channels
  se.index.names = 'channel', 'field'
  return se.unstack('channel')

#===============================================================================

def draw_dphi_all():
  """
  Draw DPHI from all Ztautau channel together.
  """
  def postproc(h):
    leg = h.anchors.legend
    leg.x1NDC = 0.2
    leg.x2NDC = 0.4
    leg.y1NDC = 0.3
    leg.y2NDC = 0.9

  ## ENV
  ROOT.gBatch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  set_qhist_color_palette()

  ## Prep trees
  all_trees = []
  for dt in CHANNELS:
    tree = trees.t_ztautau(dt)
    tree.title = LABELS_DITAU[dt]
    all_trees.append(tree)

  ## Start drawing
  h = QHist()
  h.name      = 'dphi_all'
  h.filters   = 'Sel_nodphi'
  h.trees     = all_trees
  h.params    = 'DPHI'
  h.xmin      = 0
  h.xmax      = 3.2
  h.xbin      = 64
  h.xlog      = False
  h.ymax      = 0.2
  h_st_code   = 1
  h.st_color  = True
  h.st_fill   = False
  h.st_marker = True
  h.xlabel    = '#Delta#Phi'
  h.postproc  = postproc
  h.draw()

#-------------------------------------------------------------------------------

def draw_dphi_compare():
  """
  Compare DPHI from 
  - Zmumu, data
  - Zmumu, MC
  - Ztautau(ee), MC  (largest syst)
  """
  ## ENV
  ROOT.gBatch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  set_qhist_color_palette()

  ## Shared cut for data & MC
  cut = 'Z02mumu & (APT>0.1) & (ISO1>0.9) & (ISO2>0.9)' # APT to mimic actual selection

  ## 1. Zmumu, data
  t_zmumu_data = trees.get_tree_data1()
  t_zmumu_data.SetAlias('Sel_nodphi', cut)

  ## 2. Zmumu, MC
  t_zmumu_MC = trees.t_zmu()
  t_zmumu_MC.SetAlias('Sel_nodphi', cut)

  ## 3. Ztautau, ee, MC
  dt = 'ee'
  t_ztautau = trees.t_ztautau(dt)
  t_ztautau.title = 'Z#rightarrow '+LABELS_DITAU[dt]

  ## Start drawing
  h = QHist()
  h.name      = 'dphi_compare'
  h.filters   = 'Sel_nodphi'
  h.trees     = t_zmumu_data, t_zmumu_MC, t_ztautau
  h.params    = 'DPHI'
  h.xmin      = 0
  h.xmax      = 3.2
  h.xbin      = 64
  h.xlog      = False
  h.ymax      = 0.2
  h_st_code   = 1
  h.st_color  = True
  h.st_fill   = False
  h.st_marker = True
  h.xlabel    = '#Delta#Phi'
  # h.draw()

  def postproc(h):
    leg = h.anchors.legend
    leg.header = 'LHCb 8TeV'
    leg.x1NDC  = 0.15
    leg.x2NDC  = 0.50
    leg.y1NDC  = 0.70
    leg.y2NDC  = 0.94
    # 
    line_data = h.anchors.line = ROOT.TLine(2.7, 0, 2.7, h.anchors.stack.histogram.maximum)
    line_data.lineStyle = 2
    line_data.lineColorAlpha = ROOT.kBlack, 0.5
    line_data.Draw()
    #
    ROOT.gPad.rightMargin *= 0.2
    ROOT.gPad.topMargin   *= 0.2
    ROOT.gPad.leftMargin  *= 0.7

  ## FOR LATEX
  h = QHist()
  h.name     = 'DPHI'
  h.filters  = 'Sel_nodphi'
  h.trees    = t_zmumu_data, t_zmumu_MC
  h.params   = 'DPHI'
  h.xmin     = 0
  h.xmax     = 3.2
  h.xbin     = 32
  h.xlog     = False
  h.st_code  = 2
  h.xlabel   = '#Delta#Phi'
  h.postproc = postproc
  h.draw()


#===============================================================================

if __name__ == '__main__':
  # print results()
  # draw_dphi_all()
  draw_dphi_compare()
