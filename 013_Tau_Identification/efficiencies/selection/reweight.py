#!/usr/bin/env python

"""

Approach the esel syst by using the reweighing approach

- Get the weight from Z->mumu data/MC, independent of di-tau channel.
- Apply the weight to di-tau candidates, then calc esel.

"""
import math
from PyrootCK import *

## Load local tools
import trees
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import CHANNELS, pd, pickle_dataframe_nondynamic, memorized

#===============================================================================

def wavg_eff(df, index, filt, weight):
  """
  Calculate the weighed average, in case there are possible 
  multiple-candidates-per-event, where each each an index is given.
  Discard the uncertainties away here, for faster mult
  """
  df[weight] = df[weight].apply(uncertainties.nominal_value)
  sumw_psel  = df.groupby(index).mean()[weight].sum()
  sumw_sel   = df[df[filt]==1].groupby(index).mean()[weight].sum()
  return sumw_sel/sumw_psel


#===============================================================================

def prep_weights():
  ## Get the trees & prep output
  fout    = ROOT.TFile('weights.root', 'RECREATE')
  t_mc    = trees.t_zmu()
  t_real1 = trees.get_tree_data1() # unbiased from trigger study
  t_real2 = trees.get_tree_data2() # main data tuple, innate DPHI>2.7

  def prep_weight_single(name, param, xarr, use_DCTW_tree=True, extra_cut=None):
    logger.info('Running: %s'%name)

    ## Swap tree based on the need
    t_real = t_real2 if use_DCTW_tree else t_real1

    ## Bin to histos
    h = ROOT.TH1F('htemp', 'htemp', len(xarr)-1, xarr)
    p = param + '>> htemp'
    c = utils.join('Z02mumu', extra_cut)
    #
    t_real.Draw(p, c, 'goff')
    h_real = h.Clone('h_real')
    t_mc.Draw(p, c, 'goff')
    h_mc   = h.Clone('h_mc')

    ## Prep
    h_real.Sumw2()  # Freeze the syst to Poissonian
    h_mc.Sumw2()
    h_real.Scale(1./h_real.entries)
    h_mc.Scale(1./h_mc.entries)
    h_real.title   = 'data'
    h_mc.title     = 'MC'
    h_mc.lineColor = ROOT.kRed

    ## Stack draw
    st = ROOT.THStack(name, name)
    st.Add(h_real)
    st.Add(h_mc)
    st.Draw('nostack')
    fout.cd()
    ROOT.gPad.SetName(name)
    ROOT.gPad.BuildLegend()
    ROOT.gPad.Write(name, ROOT.TObject.kOverwrite)

    ## Divide for weight
    title    = 'weight_%s'%name
    h_weight = h_real.Clone(title)
    h_weight.title = title
    h_weight.Divide(h_real, h_mc)
    h_weight.Draw('PE')
    fout.cd()
    h_weight.Write('', ROOT.TObject.kOverwrite)

  ## IP
  xmax = 1e1
  cut  = utils.join('APT<0.3', 'DPHI>2.7', 'DPHI<3.1', 'nPVs==1', 'mu1_0.50_cc_IT>0.9', 'mu2_0.50_cc_IT>0.9')
  for xbin in [10, 20, 50, 100]:
    # geo
    xmin = 1e-3
    xarr = pd.np.geomspace(xmin, xmax, xbin+1)
    prep_weight_single('IP_geo%i'%xbin, 'mu2_BPVIP', xarr, extra_cut=cut)

  ## Run over vars
  ## DPHI
  xmax = 3.15
  cut  = 'APT < 0.2'
  for xbin in [10, 20, 50, 100]:
    # linear
    xarr = pd.np.linspace(0., xmax, xbin+1)
    prep_weight_single('DPHI_lin%i'%xbin, 'DPHI', xarr, use_DCTW_tree=False, extra_cut=cut)
    # geo
    xmin = 1e-3
    xarr = (xmax+xmin) - pd.np.geomspace(xmax, xmin, xbin+1)
    prep_weight_single('DPHI_geo%i'%xbin, 'DPHI', xarr, use_DCTW_tree=False, extra_cut=cut)
  ## custom
  xarr = [0, 1, 1.5, 2.0, 2.3, 2.5, 2.6, 2.7, 2.8] + list(pd.np.linspace(2.9, 3.2, 7))
  prep_weight_single('DPHI_custom', 'DPHI', xarr, use_DCTW_tree=False, extra_cut=cut)
  ## Anti
  for xbin in [10, 20, 50, 100]:
    xarr = list(pd.np.geomspace(1e-3, math.pi, xbin+1))
    xarr[0] = 0 # force start at 0
    prep_weight_single('DPHI_anti%i'%xbin, '3.141593 - DPHI', xarr, use_DCTW_tree=False, extra_cut=cut)

  ## APT
  # For reference, not really used
  xmax = 1.
  cut  = 'DPHI > 2.7'
  for xbin in [10, 20, 50, 100]:
    xarr = pd.np.linspace(0, xmax, xbin+1)
    prep_weight_single('APT_lin%i'%xbin, 'APT', xarr, extra_cut=cut)
    xarr = pd.np.geomspace(1e-3, xmax, xbin+1)
    prep_weight_single('APT_geo%i'%xbin, 'APT', xarr, extra_cut=cut)

  ## Mass (for ref)
  xbin = 100
  xmin = 80e3
  xmax = 100e3
  xarr = pd.np.linspace(xmin, xmax, xbin+1)
  prep_weight_single('M', 'M', xarr)

  ## Finally
  fout.Close()


#===============================================================================

## tighter Z0mumu selection
_P0 = ufloat(1.13706e+00, 5.78783e-02)
_P1 = ufloat(2.83923e-02, 1.28926e-02)
def weighter_IP(x):
  """
 FCN=29.8238 FROM MIGRAD    STATUS=CONVERGED      69 CALLS          70 TOTAL
                     EDM=1.29339e-08    STRATEGY= 1      ERROR MATRIX ACCURATE
  EXT PARAMETER                                   STEP         FIRST
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE
   1  p0           1.13706e+00   5.78783e-02   2.22417e-05   1.36435e-02
   2  p1           2.83923e-02   1.28926e-02   4.95439e-06  -6.95889e-02
  """  
  if x <=0: return 0.
  return _P0 + _P1 * math.log(x)

_P0_DPHI = ufloat( 8.71091e-01, 9.72095e-03)
_P1_DPHI = ufloat(-5.77808e-02, 3.67633e-03)
def weighter_DPHI(x):
  """
 FCN=32.4551 FROM MIGRAD    STATUS=CONVERGED      86 CALLS          87 TOTAL
                     EDM=5.10679e-07    STRATEGY= 1      ERROR MATRIX ACCURATE
  EXT PARAMETER                                   STEP         FIRST
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE
   1  p0           8.71091e-01   9.72095e-03   1.51841e-05   1.79672e-01
   2  p1          -5.77808e-02   3.67633e-03   5.74007e-06  -3.14841e-01
  """
  if x <=0: return 0.
  ## CAREFUL, the weight function is in pi-x,
  # but my inputs is x
  return (_P0_DPHI + _P1_DPHI * math.log(math.pi-x))

## List of all weights & their sources
WEIGHT_SRC = {
  'DPHI': weighter_DPHI,
  'IP2' : weighter_IP,
}

#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def calc_esel_reweighted(dt):
  ## 0. Load & prepare the tree
  t_ztautau = trees.t_ztautau(dt)
  nsel      = t_ztautau.count_ent_evt('Selection_wmw')[1]   # lower estimate
  npresel   = t_ztautau.count_ent_evt('Presel_truechan')[1] # keep this number!

  ## 1. Slice the dataframe with bool of Selection
  # note: make sure I have true_chan and mass here.
  # this should agree with the result from context
  param = ':'.join(['Selection_wmw', 'eventNumber'] + WEIGHT_SRC.keys())
  t_ztautau.Draw(param, 'Presel_truechan', 'goff para')
  df = t_ztautau.sliced_dataframe()
  # print df

  ## 2. vloopkup the weight & attach
  weight_vars = {
    'mumu': ['DPHI', 'IP2'],
    'h1mu': ['DPHI', 'IP2'],
    'h3mu': ['DPHI'],
    'ee'  : ['DPHI', 'IP2'],
    'eh1' : ['DPHI', 'IP2'],
    'eh3' : ['DPHI'],
    'emu' : ['DPHI'],
  }[dt]

  fin = ROOT.TFile('weights.root')
  for var, weighter in WEIGHT_SRC.iteritems():
    if var in weight_vars:
      df[var] = df[var].apply(weighter)
      # # hist = fin.Get(src)
      # # if not hist:
      # #   raise ValueError('Missing weight histo: %s'%src)
      # ## start vlookup
      # if var.startswith('IP'):
      #   df[var] = df[var].apply(weighter_IP)
      # elif var.startswith('DPHI'):
      #   df[var] = df[var].apply(weighter_DPHI)
      # else:
      #   raise ValueError
    else:
      df[var] = 1.  # weightless
  fin.Close()
  ## weight product
  df['weight'] = df.apply(lambda se: se[WEIGHT_SRC.keys()].prod(), axis=1)
  # print df

  ## 3. Calc esel at different scenario
  # In order to calculate esel, with weights, and possible mult-candidates
  # per event, I need some kung-fu pandas.
  eff  = lambda weight: wavg_eff(df, index='eventNumber', filt='Selection_wmw', weight=weight)
  res  = pd.Series({
    'npresel' : npresel,
    'nsel'    : nsel,
    'before'  : 1. * nsel / npresel,
    'after'   : eff('weight'),
  })
  for var in weight_vars:
    if var in WEIGHT_SRC:
      res[var] = eff(var)

  ## Finally
  return res

#-------------------------------------------------------------------------------

def calc_esel_reweighted_all():
  """
  Loop calc over all channels, concat for further usage.
  """
  return pd.concat({dt:calc_esel_reweighted(dt) for dt in CHANNELS})

#===============================================================================

@memorized
def results():
  """
  Parsed result ready for integration
  """
  def get_syst(p):
    def syst(se):
      if se[p] is None or pd.np.isnan(se[p]):
        return 0
      # ref = se.before  # relative from before
      # ref = se.after.n # relative from after
      # ref = (se.before + se.after.n)/2. # relative from (mid of before & global after)
      # ref = (se.before + se[p].n)/2. # relative from (mid of before & local after)
      # print abs(ref-se[p].n), ref, abs(ref-se[p].n) / ref * 100. 
      # return abs(ref-se[p].n) / ref * 100. 
      ## delta over mean
      # return abs(se[p].n - se.before) / ((se[p].n + se.before)/2) * 100.
      ## delta over local after
      return abs(se[p] - se.before) / (se[p]) * 100.
    return syst

  ## Fetch the reweighed esel, calc the syst from changes.
  df = calc_esel_reweighted_all().unstack().T
  df.loc['syst_DPHI'] = df.apply(get_syst('DPHI'))
  df.loc['syst_IP2']  = df.apply(get_syst('IP2'))
  df.loc['syst_tot']  = (df.loc['syst_DPHI']**2 + df.loc['syst_IP2']**2)**0.5

  # ## 170323: No need in the fitted-IP batch
  # ## syst of IP mumu [0.05, 0.5], ee [0.1, 0.5] is two-way selection, so it
  # # should not double count things
  # df.loc['syst_IP2', 'ee']   = df.loc['syst_IP2', 'ee']**0.5
  # df.loc['syst_IP2', 'mumu'] = df.loc['syst_IP2', 'mumu']**0.5

  ## Throw away before/after for clarity, keep only ratio (correction)
  df.loc['corr_rew']  = df.loc['after']/df.loc['before']  # all reweights
  df.loc['corr_DPHI'] = df.loc['DPHI'] /df.loc['before']
  df.loc['corr_IP2']  = df.loc['IP2']  /df.loc['before']
  df = df.drop(['before', 'after', 'DPHI', 'IP2'])

  ## Finally
  return df[list(CHANNELS)]

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    calc_esel_reweighted_all()
    sys.exit()

  ## DEV
  # tree = get_tree_ztautau()
  # print tree.GetEntries()
  # print tree.GetEntries('Preselection')
  # print tree.GetEntries('Presel_truechan')
  # print tree.GetEntries('Selection')
  # print tree.GetEntries('Sel_truechan')
  # print tree.GetEntries('Sel_truechan & _Mass')
  # print tree.count_ent_evt('Preselection')
  # print tree.count_ent_evt('Sel_truechan')
  # print tree.count_ent_evt('Sel_truechan & _Mass')

  ## 1: Prepare the weight
  # prep_weights()

  ## 2: Calc esel per-channel
  # print calc_esel_reweighted('emu')
  # print calc_esel_reweighted_all()

  ## 3: Summary of result.
  print results().fmt4f
