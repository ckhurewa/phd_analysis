#!/usr/bin/env python

import pandas as pd
from PyrootCK import *
from PyrootCK.mathutils import EffU, sumq

## Local tool
import trees

#===============================================================================

def calc_correction_effdiv():
  """
  Calculate the fast correction by dividing the eff obtained in Z02mumu between 
  data and MC. For this to work, there should be a good data-MC agreement.
  """
  ## Localize the tree
  t_real = trees.get_tree_data2()
  t_dymu = trees.t_zmu()
  t_ztau = trees.t_ztau() # Only for isolation correction

  ## IP
  # cut = 'mu1_BPVIP > 0.02 & mu1_BPVIP < 0.3'
  # cut = 'mu1_BPVIP < 0.3'
  # cut = 'mu1_BPVIP > 0.5'
  # filt = utils.join(
  #   'Z02mumu',
  #   'nPVs == 1',
  #   'mu1_TOS_MUON',
  #   'mu2_TOS_MUON',
  # )

  ## APT for mumu, ee
  # cut = 'APT < 0.1' # reverse to have more stats
  # cut = 'APT > 0.1' # actual cut
  cut = 'APT < 0.6'  # for emu
  filt = utils.join(
    'Z02mumu', # mass, PT, eta, ISMUON, TRPCHI2>0.01, one-TOS
    'nPVs == 1',
    'mu1_TOS_MUON',
    'mu2_TOS_MUON',
    'DPHI > 3.0',
    # 'DPHI < 3.1',
    '_Iso',
  )

  ## Prep Trees
  t_mc   = t_dymu   # General
  eff_dd = EffU(t_real.GetEntries(filt), t_real.GetEntries(utils.join(filt,cut)))
  eff_mc = EffU(t_mc.GetEntries(filt)  , t_mc.GetEntries  (utils.join(filt,cut)))

  # print cut
  print 'SelSize      ', t_real.GetEntries(utils.join(filt, cut))
  print 'eff_dd       ', eff_dd
  print 'eff_mc       ', eff_mc
  print '|dd-mc|       {:.5f}'.format(abs(eff_dd-eff_mc))
  print 'dd/mc         {:.5f}'.format(eff_dd/eff_mc)
  print 'delta/mean[%] {:.5f}'.format(abs(eff_dd-eff_mc)*100./((eff_dd+eff_mc)/2))
  # print '1-dd/mc[%]    {:.4f}'.format(abs(1.-(eff_dd/eff_mc))*100)

#===============================================================================

def results():
  """
  Return the syst associated to the IP cut, no correction.
  This is calculated from above manually, harsh mode.

  
  OLD   IP1          IP2
  mumu  [0.01, 0.3]  [0.01, 0.3]
  h1mu  [0.01, 0.3]  [0.02, 0.3]
  h3mu  [0.01, 0.3]  -
  ee    [0.01, 0.3]  [0.01, 0.3]
  eh1   [0.01, 0.3]  [0.02, 0.3]
  eh3   [0.01, 0.3]  -
  emu   [0.01, 0.3]  [0.01, 0.3]

  170215 Assisted
  mumu  -            [0.05, 0.5]
  h1mu  -            > 0.03      
  h3mu  -            -
  ee    -            [0.1, 0.5]
  eh1   -            > 0.03
  eh3   -            -
  emu   -            -
  
  170410 With mass window
        IP2
  mumu  > 0.05
  h1mu  > 0.03
  ee    > 0.05
  eh1   > 0.03
  """
  return pd.DataFrame({'syst_APT': pd.Series({
    'mumu': 1.10742,
    'ee'  : 1.10742,
    'emu' : 0.01333,
  })}).T

#===============================================================================

if __name__ == '__main__':
  # calc_correction_effdiv()
  print results()
