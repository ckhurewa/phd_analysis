#!/usr/bin/env python
from PyrootCK import *
from collections import OrderedDict

## local results & tools
import sel
from ditau_utils import pd, DT_TO_LATEX, CHANNELS, inject_midrule, ignore_string

## also ordered canonically
FIELD_TO_LATEX = OrderedDict([
  ('apt'     , '\\apt'),
  ('APT'     , '\\apt'),
  ('mass'    , 'di-tau mass'),
  ('MASS'    , 'di-tau mass'),
  ## Isolation
  ('iso1'    , '\\ihatpt{$_1$}'),
  ('iso2'    , '\\ihatpt{$_2$}'),
  ('ISO1'    , '\\ihatpt{$_1$}'),
  ('ISO2'    , '\\ihatpt{$_2$}'),
  ## Displacement
  ('displ'   , 'IP'),
  ('IP'      , 'IP'),
  ('IP1'     , 'IP$_1$'),
  ('IP2'     , 'IP$_2$'),
  ('DOCACHI2', '\\chisqdoca'),
  ## DPHI
  ('dphi'    , '\\dphi'),
  ('DPHI'    , '\\dphi'),
  ## tauh3
  ('vertex'   , 'Vertex \\chisqndf'),
  ('VCHI2PDOF', 'Vertex \\chisqndf'),
  ('dr'       , '\\dropt'),
  ('DRoPT'    , '\\dropt'),
  ('ltime'    , 'Decay time'),
  ('BPVLTIME' , 'Decay time'),
  ('corrm'    , '\\mcorr'),  
  ('mcorr'    , '\\mcorr'),  
  ('BPVCORRM' , '\\mcorr'),  
  ## Metadata
  ('sel_wmw' , '(All)'),
  ('sel_nomw', '(total nomass)'),
  ('stat'    , 'Statistical'),
  ('tot'     , '\\esel'),  # same as other table.
  ('before'  , 'Before correction'),
  ('after'   , 'After correction'),
])

#===============================================================================

def latex_esel_components():
  """
  Show the esel of each variable in detail.
  """
  ## Fetch the data, sanitize
  eff = sel.results_uncorrected_raw().loc['t_ztautau']
  eff = eff.replace(1., '---').fillna('---').drop('sel_nomw')
  eff.index.name = '\\esel [\%]'
  ## Filter, sort, rename
  idx = [k for k in FIELD_TO_LATEX if k in eff.index]
  eff = eff.loc[idx].rename(index=FIELD_TO_LATEX, columns=DT_TO_LATEX)
  inject_midrule(eff, 2, 6, 10)
  ## Finally, format
  return eff.fmt1p.to_markdown(floatfmt='.1f', stralign='right')

#-------------------------------------------------------------------------------

def latex_esel_compare_detailed():
  """
  Show the changes to esel by each variable in each channels.
  """
  ## Prep
  df0     = sel.results_raw().T
  esel0   = df0['before']

  ## Helper
  TEXT_NA = '---'
  def delta(corr):
    # return the signed absolute changes.
    res = ((corr-1.)*esel0*100.).apply(uncertainties.nominal_value).fillna(TEXT_NA)
    return res.apply(ignore_string('${:+.2f}$'.format))

  ## Collect into specific order
  acc   = pd.DataFrame()
  acc['before'] = sel.results().loc['before'].fmt1lp
  # acc['IP1']    = TEXT_NA
  acc['IP2']    = delta(df0['corr_IP2'])
  acc['ISO1']   = delta(df0['corr_ISO1'])
  acc['ISO2']   = delta(df0['corr_ISO2'])
  acc['DPHI']   = delta(df0['corr_DPHI'])
  # acc['APT']    = TEXT_NA
  acc['after']  = sel.results().loc['after'].fmt1lp

  ## Finally, decorate
  df = acc.loc[list(CHANNELS)].T
  df = df.rename(FIELD_TO_LATEX, columns=DT_TO_LATEX) 
  df.index.name = '\\esel [\%]'
  inject_midrule(df, 1, -1)
  return df.to_markdown(stralign='right')

#-------------------------------------------------------------------------------

def latex_relative_uncertainties():
  """
  Gives relative uncer in specific order.
  """
  ## Sort & filter
  df   = sel.results()
  cols = ['syst_'+idx for idx in FIELD_TO_LATEX if 'syst_'+idx in df.index]
  df   = df.T[cols].T
  ## Sanitize
  df   = df.replace(0, '---').rename(lambda s: s.replace('syst_', ''))
  df   = df.rename(FIELD_TO_LATEX, columns=DT_TO_LATEX)
  df.index.name = '\\cscrerr'
  inject_midrule(df, -1)
  return df.fmt2f.to_markdown()

#===============================================================================

if __name__ == '__main__':
  print latex_esel_components()
  print latex_esel_compare_detailed()
  print latex_relative_uncertainties()
