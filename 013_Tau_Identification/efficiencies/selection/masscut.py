#!/usr/bin/env python

"""

Determine the uncertainty from masscut < 60GeV in mumu,ee channel.

Strategy: 
- Calculate the centre-of-mass between MC, data
- Use the difference from above to nudge the threshold, determine the esel changes. 

"""

from PyrootCK import *
from PyrootCK.mathutils import EffU
from uncertainties import ufloat

## Load local tools
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import pd, memorized, df_from_path, DT_TO_DTYPE
sys.path.append(os.path.expandvars('$DIR13/identification/plot'))

@memorized
def get_selected_trees(dt):
  """
  Split result to (t_real, other trees)
  """
  import selected
  trees = selected.load_selected_trees_comb(dt)
  return trees[0], trees[1:]

def get_full_ztautau_tree(dt):
  import ditau_cuts
  import id_utils
  dtype    = DT_TO_DTYPE[dt]
  tname_os = 'DitauCandTupleWriter/%s'%dtype
  tree     = import_tree(tname_os, 5230)
  id_utils.apply_alias(ditau_cuts, dtype, 't_ztautau', tree)
  return tree

@memorized
def get_nexp():
  import candidates
  return candidates.results_comb_withmasswindow()

@memorized
def get_raw_esel(dt):
  df = df_from_path('check_ztau_seleff', '$DIR13/identification/plot/bkg_context')
  return df['evt'].loc[dt, 't_ztautau']

def calc_COM(tree):
  """
  Given a tree, calculate centre-of-mass and its error,
  locked for mumu,ee channel, with cut mass < 60.
  """
  tree.Draw('mass', 'mass <= 60', 'goff')
  se = tree.sliced_dataframe()['mass']
  return ufloat(se.mean(), se.std())

def main_single(dt):
  t_real, trees = get_selected_trees(dt)
  nexp0 = get_nexp()[dt]

  ## Calculate COM of data
  com_obs = calc_COM(t_real)
  print 'COM observed (data)      : {:.1f} GeV'.format(com_obs)

  ## Calculate weighted CoM for other trees
  w_tot = 0.
  c_tot = 0.
  for tree in trees:
    weight = nexp0[tree.name]
    com    = calc_COM(tree)
    w_tot  += weight
    c_tot  += weight*com 
  com_exp = c_tot/w_tot
  print 'COM expected (signal+bkg): {:.1f} GeV'.format(com_exp)

  ## difference between the two
  nudge = abs(com_exp.n - com_obs.n)

  ## Calculate signal esel at different nudge
  t_ztautau = get_full_ztautau_tree(dt)
  nraws   = get_raw_esel(dt)
  npresel = t_ztautau.count_ent_evt('Preselection')[1]
  nsel0   = t_ztautau.count_ent_evt('Preselection & (M/1e3 <= 60)')[1]
  nsel    = t_ztautau.count_ent_evt('Preselection & (M/1e3 <= 60 - %f)'%nudge)[1]
  assert npresel == nraws['presel'], "%.2f != %.2f"%(npresel, nraws['presel'])
  assert nsel0   == nraws['mass']  , "%.2f != %.2f"%(nsel0  , nraws['mass'])
  esel0 = EffU(nraws['presel'], nsel0)*100.
  esel  = EffU(nraws['presel'], nsel )*100.
  print 'Before correction: {:.1f}'.format(esel0)
  print 'After correction : {:.1f}'.format(esel)
  print 'rerr: ', (1.-esel.n/esel0.n)*100.

def main():
  main_single('mumu')  
  main_single('ee')  

def results():
  """
  Return the final relative uncertainty (in percent) of the mass cut esel.
  """
  return pd.DataFrame({'syst_MASS': pd.Series({
    'mumu': 0.897102572816,
    'ee'  : 1.44423095924,
  })}).T

#===============================================================================

if __name__ == '__main__':
  # main()
  # print get_raw_esel('mumu')
  print results()
