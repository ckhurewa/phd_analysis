#!/usr/bin/env python

from PyrootCK import *
from PyrootCK.mathutils import EffU
sys.path.append('..')
from esel_syst import pd, get_trees


def esel_reweight(param, cut, xmin=0., xmax=1., xbin=200):
  ## Get trees
  _, t_dymu, t_ztautau = get_trees() 
  t_real = get_tree_data()

  ## Draw normalize
  h = QHist()
  h.trees   = t_real, t_dymu
  h.params  = param
  h.filters = 'Z02mumu'
  h.xmin    = xmin
  h.xmax    = xmax
  h.xbin    = xbin
  h.draw()

  ## recalc uncertainty & get weights
  n0 = h[0].entries
  n1 = h[1].entries
  s0 = h[0].series().apply(lambda x: var(x.n*n0)/n0)
  s1 = h[1].series().apply(lambda x: var(x.n*n1)/n1)
  w  = s0/s1.replace(0, 1)

  ## Shape into dataframe
  p = '%s >> h_temp(%i, %f, %f)'%(param, xbin, xmin, xmax)
  t_ztautau.Draw(p, 'Preselection')
  deno = ROOT.gROOT.Get('h_temp').series()
  t_ztautau.Draw(p, 'Preselection & %s'%cut)
  nume = ROOT.gROOT.Get('h_temp').series()
  df   = pd.DataFrame({'nume':nume, 'deno':deno, 'weight':w})

  ## Report before & after
  return pd.Series({
    'before': df.nume.sum()/df.deno.sum(),
    'after' : (df.nume*df.weight).sum() / (df.deno*df.weight).sum(),
  })

#===============================================================================

def correct_var_method():
  _, _, t_ztautau = get_trees() 
  t_ztautau.SetAlias('DPHI_', '3.141592653589793 - 0.908049*(3.141592653589793-DPHI)')
  n0 = t_ztautau.GetEntries('Preselection')
  n1 = t_ztautau.GetEntries('Preselection & (DPHI > 2.7)')
  n2 = t_ztautau.GetEntries('Preselection & (DPHI_ > 2.7)')

  print 'Before:', 1. * var(n1)/var(n0)
  print 'After :', 1. * var(n2)/var(n0)

#===============================================================================

def main():

  print esel_reweight('DPHI', 'DPHI > 2.7', xmin=0, xmax=3.2)

  # correct_var_method()

  # print esel_reweight('APT', 'APT < 0.6')


if __name__ == '__main__':
  main()
