#!/bin/bash

## remove cache
rm -rf reweight/*.df
# rm -rf isolation/*.df # fragile
rm *.df

## recalculate the reweight, isolation, and finally correlation matrix
./reweight.py --recalc
# ./isolation.py --recalc # fragile
./sel.py --recalc
