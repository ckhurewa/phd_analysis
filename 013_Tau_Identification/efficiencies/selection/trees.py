#!/usr/bin/env python

"""

Assist with the list of trees for the esel & syst computation.
Many of those is forced to be form channel mumu

"""

from PythonCK.decorators import memorized
from PyrootCK import *
from qhist import utils

## Tethering to module tool
sys.path.append(os.path.expandvars('$DIR13')) # ditau_utils

## more tools
sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
import id_utils
import ditau_cuts
from id_utils import DT_TO_DTYPE

## Cut for Z->mumu on the Tree from DitauCandTupleWriter
# (both data2 and simulation)
CUT_DCTW = utils.join(
  'StrippingWMuLineDecision',
  #
  'M > 80e3',
  'M < 100e3',
  # VCHI2PDOF not available
  'mu1_PT   > 20e3',
  'mu2_PT   > 20e3',
  'mu1_ETA  > 2',
  'mu2_ETA  > 2',
  'mu1_ETA  < 4.5',
  'mu2_ETA  < 4.5',
  #
  # 'mu1_ISMUON', # applied
  # 'mu2_ISMUON', # applied
  # 'mu1_TRPCHI2 > 0.01', # applied
  # 'mu2_TRPCHI2 > 0.01', # applied
  # 'mu1_0.50_cc_IT > 0.9', # part of var under study
  # 'mu2_0.50_cc_IT > 0.9',
  'mu1_BPVIP > 0.',
  'mu2_BPVIP > 0.',
)

@memorized
def get_tree_data1():
  """
  Load the Z->mumu from data (same one as trigger eff)

  No PV-refit result, so not viable for IP comparison.
  """

  ## 160706 Fix missing ETA
  JIDS = 5038, 5039, 5040, 5041, 5042, 5043

  cut_filt = utils.join(
    'Z0_M       > 80e3',
    'Z0_M       < 100e3',
    # 'Z0_DPHI12  > 2.7',   # part of the var under study
    # 'Z0_ENDVERTEX_CHI2 < 4',
    #
    'muminus_PT  > 20e3',
    'muplus_PT   > 20e3',
    'muminus_ETA > 2.0',
    'muplus_ETA  > 2.0',
    'muminus_ETA < 4.5',
    'muplus_ETA  < 4.5',
    #
    'muminus_ISMUON',
    'muplus_ISMUON',
    'muminus_TRPCHI2 > 0.01',
    'muplus_TRPCHI2  > 0.01',
    # 'muminus_0.50_cc_IT > 0.9',
    # 'muplus_0.50_cc_IT > 0.9',
    'muminus_IP_OWNPV > 0',
    'muplus_IP_OWNPV > 0',
    #
    # {'muminus_TOS_MUON', 'muplus_TOS_MUON'}, # OR
    ('muminus_TOS_MUON', 'muplus_TOS_MUON'), # AND
  )

  tree = import_tree( 'Trigger/Z02MuMu', *JIDS )
  tree.title = 'Z#rightarrow#mu#mu (Data)'

  ## Set up the alias
  tree.SetAlias('Z02mumu', cut_filt)
  tree.SetAlias('DPHI'   , 'Z0_DPHI12')
  tree.SetAlias('APT'    , 'TMath::Abs(muminus_PT - muplus_PT)/(muminus_PT+muplus_PT)')
  tree.SetAlias('IP'     , 'muminus_BPVIP')
  tree.SetAlias('ISO1'   , 'muminus_0.50_cc_IT')
  tree.SetAlias('ISO2'   , 'muplus_0.50_cc_IT')
  tree.SetAlias('_Iso'   , '(ISO1>0.9) & (ISO2>0.9)')

  return tree

def _get_tree_DCTW_mumu(*JIDS):
  """
  Load DCTW tree
  """
  tname_os = 'DitauCandTupleWriter/mu_mu' # force channel mumu
  tree     = import_tree( tname_os, *JIDS )
  tree.SetAlias('Z02mumu', CUT_DCTW )
  tree.SetAlias('IP'     , 'mu1_BPVIP')
  tree.SetAlias('ISO'    , 'mu1_0.50_cc_IT')
  tree.SetAlias('_Iso'   , '(mu1_0.50_cc_IT>0.9) & (mu2_0.50_cc_IT>0.9)')
  id_utils.apply_alias_simple('mumu', tree)
  return tree

@memorized
def get_tree_data2():
  """
  Alternative of above taking the tree of DCTW. Same structure as di-tau
  candidate, but have some pre-selection to suppress file size (DPHI > 2.7)
  which thus not viable for some var.

  Has PV-refit applied like cand
  """
  ## Batch1611: uncut hadron ETA, TRPCHI2
  # JIDS = 5204, 5205, 5206, 5207, 5208, 5209, 5210, 5211, 5212, 5213, 5214, 5215, 5216, 5217
  hint = '5204-5217'
  tree = _get_tree_DCTW_mumu(hint)
  tree.title = 'Z#rightarrow#mu#mu (Data)'
  return tree

@memorized
def t_zmu():
  """
  Load the Z->mumu from MC sample.

  Apply identical cut as above
  """
  tree = _get_tree_DCTW_mumu(4314)
  tree.title = 'Z#rightarrow#mu#mu (MC)'
  id_utils.apply_alias(ditau_cuts, 'mu_mu', 't_dymu', tree)
  return tree

@memorized
def t_ztau():
  tree = _get_tree_DCTW_mumu(5230)
  tree.title = 'Z#rightarrow#tau_{#mu}#tau_{#mu} (MC)'
  id_utils.apply_alias(ditau_cuts, 'mu_mu', 't_ztautau', tree)
  return tree

def t_ztautau(dt):
  """
  Arbitary channel, for reweighing
  """
  dtype    = DT_TO_DTYPE[dt]
  tname_os = 'DitauCandTupleWriter/%s'%dtype
  tree     = import_tree(tname_os, 5230).SetTitle('Zg tautau')
  id_utils.apply_alias_simple(dt, tree)
  id_utils.apply_alias(ditau_cuts, dtype, 't_ztautau', tree)
  # more alias, for smooth column name
  tree.SetAlias('Selection_wmw', 'Sel_truechan & _Mass')
  ## before slicing
  tree.estimate = tree.entries+1
  return tree

#===============================================================================
