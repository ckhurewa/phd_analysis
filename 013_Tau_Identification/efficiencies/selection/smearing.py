#!/usr/bin/env python

"""

Use the smearing technique to correct the IP distribution in MC to match data.

"""

from PyrootCK import *
from PyrootCK.mathutils import EffU
import pandas as pd
from uncertainties import ufloat

## Local handle
import trees

## Shortcut
Normal = pd.np.random.normal

#===============================================================================

def main_zmumu():
  """
  Study the effect of adding resolution in Zmumu control sample.
  Send to Ronan. 
  """
  ## Apply LHCb style
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Prepare cut for zmumu data-driven sample
  cut = utils.join(
    'Z02mumu', 'APT<0.3', 'DPHI>2.7', 'DPHI<3.1', 
    'nPVs==1', 'mu1_0.50_cc_IT>0.9', 'mu2_0.50_cc_IT>0.9'
  )

  ## PT-dependent resolution
  tree = trees.t_zmu()
  tree.SetAlias('sigma1', '11.6 + 23.4/PT2')
  tree.SetAlias('sigma2', '11.6 + 22.6/PT2')
  tree.SetAlias('reso' , '1e-3 * 2 * (sigma1**2 - sigma2**2)**0.5') # x2 for XY, conservatively
  ## Fixed resolution, based on last high-PT bins ~ 5GeV
  # reso = 1.5e-3
  # reso = 0.5e-3
  # reso = 5.0e-3
  # tree.SetAlias('reso', '1e-3 * 2 * 2.3')

  ## Slice it
  tree.Draw('PT2: IP2: reso', cut, 'goff')
  df = tree.sliced_dataframe()

  ## Histogram of IP
  nbin = 100
  h = ROOT.TH1F('hIP', 'IP', nbin, pd.np.geomspace(1e-4, 1, nbin+1))

  ## data
  trees.get_tree_data2().Draw('IP2 >> hIP', cut, 'goff')
  h_data = h.Clone('h_data')
  # h_data.Scale(1./h_data.Integral())
  h_data.Scale(1.)
  h_data.title = 'Data'
  h_data.lineWidth = 0
  h.Reset()

  ## no smearing
  tree.Draw('IP2 >> hIP', cut, 'goff')
  h_before = h.Clone('h_before')
  # h_before.Scale(1./h_before.Integral())
  h_before.Scale(h_data.Integral()/h_before.Integral())
  h_before.title = 'MC: no smearing'
  h_before.markerSize = 0
  h.Reset()

  ## with smearing
  n = len(df.index)
  for _ in xrange(1): # repeat to suppress stat effect
    se = df.IP2 + df.reso * Normal(size=n)
    for val in se:
      h.Fill(val)
  h_after = h.Clone('h_after')
  # h_after.Scale(1./h_after.Integral())
  h_after.Scale(h_data.Integral()/h_after.Integral())
  h_after.title = 'MC: with smearing'
  h_after.markerSize = 0
  h.Reset()

  ## Start drawing
  c = ROOT.TCanvas()
  h_data.Draw()
  h_data.xaxis.title = 'IP [mm]'
  # h_data.yaxis.title = 'A.U.'
  h_before.lineColor = ROOT.kRed
  h_before.Draw('same hist')
  h_after.lineColor = ROOT.kGreen
  h_after.Draw('same hist')
  c.logx = True
  c.Update()

  for h in h_data, h_before, h_after:
    print h.name, h.mean, h.RMS

  ## Kolgomorov
  text = ROOT.TPaveText(0.16, 0.74, 0.4, 0.90, 'NDC')
  text.textAlign = 10
  text.AddText('Kolgomorov')
  text.AddText('No smearing  : %.3f' % h_data.KolmogorovTest(h_before))
  text.AddText('With smearing: %.3f' % h_data.KolmogorovTest(h_after))
  text.fillColorAlpha = 0, 0.
  text.Draw('f')

  print 'data--data  :', h_data.KolmogorovTest(h_data)
  print 'data--before:', h_data.KolmogorovTest(h_before)
  print 'data--after :', h_data.KolmogorovTest(h_after)

  ## Legend
  leg = c.BuildLegend()
  leg.header = 'Z#rightarrow#mu#mu'
  leg.x1 = 0.66
  leg.x2 = 0.97
  leg.y1 = 0.75
  leg.y2 = 0.97
  leg.Draw()

  ## save
  c.rightMargin = 0.01 
  c.topMargin   = 0.01 
  c.RedrawAxis()
  c.Update()
  c.SaveAs('smearing/zmumu_IP.pdf')

#===============================================================================

def calc_ztautau(dt, minip):
  """
  pass
  """
  ## get the tree, make alias, slice for df
  tree = trees.t_ztautau(dt)
  
  ## PT-dependent resolution.
  tree.SetAlias('sigma1', '11.6 + 23.4/PT2')
  tree.SetAlias('sigma2', '11.6 + 22.6/PT2')
  tree.SetAlias('reso' , '1e-3 * 2 * (sigma1**2 - sigma2**2)**0.5') # x2 for XY, conservatively
  ## Fixed resolution, based on last high-PT bins ~ 5GeV
  # tree.SetAlias('reso', '1e-3 * 2 * 2.3')

  ## Get number ent/evt
  n1,e1 = tree.count_ent_evt('Presel_truechan')
  n2,e2 = tree.count_ent_evt('Presel_truechan & (IP2 > %f)'%minip)

  ## no-smearing eff
  esel0 = EffU(e1, e2)

  ## Slice it
  tree.estimate = tree.entries+1
  tree.Draw('runNumber: eventNumber: PT2: IP2: reso', 'Presel_truechan', 'goff')
  df = tree.sliced_dataframe()

  ## loop calculate efficiency distribution
  h = ROOT.TH1F('esel_smeared', 'esel_smeared', 100, esel0.n*0.99, esel0.n*1.02)
  n = len(df.index)
  for i in xrange(100): # repeat to suppress stat effect
    mask = (df.IP2 + df.reso*Normal(size=n)) > minip
    evt  = len(df[mask].groupby(['runNumber', 'eventNumber']))*1.
    eff  = evt/e1
    # print i, eff
    h.Fill(eff)

  ## Use the collected distribution, cast it as new esel.
  esel = ufloat(h.mean, h.RMS) * ufloat(1, esel0.rerr)
  print 'No-smearing  : {:.2f}%'.format(esel0*100.)
  print 'With-smearing: {:.2f}%'.format(esel*100.)
  print 'Changes      :', esel.n/esel0.n

  ## save Ztautau eff
  c = ROOT.TCanvas()
  h.Draw()
  c.SaveAs('smearing/ztau_esel_IP_%s.pdf'%dt)

def main_ztautau():
  ROOT.gROOT.batch = True
  calc_ztautau('h1mu', 0.03)
  calc_ztautau('eh1' , 0.03)
  calc_ztautau('mumu', 0.05)
  calc_ztautau('ee'  , 0.05)

#-------------------------------------------------------------------------------

def results():
  """
  return the correction factor to be applied to Ztautau esel.
  """
  correction = pd.Series({
    'h1mu': 1.00040287794,
    'eh1' : 1.0005372969,
    'mumu': 1.00171971194,
    'ee'  : 1.00224363613,
  })
  return pd.DataFrame({
    'corr_IP2': correction,
    'syst_IP2': (correction-1.)*100.,
  }).T

#===============================================================================

def main_dphi_zmumu():
  ## slice the dataframe: data & mc
  cut   = 'Z02mumu & (APT < 0.2)'
  tree  = trees.get_tree_data1()
  tree.Draw('DPHI: APT', cut, 'goff para')
  df_data = tree.sliced_dataframe()

  ## MC Zmumu
  tree    = trees.t_zmu()
  param   = 'DPHI: mu1_PHI: mu2_PHI'
  tree.Draw(param, cut, 'goff para')
  df_mc   = tree.sliced_dataframe()

  ## define the histogram
  from math import pi
  xmax = pi * 1.001
  xbin = 2
  ## anti
  xarr = list(xmax-pd.np.geomspace(xmax, 1e-3, xbin+1))
  ## linear
  # xarr = pd.np.linspace(0., xmax, xbin+1)

  ## Apply additional smearing on the MC
  Arr = lambda: Normal(size=len(df_mc.index))

  ## round dphi
  rounder1 = lambda x: x+2*pi if x < 0 else x
  rounder2 = lambda x: 2*pi-x if x > pi else x

  ## Helper: region lock
  def in_rough_region(phi):
    if (phi > -pi) and (phi < -2):
      return True
    if (phi > -1) and (phi < 1):
      return True
    if (phi > 2) and (phi < -pi):
      return True
    return False
  def add_reso_region(se_phi, se_reso):
    df = pd.concat({'phi':se_phi, 'reso':pd.Series(se_reso)}, axis=1)
    get = lambda se: se.phi+se.reso if in_rough_region(se.phi) else se.phi
    return df.apply(get, axis=1)

  ## binned before
  df = df_mc.DPHI
  g1 = pd.cut(df, xarr)
  h_before = df.groupby([g1]).count()
  h_before /= h_before.sum()
  print h_before

  ## binned data
  df = df_data.DPHI
  g1 = pd.cut(df, xarr)
  h_data = df.groupby([g1]).count()
  h_data /= h_data.sum()
  print h_data

  # for mult in pd.np.linspace(0.8, 1.0, 200+1):
  # for reso in pd.np.linspace(0.0001, 0.001, 10):
  # for reso in pd.np.geomspace(0.001, 0.002, 50):
  # for reso in pd.np.linspace(0.00001, 0.0001, 10):
  # for reso in [0.004, 0.002, 0.001, 0.0008, 0.0005]:
  for reso in [0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]:
  # for reso in [5e-4, 3e-4, 2e-4, 1e-4, 8e-5]:

    ## repeat n times
    # for _ in xrange(4):
    # for _ in xrange(10):

      # random
      mc_phi1 = df_mc.mu1_PHI + reso*Arr()
      mc_phi2 = df_mc.mu2_PHI + reso*Arr()
      smeared_dphi = (mc_phi1-mc_phi2).apply(rounder1).apply(rounder2)

      # ## Random, but only for PHI in [-pi,-2][-1,1][2,pi]
      # # as this region the MC shows underestimated resolution
      # mc_phi1 = add_reso_region(df_mc.mu1_PHI, reso*Arr())
      # mc_phi2 = add_reso_region(df_mc.mu2_PHI, reso*Arr())
      # smeared_dphi = (mc_phi1-mc_phi2).apply(rounder1).apply(rounder2)

      # ## const1 (prior to 2pi rounding)
      # mc_phi1 = df_mc.mu1_PHI + reso
      # mc_phi2 = df_mc.mu2_PHI
      # smeared_dphi = (mc_phi1-mc_phi2).apply(rounder1).apply(rounder2)

      ## const2: add to dphi directly
      # smeared_dphi = df_mc.DPHI - reso # plus or minus?

      ## scale factor
      # df = (pi - df_mc.DPHI)*mult

      ## Bacause of different length, cannot do unbinned chi2 test
      ## binned them first
      df   = smeared_dphi
      g1   = pd.cut(df, xarr)
      h_after = df.groupby([g1]).count()
      h_after /= h_after.sum()
      # print h_after

      # print pd.DataFrame({
      #   'data'  : h_data,
      #   'before': h_before,
      #   'after' : h_after,
      # })

      ## Calculate chisquare (goodness). Smaller the better
      # from scipy.stats import chisquare
      # print chisquare(h_data, h_before)
      # print chisquare(h_data, h_after)
      print reso, ((h_data-h_after)**2).sum()/((h_data-h_before)**2).sum()

#===============================================================================

if __name__ == '__main__':
  pass

  ## IP
  # main_zmumu() 
  # main_ztautau()
  # print results()

  ## DPHI
  main_dphi_zmumu()
