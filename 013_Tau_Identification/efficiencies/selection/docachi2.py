#!/usr/bin/env python

from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
from ditau_prep import dt, t_real, t_dymu, t_ztautau


#===============================================================================

def draw():
  """
  Draw and obtain the static syst.
  """
  assert dt=='mumu'

  t_real.title    = 'Z#rightarrow#mu#mu (data)'
  t_dymu.title    = 'Z#rightarrow#mu#mu (simulation)'
  t_ztautau.title = 'Z#rightarrow#tau_{#mu}#tau_{#mu} (simulation)'
  
  ## Setup
  QHist.reset()
  QHist.filters = 'Z02mumu', 'DPHI > 2.7', 'APT < 0.3'
  QHist.trees   = t_real, t_dymu

  ## The pretty one
  h = QHist()
  h.name    = 'DOCACHI2_full'
  h.params  = 'DOCACHI2'
  h.xmin    = 1e-6
  h.xmax    = 1e3
  h.ymin    = 1e-4
  h.xlog    = True
  h.ylog    = True
  h.xlabel  = '#chi^{2}_{DOCA}'
  # h.draw()

  ## The narrow one, showing disagreement at large DOCACHI2
  h = QHist()
  h.filters = 'DPHI > 2.7'
  h.name    = 'DOCACHI2_zoom'
  h.expansion = [
    (t_real   , 'DOCACHI2', 'Z02mumu & (APT < 0.3)'),
    (t_dymu   , 'DOCACHI2', 'Z02mumu & (APT < 0.3)'),
    (t_ztautau, 'DOCACHI2', 'Preselection'),
  ]
  h.xmin    = 1e-2
  h.xmax    = 1e2
  h.xlog    = True
  h.ylog    = True
  h.xlabel  = '#chi^{2}_{DOCA}'
  h.legends = [t.title for t,_,_ in h.expansion]
  h.draw()

  ## The (working?) correction one
  h = QHist()
  h.expansion = [
    ( t_real, 'DOCACHI2', '' ),
    ( t_dymu, 'DOCACHI2', '' ),
    ( t_dymu, 'TMath::Power(10,TMath::Log10(DOCACHI2)/0.979553+0.00430196)', '' ),
  ]
  h.xmin    = 1e-5
  h.xmax    = 1e2
  h.ylog    = True
  # h.draw()

  # print 'chi2 before:', h[0].Chi2Test( h[1], 'CHI2/NDF' )
  # print 'chi2 after :', h[0].Chi2Test( h[2], 'CHI2/NDF' )


  ## The one for syst computation
  h = QHist()
  h.params  = 'DOCACHI2'
  h.xmin    = 1e-6
  h.xmax    = 1e3
  h.xlog    = True
  h.ylog    = True
  # h.normalize = False
  # h.draw()

  ## delegate
  # calc_syst_between_histo(h)
  # exit()

#===============================================================================


if __name__ == '__main__':
  draw()
