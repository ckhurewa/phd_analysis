#!/usr/bin/env python

"""

Check on color propagation which makes rVj > 1.

qg2Wq
   g    u -->   W+    d:  368
   g    d -->   W-    u:  222
   g dbar -->   W+ ubar:  100
   g ubar -->   W- dbar:  87
   g sbar -->   W+ cbar:  57
   g    s -->   W-    c:  53
   g cbar -->   W- sbar:  33
   g    c -->   W+    s:  27
   g    u -->   W+    s:  22
   g    d -->   W-    c:  14
   g dbar -->   W+ cbar:  5
   g ubar -->   W- sbar:  5
   g    s -->   W-    u:  3
   g sbar -->   W+ ubar:  2
   g    c -->   W+    d:  1
   g cbar -->   W- dbar:  1

"""

from collections import Counter
from PyPythia8 import Pythia

#===============================================================================

# def is_Wj_opposite_sign(process):
  # return (process[5].charge * process[6].charge) < 0

#===============================================================================

def main():
  pythia = Pythia()

  ## Process
  pythia.readString('WeakBosonAndParton:qg2Wq    = on')
  # pythia.readString('WeakBosonAndParton:qqbar2Wg = on')

  pythia.readString('24:onMode = 0ff')
  pythia.readString('24:onIfAny = 13, -13')

  pythia.readString('Beams:eCM = 8000')
  # pythia.readString('HadronLevel:all = off')
  pythia.readString('Next:numberShowProcess = 0')
  pythia.readString('Next:numberShowEvent   = 0')
  pythia.init()


  counter = Counter()
  for _ in xrange(500):
    pythia.next()
    # pythia.process.list()
    # pythia.event.list()
    # print pythia.event[6].botCopyId().index
    # counter[is_Wj_opposite_sign(pythia.process)] += 1

    p = sorted(pythia.event, key=lambda p: -p.pT * p.isFinal * p.isCharged * p.isHadron)[0]
    W = pythia.event[5]
    opsign = (W.charge*p.charge < 0)
    # print W.name, p.name, opsign
    counter[opsign] += 1

    ## qg2Wq only
    # key = [pythia.process[3].name, pythia.process[4].name, pythia.process[5].name, pythia.process[6].name]
    # key.remove('g')
    # key = ['g'] + key
    # counter[tuple(key)] += 1

  # for key, val in counter.most_common():
  #   print '%04s %04s --> %04s %04s: '%key, val

  print counter
  pythia.stat()

#===============================================================================

if __name__ == '__main__':
  main()