#!/usr/bin/env python
"""

Binning configuration & weights to be used in all tracking modules

"""
from rec_utils import *

## Global binning spec
# note: low-PT is available form DD Jpsi study.
BIN_ETA     = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
BIN_PT      = 1, 1.5, 2.5, 4, 6, 10, 20, 30, 40, 50, 70
BIN_P       = 1, 10, 25, 40, 60, 200, 250, 300, 350, 400, 450, 500, 1e3, 2e3, 5e3
BIN_NTRACKS = 0, 100, 200, 300, 450, 1000

## low-PT regime for Jpsi study
BIN_ETA_LOW = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
BIN_PT_LOW  = 1, 1.5, 2.5, 4, 6, 10, 20
BIN_P_LOW   = 1, 10, 25, 40, 60, 200

BIN_PT_HIGH   = 20, 30, 40, 50, 70
BIN_ETA_HIGH  = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5

## The choice of ETA binning in 13 bins by sfarry
BIN_ETA13 = 2.0, 2.080, 2.165, 2.25, 2.375, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5

## Another alternative binning in W from Steve.
# Also known as ETA10
BIN_ETA_W = 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5

## Debugging eta bin with many bins for smoothness
BIN_ETA_MANY = list(pd.np.arange(2.0, 4.5, 2.5/40))+[4.5]

## Specific bin for testing purpose
BIN_ETA_TEST = 2.0, 2.5
BIN_PT_TEST  = 1.0, 1.5

## no binning
BIN_ETA_NONE      = 2.0, 4.5
BIN_PT_NONE       = 1, 1e3
BIN_NTRACKS_NONE  = 0, 1e3

## test ntracks to data comparison
BIN_NTRACKS_TEST = 0, 50, 75, 100, 125, 150, 200, 250, 300, 400, 600

## low-bins version
BIN_NTRACKS_LITE = 0, 100, 300, 600
BIN_PT_LITE      = 5, 20, 40, 70, 300

## List of regimes
REGIMES = {
  'trio': {
    'ETA'    : BIN_ETA_W,
    'PT'     : BIN_PT_LITE,
    'NTRACKS': BIN_NTRACKS_LITE,
  },
  'etaW': {
    'ETA'    : BIN_ETA_W,
    'NTRACKS': BIN_NTRACKS_NONE,
    'PT'     : BIN_PT_NONE,
  },
  'etaW_nlite': {
    'ETA'    : BIN_ETA_W,
    'NTRACKS': BIN_NTRACKS_LITE,
    'PT'     : BIN_PT_NONE,
  },
  'eta_pt': {
    'ETA'    : BIN_ETA_W,
    'PT'     : BIN_PT,
    'NTRACKS': BIN_NTRACKS_NONE,
  },
  ## Debugging. Making sure of the closure
  'none': {
    'ETA'    : [-1, 10],
    'PT'     : [-1, 1e6],
    'NTRACKS': [-1, 1e6],
  },
}

#===============================================================================
# WEIGHTS
#===============================================================================

@pickle_dataframe_nondynamic
def weights_pt():
  return weighter_pt( bin_pt=BIN_PT )

@pickle_dataframe_nondynamic
def weights_eta13():
  return weighter_eta( bin_eta=BIN_ETA13 )

@pickle_dataframe_nondynamic
def weights_etaW():
  return weighter_eta( bin_eta=BIN_ETA_W )

@pickle_dataframe_nondynamic
def weights_pt_eta_lowpt():
  return weighter_pt_eta( bin_pt=BIN_PT_LOW, bin_eta=BIN_ETA_LOW )

@pickle_dataframe_nondynamic
def weights_p_eta_low():
  return weighter_p_eta( bin_p=BIN_P_LOW, bin_eta=BIN_ETA_LOW )

@pickle_dataframe_nondynamic
def weights_pt_eta_highpt():
  return weighter_pt_eta( bin_pt=BIN_PT_HIGH, bin_eta=BIN_ETA_HIGH )

@pickle_dataframe_nondynamic
def weights_pt_eta_allpt_uniformeta():
  """
  For simple case like MC: Combine pt with simple uniform ETA bins
  """
  return weighter_pt_eta( bin_pt=BIN_PT, bin_eta=BIN_ETA )

#===============================================================================
# MC EFF
# Some of these are common enough for muon/elec/hadron. Compute once.
#===============================================================================

@pickle_dataframe_nondynamic
def multieff(tag):
  """
  Has all schema. Completely correlated.
  """
  logger.info('Binning %s'%tag)
  scheme = REGIMES[tag]
  return calc_etrack_mc_regime(**scheme)

def multieff_all():
  """
  TODO: I failed to collect all regimes into one, because each dataframe can have
  different index levels.
  """
  for rg in REGIMES:
    multieff(rg)

#===============================================================================

if __name__ == '__main__':
  pass 

  ## Weights
  # print weights_pt()
  # print weights_eta13()
  # print weights_etaW()
  # print weights_pt_eta_lowpt()
  # print weights_p_eta_low()
  # print weights_pt_eta_highpt()
  # print weights_pt_eta_allpt_uniformeta()

  ## Raw etrack
  # print multieff_all()
  print multieff('none').T.fmt2p
  # print multieff('etaW_nlite').loc[Idx['fulldiv'],'electrons']
