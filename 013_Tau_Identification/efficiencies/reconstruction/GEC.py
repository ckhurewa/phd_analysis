#!/usr/bin/env python

## Load tools
from rec_utils import pd, CHANNELS, DT_TO_LATEX, memorized, ufloat, nominal_value

## Source file
# JID = 3828
JID = 3845 # with Z0 mass

## Const values from data
EGEC_MU      = ufloat( 0.9300, 0.0032 )
EGEC_DIELEC  = ufloat( 0.916 , 0.006  )
EGEC_ELEC    = ufloat( (EGEC_MU.n+EGEC_DIELEC.n)/2, (EGEC_MU.n-EGEC_DIELEC.n)/2  )

#===============================================================================

def main_muon():
  """
  Fit the shape of nSPDhits of muons to analytic function
  """
  x   = RooFit.RooRealVar( 'nSPDHits'  , 'nSPDHits', 0, 1200 )
  p0  = RooFit.RooRealVar( 'p0' , 'p0' ,  1E0, 1E2 )
  p1  = RooFit.RooRealVar( 'p1' , 'p1' ,    1,  10 )
  p2  = RooFit.RooRealVar( 'p2' , 'p2' ,   10, 1E3 )
  # p3  = RooFit.RooRealVar( 'p3' , 'p3' ,    0, 1E2 )

  model = RooFit.RooGenericPdf( 'model', 'p0*pow(nSPDHits,p1)*exp(-nSPDHits/p2)', [x, p0, p1, p2])

  ## Inserting data
  tree = import_tree( 'GEC/Z02MuMu', JID )
  data = tree.dataset( x, 'L0DiMuonDecision==1' )
  # print data

  model.fitTo( data, RooFit.Range(600,900) )

  ## Start fitting
  frame = x.frame()
  data.plotOn ( frame )
  model.plotOn( frame, RooFit.Range(0,1200))
  frame.Draw()

  ROOT.gPad.Update()

  ## Calculating GEC eff
  x.setRange( 'signal' , 0  , 600  )
  x.setRange( 'outside', 900, 1200 )
  intg = model.createIntegral( {x}, RooFit.NormSet({x}), RooFit.Range('outside') )
  n_000_600   = tree.GetEntries('L0DiMuonDecision==1 & (nSPDHits<=600)')
  n_600_900   = tree.GetEntries('L0DiMuonDecision==1 & (nSPDHits>600) & (nSPDHits<=900)')
  n_900_1200  = intg.val/(1200.-900)
  print '[  0,  600]', n_000_600
  print '[600,  900]', n_600_900
  print '[900, 1200]', n_900_1200
  print 'Eff:', var(n_000_600) / (n_000_600+n_600_900+n_900_1200)

  exit()

#===============================================================================

def main_electron():
  """
  Study electron's nSPDHits distribution
  """
  tree = import_tree( 'GEC/Z02MuMu', JID )
  h = QHist()
  h.name    = 'electron_dimuon'
  h.trees   = tree 
  h.params  = 'nSPDHits'
  h.cuts    = 'L0DiMuonDecision==1', 'L0ElectronDecision==1'
  h.draw()

#===============================================================================

def compare_spd():
  """
  Compare nSPD shape between channel after acceptance.
  """
  from kinematic import TREES

  ## Visualize
  # t_mumu = TREES('mu_mu')
  # t_h3mu = TREES('h3_mu')
  # t_eh3  = TREES('e_h3')
  # h = QHist()
  # h.filters = '00_Acceptance'
  # h.trees   = t_mumu, t_h3mu, t_eh3
  # h.params  = 'nSPDhits'
  # h.draw()

  ## Everything up to before GEC cut
  filt = utils.join(
    'Fiducial',
    'Acceptance',
    'MCpredict',
    'MCrec',
    'Tracking',
    'Vertex',
    'Kinematic',
    'PID',
  )

  ## Summarize MC level GEC eff
  df = pd.DataFrame()
  for dtype in DTYPES:
    tree      = TREES(dtype)
    n_before  = tree.GetEntries(filt)
    n_after   = tree.GetEntries(filt+'& GEC')
    df[dtype] = ( n_before, n_after )
  df.index = 'before', 'after'
  df.loc['eff']       = df.loc['after'] / df.loc['before']
  df.loc['corr_mumu'] = df.loc['eff']   / df.loc['eff','mu_mu']
  df.loc['corr_ee']   = df.loc['eff']   / df.loc['eff','e_e']
  print df


#===============================================================================

def compare_spd_cumulative():
  from kinematic import TREES

  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetMarkerStyle(1)
  # ROOT.gStyle.SetFillColor(-1)
  # ROOT.gStyle.SetFillStyle(4000) # exclude, for box,legendentry  

  ## Everything up to before GEC cut
  filt = utils.join(
    'Fiducial',
    'Acceptance',
    # 'MCpredict',
    # 'MCrec',
    'Tracking',
    'Vertex',
    'Kinematic',
    'PID',
  )

  def get_cumhist(dtype):
    t = TREES(dtype)
    t.Draw('nSPDhits >> h(1000,0,1000)', filt, 'goff')
    h  = ROOT.gROOT['h']
    # h2 = h.Clone(t.name)   # normal
    h2 = h.cumulative       # cumulative
    h2.Scale(1./h.Integral())
    h.Delete()
    h2.title = LABELS_DITAU[DTYPE_TO_DT[dtype]]
    return h2

  def get_xrange(h, yvalue):
    """
    Given the value of y-axis in ufloat form, return the equivalent range in 
    x-axis also in the ufloat form covering the same region
    """
    se    = h.series()
    ylow  = yvalue.n - yvalue.s
    yhigh = yvalue.n + yvalue.s
    se2   = se[(se > ylow) & (se < yhigh)]
    xlow  = float(se2.index[0].split(',')[0].replace('(',''))
    xhigh = float(se2.index[-1].split(',')[1].replace(']',''))
    return ufloat((xlow+xhigh)/2, (xhigh-xlow)/2)

  def find_consv_y_syst(xval, *histos):
    df0 = pd.DataFrame([h.series() for h in histos]).T.applymap(nominal_value)
    df  = df0.iloc[int(xval.low):int(xval.high)]
    return max(abs(1.-x) for _,col in df.iteritems() for x in col)

  def Box(list_xy):
    """
    Quickly draw opaque box.
    """
    x,y = zip(*list_xy)
    pl  = ROOT.TPolyLine( len(x), x, y )
    pl.ownership      = False
    pl.fillColorAlpha = ROOT.kBlack, 0.4
    return pl

  def draw_trio(h1, h2, h3, ytitle, xval):
    """
    Provide consistent decoration for 2 histos
    """
    ## Repeat for EX
    c = ROOT.TCanvas()
    c.ownership = False
    h1.Draw()
    h2.Draw('same')
    h3.Draw('same')
    h1.minimum      = 0.95
    h1.maximum      = 1.02
    h1.xaxis.title  = 'nSPDhits'
    h1.yaxis.title  = ytitle
    h2.lineColor    = ROOT.kRed
    h3.lineColor    = ROOT.kBlue
    #
    leg = ROOT.gPad.BuildLegend()
    leg.ConvertNDCtoPad()
    leg.header  = 'LHCb-Simulation'
    leg.x1NDC   = 0.57
    leg.x2NDC   = 0.98
    leg.y1NDC   = 0.20
    leg.y2NDC   = 0.52
    leg.textSize = 0.07
    # ROOT.gPad.SetGridx()
    # ROOT.gPad.SetGridy()
    ROOT.gPad.RedrawAxis()
    #
    # highlight the special range. After legend, because I don't need it.
    box = ROOT.TBox(xval.low, h1.minimum, xval.high, h1.maximum)
    box.ownership = False
    box.fillColorAlpha = ROOT.kGreen, 0.5
    box.Draw()

  ## Obtain the cumulative hist.
  h_mumu = get_cumhist('mu_mu')
  h_h1mu = get_cumhist('h1_mu')
  h_h3mu = get_cumhist('h3_mu')
  h_emu  = get_cumhist('e_mu')
  h_eh1  = get_cumhist('e_h1')
  h_eh3  = get_cumhist('e_h3')

  ## Find the eqv point in MC which has same eGEC as in data. It's somewhere 
  # of smaller SPDhits than 600
  xrange_mu = get_xrange(h_mumu, EGEC_MU)
  xrange_e  = get_xrange(h_emu , EGEC_ELEC)
  print 'Key range for syst:'
  print 'mu:', xrange_mu
  print 'e :', xrange_e

  ## Scale to mumu, last first.
  h_h3mu.Divide(h_mumu)
  h_h1mu.Divide(h_mumu)
  h_mumu.Divide(h_mumu) # last
  h_eh3.Divide(h_emu)
  h_eh1.Divide(h_emu)
  h_emu.Divide(h_emu)

  ## Kill of first few bins
  for i in xrange(1,10):
    for h in [h_h1mu, h_h3mu, h_eh1, h_eh3]:
      h.SetBinContent(i,0)
    for h in [h_mumu, h_emu]:
      h_emu.SetBinContent(i,1)
  
  ## Using the special eqv range above, find the corresponding syst from the 
  # scaled histogram. 
  # Fortunately that the nSPD starts with zero, so I can do the iloc.
  print 'Conservative syst [%]:'
  print 'h1mu:', find_consv_y_syst(xrange_mu, h_h1mu)*100.
  print 'h3mu:', find_consv_y_syst(xrange_mu, h_h3mu)*100.
  print 'eh1 :', find_consv_y_syst(xrange_e , h_eh1)*100.
  print 'eh3 :', find_consv_y_syst(xrange_e , h_eh3)*100.

  ## Actual draw, start with mu
  ytitle = '#varepsilon_{GEC} / #varepsilon_{GEC}(#tau_{#mu}#tau_{#mu})'
  draw_trio(h_mumu, h_h1mu, h_h3mu, ytitle, xrange_mu)
  ROOT.gPad.SaveAs('GEC/compare_mumu.pdf')

  ## Repeat for EX
  ytitle = '#varepsilon_{GEC} / #varepsilon_{GEC}(#tau_{e}#tau_{#mu})'
  draw_trio(h_emu, h_eh1, h_eh3, ytitle, xrange_e)
  ROOT.gPad.SaveAs('GEC/compare_ex.pdf')


#===============================================================================
# EXPORT
#===============================================================================

@memorized
def efficiencies():
  """
  Return GEC with syst for each channel, careful with uncertainty correlation.
  Use the const global EGEC defined above
  """
  ## Calculated syst from above
  syst_h1mu = ufloat(1., 0.00566023588181)
  syst_h3mu = ufloat(1., 0.00561809539795)
  syst_eh1  = ufloat(1., 0.00738853216171)
  syst_eh3  = ufloat(1., 0.00133740901947)
  return {
    'mumu': EGEC_MU,
    'h1mu': EGEC_MU * syst_h1mu,
    'h3mu': EGEC_MU * syst_h3mu,
    'ee'  : EGEC_DIELEC,
    'eh1' : EGEC_ELEC * syst_eh1,
    'eh3' : EGEC_ELEC * syst_eh3,
    'emu' : EGEC_ELEC,
  }

def latex():
  df = pd.DataFrame({'eff': efficiencies()}).T
  df = df[CHANNELS].rename(columns=DT_TO_LATEX).fmt2lp
  return df.to_markdown(drop_index=True)

#===============================================================================

if __name__ == '__main__':
  # main_muon()
  # main_electron()
  # compare_spd()
  # compare_spd_cumulative()

  print efficiencies()
  print latex()

