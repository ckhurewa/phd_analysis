#!/usr/bin/env python

from rec_utils import *
import pid_electron_bins
import pid_electron_dd
import pid_electron_mc

#===============================================================================

@gpad_save
def draw_compare_dd_mc_eta():
  ## Inputs
  bin_pt, note = '(5, 8]', '5 < p_{T}(e) < 8 GeV'
  # bin_pt = '(20, 30]'
  # bin_pt = '(30, 40]'
  # bin_pt = '(40, 50]'
  df = pd.DataFrame({
    'Data'      : pid_electron_dd.eff_eta_pt_combined()['nominal'].unstack()[bin_pt],
    'Simulation': pid_electron_mc.multieff()['nominal'].unstack()['electrons'].loc[bin_pt].dropna(),
  })
  df.columns.name = '#varepsilon_{PID,e}'
  df.index.name   = '#eta(e)'

  ## Send to drawer
  gr, leg    = drawer_graphs_dd_mc(df)
  gr.minimum = 0.0
  gr.maximum = 1.02
  draw_note(note)


@gpad_save
def draw_compare_dd_mc_pt():
  # bin_eta, note = '(2.1, 2.2]', '2.1 < #eta(e) < 2.2'
  bin_eta, note = '(2.2, 2.4]', '2.2 < #eta(e) < 2.4'
  # bin_eta, note = '(2.4, 2.8]', '2.4 < #eta(e) < 2.8'
  # bin_eta, note = '(2.8, 3.2]', '2.8 < #eta(e) < 3.2'
  # bin_eta, note = '(3.2, 3.6]', '3.2 < #eta(e) < 3.6'

  ## Inputs
  df = pd.DataFrame({
    'Data'      : pid_electron_dd.eff_eta_pt_combined()['nominal'].unstack().loc[bin_eta],
    'Simulation': pid_electron_mc.multieff()['nominal'].unstack()['electrons'].unstack()[bin_eta].dropna(),
  })
  df.columns.name = '#varepsilon_{PID,e}'
  df.index.name   = 'p_{T}(e) [GeV]'
  gr, leg         = drawer_graphs_dd_mc(df)
  gr.minimum      = 0.7
  gr.maximum      = 1.02
  gr.xaxis.rangeUser     = 0, 50
  gr.xaxis.moreLogLabels = True
  draw_note(note)


#===============================================================================
# LATEX
#===============================================================================

@pickle_dataframe_nondynamic # ready for latex
def compare_eff():
  eff_dd = pid_electron_dd.scalar_results()
  eff_mc = pid_electron_mc.scalar_results()
  eff_dd.loc['MC'] = eff_mc[eff_dd.columns]
  return eff_dd


def latex_compare_eff():
  """
  Comparison of different ePID from different modes (MC, DD (nom, x2, /2)),
  as well as the associated systematics.
  """
  df = compare_eff().T
  df['Systematics [\\%]'] = pid_electron_dd.syst().apply(lambda u: u.s)
  df = df.rename({
    'ee_e1': '\\taue (higher-\\pt) from \\dee',
    'ee_e2': '\\taue (lower-\\pt) from \\dee',
    'eh1_e': '\\taue from \\deh1',
    'eh3_e': '\\taue from \\deh3',
    'emu_e': '\\taue from \\dmue',
  }, columns={
    'nominal': 'Nominal',
    'halved' : 'Halved',
    'doubled': 'Doubled',
  })
  df.index.name = ' \\ePID [\\%]'
  ## 170612: drop MC
  df = df.drop('MC', axis=1) * 100
  df.iloc[:,-1] = df.iloc[:,-1].apply('{:.2f}'.format) # force syst to 2deci
  return df.to_bicol_latex(stralign='c') # Aurelio prefer 'c'

  # # manual table, take only content
  # msg = df.fmt2lp.to_latex(escape=False)
  # return '\n'.join(msg.split('\n')[4:-3])


#===============================================================================

if __name__ == '__main__':
  ## Latex
  # print compare_eff()
  print latex_compare_eff()

  ## Drawer
  # draw_compare_dd_mc_eta()
  # draw_compare_dd_mc_pt()
