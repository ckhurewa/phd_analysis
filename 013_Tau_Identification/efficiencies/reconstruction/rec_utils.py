#!/usr/bin/env python

import itertools
from PyrootCK import *
from PyrootCK.mathutils import EffU, EffU_unguard, combine_uncorrelated

sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import *
sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
import selected

#===============================================================================

def get_selected_ztautau(dt):
  """
  Shortcut to load Ztautau tree from the cached file
  """
  return selected.load_selected_trees_dict(dt)['t_ztautau'].dataframe()

def get_selected_data(dt):
  """
  Like above, for data
  """
  return selected.load_selected_trees_dict(dt)['t_real'].dataframe()  

#===============================================================================
# WEIGHTERS
#===============================================================================

## List of all branches, with the local name for DitauCandTupleWrite, DitauRecoStats
BRANCHES = {
  'mumu_mu1': 'lep1',
  'mumu_mu2': 'lep2',
  'h1mu_mu' : 'lep' ,
  'h1mu_pi' : 'had' ,
  'h3mu_mu' : 'lep' ,
  'h3mu_pr1': 'pr1' ,
  'h3mu_pr2': 'pr2' ,
  'h3mu_pr3': 'pr3' ,
  'ee_e1'   : 'lep1',
  'ee_e2'   : 'lep2',
  'eh1_e'   : 'lep' ,
  'eh1_pi'  : 'had' ,
  'eh3_e'   : 'lep' ,
  'eh3_pr1' : 'pr1' ,
  'eh3_pr2' : 'pr2' ,
  'eh3_pr3' : 'pr3' ,
  'emu_e'   : 'lep1',
  'emu_mu'  : 'lep2',
}

## more branches which sum the numerator/denominator from above.
# skip hadrons as it show strong behavior of channel-dependent
BRANCHES_COLLAPSING = {
  'muons'    : [ 'mumu_mu1', 'mumu_mu2', 'h1mu_mu', 'h3mu_mu', 'emu_mu' ],
  'electrons': [ 'ee_e1'   , 'ee_e2'   , 'eh1_e'  , 'eh3_e'  , 'emu_e'  ],
  'h3mu_pr'  : [ 'h3mu_pr1', 'h3mu_pr2', 'h3mu_pr3' ],
  'eh3_pr'   : [ 'eh3_pr1' , 'eh3_pr2' , 'eh3_pr3'  ],
  'ee_sum'   : [ 'ee_e1'   , 'ee_e2'     ],
  'mumu_sum' : [ 'mumu_mu1', 'mumu_mu2'  ], 
}

@memorized
def _load_selected_dataframes():
  """
  Helper to load Ztau tree from all channels, used for weighters
  Return a dict of dataframe, indexed by dt

  P,PT changed from MeV to GeV
  """
  def momentum( eta, pt ):
    return pt / eta.apply(lambda x: np.sin(2*np.arctan(np.exp(-x))) )

  def whitelist(s):
    if '_0.50_' in s: # exclude iso vars
      return False
    if s.endswith('_P'):
      return True
    if s.endswith('_PT'):
      return True
    if s.endswith('_ETA'):
      return True
    if s in ('nSPDhits', 'nTracks'):
      return True
    return False

  acc = {}
  for br in BRANCHES:
    dt,prefix = br.split('_')
    if dt not in acc:
      df    = get_selected_ztautau(dt)
      cols  = [ s for s in df.columns if whitelist(s) ]
      acc[dt] = df[cols]
    df = acc[dt]
    df[prefix+'_PT'] /= 1e3
    ## the prongs unfortunately doesn't have P, provide workaround frmo (ETA,PT)
    # for now, P is only used in muon tracking
    key = prefix+'_P'
    if key in df.columns:
      df[key] /= 1e3
    else:
      df[key] = momentum( df[prefix+'_ETA'], df[prefix+'_PT'] )
  return acc 

def weighter_generic( **kwargs ):
  """
  weighter_generic( _PT=..., nTracks=... )
  """
  def repack(pf):
    """
    Repack the kwargs such that it respect the local prefix in each channel.
    """
    return { (key if not key.startswith('_') else pf+key) :val for key,val in kwargs.iteritems() }

  def custom_sort(se):
    """
    Custom sort the levels in multiindex such that it ignore the prefix, and ignore case
    e.g., mu_PT comes after nTracks.
    """
    ## skip if not multiindex
    if len(se.index.names)==1:
      return se
    l = sorted(se.index.names, key=lambda s: s.split('_')[-1].lower())
    return se.reorder_levels(l).sort_index()

  frames = _load_selected_dataframes()  
  acc = {}
  for name in BRANCHES:
    dt, prefix  = name.split('_')
    se          = custom_sort(_binning_123D( frames[dt], **repack(prefix) ))
    acc[name]   = se
  df = pd.DataFrame(acc).fillna(0)

  ## Collective items
  df['h3mu_pr']   = df.h3mu_pr1 + df.h3mu_pr2 + df.h3mu_pr3
  df['eh3_pr']    = df.eh3_pr1  + df.eh3_pr2  + df.eh3_pr3 
  df['mumu_sum']  = df.mumu_mu1 + df.mumu_mu2
  df['ee_sum']    = df.ee_e1    + df.ee_e2
  df['hadron']    = df.h1mu_pi  + df.eh1_pi + df.h3mu_pr + df.eh3_pr
  return df.applymap(var)


def weighter_pt( bin_pt ):
  df = weighter_generic( _PT=bin_pt )
  df.index.name = 'PT'
  return df

def weighter_eta( bin_eta ):
  df = weighter_generic( _ETA=bin_eta )
  df.index.name = 'ETA'
  return df

def weighter_ntracks( bins ):
  """
  Only used in electrons, so...
  """
  df = weighter_generic( nTracks=bins )
  df.index.name = 'nTracks'
  return df

def weighter_p_eta( bin_p, bin_eta ):
  df = weighter_generic( _ETA=bin_eta, _P=bin_p,  )
  df.index.names = 'ETA', 'P'
  df = df.swaplevel().sort_index()
  return df

def weighter_pt_eta( bin_pt, bin_eta ):
  """
  Swap level to have PT outermost, practical.
  """
  df = weighter_generic( _ETA=bin_eta, _PT=bin_pt )
  df.index.names = 'ETA', 'PT'
  df = df.swaplevel().sort_index() # alphabetic on default
  return df

def weighter_eta_ntracks( bin_eta, bin_ntracks ):
  return weighter_generic( _ETA=bin_eta, nTracks=bin_ntracks )

def weighter_pt_ntracks( bin_pt, bin_ntracks ):
  return weighter_generic( _PT=bin_pt, nTracks=bin_ntracks )

def weighter_pt_eta_spd( bin_pt, bin_eta, bin_spd ):
  df = weighter_generic( _ETA=bin_eta, nSPDhits=bin_spd, _PT=bin_pt )
  df.index.names = 'ETA', 'SPD', 'PT'
  return df.reorder_levels([ 2, 0, 1 ])

def weighter_pt_eta_ntracks(bin_pt, bin_eta, bin_ntracks):
  df = weighter_generic(_ETA=bin_eta, _PT=bin_pt, nTracks=bin_ntracks)
  df.index.names = 'ETA', 'NTRACKS', 'PT'
  return df

#===============================================================================

def calc_etrack_mc_regime(ETA, PT, NTRACKS):
  # alternate signature for below
  return calc_etrack_mc_raw(bin_eta=ETA, bin_pt=PT, bin_ntracks=NTRACKS)

def calc_etrack_mc_raw(bin_eta, bin_pt, bin_ntracks):
  import kinematic

  aliases = {
    'A' : 'Acceptance',
    'B' : '{0}_recob>=2',
    'Bx': '{0}_recob==2',
    'D' : '{0}_recod>=1',
    'I' : '{0}_ID!=0',
    't1': '{0}_TRTYPE==3',
    't2': '{0}_TRPCHI2 > 0.01',
    'T' : 't1&t2',
  }

  aliases0 = [
    ('ETA'  , '{0}_MCETA'   ),
    ('PT'   , '{0}_MCPT/1e3'),
    # ('ETA'    , '{0}_ETA'     ),
    # ('PT'     , '{0}_PT/1e3'  ),
    ('nTracks', 'nTracks'     ),
  ]
  cols0, cuts0 = zip(*aliases0)

  cols, cuts = zip(*[
    ('A'     , 'A'        ),
    ('AB'    , 'A&B'      ),
    ('ABD'   , 'A&B&D'    ),
    ('ABDIT' , 'A&B&D&I&T'),
    ('ABDT'  , 'A&B&D&T'  ),
    ('ABT'   , 'A&B&T'    ),
    ('ABx'   , 'A&Bx'     ),
    ('AIt1'  , 'A&I&t1'   ),
    ('AT'    , 'A&T'      ),
    ('B'     , 'B'        ),
    ('BD'    , 'B&D'      ),
    ('BDI'   , 'B&D&I'    ),
    ('BDIT'  , 'B&D&I&T'  ),
    ('BT'    , 'B&T'      ),
    ('BDT'   , 'B&D&T'    ),
    ('Bx'    , 'Bx'       ),
    ('D'     , 'D'        ),
    ('I'     , 'I'        ),
    ('It1'   , 'I&t1'     ),
    ('IT'    , 'I&T'      ),
    ('t1'    , 't1'       ),
    ('t2'    , 't2'       ),
    ('T'     , 'T'        ),
  ])


  ## Param ready
  params  = ':'.join(cuts+cols0)
  columns = cols+cols0
  log.info('params: '+params)

  ## binning spec
  spec_mc = {'ETA':bin_eta, 'PT':bin_pt, 'nTracks':bin_ntracks}

  ## Caching tree, counters
  trees = {}
  counters = {cname:pd.DataFrame() for cname in cols}

  ## Loop over list of branches
  for dest, prefix in sorted(BRANCHES.iteritems()):
    log.info('queueing: '+dest)
    dt = dest.split('_')[0]
    
    ## fetch a tree, attach alias
    if dt not in trees:
      trees[dt] = kinematic.TREES(dt)
    t = trees[dt]

    ## Reset the alias to current prefix
    for aname, aval in aliases0 + aliases.items():
      t.SetAlias(aname, aval.format(prefix))

    ## Draw all related vars
    n  = t.Draw(params, '', 'para goff')
    df = t.sliced_dataframe()
    df.columns = columns

    ## apply binning
    for cname in cols:
      counters[cname][dest] = binning_3D( df[df[cname]==1], **spec_mc ).T

      # ## debug
      # if dest=='eh1_pi':
      #   print cname, df[cname].sum(), counters[cname][dest]

  ## Collapse partial sum together
  for name, cols in BRANCHES_COLLAPSING.iteritems():
    for c in counters.values():
      c[name] = c[cols].sum(axis=1)

  # ## Debug
  # print pd.concat(counters).T.to_string()

  def div(nume, deno):
    """
    Apply division taking elements from the counter.
    Apply the correlation, per object, per schema, per divtech, here.
    """
    sn  = counters[nume].stack()
    sd  = counters[deno].stack()
    res = divide_clopper_pearson(sn, sd).unstack()
    # force correlation in each column
    for obj, col in res.iteritems():
      res[obj] = completely_correlate(col)
    return res

  ## Wrap the divisions together
  df = pd.concat({
    'fulldiv' : div('ABDIT', 'A'  ),
    'litediv' : div('AT'   , 'A'  ),
    'halfdiv' : div('ABD'  , 'A'  ) * div('BDIT','BDI'),
    'T_B'     : div('BT'   , 'B' ),
    'T_BD'    : div('BDT'  , 'BD'),
    'halfdiv2': div('ABx'  , 'A'  ) * div('IT', 'It1'),
  })
  names = list(df.index.names)
  names[0] = 'divtech'
  df.index.names = names

  ## Apply .xs to kill non-binning index
  for bname, bins in spec_mc.iteritems():
    if len(bins)==2:
      level = df.index.names.index(bname)
      val   = df.index[0][level]
      log.info('slicing: %s=%s'%(bname,val))
      df = df.xs(val, level=level)
  
  ## Finally
  return df

#-------------------------------------------------------------------------------

def calc_epid_mc_eta_pt_spd_raw( mode, bin_eta, bin_pt, bin_spd=None ):
  """
  queues is a list of 3-tuples ( uniquename, channel, tuple_prefix )
  """

  ## import tree, strip & concat, calc pid cut,
  import kinematic
  filt = utils.join([
    # 'Fiducial'  ,
    # 'Acceptance',
    # 'MCpredict' ,
    'MCrec'     ,
    'Tracking'  ,
    'Kinematic' ,
    # 'nSPDhits <= 600',
  ])

  ## Choosing column, 
  assert mode in ('muons', 'hadrons', 'electrons'), "Unknown PID mode"
  
  ## note: prefer capital H for nSPDHits
  if mode == 'muons':
    raw_col = 'nSPDhits {0}_PT {0}_ETA {0}_ISMUON'
    cols    = 'nSPDHits',  'PT',  'ETA',  'ISMUON'
    func    = lambda df: (df['ISMUON']==1)
    queues  = [
      ( 'mumu_mu1', 'mu_mu', 'lep1' ),
      ( 'mumu_mu2', 'mu_mu', 'lep2' ),
      ( 'h1mu_mu' , 'h1_mu', 'lep'  ),
      ( 'h3mu_mu' , 'h3_mu', 'lep'  ),
      ( 'emu_mu'  , 'e_mu' , 'lep2' ),
    ]

  if mode == 'hadrons':
    raw_col = 'nSPDhits {0}_PT {0}_ETA {0}_ISLOOSEMUON {0}_PP_InAccHcal {0}_HCALFrac {0}_PROBNNghost'
    cols    = 'nSPDHits',  'PT',  'ETA',  'ISLOOSEMUON',     'InAccHcal',  'HCALFrac',  'PROBNNghost'
    # func    = lambda df: (df['ISLOOSEMUON']==0) & (df['InAccHcal']==1) & (df['HCALFrac']>0.05)
    func    = lambda df: (df.ISLOOSEMUON==0) & (df.HCALFrac>0.05)
    queues  = [
      ( 'h1mu_pi' , 'h1_mu', 'had' ),
      ( 'h3mu_pr1', 'h3_mu', 'pr1' ),
      ( 'h3mu_pr2', 'h3_mu', 'pr2' ),
      ( 'h3mu_pr3', 'h3_mu', 'pr3' ),
      ( 'eh1_pi'  , 'e_h1' , 'had' ),
      ( 'eh3_pr1' , 'e_h3' , 'pr1' ),
      ( 'eh3_pr2' , 'e_h3' , 'pr2' ),
      ( 'eh3_pr3' , 'e_h3' , 'pr3' ),
    ]

  if mode == 'electrons':
    raw_col = 'nSPDhits {0}_PT {0}_ETA {0}_ISLOOSEMUON {0}_PP_InAccHcal {0}_PP_InAccEcal {0}_HCALFrac {0}_ECALFrac {0}_PP_CaloPrsE'
    cols    = 'nSPDHits',  'PT',  'ETA',  'ISLOOSEMUON',     'InAccHcal',     'InAccEcal',  'HCALFrac',  'ECALFrac',     'CaloPrsE'
    func    = lambda df: (df['ISLOOSEMUON']==0) & (df['InAccHcal']==1) & (df['InAccEcal']==1) & (df['HCALFrac']<0.05) & (df['ECALFrac']>0.1) & (df['CaloPrsE']>50)
    queues  = [
      ( 'ee_e1', 'e_e' , 'lep1' ),
      ( 'ee_e2', 'e_e' , 'lep2' ),
      ( 'eh1_e', 'e_h1', 'lep'  ),
      ( 'eh3_e', 'e_h3', 'lep'  ),
      ( 'emu_e', 'e_mu', 'lep1' ),
    ]


  ## Cache & collect
  df_trees = {}
  acc_nume = {}
  acc_deno = {}

  for name, channel, prefix in queues:
    if channel not in df_trees:
      df_trees[channel] = kinematic.TREES(channel).dataframe(filt)
    df  = df_trees[channel]
    df2 = pd.DataFrame(df[raw_col.format(prefix).split()])
    df2.columns = cols 
    df2.PT /= 1e3
    df2['passing'] = func(df2)
    ## IF SPD
    if bin_spd is not None:
      nume = binning_3D( df2[df2.passing], ETA=bin_eta, PT=bin_pt, nSPDHits=bin_spd )
      deno = binning_3D( df2             , ETA=bin_eta, PT=bin_pt, nSPDHits=bin_spd )
    else:
      nume = binning_2D( df2[df2.passing], ETA=bin_eta, PT=bin_pt )
      deno = binning_2D( df2             , ETA=bin_eta, PT=bin_pt )
    acc_nume[name] = nume.T
    acc_deno[name] = deno.T
  nume = pd.DataFrame(acc_nume)
  deno = pd.DataFrame(acc_deno)
  
  ## IF SPD
  if bin_spd is not None:
    nume = nume.reorder_levels([1,0,2]).sort_index()
    deno = deno.reorder_levels([1,0,2]).sort_index()
  else:
    nume = nume.swaplevel().sort_index()
    deno = deno.swaplevel().sort_index()

  ## partial sum
  nume[mode] = nume.sum(axis=1)
  deno[mode] = deno.sum(axis=1)
  if mode == 'hadrons':
    nume['eh3_pr' ] = nume.eh3_pr1  + nume.eh3_pr2  + nume.eh3_pr3 
    deno['eh3_pr' ] = deno.eh3_pr1  + deno.eh3_pr2  + deno.eh3_pr3 
    nume['h3mu_pr'] = nume.h3mu_pr1 + nume.h3mu_pr2 + nume.h3mu_pr3
    deno['h3mu_pr'] = deno.h3mu_pr1 + deno.h3mu_pr2 + deno.h3mu_pr3

  ## Finally
  return nume, deno

#===============================================================================

def calc_etrig_mc_eta_pt(mode, bin_eta=None, bin_pt=None):
  ## import tree, strip & concat, calc pid cut,
  import kinematic
  filt = utils.join([
    'Fiducial'  ,
    'Acceptance',
    'MCpredict' ,
    'MCrec'     ,
    'Tracking'  ,
    'Kinematic' ,
    'PID'       ,
    'GEC'       ,
  ])

  ## spec
  modes = {
    'muon'    : 'h1_mu',
    'electron': 'e_h1',
  }

  ## assert correct mode
  assert mode in modes
  assert (bin_eta is not None) or (bin_pt is not None)

  ## Prep & run
  raw_col = 'lep_ETA lep_PT lep_TOS_{}'.format(mode.upper())
  df = kinematic.TREES(modes[mode]).dataframe(filt)[raw_col.split()]
  df.columns = 'ETA', 'PT', 'passing'
  df.PT /= 1e3
  df = df[df.PT > 20] # Need high-PT by default
  #
  if (bin_eta is not None) and (bin_pt is not None): # dual bin
    nume = binning_2D(df[df.passing], ETA=bin_eta, PT=bin_pt)
    deno = binning_2D(df            , ETA=bin_eta, PT=bin_pt)
    eff  = divide_clopper_pearson(nume.fillna(0.), deno.fillna(1.)).unstack()
  elif bin_eta is not None:
    nume = binning_1D(df[df.passing], ETA=bin_eta)
    deno = binning_1D(df            , ETA=bin_eta)
    eff  = divide_clopper_pearson(nume.fillna(0.), deno.fillna(1.))
  elif bin_pt is not None:
    nume = binning_1D(df[df.passing], PT=bin_pt)
    deno = binning_1D(df            , PT=bin_pt)
    eff  = divide_clopper_pearson(nume.fillna(0.), deno.fillna(1.))

  return eff


#===============================================================================
# SIMPLE UTILS
#===============================================================================

def intv_to_latexintv(s, decimals=2):
  """
  For the latex report.
  """
  u = ufloat.from_interval(s)
  return '{1:.{0}f} -- {2:.{0}f}'.format(decimals, u.low, u.high)

def sort_intv_pt(df):
  """
  Sort the given dataframe index by asending interval-string.
  In case of multiindex, pull PT to the outermost index
  """
  sorter = lambda s: float(re.findall(r'\((\S+),',s)[0])
  if isinstance(df.index, pd.MultiIndex):
    if df.index.names[0]=='PT':
      func = lambda t: sorter(t[0])
      return df.reindex_axis(sorted(df.index, key=func))
    else:
      raise NotImplementedError
    # elif df.index.names[1]=='PT':
    #   func = lambda t: sorter(t[1])
    # else:
    #   raise KeyError
  elif isinstance(df, pd.DataFrame):
    return df.reindex_axis(sorted(df.index, key=sorter))[sorted(df.columns, key=sorter)]
  elif isinstance(df, pd.Series):
    return df[sorted(df.index, key=sorter)]
  raise NotImplementedError

def format_eff( w, e ): 
  """
  For simple case of calculating weighted eff, including pretty print.
  Note: the weights can be fraction, or even negative.
  """
  tot = uncertainties.nominal_value(w.sum())
  return '{:.5u}    (w={})'.format((w*e).sum()/tot, tot)

def gen_selections(**kwargs):
  """
  Give the array of bins, return all combination of TTree-compatible
  selection string

  Usage
  >> gen_selections(ETA=[2, 3, 4], PT=[1, 5, 10])
  [
    ('ETA[2,3);PT[1,5)', '(ETA >= 2) & (ETA < 3) & (PT >= 1) & (PT < 5)'),
    ...
  ]
  """
  ## Transform the array of edges into array of bound first
  bounds = [] # fixed order, same as dict's key in alphabetical order
  for param, arr in sorted(kwargs.items()):
    arr = [(arr[i], arr[i+1]) for i in xrange(len(arr)-1)]
    bounds.append(arr)
  ## Making Cartesian product, yield results
  params = sorted(kwargs.keys())
  for pairs in itertools.product(*bounds):
    keys = []
    vals = []
    for param, pair in zip(params, pairs):
      keys.append('%s[%s,%s]'%(param, pair[0], pair[1]))
      vals.append('%s >= %s'%(param, pair[0]))
      vals.append('%s < %s'%(param, pair[1]))
    key = '_'.join(keys)
    val = utils.join(vals)
    yield key, val


def syst_pidcalibstyle(se):
  """
  Return a ufloat multiplier representing the syst, calculate in PIDCalib style
  where 3 efficiencies are given: nominal, double, halved. 
  Their variation are syst.
  """
  diff1 = abs((se['nominal'] - se['halved']).n)
  diff2 = abs((se['nominal'] - se['doubled']).n)
  rerr  = max(diff1, diff2) / se['nominal'].n
  return ufloat(1., rerr)


#===============================================================================
# ALGORITHMIC UTILS
#===============================================================================

def binning_1D( df, **kwargs ):
  assert len(kwargs)==1
  k,v = kwargs.items()[0]
  g1  = pd.cut(df[k], v)
  return df.groupby([g1]).count().iloc[:,0]

def binning_2D( df, **kwargs ):
  """  
  Usage:
  >> binning_2D( df, mu_PT=np.linspace(20e3, 70e3, 6), mu_ETA=...)
  """
  assert len(kwargs)==2
  k1,k2 = sorted(kwargs.keys())  # arbitary-chosen order
  df    = df[[k1,k2]]
  g1    = pd.cut(df[k1], kwargs[k1])
  g2    = pd.cut(df[k2], kwargs[k2])
  return df.groupby([g1,g2]).count()[k1] #.unstack(-1).fillna(0)

def binning_3D( df, **kwargs ):
  """
  Note: Unlike binning_2D which return true 2D dataframe, 
  this will return 1D with multiIndex instead.
  """
  assert len(kwargs)==3
  k1,k2,k3  = sorted(kwargs.keys())  # arbitary-chosen order
  df2       = df[[k1,k2,k3]]
  g1        = pd.cut(df2[k1], kwargs[k1])
  g2        = pd.cut(df2[k2], kwargs[k2])
  g3        = pd.cut(df2[k3], kwargs[k3])
  return df.groupby([g1,g2,g3]).count().iloc[:,0]

def _binning_123D( df, **kwargs ):
  """
  Shorthand to use for generic weighters.
  Not public to let the explicit version be used outside for clarity.
  """
  n = len(kwargs)
  if n==1: return binning_1D( df, **kwargs )
  if n==2: return binning_2D( df, **kwargs )
  if n==3: return binning_3D( df, **kwargs )
  raise ValueError

def divide_clopper_pearson( nume, deno ):
  """
  Given 2 series of ints, also of identical multiindex, 
  return the fraction nume/deno such that the uncertainty is govern by 2-sigma CP
  """

  ## note; fillna later as making DataFrame recreate missing index
  eff = pd.DataFrame({'nume':nume, 'deno':deno})
  eff['nume'] = eff.nume.fillna(0)
  eff['deno'] = eff.deno.fillna(1)
  f_int = lambda x: int(uncertainties.nominal_value(x))

  def divider(se):
    """Guard against nume>deno case. cap maximum instead."""
    n = f_int(se.nume)
    d = f_int(se.deno)
    return EffU(d, n if n<=d else d )

  eff = eff.apply(divider, axis=1)
  eff.name = 'eff'
  return eff


def completely_decorrelate( se ):
  """
  Make sure that all ufloat in se are NOT correlated.
  """
  u = lambda x: ufloat(uncertainties.nominal_value(x), uncertainties.std_dev(x))
  return se.apply(u)

def useries_interpolate(se):
  """
  Custom interpolator to be compat with ufloat, linear in PT.

  Users
  - pid_muon
  - tracking muon

  NOTE: ONLY VALID FOR RANGE OF BIN THAT ARE LINEAR
  If not, it's adviced to do interpolation manually.

  Usage:
  >> df = df.apply(useries_interpolate)

  """
  nom = uncertainties.nominal_value
  std = uncertainties.std_dev

  se2 = pd.Series([ nom(x) for x in se ])
  se3 = se2.interpolate()

  #
  err2 = pd.Series([ (std(x) or np.nan) for x in se ])
  err3 = err2.interpolate()

  acc = []
  for i,(x1,x2,x3,e3) in enumerate(zip(se, se2, se3,err3)):
    if not np.isnan(x2): # not part of the interpolation
      acc.append(x1)
    else:
      if i==0:
        val = np.nan
      elif i+1 < len(se):
        # diff  = abs(nom((se3.iloc[i-1]-se3.iloc[i+1])/2))
        val   = ufloat(x3, e3)
      elif i+1 == len(se): # upper edge: treated as last value scaled (i.e., same rerr)
        # diff = abs(nom(se3.iloc[i-1]-x3))
        diff  = nom(x3) / nom(se3.iloc[i-1]) * std(se.iloc[i-1])
        val   = ufloat(x3, diff)
      acc.append(val)
  se2 = pd.Series(acc)
  se2.index = se.index
  return se2

#===============================================================================

# def lookup_eff_generic( eff0, **binning_kwargs ):
#   """
#   Helper method to return a function helps attaching efficiencies for given binning.
#   Used in the last stage of calculating average of products.
  
#   eff0 is 1D series (with possibly multiindex)

#   Usage:
#   >> lookup_eff_dd = lookup_eff_generic( eff_dd_pt, _PT=BIN_PT, nSPDHits=BIN_SPD )

#   """
#   # assert eff0.name, "Need some name for debugging purpose."

#   def lookup_eff_wrap( newname=None, df=None, fullname=None ):
#     """
#     Usage:
#     >> lookup_eff_wrap( 'etrack_mu', df, 'h1mu_mu' )

#     Then, using fullname to identify the branch name ('mu'), all the binning 
#     kwargs given initially will be binned with respective spec 
#     (e.g., mu_PT, nSPDHits ). These columns must be presented in the dataframe.
#     The efficiency for given bin will be attached to each row of original df.

#     """
#     ## Hack: Return the entire table if everything is None
#     if newname is None and df is None and fullname is None:
#       return eff0

#     ## Actual work
#     eff       = eff0[fullname] if fullname in eff0 else eff0
#     eff.name  = newname
#     prefix    = fullname.split('_')[-1]
#     df2       = pd.DataFrame()
#     for arg,bins in binning_kwargs.iteritems():
#       ## Some argument are particle, some are event. Use prefix '_' to separate
#       arg2     = prefix+arg if arg.startswith('_') else arg
#       key      = arg[1:] if arg.startswith('_') else arg
#       df2[key] = pd.cut( df[arg2], bins )
#     ## Sort columns to match with eff, if multiindex
#     if len(eff.index.names)>1:
#       df2 = df2[eff.index.names]
#     ## Finally, join
#     res = df2.join(eff, on=list(df2.columns))[newname]
#     # df[newname] = res
#     logger.info('Attached: %s'%newname)
#     # also return, this is the new API
#     return res

#   return lookup_eff_wrap


def lookup_eff_generic(_file, eff0, **binning_kwargs):
  """
  Helper method to return a function helps attaching efficiencies for given binning.
  Used in the last stage of calculating average of products.
  
  eff0 is 1D series (with possibly multiindex), 
  May optionally has a "channel_obj" dependency (e.g., tracking_hadron).

  Usage:
  >> lookup_eff_dd = lookup_eff_generic( eff_dd_pt, _PT=BIN_PT, nSPDHits=BIN_SPD )

  """
  ## store script name, useful for debugging
  name = os.path.basename(os.path.splitext(_file)[0])

  def lookup_wrap(dfin=None, dt=None, obj=None):
    """
    Usage:
    >> lookup_wrap( df, 'h1mu', 'mu' )

    Then, using fullname to identify the branch name ('mu'), all the binning 
    kwargs given initially will be binned with respective spec 
    (e.g., mu_PT, nSPDHits ). These columns must be presented in the dataframe.
    The efficiency for given bin will be attached to each row of original df.

    """

    ## Hack: Return the entire table if everything is None. Useful for debugging
    if dfin is None and dt is None and obj is None:
      return eff0.copy() # return a copy to be clean

    ## Fetch channel&object-dependent efficiency map
    tag = dt+'_'+obj
    if tag in eff0:
      logger.info('Resolving channel-dependent: %s'%tag)
      eff = eff0[tag]
    else:
      eff = eff0
    eff.name = name
    
    ## Fetch the subset from given data, apply the binning.
    acc = pd.DataFrame()
    for arg, bins in binning_kwargs.iteritems():
      # Some argument are particle, some are event. Use prefix '_' to separate
      cname = obj+arg if arg.startswith('_') else arg
      key   = arg[1:] if arg.startswith('_') else arg
      acc[key] = pd.cut(dfin[cname], bins)

    ## Sort columns to match with eff, if multiindex
    if len(eff.index.names)>1:
      acc = acc[eff.index.names]

    ## Ready, join & return
    res = acc.join(eff, on=list(acc.columns))[name]
    logger.info('Lookup success [%s]: %s'%(name, tag))
    return res

  return lookup_wrap


#===============================================================================

def dataset_to_TFile(ds, fields):
  """
  Note: May conflicts with PyrootCK
  """
  from rootpy.io import root_open
  from rootpy.tree import Tree # not compat with PyrootCK

  ## manual fill: prep
  fout = root_open("%s.root"%ds.name, "recreate")
  tree = Tree(ds.name)
  tree.create_branches({key:'F' for key in fields})

  ## loop fill
  logger.info('Start filling %s [%i]'%(ds.name, ds.numEntries()))
  for i in xrange(ds.numEntries()):
    if i%10000 == 0:
      print 'Filling', i
    for key in fields:
      setattr(tree, key, ds.get(i).getRealValue(key))
    tree.Fill()

  ## finally
  tree.Write()
  fout.Close()


#===============================================================================

if __name__ == '__main__':
  pass
