#!/usr/bin/env python
"""

Calculate the low-PT electron PID efficiency from PIDCalib sample:

[B+ -> (J/psi(1S) -> e+ e-) K+]cc

"""
from rec_utils import *
from pid_electron_bins import SCHEMES_2D, multiweights_pt_eta

#===============================================================================
# LOW-PT: Use JPSI sample
#===============================================================================

@memorized
def get_tree_lowpt():
  src = '$DIR13/efficiencies/reconstruction/pid_electron_dd/weighed.root'
  return import_tree('electron', src)

# @pickle_dataframe_nondynamic # It's fast enough
@memorized
def eff_2D_sweight():
  """
  Load the SW-weighed tree and compute weighed eff via TProfile2D.
  """
  tree = get_tree_lowpt()
  acc  = {}
  for tag, scheme in SCHEMES_2D.iteritems():
    logger.info('Binning %s'%tag)
    h = ROOT.TProfile2D('htemp', 'htemp', scheme['PT'], scheme['ETA'])
    tree.Draw('passed: ETA: PT >> htemp', 'nsig_sw', 'prof goff')
    acc[tag] = completely_correlate(h.dataframe().unstack())
    h.Delete()
  ## finally, wrap
  df = pd.DataFrame(acc)
  df.index.names = 'PT', 'ETA' 
  return df


#===============================================================================
# HIGH-PT: Use Z0 sample
#===============================================================================

JID = 5105

cut_pid = utils.join(
  '{0}_CaloEcalE/{0}_P > 0.1',
  '{0}_CaloHcalE/{0}_P < 0.05',
  '{0}_CaloHcalE >= 0.',
  '{0}_CaloPrsE > 50',
  '!{0}_ISMUONLOOSE',
).format

filt = utils.join(
  ## evt
  # 'Z0_M > 80e3',
  # 'Z0_M < 100e3',
  'nPVs == 1',
  'Z0_M > 60e3',
  'Z0_DPHI > 2.8',

  ## both
  'tag_ETA          > 2.0',
  'tag_ETA          < 4.5',
  'tag_TRPCHI2      > 0.01',
  'tag_0.50_cc_IT   > 0.9',
  'probe_ETA        > 2.0',
  'probe_ETA        < 4.5',
  'probe_TRPCHI2    > 0.01',
  'probe_0.50_cc_IT > 0.9',

  ## tag
  'tag_TOS_ELEC',
  'tag_PT > 20e3',
  cut_pid('tag'),
)

@memorized
def get_tree_highpt():
  tree  = import_tree('PID/Z02ee', JID)
  tree.SetAlias('PT'    , 'probe_PT/1e3')
  tree.SetAlias('ETA'   , 'probe_ETA')
  tree.SetAlias('passed', cut_pid('probe'))
  tree.ownership = False
  return tree

#-------------------------------------------------------------------------------

# @pickle_dataframe_nondynamic # It's fast enough
@memorized
def eff_eta_pt_highpt():
  tree = get_tree_highpt()
  ## Loop over established schema
  acc = {}
  for tag, scheme in SCHEMES_2D.iteritems():
    logger.info('Binning %s'%tag)
    h = ROOT.TProfile2D('htemp', 'htemp', scheme['PT'], scheme['ETA'])
    tree.Draw('passed: ETA: PT >> htemp', filt, 'prof goff')
    acc[tag] = completely_correlate(h.dataframe().unstack())
    h.Delete()
  ## finally, wrap
  df = pd.DataFrame(acc)
  df.index.names = 'PT', 'ETA' 
  return df

#===============================================================================

@pickle_dataframe_nondynamic
def eff_eta_pt_combined():
  """
  Combine low-PT and high-PT together
  """
  acc = {}
  for schema in SCHEMES_2D:
    eff_low = sort_intv_pt(eff_2D_sweight()[schema].dropna().unstack())
    eff_hi  = sort_intv_pt(eff_eta_pt_highpt()[schema].dropna().unstack())

    ## the threshold is at PT=20GeV
    eff = pd.concat([eff_low.iloc[:2], eff_hi.iloc[2:]])

    ## Overlap the joint with BLUE
    eff.iloc[1] = pd.concat([eff_low.iloc[1], eff_hi.iloc[1]], axis=1).apply(combine_uncorrelated, axis=1)

    ## Force bad low (negative by sWeight) to zero
    eff = eff.applymap(lambda u: u if u.n>0 else ufloat(0., 1e-6))
    acc[schema] = eff.unstack()

  ## finally, pack & return
  return pd.concat(acc)

#===============================================================================

@memorized
def scalar_results():
  """
  Return the final ePID for each scheme and each taue probe.
  """
  all_eff     = eff_eta_pt_combined()
  all_weights = multiweights_pt_eta()
  acc = pd.DataFrame()
  for obj in ['ee_e1', 'ee_e2', 'eh1_e', 'eh3_e', 'emu_e']:
    for tag, weights in all_weights.iteritems():
      w = weights.unstack()[obj].dropna().unstack().T.stack()
      e = all_eff.loc[tag]
      acc.loc[tag, obj] = (w*e).sum()/w.sum()
  return acc

@memorized
def syst():
  """
  Return channel-dependent syst multiplication factor
  """
  return scalar_results().apply(syst_pidcalibstyle)


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_probe_occupancy():
  ## Start
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors

  ## Prep
  xarr = pd.np.geomspace(4.9, 70, 100)
  yarr = pd.np.geomspace(2, 4.5, 100)
  c = ROOT.TCanvas()
  h = ROOT.TH2F('htemp', 'htemp', xarr, yarr)
  c.ownership = False
  h.ownership = False

  ## High-PT
  tree = get_tree_highpt()
  tree.Draw('ETA: PT >> htemp', filt, 'goff')

  ## Low-PT
  tree = get_tree_lowpt()
  tree.Draw('ETA: PT >>+ htemp', 'nsig_sw', 'goff')

  ## Finally
  h.Draw('colz')
  h.xaxis.title = 'p_{T} [GeV]'
  h.yaxis.title = '#eta'
  h.xaxis.moreLogLabels = True
  h.xaxis.labelOffset   = -0.01
  h.xaxis.titleOffset   = 0.95
  ROOT.gPad.logx = True
  ROOT.gPad.logz = True
  ROOT.gPad.rightMargin  = 0.12
  ROOT.gPad.bottomMargin = 0.16
  ROOT.gPad.topMargin    = 0.03

  ## Draw nominal bins grid
  drawers.occupancy_grid(h, **SCHEMES_2D['nominal'])

#===============================================================================

def lookup(*args):
  eff     = eff_eta_pt_combined().loc['nominal']
  bin_eta = SCHEMES_2D['nominal']['ETA']
  bin_pt  = SCHEMES_2D['nominal']['PT']
  return lookup_eff_generic(__file__, eff, _PT=bin_pt, _ETA=bin_eta)(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ## Low-PT
  # print eff_2D_sweight()
  # print sort_intv_pt(eff_2D_sweight()['nominal'].dropna().unstack()).fmt2p
  # print sort_intv_pt(eff_2D_sweight()['doubled'].dropna().unstack()).T.fmt2p

  ## High-PT
  # print eff_eta_pt_highpt()
  # print sort_intv_pt(eff_eta_pt_highpt()['nominal'].dropna().unstack()).fmt2p

  ## Combine
  # print eff_eta_pt_combined()
  # print eff_eta_pt_combined().loc['nominal'].stack().dropna().unstack().fmt2p

  ## Scalar result & syst
  # print scalar_results()
  # print syst()

  ## Draw
  draw_probe_occupancy()

  ## Note to self 170328
  # - Jpsi->ee stats is too low, as well as difficult to handle the background
  # - The eff is not important for electron. We'll use the one from simulation.
