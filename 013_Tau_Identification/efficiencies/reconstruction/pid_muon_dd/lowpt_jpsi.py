#!/usr/bin/env python

from PyrootCK import *
PATH_EREC = os.path.expandvars('$DIR13/efficiencies/reconstruction')
sys.path.append(PATH_EREC)
from rec_utils import *
from pid_muon_bins import *

#===============================================================================

def inflate(df0, index_old, index_new):
  """
  Helper method to inflate the index, from coarse to finer one.
  (each large bin is broken down to smaller ones, where they all have the same
  value as their parent).
  The finer one should be a strict subset of coarse one, i.e., any union of 
  fine-bins must always be inside some unique coarse-bin.
  """
  idx = pd.cut(index_new, index_old)[1:]
  df  = pd.DataFrame({'temp':idx}).join(df0, on='temp').drop('temp', axis=1) # preserve non-unique-index order
  df.index = pd.cut([], index_new).categories # important that above is sorted
  df.index.name = df0.index.name 
  df.columns.name = df0.columns.name
  return df

#===============================================================================

@memorized
def get_tree():
  """
  Load the weighed tree from the fit.
  """
  src = os.path.join(PATH_EREC, 'pid_muon_dd/weighed.root')
  return import_tree('muon', src)

@memorized
def efficiencies_all():
  """
  Load the SW-weighed tree and compute weighed eff via TProfile2D.
  """
  tree = get_tree()
  acc  = {}
  for tag, scheme in REGIMES.iteritems():
    logger.info('Binning %s'%tag)
    h = ROOT.TProfile2D('htemp', 'htemp', scheme['PT'], scheme['ETA'])
    tree.Draw('passed: ETA: PT >> htemp', 'nsig_sw', 'prof goff')
    acc[tag] = completely_correlate(h.dataframe().unstack())
    h.Delete()
  ## finally, wrap
  df = pd.DataFrame(acc)
  df.index.names = 'PT', 'ETA' 
  return df

@memorized
def efficiencies():
  """
  Parse into nominal-ready schema.
  - Contain PT [5, 25]
  - [5,7,10,15] use nominal (better resolution)
  - [10,20,30] use etakew (compat with sweight)
  """
  ## join super-low and low together
  eff  = efficiencies_all()
  eff1 = eff['nominal'].dropna().unstack().loc[['(5, 7]', '(7, 10]', '(10, 15]']]
  eff2 = eff['etaskew'].dropna().unstack().loc[['(10, 20]', '(20, 30]']]
  eff2 = inflate(eff2.T, BIN_ETA_SKEW, BIN_ETA_NOMINAL).T
  eff2 = inflate(eff2  , [10,20,30], [15,20,25,30])
  eff  = pd.concat([eff1, eff2])
  ## one bin is particularly bad (low stat), fix it
  e = efficiencies_all()['patchEta354'].dropna().iloc[0]
  eff.loc['(20, 25]', '(3.5, 3.75]'] = e
  eff.loc['(20, 25]', '(3.75, 4]'] = e
  ## finally
  return eff

#===============================================================================

@memorized
def scalar_results():
  """
  For syst, filter to do in low-PT only. Choose emu_mu for better stats
  """
  idx = list(pd.cut([], BIN_PT_NOMINAL_LOW).categories)
  e   = efficiencies_all().loc[idx]
  acc = {}
  for obj in ['mumu_mu2', 'emu_mu']:
    w   = multiweights_pt_eta().xs(obj, level=2).loc[idx]
    eff = (w*e).sum()/w.sum()
    acc[obj] = eff[['nominal', 'doubled', 'halved']]
  return pd.concat(acc)


#===============================================================================

if __name__ == '__main__':
  pass

  # print efficiencies().fmt2p
  # print efficiencies_all()['halved'].dropna().unstack().fmt2p.T
  # print efficiencies_all()['doubled'].dropna().unstack().fmt2p.T
  print scalar_results().fmt2p


  # df2 = inflate(df, BIN_ETA_SKEW, BIN_ETA_NOMINAL)
  # print inflate(df2.T, BIN_PT_LOW, BIN_PT_NOMINAL[:-1])
