#!/usr/bin/env python

from PyrootCK import *
sys.path.append('..') # drawer is always stand-alone
from rec_utils import *
from pid_muon_bins import REGIMES
ROOT.RooFit # trigger patch
ROOT.gROOT.batch = True

#===============================================================================

@gpad_save(subdir=False)
def draw_probe_occupancy():
  ## Start
  import lowpt_jpsi
  import highpt_zmumu
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors

  ## Prep
  xarr = pd.np.geomspace(4.9, 70, 100)
  yarr = pd.np.geomspace(2, 4.5, 100)
  c = ROOT.TCanvas()
  h = ROOT.TH2F('htemp', 'htemp', xarr, yarr)
  c.ownership = False
  h.ownership = False

  ## High-PT
  tree = highpt_zmumu.get_tree()
  tree.Draw('ETA: PT >> htemp', 'weight', 'goff')

  ## Low-PT
  tree = lowpt_jpsi.get_tree()
  tree.Draw('ETA: PT >>+ htemp', 'L_nsig', 'goff')

  ## Finally
  h.Draw('colz')
  h.xaxis.title = 'p_{T} [GeV]'
  h.yaxis.title = '#eta'
  h.xaxis.moreLogLabels = True
  h.xaxis.labelOffset   = -0.01
  h.xaxis.titleOffset   = 0.95
  ROOT.gPad.logx = True
  ROOT.gPad.logz = True
  ROOT.gPad.rightMargin  = 0.12
  ROOT.gPad.bottomMargin = 0.16
  ROOT.gPad.topMargin    = 0.03

  ## Draw nominal bins grid
  drawers.occupancy_grid(h, **REGIMES['nominal'])

#===============================================================================

def draw_contrib():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Load
  fin = ROOT.TFile('fit.root')
  w   = fin['muon']

  ## Pre-draw prep
  poi = w.var('jpsi_M')
  poi.title = 'mass(J/#psi)'
  poi.unit  = 'MeV/c^{2}'

  ## Get frame & draw all
  frame = poi.frame()
  data = w.data('dataset_with_weights')
  data.plotOn(frame, name='data', markerSize=0.2)
  #
  nmz   = (1.0, ROOT.RooAbsReal.RelativeExpected)
  model = w.pdf('model')
  model.plotOn(frame, name='model'                     , lineColor=ROOT.kGreen, normalization=nmz)
  model.plotOn(frame, name='sig', components='sigModel', lineColor=ROOT.kBlack, normalization=nmz)
  model.plotOn(frame, name='bkg', components='bkgModel', lineColor=ROOT.kRed  , normalization=nmz)

  ## Tune
  frame.drawAfter('model', 'data')
  frame.Draw()

  ## Legends
  leg = ROOT.TLegend(0.15, 0.55, 0.65, 0.92, 'LHCb preliminary', 'NDC')
  leg.AddEntry(data                         , 'Observed'  , 'PE')
  leg.AddEntry(ROOT.gPad.FindObject('model'), 'Model'     , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('sig')  , 'Signal'    , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('bkg')  , 'Background', 'l')
  leg.fillColorAlpha = 1, 0.
  leg.Draw()

  ## Finally
  ROOT.gPad.SaveAs('contrib.pdf')

#===============================================================================

def draw_sweights():
  """
  Stacked TProfile of sWeights as a function of 2 POIs
  """
  ## Harsher contrast
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.markerSize = 0.

  ## Start
  tree    = import_tree('muon', 'weighed.root')
  tags    = 'nsig_sw', 'nbkg_sw'
  legends = 'Signal', 'Background'

  QHist.reset()
  QHist.trees   = tree
  QHist.ymin    = -1.2
  QHist.ymax    = 3.5
  QHist.options = 'prof'
  QHist.legends = legends
  QHist.st_line = False

  h = QHist()
  h.name    = 'sweights'
  h.params  = [x+':jpsi_M' for x in tags]
  h.xmin    = 2986
  h.xmax    = 3206
  h.xbin    = 120
  h.xlabel  = 'mass(J/#psi) (MeV/c^{2})'
  h.draw()

#===============================================================================

if __name__ == '__main__':
  pass

  draw_probe_occupancy()
  # draw_contrib()
  # draw_sweights()
