#!/usr/bin/env python

from PyrootCK import *
PATH_EREC = os.path.expandvars('$DIR13/efficiencies/reconstruction')
sys.path.append(PATH_EREC)
from rec_utils import *
from pid_muon_bins import REGIMES

#===============================================================================
# DD - HIGH PT (redo Z02mumu)
# reasoning: The tracking cut in my analysis is tighter that what used in sfarry
#===============================================================================

def get_tree():
  """
  Use as dummy in probe occupancy plot
  """
  # JIDS = 5024, # partial, missing ETA
  JIDS = 5027, 5028, 5029, 5036

  filt = utils.join(
    'Z0_M   > 80e3',
    'Z0_M   < 100e3',
    'Z0_VCHI2PDOF < 9',
    #
    'tag_ISMUON',
    'tag_TOS_MUON',
    'tag_PT         > 20e3',
    'tag_TRPCHI2    > 0.01',
    'tag_0.50_cc_IT > 0.9',
    #
    # 'probe_PT         > 20e3',
    'probe_TRPCHI2    > 0.01',
    'probe_0.50_cc_IT > 0.9',
    #
    'TMath::Abs(Z0_DPHI12) > 2.7',
    'TMath::Abs(Z0_DPHI12) < 3.1',
  )
  tree = import_tree( 'PID/Z02MuMu', *JIDS )
  tree.SetAlias('PT'    , 'probe_PT/1e3')
  tree.SetAlias('ETA'   , 'probe_ETA')
  tree.SetAlias('passed', 'probe_ISMUON')
  tree.SetAlias('weight', filt)
  return tree

# @auto_draw
# @pickle_dataframe_nondynamic
def eff_eta_pt_highpt_z02mumu():
  """
  Use tag-and-probe to compute eTrig muon.
  """
  tree = get_tree()

  ## Loop over established schema
  acc = {}
  for tag, scheme in REGIMES.iteritems():
    logger.info('Binning %s'%tag)
    h = ROOT.TProfile2D('htemp', 'htemp', scheme['PT'], scheme['ETA'])
    tree.Draw('passed: ETA: PT >> htemp', 'weight', 'prof goff')
    acc[tag] = completely_correlate(h.dataframe().unstack())
    h.Delete()
  ## finally, wrap
  df = pd.DataFrame(acc)
  df.index.names = 'PT', 'ETA' 
  return df

#===============================================================================

if __name__ == '__main__':
  print sort_intv_pt(eff_eta_pt_highpt_z02mumu()['nominal'].dropna().unstack()).fmt2p

