#!/usr/bin/env python

from PyrootCK import *
import subprocess

#===============================================================================

filt = utils.join(
  ## event
  # 'nSPDhits <= 600',
  # {'runNumber < 111802', '111890 < runNumber'},
  # {'runNumber < 126124', '126160 < runNumber'},
  # {'runNumber < 129530', '129539 < runNumber'},

  ## tracks
  'probe_TRPCHI2 > 0.01',
)

def main_lowpt():
  exe  = lambda sign: ROOT.TFile.slice_tree(
    src       = 4154,
    tname     = 'tup_PID/jpsi_filt_probe_%s'%sign,
    aliases   = {'passed': 'probe_ISMUON', 'PT': 'probe_PT/1e3', 'ETA': 'probe_ETA'},
    params    = ['passed', 'nTracks', 'jpsi_M', 'PT', 'ETA'],
    selection = filt,
    dest      = '%s.root'%sign
  )
  exe('plus')
  exe('minus')
  ## hadd them together
  args = 'hadd', 'extracted.root', 'plus.root', 'minus.root'
  print subprocess.check_output(args)
  os.remove(args[2])
  os.remove(args[3])

#===============================================================================

filt_highpt = utils.join(
  # 'Z0_DPHI12    > 2.7',
  # 'Z0_VCHI2PDOF < 9',
  #
  'tag_ISMUON',
  'tag_TOS_MUON',
  'probe_TRPCHI2 > 0.01',
  'probe_PT      > 5e3',
)

def main_highpt():
  """
  Preparation before fitting
  """
  ROOT.TFile.slice_tree(
    src       = [5027, 5028, 5029, 5036],
    tname     = 'PID/Z02MuMu',
    aliases   = {'passed': 'probe_ISMUON', 'PT': 'probe_PT/1e3', 'ETA': 'probe_ETA', 'M': 'Z0_M/1e3'},
    params    = ['passed', 'nTracks', 'M', 'PT', 'ETA'],
    selection = filt_highpt,
    dest      = 'extracted_highpt.root'
  )

#===============================================================================

if __name__ == '__main__':
  # main_lowpt()
  main_highpt()
