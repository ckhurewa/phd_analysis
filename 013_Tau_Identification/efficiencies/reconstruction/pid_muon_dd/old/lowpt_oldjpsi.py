#!/usr/bin/env python

#===============================================================================
# DD - LOW PT
#===============================================================================

JIDS_jpsi = 4154, #

filt_jpsi = utils.join(
  'jpsi_VCHI2PDOF < 5',
  'jpsi_M   > 3000',
  'jpsi_M   < 3200',
  #
  'tag_PT       >  800',
  'tag_P        > 3000',
  'tag_ETA      > 2.0',
  'tag_ETA      < 4.5',
  'tag_TRPCHI2  > 0.01',
  #
  'probe_PT       > 1500',
  'probe_P        > 6000',
  'probe_ETA      > 2.0',
  'probe_ETA      < 4.5',
  'probe_TRPCHI2  > 0.01',

  ## unused
  # 'TMath::Abs(DPHI) > 0.1',
  # 'PT     > 1e3',
  # 'tag_ISMUON',
  # 'tag_TRPCHI2 > 0.01',
  # 'probe_PT > 0',
)

filt_passing = 'probe_ISMUON'

#-------------------------------------------------------------------------------

def calc_lowpt_via_jpsi_fitting():
  from loop_jpsi_roofit import calc_cb_binning_all, pre_binning
  ROOT.gROOT.SetBatch(True)

  ## Prepare incoming tree
  t1 = import_tree('tup_PID/jpsi_filt_probe_minus', *JIDS_jpsi )
  t2 = import_tree('tup_PID/jpsi_filt_probe_plus' , *JIDS_jpsi )
  t  = ROOT.TChain()
  t.Add(t1)
  t.Add(t2)

  t.SetAlias('PREBIN_MASS', 'jpsi_M/1e3'  )
  t.SetAlias('PREBIN_ETA' , 'probe_ETA'   )
  t.SetAlias('PREBIN_PT'  , 'probe_PT/1e3')
  t.SetAlias('PREBIN_FILT', filt_jpsi     )
  t.SetAlias('PREBIN_PASS', filt_passing  )

  ## Invoke function
  calc_cb_binning_all( t, BIN_ETA_LOW10, BIN_PT_LOW, mass_min=3.0, mass_max=3.2 )

# @auto_draw
def eff_eta5_pt_lowpt():
  from loop_jpsi_roofit import read_cb_result
  return read_cb_result( BIN_ETA_LOW5, BIN_PT_LOW, dname='fitting_jpsi_eta5' ).T

# @auto_draw
def eff_eta10_pt_lowpt():
  from loop_jpsi_roofit import read_cb_result
  return read_cb_result( BIN_ETA_LOW10, BIN_PT_LOW, dname='fitting_jpsi_eta10' ).T

#===============================================================================

  ## DD - LOW
  # calc_lowpt_via_jpsi_fitting()
  # print eff_eta5_pt_lowpt()
  # print eff_eta10_pt_lowpt()

