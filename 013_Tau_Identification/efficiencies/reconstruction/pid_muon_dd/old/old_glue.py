#!/usr/bin/env python

"""

Old high+low glue method, now superceded by sWeight & BLUE.

"""

# @auto_draw
@pickle_dataframe_nondynamic
def eff_dd_eta10_pt():
  """
  Another regime with 10 bins in ETA.
  """
  ## Tools
  def cleaner_low(x):
    if x.s/x.n > 0.02: # tolrelate up to 2%
      return np.nan
    return x
  def cleaner_high(x):
    if x.n<0.1:
      return np.nan
    if (x.n>0) and (x.s/x.n > 0.1):
      return np.nan
    return x

  ## Load result
  df_low  = eff_eta10_pt_lowpt().applymap(cleaner_low)
  df_high = eff_eta_pt_highpt_sfarry_etaW().applymap(cleaner_high).drop('(20, 25]')

  ## Veto bad ones
  df_low.loc['(10, 15]','(4, 4.25]']    = np.nan
  df_low.loc['(15, 20]','(2.75, 3]']    = np.nan
  df_low.loc['(15, 20]','(3, 3.25]']    = np.nan
  df_low.loc['(15, 20]','(3.25, 3.5]']  = np.nan
  df_low.loc['(20, 25]','(2.5, 2.75]']  = np.nan
  df_low.loc['(20, 25]','(3.25, 3.5]']  = np.nan
  df_low.loc['(20, 25]','(3.5, 3.75]']  = np.nan
  df_high.loc['(25, 30]']               = np.nan # clean the joint, usually bad
  df_high.loc['(30, 35]','(3, 3.25]']   = np.nan
  df_high.loc['(30, 35]','(3.25, 3.5]'] = np.nan
  df_high.loc['(35, 40]','(3, 3.25]']   = np.nan

  df = sort_intv_pt(pd.concat([df_low, df_high]))
  print 'After veto, before interpolate'
  print df

  df2 = df.apply(useries_interpolate)
  df2.index = df.index 
  return df2


#-------------------------------------------------------------------------------

def eff_dd_pt():
  """
  FOR VISUALIZATION ONLY! (compare DD-MC as a func of PT)
  """
  eff     = eff_dd_eta10_pt()
  weights = weight_eta10_ptW()
  def avg(df):
    pt = df.name
    w  = df.loc[pt]
    e  = eff.loc[pt]
    return w.apply(lambda se: (se*e).sum()/(se.sum() or 1.))
  df = weights.groupby(level='PT').apply(avg)

  ## forward-fill some rare case: superhigh-PT
  df.mumu_mu1.replace(0., np.nan, inplace=True)
  df.h1mu_mu.replace (0., np.nan, inplace=True)
  df.h3mu_mu.replace (0., np.nan, inplace=True)
  df.mumu_mu1.fillna(method='ffill', inplace=True)
  df.h1mu_mu.fillna (method='ffill', inplace=True)
  df.h3mu_mu.fillna (method='ffill', inplace=True)

  ## Provide the unweighted version, out of curiosity.
  df['unweighted'] = eff.apply(lambda se: se.sum()/len(se), axis=1)
  return df

def eff_dd_eta():
  eff     = eff_dd_eta10_pt().T
  weights = weight_eta10_ptW().swaplevel()
  def avg(df):
    pt = df.name
    w  = df.loc[pt]
    e  = eff.loc[pt]
    return w.apply(lambda se: (se*e).sum()/(se.sum() or 1.))
  df = weights.groupby(level='ETA').apply(avg)
  return df


@gpad_save
def draw_eff_dd_pt():
  eff = eff_dd_pt()[['unweighted', 'mumu_mu1', 'mumu_mu2', 'emu_mu']]
  gr,leg = drawer_graphs_eff_pt( eff )
  gr.minimum = 0.90
  gr.maximum = 1.05
  gPad.Update()
