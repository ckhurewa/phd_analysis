#!/usr/bin/env python

from PyrootCK import *
sys.path.append('..')
from rec_utils import *

## Fit output file
FIT_OUTPUT = 'fit_highpt.root'

#===============================================================================

def load_data(w):
  ## POI
  w.factory('M       [60  , 120 ]')
  w.factory('PT      [5   , 300 ]')
  w.factory('ETA     [2.0 , 4.5 ]')
  w.factory('nTracks [0   , 600 ]')
  w.factory('passed  [0, 1]')
  w.defineSet('params', 'M,PT,ETA,nTracks,passed')
  w.defineSet('POIs'  , 'M')
  poi = w.set('POIs').first()
  poi.title = 'mass(Z)'
  poi.unit  = 'GeV/c^{2}'

  ## Import data
  tree = import_tree('Z02MuMu', 'extracted_highpt.root')
  ds   = ROOT.RooDataSet('dataset', 'dataset', tree, w.set('params'))
  dh   = ROOT.RooDataHist('datahist', 'datahist', w.set('POIs'), ds)
  return ds, dh

#===============================================================================

def prepare_model(w, nentries):
  ## Obsv, help the starting point
  w.factory('nsig [%.2f, 0, %i]'%(0.8*nentries, nentries ))
  w.factory('nbkg [%.2f, 0, %i]'%(0.2*nentries, nentries ))

  ## Signal
  w.factory('sig_mu    [91. , 89. , 93. ]')
  w.factory('sig_sigma [3.  , 0.  , 5.  ]')
  w.factory('sig_alpha [1.  , 0.  , 5.  ]')
  w.factory('sig_n     [5.  , 1.  , 10. ]')
  # w.factory('Gaussian: sigModel(M, sig_mu, sig_sigma)')
  w.factory('RooVoigtian: sigModel(M, sig_mu, sig_sigma, sig_n)')
  # w.factory('CBShape : sigModel(M, sig_mu, sig_sigma, sig_alpha, sig_n)')

  ## Background
  w.factory('bkg_decay [-0.1, -1., 0.]')
  w.factory('Exponential: bkgModel(M, bkg_decay)')
  # w.factory('bkg_linear [0, -10, +10]')
  # w.factory('Polynomial: bkgModel(M, bkg_linear)')

  ## Finally
  w.factory('SUM: model(nsig*sigModel, nbkg*bkgModel)')

#===============================================================================

def execute_fit(w, ds, dh):
  ## Fit to model
  model = w.pdf('model')
  res   = model.fitTo(dh, sumW2Error=False, save=True, printLevel=1)  
  w.Import(res)

  # ## For debugging
  # ## Get frame & draw all
  # frame = w.set('POIs').first().frame()
  # dh.plotOn(frame, name='data', markerSize=0.2)
  # nmz   = (1.0, ROOT.RooAbsReal.RelativeExpected)
  # model.plotOn(frame, name='model'                     , lineColor=ROOT.kGreen, normalization=nmz)
  # model.plotOn(frame, name='sig', components='sigModel', lineColor=ROOT.kBlack, normalization=nmz)
  # model.plotOn(frame, name='bkg', components='bkgModel', lineColor=ROOT.kRed  , normalization=nmz)
  # frame.drawAfter('model', 'data')
  # frame.Draw()
  # exit()

  ## sWeight
  splot = ROOT.RooStats.SPlot("sPlot", "An sPlot", ds, model, model.coefList())
  rds_withWeights = splot.GetSDataSet()
  rds_withWeights.name = 'dataset_with_weights'
  w.Import(rds_withWeights)

#===============================================================================

def main_fit():
  # Create new Workspace
  w = ROOT.RooWorkspace('muon')

  ## Load data
  ds, dh = load_data(w)
  n = ds.numEntries()

  ## Prepare model
  prepare_model(w, n)
  w.Print()
  w.PrintVars()

  ## Start fit
  fout = ROOT.TFile(FIT_OUTPUT, 'recreate')
  execute_fit(w, ds, dh)

  ## Finally, save
  fout.cd()
  w.Write()
  fout.Close()

#===============================================================================

def main_export_tree():
  """
  Load the new dataset and convert to TTree instead.
  """
  import ROOT
  fin = ROOT.TFile(FIT_OUTPUT)
  w  = fin.Get('muon')
  ds = w.data('dataset_with_weights')
  ds.name = 'muon'

  ## It'll be nice to have this dynamically
  fields1 = 'M', 'PT', 'ETA', 'nTracks', 'passed'
  fields2 = 'nsig_sw', 'L_nsig', 'nbkg_sw', 'L_nbkg'
  fields  = fields1 + fields2

  ## Ready, fire
  dataset_to_TFile(ds, fields)

#===============================================================================

if __name__ == '__main__':
  # main_fit()
  main_export_tree()
