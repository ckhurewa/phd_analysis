#!/usr/bin/env python

from PyrootCK import *
PATH_EREC = os.path.expandvars('$DIR13/efficiencies/reconstruction')
sys.path.append(PATH_EREC)
from rec_utils import *

#===============================================================================
# DD - HIGH PT (sfarry)
#===============================================================================

def _eff_eta_pt_highpt_sfarry_raw(fname):
  """
  Retrieve the DD high-PT region from sfarry, sanitize to compat format.
  Also collapse [50,70]
  """
  fin = ROOT.TFile(os.path.expandvars(fname))
  df  = fin.Get('ETA_PT/EffGraph2D').dataframe()
  fin.Close()

  ## Sanitize
  df.index = [ x.replace('000','') for x in df.index ]
  df.columns.name = 'ETA'
  df.index.name   = 'PT'

  ## Collapse large bin together
  idx = ['(50, 55]', '(55, 60]', '(60, 65]', '(65, 70]']
  def collapse(se):
    # filter zero out first
    arr = [x for x in se[idx] if x.n!=0]
    return combine_uncorrelated(arr)
  df.loc['(50, 70]'] = df.apply(collapse)
  df = df.drop(idx)
  return df

# @auto_draw
def eff_eta_pt_highpt_sfarry():
  """
  Note: May not be compat due to conditional tracking criteria.
  """
  fname = '$DIR13/efficiencies/sfarry/Efficiencies/MuonID2012.root'
  return _eff_eta_pt_highpt_sfarry_raw(fname)


def eff_eta_pt_highpt_sfarry_etaW():
  """
  Use the one from W analysis, more compat track cut
  """
  fname = '$DIR13/efficiencies/sfarry/Efficiencies/WEffs/2012/MuonWID2012.root'
  return _eff_eta_pt_highpt_sfarry_raw(fname)

#===============================================================================

@memorized
def efficiencies():
  """
  Make it compat with nominal here.
  """
  eff = eff_eta_pt_highpt_sfarry_etaW()
  eff.loc['(70, 300]'] = eff.iloc[-1]
  return eff

#===============================================================================

if __name__ == '__main__':
  pass

  # print eff_eta_pt_highpt_sfarry()
  # print eff_eta_pt_highpt_sfarry_etaW().fmt2p
  # draw_eff_eta_pt_highpt_sfarry()
  print efficiencies().fmt2p
