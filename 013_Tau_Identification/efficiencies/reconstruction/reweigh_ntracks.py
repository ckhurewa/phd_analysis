#!/usr/bin/env python
"""

Module to handle the reweighing by nTracks

"""

from rec_utils import *

#===============================================================================
# DRAWER
#===============================================================================

def draw_ntracks_data_mc(dt):
  """
  Note: This will NOT be necessary for the nTracks reweighting computation
  """
  ## Start
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Establish histograms
  t_ztautau = id_utils.load_selected_trees_dict(dt)['t_ztautau']
  t_real    = id_utils.load_selected_trees_dict(dt)['t_real']
  xbin = 20 if t_real.entries > 400 else 10   # larger stat grants larger bins
  arr  = list(pd.np.geomspace(10, 500, xbin))
  h    = ROOT.TH1F('htemp', 'htemp', arr)

  t_ztautau.Draw('nTracks >> htemp', '', 'goff')
  h_mc = h.Clone('h_mc')
  h_mc.Scale(1./h_mc.Integral())
  h_mc.title = 'Simulation'

  t_real.Draw('nTracks >> htemp', '', 'goff')
  h_data = h.Clone('h_data')
  h_data.Scale(1./h_data.Integral())
  h_data.title = 'Data'
  h_data.xaxis.title = 'nTracks'

  ## Decorate
  h_data.markerSize     = 1.0
  h_data.markerStyle    = 20
  h_data.fillColorAlpha = 1, 0.
  h_mc.markerSize       = 0
  h_mc.fillStyle        = 3254
  h_mc.fillColorAlpha   = ROOT.kRed, 0.8

  ## Use the new RatioPlot
  c = ROOT.TCanvas()
  rp = ROOT.TRatioPlot(h_data, h_mc, 'divsym')
  rp.ownership = False
  rp.separationMargin = 0.01
  rp.h1DrawOpt = 'PE'
  rp.h2DrawOpt = 'hist'
  rp.Draw()

  ## Legends
  leg = rp.upperPad.BuildLegend()
  leg.ConvertNDCtoPad()
  leg.header    = 'LHCb preliminary'
  leg.fillStyle = 0
  leg.x1NDC     = 0.12
  leg.x2NDC     = 0.60
  leg.y1NDC     = 0.65
  leg.y2NDC     = 0.94

  ## Workaround for splitFraction (not working)
  frac = 0.4
  rp.upperPad.pad = 1., frac, 0., 1.-frac
  rp.lowerPad.pad = 0., 0., 1., frac

  ## fine-tune
  rp.lowerRefGraph.xaxis.moreLogLabels = True
  rp.lowerRefGraph.xaxis.noExponent = True
  rp.upperRefObject.minimum = 0.
  rp.lowerPad.logx   = True
  rp.lowBottomMargin = 0.3
  rp.upTopMargin     = 0.02
  rp.lowBottomMargin = 0.40
  rp.rightMargin     = 0.03


  ## Manually save both figure & root file
  c.SaveAs('trk_correction/ntracks_ddmc_%s.pdf'%dt)
  fout = ROOT.TFile(NTRACK_FILE, 'update')
  c.Write(dt, ROOT.TObject.kOverwrite)
  fout.Close()

def draw_ntracks_dddmc_all():
  """
  To compare nTracks of data and Ztautau.
  """
  for dt in CHANNELS:
    draw_ntracks_data_mc(dt)

#===============================================================================

@memorized
def get_ntracks_reweighter_ddmc():
  """
  Fetch the histogram for vlookup of weight.
  Only usable for Ztautau, for debugging purpose only
  """
  fin = ROOT.TFile(NTRACK_FILE)
  fin.ownership = False
  acc = {}
  for dt in CHANNELS:
    upad = fin.Get(dt).FindObject('upper_pad')
    h_data = upad.FindObject('h_data')
    h_mc   = upad.FindObject('h_mc')
    h_data.Divide(h_mc)
    weight = h_data.Clone('ntracks_weights')
    ## make sure the weight is non-zero for missing one
    for i in xrange(weight.nbinsX):
      cont = weight.GetBinContent(i+1)
      if cont == 0.:
        weight.SetBinContent(i+1, 1.)
    acc[dt] = weight
  return acc

#===============================================================================

@memorized
def get_ntracks_data(dt):
  df = get_selected_data(dt)
  return df.nTracks


#===============================================================================
# EXTERNAL
#===============================================================================

def vlookup(se, dt):
  """
  Given the array of nTracks (from MC process), return the array of same length
  containing DD/MC ratio to be used as event-level weight.
  Dynamically adjust the number of bins to the amount of input MC.
  """
  ## wrap for dev
  if isinstance(se, list):
    se = pd.Series(se)
    se.name = 'nTracks'

  df_data = get_ntracks_data(dt)
  n_data  = len(df_data.index)
  n_mc    = len(se.index)
  nbins   = 20 if min(n_data, n_mc) > 400 else 10
  arr     = [1] + list(pd.np.geomspace(15, 600, nbins))
  g_mc    = pd.cut(se, arr)
  g_data  = pd.cut(df_data, arr)
  se_mc   = se.groupby(g_mc).count() / n_mc
  se_data = df_data.groupby(g_data).count() / n_data
  se_data = se_data.replace(0, pd.np.nan) # to allow forward fill of high-Tracks
  weights = (se_data/se_mc).replace(pd.np.inf, pd.np.nan)
  weights = weights.fillna(method='ffill').fillna(1.) # forward fill first, then fallback to 1.
  assert not weights.isnull().values.any(), 'Has null values, check me.\n%s'%weights.to_string()
  
  ## finally, vlookup to request array
  res = pd.DataFrame({'nTracks':g_mc}).join(weights, on='nTracks', rsuffix='rsf')
  return res['nTracksrsf']

def lookup(df, dt):
  """
  Need the df with column 'nTracks'. Hide the explicit dependency.
  """
  return vlookup(df.nTracks, dt)

#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print get_ntracks_data('ee')
  # dt = 'ee'
  # print vlookup([400.], dt)

  # df = get_selected_ztautau(dt)
  # # print df
  # print attach(df, dt)

