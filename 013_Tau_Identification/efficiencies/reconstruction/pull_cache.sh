rsync -rav --dry-run --update --include=*/ --include='*/*.df' --exclude=* lpheb:~/analysis/013_Tau_Identification/efficiencies/reconstruction/ .

## large ROOT file
# scp lpheb:~/analysis/013_Tau_Identification/efficiencies/reconstruction/pid_muon_dd/fit.root pid_muon_dd/fit.root
