#!/usr/bin/env python

"""

Study intensively the PROBNNghost variable and its effect.

By 'ghost', I mean PROBNNghost>0.5

"""

from kinematic import *

def main():

  for dtype,objs in reco_objects.iteritems():
    tree  = TREES(dtype)
    filt  = ALL_CUTS(dtype)[:5] # Cut with track-quality
    # filt  = ALL_CUTS(dtype)[:4] # Cut before track-quality (at reco)

    QHist.reset()
    QHist.filters = filt
    QHist.prefix  = dtype.replace('_','')
    QHist.trees   = tree 

    ## Scan the values of ghost on top of the trackQ cut.
    ## --> Verdict: Virtually no ghost left.
    h = QHist()
    h.name    = 'PROBNNghost'
    h.params  = ['%s_PROBNNghost'%obj for obj in objs]
    h.ylog    = True
    h.draw()

    h = QHist()
    h.name    = 'TRGHP'
    h.params  = ['%s_TRGHOSTPROB'%obj for obj in objs]
    h.ylog    = True
    h.draw()

    ## 

  exit()

if __name__ == '__main__':
  main()
