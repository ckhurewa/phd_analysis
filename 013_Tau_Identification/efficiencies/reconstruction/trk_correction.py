#!/usr/bin/env python
"""

Provide the contralized module for the computation of track correction.

"""

from rec_utils import *

## Per-track systematic for 2012 DD/MC
# https://twiki.cern.ch/twiki/bin/view/LHCb/TrackingEffStatus2012S20 
SYST_TRK_CORR = ufloat(1, 0.004)

## match the spec of correction map
BIN_ETA = 1.9, 3.2, 4.9
BIN_P   = 0, 5, 10, 20, 40, 100, 201, 1e6

#===============================================================================

@memorized
def correction_eta_p():
  """
  Return the map of correction factor, wrapped if needed.
  The per-track systematic is already included here.
  """
  ## Grab the correction map & prepare empty map
  path = '$DIR13/efficiencies/reconstruction/trk_correction/ratio2012S20.root'
  fin  = ROOT.TFile(os.path.expandvars(path))
  eff  = fin.Get('Ratio').dataframe()
  # add the per-track syst
  eff = eff.applymap(lambda x: x*SYST_TRK_CORR)
  eff.index.name = 'ETA'
  eff.columns.name = 'P'
  # wrapped
  eff['(0, 5]']        = eff['(5, 10]']
  eff['(201, %i]'%1e6] = eff['(100, 201]']
  return sort_intv_pt(eff)

#===============================================================================

def lookup(*args):
  df = correction_eta_p().stack()
  return lookup_eff_generic(__file__, df, _ETA=BIN_ETA, _P=BIN_P)(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print correction_eta_p()
  # print lookup()

  ## Test
  df_h3mu = get_selected_ztautau('h3mu')
  df_h3mu['pr1_P'] = df_h3mu['pr1_P']/1e3
  print lookup(df_h3mu, 'h3mu', 'pr1')

