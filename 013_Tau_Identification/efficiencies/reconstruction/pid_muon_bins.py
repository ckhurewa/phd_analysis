#!/usr/bin/env python
from rec_utils import *

## for high-pt regime, dictated by sfarry data
BIN_ETA_HIGH  = 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5
BIN_PT_HIGH   = 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70

# for high0PT manual Z0 
BIN_PT_Z0     = 20, 25, 30, 40, 70
BIN_ETA5_Z0   = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
BIN_ETA10_Z0  = 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5

## for low-pt regime: using Jpsi fitting
BIN_ETA_5  = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
BIN_PT_LOW = 5, 7, 10, 20, 30, 70

## Global regime for all selected sample
BIN_ETA10 = 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5
BIN_PT_W  = 5, 7, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70

## Collapsed in large ETA, good for restricted sWeight, where stats is low in 
# high-ETA
BIN_ETA_SKEW = 2.0, 2.25, 2.5, 2.75, 3.0, 3.5, 4.0, 4.5

## Extended regime for HMT compat
# One extra constant bin at super-high muon PT
# also has a mod where large-PT bin collapse [50, 55, 60, 65, 70]
BIN_ETA_NOMINAL = 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5
BIN_PT_NOMINAL  = 5, 7, 10, 15, 20, 25, 30, 35, 40, 45, 50, 70, 300

## doubled/halved for syst
BIN_PT_NOMINAL_LOW = 5, 7, 10, 15, 20 # same as nominal, until 20GeV
BIN_ETA_DOUBLED = pd.np.linspace(2, 4.5, 20+1)

## Null bins
BIN_ETA_NONE = 2, 4.5
BIN_PT_NONE  = 5, 70
BIN_SPD_NONE = 0, 600

## Composite from regime
REGIMES = {
  'nominal': {
    'ETA': BIN_ETA_NOMINAL,
    'PT' : BIN_PT_NOMINAL,
  },
  'ptonly': {
    'ETA': BIN_ETA_NONE,
    'PT' : BIN_PT_NOMINAL,
  },
  'etaonly': {
    'ETA': BIN_ETA_NOMINAL,
    'PT' : BIN_PT_NONE,
  },
  'etaskew': { # suitable for low-pt sweight calc.
    'ETA': BIN_ETA_SKEW,
    'PT' : BIN_PT_LOW,
  },
  'patchEta354': { # this particular bin [20,25]x[3.5,4] need patch, assume flat PT around [20,25]
    'ETA': [3.5, 4],
    'PT' : [15, 25],
  },
  'halved': { # for syst of PIDCalib
    'ETA': BIN_ETA_5,
    'PT' : BIN_PT_NOMINAL_LOW,
  },
  'doubled': { # for syst of PIDCalib
    'ETA': BIN_ETA_DOUBLED,
    'PT' : BIN_PT_NOMINAL_LOW,    
  }
}

#===============================================================================
# WEIGHTS
#===============================================================================

@pickle_dataframe_nondynamic
def multiweights_pt_eta():
  acc = {}
  for tag, scheme in REGIMES.iteritems():
    acc[tag] = weighter_pt_eta(bin_pt=scheme['PT'], bin_eta=scheme['ETA']).stack()
  return pd.DataFrame(acc)

#===============================================================================

if __name__ == '__main__':
  print multiweights_pt_eta()
