#!/usr/bin/env python

"""
Aux of hadron ePID: Data-MC comparison, systematics.
"""

from rec_utils import *
import pid_hadron_dd
import pid_hadron_mc

#===============================================================================

@pickle_dataframe_nondynamic # shared to both latex & sys
def eff_compare_dd_mc():
  """
  Compare per-channel scalar eff. Keep only necessary columns
  """
  ## from disperse version
  # import pid_hadron_mc
  # eff_mc = pid_hadron_mc.scalar_results()

  ## from cut-and-count version
  import kinematic
  eff0_mc = kinematic.calculate_stages_detailed()
  eff_mc = pd.Series({
    'h1mu': eff0_mc.loc['epid_pi', 'h1mu'],
    'h3mu': eff0_mc.loc['epid_pr', 'h3mu'],
    'eh1' : eff0_mc.loc['epid_pi', 'eh1'],
    'eh3' : eff0_mc.loc['epid_pr', 'eh3'],
  })

  ## Columns/Indices to be compared, renamed
  columns = {
    '2D-nominal' : 'nominal',
    '2D-hETA_hPT': 'halved',
    '2D-wETA'    : 'doubled',
  }
  indices = {
    'h1mu_pi'   : 'h1mu',
    'eh1_pi'    : 'eh1',
    'h3mu_tauh3': 'h3mu',
    'eh3_tauh3' : 'eh3',
  }

  ## Grab eff from data, keep some, make order, rename
  df = pid_hadron_dd.scalar_results()
  df = df.loc[indices.keys(), columns.keys()]
  df = df.rename(indices, columns=columns)
  df.insert(0, 'MC', eff_mc)
  return df


@memorized
def syst():
  """
  Determine the ePID systematics for each object.
  Taken from the differences between nominal bin and halved/doubled.
  """
  ## compat
  columns = {'dd_nominal': 'nominal', 'dd_doubled': 'doubled', 'dd_halved': 'halved'}
  return eff_compare_dd_mc().rename(columns=columns).apply(syst_pidcalibstyle, axis=1)


#-------------------------------------------------------------------------------

def latex_compare_eff():
  """
  Comparison of different ePID from different modes (MC, DD (nom, x2, /2)),
  as well as the associated systematics.
  """
  df = eff_compare_dd_mc().loc[['h1mu', 'eh1', 'h3mu', 'eh3']]
  df['Systematics [\\%]'] = syst().apply(lambda u: u.s)
  df = df.rename({
    'h1mu': '\\tauh1 from \\dmuh1',
    'eh1' : '\\tauh1 from \\deh1',
    'h3mu': '\\tauh3 from \\dmuh3',
    'eh3' : '\\tauh3 from \\deh3',
  }, columns={
    'dd_nominal': 'Nominal',
    'dd_halved' : 'Halved',
    'dd_doubled': 'Doubled',
  })
  df.index.name = ' \\ePID [\\%]'
  ## 170612: drop MC
  df = df.drop('MC', axis=1) * 100
  df.iloc[:,-1] = df.iloc[:,-1].apply('{:.2f}'.format) # force syst to 2deci
  return df.to_bicol_latex(stralign='c') # Aurelio prefer 'c'

  # manual table, take only content
  # msg = df.fmt2lp.to_latex(escape=False)
  # return '\n'.join(msg.split('\n')[5:9])


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_compare_dd_mc_eta():
  ## weighted to h1mu
  eff_mc = pid_hadron_mc.eff_eta_weighed_H1MU()
  eff_dd = pid_hadron_dd.eff_eta_weighed_H1MU() * ufloat(1, 0.0165) # conservatively from bin-variation, in eh3

  ## Do draw
  df = pd.DataFrame({
    'Data'      : eff_dd,
    'Simulation': eff_mc,
  })
  df.columns.name = '#varepsilon_{PID,h}'
  df.index.name   = '#eta(h)'
  gr, leg = drawer_graphs_dd_mc(df)
  ROOT.gPad.logx = False


@gpad_save
def draw_eff_compare_dd_mc_pt():
  ## All hadrons 
  eff_mc = pid_hadron_mc.eff_pt_weighed_H3MU()
  eff_dd = pid_hadron_dd.eff_pt_weighed_H3MU() * ufloat(1, 0.0165) # conservatively from bin-variation, in eh3

  ## Do Draw
  df = pd.DataFrame({
    'Data'      : eff_dd,
    'Simulation': eff_mc,
  })
  # del df['(30, 70]']
  df.columns.name = '#varepsilon_{PID,h}'
  df.index.name   = 'p_{T}(h) [GeV]'
  gr, leg = drawer_graphs_dd_mc(df)
  gr.xaxis.moreLogLabels = True
  gr.xaxis.labelOffset = -0.01
  gr.xaxis.limits = 1, 30
  ROOT.gPad.logx = True


#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print eff_compare_dd_mc().fmt2p
  # print syst().fmt3f

  ## Drawer
  # draw_eff_compare_dd_mc_eta()
  # draw_eff_compare_dd_mc_pt()

  ## Latex
  print latex_compare_eff()
