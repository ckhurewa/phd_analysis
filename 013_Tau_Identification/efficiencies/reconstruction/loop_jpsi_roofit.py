#!/usr/bin/env python

from rec_utils import *
from collections import OrderedDict
import __main__

## default write/read directory
DNAME = 'fitting_jpsi'

#===============================================================================

class Snapper(OrderedDict):
  """
  Helper method to collect value of (moving) parameters at different stages.
  
  Usage:

  >> snapper = Snapper( p1, p2, p3 )
  >> (later on)
  >> snapper.snap('some commit msg')

  """
  def __init__(self, *args):
    super(Snapper, self).__init__()
    self.args = args
    self.snap('init')

  def snap(self, msg):
    res = {}
    for arg in self.args:
      res[arg.title] = ufloat( arg.val, arg.error )
    self[msg] = res
    print 'Snapped: ', msg


#===============================================================================

def fullpath(fname, dname=DNAME):
  """
  Return apprepriate fullpath to be used to load/save object.
  Do folder creation here
  """
  d1    = os.path.splitext(os.path.abspath(__main__.__file__))[0]
  dname = os.path.join(d1, dname)
  if not os.path.exists(dname):
    os.makedirs(dname)
  return os.path.join( dname, fname )


def pre_binning( tree, mass_min=2.7, mass_max=3.5 ):
  """
  Prebinning will attempt to discreticize values for better performance.

  The tree should have following alias ready (usually based on probe's)
  - PREBIN_MASS (mass of Jpsi in GeV)
  - PREBIN_ETA  (eta of probe)
  - PREBIN_PT   (pt of probe in GeV)
  - PREBIN_FILT (global filter)
  - PREBIN_PASS (condition for valid tag-and-probe)

  Following binning applies (should be versatile for any specific choice)
  - mass: from [2.7, 3.5] GeV, in 40 bins (i.e., step of 20 MeV)
  - eta : 2.0 to 4.5, in step of 0.25
  - pt  : [1,30], in step of 0.5 --> 58 bins

  Return 2 TH3F corresponding to the before/after entries

  note: param order is 'z:y:x >> h(x,y,z)
  """
  
  ## PT
  param = 'PREBIN_PT: PREBIN_ETA: PREBIN_MASS >> h(40,%s,%s, 10,2.0,4.5, 58,1,30)'%(mass_min,mass_max)
  ## P
  # param = 'PREBIN_P: PREBIN_ETA: PREBIN_MASS >> h(40,%s,%s, 10,2.0,4.5, 200,0,200)'%(mass_min,mass_max)

  log.info('Start prebinning.')
  tree.Draw( param, 'PREBIN_FILT', 'goff' )
  h_before = ROOT.gROOT.Get('h').Clone('h_before')
  tree.Draw( param, utils.join('PREBIN_FILT', 'PREBIN_PASS'), 'goff' )
  h_after = ROOT.gROOT.Get('h').Clone('h_after')
  return h_before, h_after




def projx( h3, ymin, ymax, zmin, zmax ):
  """
  Helper method to do projection using content value instead of bin indices.
  """
  iymin = h3.yaxis.FindBin(ymin)
  iymax = h3.yaxis.FindBin(ymax)
  if ymin == h3.yaxis.GetBinUpEdge(iymin):
    iymin += 1
  if ymax == h3.yaxis.GetBinLowEdge(iymax):
    iymax -= 1
  izmin = h3.zaxis.FindBin(zmin)
  izmax = h3.zaxis.FindBin(zmax)
  if zmin == h3.zaxis.GetBinUpEdge(izmin):
    izmin += 1
  if zmax == h3.zaxis.GetBinLowEdge(izmax):
    izmax -= 1
  print iymin, iymax, izmin, izmax
  return h3.ProjectionX( h3.name+'_x', iymin, iymax, izmin, izmax )


#===============================================================================

# def _raw_jpsi():
#   df = import_tree('TrackingEffAlgo/Tracking/Jpsi', *JIDS_jpsi ).dataframe()
#   df.probe_PT /= 1e3
#   return df  

# def estimate_fitting_stat():
#   df = _raw_jpsi()
#   bins_eta = 2.0, 2.25, 2.75, 3.25, 3.75, 4.5
#   bins_pt  = 1, 1.5, 2.5, 4, 6, 10
#   print binning_2D( df, probe_ETA=bins_eta, probe_PT=bins_pt )


#===============================================================================

def roo_draw( prefix, var, data, model ):
  """
  Take care of the boiler-plates of drawing
  """
  canvas  = ROOT.TCanvas( model.title, model.title )
  frame   = var.frame()
  canvas.ownership  = False
  frame.ownership   = False
  
  ## adding components
  data.plotOn ( frame )
  model.plotOn( frame )

  ## If there's linear (m1b, m1a), draw line
  if hasattr( model, 'pdfList' ):
    line_st = RooFit.LineStyle(ROOT.kDashed)
    for i in xrange( model.pdfList().size ):
      pdf = model.pdfList()[i]
      if pdf.name in ('m1b', 'm1a'):
        line_cl = RooFit.LineColor(utils.COLORS(i))
        comp    = RooFit.Components(pdf.name)
        model.plotOn( frame, comp, line_st, line_cl )
  frame.Draw()
  ROOT.gPad.Update()
  fname = '{}_{}.pdf'.format( prefix, model.name )
  ROOT.gPad.SaveAs(fullpath(fname))


#===============================================================================

def gen_prefixes( bin_eta, bin_pt ):
  for i in xrange(len(bin_pt)-1):
    for j in xrange(len(bin_eta)-1):
      pt_min ,pt_max  = bin_pt [i:i+2]
      eta_min,eta_max = bin_eta[j:j+2]
      prefix  = 'pt_%03i_%03i_eta_%03i_%03i'%(pt_min*10, pt_max*10, eta_min*100, eta_max*100)
      yield prefix, eta_min, eta_max, pt_min, pt_max


def calc_cb_binning_all( tree, bin_eta, bin_pt, mass_min=2.7, mass_max=3.5 ):
  hb,ha = pre_binning( tree, mass_min, mass_max )
  for prefix, eta_min, eta_max, pt_min, pt_max in gen_prefixes(bin_eta, bin_pt):
    h_bef   = projx( hb, eta_min, eta_max, pt_min, pt_max )
    h_aft   = projx( ha, eta_min, eta_max, pt_min, pt_max )
    fname   = '%s.df'%prefix
    snapper = calc_cb_binning_single( prefix, h_bef, h_aft, mass_min, mass_max )
    pd.DataFrame(snapper).to_pickle(fullpath(fname))


def calc_cb_binning_single( prefix, h_bef, h_aft, mass_min, mass_max ):
  """
  prefix: String to be used for saving plots.
  """

  ## Preamble
  x = RooFit.RooRealVar('M', 'M', mass_min, mass_max )
  #
  n1b = RooFit.RooRealVar('n1b' , 'n1b', 0., 0, h_bef.entries )
  n1a = RooFit.RooRealVar('n1a' , 'n1a', 0., 0, h_aft.entries )
  n2b = RooFit.RooRealVar('n2b' , 'n2b', 0., 0, h_bef.entries ) # reused in Gauss/CB
  n2a = RooFit.RooRealVar('n2a' , 'n2a', 0., 0, h_aft.entries )
  #
  p1b = RooFit.RooRealVar('p1b', 'p1b', 0.   , -10  , +10 ) # linear slope
  p1a = RooFit.RooRealVar('p1a', 'p1a', 0.   , -10  , +10 ) # linear slope
  #
  p3 = RooFit.RooRealVar('p3', 'p3', 3.1  , 3.0 , 3.2 ) # gauss position
  p4 = RooFit.RooRealVar('p4', 'p4', 0.05 , 0.  , 10  ) # gauss width
  p5 = RooFit.RooRealVar('p5', 'p5', 1    , 0.  , 10  ) # CB threshold (alpha)
  p6 = RooFit.RooRealVar('p6', 'p6', 1    , 1.  , 10  ) # CB scale (n)

  ## Prepare the snapper instance to collect info
  snapper = Snapper( n1b, n1a, n2b, n2a, p1b, p1a, p3, p4, p5, p6 )

  ## Prepare sample dataset
  ds_bef = RooFit.RooDataHist('ds_bef', 'ds_bef', [x], h_bef)
  ds_aft = RooFit.RooDataHist('ds_aft', 'ds_aft', [x], h_aft)

  ## Abort calculation if bin is empty
  if h_bef.entries == 0:
    return snapper

  ## Stage 1: Linear fit
  m_linear_bef  = RooFit.RooPolynomial('m_linear_bef', 'm_linear_bef' , x, [p1b])
  m_linear_aft  = RooFit.RooPolynomial('m_linear_aft', 'm_linear_aft' , x, [p1a])
  x_linear_bef  = RooFit.RooExtendPdf( 'm1b', 'm1b' , m_linear_bef, n1b )
  x_linear_aft  = RooFit.RooExtendPdf( 'm1a', 'm1a' , m_linear_aft, n1a )
  x_linear_bef.fitTo( ds_bef )
  x_linear_aft.fitTo( ds_aft )
  roo_draw( prefix+'_b1', x, ds_bef, x_linear_bef )
  roo_draw( prefix+'_a1', x, ds_aft, x_linear_aft )
  snapper.snap('At linear fit')
  # exit()  


  ## Stage 2: Gaussian fit
  m_gauss     = RooFit.RooGaussian ('m_gauss'     , 'gauss'       , x, p3, p4    )
  x_gauss_bef = RooFit.RooExtendPdf('x_gauss_bef' , 'x_gauss_bef' , m_gauss, n2b )
  x_gauss_aft = RooFit.RooExtendPdf('x_gauss_aft' , 'x_gauss_aft' , m_gauss, n2a )
  m2_bef      = RooFit.RooAddPdf   ('m2b', 'm2b', [ x_linear_bef, x_gauss_bef ])
  m2_aft      = RooFit.RooAddPdf   ('m2a', 'm2a', [ x_linear_aft, x_gauss_aft ])
  m2_bef.fitTo( ds_bef )
  roo_draw( prefix+'_b2', x, ds_bef, m2_bef )
  snapper.snap('At gaussian fit (before)')
  m2_aft.fitTo( ds_aft )
  roo_draw( prefix+'_a2', x, ds_aft, m2_aft )
  snapper.snap('At gaussian fit (after)')
  # exit()


  ## Stage 3: CrystalBall fit (non-simultaneous)
  # roofit( m, m0, sigma, alpha, n) 
  # wiki( x; xbar, sigma, alpha, n )
  # Phil( x; p3, p4, p5, p6 )
  #
  m_cb      = RooFit.RooCBShape   ('m_cb'     , 'm_cb'    , x, p3, p4, p5, p6 )
  x_cb_bef  = RooFit.RooExtendPdf ('x_cb_bef' , 'x_cb_bef', m_cb, n2b )
  x_cb_aft  = RooFit.RooExtendPdf ('x_cb_aft' , 'x_cb_aft', m_cb, n2a ) # share p3,p4,p5,p6
  m3_bef    = RooFit.RooAddPdf    ( 'm3b', 'm3b', [ x_linear_bef, x_cb_bef ])
  m3_aft    = RooFit.RooAddPdf    ( 'm3a', 'm3a', [ x_linear_aft, x_cb_aft ])

  ## Record
  m3_bef.fitTo( ds_bef )
  roo_draw( prefix+'_b3', x, ds_bef, m3_bef )
  snapper.snap('At non-sim CB fit (before)')
  m3_aft.fitTo( ds_aft )
  roo_draw( prefix+'_a3', x, ds_aft, m3_aft )
  snapper.snap('At non-sim CB fit (after)')
  # exit()


  ## Stage 4: Simultaneous CB fit
  cat = RooFit.RooCategory('category', 'category')
  cat.defineType('before')
  cat.defineType('after' )

  # ds  = RooFit.RooDataSet( 'combined_data', 'combined_data', {x}, RooFit.Index(cat), RooFit.Import('before',ds_bef), RooFit.Import('after',ds_aft))
  ds  = RooFit.RooDataHist( 'combined_data', 'combined_data', [x], RooFit.Index(cat), RooFit.Import('before',ds_bef), RooFit.Import('after',ds_aft))

  m3_bef.name = m3_bef.title = 'm4b'
  m3_aft.name = m3_aft.title = 'm4a'

  simfit = RooFit.RooSimultaneous('simfit', 'simultaneous pdf', cat)
  simfit.addPdf( m3_bef, 'before' )
  simfit.addPdf( m3_aft, 'after'  )
  simfit.fitTo(ds)
  snapper.snap('At sim CB fit')

  roo_draw( prefix+'_b4', x, ds_bef, m3_bef )
  roo_draw( prefix+'_a4', x, ds_aft, m3_aft )

  return snapper

#===============================================================================

def read_cb_result( bin_eta, bin_pt, dname=DNAME ):
  ## Read from pickle
  res = {}
  for prefix,_,_,_,_ in gen_prefixes( bin_eta, bin_pt ):
    fname       = '%s.df'%prefix
    res[prefix] = pd.read_pickle(fullpath(fname, dname)).stack()

  ## Pack into df
  # Don't veto yet, for reference
  df = pd.DataFrame(res).swaplevel().loc['At sim CB fit'].loc[['n2a','n2b']].T

  print df[['n2a','n2b']]

  df = pd.DataFrame({'eff': divide_clopper_pearson( nume=df.n2a, deno=df.n2b )})

  def get_pt(s):
    s1,s2 = re.findall(r'pt_(\d+)_(\d+)_eta', s)[0]
    s1 = str(float(s1)/10).replace('.0','')
    s2 = str(float(s2)/10).replace('.0','')
    s1 = '%i'%int(s1) if s1.isdigit() else '%.1f'%float(s1)
    s2 = '%i'%int(s2) if s2.isdigit() else '%.1f'%float(s2)
    return '(%s, %s]'%(s1,s2)

  def get_eta(s):
    ## BUG fix
    s = s.replace('254', '255')
    #
    s1,s2 = re.findall(r'.*_eta_(\d+)_(\d+)', s)[0]
    s1 = str(float(s1)/100).replace('.0','')
    s2 = str(float(s2)/100).replace('.0','')
    s1 = '%i'%int(s1) if s1.isdigit() else '%.2f'%float(s1)
    s2 = '%i'%int(s2) if s2.isdigit() else '%.2f'%float(s2)
    s1 = s1.replace('.50', '.5')
    s2 = s2.replace('.50', '.5')
    return '(%s, %s]'%(s1,s2)

  ## Breaking index, repoviting, correlate
  df['PT']  = [ get_pt(s)   for s in df.index ]
  df['ETA'] = [ get_eta(s)  for s in df.index ]
  df = sort_intv_pt(df.pivot( 'PT', 'ETA', 'eff' ))
  return df.T

