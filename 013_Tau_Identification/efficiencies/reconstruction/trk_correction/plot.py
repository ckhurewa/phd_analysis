#!/usr/bin/env python

from PyrootCK import *
ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
ROOT.gROOT.batch = True
ROOT.gStyle.palette = ROOT.kLightTemperature ## Colors
# ROOT.gStyle.palette = ROOT.kBird ## Colors
ROOT.gStyle.SetPaintTextFormat("4.3f");

## Grab
fin  = ROOT.TFile('ratio2012S20.root')
hist = fin.Get('Ratio')

## Draw
c = ROOT.TCanvas()
c.canvasSize = 640, 320
hist.Draw('texte col')
hist.yaxis.titleOffset = 0.4
hist.markerSize = 4

c.logx = True
c.topMargin = 0.02
c.leftMargin = 0.08
c.rightMargin = 0.01
c.SaveAs('ratio2012S20.pdf')
