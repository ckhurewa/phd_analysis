#!/usr/bin/env python

from rec_utils import *
from tracking_bins import *

#===============================================================================
# MC
#===============================================================================

def eff_trio():
  """
  Binning in all three variables. For reweigh against Zmumu
  """
  return multieff('trio').loc['fulldiv']['muons']

def eff_eta_pt():
  ## just curious
  return sort_intv_pt(multieff('eta_pt').loc['fulldiv']['muons'].unstack())

def eff_etaW():
  """
  Preferred binning scheme matched with W/Z paper
  """
  return multieff('etaW').loc['fulldiv']['muons']

#===============================================================================

if __name__ == '__main__':
  # print eff_etaW()
  print eff_eta_pt().fmt2p
  # print eff_trio()
