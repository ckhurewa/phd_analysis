#!/usr/bin/env python

from rec_utils import *
import pid_muon_mc
import pid_muon_dd

#===============================================================================

@gpad_save
def draw_compare_dd_mc_eta():
  ## Inputs
  # bin_pt, note = '(5, 7]', '5 < p_{T} < 7 GeV'
  # bin_pt, note = '(7, 10]', '7 < p_{T} < 10 GeV'
  # bin_pt, note = '(10, 15]', '10 < p_{T} < 15 GeV'
  # bin_pt, note = '(15, 20]', '15 < p_{T} < 20 GeV'
  # bin_pt, note = '(20, 25]', '20 < p_{T} < 25 GeV'
  bin_pt, note = '(25, 30]', '25 < p_{T} < 30 GeV'
  df = pd.DataFrame({
    'Data'      : pid_muon_dd.lookup().unstack()[bin_pt],
    'Simulation': pid_muon_mc.efficiencies().loc[bin_pt],
  })
  df.columns.name = '#varepsilon_{PID,#mu}'
  df.index.name   = '#eta(#mu)'

  ## Send to drawer
  gr, leg    = drawers.graphs_dd_mc(df)
  gr.minimum = 0.7
  gr.maximum = 1.02
  drawers.note(note)


@gpad_save
def draw_compare_dd_mc_pt():
  ## Inputs
  bin_eta, note = '(2.75, 3]', '2.75 < #eta < 3.0'

  ## Inputs
  df = pd.DataFrame({
    'Data'      : pid_muon_dd.lookup().unstack().T[bin_eta],
    'Simulation': pid_muon_mc.efficiencies()[bin_eta],
  })
  df.columns.name = '#varepsilon_{PID,#mu}'
  df.index.name   = 'p_{T}(#mu) [GeV]'
  gr, leg         = drawers.graphs_dd_mc(df)
  gr.minimum      = 0.80
  gr.maximum      = 1.02
  gr.xaxis.rangeUser     = 0, 50
  gr.xaxis.moreLogLabels = True
  drawers.note(note)


#===============================================================================

@pickle_dataframe_nondynamic # ready for latex
def compare_eff():
  sys.path.append('pid_muon_dd')
  import lowpt_jpsi
  df = lowpt_jpsi.scalar_results().unstack()
  df['MC'] = pid_muon_mc.scalar_results()
  return df.T

def latex_compare_eff():
  """
  Comparison of different ePID from different modes (MC, DD (nom, x2, /2)),
  as well as the associated systematics.
  """
  ## calculate syst on-the-fly. It's not used in computation anyway.
  df = compare_eff()
  df.loc['Systematics [\\%]'] = df.apply(syst_pidcalibstyle).apply(std_dev)
  df = df.T.rename({
    'mumu_mu2': '\\taumu (low-\\pt) from \\dmumu',
    'emu_mu'  : '\\taumu from \\dmue',
  }, columns={
    'nominal': 'Nominal',
    'halved' : 'Halved',
    'doubled': 'Doubled',
  })
  df.index.name = ' \\ePID [\\%]' # space bug
  ## 170612: drop MC
  df = df.drop('MC', axis=1) * 100
  df.iloc[:,-1] = df.iloc[:,-1].apply('{:.2f}'.format) # force syst to 2deci
  return df.to_bicol_latex(stralign='c') # Aurelio prefer 'c'


  # # manual table, take only content
  # msg = df.fmt2lp.to_latex(escape=False)
  # return '\n'.join(msg.split('\n')[4:-3])


#===============================================================================

if __name__ == '__main__':
  pass
  ## Drawer
  # draw_compare_dd_mc_eta()
  # draw_compare_dd_mc_pt()

  ## Latex
  # print compare_eff().fmt2p
  print latex_compare_eff()
