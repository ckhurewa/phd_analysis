#!/usr/bin/env python

"""

Old hadron ePID method with minimum bias sample. 
Somewhat not working.

"""

from rec_utils import *

#===============================================================================
# DATA-DRIVEN
#===============================================================================

@pickle_dataframe_nondynamic
def _load_raw_tree_cutlater():
  return import_tree( 'HadronPIDAlgo/hardest_cutlater', 5021 ).dataframe()


@pickle_dataframe_nondynamic
def _load_raw_tree_cutearly():
  df = import_tree( 'evttup_PID_had/had', 4423, 4424, 4425 ).dataframe('StrippingMBNoBiasDecision')
  df['ISLOOSEMUON'] = df.ISMUONLOOSE
  return df[['ETA', 'PT', 'nSPDHits', 'ISLOOSEMUON', 'HCALFrac']]


@pickle_dataframe_nondynamic
def eff_dd_pt_eta_spd_dirty():
  """
  By dirty, it means the high-PT bin eff is still not well-defined.
  """
  ## Load MINIBIAS tree
  df = _load_raw_tree_cutlater()  # lower stat
  ## Load cutearly tree
  # df = _load_raw_tree_cutearly()  # larger stat
  
  ## Sanitize
  df.PT  /= 1e3
  df_deno = df 
  df_nume = df[(df.HCALFrac>=0.05) & (df.ISLOOSEMUON==0)]
  ## Bin in 3D, adjusting corr
  se_deno = binning_3D( df_deno, ETA=BIN_ETA, PT=BIN_PT, nSPDHits=BIN_SPD ).apply(lambda x: x or 1.)
  se_nume = binning_3D( df_nume, ETA=BIN_ETA, PT=BIN_PT, nSPDHits=BIN_SPD ).apply(lambda x: np.nan if pd.isnull(x) else var(x) )
  se_eff  = (se_nume / se_deno).fillna(ufloat(0,0))
  return se_eff.reorder_levels(['PT', 'ETA', 'nSPDHits']).sort_index()


@pickle_dataframe_nondynamic
def eff_dd_pt_dirty():
  """
  Efficiencies table from DD, as a func of PT, weighted in ETA,SPD of selection.
  """
  cols    = eff_mc_pt().columns # relevant column
  eff     = eff_dd_pt_eta_spd_dirty()
  weights = weights_pt_eta_spd()[cols]
  def avg(df):
    pt = df.name
    w  = df.loc[pt]
    e  = eff.loc[pt]
    return w.apply(lambda se: (se*e).sum()/(se.sum() or 1.))
  return weights.groupby(level='PT').apply(avg)


@gpad_save
def draw_eff_dd_pt_dirty():
  src = ['h3mu_pr1', 'h3mu_pr3', 'h3mu_pr']
  eff = eff_dd_pt_dirty()[src]
  drawer_graphs_eff_pt( eff )


#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def eff_dd_pt():
  """
  Now it's a clean version using shape from MC but scale to data,
  in order to have the value at high-PT regime

  Two scaling methods possible:
  1. Scale using all good bins from data 
  - PRO: larger stats
  - CON: the factor may not be uniform in lower PT bins

  2. Using last PT bin at the join
  - PRO: Shouldn't change much in larger PT
  - CON: lower stat
  
  NOTE: Unlike etrack of hadron, which is NOT determined directly from data,
  epid is partially determined from data (up to mid-PT regime),
  
  Assumption: unlike etrack_hadron, this does NOT variy by channel, thus the
  eff can be collected into one 'hadron' channel.

  Return epid(pt) as global eff to be used on all hadron channel.
  This is reweighted in ETA,SPD of the hadron in Ztautau analysis, 
  which are already similar enough between themselve

  """ 

  ## Prepare the joint
  JOINT = '(6, 10]'  # last good DD bin
  # JOINT = '(10, 20]'  # last good DD bin
  arr   = list(pd.cut(pd.DataFrame(), BIN_PT).categories)
  index = arr.index(JOINT)
  low   = arr[:index]
  hi    = arr[index:]

  ## Prepare ratios. 
  eff_dd = eff_dd_pt_dirty().hadrons
  eff_mc = eff_mc_pt().hadrons
  ratios = eff_dd / eff_mc

  ## Obtain the scaling factor,
  # 1. Using only last bin joint
  scale = ratios.loc[JOINT]
  # 2. Use average of the first few bins until joint, which has highest stat (1,6]
  # scale = (eff.ratio.iloc[:4].sum())/4
  print 'Scaling factor:', scale

  eff_low = eff_dd.loc[list(low)]
  eff_hi  = eff_mc.loc[list(hi)]*scale
  eff     = pd.concat([eff_low, eff_hi])
  return eff


@gpad_save
def draw_compare_eff_pt():
  """
  Compare the MC and DD as a func of PT
  """
  df = pd.DataFrame({
    'MC:hadrons': eff_mc_pt()['hadrons'],
    # 'MC: h3mu'  : calc_eff_mc_pt()['h3mu_pr'],
    'DD (dirty)': eff_dd_pt_dirty()['hadrons'], 
    'DD (clean)': eff_dd_pt(),
  })
  graphs,_ = drawer_graphs_eff_pt(df)


#===============================================================================

# def calc_weighted_eff_mc_allsrc():
#   e = eff_mc_pt()
#   w = weights_pt()
#   queues = 'h1mu_pi', 'h3mu_pr1', 'h3mu_pr2', 'h3mu_pr3', 'eh1_pi', 'eh3_pr1', 'eh3_pr2', 'eh3_pr3'  
#   for name in queues:
#     print name, format_eff( w[name], e[name] )

# def calc_weighted_eff_dd_allsrc():
#   e = eff_dd_pt()
#   w = weights_pt()
#   queues = 'h1mu_pi', 'h3mu_pr1', 'h3mu_pr2', 'h3mu_pr3', 'eh1_pi', 'eh3_pr1', 'eh3_pr2', 'eh3_pr3'
#   # queues = 'h3mu_pr1', 'h3mu_pr2', 'h3mu_pr3'
#   for name in queues:
#     print name, format_eff( w[name], e )
#     # print w[name]

#===============================================================================

if __name__ == '__main__':
  pass 

  ## DD - dirty
  # eff_dd_pt_eta_spd_dirty()
  # print eff_dd_pt_dirty()
  # draw_eff_dd_pt_dirty()
  # print eff_dd_pt()
  # draw_compare_eff_pt()
  
  ## Final scalar
  # calc_weighted_eff_mc_allsrc()
  # calc_weighted_eff_dd_allsrc()
