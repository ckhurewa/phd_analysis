#!/usr/bin/env python

"""

Calculate the MC recoeff as well as kinematic efficiency.

"""
import sys
from rec_utils import (
  DTYPES, DTYPE_TO_DT, DT_TO_DTYPE, DT_TO_LATEX, LABELS_DITAU_SEP, DTYPE_TO_DGROUP,
  ROOT, utils, drawers, import_tree, EffU,
  report_info, pickle_dataframe_nondynamic,
)
from PyrootCK.QHist.CoreV3 import QHist as QHistV3

## Generator for list of primitive objects used in reco cut
reco_objects = {
  'e_e'  : ('lep1', 'lep2'),
  'e_mu' : ('lep1', 'lep2'),
  'mu_mu': ('lep1', 'lep2'),
  'e_h1' : ('lep' , 'had' ),
  'h1_mu': ('lep' , 'had' ),
  'e_h3' : ('lep' , 'pr1' , 'pr2' , 'pr3'),
  'h3_mu': ('lep' , 'pr1' , 'pr2' , 'pr3'),
}

#===============================================================================
# SOURCE TREE: RECOSTAT
#===============================================================================

# 4029
# 4084
# 4093
# 4141
# 4259
# 4280: New 1Mevt batch
# 4297: Fix mcMatching
# 4677: Revert to simple mcMatching
# 5433: Fix tauh3 non-unique prongs
def TREES(dtype):
  # support both dt, dtype
  dtype = DT_TO_DTYPE.get(dtype, dtype)
  tree  = import_tree( 'DitauRecoStats/'+dtype, 5433 )
  ## Apply stages as aliases
  for name,cuts in stages.iteritems():
    if isinstance(cuts, dict):
      cuts = cuts[dtype]
    tree.SetAlias( name, utils.join(cuts))
    # new version: no more leading number prefix
    name2 = name.split('_')[-1]
    tree.SetAlias( name2, utils.join(cuts))
  ## more aliases
  tree.SetAlias('MCpredict', utils.join(cuts_mcpredict[dtype]))
  if 'h3' not in dtype:
    p1,p2 = reco_objects[dtype]
    tree.SetAlias('PT1', p1+'_PT/1e3') # LHCb MeV to GeV
    tree.SetAlias('PT2', p2+'_PT/1e3') # LHCb MeV to GeV
  ## finally
  return tree

#===============================================================================

#----------#
# FIDUCIAL #
#----------#

## LHCb Fiducial cut on tau-lepton at MC level.
## Some MC has already taken care of PT, but still with loose ETA.
cuts_fiducial = [
  'tau1_MCPT  > 20E3',
  'tau2_MCPT  > 20E3',
  'tau1_MCETA > 2.0',
  'tau1_MCETA < 4.5',
  'tau2_MCETA > 2.0',
  'tau2_MCETA < 4.5',
  'ditau_MCM  > 60E3',
  'ditau_MCM  < 120E3',
]

#------------#
# ACCEPTANCE #
#------------#

## Cut on MC charged children of tau, for ee,emu,mumu channel
## This defines the goal for selection cut later.
cuts_mckine_ll = [
  {
    ( 'lep1_MCPT > 20E3', 'lep2_MCPT >  5E3' ),
    ( 'lep1_MCPT >  5E3', 'lep2_MCPT > 20E3' ),
  },
  'lep1_MCETA   > 2.0',
  'lep1_MCETA   < 4.5',
  'lep2_MCETA   > 2.0',
  'lep2_MCETA   < 4.5',
  #
  'ditauch_MCM  >  20E3',
  # 'ditauch_MCM  < 120E3',
]

cuts_mckine_lh = [
  'lep_MCPT    > 20E3',
  'had_MCPT    > 10E3',
  #
  'lep_MCETA   > 2.0',
  'lep_MCETA   < 4.5',
  'had_MCETA   > 2.00',
  'had_MCETA   < 4.50',
  #
  'ditauch_MCM  >  30E3',
  # 'ditauch_MCM  < 120E3',
]

cuts_mckine_lh3 = [
  'lep_MCPT   > 20E3',
  'lep_MCETA  > 2.0',
  'lep_MCETA  < 4.5',
  #
  'pr1_MCPT   >  6E3',
  'pr3_MCPT   >  1E3',
  #
  'pr1_MCETA  > 2.00',
  'pr1_MCETA  < 4.50',
  'pr2_MCETA  > 2.00',
  'pr2_MCETA  < 4.50',
  'pr3_MCETA  > 2.00',
  'pr3_MCETA  < 4.50',
  #
  'tauh3ch_MCPT > 12E3',
  'tauh3ch_MCM  > 700',
  'tauh3ch_MCM  < 1500',
  #
  'ditauch_MCM >  30E3',
  # 'ditauch_MCM < 120E3',
]

# cuts_mckine_noz = [ 'ditauch_MCM < 80E3' ]

cuts_mckine = {
  'e_e'   : cuts_mckine_ll,
  'e_h1'  : cuts_mckine_lh,
  'e_h3'  : cuts_mckine_lh3,
  'e_mu'  : cuts_mckine_ll,
  'h1_mu' : cuts_mckine_lh,
  'h3_mu' : cuts_mckine_lh3,
  'mu_mu' : cuts_mckine_ll, 
}

#------------#
# MC-Predict #
#------------#

## Ask the particle to be reconstructed, predicted by IMCReconstructed.
cut_mcpredict = utils.join(
  '{0}_recob>=2',
  '{0}_recod>=1',
).format

cut_recob = '{0}_recob>=2'.format

cuts_mcpredict = { dtype:[cut_mcpredict(obj) for obj in objs ] for dtype,objs in reco_objects.iteritems()}

cuts_recob = { dtype:['{0}_recob>=2'.format(obj) for obj in objs ] for dtype,objs in reco_objects.iteritems()}
cuts_recod = { dtype:['{0}_recod>=1'.format(obj) for obj in objs ] for dtype,objs in reco_objects.iteritems()}

#----------#
# HAS-RECO #
#----------#

## The prediction from IMCReconstructed is no perfect. This will ask again.
cut_mcrec = '{}_ID!=0'.format

cuts_mcrec = { dtype:[cut_mcrec(obj) for obj in objs ] for dtype,objs in reco_objects.iteritems()}

#-------#
# TRACK #
#-------#

## Tracking quality cut
cut_track = utils.join(
  '{0}_TRPCHI2>0.01',
  '{0}_TRTYPE==3',
  #
  # '{0}_TRGHOSTPROB<0.2',  ## REMOVED 160710
).format

cuts_track = { dtype:[cut_track(obj) for obj in objs ] for dtype,objs in reco_objects.iteritems()}

# split, between tau1, and the rest (group 3prongs together)
# cuts_track1 = { dtype:cut(objs[0]) for dtype,objs in reco_objects.iteritems()}
# cuts_track2 = { dtype:[cut(obj) for obj in objs[1:] ] for dtype,objs in reco_objects.iteritems()}

cuts_track_more1 = {
  'mu_mu': cut_track('lep1'),
  'e_e'  : cut_track('lep1'),
  'e_mu' : cut_track('lep1'),
  'h1_mu': cut_track('lep'),
  'h3_mu': cut_track('lep'),
  'e_h1' : cut_track('lep'),
  'e_h3' : cut_track('lep'),
}

cuts_track_more2 = {
  'mu_mu': cut_track('lep2'),
  'e_e'  : cut_track('lep2'),
  'e_mu' : cut_track('lep2'),
  'h1_mu': cut_track('had'),
  'h3_mu': cut_track('had'),
  'e_h1' : cut_track('pr1'),
  'e_h3' : cut_track('pr1'),
}

cuts_track_more3 = {
  'mu_mu': '1==1',
  'e_e'  : '1==1',
  'e_mu' : '1==1',
  'h1_mu': '1==1',
  'h3_mu': '1==1',
  'e_h1' : cut_track('pr2'),
  'e_h3' : cut_track('pr2'),
}

cuts_track_more4 = {
  'mu_mu': '1==1',
  'e_e'  : '1==1',
  'e_mu' : '1==1',
  'h1_mu': '1==1',
  'h3_mu': '1==1',
  'e_h1' : cut_track('pr3'),
  'e_h3' : cut_track('pr3'),
}


#-----#
# PID #
#-----#

## PID cuts
cut_PID_mu  = '{0}_ISMUON'.format
cut_PID_h   = utils.join(
  '!{0}_ISLOOSEMUON',
  '{0}_PP_InAccHcal',
  '{0}_HCALFrac > 0.05',
).format
cut_PID_e   = utils.join(
  '!{0}_ISLOOSEMUON',
  '{0}_PP_InAccHcal',
  '{0}_PP_InAccEcal',
  '{0}_HCALFrac    < 0.05',
  '{0}_ECALFrac    > 0.1',
  '{0}_PP_CaloPrsE > 50',
).format


cuts_pid = {
  'e_e': [
    cut_PID_e('lep1'),
    cut_PID_e('lep2'),
  ],
  'e_h1': [
    cut_PID_e('lep'),
    cut_PID_h('had'),
  ],
  'e_mu': [
    cut_PID_e('lep1'),
    cut_PID_mu('lep2'),
  ],
  'h1_mu': [
    cut_PID_h('had'),
    cut_PID_mu('lep'),
  ],
  'mu_mu': [
    cut_PID_mu('lep1'),
    cut_PID_mu('lep2'),
  ],
  'e_h3': [
    cut_PID_e('lep'),
    cut_PID_h('pr1'),
    cut_PID_h('pr2'),
    cut_PID_h('pr3'),
  ],
  'h3_mu': [
    cut_PID_mu('lep'),
    cut_PID_h('pr1'),
    cut_PID_h('pr2'),
    cut_PID_h('pr3'),
  ],
}

#-----#
# GEC #
#-----#

cuts_gec = {ch:'nSPDhits<=600' for ch in DTYPES}

#---------#
# TRIGGER #
#---------#

cuts_trigger = {
  'e_e'  : {
    'lep1_TOS_ELECTRON & lep1_PT>20e3',
    'lep2_TOS_ELECTRON & lep2_PT>20e3',
  },
  'e_mu' : {
    'lep1_TOS_ELECTRON & lep1_PT>20e3',
    'lep2_TOS_MUON     & lep2_PT>20e3',
  },
  'mu_mu': {
    'lep1_TOS_MUON & lep1_PT>20e3',
    'lep2_TOS_MUON & lep2_PT>20e3',
  },
  'e_h1' : { 'lep_TOS_ELECTRON'   },
  'e_h3' : { 'lep_TOS_ELECTRON'   },
  'h1_mu': { 'lep_TOS_MUON'       },
  'h3_mu': { 'lep_TOS_MUON'       },
}

#--------#
# VERTEX #
#--------#

## In addition to h3: Vertex formed.

cuts_vertex = {
  'e_e'   : '1==1',
  'e_h1'  : '1==1',
  'e_mu'  : '1==1',
  'h1_mu' : '1==1',
  'mu_mu' : '1==1',
  'e_h3'  : 'tauh3ch_P>0',
  'h3_mu' : 'tauh3ch_P>0',
}

#------#
# KINE #
#------#

## Find min/max from basic formula
min_pr_PT = '(pr1_PT<=pr2_PT)*((pr1_PT<=pr3_PT)*pr1_PT + (pr1_PT>pr3_PT)*pr3_PT)  + (pr1_PT>pr2_PT)*((pr2_PT<=pr3_PT)*pr2_PT + (pr2_PT>pr3_PT)*pr3_PT)'
max_pr_PT = '(pr1_PT>=pr2_PT)*((pr1_PT>=pr3_PT)*pr1_PT + (pr1_PT<pr3_PT)*pr3_PT)  + (pr1_PT<pr2_PT)*((pr2_PT>=pr3_PT)*pr2_PT + (pr2_PT<pr3_PT)*pr3_PT)'

## The prong's PT can be permuted
cuts_kine_lh3 = [
  'lep_PT   > 20E3',
  'lep_ETA  > 2.0',
  'lep_ETA  < 4.5',
  #
  max_pr_PT+' > 6E3',
  min_pr_PT+' > 1E3',
  #
  'pr1_ETA  > 2.00',
  'pr1_ETA  < 4.50',
  'pr2_ETA  > 2.00',
  'pr2_ETA  < 4.50',
  'pr3_ETA  > 2.00',
  'pr3_ETA  < 4.50',
  #
  'tauh3ch_PT > 12E3',
  'tauh3ch_M  > 700',
  'tauh3ch_M  < 1500',
  #
  'ditauch_M >  30E3',
  # 'ditauch_M < 120E3',
  #
  'tauh3ch_P>0', # success combine
]

## Like cuts_mckine, but simply replace the MC functors to regular functors.
cuts_kine = { key:utils.join(val).replace('_MC','_') for key,val in cuts_mckine.iteritems() }
cuts_kine['e_h3']   = cuts_kine_lh3
cuts_kine['h3_mu']  = cuts_kine_lh3

#===============================================================================

stages_Phil = {
  '00_Fiducial'   : cuts_fiducial,
  '01_Acceptance' : cuts_mckine,
  '02_MCpredict'  : cuts_mcpredict,
  '03_MCrec'      : cuts_mcrec,
  '04_Tracking'   : cuts_track,
  '05_PID'        : cuts_pid,
  '06_Trigger'    : cuts_trigger,
  '07_Vertex'     : cuts_vertex, # optionally to tauh3
  '08_GEC'        : cuts_gec,
  '09_Kinematic'  : cuts_kine,
}

stages_David = {
  '00_Fiducial'   : cuts_fiducial,
  '01_Acceptance' : cuts_mckine,
  #
  '02_MCpredict'  : cuts_mcpredict,
  # '02a_recob'     : cuts_recob,
  #
  '03_MCrec'      : cuts_mcrec,
  '04_Tracking'   : cuts_track,
  '05_Vertex'     : cuts_vertex, # optionally to tauh3
  '06_Kinematic'  : cuts_kine,
  '07_PID'        : cuts_pid,
  '08_GEC'        : cuts_gec,
  '09_Trigger'    : cuts_trigger,
}

stages = stages_David

#===============================================================================

def fraction( n, N ):
  """
  Util func to help print consistent fraction format
  """
  return '{:6d} / {:6d} = {:9.2%}'.format( n, N, 1.*n/N)

@report_info
def calculate_stages_single(dtype):
  """
  The cuts are ACCUMULATIVE.
  """
  # log.info('calculating: %s'%dtype)
  tree    = TREES(dtype)
  cut0    = ''
  res     = {}
  # collect number at each stage first
  for cut in sorted(stages):
    cut0      = utils.join( cut0, cut )
    res[cut]  = tree.GetEntries( cut0 )
  ## collect
  return pd.Series(res)


def calculate_stages():
  acc = pd.DataFrame()
  for dtype in DTYPES:
    acc[dtype] = calculate_stages_single(dtype)
  return acc


@pickle_dataframe_nondynamic
def calculate_stages_detailed():
  """
  Including the detail eff on subparticle.
  Do by hand for now.
  """
  etrack  = lambda se: EffU(se['01_Acceptance'] , se['04_Tracking'])
  ekine   = lambda se: EffU(se['04_Tracking']   , se['06_Kinematic'])
  epid    = lambda se: EffU(se['06_Kinematic']  , se['07_PID'])
  egec    = lambda se: EffU(se['07_PID']        , se['08_GEC'])
  etrig   = lambda se: EffU(se['08_GEC']        , se['09_Trigger'])
  erec    = lambda se: EffU(se['01_Acceptance'] , se['09_Trigger'])

  ## Calculate homogen eff
  df    = calculate_stages()
  funcs = dict(locals())
  keys  = 'etrack', 'ekine', 'epid', 'egec', 'etrig', 'erec'
  eff0  = pd.DataFrame({ key:df.apply(funcs[key]) for key in keys }).T

  ## Replace the ekine
  eff0.loc['ekine'] = calculate_ekine_corrected()

  ## Add the components eff
  df = pd.concat([eff0, expand_etrack(), expand_epid(), expand_etrig()])

  ## Rename, sort, return
  return df.rename(columns=DTYPE_TO_DT)[CHANNELS]


#===============================================================================
# KINEMATIC EFF
#===============================================================================

@pickle_dataframe_nondynamic
def calculate_ekine_raw():
  """
  ekine with correction for migration of particle outside acceptance.
  """
  cuts = [
    'Acceptance',
    'Acceptance & MCpredict',
    'Acceptance & MCpredict & MCrec',
    'Acceptance & MCpredict & MCrec & Tracking',
    'Acceptance & MCpredict & MCrec & Tracking & Kinematic',
    'Acceptance & MCrec',
    'Acceptance & MCrec & Tracking',
    'Acceptance & MCrec & Tracking & Kinematic',
    'MCpredict',
    'MCpredict & MCrec',
    'MCpredict & MCrec & Tracking',
    'MCpredict & MCrec & Tracking & Kinematic',
    'MCrec',
    'MCrec & Tracking',
    'MCrec & Tracking & Kinematic',
    'Tracking',
    'Tracking & Kinematic',
    'Kinematic',
  ]
  labels = 'A','AP','API','APIT','APITK','AI','AIT','AITK','P','PI','PIT','PITK','I','IT','ITK','T','TK','K'

  ## Calculate
  acc = pd.DataFrame()
  for dtype in DTYPES:
    logger.info('Counting: '+dtype)
    t = TREES(dtype)
    acc[dtype] = [ t.GetEntries(cut) for cut in cuts ]
  acc.index = labels
  return acc


@pickle_dataframe_nondynamic
def ekine_syst():
  """
  Compute the systematic uncertainty of ekine based on alpha-variation 1+-0.005
  from Z->ee 8TeV paper.
  """
  cuts = {
    'e_h1': 'lep_PT > 20e3 * {0}',
    'e_h3': 'lep_PT > 20e3 * {0}',
    'e_e' : utils.join({
      ('lep1_PT > 20e3 * {0}', 'lep2_PT > 5e3 *{0}'),
      ('lep2_PT > 20e3 * {0}', 'lep1_PT > 5e3 *{0}'),
    }),
    'e_mu' : utils.join({
      ('lep1_PT > 20e3 * {0}', 'lep2_PT > 5e3'), # hard e, soft mu
      ('lep1_PT > 5e3 * {0}', 'lep1_PT > 20e3'), # soft e, hard mu
    }),
  }

  ## Loop over each channels of electron
  cut0 = 'Acceptance & MCpredict & MCrec & Tracking'
  acc  = pd.Series()
  for dtype, cut in cuts.iteritems():
    t = TREES(dtype)
    vnom = 1. * t.GetEntries(utils.join(cut0, cut.format(1.0)))
    vhi  = 1. * t.GetEntries(utils.join(cut0, cut.format(1.0-0.005)))
    vlow = 1. * t.GetEntries(utils.join(cut0, cut.format(1.0+0.005)))
    syst = max(1. - vlow/vnom, vhi/vnom - 1.)
    acc[dtype] = ufloat(1.0, syst)

  ## Package & return
  acc['mu_mu'] = acc['h1_mu'] = acc['h3_mu'] = 1.
  return acc


def calculate_ekine_corrected():
  """
  Final value, with chosen spec & syst.
  """
  df = calculate_ekine_raw()
  return df.apply(lambda se: EffU_unguard(se.APIT, se.PITK)) * ekine_syst()


def latex_ekine_corrected():
  """
  For appendix section
  """
  df0 = calculate_ekine_raw().T
  df  = pd.DataFrame()
  df['`acc`,`trk`']          = df0['APIT'].apply(str)
  df['`acc`, `trk`, `kine`'] = df0['APITK'].apply(str)
  df['`trk`, `kine`']        = df0['PITK'].apply(str)
  df['\\midrule Migration into \\ekine [%]'] = df0.apply(lambda se: EffU(se['PITK'], se['PITK']-se['APITK']), axis=1)
  df['Effective \\ekine [%]'] = calculate_ekine_corrected()
  df.iloc[:,-2] = df.iloc[:,-2].fmt2lp
  df.iloc[:,-1] = df.iloc[:,-1].fmt2lp
  #
  df.columns.name = ''
  df = df.rename(DTYPE_TO_DT).rename(DT_TO_LATEX).T
  return df.to_markdown(stralign='right')


#===============================================================================
# EFF OF COMPONENTS
#===============================================================================

# my fault, I used different set of nickname between kinematic.py and rec.py
NICKNAMES_KIN_REC = {
  'mu_mu': {
    'lep1': 'mu1',
    'lep2': 'mu2',
  },
  'e_e': {
    'lep1': 'e1',
    'lep2': 'e2',
  },
  'e_mu': {
    'lep1': 'e',
    'lep2': 'mu',
  },
  'h1_mu': {
    'lep': 'mu',
    'had': 'pi',
  },
  'e_h1': {
    'lep': 'e',
    'had': 'pi',
  },
  'h3_mu': {
    'lep': 'mu',
  },
  'e_h3': {
    'lep': 'e',
  },
}

@report_info
def expand_etrack_single(dtype):
  """
  Show the etrack of all components in given dtype.

  old: etrack = Tracking / Acceptance
  new: etrack = (MCpredict/Acceptance) * (Tracking/MCrec)
       (attempt to suppress MCmatch efficiency)

  Do both old/new here to get a comparison
  """
  cuts0 = 'Fiducial', 'Acceptance'
  tree  = TREES(dtype)

  def count(*args):
    return tree.GetEntries(utils.join( cuts0, args ))

  n_acc     = count('')
  acc       = pd.Series()
  acc.name  = dtype
  for obj in reco_objects[dtype]:
    ## With MCPredict
    # n_mcpred  = count(cut_mcpredict(obj))
    # n_mcrec   = count(cut_mcpredict(obj), cut_mcrec(obj))
    # n_track   = count(cut_mcpredict(obj), cut_mcrec(obj), cut_track(obj))
    # eff_new   = EffU( n_acc, n_mcpred ) * EffU( n_mcrec, n_track) # new

    ## With MCPredict #2
    n_track = count(cut_mcpredict(obj), cut_mcrec(obj), cut_track(obj))
    eff     = EffU(n_acc, n_track)

    # ## Without MCPredict
    # n_track = count(cut_mcrec(obj), cut_track(obj))
    # eff     = EffU(n_acc, n_track)

    ## bind
    key       = 'etrack_' + NICKNAMES_KIN_REC[dtype].get(obj, obj)
    acc[key]  = eff

  ## Extra for h3, calculate 3-prongs together
  if 'h3' in dtype:
    prongs    = 'pr1', 'pr2', 'pr3'
    cuts      = []
    cuts     += [ cut_mcpredict(x) for x in prongs ]
    cuts     += [ cut_mcrec(x) for x in prongs ]
    cuts     += [ cut_track(x) for x in prongs ]
    n_track   = count(cuts)
    eff       = EffU(n_acc, n_track) # old
    # eff_new   = EffU(n_acc, n_mcpred) * EffU(n_mcrec, n_track) # new
    acc['etrack_tauh3'] = eff

  ## Finally
  return acc


def expand_etrack():
  return pd.DataFrame({ch:expand_etrack_single(ch) for ch in DTYPES})


def calc_etrack_h1_h3_ntracks():
  """
  Calculate the tracking eff of tauh1, tauh3, including the correlation,
  as a function of nTracks.
  For the sake of plotting only, not for computation.
  To be plotted by `tracking_hadron` module.
  """
  ## Borrow the binning & plotting tools
  import rec_utils
  xarr = list(pd.np.linspace(0, 400, 8+1)) + [500, 600]

  def calc(dt, *cuts):
    ## Slice the tree
    tree = TREES(dt)
    tree.SetAlias('trkpr', utils.join(cuts))
    tree.Draw('trkpr: nTracks', 'Fiducial & Acceptance', 'goff para')
    df = tree.sliced_dataframe()

    ## Binned result
    deno = rec_utils.binning_1D(df             , nTracks=xarr)
    nume = rec_utils.binning_1D(df[df.trkpr==1], nTracks=xarr)
    eff  = rec_utils.divide_clopper_pearson(nume.fillna(0.), deno.fillna(1.))
    eff *= ufloat(1, 0.014)  # material syst
    return eff

  ## Perform calculation with h1, h3
  return pd.DataFrame({
    'tauh1': calc('h1mu', cut_track('had')),
    'tauh3': calc('h3mu', cut_track('pr1'), cut_track('pr2'), cut_track('pr3')),
  })

#-------------------------------------------------------------------------------

@report_info
def expand_epid_single(dtype):
  ## MUST BE SAME ORDER to zip with reco_objects
  cutter_PID = {
    'e_e'   : [cut_PID_e  , cut_PID_e],
    'e_mu'  : [cut_PID_e  , cut_PID_mu],
    'mu_mu' : [cut_PID_mu , cut_PID_mu],
    'e_h1'  : [cut_PID_e  , cut_PID_h],
    'h1_mu' : [cut_PID_mu , cut_PID_h],
    'e_h3'  : [cut_PID_e  , cut_PID_h, cut_PID_h, cut_PID_h],
    'h3_mu' : [cut_PID_mu , cut_PID_h, cut_PID_h, cut_PID_h],
  }

  ## Start right before PID.
  filt = [
    'Fiducial'  ,
    'Acceptance',
    # 'MCpredict' ,
    'MCrec'     ,
    'Tracking'  ,
    'Kinematic' ,
    'Vertex'    ,
  ]
  tree      = TREES(dtype)
  counter   = lambda x: tree.GetEntries(utils.join( filt, x ))
  n0        = counter('')
  acc       = pd.Series()
  acc.name  = dtype
  for obj,cutter in zip(reco_objects[dtype], cutter_PID[dtype]):
    n1  = counter(cutter(obj))
    key = 'epid_' + NICKNAMES_KIN_REC[dtype].get(obj, obj)
    acc[key] = EffU( n0, n1 )

  ## Extra for h3
  if 'h3' in dtype:
    prongs  = 'pr1', 'pr2', 'pr3'
    n1      = counter([ cut_PID_h(x) for x in prongs ])
    acc['epid_tauh3'] = EffU(n0, n1)

  return acc

def expand_epid():
  return pd.DataFrame({ch:expand_epid_single(ch) for ch in DTYPES})


#-------------------------------------------------------------------------------

def expand_etrig():
  """
  Only for dilepton channel having 2 trigger possibilities
  """
  filt = utils.join(
    'Fiducial'  ,
    'Acceptance',
    # 'MCpredict' ,
    'MCrec'     ,
    'Tracking'  ,
    'Kinematic' ,
    'Vertex'    ,
    'PID'       ,
    'GEC'       ,
  )

  l_hi_TOS    = '(lep1_PT>=lep2_PT)*(lep1_TOS_{0} & lep1_PT>20e3) + (lep2_PT>=lep1_PT)*(lep2_TOS_{0} & lep2_PT>20e3)'.format
  l_low_TOS   = '(lep1_PT <lep2_PT)*(lep1_TOS_{0} & lep1_PT>20e3) + (lep2_PT <lep1_PT)*(lep2_TOS_{0} & lep2_PT>20e3)'.format

  ## Do manually in each channel
  tree  = TREES('mu_mu')
  deno  = tree.GetEntries(filt)
  nhi   = tree.GetEntries(utils.join( filt, l_hi_TOS ('MUON')))
  nlow  = tree.GetEntries(utils.join( filt, l_low_TOS('MUON')))
  # print 'mumu_mu1:', fraction( nhi , deno )
  # print 'mumu_mu2:', fraction( nlow, deno )
  mumu  = pd.Series({'etrig_mu1': EffU( deno, nhi ), 'etrig_mu2': EffU( deno, nlow )})

  tree  = TREES('e_e')
  deno  = tree.GetEntries(filt)
  nhi   = tree.GetEntries(utils.join( filt, l_hi_TOS ('ELECTRON')))
  nlow  = tree.GetEntries(utils.join( filt, l_low_TOS('ELECTRON')))
  # print 'ee_e1:', fraction( nhi , deno )
  # print 'ee_e2:', fraction( nlow, deno )
  ee    = pd.Series({'etrig_e1': EffU( deno, nhi ), 'etrig_e2': EffU( deno, nlow )})


  tree  = TREES('e_mu')
  deno  = tree.GetEntries(filt)
  ne    = tree.GetEntries(utils.join( filt, 'lep1_TOS_ELECTRON', 'lep1_PT>20e3' ))
  nmu   = tree.GetEntries(utils.join( filt, 'lep2_TOS_MUON'    , 'lep2_PT>20e3' ))
  # print 'emu_e :', fraction( ne , deno )
  # print 'emu_mu:', fraction( nmu, deno )
  emu   = pd.Series({'etrig_e': EffU( deno, ne ), 'etrig_mu': EffU( deno, nmu )})

  ## Combine
  df = pd.DataFrame({'mu_mu':mumu, 'e_e':ee, 'e_mu':emu})
  return df


#===============================================================================
# MIS-IDENTIFICATION
#===============================================================================

def main_electron_misid():
  """
  Determine the mis-id rate that electron is identified as hadron.
  Used for the calculation of Zee background for ditaueh1.

  It is more precise to check this after selection (i.e., with isolation too.)
  """

  dtype = 'e_h1'
  key   = 'lep'
  tree  = TREES(dtype)
  count = lambda *args: tree.GetEntries(utils.join(args))

  # ## Remove PID,trigger, but keep kinematic cut
  # filt = []
  # for name, cuts in stages.iteritems():
  #   if ('PID' not in name) and ('Trigger' not in name):
  #     filt.append(cuts[dtype])

  # # cuts  = ALL_CUTS(dtype)
  # # del cuts[5] # remove PID
  # # del cuts[5] # remove Trigger
  # # filt  = utils.join(cuts)

  ## David's stages
  filt  = [
    'Fiducial',
    # '01_Acceptance',
    'MCpredict',
    'MCrec',
    'Tracking',
    'Vertex',
    # 'Kinematic',
    #
    'GEC',
    # '09_Trigger',
  ]

  cute = cut_PID_e(key)
  cuth = cut_PID_h(key)
  cutm = cut_PID_mu(key)

  print '## Determine electron mis-id'
  print 'eh1 without PID/trig      :', count(filt)
  print 'eh1 without PID/trig, any :', count(filt, {cute, cuth, cutm})
  print 'electron id correctly     :', count(filt, cute )
  print 'electron id as hadron     :', count(filt, cuth )
  print 'electron id as muon       :', count(filt, cutm )


def main_muon_misid():
  """
  When muon is misid as elec,had.

  Note: May not be so correct as the rate may change with offline selection
  (e.g. isolation)
  """
  # dtype = 'e_e'
  # dtype = 'e_h1'
  # dtype = 'h1_mu'
  dtype = 'mu_mu'
  key   = 'lep2'
  # key   = 'lep'
  tree  = TREES(dtype)
  count = lambda *args: tree.GetEntries(utils.join(args))

  # cuts  = ALL_CUTS(dtype)
  # ## Remove PID,trigger, but keep kinematic cut
  # del cuts[5] # remove PID
  # del cuts[5] # remove Trigger

  ## David's stages
  filt  = [
    'Fiducial',
    'Acceptance',
    'MCpredict',
    'MCrec',
    'Tracking',
    # 'Vertex',
    # 'Kinematic',
    # 'GEC',
    # 'Trigger',
    # 'TMath::Abs(lep_PT-had_PT)/(lep_PT+had_PT) < 0.6', # APT
    # 'TMath::Abs(TMath::ACos(TMath::Cos(lep_PHI-had_PHI))) > 2.7', # DPHI
  ]

  narrow_eta = utils.join(key+'_ETA>2.00', key+'_ETA<4.50', key+'_PT>10e3')

  print '## Determine muon mis-id'
  print 'h1mu before PID        :', count(filt)
  print 'h1mu, NoPID, narrow eta:', count(filt, narrow_eta)
  print 'muon id correctly      :', count(filt, cut_PID_mu(key))
  print 'muon id as hadron      :', count(filt, cut_PID_h(key))
  print 'muon id as hadron (nar):', count(filt, cut_PID_h(key), narrow_eta)
  print 'muon id as electron    :', count(filt, cut_PID_e(key))
  # print 'else', count(filt, '!' )

#===============================================================================

def main_tracking_muon_correction():
  """
  Determine the small difference between my tracking cut, and what used in
  high-PT muon study by sfarry.

  MINE  : ISLONG, TRPCHI2 > 0.01
  SFARRY: ISLONG, TRPCHI2 > 0.001, PRERR < 0.1
  """

  passing_mine = utils.join(
    '00_Fiducial',
    '01_Acceptance',
    cut_mcpredict('{0}'),
    cut_mcrec('{0}'),
    '{0}_TRTYPE==3',
    '{0}_TRPCHI2>0.01',
  ).format

  passing_sfarry = utils.join(
    '00_Fiducial',
    '01_Acceptance',
    cut_mcpredict('{0}'),
    cut_mcrec('{0}'),
    '{0}_TRTYPE==3',
    '{0}_TRPCHI2>0.001',
    '{0}_PERR2/{0}_P/{0}_P < 0.01',
  ).format

  queue = [
    ( 'mu_mu', 'lep1' ),
    ( 'mu_mu', 'lep2' ),
    ( 'h1_mu', 'lep'  ),
    ( 'h3_mu', 'lep'  ),
    ( 'e_mu' , 'lep2' ),
  ]
  acc = []
  for dtype, prefix in queue:
    t   = TREES(dtype)
    n1  = t.GetEntries(passing_mine  (prefix))
    n2  = t.GetEntries(passing_sfarry(prefix))
    acc.append([n1,n2])
  n1,n2 = zip(*acc)
  n1 = 1.*sum(n1)
  n2 = 1.*sum(n2)
  print var(n1) / n2


#===============================================================================
# DRAWER
#===============================================================================

def draw_kine_pt(dtype):
  """
  Draw the 2D plot of PT, aim to show bremstrahllung.
  """
  ## Boxes for presel
  xmin = ymin = 0
  xmax = ymax = 60
  arr = {
    'll' : [(xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,5) , (20,5) , (20,20), (5,20), (5,ymax)],
    'lh1': [(xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,10), (20,10), (20,ymax)],
    'lh3': [(xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,12), (20,12), (20,ymax)],
  }[DTYPE_TO_DGROUP[dtype]]
  label0  = 'p_{T}(%s) [GeV/c]'

  ## Post-processor
  def postproc(h):
    ## Tune spacing
    h.anchors.stack.yaxis.titleOffset = 1.1
    ## same box in WEH patch
    drawers.Box(arr).Draw('f')

  ## Start drawing
  h = QHistV3()
  h.prefix   = DTYPE_TO_DT[dtype]
  h.name     = 'kine_pt'
  h.filters  = 'Fiducial & Acceptance & MCpredict & MCrec & Tracking'
  h.trees    = TREES(dtype)
  h.params   = 'PT2:PT1'
  h.xmin     = xmin
  h.ymin     = ymin
  h.xmax     = xmax
  h.ymax     = ymax
  h.option   = 'col'
  h.xlabel   = label0%LABELS_DITAU_SEP[dtype][0]
  h.ylabel   = label0%LABELS_DITAU_SEP[dtype][1]
  h.postproc = postproc
  h.draw()

#===============================================================================

if __name__ == '__main__':
  ## SETUP
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors
  drawers.set_style_square()
  ## Update the cache
  if '--cache' in sys.argv:
    print calculate_stages_detailed()
    print calculate_ekine_raw()
    print ekine_syst()
    sys.exit()

  ## ALL
  # print calculate_stages_single('h3_mu')
  # print calculate_stages()
  # print calculate_stages_detailed().fmt2p

  ## EKINE
  # print ekine_syst().fmt2p
  # print calculate_ekine_raw()
  # print calculate_ekine_corrected().fmt3p

  ## Detailed
  # print expand_etrack_single('e_e')
  # print expand_epid_single('h3_mu')
  # print expand_etrack()
  # print expand_epid()
  # print expand_etrig()

  # main_electron_misid()
  # main_muon_misid()
  # main_hadron_misid_as_muon()
  # main_tracking_muon_correction()

  ## For latex
  # print latex_ekine_corrected()

  ## Drawer
  # draw_kine_pt('e_mu')

  ## To start interactive shell
  # import IPython; IPython.embed()
