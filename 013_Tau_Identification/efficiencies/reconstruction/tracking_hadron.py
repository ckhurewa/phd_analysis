#!/usr/bin/env python

"""

Hadron tracking eff. Based on the cut-and-count approach in kinematic.py,
with correlation between prongs included (i.e., binning not possible),
including the tracking correction factor from LHCb Tracking group

"""

from rec_utils import *
import kinematic
import trk_correction
import tracking_muon_aux
from tracking_bins import BIN_ETA_W, BIN_NTRACKS_LITE, weights_etaW, multieff

## Specifically for tauh1, which starts from 10GeV, and not so high.
BIN_PT_TAUH1_LITE = 10, 15, 20, 30, 300

## Material interaction syst
SYST_MATERIAL = ufloat(1, 0.014)

#===============================================================================

@memorized
def get_decorr_lepton():
  """
  Return estimated decorrelation factor with lepton in the same channel
  This should be quite small
  """
  ## Import source eff, prep handles
  df0  = kinematic.calculate_stages_detailed()
  h1mu = df0['h1mu']
  eh1  = df0['eh1' ]
  h3mu = df0['h3mu']
  eh3  = df0['eh3' ]
  return pd.Series({
    'h1mu_pi': h1mu.etrack / h1mu.etrack_mu / h1mu.etrack_pi,
    'h3mu'   : h3mu.etrack / h3mu.etrack_mu / h3mu.etrack_tauh3,
    'eh1_pi' : eh1.etrack  / eh1.etrack_e   / eh1.etrack_pi,
    'eh3'    : eh3.etrack  / eh3.etrack_e   / eh3.etrack_tauh3,
  })


# @memorized
# def get_syst_TRPCHI2_tauh3():
#   """
#   Compute the systematics from TRPCHI2 disagreement, via tracking muon.
#   Return scalar value, to be used for tauh3 only
#   """
#   w = weights_etaW().applymap(nominal_value) # don't need this here.
#   e = tracking_muon_aux.syst_dd_mc_TRPCHI2() # syst as a func of ETA
#   syst = w.apply(lambda se: (se*e).sum()/se.sum())
#   syst = completely_decorrelate(syst)
#   return pd.Series({
#     'h3mu': syst.h3mu_pr1 * syst.h3mu_pr2 * syst.h3mu_pr3,
#     'eh3' : syst.eh3_pr1  * syst.eh3_pr2  * syst.eh3_pr3,
#   })

#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_ntracks():
  """
  Draw the simultaneous-etrack of tauh3, as a func of nTracks,
  with out including the muon's tracking
  """
  ## Grab the calculated eff from direct-MC module
  import kinematic
  eff = kinematic.calc_etrack_h1_h3_ntracks()
  eff = eff.rename(columns={'tauh1': '#tau_{h1}','tauh3': '#tau_{h3}'})

  ## Draw
  gr,leg = drawer_graphs_eff(eff)
  ROOT.gPad.logx  = False
  gr.minimum      = 0.0
  gr.maximum      = 1.02
  gr.xaxis.limits = -10, 610
  gr.xaxis.title  = 'nTracks'
  gr.yaxis.title  = '#varepsilon_{track,h}'
  leg.header      = 'LHCb-Simulation'
  leg.x1NDC       = 0.18
  leg.x2NDC       = 0.50
  leg.y1NDC       = 0.18
  leg.y2NDC       = 0.40
  leg.textSize    = 0.07


#===============================================================================
# FINAL
#===============================================================================

@memorized
def efficiencies_tauh1():
  """
  Channel-dependent (just h1mu_pi, eh1_pi) tracking eff from MC.
  With the systematics applied
  """
  ## Coverage
  tags = ['h1mu_pi', 'eh1_pi']

  ## Pull out uncorrected eff map
  # 170408: changed from litediv to fulldiv 
  eff = multieff('etaW_nlite').loc[Idx['fulldiv'], tags].swaplevel().sort_index()

  ## Resolve correlation with lepton, this should be quite small
  corr = get_decorr_lepton()[tags]

  ## Syst: TRPCHI2 disagreement, as a function of ETA.
  # syst1 = tracking_muon_aux.syst_dd_mc_TRPCHI2()

  ## Syst: Material interaction. Correlated between channels
  syst2 = pd.Series({tag:SYST_MATERIAL for tag in tags})

  ## Finally
  def collapse(se):
    dt = se.name
    df = se.unstack().T
    res = df * syst2[dt] * corr[dt]
    return res.stack()
  return eff.apply(collapse)


@memorized
def efficiencies_tauh3():
  """
  Return eff as a function of channel. Only for h3mu, eh3.
  Precomputed, but no tracking correction applied.

  List of syst
  > TRPCHI2 disagreement, via muon tracking eff
  > Per-track syst from material interaction, ~0.014%
  """
  ## Coverage
  tags = ['h3mu', 'eh3']

  ## Pull out uncorrected eff
  df0 = kinematic.calculate_stages_detailed()
  eff = pd.Series({tag:df0[tag].etrack_tauh3 for tag in tags})
  
  ## Resolve correlation with lepton, this should be quite small
  corr = get_decorr_lepton()[tags]

  ## Syst: TRPCHI2 disagreement
  # syst1 = get_syst_TRPCHI2_tauh3()[tags]

  ## Syst: Material interaction. Correlated between channels
  syst_mat = SYST_MATERIAL*3 - 2 # for 3 tracks
  syst2 = pd.Series({dt:syst_mat for dt in tags})

  ## Finally
  return eff * corr * syst2

#-------------------------------------------------------------------------------

def lookup_tauh1(*args):
  eff = efficiencies_tauh1()
  return lookup_eff_generic(__file__, eff, _ETA=BIN_ETA_W, nTracks=BIN_NTRACKS_LITE)(*args)  

#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print weights_etaW()
  # print get_syst_TRPCHI2_tauh3()
  # print weight_eta_p_correction()
  # print eff_eta_raw().loc['litediv', ['h1mu_pi', 'eh1_pi']]
  # print eff_eta_pt_raw().loc['litediv', ['h1mu_pi', 'eh1_pi']]

  ## DRAW
  # draw_eff_ntracks()

  ## Finally
  print efficiencies_tauh1()
  print efficiencies_tauh3()
  print lookup_tauh1()
