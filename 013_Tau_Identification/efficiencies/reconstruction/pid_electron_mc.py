#!/usr/bin/env python

from rec_utils import *
from pid_electron_bins import *

#===============================================================================
# EFF
#===============================================================================

@pickle_dataframe_nondynamic
def multieff():
  """
  Has all schema. Completely correlated.
  """
  acc = {}
  for tag, scheme in SCHEMES_2D.iteritems():
    logger.info('Binning %s'%tag)
    nume, deno = calc_epid_mc_eta_pt_spd_raw('electrons', bin_eta=scheme['ETA'], bin_pt=scheme['PT'])
    eff        = divide_clopper_pearson(nume.stack(), deno.stack())
    acc[tag]   = completely_correlate(eff)
  return pd.DataFrame(acc)

@memorized
def multieff_addedsyst():
  """
  Attach the additional per-electron systematics from Z->ee analysis
  Still has all schema, but collapse items to 'electrons' as they are in good agreement
  """
  mult = lambda x: x*ufloat(1, 0.007) if not pd.np.isnan(nominal_value(x)) else x
  return multieff().applymap(mult).xs('electrons', level=2)

@memorized
def eff_eta_pt_addedsyst():
  """
  The nominal version, also channel-independent.
  Mostly for the sake of drawing
  """
  return sort_intv_pt(multieff_addedsyst()['nominal'].dropna().unstack())


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_eta_pt_addedsyst():
  eff = eff_eta_pt_addedsyst()
  gr,leg = drawer_graphs_eff_pt(eff)
  gr.xaxis.title = 'p_{T}(e) [GeV]'
  gr.yaxis.title = '#varepsilon_{PID, e}'
  gr.xaxis.moreLogLabels  = True
  gr.xaxis.rangeUser      = 2,80
  gr.xaxis.labelOffset    = -0.01
  gr.maximum    = 1.1
  gr.minimum    = 0.0
  leg.header    = 'LHCb-Simulation'
  leg.x1NDC     = 0.16
  leg.x2NDC     = 0.50
  leg.y1NDC     = 0.2
  leg.y2NDC     = 0.65

@gpad_save
def draw_eff_pt_eta():
  """
  xaxis in eta.
  Collapse some PT bin together for clarity, naive average
  """
  eff  = pd.DataFrame()
  eff0 = eff_eta_pt_addedsyst().T
  eff['(5, 10] GeV']  = (eff0['(5, 7]']   + eff0['(7, 10]'])/2
  eff['(10, 20] GeV'] = (eff0['(10, 15]'] + eff0['(15, 20]'])/2
  eff['(20, 60] GeV'] = (eff0['(20, 25]'] + eff0['(25, 30]'] + eff0['(30, 40]'] + eff0['(40, 60]'])/4
  eff.columns.name = 'PT'
  #
  gr,leg = drawer_graphs_eff_pt(eff)
  gr.xaxis.title = '#eta(e)'
  gr.yaxis.title = '#varepsilon_{PID, e}'
  gr.maximum    = 1.1
  gr.minimum    = 0.0
  leg.header    = 'LHCb-Simulation'
  leg.x1NDC     = 0.35
  leg.x2NDC     = 0.75
  leg.y1NDC     = 0.20
  leg.y2NDC     = 0.45
  ROOT.gPad.SetLogx(False)


@gpad_save
def draw_eff2D_mc_eta_pt():
  ROOT.gStyle.SetPalette(ROOT.kBird)
  ROOT.gStyle.SetPaintTextFormat(".4f")

  eff = eff_eta_pt_addedsyst()
  h   = ROOT.TH2F.from_uframe(eff)
  h.Draw('texte colz')
  h.markerSize          = 1.0 # text size
  h.xaxis.title         = '#eta(e)'
  h.yaxis.title         = 'p_{T}(e) [GeV]'
  h.yaxis.titleOffset   = 1.1
  h.yaxis.labelOffset   = 0.002
  h.yaxis.rangeUser     = 0, 8
  ROOT.gPad.rightMargin = 0.12
  ROOT.gPad.leftMargin  = 0.16
  ROOT.gPad.Update()


@gpad_save
def draw_eff_eta():
  """
  Use the 2D eff above, repr only in eta. Use channel eh1.
  """
  w   = weights_pt_eta()['eh3_e'].unstack()
  e   = eff_eta_pt_addedsyst()
  eff = (w*e).sum()/w.sum()
  gr, leg = drawer_graphs_eff_pt(pd.DataFrame({'eff':eff}))
  gr.maximum = 1.0
  gr.minimum = 0.5
  gr.xaxis.title = '#eta'
  ROOT.gPad.SetLogx(False)


#===============================================================================
# FINALLY
#===============================================================================

@memorized
def scalar_results():
  """
  Cmopute eff at nominal for compare with dd
  """
  weights = multiweights_pt_eta()['nominal'].dropna().unstack()
  e       = multieff_addedsyst()['nominal'].dropna()
  return weights.apply(lambda w: (w*e).sum()/w.sum())

#-------------------------------------------------------------------------------

def lookup(*args):
  eff = eff_eta_pt_addedsyst()
  return lookup_eff_generic(__file__, eff, _PT=BIN_PT, _ETA=BIN_ETA)(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print sort_intv_pt(multieff()['nominal'].unstack()['electrons'].dropna().unstack()).fmt2p
  # print multieff()
  # print multieff_addedsyst()
  # print eff_eta_pt_addedsyst()

  # draw_eff_eta_pt_addedsyst() # xaxis = PT
  # draw_eff_pt_eta() # xaxis = ETA
  # draw_eff2D_mc_eta_pt()
  # draw_eff_eta()

  ## Finally
  print scalar_results().fmt2p
  # print lookup().unstack().fmt2p
  # print sort_intv_pt(lookup().unstack()).applymap(lambda u: u.rerr*100) # rerr
