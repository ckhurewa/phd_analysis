#!/usr/bin/env python

from PyrootCK import *
sys.path.append('..')
from rec_utils import *

## Fit output file
FIT_OUTPUT = 'fit.root'

#===============================================================================

def load_data(w):
  ## POI
  w.factory('Jpsi_M    [2400, 3600]')
  w.factory('PT        [1   , 300 ]')
  w.factory('ETA       [2.0 , 4.5 ]')
  w.factory('nTracks   [0   , 600 ]')
  w.factory('passed    [0, 1]')
  w.defineSet('params', 'Jpsi_M,PT,ETA,nTracks,passed')
  w.defineSet('POIs'  , 'Jpsi_M')
  poi = w.var('Jpsi_M')
  poi.title = 'mass(J/#psi)'
  poi.unit  = 'MeV/c^{2}'


  ## Import data
  tree = import_tree('electron', 'tuple.root')
  ds   = ROOT.RooDataSet('dataset', 'dataset', tree, w.set('params'))
  dh   = ROOT.RooDataHist('datahist', 'datahist', w.set('POIs'), ds)
  return ds, dh

#===============================================================================

def prepare_model_nobrem(w, nentries):
  ## Obsv
  w.factory('nsig [%.2f, 0, %i]'%(0.90*nentries, nentries ))
  w.factory('nbkg [%.2f, 0, %i]'%(0.10*nentries, nentries ))

  ## Signal
  w.factory('sig_mu    [3056, 3025, 3075]')
  w.factory('sig_sigma [45. , 1.  , 250.]')
  w.factory('sig_alpha [0.7 , 0.0 , 5.0 ]')
  w.factory('sig_n     [5.0]') # const
  w.factory('CBShape : sigModel(Jpsi_M, sig_mu, sig_sigma, sig_alpha, sig_n)')

  ## Background
  # w.factory('bkg_poly_c1 [-0.9, -1.5, 0.]')
  w.factory('bkg_poly_c1 [0., 0., 1.0]') # override
  w.factory('Chebychev: bkgModel(Jpsi_M, bkg_poly_c1)')

  ## Finally
  w.factory('SUM: model(nsig*sigModel, nbkg*bkgModel)')


#-------------------------------------------------------------------------------

def prepare_model_withbrem(w, nentries):
  raise NotImplementedError('Unknown slope')

  ## Obsv
  w.factory('nsig [%.2f,  0, %i]'%(0.95*nentries, nentries ))
  w.factory('nbkg [-5  , -{0:.2f}, {0:.2f}]'.format(0.05*nentries)) # around zero

  ## Signal
  w.factory('sig_mu          [3056, 3025, 3075]')
  w.factory('sig_sigma_left  [ 45.,  1.0, 250.]')
  w.factory('sig_sigma_right [ 100,  1.0, 250.]')
  w.factory('sig_alpha_left  [ 0.6 ]') # const
  w.factory('sig_alpha_right [ 1.6,  0.0, 5.  ]')
  w.factory('sig_n_left      [ 5.0 ]') # const
  w.factory('sig_n_right     [ 1.0 ]') # const
  w.factory('sig_frac_left   [0.75,  0.0, 1.0 ]')

  w.factory('expr   : neg_mass    ("-Jpsi_M", Jpsi_M)')
  w.factory('expr   : neg_mu      ("-sig_mu", sig_mu)')
  w.factory('CBShape: sig_cb_left (Jpsi_M   , sig_mu, sig_sigma_left , sig_alpha_left , sig_n_left)' )
  w.factory('CBShape: sig_cb_right(neg_mass , neg_mu, sig_sigma_right, sig_alpha_right, sig_n_right)')
  w.factory('SUM    : sigModel    (sig_frac_left * sig_cb_left, sig_cb_right)')

  ## Background
  w.factory('bkg_poly_c1[?]')
  w.factory('Chebychev: bkgModel(Jpsi_M, bkg_poly_c1)')

  ## Finally
  w.factory('SUM: model(nsig*sigModel, nbkg*bkgModel)')


#===============================================================================

def execute_fit(w, ds, dh):
  ## Fit to model
  model = w.pdf('model')
  res   = model.fitTo(dh, sumW2Error=False, save=True, printLevel=1)
  w.Import(res)

  ## sWeight
  splot = ROOT.RooStats.SPlot("sPlot", "An sPlot", ds, model, model.coefList())
  rds_withWeights = splot.GetSDataSet()
  rds_withWeights.name = 'dataset_with_weights'
  w.Import(rds_withWeights)


#===============================================================================

def main_fit():
  # Create new Workspace
  w = ROOT.RooWorkspace('electron')

  ## Load data
  ds, dh = load_data(w)
  n = ds.numEntries()

  ## Prepare model
  prepare_model_nobrem(w, n)
  w.Print()
  w.PrintVars()

  ## Start fit
  execute_fit(w, ds, dh)

  ## Finally, save
  fout = ROOT.TFile(FIT_OUTPUT, 'recreate')
  fout.cd()
  w.Write()
  fout.Close()

#===============================================================================

def main_export_tree():
  """
  Load the new dataset and convert to TTree instead.
  """
  import ROOT
  fin = ROOT.TFile(FIT_OUTPUT)
  w  = fin.Get('electron')
  ds = w.data('dataset_with_weights')
  ds.name = 'electron'

  ## It'll be nice to have this dynamically
  fields1 = 'Jpsi_M', 'PT', 'ETA', 'nTracks', 'passed'
  fields2 = 'nsig_sw', 'L_nsig', 'nbkg_sw', 'L_nbkg'
  fields  = fields1 + fields2

  ## Ready, fire
  dataset_to_TFile(ds, fields)


#===============================================================================

if __name__ == '__main__':
  # main_fit()
  main_export_tree()