
#===============================================================================
# ROOFIT BACKGROUND REMOVAL
#===============================================================================

def perform_fit():
  ## Init workspace
  w = ROOT.RooWorkspace('electron')
  w.factory('B_M    [5000, 5500]') # 5279.26 +- 0.17
  w.factory('Jpsi_M [2800, 3200]') # 3096.92 +- ?
  w.factory('PT     [1   , 30  ]')
  w.factory('ETA    [2.0 , 4.5 ]')
  w.factory('nTracks[0   , 500 ]')
  w.factory('passed [0, 1]')
  w.factory('state  [before, after]')
  w.defineSet('params', 'B_M,Jpsi_M,PT,ETA,passed,nTracks')
  w.defineSet('POI'   , 'Jpsi_M')
  POI = w.set('POI').first()
  POI.bins = 50

  ## define model: BKG + SIGNAL
  w.factory('nsig0 [0   , 1e7 ]')
  w.factory('nsig  [0   , 1e7 ]')
  w.factory('nbkg0 [1, 0, 1e6 ]')
  w.factory('nbkg  [1, 0, 1e6 ]')
  # w.factory('mean  [5220, 5340]') # B_M
  w.factory('mean  [3047, 3117]') # Jpsi
  w.factory('sigma [  20,   0, 200 ]')
  w.factory('width [  10,   0,  20 ]') # Voigtian only
  w.factory('slope [-0.1, -1,   0 ]')
  w.factory('alpha [1., 0.1, 10  ]') # CB only
  w.factory('scale [1., 0.5,  4  ]') # CB only
  #
  w.factory('Exponential:pbkg(%s, slope)'%POI.name)
  # w.factory('Gaussian:   psig(%s, mean, sigma)'%POI.name)
  # w.factory('Voigtian:   psig(%s, mean, width, sigma )'%POI.name)
  w.factory('CBShape:    psig(%s, mean, sigma, alpha, scale)'%POI.name)
  #
  w.factory('SUM:esig0 (nsig0 * psig)')
  w.factory('SUM:ebkg0 (nbkg0 * pbkg)')
  w.factory('SUM:esig  (nsig  * psig)')
  w.factory('SUM:ebkg  (nbkg  * pbkg)')
  w.factory('SUM:model0(nsig0 * psig, nbkg0 * pbkg)')
  w.factory('SUM:model (nsig  * psig, nbkg  * pbkg)')
  w.factory('SIMUL:PDF (state, before=model0, after=model)')

  ## all params & snapshot
  w.defineSet('fit_params', 'nsig0,nsig,nbkg0,nbkg,mean,sigma,alpha,scale,slope') # CB
  # w.defineSet('fit_params', 'nsig0,nsig,nbkg0,nbkg,mean,sigma,width,slope') # Voig
  # w.defineSet('fit_params', 'nsig0,nsig,nbkg0,nbkg,mean,sigma,slope') # Gauss
  w.saveSnapshot('init', w.set('fit_params'))

  ## Import data
  tree = import_tree('electron', 'tuple.root')
  ds0  = ROOT.RooDataSet('ds0', 'ds0', tree, w.set('params'), utils.join(
    'B_M-Jpsi_M > 2130',
    'B_M-Jpsi_M < 2230',
  ))

  ## Save model & prepare output
  fout = ROOT.TFile(FIT_OUTPUT, 'recreate')

  ## Loop over different schemes & bin
  gen = gen_selections(**SCHEMES_2D['full'])
  for key, selection in gen:
    ds_before = ds0.reduce(selection)
    ds_after  = ds_before.reduce('passed')
    data = ROOT.RooDataSet('data', 'data', w.set('params'),
      ROOT.RooFit.Index(w.cat('state')),
      ROOT.RooFit.Import({'before':ds_before, 'after':ds_after}),
    ) # cannot use kwargs here as the order of arg matters

    ## Import to workspace as RooDataHist, for later plot
    dh_before = ROOT.RooDataHist('dh_before_'+key, 'before', w.set('POI'), ds_before)
    dh_after  = ROOT.RooDataHist('dh_after_'+key , 'after' , w.set('POI'), ds_after)
    w.Import(dh_before)
    w.Import(dh_after)

    ## Execute fitting, if it's adequate
    n0 = 1. * ds_before.numEntries()
    n1 = 1. * ds_after.numEntries()
    logger.info('Stats: %s: %i --> %i'%(key, n0, n1))
    if n1 > 100: # 5%
      # massaging range of nsig, nbkg with given data
      # note that the bound is NOT part of the snapshot
      w.var('nsig0').min = 0   # push min to 0 first, to avoid newmax < oldmin
      w.var('nsig').min  = 0
      w.var('nsig0').max = n0  # start with max first, otherwise min error
      w.var('nsig').max  = n1
      w.var('nbkg0').max = n0
      w.var('nbkg').max  = n1
      # w.var('nsig0').min = n0 * 0.7 # expect min purity
      # w.var('nsig').min  = n1 * 0.7
      w.var('nsig0').val = n0 * 0.4 # starting point
      w.var('nsig').val  = n1 * 0.4


      ## start fitting
      model = w.pdf('PDF')
      res = model.fitTo(data, save=True)
      res.Write('fit_result_'+key)

      frame = POI.frame()
      dh_after.plotOn(frame)
      ## BKG & SIG
      w.pdf('ebkg').plotOn(frame,
        normalization = (1.0, ROOT.RooAbsReal.RelativeExpected),
        lineColor     = ROOT.kRed,
      )
      w.pdf('esig').plotOn(frame,
        normalization = (1.0, ROOT.RooAbsReal.RelativeExpected),
        lineColor     = ROOT.kBlue,
        lineStyle     = 2,
      )
      w.pdf('model').plotOn(frame,
        ROOT.RooFit.Normalization(1.0, ROOT.RooAbsReal.RelativeExpected),
        ROOT.RooFit.LineColor(ROOT.kBlack),
      )
      frame.Draw()
      exit()

      # finally, reset paramss
      w.loadSnapshot('init')


  ## finally, save fit results, DataHist
  fout.Close()
  w.writeToFile(FIT_OUTPUT, False) # recreate
