#!/usr/bin/env python

from PyrootCK import *
sys.path.append('..')
from rec_utils import *
ROOT.RooFit # trigger patch
ROOT.gROOT.batch = True

#===============================================================================

def draw_contribution():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Load
  fin = ROOT.TFile('fit.root')
  w   = fin['electron']
  w.Print()
  w.PrintVars()

  ## Pre-draw prep
  poi = w.var('Jpsi_M')
  poi.title = 'mass(J/#psi)'
  poi.unit  = 'MeV/c^{2}'

  ## Get frame & draw all
  frame = poi.frame()
  data  = w.data('dataset_with_weights')
  data.plotOn(frame, name='data', markerSize=0.2)
  #
  nmz   = (1.0, ROOT.RooAbsReal.RelativeExpected)
  model = w.pdf('model')
  model.plotOn(frame, name='model'                     , lineColor=ROOT.kGreen, normalization=nmz)
  model.plotOn(frame, name='sig', components='sigModel', lineColor=ROOT.kBlack, normalization=nmz)
  model.plotOn(frame, name='bkg', components='bkgModel', lineColor=ROOT.kRed  , normalization=nmz)

  ## Tune
  frame.drawAfter('model', 'data')
  frame.Draw()

  ## Legends
  leg = ROOT.TLegend(0.15, 0.55, 0.65, 0.92, 'LHCb preliminary', 'NDC')
  leg.AddEntry(data                         , 'Observed'  , 'PE')
  leg.AddEntry(ROOT.gPad.FindObject('model'), 'Model'     , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('sig')  , 'Signal'    , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('bkg')  , 'Background', 'l')
  leg.fillColorAlpha = 1, 0.
  leg.Draw()

  ## Finally
  ROOT.gPad.SaveAs('contrib.pdf')

#===============================================================================

def draw_sweights_POI():
  """
  Stacked TProfile of sWeights as a function of 2 POIs
  """
  ## Harsher contrast
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.markerSize = 0.

  ## Start
  tree    = import_tree('electron', 'weighed.root')
  tags    = 'nsig_sw', 'nbkg_sw'
  legends = 'Signal', 'Background'

  QHist.reset()
  QHist.trees   = tree
  QHist.ymin    = -1.2
  QHist.ymax    = 3.5
  QHist.options = 'prof'
  QHist.legends = legends
  QHist.st_line = False

  h = QHist()
  h.name    = 'sweights'
  h.params  = [x+':Jpsi_M' for x in tags]
  h.xmin    = 2400
  h.xmax    = 3600
  h.xbin    = 120
  h.xlabel  = 'mass(J/#psi) [MeV/c^{2}]'
  h.draw()

#===============================================================================

if __name__ == '__main__':
  # draw_contribution()
  draw_sweights_POI()
