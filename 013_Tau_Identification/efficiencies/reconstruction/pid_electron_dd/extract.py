#!/usr/bin/env python

from PyrootCK import *
import subprocess

#===============================================================================

cut_PID_e = utils.join(
  '!{0}_ISMUONLOOSE',
  '{0}_PP_InAccEcal',
  '{0}_PP_InAccHcal',
  '{0}_PP_CaloHcalE/{0}_P < 0.05',
  '{0}_PP_CaloEcalE/{0}_P > 0.10',
  '{0}_PP_CaloPrsE > 50',
)

## Preselection for purer electron sample
filt = utils.join(
  ## event
  'nSPDHits<=600',
  {'runNumber < 111802', '111890 < runNumber'},
  {'runNumber < 126124', '126160 < runNumber'},
  {'runNumber < 129530', '129539 < runNumber'},

  ## mothers
  'B_ENDVERTEX_CHI2/B_ENDVERTEX_NDOF       < 9', # already 9 from stripping level
  'Jpsi_ENDVERTEX_CHI2/Jpsi_ENDVERTEX_NDOF < 9', # already 9 from stripping level

  ## acceptance
  'e1_ETA   > 2.',
  'e1_ETA   < 4.5',
  'e2_ETA   > 2.',
  'e2_ETA   < 4.5',
  'K_ETA    > 2.',
  'K_ETA    < 4.5',

  ## tracks
  'e1_TRPCHI2 > 0.01',
  'e2_TRPCHI2 > 0.01',
  'K_TRPCHI2  > 0.01',
  'e1_TRGHP   < 0.4',
  'e2_TRGHP   < 0.4',
  'K_TRGHP    < 0.4',
  'e1_TRTYPE == 3',
  'e2_TRTYPE == 3',
  'K_TRTYPE  == 3',

  ## tag
  '!K_ISMUONLOOSE',
  'K_PP_InAccHcal',
  'K_PP_CaloHcalE/K_P > 0.05',
  'K_PROBNNghost < 0.4',
  cut_PID_e,

  ## Other on probe
  '{1}_PT > 5e3',
)

#===============================================================================
# EXTRACTION
#===============================================================================

def main():
  """
  Process over the PID.MDST ntuples, and sliced for the releveant part
  to a smaller ntuple.
  """
  exe = lambda tag, probe: ROOT.TFile.slice_tree(
    src       = 5437,  # for tfile_seeker
    tname     = 'PID_electron/electron',
    aliases   = {'passed': cut_PID_e.format(probe), 'PT':'%s_PT/1e3'%probe, 'ETA': '%s_ETA'%probe},
    params    = ['passed', 'nTracks', 'B_M', 'Jpsi_M', 'PT', 'ETA'],
    selection = filt.format(tag, probe),
    dest      = '%s.root'%probe, # to be different than temp file
  )
  exe('e1', 'e2')
  exe('e2', 'e1')
  ## hadd them together
  args = 'hadd', 'extracted.root', 'e1.root', 'e2.root'
  print subprocess.check_output(args)
  os.remove(args[2])
  os.remove(args[3])

#===============================================================================

if __name__ == '__main__':
  main()
