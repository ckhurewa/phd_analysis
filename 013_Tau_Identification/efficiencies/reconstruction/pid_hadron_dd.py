#!/usr/bin/env python
"""

Calculate the low-PT charged-hadron PID efficiency from PIDCalib sample:

[D*(2010)+ -> D0 pi+]cc,  [D0 -> K- pi+]cc

Because it's quite pure, from the interactive check, TProfile is sufficient.

"""

from rec_utils import *
from pid_hadron_bins import *

## choice of fit spec for bkg-syst
FIT_SPEC_2D = 'nominal'

#===============================================================================
# QUICK MODE
#===============================================================================

@gpad_save
def draw_eff_quick():
  """
  Draw the quick estimation of ePID as a function of ETA,PT,
  without background subtraction procedure.
  """
  ## Define canvas
  prof = ROOT.TProfile2D('eff', 'eff', BIN_ETA_DETAILED, BIN_PT_DETAILED)
  prof.ownership = False
  prof.markerSize = 0.5

  ## Actual draw
  tree = import_tree('hadron', 'tuple.root')
  cut  = utils.join(
    'D0_M > 1840',
    'D0_M < 1890',
    'Dst_M > 2000',
    'Dst_M < 2030',
  )
  tree.Draw('passed: PT: ETA >> eff', cut, 'prof colz texte')
  ROOT.gPad.logy = True


@memorized
def eff_quick_raw():
  """
  Filter the good part of the tree, slice it and save as DataFrame for further
  processing.
  """
  tree = import_tree('hadron', 'tuple.root')
  cut  = utils.join(
    'D0_M > 1840',
    'D0_M < 1890',
    'Dst_M > 2000',
    'Dst_M < 2030',
  )
  tree.estimate = tree.entries+1
  tree.Draw('passed: ETA: PT: nTracks', cut, 'para goff')
  return tree.sliced_dataframe()


@pickle_dataframe_nondynamic
def eff_3D_quick():
  """
  As a function of ETA, PT, nTracks
  """
  df0    = eff_quick_raw()
  binner = lambda df: binning_3D(df, ETA=BIN_ETA, PT=BIN_PT, nTracks=BIN_NTRACKS)
  deno   = binner(df0)
  nume   = binner(df0[df0.passed==1])
  eff    = divide_clopper_pearson(nume.fillna(0.), deno.fillna(1.))
  return eff


@pickle_dataframe_nondynamic
def eff_2D_quick():
  acc = {}
  df0 = eff_quick_raw()
  for tag, scheme in SCHEMES_2D.iteritems():
    logger.info('Binning %s'%tag)
    binner   = lambda df: binning_2D(df, ETA=scheme['ETA'], PT=scheme['PT'])
    deno     = binner(df0)
    nume     = binner(df0[df0.passed==1])
    eff      = divide_clopper_pearson(nume.fillna(0.), deno.fillna(1.))
    acc[tag] = eff
  return pd.DataFrame(acc)


#===============================================================================
# SW (SWEIGHT) MODE
#===============================================================================

@pickle_dataframe_nondynamic
def eff_2D_sweight():
  """
  Load the SW-weighed tree and compute weighed eff via TProfile2D.
  """
  tree = import_tree('hadron', 'weighed.root')
  acc  = {}
  for tag, scheme in SCHEMES_2D.iteritems():
    logger.info('Binning %s'%tag)
    h = ROOT.TProfile2D('htemp', 'htemp', scheme['PT'], scheme['ETA'])
    tree.Draw('passed: ETA: PT >> htemp', 'nsig_sw', 'prof goff')
    acc[tag] = completely_correlate(h.dataframe().unstack())
    h.Delete()
  ## finally, wrap
  df = pd.DataFrame(acc)
  df.index.names = 'PT', 'ETA' 
  return df


#===============================================================================
# WEIGHTS & POST-PROCESSING
#===============================================================================

def compare_2D_quick_fit():
  """
  Glimpse comparison between the two, by looking at the relative errors.
  They should not be too different, except near the edges.
  Show the relative errors in %
  """
  def rerr(u):
    return abs(1.-u.n)*100
  df_quick = sort_intv_pt(eff_2D_quick()['nominal'].dropna().unstack())
  df_fit   = eff_2D_from_fit()
  return (df_fit/df_quick).applymap(rerr)


@pickle_dataframe_nondynamic
def syst_quick_vs_fit():
  """
  Determine the scalar systematics to be used, if the results from quick method
  will be used instead of the one from fitting, for the sake of performance.

  Calculate from the weighted average of disagreement between the two.

  Return the relative error in percent, approximately 0.34%
  """
  w = binning_2D(eff_quick_raw(), **SCHEMES_2D['nominal'])
  e = compare_2D_quick_fit().stack()
  return pd.Series([(w*e).sum()/w.sum()])


#-------------------------------------------------------------------------------

@memorized
def eff_3D_combined():
  """
  Return the eff map as a function of ETA, PT, NTRACKS,
  based on the result from both background-removal and quick.
  Use the former as a base, and fill na with the latter.
  """
  df0 = pd.concat({'quick': eff_3D_quick(), 'fit':eff_3D_from_fit()}, axis=1)
  df0 = df0.fillna(0)
  eff = df0.apply(lambda se: se.quick if se.fit==0 else se.fit, axis=1)
  ## reorder to alphabetical
  eff.index.names = 'ETA', 'PT', 'NTRACKS'
  return eff.reorder_levels([ 0, 2, 1 ])

@memorized
def eff_2D_combined():
  df0 = pd.concat({
    'quick' : eff_2D_quick()[FIT_SPEC_2D].dropna(),
    'fit'   : eff_2D_from_fit().stack(),
  }, axis=1)
  df0 = df0.fillna(0)
  eff = df0.apply(lambda se: se.quick if se.fit==0 else se.fit, axis=1)
  eff.index.names = 'ETA', 'PT'
  eff = eff.reorder_levels([ 1, 0 ])
  return sort_intv_pt(eff.unstack()).stack()


#-------------------------------------------------------------------------------
# HETEROGENEOUS VERSION
#-------------------------------------------------------------------------------

@memorized
def eff_hetero():
  """
  Take the quick heterohenous-binning method with added syst from bkg.
  """
  ## Choice of eff map
  # eff = eff_2D_quick()
  eff = eff_2D_sweight()

  ## Pick up 2 regions
  eff_low = eff['hetero_lowpt'].dropna().unstack().T
  eff_hi0 = eff['hetero_highpt'].dropna().unstack().T

  ## Need to camoflage the high-PT binning to have same dimension as low,
  # whilst duplicate the eff to make it effectively the same.
  ## By design, low-PT bin has ETA/20, and high-PT bin has ETA/10.
  assert len(eff_low.index)==20
  assert len(eff_hi0.index)==10
  eff_hi = pd.concat([eff_hi0.iloc[i/2,:] for i in xrange(20)], axis=1).T
  eff_hi.index = eff_low.index
  eff = sort_intv_pt(pd.concat([eff_low, eff_hi], axis=1))

  ## Drop bad values to NA, threshold at 5%, then forward-fill
  eff = eff.applymap(lambda u: pd.np.nan if (u.n==0 or u.n==1 or u.rerr > 0.05) else u)
  eff = eff.fillna(method='ffill', axis=1)

  ## stats are fully correlated, then add syst, return.
  # NOTE: Syst are only for quick mode
  eff = completely_correlate(eff.stack())
  # eff = eff * ufloat(1, syst_quick_vs_fit()[0]/100.)

  ## Syst from sWeight method, 0.1% per track
  eff *= ufloat(1, 0.001)

  ## Finally
  eff.index.names = 'ETA', 'PT'
  return eff.unstack()

#-------------------------------------------------------------------------------

@memorized
def eff_eta_weighed_H1MU():
  """
  Weighed to the PT distribution of tauh1 from h1mu channel.
  Because of low-stats in h1mu, use the quick version of lower bins
  """
  w = multiweights_pt_eta()['nominal'].unstack()['h1mu_pi'].dropna().unstack()
  e = eff_2D_sweight()['nominal'].dropna().unstack() * ufloat(1, 0.001)
  return (w*e).sum()/w.sum().replace(0, 1.)


@memorized
def eff_eta_weighed_H3MU():
  """
  May have better advantage of showing finer bins
  """
  w = multiweights_pt_eta()['nominal'].unstack()['h3mu_pr'].dropna().unstack()
  e = eff_2D_sweight()['nominal'].dropna().unstack() * ufloat(1, 0.001)
  return (w*e).sum()/w.sum().replace(0, 1.)


@memorized
def eff_pt_weighed_H3MU():
  w = multiweights_pt_eta()['hETA_wPT'].unstack()['h3mu_pr'].dropna().unstack().T
  e = eff_2D_sweight()['hETA_wPT'].dropna().unstack().T * ufloat(1, 0.001)
  return sort_intv_pt((w*e).sum()/w.sum().replace(0, 1.))


#===============================================================================
# FINALLY
#===============================================================================

# @pickle_dataframe_nondynamic # temp
@memorized
def scalar_results():
  """
  Compute eff results on Ztautau signal, before the equiv-run.

  IMPORTANT: This is required for the computation of systematics,
  found in the AUX.

  """
  res = pd.DataFrame()

  # ## 3D, combined
  # w = weights_pt_eta_ntracks()
  # e = eff_3D_combined()
  # res['3D-combined'] = w.apply(lambda se: (se*e).sum()/se.sum())

  # ## 2D, combined
  # w = multiweights_pt_eta()[FIT_SPEC_2D].dropna().unstack()
  # e = eff_2D_combined()
  # res['2D-combined'] = w.apply(lambda se: (se*e).sum()/se.sum())

  ## 2D, different scheme
  weights = multiweights_pt_eta()
  # eff     = eff_2D_quick().reorder_levels([1, 0])
  eff     = eff_2D_sweight()

  for tag, e in eff.iteritems():
    if tag=='detailed': # too heavy, skippable
      continue
    logger.info('processing: %s'%tag)
    e = e.dropna().unstack().stack()
    w = weights[tag].dropna().unstack().filter(regex='.*pr[123]|.*pi', axis=1)
    k = '2D-'+tag
    res[k] = w.apply(lambda se: (se*e).sum()/(se.sum() or 1.))

  ## Finally
  res.loc['h3mu_tauh3'] = res.loc['h3mu_pr1'] * res.loc['h3mu_pr2'] * res.loc['h3mu_pr3']
  res.loc['eh3_tauh3']  = res.loc['eh3_pr1']  * res.loc['eh3_pr2']  * res.loc['eh3_pr3']
  return res

#-------------------------------------------------------------------------------

def lookup(*args):
  eff = eff_hetero().stack()
  return lookup_eff_generic(__file__, eff, _PT=BIN_PT, _ETA=BIN_ETA)(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ## Obtaining efficiency map
  # draw_eff_quick()
  # print eff_quick_raw()
  # print eff_3D_quick()
  # print sort_intv_pt(eff_2D_quick()['nominal'].dropna().unstack()).fmt2p

  ## sWeight method
  # print eff_2D_sweight()
  # print sort_intv_pt(eff_2D_sweight()['nominal'].dropna().unstack().T)

  ## Background removal
  # perform_fit()
  # read_fit()
  # print eff_3D_from_fit()
  # print eff_2D_from_fit()

  ## Post-processing
  # print weights_pt_eta()
  # print weights_pt_eta_ntracks()
  # print multiweights_pt_eta()['nominal'].unstack()['h1mu_pi'].dropna().unstack().T
  # print compare_2D_quick_fit().fmt2f
  # print syst_quick_vs_fit()

  # print eff_hetero()
  # print eff_eta_weighed_H1MU()
  # print eff_eta_weighed_H3MU()
  # print eff_pt_weighed_H3MU()
  # print scalar_results().fmt3p
  # print eff_compare_dd_mc().fmt2p

  ## Finally
  # print scalar_results().fmt2p
  # print lookup().unstack()
