#!/usr/bin/env python

"""

Note:
- Due to my poor oversight, I already did the binning and calculating eff for PT
only up to 70 GeV. Thus, in this script the entry with PT>=70 GeV is discarded.
The effect is very negligible, usually 1 in 10k.

"""

from rec_utils import *
from PyrootCK.mathutils import weighted_harmonic_average, weighted_average

##
REC_COLS      = [ 'etrack', 'ekine', 'epid', 'egec', 'etrig' ]
REC_COLS_ALL  = REC_COLS + ['erec']

#-------------------------------------------------------------------------------
# These can be disable in stable result for fast import
#-------------------------------------------------------------------------------

import kinematic
import tracking_muon_dd
import tracking_hadron
import tracking_electron
import trk_correction
import pid_muon_dd
import pid_hadron_dd
import pid_hadron_aux # for syst
import pid_electron_dd
import GEC
import trigger_muon
import trigger_electron
import reweigh_ntracks
from tracking_muon_aux import lookup_fix_TRPCHI2

## Load number of candidates. Used as weights
import candidates

#===============================================================================

## Load results from MC. Need memorized for correct correlation
@memorized
def get_mc_ekine():
  return kinematic.calculate_stages_detailed().loc['ekine']

#===============================================================================

def preprocess(tree):
  """
  Use at the beginning, several functions in one.
  """
  ## Prepare a dataframe, subset of original tree
  # no more need to trim as the tree was trimmed
  logger.info('Input tree: %s'%tree)
  tname = tree.name
  df    = tree.dataframe('weight') # Choose this instead of Selection
  n0    = len(df)
  df.index.name = tname

  ## In some case, where the expected bkg is very low, but input tree is very 
  # large (e.g., misid-lepton via Zmumu), do the prescale.
  if tree.name in ('Zmu', 'Ze'): # characteristic name
    logger.info('Prescale 0.05')
    df = df.iloc[::20]
    n0 = len(df.index)

  ## Convert unit to GeV, also trim the edge case
  for cname in df.columns:
    if cname.endswith('_PT'):
      df[cname] /= 1e3
      df = df.loc[df[cname]<=300] # 300GeV max
    if cname.endswith('_P'):
      df[cname] /= 1e3
  df = df.loc[df['nTracks']<=600]
  n = len(df)

  ## Report the edge case
  if n0!=n:
    msg = 'Discarded extreme values: {} --> {}'.format( n0, n )
    if 1.*n/n0 > 0.01: # tolerate 1% missing
      logger.warning(msg)
    else:
      raise ValueError(msg)
  logger.info('Analysing: {}, size={}'.format( tname, n ))

  ## Ready to go
  return df

#-------------------------------------------------------------------------------

def isgood(x):
  """
  Used for post-processor: True if value looks not fishy.
  """
  if isinstance(x, float):
    return not pd.np.isnan(x) and not pd.np.isinf(x)
  ## ufloat
  # optional trigger can have eff=0
  return (x.n >= 0) and not math.isnan(x.n)

def postprocess(df, fast=False):
  """
  Given a large df before making average, collapse to only essential columns.
  Provide some validation.
  """
  ## Collect the sample size
  N = len(df.index)

  ## empty dataframe, possible if no bkg left. Return empty frame
  if N==0:
    df = pd.DataFrame(columns=REC_COLS_ALL)
    df.loc[0] = [0.]*len(REC_COLS_ALL)
    return df.T

  ## Fast mode: Throw away the correlation & syst.
  if fast:
    logger.info('Fast mode enabled')
    df = df.applymap(uncertainties.nominal_value)

  ## Multiply out the total erec.
  logger.info('Multiplying erec')
  df['erec'] = df[REC_COLS].product(axis=1)

  ## Multiply out the total weight. Each channel may have different sources
  # of weight. This should be prepared in the `channel_single` individually.
  # At this stage, it's multiplied out based on the matched prefix.
  ## Note: in the new version of exported_trees, there's always a branch name
  # "weight" which is either misid-rate for fake Zll background, or alias of
  # Sel_Comb which is always uniformly 1.
  cols = [s for s in df.columns if s.startswith('weight_')]
  logger.info('Multiplying weights: %s'%str(cols))
  weight = df[cols].product(axis=1)
  wsum   = weight.sum()
  df.drop(cols, axis=1, inplace=True) # delete those columns

  ## Validation: Check for missing values
  logger.info('Perform validation')
  for i, row in df.iterrows():
    if not row.apply(isgood).all():
      # Raise exception, print only first bad
      print row.to_string()
      raise ValueError('Found bad row at row=%i, abort.'%i)
  if not weight.apply(isgood).all():
    print weight.to_string()
    print df.to_string()
    raise ValueError('Found bad weight abort.')

  ## Collapse the ARITH/HARMONIC average of each column.
  # Effectively reduce DF to SE.
  # Theoretically, the efficiencies should be harmonic average, whereas other
  # multiplicative factor (correction, weight) should be arithmetic.
  # Because of the stability, choose to do arithmetic for both as the difference
  # are observed to be not so much
  logger.info('Collapsing events')
  # f_avg = lambda se: weighted_harmonic_average(se, weight) # w-harmonic
  f_avg = lambda se: (se*weight).sum()/wsum # w-arithmetic
  se = df.apply(f_avg)

  ## Attach the sample size for stat, then return
  se['sample_size'] = N
  logger.info('Postprocess completed: erec = {:.3f}%'.format(se['erec']*100.))
  return se


#===============================================================================

def mumu_single(dfin):
  ## Attach
  dt = 'mumu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['etrack_mu1']       = tracking_muon_dd.lookup( dfin, dt, 'mu1' )
  df['etrack_mu2']       = tracking_muon_dd.lookup( dfin, dt, 'mu2' )
  df['epid_mu1' ]        = pid_muon_dd.lookup     ( dfin, dt, 'mu1' )
  df['epid_mu2' ]        = pid_muon_dd.lookup     ( dfin, dt, 'mu2' )
  df['etrig_mu1']        = trigger_muon.lookup    ( dfin, dt, 'mu1' )
  df['etrig_mu2']        = trigger_muon.lookup    ( dfin, dt, 'mu2' )

  ## Finally
  df['etrack'] = df.etrack_mu1 * df.etrack_mu2
  df['ekine']  = get_mc_ekine()[dt]
  df['epid']   = df.epid_mu1   * df.epid_mu2
  df['egec']   = GEC.efficiencies()[dt]
  df['etrig']  = df.etrig_mu1  + df.etrig_mu2 - df.etrig_mu1*df.etrig_mu2
  return df


#===============================================================================

def h1mu_single(dfin):
  ## Attach
  dt = 'h1mu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = reweigh_ntracks.lookup      ( dfin, dt )
  df['corr_trk_pi']      = trk_correction.lookup       ( dfin, dt, 'pi' )
  df['corr_trp_pi']      = lookup_fix_TRPCHI2          ( dfin, dt, 'pi' )
  df['etrack_mu']        = tracking_muon_dd.lookup     ( dfin, dt, 'mu' )
  df['etrack_pi']        = tracking_hadron.lookup_tauh1( dfin, dt, 'pi' ) * df.corr_trk_pi * df.corr_trp_pi
  df['epid_mu']          = pid_muon_dd.lookup          ( dfin, dt, 'mu' )
  df['epid_pi']          = pid_hadron_dd.lookup        ( dfin, dt, 'pi' ) * pid_hadron_aux.syst()[dt]
  df['etrig']            = trigger_muon.lookup         ( dfin, dt, 'mu' )

  ## Finally
  df['etrack'] = df.etrack_pi * df.etrack_mu
  df['ekine']  = get_mc_ekine()[dt]
  df['epid']   = df.epid_pi   * df.epid_mu
  df['egec']   = GEC.efficiencies()[dt]
  return df


#===============================================================================

def h3mu_single(dfin):
  ## Attach
  dt = 'h3mu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = reweigh_ntracks.lookup ( dfin, dt )
  df['etrack_mu']        = tracking_muon_dd.lookup( dfin, dt, 'mu'  )
  df['corr_trk_pr1']     = trk_correction.lookup  ( dfin, dt, 'pr1' )
  df['corr_trk_pr2']     = trk_correction.lookup  ( dfin, dt, 'pr2' )
  df['corr_trk_pr3']     = trk_correction.lookup  ( dfin, dt, 'pr3' )
  df['corr_trp_pr1']     = lookup_fix_TRPCHI2     ( dfin, dt, 'pr1' )
  df['corr_trp_pr2']     = lookup_fix_TRPCHI2     ( dfin, dt, 'pr2' )
  df['corr_trp_pr3']     = lookup_fix_TRPCHI2     ( dfin, dt, 'pr3' )
  df['epid_mu']          = pid_muon_dd.lookup     ( dfin, dt, 'mu'  )
  df['epid_pr1']         = pid_hadron_dd.lookup   ( dfin, dt, 'pr1' )
  df['epid_pr2']         = pid_hadron_dd.lookup   ( dfin, dt, 'pr2' )
  df['epid_pr3']         = pid_hadron_dd.lookup   ( dfin, dt, 'pr3' )
  df['etrig']            = trigger_muon.lookup    ( dfin, dt, 'mu'  )

  ## Finally
  corr_trk_tauh3     = df.corr_trk_pr1 * df.corr_trk_pr2 * df.corr_trk_pr3
  corr_trk_tauh3    *= df.corr_trp_pr1 * df.corr_trp_pr2 * df.corr_trp_pr3
  df['etrack_tauh3'] = tracking_hadron.efficiencies_tauh3()[dt] * corr_trk_tauh3
  df['etrack']       = df.etrack_mu  * df.etrack_tauh3
  df['ekine']        = get_mc_ekine()[dt]
  df['epid_tauh3']   = df.epid_pr1  * df.epid_pr2 * df.epid_pr3 * pid_hadron_aux.syst()[dt]
  df['epid']         = df.epid_mu   * df.epid_tauh3
  df['egec']         = GEC.efficiencies()[dt]
  return df


#===============================================================================

def ee_single(dfin):
  ## Attach
  dt = 'ee'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = reweigh_ntracks.lookup    ( dfin, dt )
  df['corr_trk_e1']      = trk_correction.lookup     ( dfin, dt, 'e1' )
  df['corr_trk_e2']      = trk_correction.lookup     ( dfin, dt, 'e2' )
  df['corr_trp_e1']      = lookup_fix_TRPCHI2        ( dfin, dt, 'e1' )
  df['corr_trp_e2']      = lookup_fix_TRPCHI2        ( dfin, dt, 'e2' )
  df['etrack_e1']        = tracking_electron.lookup  ( dfin, dt, 'e1' ) * df.corr_trk_e1 * df.corr_trp_e1
  df['etrack_e2']        = tracking_electron.lookup  ( dfin, dt, 'e2' ) * df.corr_trk_e2 * df.corr_trp_e2
  df['epid_e1']          = pid_electron_dd.lookup    ( dfin, dt, 'e1' ) * pid_electron_dd.syst()['ee_e1']
  df['epid_e2']          = pid_electron_dd.lookup    ( dfin, dt, 'e2' ) * pid_electron_dd.syst()['ee_e2']
  df['etrig_e1']         = trigger_electron.lookup   ( dfin, dt, 'e1' )
  df['etrig_e2']         = trigger_electron.lookup   ( dfin, dt, 'e2' )

  ## Finally
  df['etrack'] = df.etrack_e1 * df.etrack_e2
  df['ekine']  = get_mc_ekine()[dt]
  df['epid']   = df.epid_e1   * df.epid_e2
  df['egec']   = GEC.efficiencies()[dt]
  df['etrig']  = df.etrig_e1  + df.etrig_e2 - df.etrig_e1*df.etrig_e2
  return df


#===============================================================================

def eh1_single(dfin):
  ## Attach
  dt = 'eh1'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = reweigh_ntracks.lookup      ( dfin, dt)
  df['corr_trk_e']       = trk_correction.lookup       ( dfin, dt, 'e'  )
  df['corr_trk_pi']      = trk_correction.lookup       ( dfin, dt, 'pi' )
  df['corr_trp_e']       = lookup_fix_TRPCHI2          ( dfin, dt, 'e'  )
  df['corr_trp_pi']      = lookup_fix_TRPCHI2          ( dfin, dt, 'pi' )
  df['etrack_e']         = tracking_electron.lookup    ( dfin, dt, 'e'  ) * df.corr_trk_e  * df.corr_trp_e
  df['etrack_pi']        = tracking_hadron.lookup_tauh1( dfin, dt, 'pi' ) * df.corr_trk_pi * df.corr_trp_pi
  df['epid_e']           = pid_electron_dd.lookup      ( dfin, dt, 'e'  ) * pid_electron_dd.syst()['eh1_e']
  df['epid_pi']          = pid_hadron_dd.lookup        ( dfin, dt, 'pi' ) * pid_hadron_aux.syst()[dt]
  df['etrig']            = trigger_electron.lookup     ( dfin, dt, 'e'  )

  ## Finally
  df['etrack'] = df.etrack_e * df.etrack_pi
  df['ekine']  = get_mc_ekine()[dt]
  df['epid']   = df.epid_e   * df.epid_pi
  df['egec']   = GEC.efficiencies()[dt]
  return df


#===============================================================================

def eh3_single(dfin):
  ## Attach
  dt = 'eh3'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = reweigh_ntracks.lookup  ( dfin, dt)
  df['corr_trk_e']       = trk_correction.lookup   ( dfin, dt, 'e'   )
  df['corr_trk_pr1']     = trk_correction.lookup   ( dfin, dt, 'pr1' )
  df['corr_trk_pr2']     = trk_correction.lookup   ( dfin, dt, 'pr2' )
  df['corr_trk_pr3']     = trk_correction.lookup   ( dfin, dt, 'pr3' )
  df['corr_trp_e']       = lookup_fix_TRPCHI2      ( dfin, dt, 'e'   )
  df['corr_trp_pr1']     = lookup_fix_TRPCHI2      ( dfin, dt, 'pr1' )
  df['corr_trp_pr2']     = lookup_fix_TRPCHI2      ( dfin, dt, 'pr2' )
  df['corr_trp_pr3']     = lookup_fix_TRPCHI2      ( dfin, dt, 'pr3' )
  df['etrack_e']         = tracking_electron.lookup( dfin, dt, 'e'   ) * df.corr_trk_e * df.corr_trp_e
  df['epid_e']           = pid_electron_dd.lookup  ( dfin, dt, 'e'   ) * pid_electron_dd.syst()['eh3_e']
  df['epid_pr1']         = pid_hadron_dd.lookup    ( dfin, dt, 'pr1' )
  df['epid_pr2']         = pid_hadron_dd.lookup    ( dfin, dt, 'pr2' )
  df['epid_pr3']         = pid_hadron_dd.lookup    ( dfin, dt, 'pr3' )
  df['etrig']            = trigger_electron.lookup ( dfin, dt, 'e'   )

  ## Finally
  corr_trk_tauh3     = df.corr_trk_pr1 * df.corr_trk_pr2 * df.corr_trk_pr3
  corr_trk_tauh3    *= df.corr_trp_pr1 * df.corr_trp_pr2 * df.corr_trp_pr3
  df['etrack_tauh3'] = tracking_hadron.efficiencies_tauh3()[dt] * corr_trk_tauh3
  df['etrack']       = df.etrack_e  * df.etrack_tauh3
  df['ekine']        = get_mc_ekine()[dt]
  df['epid_tauh3']   = df.epid_pr1  * df.epid_pr2 * df.epid_pr3 * pid_hadron_aux.syst()[dt]
  df['epid']         = df.epid_e    * df.epid_tauh3
  df['egec']         = GEC.efficiencies()[dt]
  return df


#===============================================================================

def emu_single(dfin):
  ## Attach
  dt = 'emu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = reweigh_ntracks.lookup  ( dfin, dt)
  df['corr_trk_e']       = trk_correction.lookup   ( dfin, dt, 'e'  )
  df['corr_trp_e']       = lookup_fix_TRPCHI2      ( dfin, dt, 'e'  )
  df['etrack_mu']        = tracking_muon_dd.lookup ( dfin, dt, 'mu' )
  df['etrack_e']         = tracking_electron.lookup( dfin, dt, 'e'  ) * df.corr_trk_e * df.corr_trp_e
  df['epid_mu']          = pid_muon_dd.lookup      ( dfin, dt, 'mu' )
  df['epid_e']           = pid_electron_dd.lookup  ( dfin, dt, 'e'  ) * pid_electron_dd.syst()['emu_e']
  df['etrig_mu']         = trigger_muon.lookup     ( dfin, dt, 'mu' )
  df['etrig_e' ]         = trigger_electron.lookup ( dfin, dt, 'e'  )

  ## Finally
  df['etrack'] = df.etrack_e * df.etrack_mu
  df['ekine']  = get_mc_ekine()[dt]
  df['epid']   = df.epid_e   * df.epid_mu
  df['egec']   = GEC.efficiencies()[dt]
  df['etrig']  = df.etrig_e  + df.etrig_mu - df.etrig_e*df.etrig_mu
  return df


#===============================================================================
# EQUIVALENT EFFICIENCY
#===============================================================================

@pickle_dataframe_nondynamic
def calc_single(dt, fast=False):
  """
  Return the calculation of all sources, for single channel.
  Shape is different between channel.
  """

  ## Loop over all trees, same order as rows
  func    = globals()[dt+'_single']
  trees   = selected.load_selected_tree_erec(dt)
  df      = pd.DataFrame()
  df.name = dt
  for t in trees:
    df[t.name] = postprocess(func(preprocess(t)), fast)

  ## Attach the weight: Positive sign for obv, negative for bkg. 0 for Ztautau
  weights = df.loc['cands'] = candidates.results_signed_for_erec()[dt]

  ## Calculate the equivalent eff = obsv - bkg, on all kind of eff not just erec
  # temporary excluded the null entry from calculation, which is effectively
  # the negligible process & ztautau
  mask = (df.loc['cands'] != 0.) & (df.loc['erec']>0)
  df2  = df.loc[:,mask]  # exclude some columns

  # loop over each eff individually to check for zero, to fallback to arithmean.
  acc = {'sample_size': np.nan, 'cands':weights.sum()}
  for rname, row in df2.iterrows():
    if rname in ['sample_size', 'cands']:
      continue

    ## Force align row & weight with left-join,
    # assert that there's no NA
    vals_weights = pd.DataFrame(row).join(weights, how='left')
    if vals_weights.isnull().values.any():
      print vals_weights
      raise ValueError('Missing value before weighted average.')

    ## Do the actual average
    ## leave it negative to show the effect of hmean.
    e = vals_weights.iloc[:,0]
    w = vals_weights.iloc[:,1]
    x = weighted_harmonic_average(e, w)
    acc[rname] = x

  ## Finally, put this into another column, return
  df['equiv'] = pd.Series(acc)
  return df


#===============================================================================
# FINALLY
#===============================================================================

@pickle_dataframe_nondynamic
def export_corr_matrix():
  """
  Print the correlation matrix to be used in combine_csc script.

  Due to limitation of package `uncertainties`, the correlation will needed to be
  pickled from the SAME session. Thus, this will be very slow and CPU consuming.
  """
  acc = [ calc_single(dt).loc['erec','equiv'] for dt in CHANNELS ]
  df  = pd.DataFrame(uncertainties.correlation_matrix(acc))
  df.columns = CHANNELS
  df.index   = CHANNELS
  return df


def to_fast_csc():
  acc = pd.Series({dt:calc_single(dt).loc['erec','equiv'] for dt in CHANNELS })
  return acc.to_fast_csc('rec')


#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print export_corr_matrix()
    print to_fast_csc()
    sys.exit()

  ## DEV
  # print get_mc_ekine()

  ## Single channel, single source
  # # t_real, t_ztautau, t_zll, tdd_qcd, t_EWK, t_WW_lx, t_ttbar, tss_ztau
  # t = list(selected.load_selected_tree_erec('h3mu'))[0]
  # df = preprocess(t)
  # print df.to_string()
  # se = postprocess(eh1_single(df))

  ## Single channel, all sources
  for dt in CHANNELS:
    print calc_single(dt).fmt1p
  # print calc_single('mumu', True).fmt1p

  ## SLOW: Complete correlation matrix
  # run me without others.
  # print export_corr_matrix()

  ## Finally
  # print to_fast_csc()
 