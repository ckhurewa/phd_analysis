#!/usr/bin/env python

"""
Provide printer module to be binded to latex
"""

from rec_utils import *
from PyrootCK.mathutils import weighted_average

#===============================================================================

## simple labels to latex
EFF_TO_LATEX = {
  'etrack': '\\etrack',
  'ekine' : '\\ekine',
  'epid'  : '\\ePID',
  'egec'  : '\\egec',
  'etrig' : '\\etrig',
  'erec'  : '\\erec',
  #
  'etrack_e'     : '\\etrk\\elec',
  'etrack_mu'    : '\\etrk\\muon',
  'etrack_pi'    : '\\etrk\\hadron',
  'etrack_tauh3' : '\\etrk{\\tauh3}',
  'etrack_e1'    : '\\etrk{\\elec{}1}',
  'etrack_e2'    : '\\etrk{\\elec{}2}',
  'etrack_mu1'   : '\\etrk{\\muon{}1}',
  'etrack_mu2'   : '\\etrk{\\muon{}2}',
  #
  'epid_e'     : '\\epid\\elec',
  'epid_mu'    : '\\epid\\muon',
  'epid_pi'    : '\\epid\\hadron',
  'epid_tauh3' : '\\epid{\\tauh3}',
  'epid_e1'    : '\\epid{\\elec{}1}',
  'epid_e2'    : '\\epid{\\elec{}2}',
  'epid_mu1'   : '\\epid{\\muon{}1}',
  'epid_mu2'   : '\\epid{\\muon{}2}',
  'epid_pr1'   : '\\epid{\\hadron{}1}',
  'epid_pr2'   : '\\epid{\\hadron{}2}',
  'epid_pr3'   : '\\epid{\\hadron{}3}',
  #
  'etrig_e'  : '\\etrg\\elec',
  'etrig_mu' : '\\etrg\\muon',
  'etrig_e1' : '\\etrg{\\elec{}1}',
  'etrig_e2' : '\\etrg{\\elec{}2}',
  'etrig_mu1': '\\etrg{\\muon{}1}',
  'etrig_mu2': '\\etrg{\\muon{}2}',
}

#===============================================================================
# RAW TABLE OF EFFICIENCIES
#===============================================================================

## number of decimals
f2 = intv_to_latexintv
f1 = lambda x: intv_to_latexintv(x,1)
f0 = lambda x: intv_to_latexintv(x,0)
f3 = lambda x: intv_to_latexintv(x,3)

key_labels = {
  'PT'      : '\\pt [\\gev]',
  'ETA'     : '\\Eta',
  'ETA3'    : '\\Eta',
  'nTracks' : '`nTracks`',
}
key_printer = {
  'PT'     : f0,
  'ETA'    : f2,
  'ETA3'   : f3, # force 3-digits
  'nTracks': f0,
}


def printer(func, index, column):
  def wrap(*args):
    df = func(*args)
    assert df.index.name == index.replace('ETA3','ETA'), '%s != %s'%(df.index.name, index)
    assert df.columns.name == column, '%s != %s'%(df.columns.name, column)
    df.index.name = '\\\\ '.join([key_labels[index], key_labels[column]])
    df = df.rename(index=key_printer[index], columns=key_printer[column])
    return df.fmt2lp.to_markdown()
  return wrap

printer_pt_eta      = lambda func: printer(func, 'PT', 'ETA')
printer_eta_pt      = lambda func: printer(func, 'ETA3', 'PT')
printer_eta_ntracks = lambda func: printer(func, 'ETA', 'nTracks')

#------------------------------------------------------------------------------

def etrack_muon():
  import tracking_muon_dd
  df = tracking_muon_dd.lookup()
  return '\n'.join([
    '\\Eta            \\etrk\\muon   ',
    '--------------- ----------------',
    df.rename(f2).fmt2lp.to_string(),
    '---------------------------------'
  ])

@printer_eta_ntracks
def etrack_electron():
  import tracking_electron
  return tracking_electron.lookup().unstack()

@printer_eta_ntracks
def etrack_hadron_tauh1(name):
  import tracking_hadron
  return tracking_hadron.lookup_tauh1()[name].unstack()

# note: tauh3 is not applicable to eff map
etrack_hadron_h1mu = lambda: etrack_hadron_tauh1('h1mu_pi')
etrack_hadron_eh1  = lambda: etrack_hadron_tauh1('eh1_pi')

@printer_pt_eta
def epid_muon():
  import pid_muon_dd
  return pid_muon_dd.lookup().unstack().iloc[:-1].T # exclude 70++

@printer_pt_eta
def epid_electron():
  import pid_electron_dd
  return sort_intv_pt(pid_electron_dd.lookup().unstack().T).iloc[:-1] # exclude 70++

@printer_eta_pt
def epid_hadron():
  ## has detailed ETA range, so ETA are rows.
  import pid_hadron_dd
  return pid_hadron_dd.lookup().unstack().iloc[:,:-1] # exclude 70++

@printer_pt_eta
def etrigger_muon():
  import trigger_muon
  return trigger_muon.lookup().unstack().loc['(20, 25]':'(65, 70]']

@printer_pt_eta
def etrigger_electron():
  import trigger_electron
  return trigger_electron.lookup().unstack().loc['(20, 25]':'(50, 70]']


#===============================================================================
# LATEX pretty printing
#===============================================================================

def latex_alleff_summary():
  """
  Export only the homogeneous part from all channel.
  And only the equivalent eff.
  """
  import rec

  ## Load result from all channels
  df = pd.DataFrame()
  for dt in CHANNELS:
    df[dt] = rec.calc_single(dt).loc[rec.REC_COLS_ALL,'equiv']

  ## Decorate & print without header
  df = df.applymap(fmt2cols_percent).rename(EFF_TO_LATEX)
  inject_midrule(df, -1)
  msg = df.to_latex(escape=False)
  msg = '\n'.join(msg.split('\n')[4:-3])
  return '\\begin{ditautabular}\n'+msg+'\n\\end{ditautabular}'

#===============================================================================
# SEPERATING SYST
#===============================================================================

## Determine the fixed order of syst to be translated homogeneously in all channels
HMG_SYST = {
  'mumu': [ 'etrack', 'etrack_mu1', 'etrack_mu2'   , 'ekine', 'epid', 'epid_mu1', 'epid_mu2'   , 'egec', 'etrig', 'erec' ],
  'h1mu': [ 'etrack', 'etrack_mu' , 'etrack_pi'    , 'ekine', 'epid', 'epid_mu' , 'epid_pi'    , 'egec', 'etrig', 'erec' ],
  'h3mu': [ 'etrack', 'etrack_mu' , 'etrack_tauh3' , 'ekine', 'epid', 'epid_mu' , 'epid_tauh3' , 'egec', 'etrig', 'erec' ],
  'ee'  : [ 'etrack', 'etrack_e1' , 'etrack_e2'    , 'ekine', 'epid', 'epid_e1' , 'epid_e2'    , 'egec', 'etrig', 'erec' ],
  'eh1' : [ 'etrack', 'etrack_e'  , 'etrack_pi'    , 'ekine', 'epid', 'epid_e'  , 'epid_pi'    , 'egec', 'etrig', 'erec' ],
  'eh3' : [ 'etrack', 'etrack_e'  , 'etrack_tauh3' , 'ekine', 'epid', 'epid_e'  , 'epid_tauh3' , 'egec', 'etrig', 'erec' ],
  'emu' : [ 'etrack', 'etrack_mu' , 'etrack_e'     , 'ekine', 'epid', 'epid_mu' , 'epid_e'     , 'egec', 'etrig', 'erec' ],
  ## SWAP EMU->MUE HERE
}

LABELS_HMG_SYST = [
  '\\etrack',
  '\\etrk1',
  '\\etrk2',
  '\\ekine',
  '\\ePID',
  '\\epid1',
  '\\epid2',
  '\\egec',
  '\\etrig',
  '\\erec',
]

def calc_relative_uncertainties(dt):
  """
  Return the Series of relative uncertainties. No scaling.
  Imply partial correlation here.
  """
  import rec
  df0 = rec.calc_single(dt)
  ## Extract table, only the relevant part
  se = df0.loc[HMG_SYST[dt], 'equiv']
  se.index = LABELS_HMG_SYST
  return se.apply(lambda u: u.rerr) # loaded from pickle, cant patched.


def latex_relative_uncertainties():
  """
  Aim to export the latex table of relative uncertainties, from equiv
  """
  df = pd.DataFrame()
  for dt in CHANNELS:
    df[dt] = calc_relative_uncertainties(dt)
  df = df.rename(columns=DT_TO_LATEX)
  df.index.name = '\\cscrerr'
  # stylize, extra subitem indent
  inject_midrule(df, -1)
  df = df.rename(lambda s: '$\\hookrightarrow$'+s if ('1' in s or '2' in s) else s)
  return df.fmt2p.to_markdown()


#===============================================================================
# EQUIV EFF IN EACH CHANNEL
#===============================================================================

def latex_detaileff(dt):
  """
  For comparing components between MC, DD1, DD2
  """
  import rec
  import kinematic
  BLANK = '\\bicol{---}'

  def fmt_nonmc(x):
    ## Treat normal float first
    if isinstance(x, float):
      if math.isnan(x):
        return BLANK
      if x==0.:
        return BLANK
      if x==1.:
        return '\\bicol{100}'
      x = ufloat( x, 0. )
      # return '{:.1f}'.format(x*100)
    ## ufloat
    if x.n == 1.: # revert unity from ufloat to float
      return '\\bicol{100}'
    if math.isnan(x.n):
      return BLANK
    return '{:.1f}'.format(x*100).replace('+/-',' & ')

  @ignore_string
  def fmt_1col(x):
    """
    In 1-column, used in MC, show only the nominal values.
    """
    return '{:.1f}'.format(nominal_value(x)*100)

  @ignore_string
  def fmt_sample_size(x):
    return BLANK if pd.np.isnan(x) else '\\bicol{%i}  '%int(x)

  @ignore_string
  def fmt_expected(x):
    ## expected==0 for equiv column
    if x==0:
      return BLANK
    s = str(int(x)) if x.is_integer() else '{:.1f}'.format(abs(x))
    return '\\bicol{%s} '%s

  def sorter(x):
    """Custom row sorting by eff type first, then eff suffix"""
    i = rec.REC_COLS_ALL.index(x.split('_')[0])
    return i*10+1 if '_' in x else i*10

  ## Load data, reorder columns first: (MC), real, others, Ztau
  df = rec.calc_single(dt)
  df = df.iloc[:,[0]+range(2, df.shape[1])+[1]].copy()

  ## Drop the (tracking) correction rows, no needed
  for idx in df.index:
    if idx.startswith('corr_'):
      df = df.drop(idx)

  ## Take away irregular row, for later, then sort the remaining rows
  row_ssize = df.loc['sample_size']
  row_cands = df.loc['cands']
  df.drop('cands'      , inplace=True)
  df.drop('sample_size', inplace=True)
  df = df.loc[sorted(df.index, key=sorter)]  

  # ## more null entries, those non-equiv, skip equiv & Ztau
  # df.loc['ekine'].iloc[:-2] = pd.np.nan
  # df.loc['egec'].iloc[:-2]  = pd.np.nan
  # df.loc['ekine'].iloc[-1]  = pd.np.nan
  # df.loc['egec'].iloc[-1]   = pd.np.nan

  ## Inject back Zll as null entry
  if dt in ('h3mu', 'eh3'):
    df.insert(1, 'Zll', pd.Series())
    # workaround that Series has no insert
    row_ssize = list(row_ssize)
    row_ssize.insert(1, pd.np.nan)
    row_ssize = pd.Series(row_ssize)
    row_cands = list(row_cands)
    row_cands.insert(1, 0.)
    row_cands = pd.Series(row_cands)

  # ## Force recalc second trigger by amean
  ## Already handled in the post-processors.
  # if dt in ('mumu', 'ee'):
  #   key = 'etrig_mu2' if dt == 'mumu' else 'etrig_e2'
  #   l   = [se for _,se in df.loc[[key,'cands']].iloc[:,:-2].iteritems()]
  #   df.loc[key,'equiv'] = weighted_average(l)

  ## Pretty format the ufloat eff
  df = df.applymap(fmt_nonmc)

  ## Inject MC eff (incompat format, so do this after above)
  eff = pd.Series(kinematic.calculate_stages_detailed()[dt])
  df.insert(0, 'MC', eff.apply(fmt_1col))

  ## Pretty latex indices & columns, do this after binding to MC
  df = df.rename(index=EFF_TO_LATEX, columns=PROCESS_LATEX)

  ## Pretty format to latex, remove the tabular environment, insert midule
  msg   = df.to_latex(escape=False, header=False)
  lines = msg.split('\n')[2:-3]
  lines.insert(-1, '\midrule')
  msg   = '\n'.join(lines)

  ## Fast-track to add the sample size & number of expected candidates
  row_cands.iloc[-1] = row_cands.iloc[-2] # restore value of expected of Ztau by Equiv
  line = '&'.join(row_ssize.apply(fmt_sample_size))
  line = 'Size     & --- & %s \\\\'%line
  msg += ('\n\\midrule\n'+line)
  line = '&'.join(row_cands.apply(fmt_expected))
  line = 'Expected & --- & %s \\\\'%line
  msg += ('\n'+line)

  ## Finally
  return msg

## non-argument alias for latex
latex_detaileff_mumu = lambda: latex_detaileff('mumu')
latex_detaileff_h1mu = lambda: latex_detaileff('h1mu')
latex_detaileff_h3mu = lambda: latex_detaileff('h3mu')
latex_detaileff_emu  = lambda: latex_detaileff('emu')
latex_detaileff_ee   = lambda: latex_detaileff('ee')
latex_detaileff_eh1  = lambda: latex_detaileff('eh1')
latex_detaileff_eh3  = lambda: latex_detaileff('eh3')


#===============================================================================
# TRACKING CORRECTION FACTOR
#===============================================================================

def latex_trk_correction():
  """
  Show the tracking correction factor for each particle.
  """
  import rec
  rename = {
    'corr_e'  : '\\taue',
    'corr_e1' : '\\taue (1st-electron)',
    'corr_e2' : '\\taue (2nd-electron)',
    'corr_pi' : '\\tauh1',
    'corr_pr1': '\\tauh3 (1st-prong)',
    'corr_pr2': '\\tauh3 (2nd-prong)',
    'corr_pr3': '\\tauh3 (3rd-prong)',
  }

  df0 = pd.concat({dt:rec.calc_single(dt)['Ztau'] for dt in CHANNELS})
  corr_trk = df0.filter(regex=r'corr_trk.*').rename(lambda s: s.replace('_trk',''))
  corr_trp = df0.filter(regex=r'corr_trp.*').rename(lambda s: s.replace('_trp',''))
  df = pd.concat({'Correction1': corr_trk, 'Correction2': corr_trp}, axis=1)
  df.index.names = 'Channel', 'Track'

  ## not applicable to non-MC tracking eff
  df.loc[('mumu', '\\taumu'),:] = '---'
  df.loc[('h1mu', '\\taumu'),:] = '---'
  df.loc[('h3mu', '\\taumu'),:] = '---'
  df.loc[('emu' , '\\taumu'),:] = '---'
  ## Sort & rewrap to dataframe
  df = df.reindex(CHANNELS, level=0).rename(rename).rename(DT_TO_LATEX)
  df = df.reset_index()
  ## Blank ff-duplicate cell
  dt_dup = df['Channel'].duplicated()
  df['Channel'] = [dt if not dup else '' for dt,dup in zip(df.Channel, dt_dup)]
  return df.fmt3l.to_markdown(drop_index=True)

#===============================================================================

if __name__ == '__main__':
  pass

  # ## Table of efficiencies
  # print etrack_muon()
  # print etrack_electron()
  # print etrack_hadron_h1mu()
  # print etrack_hadron_eh1()
  # print epid_muon()
  # print epid_hadron()
  # print epid_electron()
  # print etrigger_muon()
  # print etrigger_electron()

  ## Eff summaries
  # print latex_alleff_summary()
  # print latex_relative_uncertainties()
  print latex_detaileff_mumu()
  print latex_detaileff_h1mu()
  print latex_detaileff_h3mu()
  print latex_detaileff_ee  ()
  print latex_detaileff_eh1 ()
  print latex_detaileff_eh3 ()
  print latex_detaileff_emu ()
  # print latex_trk_correction()
