#!/usr/bin/env python

from rec_utils import *
from pid_muon_bins import *

#===============================================================================
# EFF
#===============================================================================

@pickle_dataframe_nondynamic
def multieff():
  """
  Has all schema. Completely correlated.
  """
  acc = {}
  for tag, scheme in REGIMES.iteritems():
    logger.info('Binning %s'%tag)
    nume, deno = calc_epid_mc_eta_pt_spd_raw('muons', bin_eta=scheme['ETA'], bin_pt=scheme['PT'])
    eff        = divide_clopper_pearson(nume.stack(), deno.stack())
    acc[tag]   = completely_correlate(eff)
  return pd.DataFrame(acc)

#===============================================================================

@memorized
def scalar_results():
  """
  To be used for syst table
  """
  idx  = list(pd.cut([], BIN_PT_NOMINAL_LOW).categories)
  e    = multieff()['nominal'].loc[idx].xs('muons', level=2)
  acc  = {}
  for obj in ['mumu_mu2', 'emu_mu']:
    w = multiweights_pt_eta().xs(obj, level=2).loc[idx]['nominal']
    acc[obj] = (w*e).sum()/w.sum()
  return pd.Series(acc)


def efficiencies():
  """
  Nominal choice for plotting.
  """
  return sort_intv_pt(multieff()['nominal'].xs('muons', level=2).dropna().unstack())

#===============================================================================

if __name__ == '__main__':
  pass

  # print multieff()
  print scalar_results().fmt2p
  # print efficiencies().fmt2p
