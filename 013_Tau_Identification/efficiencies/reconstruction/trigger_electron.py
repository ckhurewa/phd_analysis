#!/usr/bin/env python

"""

TODO: Missing ETA in the tuple, so I cannot yet do this as a func of ETA.

"""

from rec_utils import *

# BIN_ETA = 2.0, 2.83, 3.67, 4.5
BIN_ETA = 2.0, 2.25, 2.5, 3.0, 3.5, 3.75, 4.0, 4.5
BIN_PT  = 5, 15, 20, 25, 30, 35, 40, 50, 70, 100, 300 # need low-PT bin for correct denominator
# BIN_PT  = np.linspace(5, 70 , 13+1)

#===============================================================================
# WEIGHTS
#===============================================================================

@pickle_dataframe_nondynamic
def weights_pt():
  return weighter_pt( bin_pt=BIN_PT )

@pickle_dataframe_nondynamic
def weights_eta_pt():
  return weighter_eta_pt( bin_eta=BIN_ETA, bin_pt=BIN_PT )


#===============================================================================
# DATA-DRIVEN
#===============================================================================

## 160628 half-done
# JIDS = 5025,
## 160706 Fix missing ETA
JIDS = 5038, 5039, 5040, 5041, 5042, 5043

cut_filt = utils.join(
  'Z0_M       > 70e3',
  'Z0_M       < 120e3',
  'Z0_DPHI12  > 2.7',
  'Z0_ENDVERTEX_CHI2 < 3',
  #
  'eminus_PT  > 15e3',
  'eplus_PT   > 15e3',
  'eminus_ETA > 2.0',
  'eplus_ETA  > 2.0',
  'eminus_ETA < 4.5',
  'eplus_ETA  < 4.5',
  #
  '{tag}_TOS_ELEC',
  '{tag}_TRPCHI2 > 0.01',
  '{tag}_ISMUONLOOSE == 0',
  '{tag}_ECALFrac > 0.1',
  '{tag}_HCALFrac >= 0', # new
  '{tag}_HCALFrac < 0.05',
  '{tag}_0.50_cc_IT > 0.9',
  # I FORGOT PRS > 50 MeV in tuple
  #
  '{probe}_TRPCHI2 > 0.01',
  '{probe}_ISMUONLOOSE == 0',
  '{probe}_ECALFrac > 0.1',
  '{probe}_HCALFrac >= 0', # new
  '{probe}_HCALFrac < 0.05',
  '{probe}_0.50_cc_IT > 0.9',  
).format

param = '{probe}_TOS_ELEC: {probe}_PT/1e3: {probe}_ETA'.format
# param = '{probe}_TOS_ELEC: {probe}_PT: {probe}_ETA'.format

#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def eff_dd_eta_pt_raw():
  tree = import_tree( 'Trigger/Z02ee', *JIDS )

  ## Apply selection
  tree.Draw( param(probe='eplus') , cut_filt(tag='eminus', probe='eplus') , 'goff' )
  df1 = tree.sliced_dataframe()
  tree.Draw( param(probe='eminus'), cut_filt(tag='eplus' , probe='eminus'), 'goff' )
  df2 = tree.sliced_dataframe()
  df1.columns = 'passing', 'PT', 'ETA'
  df2.columns = 'passing', 'PT', 'ETA'
  df = pd.concat([df1, df2])
  df.passing = df.passing.apply(bool)
  return df


def eff_dd_eta_pt():
  df   = eff_dd_eta_pt_raw()
  nume = binning_2D( df[df.passing], PT=BIN_PT, ETA=BIN_ETA )
  deno = binning_2D( df            , PT=BIN_PT, ETA=BIN_ETA )
  eff  = divide_clopper_pearson( nume, deno ).unstack().T
  eff.loc['(5, 15]'] = ufloat( 0, 0 ) # kill low-PT row
  return eff


def eff_dd_eta():
  df = eff_dd_eta_pt_raw()
  df = df[df.PT > 20] # early discard low-PT away from averaging
  nume = binning_1D( df[df.passing], ETA=BIN_ETA )
  deno = binning_1D( df            , ETA=BIN_ETA )
  eff  = divide_clopper_pearson(nume, deno)
  return eff


def eff_dd_pt():
  df = eff_dd_eta_pt_raw()
  nume = binning_1D( df[df.passing], PT=BIN_PT )
  deno = binning_1D( df            , PT=BIN_PT )
  eff  = divide_clopper_pearson(nume, deno)
  return eff


@gpad_save
def draw_eff_dd_pt_eta():
  eff = eff_dd_eta_pt()
  gr,leg = drawer_graphs_eff_pt( eff )
  gr.yaxis.title = '#varepsilon_{trig, e}'
  gr.maximum    = 1.0
  gr.minimum    = 0.0
  leg.x1NDC     = 0.2
  leg.x2NDC     = 0.6
  leg.y1NDC     = 0.2
  leg.y2NDC     = 0.6
  ROOT.gPad.SetLogx(False)

@gpad_save
def draw_eff2D_dd_pt_eta():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird)
  ROOT.gStyle.SetPaintTextFormat(".4f")

  ## Start from PT > 20GeV
  eff = eff_dd_eta_pt().loc['(20, 25]':'(50, 70]']
  h   = ROOT.TH2F.from_uframe(eff)
  h.Draw('texte colz')
  h.markerSize          = 1.0 # text size
  h.xaxis.title         = '#eta(#mu)'
  h.yaxis.title         = 'p_{T}(#mu) [GeV]'
  h.yaxis.titleOffset   = 1.1
  h.yaxis.labelOffset   = 0.002
  # h.yaxis.rangeUser     = 0, 8
  ROOT.gPad.rightMargin = 0.12
  ROOT.gPad.leftMargin  = 0.16
  ROOT.gPad.Update()


#===============================================================================

@pickle_dataframe_nondynamic
def eff_mc_eta_pt():
  return calc_etrig_mc_eta_pt('electron', bin_eta=BIN_ETA, bin_pt=BIN_PT).T

@pickle_dataframe_nondynamic
def eff_mc_eta():
  return calc_etrig_mc_eta_pt('electron', bin_eta=BIN_ETA)

@pickle_dataframe_nondynamic
def eff_mc_pt():
  return calc_etrig_mc_eta_pt('electron', bin_pt=BIN_PT)


#===============================================================================

@gpad_save
def draw_eff_compare_dd_mc_eta():
  df = pd.DataFrame({
    'Data'      : eff_dd_eta(),
    'Simulation': eff_mc_eta(),
  })
  df.columns.name = '#varepsilon_{trig,e}'
  df.index.name   = '#eta(e)'
  drawer_graphs_dd_mc(df)


@gpad_save
def draw_eff_compare_dd_mc_pt():
  df = pd.DataFrame({
    'Data'      : eff_dd_pt(),
    'Simulation': eff_mc_pt(),
  }).iloc[2:8] # from 20GeV onward
  df.columns.name = '#varepsilon_{trig,e}'
  df.index.name   = 'p_{T}(e) [GeV]'
  #
  gr, leg = drawer_graphs_dd_mc(df)
  gr.xaxis.moreLogLabels = True
  gr.xaxis.labelOffset   = -0.01
  ROOT.gPad.SetLogx(True)


#===============================================================================

def calc_weighted_eff_dd_eh1_eh3():
  weights = weights_eta_pt()
  eff     = eff_dd_eta_pt().stack()
  print 'eh1', format_eff( weights.eh1_e, eff )
  print 'eh3', format_eff( weights.eh3_e, eff )


#===============================================================================

@memorized # For the correct correlation
def efficiencies():
  # kill low PT again
  eff = eff_dd_eta_pt()
  eff.loc['(5, 15]'] = 0.
  eff.loc['(15, 20]'] = 0.
  return completely_correlate(eff.stack())

def lookup(*args):
  eff = efficiencies()
  return lookup_eff_generic(__file__, eff, _PT=BIN_PT, _ETA=BIN_ETA )(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ## Data-driven
  # print eff_dd_eta_pt_raw()
  # print eff_dd_eta_pt()
  # print eff_dd_eta()
  # print eff_dd_pt()
  # draw_eff_dd_pt_eta()  
  # draw_eff2D_dd_pt_eta()
  # calc_weighted_eff_dd_eh1_eh3()

  ## MC
  # print eff_mc_eta_pt()
  # print eff_mc_eta()
  # print eff_mc_pt()

  ## Drawer
  # draw_eff_compare_dd_mc_eta()
  # draw_eff_compare_dd_mc_pt()

  ## Finally
  # print efficiencies()
  print lookup().unstack().fmt1p
