#!/bin/bash

QHistPdfExporter.py presel.root \
  --legend-hide \
  --width=500 \
  --height=480 \
  --pad-margin-left=0.11 \
  --pad-margin-bottom=0.12 \
  --axis-xoffset=0.80 \
  --axis-yoffset=0.90