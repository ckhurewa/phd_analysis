#!/usr/bin/env python

import math
from PyrootCK import *

def draw():
  m       = RooFit.RooRealVar('J_psi_1S_M', 'm'     , 2900, 3300  )
  mean    = RooFit.RooRealVar('mean'      , 'mean'  , 2900, 3300  )
  sigma   = RooFit.RooRealVar('sigma'     , 'sigma' , 0   , 20    )
  model1  = RooFit.RooGaussian('model1'   , 'model1', m, mean, sigma )

  # p1      = RooFit.RooRealVar   ('p1' , 'p1' , -10 )
  # model2  = RooFit.RooPolynomial('model2', 'model2', m, [p1])
  
  exp     = RooFit.RooRealVar('exp','exp', -1E4, 0)  
  model2  = RooFit.RooExponential('model2','model2', m, exp )


  nsig    = RooFit.RooRealVar ('nsig' , 'nsig'  , 1E3, 0, 1E5 )
  nbkg    = RooFit.RooRealVar ('nbkg' , 'nbkg'  , 1E4, 0, 1E5 )
  esig    = RooFit.RooExtendPdf('esig','esig', model1, nsig )
  ebkg    = RooFit.RooExtendPdf('ebkg','ebkg', model2, nbkg )

  model   = RooFit.RooAddPdf  ('model', 'model1+model2', [esig,ebkg])
  # model   = RooFit.RooAddPdf  ('model', 'model' , [model1], [nsig])

  tree  = import_tree( 'tup_PID_Jpsi_muon/jpsi', '$LPHE_OFFLINE/gangadir_large/4136/4136.0.root' )
  data0 = RooFit.RooDataSet('data', 'data', tree, {m})
# # data  = data0.reduce('muminus_ISMUON==1')
# # print data
  # print data0

  model.fitTo( data0, RooFit.Extended() )

  frame = m.frame()
  data0.plotOn(frame)
  model.plotOn(frame)

  frame.Draw()
  gPad.Update()

  exit()

# draw()

#===============================================================================

tree = import_tree( 'tup_PID_Jpsi_muon/jpsi', 4136 )
# tree  = import_tree( 'tup_PID_Jpsi_muon/jpsi', '$LPHE_OFFLINE/gangadir_large/4136/4136.0.root' )

filt = utils.join(
  'J_psi_1S_M > 2950',
  'J_psi_1S_M < 3250',
  # tag
  'muminus_ISMUON',
  'muminus_ISLONG',
  'muminus_TRPCHI2 > 0.01',
  'muminus_ETA > 2.0',
  'muminus_ETA < 4.5',
  # probe
  'muplus_ISLONG',
  'muplus_TRPCHI2 > 0.01',
  'muplus_ETA     > 2.0',
  'muplus_ETA     < 4.5',
  #
  # 'muplus_PT      < 30E3',
)
tree.SetAlias('filtered', filt)

#===============================================================================

def draw():
  func = ROOT.TF1('func', 'gaus + pol1(3)', 2950, 3250)
  func.SetParNames('str','mean','sigma','p0','p1')
  func.SetParameters( 3E1, 3100, 16, 10, 0 )

  # func = ROOT.TF1('func', 'gaus + pol0(3) + expo(4)', 2950, 3250)
  # func.SetParNames('str','mean','sigma','bkg1','bkg2','bkg3')
  # func.SetParameters( 3E1, 3100, 16, 10, 0, 0)
  # func.SetParameters( 3E5, 3100, 16, 10, 0, 0)

  tree.Fit('func', 'J_psi_1S_M>>h', filt )
  h = gROOT.Get('h')
  h.ownership = False
  h.minimum   = 0
  gPad.Update()
  exit()

draw()

# def weight(m):
#   ## https://root.cern.ch/root/HowtoFit.html
#   params = 1.86840e+05, 3.09970e+03, -1.49905e+01, 1.53742e+05, -4.16972e+01
#   nsig   = params[0]*math.exp(-0.5*((m-params[1])/params[2])**2)
#   nbkg   = params[3] + m*params[4]
#   return nsig / (nsig+nbkg)
# print weight(3100)

## Aliasing weight as a func of Jpsi_M
tree.SetAlias('nsig'  , '1.86840e+05 * TMath::Exp(-0.5*((J_psi_1S_M-3.09970e+03)/-1.49905e+01)**2)' )
tree.SetAlias('nbkg'  , '1.53742e+05 + J_psi_1S_M*-4.16972e+01' )
tree.SetAlias('weight', 'nsig/(nsig+nbkg)')

tree.Scan('J_psi_1S_M:weight')

# tree.Draw('(muplus_ISMUON==1) : muplus_ETA: muplus_P/1000 >>h(10,0,200,10,2.0,4.5) ', 'weight*filtered', 'colz prof text' )
# tree.Draw('(muplus_ISMUON==1) : muplus_ETA: muplus_PT/1000 >>h(10,0,20,10,2.0,4.5) ', 'weight*filtered', 'colz prof text' )
# tree.Draw('(muplus_ISMUON==1) : muplus_PT/1000 >> h(10,0,30)', 'weight*(filtered & muplus_PT<30E3 & muplus_ETA<2.25)', 'colz prof' )

gPad.Update()

exit()

