#!/usr/bin/env python

"""

Preselection (also known as Kinematic selection).

"""

from common_utils import *
import kinematic

#===============================================================================
# DRAW PRESELECTION BOXES
#===============================================================================

def Line(*list_xy):
  """
  Quickly create a polyline.
  """
  # from array import array
  # xarr,yarr = zip(*list_xy)
  # x   = array('d', xarr)
  # y   = array('d', yarr)
  x,y = zip(*list_xy)
  pl  = ROOT.TPolyLine( len(x), x, y )
  pl.lineColor = ROOT.kRed
  pl.lineWidth = 4
  return pl


def box(list_xy):
  """
  Quickly draw opaque box.
  """
  x,y = zip(*list_xy)
  pl  = ROOT.TPolyLine( len(x), x, y )
  pl.ownership      = False
  pl.fillColorAlpha = ROOT.kBlack, 0.4
  return pl


def draw_kinematic_pt(dtype):
  """
  Note: This is now checked after the tracking but before PID.
  """

  QHist.reset()
  dt = dtype.replace('_','')

  param = {
    #        Y             X
    'ee'  : 'lep1_PT/1000: lep2_PT/1000',
    'emu' : 'lep2_PT/1000: lep1_PT/1000',
    'mumu': 'lep1_PT/1000: lep2_PT/1000',
    'h1mu': 'had_PT /1000: lep_PT/1000',
    'eh1' : 'had_PT /1000: lep_PT/1000',
    'h3mu': 'tauh3ch_PT/1000: lep_PT/1000',
    'eh3' : 'tauh3ch_PT/1000: lep_PT/1000',
  }[dt]

  xlab,ylab = {
    'ee'  : ('e'  , 'e'  ),
    'emu' : ('e'  , '#mu'),
    'mumu': ('#mu', '#mu'),
    'h1mu': ('#mu', 'h1' ),
    'h3mu': ('#mu', 'h3' ),
    'eh1' : ('e'  , 'h1' ),
    'eh3' : ('e'  , 'h3' ),
  }[dt]

  ## Boxes for trigger
  # line_10     = Line([10,0], [AMAX,0], [AMAX,AMAX], [10,AMAX], [10,0])
  # line_15     = Line([15,0], [AMAX,0], [AMAX,AMAX], [15,AMAX], [15,0])
  # line_10_10  = Line([10,0], [AMAX,0], [AMAX,AMAX], [0,AMAX] , [0,10], [10,10], [10,0])
  # line_15_10  = Line([15,0], [AMAX,0], [AMAX,AMAX], [0,AMAX] , [0,10], [15,10], [15,0])
  # line_15_15  = Line([15,0], [AMAX,0], [AMAX,AMAX], [0,AMAX] , [0,15], [15,15], [15,0])
  # line_trig   = {
  #   'ee'  : line_15_15,
  #   'emu' : line_15_10,
  #   'mumu': line_10_10,
  #   'h1mu': line_10,
  #   'h3mu': line_10,
  #   'eh1' : line_15,
  #   'eh3' : line_15,
  # }[dt]

  ## Boxes for presel
  xmin = ymin = 0
  xmax = ymax = 60
  arr_ll  = (xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,5) , (20,5) , (20,20), (5,20), (5,ymax),
  arr_lh1 = (xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,10), (20,10), (20,ymax)
  arr_lh3 = (xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,12), (20,12), (20,ymax)

  arr = {
    'ee'  : arr_ll,
    'emu' : arr_ll,
    'mumu': arr_ll,
    'h1mu': arr_lh1,
    'eh1' : arr_lh1,
    'h3mu': arr_lh3,
    'eh3' : arr_lh3,
  }[dt]

  filters = [
    '00_Fiducial', 
    '01_Acceptance', 
    '02_MCpredict', 
    '03_MCrec', 
    '04_Tracking',
  ]

  h = QHist()
  h.prefix  = dt
  h.name    = 'kine_pt'
  # h.filters = ALL_CUTS(dtype)[:-1] # exclude the last one: kinematic cut
  h.filters = filters
  h.trees   = kinematic.TREES(dtype)
  h.params  = param
  # h.xlog    = True
  # h.ylog    = True
  # h.zlog    = True
  h.xmin    = xmin
  h.ymin    = ymin
  h.xmax    = xmax
  h.ymax    = ymax
  h.options = 'col'
  h.xlabel  = 'p_{T}(#tau_{%s}) [GeV]'%xlab
  h.ylabel  = 'p_{T}(#tau_{%s}) [GeV]'%ylab
  h.draw()
  h.upperPad.cd()

  pl = box(arr)
  pl.Draw('f')


  # line_trig.lineColor = ROOT.kBlack
  # line_trig.lineStyle = 2
  # line_trig.Draw()

  h.save()

  # exit()

def draw_kinematic_eta(dtype):
  """
  Just checking, but dont bother put in report.
  """
  QHist.reset()
  dt = dtype.replace('_','')

  xlab,ylab = {
    'ee'  : ('e}^{-'  , 'e}^{+'   ),
    'emu' : ('e'      , '#mu'     ),
    'mumu': ('#mu}^{-', '#mu}^{-' ),
    'h1mu': ('#mu'    , 'h1' ),
    'h3mu': ('#mu'    , 'h3' ),
    'eh1' : ('e'      , 'h1' ),
    'eh3' : ('e'      , 'h3' ),
  }[dt]

  h = QHist()
  h.prefix  = dt
  h.name    = 'kine_eta'
  h.filters = ALL_CUTS(dtype)[:-1] # exclude the last one: kinematic cut
  h.trees   = TREES(dtype)
  h.params  = 'lep_ETA: had_ETA'
  # h.zlog    = True
  h.xmin    = 1.5
  h.ymin    = 1.5
  h.xmax    = 5.0
  h.ymax    = 5.0
  h.options = 'col'
  h.xlabel  = '#eta(#tau_{%s})'%xlab
  h.ylabel  = '#eta(#tau_{%s})'%ylab
  h.draw()


def main_draw_kinematic():
  for dtype in kinematic.channels:
    draw_kinematic_pt(dtype)
    # draw_kinematic_eta(dtype)


#===============================================================================

def check_leakage():
  """
  Check the amount of leak-in, leak-out for each channel.
  """



# ## MC binning spec, all types
# BIN_ETA = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
# BIN_PT  = 1, 3, 5, 10, 15, 20, 25, 30, 40, 50, 70

# #===============================================================================
# # WEIGHTS
# #===============================================================================

# @pickle_dataframe
# def weights_eta_pt():
#   return weighter_eta_pt( bin_eta=BIN_ETA, bin_pt=BIN_PT )

# #===============================================================================
# # MC
# #===============================================================================

# def eff_mc_eta_pt():
#   """
#   queues is a list of 3-tuples ( uniquename, channel, tuple_prefix )
#   """

#   ## import tree, strip & concat, calc pid cut,
#   import kinematic
#   filt = utils.join([
#     '00_Fiducial'  ,
#     '01_Acceptance',
#     '02_MCpredict' ,
#     '03_MCrec'     ,
#     '04_Tracking'  ,
#     '05_PID'       ,
#     '06_Trigger'   ,
#   ])

#   queues = {
#     'e_e': 'lep1_PT/1e3: lep2_PT/1e3',
#   }

#   for ch,p in queues.iteritems():
#     name  = 'pr_'+ch
#     pr    = ROOT.TProfile2D( name, name, 13,5,70, 13,5,70 )
#     # pr    = ROOT.TProfile2D( name, name, 3,5,70, 3,5,70 )
#     tree  = kinematic.TREES(ch)
#     param = '07_Kinematic:'+p+' >> '+name
#     # param = '07_Kinematic:'+p
#     tree.Draw(param, filt, 'prof colz texte')
#     exit()


#   ## Cache & collect
#   df_trees = {}
#   acc_nume = {}
#   acc_deno = {}

#   for name, channel, prefix in queues:
#     if channel not in df_trees:
#       df_trees[channel] = kinematic.TREES(channel).dataframe(filt)
#     df  = df_trees[channel]
#     df2 = pd.DataFrame(df[raw_col.format(prefix).split()])
#     df2.columns = cols 
#     df2.PT /= 1e3
#     df2['passing'] = func(df2)
#     nume = binning_3D( df2[df2.passing], ETA=bin_eta, PT=bin_pt, nSPDHits=bin_spd ).T.apply(lambda x: var(x) if x>0 else 0. )
#     deno = binning_3D( df2             , ETA=bin_eta, PT=bin_pt, nSPDHits=bin_spd ).T.fillna(1.)
#     acc_nume[name] = nume
#     acc_deno[name] = deno
#   nume = pd.DataFrame(acc_nume).reorder_levels([1,0,2]).sort_index()
#   deno = pd.DataFrame(acc_deno).reorder_levels([1,0,2]).sort_index()


#===============================================================================

if __name__ == '__main__':
  pass

  ## Draw boxes
  # draw_kinematic_pt('e_e')
  main_draw_kinematic()

  ## Patch to save all new color (especially with transparency) into tfile
  import itertools
  fin = ROOT.TFile.Open('presel/presel.root', 'UPDATE')
  fin.mkdir('colors').cd()
  for color in itertools.islice( ROOT.gROOT.colors, 600, None):
    if color.alpha:
      print 'Saving custom color with transparency:', color
      color.Write(str(color.number)+'_'+color.name, ROOT.TObject.kOverwrite)
  fin.Close()


  # weights_eta_pt()
  # eff_mc_eta_pt()
  # calc_weighted_eff_mc_allsrc()