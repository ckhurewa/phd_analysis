#!/usr/bin/env python

"""

Specialize case for mumu, emu, ee channel trigger,
requiring 4D binning.

Note: Depreciated.

"""

from common_utils import *
import trigger_muon
import trigger_electron

#===============================================================================
# WEIGHTS
#===============================================================================

def weighter( dtype, bins ):
  """
  Cartesian-prod for muons, for 4D binning
  """
  pt1,pt2 = [ k for k in bins if k.endswith('_PT') ]

  df = get_selected_ztautau(dtype)
  df[pt1] /= 1e3
  df[pt2] /= 1e3
  df      = df[bins.keys()]
  groups  = [ pd.cut(df[k],v) for k,v in sorted(bins.iteritems()) ]
  eff     = df.groupby(groups).count().iloc[:,0].fillna(0)
  return eff

@pickle_dataframe
def weights_mumu():
  bins = {
    'mu1_ETA' : trigger_muon.BIN_ETA,
    'mu2_ETA' : trigger_muon.BIN_ETA,
    'mu1_PT'  : trigger_muon.BIN_PT,
    'mu2_PT'  : trigger_muon.BIN_PT,
  }
  return weighter( 'mu_mu', bins )


@pickle_dataframe
def weights_emu():
  bins = {
    'e_ETA' : trigger_electron.BIN_ETA,
    'e_PT'  : trigger_electron.BIN_PT,
    'mu_ETA': trigger_muon.BIN_ETA,
    'mu_PT' : trigger_muon.BIN_PT,
  }
  return weighter( 'e_mu', bins )

@pickle_dataframe
def weights_ee():
  bins = {
    'e1_ETA': trigger_electron.BIN_ETA,
    'e1_PT' : trigger_electron.BIN_PT,
    'e2_ETA': trigger_electron.BIN_ETA,
    'e2_PT' : trigger_electron.BIN_PT,
  }
  return weighter( 'e_e', bins )

#===============================================================================

def calc_weighted_eff_dd_mumu():
  ## Make eff into 4D (swap for eta first)
  se_eff = trigger_muon.eff_dd_eta_pt().swaplevel().sort_index()
  acc    = []
  for (s1,x1),(s2,x2) in itertools.product( se_eff.iteritems(), se_eff.iteritems() ):
    x = x1 + x2 - x1*x2
    s = s1 + s2
    acc.append([ s, x ])
  index,vals = zip(*acc)
  index = pd.MultiIndex.from_tuples( index, names=['mu1_ETA','mu1_PT','mu2_ETA','mu2_PT'])
  eff   = pd.Series( vals, index=index )
  
  ## Apply weights
  w =  weights_mumu()
  print 'mumu', format_eff( w, eff )

def calc_weighted_eff_dd_emu():
  """
  Sort the multiindex alphabetically.
  """
  eff_mu = trigger_muon.eff_dd_eta_pt().swaplevel().sort_index()
  eff_e  = trigger_electron.eff_dd_eta_pt().stack().swaplevel().sort_index()
  acc    = []
  for (s1,x1),(s2,x2) in itertools.product( eff_e.iteritems(), eff_mu.iteritems() ):
    x = x1 + x2 - x1*x2
    s = s1 + s2
    acc.append([ s, x ])
  index,vals = zip(*acc)
  index = pd.MultiIndex.from_tuples( index, names=['e_ETA','e_PT','mu_ETA','mu_PT'])
  eff   = pd.Series( vals, index=index )
  print eff

  ## Apply weights
  w   = weights_emu()
  print 'emu', format_eff( w, eff )

def calc_weighted_eff_dd_ee():
  se_eff = trigger_electron.eff_dd_eta_pt().stack().swaplevel().sort_index()
  acc    = []
  for (s1,x1),(s2,x2) in itertools.product( se_eff.iteritems(), se_eff.iteritems() ):
    x = x1 + x2 - x1*x2
    s = s1 + s2
    acc.append([ s, x ])
  index,vals = zip(*acc)
  print index
  index = pd.MultiIndex.from_tuples( index, names=['e1_ETA', 'e1_PT', 'e2_ETA', 'e2_PT'])
  eff   = pd.Series( vals, index=index )

  print eff.to_string()

  ## Apply weights
  print 'ee', format_eff( weights_ee(), eff )


#===============================================================================

if __name__ == '__main__':
  # weights_mumu()
  # weights_emu()
  # weights_ee()

  # calc_weighted_eff_dd_mumu()
  # calc_weighted_eff_dd_ee()
  calc_weighted_eff_dd_emu()

