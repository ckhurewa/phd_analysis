#!/usr/bin/env python

"""

For Ztautau contamination study.

"""

import itertools
from PyrootCK import *
from uncertainties import ufloat
import pandas as pd
pd.options.display.width = 160

Var = lambda n: ufloat( n, n**0.5 )

## Order follows the latex note
dtypes   = 'e_e', 'e_h1', 'e_h3', 'e_mu', 'h1_h1', 'h1_h3', 'h1_mu', 'h3_h3', 'h3_mu', 'mu_mu'
channels = 'mu_mu', 'h1_mu', 'h3_mu', 'e_e', 'e_h1', 'e_h3', 'e_mu'

latex_cols = {
  'e_e'   : '#tau_{e}#tau_{e}',
  'e_h1'  : '#tau_{e}#tau_{h1}',
  'e_h3'  : '#tau_{e}#tau_{h3}',
  'e_mu'  : '#tau_{#mu}#tau_{e}',
  'h1_h1' : '#tau_{h1}#tau_{h1}',
  'h1_h3' : '#tau_{h1}#tau_{h3}',
  'h1_mu' : '#tau_{#mu}#tau_{h1}',
  'h3_h3' : '#tau_{h3}#tau_{h3}',
  'h3_mu' : '#tau_{#mu}#tau_{h3}',
  'mu_mu' : '#tau_{#mu}#tau_{#mu}',
}

#===============================================================================

def asymvar_to_ufloat( var ):
  """
  Convert from my asymmetric var instance to ufloat instance.
  Shifting method.
  """
  x = float(var)
  h = var.ehigh
  l = var.elow
  return ufloat( x+(h+l)/2, (h-l)/2 )

def eff( numerator, denominator ):
  return asymvar_to_ufloat(Eff( denominator, numerator ))


#===============================================================================

## 160606: Retuned after Stephane's comment

product_mu_mu ={'any': {'presel': (28966, 28915), 'selected': (5008, 5008)},
 'e_e': {'presel': (0, 0), 'selected': (0, 0)},
 'e_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'e_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'e_mu': {'presel': (15, 15), 'selected': (0, 0)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (226, 220), 'selected': (38, 38)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (127, 109), 'selected': (0, 0)},
 'mu_mu': {'presel': (28517, 28492), 'selected': (4959, 4959)}}

product_h1_mu ={'any': {'presel': (52424, 50409), 'selected': (9850, 9850)},
 'e_e': {'presel': (0, 0), 'selected': (0, 0)},
 'e_h1': {'presel': (5, 5), 'selected': (2, 2)},
 'e_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'e_mu': {'presel': (434, 411), 'selected': (25, 25)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (39268, 38817), 'selected': (9745, 9745)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (12281, 10770), 'selected': (60, 60)},
 'mu_mu': {'presel': (325, 300), 'selected': (1, 1)}}

product_h3_mu ={'any': {'presel': (10719, 9012), 'selected': (2422, 2421)},
 'e_e': {'presel': (0, 0), 'selected': (0, 0)},
 'e_h1': {'presel': (2, 2), 'selected': (0, 0)},
 'e_h3': {'presel': (6, 3), 'selected': (0, 0)},
 'e_mu': {'presel': (378, 199), 'selected': (0, 0)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (826, 448), 'selected': (28, 28)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (9021, 8142), 'selected': (2388, 2387)},
 'mu_mu': {'presel': (458, 194), 'selected': (0, 0)}}

product_e_e ={'any': {'presel': (11209, 11086), 'selected': (1717, 1717)},
 'e_e': {'presel': (10693, 10576), 'selected': (1667, 1667)},
 'e_h1': {'presel': (368, 363), 'selected': (48, 48)},
 'e_h3': {'presel': (83, 83), 'selected': (0, 0)},
 'e_mu': {'presel': (35, 35), 'selected': (0, 0)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (0, 0), 'selected': (0, 0)},
 'mu_mu': {'presel': (0, 0), 'selected': (0, 0)}}

product_e_h1 ={'any': {'presel': (24708, 23730), 'selected': (4577, 4577)},
 'e_e': {'presel': (204, 194), 'selected': (10, 10)},
 'e_h1': {'presel': (18307, 18093), 'selected': (4531, 4531)},
 'e_h3': {'presel': (5987, 5237), 'selected': (26, 26)},
 'e_mu': {'presel': (152, 148), 'selected': (2, 2)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (1, 1), 'selected': (0, 0)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (1, 1), 'selected': (0, 0)},
 'mu_mu': {'presel': (0, 0), 'selected': (0, 0)}}

product_e_h3 ={'any': {'presel': (5112, 4291), 'selected': (1035, 1035)},
 'e_e': {'presel': (174, 109), 'selected': (0, 0)},
 'e_h1': {'presel': (434, 235), 'selected': (8, 8)},
 'e_h3': {'presel': (4196, 3807), 'selected': (1025, 1025)},
 'e_mu': {'presel': (292, 126), 'selected': (0, 0)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (3, 2), 'selected': (0, 0)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (0, 0), 'selected': (0, 0)},
 'mu_mu': {'presel': (0, 0), 'selected': (0, 0)}}

product_e_mu ={'any': {'presel': (38882, 38578), 'selected': (10881, 10881)},
 'e_e': {'presel': (12, 12), 'selected': (2, 2)},
 'e_h1': {'presel': (142, 142), 'selected': (33, 33)},
 'e_h3': {'presel': (59, 55), 'selected': (0, 0)},
 'e_mu': {'presel': (37616, 37333), 'selected': (10676, 10676)},
 'h1_h1': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h1_mu': {'presel': (702, 691), 'selected': (145, 145)},
 'h3_h3': {'presel': (0, 0), 'selected': (0, 0)},
 'h3_mu': {'presel': (166, 165), 'selected': (1, 1)},
 'mu_mu': {'presel': (63, 61), 'selected': (0, 0)}}


#===============================================================================

def normalize_colwise(df):
  """
  Return a dataframe of same dimension, but each column is normalize such that
  the sum of column == 1
  """
  df.loc['Total',:] = df.sum()
  df = df.apply(lambda se: se/se['Total'].n)
  return df.drop(['Total'])

# def DataFrame_to_TH2F(df):
#   """
#   Conver with custom format on percentage.
#   """
#   nrow,ncol = df.shape

#   h = ROOT.TH2F('name', 'title', ncol,0,ncol, nrow,0,nrow )
#   h.markerSize        = 2.
#   h.xaxis.title       = 'Product'
#   h.yaxis.title       = 'Origin'
#   h.yaxis.titleOffset = 1.2
#   h.yaxis.titleSize   = 0.05

#   ## Init array
#   for prod,col in df.iteritems():
#     for orig,_ in col.iteritems():
#       h.Fill( prod, orig, 0. )

#   ## Fill array
#   for j,(_,row) in enumerate(df.iterrows()):
#     for i,(_,cell) in enumerate(row.iteritems()):
#       if cell.n > 0:
#         ## set format to percent, 1 decimal
#         val = float('%.2f'%(cell.n*100))
#         err = float('%.2f'%(cell.s*100))
#         h.SetBinContent( i+1, j+1, val )
#         h.SetBinError  ( i+1, j+1, err )
#   return h 


def calc_acc_contamination():
  ## Get 2D histogram, 
  f = ROOT.TFile('/Users/khurewat/Documents/lphe_offline/gangadir_large/5008/tuple.root')
  h = f.Get('contamination_evt')

  ## Convert to compat dataframe
  res = {}
  for i in xrange(1,h.xaxis.nbins+1): # product
    product = h.xaxis.GetBinLabel(i)
    if product not in channels: # skip some
      continue
    origins = {}
    for j in xrange(1,h.yaxis.nbins+1): # origin
      origin = h.yaxis.GetBinLabel(j)
      origins[origin] = h.GetBinContent( i,j )
    res[product] = origins
  df = pd.DataFrame(res)

  ## Normalize for each product type
  df  = df.applymap(Var) # attach syst as err.
  df  = normalize_colwise(df)

  ## store column/index names
  cols = list(df.columns)
  rows = list(df.index)
  df.columns  = [ latex_cols[x] for x in cols ]
  df.index    = [ latex_cols[x] for x in rows ]

  ## Also do the 2D figure
  h = DataFrame_to_TH2F(df)
  h.name  = 'cont'
  h.title = 'Acceptance purity [%]'
  h.Draw('texte colz')
  h.xaxis.titleOffset = 1.5
  h.xaxis.labelSize   = 0.07
  h.yaxis.labelSize   = 0.07

  gPad.SetBottomMargin(0.12)
  gPad.SetLeftMargin(0.12)
  gPad.SetRightMargin(0.10)
  gPad.Update()
  gPad.SaveAs('contamination/acc.pdf')

  ## Finally, restore + return
  df.columns  = cols
  df.index    = rows
  return df


def calc_esel():

  ## Convert to dataframe
  result = {}
  for product in channels:
    result[product] = {}
    name  = 'product_%s'%product
    dat   = globals()[name]
    for origin in dtypes:
      sel     = dat[origin]['selected'][1]
      presel  = dat[origin]['presel'][1]
      esel    = ufloat(0., 0.)
      if presel > 10.: # min size to consider
        esel = eff( sel, presel )
      result[product][origin] = esel
  df = pd.DataFrame(result)

  ## store column/index names
  cols = list(df.columns)
  rows = list(df.index)
  df.columns  = [ latex_cols[x] for x in cols ]
  df.index    = [ latex_cols[x] for x in rows ]

  ## Also do the 2D figure
  h = DataFrame_to_TH2F(df)
  h.name  = 'esel'
  h.title = 'Selection efficiency [%]'
  h.Draw('texte colz')
  h.xaxis.titleOffset = 1.5
  h.xaxis.labelSize   = 0.07
  h.yaxis.labelSize   = 0.07

  gPad.SetBottomMargin(0.12)
  gPad.SetLeftMargin(0.12)
  gPad.SetRightMargin(0.10)
  gPad.SaveAs('contamination/esel.pdf')

  ## Finally, restore + return
  df.columns  = cols
  df.index    = rows
  return df

#-------------------------------------------------------------------------------

if __name__ == '__main__':
  ROOT.gROOT.SetBatch(True)

  df1 = calc_acc_contamination()
  df2 = calc_esel()
  df  = normalize_colwise(df1.multiply(df2))
  # print df1
  # print df2
  # print df

  ## Print main table in latex compat format
  def formatter(x):
    if x < 1E-3:
      return '-'
    return '\num{{{:.1f}}}'.format(x*100).replace('/','')
  df2 = df.applymap(formatter)
  print df2

  ## Reprint purity again in flat format
  dat = [ (ch,df.loc[ch,ch]*100) for ch in channels ]
  header, arr = zip(*dat)
  print 
  print ('{:11} '*7).format(*header)
  print ('-'*11+' ')*7
  print ('{:4.1f} '*7).format(*arr)
