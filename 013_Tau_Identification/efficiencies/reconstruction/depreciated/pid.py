#!/usr/bin/env python

from PyrootCK import *
import pandas as pd

# ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine('.L ~/Documents/lphe_offline/mylib/PyrootCK/macros/lhcbStyle.C')

#===============================================================================

t_mc_biased = import_tree( 'DitauRecoStats/mu_mu', 4093 )
t_mc_biased.SetAlias('filtered', utils.join(
  ## Fiducial
  'tau1_MCPT  > 20E3',
  'tau2_MCPT  > 20E3',
  'tau1_MCETA > 2.0',
  'tau1_MCETA < 4.5',
  'tau2_MCETA > 2.0',
  'tau2_MCETA < 4.5',
  'ditau_MCM  > 60E3',
  'ditau_MCM  < 120E3',
  # ## Acceptance -- h1mu
  # 'tau1ch_MCPT    > 10E3',
  # 'tau2ch_MCPT    > 20E3',
  # 'tau1ch_MCETA   > 2.25', 
  # 'tau1ch_MCETA   < 3.75', 
  # 'tau2ch_MCETA   > 2.0', 
  # 'tau2ch_MCETA   < 4.5', 
  # 'ditauch_MCM    >  30E3',
  # 'ditauch_MCM    < 120E3',
  ## Acceptance -- mumu
  {
    ( 'tau1ch_MCPT > 20E3', 'tau2ch_MCPT >  5E3' ),
    ( 'tau1ch_MCPT >  5E3', 'tau2ch_MCPT > 20E3' ),
  },
  'tau1ch_MCETA   > 2.0', 
  'tau1ch_MCETA   < 4.5', 
  'tau2ch_MCETA   > 2.0', 
  'tau2ch_MCETA   < 4.5', 
  'ditauch_MCM    >  20E3',
  'ditauch_MCM    < 120E3',
  ## Recod & Tracked
  'tau1ch_recob   == 2',
  'tau1ch_recod   == 1',
  'tau1ch_TRTYPE  == 3',
  'tau1ch_TRPCHI2 > 0.01',
  'tau2ch_recob   == 2',
  'tau2ch_recod   == 1',
  'tau2ch_TRTYPE  == 3',
  'tau2ch_TRPCHI2 > 0.01',
))
#
t_mc_biased.SetAlias('mu1_MATCH', 'tau1ch_ISMUON' )
t_mc_biased.SetAlias('mu1_MCETA', 'tau1ch_MCETA')
t_mc_biased.SetAlias('mu1_MCPHI', 'tau1ch_MCPHI')
t_mc_biased.SetAlias('mu1_MCPT' , 'tau1ch_MCPT' )
#
t_mc_biased.SetAlias('mu2_MATCH', 'tau2ch_ISMUON' )
t_mc_biased.SetAlias('mu2_MCETA', 'tau2ch_MCETA')
t_mc_biased.SetAlias('mu2_MCPHI', 'tau2ch_MCPHI')
t_mc_biased.SetAlias('mu2_MCPT' , 'tau2ch_MCPT' )

h = ROOT.TProfile('h','title',12,0,60,0,1)
# h = ROOT.TProfile('h','title',10,2.0,4.5,0,1)
# h = ROOT.TProfile('h','title',20,-3.15,3.15,0,1)
h.ownership = False

n = t_mc_biased.GetEntries('filtered')
t_mc_biased.Draw('mu1_MATCH:mu1_MCPT/1000', 'filtered', 'goff')
for i in xrange(n):
  h.Fill( t_mc_biased.v2[i], t_mc_biased.v1[i] )
t_mc_biased.Draw('mu2_MATCH:mu2_MCPT/1000', 'filtered', 'goff')
for i in xrange(n):
  h.Fill( t_mc_biased.v2[i], t_mc_biased.v1[i] )

# t_mc_biased.Draw('mu_MATCH:mu_MCETA', 'filtered', 'prof')
h.minimum = 0.9
h.Draw()

gPad.Update()

exit()