#!/usr/bin/env python

"""

OLD MUON TRACKING EFF SCRIPT which attempts to determine muon tracking 
as a function of PT. 

This is not needed if one assumed that etrack_muon is independent of PT,
as found in Steve's internal note.

"""

## Same JIDS are used for both Jpis/Z0, handle with care
# first batch
# JIDS = 3993, 3994, 3995
# fixed match condition to de Cain's (70%, and-TT only if existed)
JIDS = 5047, 5048, 5049, 5050, 5051, 5052

#===============================================================================
# DD - LOW-PT: study with Jpsi
#===============================================================================

## 
filt_jpsi = utils.join(
  'TMath::Abs(DPHI) > 1',
  'VCHI2  < 5',
  'PT     > 1e3',
  'M      > 2.7e3',
  'M      < 3.5e3',
  #
  'tag_PT       > 1.3e3',
  'tag_ETA      > 2.0',
  'tag_ETA      < 4.5',
  'tag_TRPCHI2  > 0.01',
  'tag_ISMUON',
  #
  'probe_PT   > 0',
  'probe_ETA  > 2.0',
  'probe_ETA  < 4.5',
)

#-------------------------------------------------------------------------------

def load_jpsi_tree_eta_pt():
  """
  Shared to tracking_hadron script
  """
  ## Prepare incoming tree
  t = import_tree('TrackingEffAlgo/Jpsi', *JIDS )
  t.SetAlias('PREBIN_MASS', 'M/1e3'       )
  t.SetAlias('PREBIN_ETA' , 'probe_ETA'   )
  t.SetAlias('PREBIN_PT'  , 'probe_PT/1e3')
  t.SetAlias('PREBIN_FILT', filt_jpsi     )
  t.SetAlias('PREBIN_PASS', 'match'       )
  return t

def load_jpsi_tree_eta_p():
  t = import_tree('TrackingEffAlgo/Jpsi', *JIDS )
  t.SetAlias('PREBIN_MASS', 'M/1e3'       )
  t.SetAlias('PREBIN_ETA' , 'probe_ETA'   )
  t.SetAlias('PREBIN_P'   , 'probe_P/1e3' )
  t.SetAlias('PREBIN_FILT', filt_jpsi     )
  t.SetAlias('PREBIN_PASS', 'match'       )
  return t


def calc_etrack_via_jpsi_fitting_eta_pt():
  from loop_jpsi_roofit import calc_cb_binning_all
  ROOT.gROOT.SetBatch(True)
  t = load_jpsi_tree()
  calc_cb_binning_all( t, BIN_ETA_LOW, BIN_PT_LOW, mass_min=2.7, mass_max=3.5 )


def calc_etrack_via_jpsi_fitting_eta_p():
  from loop_jpsi_roofit import calc_cb_binning_all
  ROOT.gROOT.SetBatch(True)
  t = load_jpsi_tree_eta_p()
  calc_cb_binning_all( t, BIN_ETA_LOW, BIN_P_LOW, mass_min=2.7, mass_max=3.5 )


# @auto_draw
def eff_dd_eta_pt_lowpt_dirty():
  from loop_jpsi_roofit import read_cb_result
  return read_cb_result( BIN_ETA_LOW, BIN_PT_LOW, 'v160705_muon_fixmatchpt' ).T

# @auto_draw
def eff_dd_p_eta_low_dirty():
  def veto(x):
    if x>1: 
      return np.nan
    if x.s/x.n > 0.5: # 50% rerr
      return np.nan
    return x
  from loop_jpsi_roofit import read_cb_result
  df = read_cb_result( BIN_ETA_LOW, BIN_P_LOW ).T.applymap(veto)
  df.index = 'P'
  return df

#===============================================================================
# DD - HIGH-PT: using Z0
#===============================================================================

def load_tree_zmumu():
  """
  Provide the alias also for the trimmed tree
  """
  tree = import_tree('TrackingEffAlgo/Z', *JIDS )
  filt = utils.join(
    'M   > 60e3',
    'M   < 120e3',
    'VCHI2PDOF < 5',
    # 'TMath::Abs(DPHI) > 1',
    'DPHI > 2.7',
    #
    'tag_ISMUON',
    'tag_TOS_MUON_HIGHPT',
    'tag_PT         > 20e3',
    'tag_ETA        > 2.0',
    'tag_ETA        < 4.5',
    'tag_TRPCHI2    > 0.01',
    # 'tag_TRGHP      < 0.2',
    # 'tag_0.50_cc_IT > 0.9',
    #
    'probe_PT   > 20e3',
    'probe_ETA  > 2.0',
    'probe_ETA  < 4.5',
    # 'probe_TRPCHI2    > 0.01',
    # 'probe_TRGHP      < 0.2',
    # 'probe_0.50_cc_IT > 0.9',
  )
  tree.SetAlias('PRESEL', filt)
  return tree


# @auto_draw
def eff_dd_p_eta_high_z02mumu():

  ## Do 2D binning
  tree = load_tree_zmumu()
  tree.Draw( ':'.join(['match', 'probe_P/1e3', 'probe_ETA']), 'PRESEL', 'goff' )
  df = tree.sliced_dataframe()
  df.columns = 'passing', 'P', 'ETA'

  nume = binning_2D( df[df.passing], P=BIN_P, ETA=BIN_ETA ).apply(lambda x: var(x) if x>0 else 0.)
  deno = binning_2D( df            , P=BIN_P, ETA=BIN_ETA )
  print 'Size: ', nume.sum().n, deno.sum()

  eff = (nume / deno).swaplevel().sort_index().unstack()
  eff.name = 'eff'
  return eff


#===============================================================================
# DD - HIGH-PT: using sfarry result
#===============================================================================

# @auto_draw
def eff_dd_eta_pt_highpt():
  """
  Retrieve the DD high-PT region from sfarry, sanitize to compat format.
  Also collapse [50,70]
  """
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/MuonTracking2012.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  df    = fin.Get('ETA_PT/EffGraph2D').dataframe().T
  fin.Close()
  df.columns = [ x.replace('000','') for x in df.columns ]
  ## collapse
  df_5070 = df.iloc[:,-2:].T.sum()/2
  df = df.iloc[:,:-2]
  df.loc[:,'(50, 70]'] = df_5070
  ## Sanitize
  df.columns.name = 'PT'
  df.index.name   = 'ETA'
  return df.T


def eff_dd_sfarry_eta13():
  """
  Use only ETA from sfarry, claiming that it's independent of PT for PT>3GeV
  """
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/MuonTracking2012.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  df    = fin.Get('ETA13/EfficiencyGraph').dataframe()
  fin.Close()

  ## I know exactly the binning in ETA
  df.index = pd.cut(pd.DataFrame(), BIN_ETA13).categories

  ## Collapse asymerr into ufloat, that's it.
  return df.apply(lambda se: ufloat( se.y, max([se.eylow,se.eyhigh])), axis=1)


@memorized
def eff_dd_sfarry_etaW():
  """
  Like above, but no need for correction (via W study).

  Quoted from Steve:
      This is the W tracking efficiency but now corrected for t&p bias and also 
      matching efficiency. This is the tuple you want.

  """ 
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/WEffs/2012/MuonWTrackingCorr2012.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  df    = fin.Get('ETA/EfficiencyGraph').dataframe()
  fin.Close()

  ## Bin in eta, then collapse asymerr into ufloat, that's it.
  df.index = pd.cut(pd.DataFrame(), BIN_ETA_W).categories
  return df.apply(lambda se: ufloat( se.y, max([se.eylow,se.eyhigh])), axis=1)

#-------------------------------------------------------------------------------

def draw_compare_eff_dd_sfarry():
  """
  Cmopare these cases, as I'm not quite sure which to use.

  1. Efficiencies/MuonTracking2012.root with TRPCHI2 downgrade correction
  2. Efficiencies/WEffs/2012/MuonWTracking2012.root

  """

  ## Standard muon tracking (Stephen & Michael), TRPCHI2 > 0.001 (not compat)
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/MuonTracking2012.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  g0    = fin.Get('ETA13/EfficiencyGraph')
  g0.ownership = False
  g0.name = 'g0'
  fin.Close()

  df = pd.DataFrame({'eff': eff_dd_sfarry_eta13_corrected()})
  g1 = list(drawer_graphs_raw(df))[0]
  g1.ownership = False
  g1.name = 'g1'
  g1.lineColor = ROOT.kMagenta

  ## uncorrected for t&p bias and matching eff.
  # fin = ROOT.TFile( '../sfarry/Efficiencies/WEffs/2012/MuonWTracking2012.root')
  # g2  = fin.Get('ETA8/EfficiencyGraph')
  # g2.ownership = False
  # g2.name = 'g2'
  # g2.lineColor = ROOT.kRed
  # fin.Close()

  ## TRPCHI2 > 0.01 from W paper
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/WEffs/2012/MuonWTrackingCorr2012.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  g3    = fin.Get('ETA8/EfficiencyGraph')
  g3.ownership = False
  g3.name = 'g3'
  g3.lineColor = ROOT.kBlue
  fin.Close()

  ROOT.gROOT.SetBatch(False)

  g = ROOT.TMultiGraph()
  # g.Add(g0, 'AP')
  g.Add(g1, 'AP')
  g.Add(g3, 'AP')
  g.Draw('AP')
  ROOT.gPad.SetGrid( 1, 1 )
  # ROOT.gPad.BuildLegend()
  exit()


#===============================================================================
# DD: Combine lowPT + highPT together
# Aim to have muon etrack as a func of PT
# the choice of weighting in ETA can be quite complicate...
# - using mu1_, mu2_ for muons mumu channel
# - using mu_ for muon in h1mu, h3mu channel
# - using pi_ to have a scaling shape tauh1
# - using pr1_, pr2_, pr3_, to have scaling shape for tauh3
#
# Since this is binned only in 1 var now, uses different columns as
# different weighting scheme.
#===============================================================================

# @auto_draw
def eff_dd_eta_pt_dirty():
  """
  Just putting the two together, getting the axis right. No correction yet (dirty).
  """
  df_low  = eff_dd_eta_pt_lowpt_dirty()
  df_high = eff_dd_eta_pt_highpt()
  return pd.concat([ df_low, df_high ])

# @auto_draw
def eff_dd_eta_pt_veto():
  def veto(x):
    if x>1.1:
      return np.nan 
    if x.s/x.n > 0.04: # limit relative uncertainty to 4%
      return np.nan 
    return x
  eff = eff_dd_eta_pt_dirty().applymap(veto)

  # For ETA (2, 2.5], the value is noticably degraded fast. Veto it.
  eff.loc['(20, 30]', '(2, 2.5]'] = np.nan
  eff.loc['(6, 10]' , '(2, 2.5]'] = np.nan
  eff.loc['(6, 10]' , '(2.5, 3]'] = np.nan

  return eff
  

# @auto_draw
def eff_dd_eta_pt():
  """
  Like above, but with following correction, for smooth function
  - Kill bad binning result, judged by eye.
  - Amend the mising PT bin by connecting the dot linearly
  """
  eff = eff_dd_eta_pt_veto()
  print 'after veto'
  print eff

  eff_dirty = eff_dd_eta_pt_dirty()

  ## Because of the log, scale, it's easier to do interpolation by hand
  #
  # take bake from dirty
  eff.iloc[0,0] = eff_dirty.iloc[0,0] # (1, 1.5], (2, 2.5]
  eff.iloc[0,4] = eff_dirty.iloc[0,4] # (1, 1.5], (4, 4.5]
  #
  # (10, 20], (2, 2.5]
  eff.iloc[4,0] = eff.iloc[3,0] + ( 8.-5)/(35.-5) * (eff.iloc[7,0]-eff.iloc[3,0])
  eff.iloc[5,0] = eff.iloc[3,0] + (15.-5)/(35.-5) * (eff.iloc[7,0]-eff.iloc[3,0])
  eff.iloc[6,0] = eff.iloc[3,0] + (25.-5)/(35.-5) * (eff.iloc[7,0]-eff.iloc[3,0])
  #
  # (10, 20], (2.5, 3]
  eff.iloc[4,1] = eff.iloc[3,1] + ( 8.-5)/(25.-5) * (eff.iloc[6,1]-eff.iloc[3,1])
  eff.iloc[5,1] = eff.iloc[3,1] + (15.-5)/(25.-5) * (eff.iloc[6,1]-eff.iloc[3,1])

  # (10, 20], (3, 3.5]
  eff.iloc[5,2] = eff.iloc[4,2] + (15.-8)/(25.-8) * (eff.iloc[6,2]-eff.iloc[4,2])
  # (6,10](10,20], (3.5,4]
  eff.iloc[4,3] = eff.iloc[3,3] + (8.-5)/(25.-5)  * (eff.iloc[6,3]-eff.iloc[3,3])
  eff.iloc[5,3] = eff.iloc[3,3] + (15.-5)/(25.-5) * (eff.iloc[6,3]-eff.iloc[3,3])
  # (6,10](10,20], (4,4.5]
  eff.iloc[4,4] = eff.iloc[3,4] + (8.-5)/(25.-5)  * (eff.iloc[6,4]-eff.iloc[3,4])
  eff.iloc[5,4] = eff.iloc[3,4] + (15.-5)/(25.-5) * (eff.iloc[6,4]-eff.iloc[3,4])
  #
  ## 
  # (50, 70], (4, 4.5], approximate the jump
  eff.iloc[9,-1] = (eff.iloc[9,:-1]/eff.iloc[8,:-1]).sum()/4 * eff.iloc[8,-1]
  #
  return eff


# def eff_dd_pt_forhad():
#   """
#   Weighted in ETA, binned only in PT, to be used for the scaling on hadron tracking.

#   NOTE: the hadron share a very similar ETA profile, so let's join them together
#   """
#   def reweight_eta(se):
#     pt  = se.name
#     w   = se.loc[pt]
#     e   = eff[pt]
#     tot = w.sum()
#     if tot==0.:
#       return np.nan
#     return (w*e).sum()/tot

#   def sum_skipna(se):
#     l = [ x for x in se.values if str(x)!='nan' ]
#     return sum(l)/len(l)

#   eff     = eff_dd_eta_pt().stack()
#   weights = weights_pt_eta_allpt_uniformeta()
#   acc     = {}
#   for src in ['h1mu_pi', 'h3mu_pr', 'eh1_pi', 'eh3_pr']:
#     res = weights[src].groupby(level='PT').apply(reweight_eta)
#     acc[src] = res
#   return pd.DataFrame(acc).apply(sum_skipna, axis=1)

#===============================================================================

if __name__ == '__main__':
  pass

  ## Jpsi study (DD low-PT dirty)
  # estimate_fitting_stat()
  # calc_etrack_via_jpsi_fitting_eta_pt()
  # calc_etrack_via_jpsi_fitting_eta_p()
  # print eff_dd_eta_pt_lowpt_dirty().applymap('{:.3f}'.format)
  # print eff_dd_eta_p_lowpt_dirty()

  ## DD: High-PT, redo Z0
  # print eff_dd_p_eta_high_z02mumu()

  ## DD: High PT (using sfarry)
  # print eff_dd_eta_pt_highpt()
  # print eff_dd_sfarry_eta()
  # print eff_dd_sfarry_eta13_corrected()
  # print eff_dd_sfarry_etaW()
  # draw_compare_eff_dd_sfarry()

  ## DD: Combining low+high
  # print eff_dd_eta_pt_dirty()
  # print eff_dd_eta_pt_veto()
  # print eff_dd_eta_pt()
  # print eff_dd_pt_forhad()
