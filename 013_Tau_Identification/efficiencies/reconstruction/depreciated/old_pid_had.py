
#===============================================================================
#===============================================================================

def init_old():
  ## Borrow the signal MC DitauRecoStats
  # from kinematic import *

  tname = 'evttup_PID_had/had'
  t_mc_minibias = import_tree( tname, 4255 ).SetTitle('MC: MiniBias 30000000')
  t_mc_hardqcd  = import_tree( tname, 4253 ).SetTitle('MC: HardQCD  49001000')
  t_data_S20    = import_tree( tname, 4252 ).SetTitle('S20')
  t_part_S20    = import_tree( 'parttup_PID_had/had', 4252 ).SetTitle('S20 (particles)')
  # t_data_S20r1_MBNoBias  = import_tree( tname, 4162 ).SetTitle('S20r1: MBNoBias')

  t_mc_hardqcd.SetAlias ('filtered', '1==1' )
  t_mc_minibias.SetAlias('filtered', '1==1' )
  t_data_S20.SetAlias   ('filtered', '1==1' )
  t_part_S20.SetAlias   ('filtered', '1==1' )
  # t_data_S20.SetAlias   ('filtered', 'StrippingMBNoBiasDecision' )

  ## Integrate with signal MC
  dtype = 'h1_mu'
  filt  = ALL_CUTS(dtype)[:5] # Cut with track-quality
  t_mc_z02tautau = TREES(dtype).SetTitle('MC: Z02TauTau '+dtype)
  t_mc_z02tautau.SetAlias('filtered', utils.join(filt))

  ## for schema compat
  t_mc_z02tautau.SetAlias('nSPDHits'    , 'nSPDhits'   )
  t_mc_z02tautau.SetAlias('P'           , 'had_P'      )
  t_mc_z02tautau.SetAlias('PERR2'       , 'had_PERR2'  )
  t_mc_z02tautau.SetAlias('ETA'         , 'had_ETA'    )
  t_mc_z02tautau.SetAlias('PHI'         , 'had_PHI'    )
  t_mc_z02tautau.SetAlias('PT'          , 'had_PT'     )
  t_mc_z02tautau.SetAlias('TRGHP'       , 'had_TRGHOSTPROB' )
  t_mc_z02tautau.SetAlias('TRPCHI2'     , 'had_TRPCHI2'     )
  t_mc_z02tautau.SetAlias('HCALFrac'    , 'had_HCALFrac'    )
  t_mc_z02tautau.SetAlias('PROBNNghost' , 'had_PROBNNghost' )
  t_mc_z02tautau.SetAlias('ISMUONLOOSE' , 'had_ISLOOSEMUON' )

  t_part_S20.SetAlias('P'           , 'piplus_P'          )
  t_part_S20.SetAlias('PERR2'       , 'piplus_PERR2'      )
  t_part_S20.SetAlias('ETA'         , 'piplus_ETA'        )
  t_part_S20.SetAlias('PHI'         , 'piplus_PHI'        )
  t_part_S20.SetAlias('PT'          , 'piplus_PT'         )
  t_part_S20.SetAlias('TRGHP'       , 'piplus_TRGHP'      )
  t_part_S20.SetAlias('TRPCHI2'     , 'piplus_TRPCHI2'    )
  t_part_S20.SetAlias('HCALFrac'    , 'piplus_HCALFrac'   )
  t_part_S20.SetAlias('PROBNNghost' , 'piplus_PROBNNghost')
  t_part_S20.SetAlias('ISMUONLOOSE' , 'piplus_ISMUONLOOSE')

  for tree in (t_mc_z02tautau, t_mc_minibias, t_mc_hardqcd, t_data_S20, t_part_S20):
    tree.SetAlias('PRERR'   , '(PERR2**0.5)/P')
    # tree.SetAlias("NOGHOST" , "(PROBNNghost<0.1) & (PROBNNghost>=0) & (TRGHP<0.002)")
    tree.SetAlias("MATCH"   , "(HCALFrac>0.05)&(!ISMUONLOOSE)")



#===============================================================================
# Single MC tauh3
#===============================================================================

def draw():
  assert 'h3' in dtype

  t_h3    = TREES('h3_mu')
  t_h3.SetAlias('filtered', utils.join(ALL_CUTS('h3_mu')[:5]))

  t_h1    = TREES('h1_mu')
  t_h1.SetAlias('filtered', utils.join(ALL_CUTS('h1_mu')[:5]))

  cut_h3 = utils.join(
    'filtered', 
    'nSPDhits<600', 
    'pr1_ETA>2.25', 
    'pr1_ETA<3.75', 
    'pr2_ETA>2.25', 
    'pr2_ETA<3.75',
    'pr3_ETA>2.25',
    'pr3_ETA<3.75',
  )

  cut_h1 = utils.join(
    'filtered', 
    'nSPDhits<600', 
    'had_ETA>2.25', 
    'had_ETA<3.75', 
  )

  exp = [
    ( t_h1, '(had_HCALFrac>0.05&!had_ISLOOSEMUON):had_PT', cut_h1 ),
    ( t_h3, '(pr1_HCALFrac>0.05&!pr1_ISLOOSEMUON):pr1_PT', cut_h3 ),
    ( t_h3, '(pr2_HCALFrac>0.05&!pr2_ISLOOSEMUON):pr2_PT', cut_h3 ),
    ( t_h3, '(pr3_HCALFrac>0.05&!pr3_ISLOOSEMUON):pr3_PT', cut_h3 ),
  ]

  h = QHist()
  h.name      = 'mc_MATCH_PT'
  h.expansion = exp 
  h.xlog      = True 
  h.options   = 'prof'
  h.ymin      = 0.4
  h.ymax      = 1.1
  h.xmin      = 5E2
  h.xmax      = 1E5
  h.xbin      = 20
  h.legends   = 'h1', 'pr1', 'pr2', 'pr3'

  h.draw()

  exit()


#===============================================================================
# - Compare all suspecting vars across all samples
# - Compare effect of ghost removal in fixed sample
#===============================================================================

def draw():

  QHist.reset()
  QHist.filters = 'filtered', 'ETA>2.25', 'ETA<3.75', 'nSPDHits<600', 'TRGHP < 0.2'
  # QHist.filters = 'filtered', 'NOGHOST', 'ETA>2.25', 'ETA<3.75', 'nSPDHits<600'

  ## For comparison across different trees
  # QHist.trees   = t_mc_z02tautau, t_mc_minibias, t_mc_hardqcd, t_data_S20, t_data_S20r1_MBNoBias
  QHist.trees   = t_mc_z02tautau, t_mc_hardqcd, t_mc_minibias, t_part_S20

  ## For comparison with/without ghost on fixed tree
  # QHist.trees   = t_data_S20
  # QHist.trees   = t_mc_hardqcd
  # QHist.cuts    = '', 'NOGHOST'

  h = QHist()
  h.params = 'TRGHP'
  h.xmin   = 1E-6
  h.xmax   = 2
  h.xlog   = True
  h.ylog   = True
  # h.draw()

  h = QHist()
  h.params = 'PROBNNghost'
  h.xmin   = 1E-4
  h.xmax   = 2
  h.xlog   = True
  h.ylog   = True
  # h.draw()

  # exit()

  h = QHist()
  h.params = 'nPVs'
  h.xmin   = 0
  h.xmax   = 11
  h.xbin   = 11
  # h.draw()

  h = QHist()
  h.params = 'nTracks'
  h.xmax   = 600
  h.draw()

  h = QHist()
  h.params = 'nSPDHits'
  h.xlog   = False
  # h.draw()

  h = QHist()
  h.params = 'P'
  h.xmin   = 1E4
  h.xmax   = 1E6
  h.xlog   = True
  h.draw()

  h = QHist()
  h.params = 'PRERR'
  h.xmin   = 1E-3
  h.xmax   = 1E0
  h.ylog   = True
  # h.draw()

  h = QHist()
  h.params = 'ETA'
  h.xmin   = 2.25
  h.xmax   = 3.75
  h.xbin   = 30
  h.draw()

  h = QHist()
  h.params = 'PHI'
  # h.draw()

  h = QHist()
  h.params = 'PT'
  h.xmin   = 4E3
  h.xmax   = 1E5
  h.xlog   = True
  # h.draw()

  h = QHist()
  h.params = 'TRPCHI2'
  h.xlog   = True
  # h.draw()

  exit()

# draw()

#===============================================================================
# Gauge which vars actually depends on, for reweighting
# 
# Verdict:
# - 
#===============================================================================

def draw():

  ## For all
  filt = utils.join(
    'filtered', 
    'ETA>2.25', 
    'ETA<3.75', 
    'nSPDHits<600', 
    'TRGHP < 0.2',
    #
    # 'nPVs==1',
    # 'PT < 10E3',
  )

  ## replace alias
  # *Narrow Bin Approximation*
  # t_part_S20.SetAlias('filtered', '(PT>9E3) & (PT<10E3) & (TRGHP<1E-2) & (TRGHP>1E-3) & (nSPDHits>150)&(nSPDHits<200)')
  # t_part_S20.SetAlias('filtered', '(PROBNNghost<0.1) & (PT>9E3) & (PT<10E3) & (TRGHP<1E-3) & (TRGHP>1E-4) & (nTracks>80)&(nTracks<100)')

  QHist.reset()
  QHist.filters = filt
  QHist.trees   = t_mc_z02tautau, t_mc_hardqcd, t_mc_minibias, t_part_S20
  QHist.options = 'prof'
  QHist.ymin    = 0.7
  QHist.ymax    = 1.1
  QHist.auto_name = True

  h = QHist()
  h.params = 'MATCH: PROBNNghost'
  h.xlog   = True
  h.xmin   = 1E-6
  h.xmax   = 1
  h.xbin   = 12
  # h.draw()

  h = QHist()
  h.params = 'MATCH: TRGHP'
  h.xlog   = True
  h.xmin   = 1E-6
  h.xmax   = 1E0
  h.xbin   = 12
  # h.draw()

  h = QHist()
  h.params = 'MATCH: nPVs'
  h.xmin   = 0
  h.xmax   = 11
  h.xbin   = 11
  # h.draw()

  h = QHist()
  h.params = 'MATCH: nTracks'
  h.xbin   = 10
  h.xmax   = 600
  # h.draw()

  h = QHist()
  h.params = 'MATCH: nSPDHits'
  h.xbin   = 10
  h.xmax   = 600
  # h.draw()

  h = QHist()
  h.params = 'MATCH: P'
  h.xmin   = 1E4
  h.xmax   = 1E6
  h.xbin   = 20
  h.xlog   = True
  # h.draw()

  h = QHist()
  h.params = 'MATCH: ETA'
  h.xbin   = 10
  # h.draw()

  h = QHist()
  h.params = 'MATCH: PHI'
  h.xbin   = 20
  # h.draw()

  h = QHist()
  h.params = 'MATCH: PT'
  h.xlog   = True
  h.xmin   = 1E3
  h.xmax   = 1E5
  h.xbin   = 15
  h.draw()

  h = QHist()
  h.params = 'MATCH: TRPCHI2'
  h.xlog   = True
  h.xbin   = 15
  # h.draw()

  exit()

# draw()

