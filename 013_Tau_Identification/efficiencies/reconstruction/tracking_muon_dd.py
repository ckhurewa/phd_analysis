#!/usr/bin/env python
"""

Handle the data-driven sources of muon tracking efficiency

"""

from rec_utils import *
from tracking_bins import *

#===============================================================================
# DD - HIGH-PT: using sfarry result
#===============================================================================

@memorized
def eff_dd_sfarry_etaW():
  """
  Like above, but no need for correction (via W study).

  Quoted from Steve:
      This is the W tracking efficiency but now corrected for t&p bias and also 
      matching efficiency. This is the tuple you want.

  """ 
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/WEffs/2012/MuonWTrackingCorr2012.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  df    = fin.Get('ETA/EfficiencyGraph').dataframe()
  fin.Close()
  logger.info('Imported: %s'%fpath)

  ## Bin in eta, then collapse asymerr into ufloat, that's it.
  df.index = pd.cut(pd.DataFrame(), BIN_ETA_W).categories
  return df.apply(lambda se: ufloat( se.y, max([se.eylow,se.eyhigh])), axis=1)


#===============================================================================
# FINALLY
#===============================================================================

@memorized # For the correct correlation
def efficiencies():
  ## Alias to final choice
  return completely_correlate(eff_dd_sfarry_etaW())

def lookup(*args):
  return lookup_eff_generic(__file__, efficiencies(), _ETA=BIN_ETA_W)(*args)

#===============================================================================

if __name__ == '__main__':

  ## Finally
  print efficiencies()
  # print lookup()
