## Delete the cache and re-run before the full rec.py computation. Use with care.

rm -vrf kinematic/*.df

./kinematic.py --cache

