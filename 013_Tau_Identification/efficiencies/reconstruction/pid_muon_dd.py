#!/usr/bin/env python
"""

Calculate muon identification efficiency from data.

"""

from rec_utils import *
from pid_muon_bins import *

#===============================================================================
# COMBINE LOW + HIGH
#===============================================================================

# @auto_draw
@pickle_dataframe_nondynamic
def eff_combine_eta_pt():
  """
  Combine the eff from low-PT and high-PT regions together at nominal binning.
  """
  ## Import all regions
  sys.path.append('pid_muon_dd')
  import lowpt_jpsi     # 5, 7, 10, 15, 20, 25, 30
  import highpt_sfarry  # 20, 25, 30, 35, 40, 45, 50, 70
  eff_low = lowpt_jpsi.efficiencies()
  eff_hi  = highpt_sfarry.efficiencies()

  ## merge [20, 25, 30] together
  eff_20_25 = pd.DataFrame({'(20, 25]':pd.concat([eff_low.iloc[-2], eff_hi.iloc[0]], axis=1).apply(combine_uncorrelated, axis=1)}).T
  eff_25_30 = pd.DataFrame({'(25, 30]':pd.concat([eff_low.iloc[-1], eff_hi.iloc[2]], axis=1).apply(combine_uncorrelated, axis=1)}).T

  ## Collect together, completely corr, done.
  eff = pd.concat([eff_low.iloc[:-2], eff_20_25, eff_25_30, eff_hi.iloc[2:]])
  eff = completely_correlate(eff.stack()).unstack()
  return eff


#===============================================================================
# FINALLY
#===============================================================================

def scalar_results():
  raise NotImplementedError('no longer used')
  queue   = ['mumu_mu1', 'mumu_mu2', 'h1mu_mu', 'h3mu_mu', 'emu_mu']
  eff     = eff_dd_eta_pt().T.unstack()
  weights = weight_eta_pt_global()[queue]
  for name in queue:
    print name, format_eff( weights[name], eff )

#-------------------------------------------------------------------------------

def lookup(*args):
  return lookup_eff_generic(__file__, eff_combine_eta_pt().unstack(), _PT=BIN_PT_NOMINAL, _ETA=BIN_ETA_NOMINAL)(*args)

#===============================================================================

if __name__ == '__main__':
  ## Combine
  # print eff_combine_eta_pt().fmt2p

  ## Finally
  print lookup().unstack().T.fmt2p

