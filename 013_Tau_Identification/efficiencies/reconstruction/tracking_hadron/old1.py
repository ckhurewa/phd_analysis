#!/usr/bin/env python


#===============================================================================
# DATA-DRIVEN: Jpsi fitting
#===============================================================================

def calc_etrack_via_jpsi_fitting():
  """
  The particularity is that the binning in ETA is at reduced value [2.25, 3.75]
  """
  from loop_jpsi_roofit import calc_cb_binning_all

  ROOT.gROOT.SetBatch(True)
  t = tracking_muon.load_jpsi_tree()
  calc_cb_binning_all( t, BIN_ETA, BIN_PT_LOW, mass_min=2.7, mass_max=3.5 )


@auto_draw
@pickle_dataframe_nondynamic
def muon_eff_dd_eta_pt_lowpt_dirty():
  from loop_jpsi_roofit import read_cb_result
  return read_cb_result( BIN_ETA, BIN_PT_LOW ).T


@auto_draw
@pickle_dataframe_nondynamic
def muon_eff_dd_pt_dirty():
  """
  Weights in ETA of the hadrons. Since this still effectively muon's etrack,
  and we're only interested for ETA of hadrons, treat ETA of hadron as the same
  in order to have larger stats and long graph.
  Safe to sum together across channel because what I just want is the ETA dist
  in each PT bin, and each PT bin are independent from each other anyway.
  """
  cols_hadrons  = ['h1mu_pi', 'h3mu_pr', 'eh1_pi', 'eh3_pr']
  w = weights_eta_pt()[cols_hadrons].sum(axis=1).unstack().T
  e = muon_eff_dd_eta_pt_lowpt_dirty().T
  print w
  print e

  ## collapse, sort, keep only low-PT, veto last two bin (poor)
  se1 = sort_intv_pt((w*e).sum()/w.sum().replace(0.,1.)).loc[:'(10, 20]']
  se1.loc['(6, 10]']  = 0.
  se1.loc['(10, 20]'] = 0.
  print se1

  ## Now, get the high-PT from sfarry, and also bin in hadrons
  w = weights_eta_pt_highmu()[cols_hadrons].sum(axis=1).unstack().T
  e = tracking_muon.eff_dd_eta_pt_highpt().T
  se2 = sort_intv_pt((w*e).sum()/w.sum().replace(0.,1.))
  print se2

  ## Join & interpolate
  se = pd.concat([se1, se2])
  se.iloc[4] = se.iloc[3] + ( 8.-5)/(25.-5)*(se.iloc[6]-se.iloc[3])
  se.iloc[5] = se.iloc[3] + (15.-5)/(25.-5)*(se.iloc[6]-se.iloc[3])
  return pd.DataFrame({'eff':se})


#===============================================================================
# DATA-DRIVEN
#===============================================================================

def join_eff_muon_hadron_pt():
  """
  Carefully join etrack from muon-hadron together for comparative study.
  """
  import tracking_muon
  df0 = tracking_muon.eff_mc_pt().mumu_sum
  df1 = muon_eff_dd_pt_dirty().iloc[:,0]
  df2 = eff_mc_pt()

  ## Different weighting scheme
  srcs = 'eh1_pi', 'h1mu_pi', 'h3mu_pr', 'eh3_pr'
  acc  = []
  for src in srcs:
    df = pd.DataFrame({
      'muon_mc'  : df0,   # use as constant model
      'muon_dd'  : df1,   # shape of muon, ETA binned in hadrons, which are similar.
      'hadron_mc': df2[src],
    })
    df.columns = pd.MultiIndex.from_tuples([(src,x) for x in df.columns], names=['src','type'])
    df.index.name = 'PT'
    acc.append(df.T)
  df = sort_intv_pt(pd.concat(acc).T)
  return df


def join2_eff_muon_hadron_pt():
  """
  Adding HadronDD, obtained by combing result from 2 methods:

  1. apply ratio (MuonDD / MuonMC) to HadronMC
  2. apply ratio (HadronMC / MuonMC) to MuonDD

  For each weighting regime, do it case-by-case.

  Preserve the 4 sources altogether here for dev.
  """

  df0 = join_eff_muon_hadron_pt()
  acc = []

  ## h3mu_pr, eh3_pr @ method2
  intv = ['(4, 6]']  # best intv for scaling
  # intv = ['(4, 6]', '(6, 10]', '(10, 20]', '(20, 30]'] # range for scaling
  for src in ['h3mu_pr', 'eh3_pr']:
    df    = pd.DataFrame(df0[src]) # make a new copy
    scale = (df.hadron_mc[intv] / df.muon_mc[intv]).sum() / len(intv) # scale range
    # Fix the scale's syst to LHCB budget, see Phil's
    scale = ufloat( scale.n, scale.n * .015 )
    df['hadron_dd'] = scale * df.muon_dd 
    df.columns      = pd.MultiIndex.from_tuples([(src,x) for x in df.columns])
    acc.append(df.T)
    print 'Scale:', src, scale

  ## h1mu_pi @ method2
  # Because muon_dd,muon_mc shape using h1mu_pi weight has no data for low-pt region,
  # borrow the shape from h3mu_pr instead, effectively borrowing only ETA dist
  # of prongs in h3 into pi in h1.

  # intv = ['(10, 20]'] # single intv
  intv = ['(10, 20]', '(20, 30]', '(30, 40]', '(40, 50]'] # range for scaling
  for src in ['h1mu_pi', 'eh1_pi']:
    df  = pd.DataFrame({
      'muon_mc'  : df0[src].muon_mc,
      'muon_dd'  : df0[src].muon_dd,
      'hadron_mc': df0[src].hadron_mc,
    })
    scale = (df.hadron_mc[intv] / df.muon_mc[intv]).sum() / len(intv) # scale range
    scale = ufloat( scale.n, scale.n * .015 )
    df['hadron_dd'] = scale * df.muon_dd 
    df.columns      = pd.MultiIndex.from_tuples([(src,x) for x in df.columns])
    acc.append(df.T)
    print 'Scale:', src, scale

  ## Finally
  df = pd.concat(acc).T
  df.columns.names = 'src', 'stage'
  return df


@pickle_dataframe_nondynamic
def eff_dd_pt():
  """
  Finally, collapsing the result from previous stage to just effective HardonDD.
  - forward-fill missing eff 
  """
  def cleaner(x):
    if x==0.:
      return np.nan 
    if math.isnan(x.n):
      return np.nan
    return x
  df = join2_eff_muon_hadron_pt().xs('hadron_dd', axis=1, level=1)
  df = df.applymap(cleaner).fillna(method='ffill')

  ## inflate smaller prongs here for smooth algo (but more verbose)
  df['h3mu_pr1'] = df['h3mu_pr2'] = df['h3mu_pr3'] = df.h3mu_pr
  df['eh3_pr1']  = df['eh3_pr2']  = df['eh3_pr3']  = df.eh3_pr
  return df

def draw_comb_eff_muon_hadron_pt():
  """
  Compare etrack as a func of pt, for muonMC, muonDD, hadronMC.
  """
  df0 = join2_eff_muon_hadron_pt()
  df0.columns.names = 'src', 'kind'

  for src,df in df0.groupby(level='src', axis=1):
    df  = df[src]
    c   = ROOT.TCanvas('c_'+src)
    graphs, leg = drawer_graphs_eff_pt(df)
    graphs.xaxis.rangeUser = 1, 60
    gPad.Update()
    gPad.SaveAs('tracking_hadron/eff_pt_%s.pdf'%src)


#===============================================================================
# FINAL SCALAR EFF
# Calculate in all sources for comparison.
#===============================================================================

def calc_weighted_eff_allsrc_h3mu_eh3():
  print 'Summary h3mu. eh3'
  # ch,queues = 'eh3_pr' , [ 'eh3_pr1'  , 'eh3_pr2'  , 'eh3_pr3'  ]
  ch,queues = 'h3mu_pr', [ 'h3mu_pr1' , 'h3mu_pr2' , 'h3mu_pr3' ]

  ## Preloading
  weights = weights_eta_pt()
  eff_mc1 = eff_mc_eta_pt()
  eff_mc2 = eff_mc_pt()[ch]
  eff_dd  = eff_dd_pt()[ch]

  ## 1. MC: Full ETA-PT weight, prongs not combined
  print 'MC'
  for name in queues:
    eff = eff_mc1[name]
    w   = weights[name]
    print name, format_eff( w, eff )

  ## 2. MC: Weighted in PT, prongs-maxes combined
  print 'MC prong-combined'
  for name in queues:
    w = weights[name]
    print name, format_eff( w, eff_mc2 )

  ## 3. DD: weight in PT (eta already consumed)
  print 'DD'
  for name in queues:
    w = weights[name].unstack().T.sum()
    print name, format_eff( w, eff_dd )


def calc_weighted_eff_allsrc_h1mu_eh1():
  print 'Summary h1mu,eh1'
  name    = 'h1mu_pi'
  # name    = 'eh1_pi'
  weights = weights_eta_pt()

  ## 1. MC: Full ETA-PT weight
  print '1. MC (eta-pt)'
  eff = eff_mc_eta_pt()[name]
  w   = weights[name]
  print name, format_eff( w, eff )

  ## 2. MC: Weighted in PT
  print '2. MC (pt)'
  eff = eff_mc_pt()[name]
  w   = weights[name]
  print name, format_eff( w, eff )

  ## 3. DD: weight in PT (eta already consumed)
  print '3. DD'
  eff = eff_dd_pt()[name]
  w   = weights[name].unstack().T.sum()
  print name, format_eff( w, eff )


def calc_eff_mc_eta_ntracks():
  weights = weights_eta13_ntracks()
  eff     = eff_mc_eta13_ntracks_corrected()

  queues = [
    #  weight    , eff
    ( 'h1mu_pi' , 'h1mu_pi' ),
    ( 'h3mu_pr1', 'h3mu_pr' ),  
    ( 'h3mu_pr2', 'h3mu_pr' ),  
    ( 'h3mu_pr3', 'h3mu_pr' ),  
  ]

  for wname, ename in queues:
    w = weights[wname]
    e = eff[ename]
    print wname, format_eff( w, e )
