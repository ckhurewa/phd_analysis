#!/usr/bin/env python

from rec_utils import *
import tracking_muon

## eta for hadron is always [2.25, 3.75]
# BIN_ETA = 2.25, 2.75, 3.25, 3.75
# BIN_ETA = 2.25, 2.55, 2.85, 3.15, 3.45, 3.75

## pt is constraint by the source of low-PT dd, using jpsi study 
# high-pt is not important, almost always empty.
BIN_PT  = 1, 1.5, 2.5, 4, 6, 10, 20, 30, 40, 50, 70
# BIN_PT  = 1, 2.5, 6, 20, 30, 40, 50, 70

BIN_NTRACKS_MC = 0, 100, 200, 300, 400, 600, 1000

## Used only in Jpsi study
BIN_PT_LOW  = 1, 1.5, 2.5, 4, 6, 10, 20
# BIN_PT_LOW  = 1, 2.5, 6, 20

## Used to collapse high-PT muon
BIN_ETA_HIGHMU = 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
BIN_PT_HIGHMU  = 20, 30, 40, 50, 70

## The choice of ETA binning in 13 bins by sfarry
# note: with reduced acceptance [2.25, 3.75]
# BIN_ETA13 = 2.25, 2.375, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75
# BIN_ETA_W = 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75

## Binning parallel to muon's W binning (for syst)
BIN_ETA_W = 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5

## No binning
BIN_ETA_NONE = 2.25, 3.75
BIN_PT_NONE  = 1, 300
BIN_NTRACKS_NONE = 0, 1000

## Final hadron columns
COLS = ['h1mu_pi', 'eh1_pi', 'h3mu_pr', 'eh3_pr']

#===============================================================================
# WEIGHTS
#===============================================================================

@pickle_dataframe_nondynamic
def weights_etaW_ntracksMC():
  return weighter_eta_ntracks( bin_eta=BIN_ETA_W, bin_ntracks=BIN_NTRACKS_MC )


#===============================================================================
# MC
#===============================================================================

@pickle_dataframe_nondynamic
def eff_mc_eta_ntracks_raw():
  """
  The main one.
  """
  return calc_etrack_mc_raw(bin_eta=BIN_ETA_W, bin_pt=BIN_PT_NONE, bin_ntracks=BIN_NTRACKS_MC)


@pickle_dataframe_nondynamic
def eff_mc_etaW():
  """
  For visualization only.
  """
  return calc_etrack_mc_raw( BIN_ETA_W, BIN_PT_NONE, BIN_NTRACKS_NONE )


@memorized
def eff_mc_eta_ntracks_addedsyst():
  """
  Pure etrack from MC, but with additional material & data-MC uncertainty.
  """
  syst  = tracking_muon.calc_syst_dd_mc() #.iloc[1:-3] # only [2.25, 3.75]
  eff   = eff_mc_eta_ntracks_raw().loc[Idx['litediv']][COLS]
  eff   = eff.unstack().apply(lambda se: se*syst).T.unstack().T
  eff   = eff * ufloat( 1, 0.014 )  # material syst

  ## fix name
  eff.index.names = [ s.replace('MC','') for s in eff.index.names]
  ## Apply trick to let pr1,pr2,pr3 use pr (combined stat) instead.
  eff['h3mu_pr1']  = eff['h3mu_pr2'] = eff['h3mu_pr3'] = eff['h3mu_pr']
  eff['eh3_pr1']   = eff['eh3_pr2']  = eff['eh3_pr3']  = eff['eh3_pr']
  # finally
  return eff

def eff_mc_ntracks_addedsyst():
  """
  binned in ntrack only.
  Obtained by integrating over all ETA from 2D ETA-NTRACKS bins.
  """
  eff = eff_mc_eta_ntracks_addedsyst()[COLS].swaplevel()
  wgh = weights_etaW_ntracksMC()[COLS].swaplevel()
  w2  = wgh.groupby(level=0).sum().replace(0, 1.)
  return (eff*wgh).groupby(level=0).sum()/w2


#===============================================================================

@memorized
def calc_syst_dd_mc_scalar():
  """
  Approximate value of syst of TRPCHI2 as single scalar, 
  useful for tauh3 when multiplied by 3
  """
  e = tracking_muon.calc_syst_dd_mc()
  w = weights_etaW_ntracksMC()[['h3mu_pr1', 'h3mu_pr2', 'h3mu_pr3']]
  w = w.sum(axis=1).unstack().T.sum()
  return sum_fullcorr(e*w.apply(nominal_value)) / w.sum()


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_mc_etaW():
  """
  For report
  """
  ## Prepare input
  eff   = eff_mc_etaW().loc['litediv',COLS]
  syst  = tracking_muon.calc_syst_dd_mc() #.iloc[1:-3] # only [2.25, 3.75]
  eff   = eff.apply(lambda se: se*syst) * ufloat( 1, 0.014 )  # material syst
  eff.columns = [
    '#tau_{#mu}#tau_{h1}',
    '#tau_{e}#tau_{h1}',
    '#tau_{#mu}#tau_{h3}',
    '#tau_{e}#tau_{h3}',
  ]

  ## Draw
  gr,leg = drawer_graphs_eff_pt( eff )
  gr.xaxis.title = '#eta_{h}'
  gr.yaxis.title = '#varepsilon_{track,h}'
  gr.minimum     = 0.5
  gr.maximum     = 1.0
  leg.header     = 'LHCb-Simulation'
  leg.nColumns   = 2
  leg.x1NDC      = 0.65
  leg.x2NDC      = 0.92
  leg.y1NDC      = 0.74
  leg.y2NDC      = 0.94
  leg.textSize   = 0.06
  ROOT.gPad.logx = False


@gpad_save
def draw_eff_mc_h1mu_eta_ntracks():
  eff = eff_mc_eta_ntracks_addedsyst()['h1mu_pi'].unstack()
  eff = eff.drop('(600, 1000]', axis=1)

  ## Draw
  gr,leg = drawer_graphs_eff(eff)
  gr.minimum     = 0.
  gr.maximum     = 1.
  gr.xaxis.title = '#eta(h)'
  gr.yaxis.title = '#varepsilon_{track,h}'
  ROOT.gPad.logx = False
  leg.header     = 'LHCb-Simulation'
  leg.x1NDC      = 0.50
  leg.x2NDC      = 0.85
  leg.y1NDC      = 0.20
  leg.y2NDC      = 0.55


#===============================================================================
# FINAL TABLE
#===============================================================================

def lookup_eff_mc(*args):
  eff = eff_mc_eta_ntracks_addedsyst()
  return lookup_eff_generic( eff, _ETA=BIN_ETA_W, nTracks=BIN_NTRACKS_MC )(*args)


#===============================================================================

if __name__ == '__main__':
  pass

  # print weights_etaW_ntracksMC()
  
  ## MC
  # print eff_mc_etaW()['h1mu_pi'].unstack()
  # print eff_mc_eta_pt()
  # print eff_mc_pt()
  # print eff_mc_eta13()
  # print eff_mc_ntracks()
  # print eff_mc_eta_ntracks_addedsyst()['h1mu_pi'].unstack()
  # print eff_mc_eta_ntracks_raw()['h1mu_pi'].unstack()
  # print eff_mc_ntracks_addedsyst()

  ## Syst
  # print calc_syst_dd_mc_scalar()

  ## Drawer
  # draw_eff_mc_etaW()
  draw_eff_mc_ntracks()
  # draw_eff_mc_h1mu_eta_ntracks()

  ## Finally
  # print lookup_eff_mc()

  # print eff_mc_eta_ntracks_raw()['h3mu_pr'].unstack()

