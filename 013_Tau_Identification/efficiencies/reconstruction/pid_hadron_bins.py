#!/usr/bin/env python

from rec_utils import *


## nominal
BIN_ETA = pd.np.linspace(2.0, 4.5, 20+1)
BIN_PT  = 1, 2, 3, 4, 6, 10, 15, 20, 70, 300
BIN_NTRACKS = 0, 100, 200, 300, 600

## halved
BIN_ETA_HALVED = pd.np.linspace(2, 4.5, 10+1)
BIN_PT_HALVED = 1, 3, 6, 10, 20, 70

## doubled
BIN_ETA_DOUBLED = pd.np.linspace(2.0, 4.5, 30+1)
BIN_PT_DOUBLED  = list(pd.np.geomspace(1, 10, 10+1)) + [15, 20, 30, 70]

## crazy detailed version for fast only
BIN_ETA_DETAILED = pd.np.linspace(2, 4.5, 40+1)
BIN_PT_DETAILED  = list(pd.np.geomspace(1, 50, 30+1))

# Nones
BIN_ETA_NONE = 2, 4.5
BIN_PT_NONE  = 1, 300

## Schemes
SCHEMES_2D = {
  'nominal': {
    'ETA': BIN_ETA,
    'PT' : BIN_PT,
  },
  'wETA': {
    'ETA': BIN_ETA_DOUBLED,
    'PT' : BIN_PT,
  },
  'hETA': {
    'ETA': BIN_ETA_HALVED,
    'PT' : BIN_PT,
  },
  'wPT': {
    'ETA': BIN_ETA,
    'PT' : BIN_PT_DOUBLED,  
  },
  'hETA_wPT': {
    'ETA': BIN_ETA_HALVED,
    'PT' : BIN_PT_DOUBLED,
  },
  'hETA_hPT': {
    'ETA': BIN_ETA_HALVED,
    'PT' : BIN_PT_HALVED,
  },
  'wETA_wPT': {
    'ETA': BIN_ETA_DOUBLED,
    'PT' : BIN_PT_DOUBLED,
  },
  'hetero_lowpt': {
    'ETA': BIN_ETA,
    'PT' : [1, 2, 3, 4, 6],
  },
  'hetero_highpt': {
    'ETA': BIN_ETA_HALVED,
    'PT' : [6, 10, 15, 20, 70, 300],
  },
  'pt_only': {
    'ETA': BIN_ETA_NONE,
    'PT' : BIN_PT,
  },
  'detailed': {
    'ETA': BIN_ETA_DETAILED,
    'PT' : BIN_PT_DETAILED,
  }
}

#===============================================================================

@pickle_dataframe_nondynamic
def multiweights_pt_eta():
  acc = {}
  for tag, scheme in SCHEMES_2D.iteritems():
    acc[tag] = weighter_pt_eta(bin_pt=scheme['PT'], bin_eta=scheme['ETA']).stack()
  return pd.DataFrame(acc)

#===============================================================================

if __name__ == '__main__':
  print multiweights_pt_eta()
