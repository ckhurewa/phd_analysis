#!/usr/bin/env python
from rec_utils import *

## Nominal
BIN_ETA = 2, 2.1, 2.2, 2.4, 2.8, 3.2, 3.6, 4.0, 4.5
BIN_PT  = 5, 8, 20, 30, 40, 50, 300

BIN_PT_LOW = 5, 20

## Null bins
BIN_ETA_NONE = 2, 4.5
BIN_PT_NONE  = 5, 300
BIN_SPD_NONE = 0, 600

## Schemes
SCHEMES_2D = {
  'nominal': {
    'ETA': BIN_ETA,
    'PT' : BIN_PT,
  },
  # 'lowpt': { # NOT COMPAT WITH combining hi+low
  #   'ETA': BIN_ETA,
  #   'PT' : BIN_PT_LOW,
  # },
  'halved': {
    'ETA': [2., 2.2, 2.8, 3.6, 4.0, 4.5],
    'PT' : BIN_PT,
  },
  'doubled': {
    'ETA': [2, 2.05, 2.1, 2.15, 2.2, 2.3, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.25, 4.5],
    'PT' : BIN_PT,
  },
}

#===============================================================================
# WEIGHTS
#===============================================================================

@pickle_dataframe_nondynamic
def multiweights_pt_eta():
  acc = {}
  for tag, scheme in SCHEMES_2D.iteritems():
    acc[tag] = weighter_pt_eta(bin_pt=scheme['PT'], bin_eta=scheme['ETA']).stack()
  return pd.DataFrame(acc)

#===============================================================================

if __name__ == '__main__':
  print multiweights_pt_eta()
