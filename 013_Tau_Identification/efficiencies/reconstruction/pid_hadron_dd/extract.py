#!/usr/bin/env python

from PyrootCK import *

#===============================================================================

## Preselection for purer hadron sample
filt = utils.join(
  ## event
  'nSPDHits<=600',
  {'runNumber < 111802', '111890 < runNumber'},
  {'runNumber < 126124', '126160 < runNumber'},
  {'runNumber < 129530', '129539 < runNumber'},

  ## mothers
  'D0_ENDVERTEX_CHI2/D0_ENDVERTEX_NDOF   < 4',
  'Dst_ENDVERTEX_CHI2/Dst_ENDVERTEX_NDOF < 4',
  # 'D0_M > 1840', # To have sidebanc
  # 'D0_M < 1890',

  ## acceptance
  'Pi_ETA  > 2.',
  'Pi_ETA  < 4.5',
  'Pi2_ETA > 2.',
  'Pi2_ETA < 4.5',
  'K_ETA   > 2.',
  'K_ETA   < 4.5',

  ## tracks
  'Pi_TRPCHI2  > 0.01',
  'Pi2_TRPCHI2 > 0.01',
  'K_TRPCHI2   > 0.01',
  # 'Pi_TRGHP    < 0.05', # NO
  'Pi2_TRGHP   < 0.05',
  'K_TRGHP     < 0.05',
  'Pi_TRTYPE  == 3',
  'Pi2_TRTYPE == 3',
  'K_TRTYPE   == 3',

  ## tag
  '!K_ISMUONLOOSE',
  'K_PP_InAccHcal',
  'K_PP_CaloHcalE/K_P > 0.10',
  'K_PROBNNghost < 0.1',
  #
  '!Pi2_ISMUONLOOSE',
  'Pi2_PP_InAccHcal',
  'Pi2_PP_CaloHcalE/Pi2_P > 0.10',
  'Pi2_PROBNNghost < 0.1',

  ## others on probe
  # 'Pi_PROBNNghost < 0.05', # NO
  'Pi_PT > 1e3',

  ## test bin
  # 'Pi_PT>10e3',
  # 'Pi_PT<20e3',
  )

cut_passed = utils.join(
  '!Pi_ISMUONLOOSE',
  'Pi_PP_InAccMuon',
  'Pi_PP_InAccHcal',
  'Pi_PP_CaloHcalE/Pi_P > 0.05',
)

#===============================================================================

def main():
  """
  Process over the PID.MDST ntuples, and sliced for the releveant part
  to a smaller ntuple.
  """
  aliases = {
    'passed'      : cut_passed,
    'PT'          : 'Pi_PT/1e3',
    'ETA'         : 'Pi_ETA',
    'deltamass'   : 'Dst_M-D0_M',
    'TRGHP'       : 'Pi_TRGHP',
    'PROBNNghost' : 'Pi_PROBNNghost',
  }  
  params = ['nTracks', 'Dst_M', 'D0_M'] + aliases.keys()

  ## Execute
  ROOT.TFile.slice_tree(
    # ## Test file on EOS
    # src = 'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/5437_57.root',
    # dest = 'temp.root',
    src       = 5437,
    dest      = 'extracted.root',
    tname     = 'PID_hadron/hadron',
    aliases   = aliases,
    params    = params,
    selection = filt,
    nprocs    = 8,
  )

#===============================================================================

if __name__ == '__main__':
  main()
