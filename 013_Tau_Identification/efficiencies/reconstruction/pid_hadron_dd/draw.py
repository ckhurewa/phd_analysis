#!/usr/bin/env python

"""

Handle the drawing of 2D fitting of D0, Dst

"""

from PyrootCK import *
sys.path.append('..')
from rec_utils import *
from pid_hadron_bins import SCHEMES_2D
ROOT.RooFit # trigger patch
ROOT.gROOT.batch = True

#===============================================================================

def draw_contribution_single(w, poiname):
  ## Get frame & draw all
  frame = w.var(poiname).frame()
  #
  data = w.data('dataset_with_weights')
  data.plotOn(frame, name='data', markerSize=0.2)
  #
  nmz   = (1.0, ROOT.RooAbsReal.RelativeExpected)
  model = w.pdf('model')
  model.plotOn(frame, name='model'                       , lineColor=ROOT.kGreen, normalization=nmz)
  model.plotOn(frame, name='sig' , components='sigModel' , lineColor=ROOT.kBlack, normalization=nmz)
  model.plotOn(frame, name='comb', components='combModel', lineColor=ROOT.kRed  , normalization=nmz)
  model.plotOn(frame, name='pi'  , components='piModel'  , lineColor=ROOT.kBlue , normalization=nmz)
  model.plotOn(frame, name='d0'  , components='d0Model'  , lineColor=28         , normalization=nmz)

  ## Tune
  frame.drawAfter('model', 'data')
  frame.Draw()

  ## Legends
  leg = ROOT.TLegend()
  leg.header = 'LHCb preliminary'
  leg.AddEntry(data                         , 'Observed'          , 'PE')
  leg.AddEntry(ROOT.gPad.FindObject('model'), 'Model'             , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('sig')  , 'Signal'            , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('comb') , 'BKG: Combinatorics', 'l')
  leg.AddEntry(ROOT.gPad.FindObject('pi')   , 'BKG: Slow pion'    , 'l')
  leg.AddEntry(ROOT.gPad.FindObject('d0')   , 'BKG: Fake D0'      , 'l')
  leg.fillColorAlpha = 1, 0.
  leg.x1NDC = 0.62
  leg.x2NDC = 0.95
  leg.y1NDC = 0.62
  leg.y2NDC = 0.95
  leg.Draw()

  ## Finally
  ymax = frame.maximum
  ROOT.gPad.ls()
  ROOT.gPad.logy = False
  ROOT.gPad.SaveAs('draw/contrib0_%s.pdf'%poiname)

  ## Zoom 
  frame.maximum = 0.1 * ymax
  ROOT.gPad.SaveAs('draw/contrib0z_%s.pdf'%poiname)

  ## Log scale
  frame.maximum = 10. * ymax
  frame.minimum = 5e2
  ROOT.gPad.logy = True
  ROOT.gPad.SaveAs('draw/contrib_%s.pdf'%poiname)


def draw_contribution():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Load
  fin  = ROOT.TFile('fit2.root')
  w    = fin['hadron']
  # w.Print()
  # w.PrintVars()

  ## Pre-draw prep
  w.var('D0_M').unit       = 'MeV/c^{2}'
  w.var('D0_M').title      = 'mass(D0)'
  w.var('deltamass').unit  = 'MeV/c^{2}'
  w.var('deltamass').title = '#Delta^{}m_{D}'
  # w.var('deltamass').title = 'mass(D*) - mass(D0)'

  ## Do draw
  draw_contribution_single(w, 'D0_M')
  draw_contribution_single(w, 'deltamass')

#===============================================================================

def draw_sweights_POI():
  """
  Stacked TProfile of sWeights as a function of 2 POIs
  """
  ## Harsher contrast
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.markerSize = 0.
  # ROOT.gStyle.SetLineWidth(3)
  # ROOT.gStyle.SetFrameLineWidth(3)
  # ROOT.gStyle.SetHistLineWidth(3)

  ## Start
  tree    = import_tree('hadron', 'weighed.root')
  tags    = 'nsig_sw', 'ncomb_sw', 'npi_sw', 'nD0_sw'
  legends = 'Signal', 'BKG: Combinatorics', 'BKG: Slow pion', 'BKG: Fake D0'

  QHist.reset()
  QHist.trees   = tree
  QHist.ymin    = -1.2
  QHist.ymax    = 3.5
  QHist.options = 'prof'
  QHist.legends = legends
  QHist.st_line = False

  h = QHist()
  h.name    = 'sweights_POI1'
  h.params  = [x+':D0_M' for x in tags]
  h.xmin    = 1823
  h.xmax    = 1920
  h.xbin    = 97
  h.xlabel  = 'mass(D0) [MeV/c^{2}]'
  h.draw()

  h = QHist()
  h.name    = 'sweights_POI2'
  h.params  = [x+':deltamass' for x in tags]
  h.xmin    = 141
  h.xmax    = 153
  h.xbin    = 120
  h.xlabel  = '#Delta^{}m_{D} [MeV/c^{2}]'
  # h.xlabel  = 'mass(D*) - mass(D0) [MeV/c^{2}]'
  h.draw()

#===============================================================================

@gpad_save
def draw_2D_observed():
  """
  The 2D plot of POI (deltamass against D0)
  """
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors
  tree = import_tree('hadron', 'weighed.root')
  tree.Draw('deltamass: D0_M >> h(100, 1832, 1920, 100, 141, 153)', '', 'colz')
  h = ROOT.gROOT.FindObject('h')
  h.xaxis.title = 'mass(D0) [MeV/c^{2}]'
  h.yaxis.title = '#Delta^{}m_{D} [MeV/c^{2}]'
  # h.yaxis.title = 'mass(D*) - mass(D0) [MeV/c^{2}]'
  ROOT.gPad.logz = True
  ROOT.gPad.topMargin = 0.02
  ROOT.gPad.rightMargin = 0.12


#===============================================================================

@gpad_save
def draw_probe_occupancy():
  ## Start
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors

  ## Prep
  xarr = pd.np.geomspace(0.99, 70, 100)
  yarr = pd.np.geomspace(2, 4.5, 100)
  c = ROOT.TCanvas()
  h = ROOT.TH2F('htemp', 'htemp', xarr, yarr)
  c.ownership = False
  h.ownership = False

  ## Draw
  tree = import_tree('hadron', 'weighed.root')
  tree.Draw('ETA: PT >> htemp', 'nsig_sw', 'goff')
  h.Draw('colz')
  h.xaxis.title = 'p_{T}(h) [GeV]'
  h.yaxis.title = '#eta(h)'
  h.xaxis.moreLogLabels = True
  h.xaxis.labelOffset   = -0.01
  h.xaxis.titleOffset   = 0.95
  ROOT.gPad.logx = True
  ROOT.gPad.logz = True
  ROOT.gPad.rightMargin  = 0.12
  ROOT.gPad.bottomMargin = 0.16
  ROOT.gPad.topMargin    = 0.03

  ## Draw nominal bins grid
  drawers.occupancy_grid(h, **SCHEMES_2D['nominal'])


#===============================================================================

if __name__ == '__main__':

  ## Drawer
  draw_contribution()
  # draw_sweights_POI()
  # draw_2D_observed()
  # draw_probe_occupancy()
