#!/usr/bin/env python

"""

More advance version from PIDCalib model. 2D fitting in D0, Dst-D0

"""

from PyrootCK import *
sys.path.append('..')
from rec_utils import *

## Fit output file
FIT_OUTPUT = 'fit2.root'

#===============================================================================

def load_data(w):
  ## POI
  w.factory('D0_M      [1823, 1920]')
  w.factory('deltamass [ 141,  153]')
  # spectators
  # w.factory('Dst_M      [1940, 2080]') # 2012 # too large
  w.factory('PT         [1   , 300 ]')
  w.factory('ETA        [2.0 , 4.5 ]')
  w.factory('nTracks    [0   , 600 ]')
  # w.factory('TRGHP      [0, 1]')
  # w.factory('PROBNNghost[0, 1]')
  w.factory('passed     [0, 1]')  
  w.defineSet('params', 'D0_M,deltamass,PT,ETA,nTracks,passed')
  # w.defineSet('params', 'D0_M,deltamass,PT,ETA,nTracks,TRGHP,PROBNNghost,passed')
  w.defineSet('POIs'  , 'D0_M,deltamass')

  ## Import data
  tree = import_tree('hadron', 'tuple.root')
  ds   = ROOT.RooDataSet('dataset', 'dataset', tree, w.set('params'))
  dh   = ROOT.RooDataHist('datahist', 'datahist', w.set('POIs'), ds)
  return ds, dh


#===============================================================================

def prepare_model(w, nentries):
  """
  Make all the model into given Workspace.
  """
  w.factory('nsig  [%.2f, 0, %i]'%(0.65*nentries, nentries ))
  w.factory('ncomb [%.2f, 0, %i]'%(0.15*nentries, nentries ))
  w.factory('npi   [%.2f, 0, %i]'%(0.19*nentries, nentries ))
  w.factory('nD0   [%.2f, 0, %i]'%(0.01*nentries, nentries ))

  ## D0: d0MassSigModel
  d0MassPDG = 1864.83
  w.factory('dmass_sig_mu       [%f, %f, %f]'%(d0MassPDG, d0MassPDG-5., d0MassPDG+5.))
  w.factory('dmass_sig_sigma0   [8.0, 7.0, 9.0]')
  w.factory('dmass_sig_s1oS0    [2.0, 1.5, 2.5]')
  w.factory('dmass_sig_coreFrac [0.8,   0, 1.0]')
  w.factory('expr    : dmass_sig_sigma1 ("@0 * @1", dmass_sig_s1oS0, dmass_sig_sigma0)')
  w.factory('Gaussian: dmass_sig_g0     (D0_M, dmass_sig_mu, dmass_sig_sigma0)')
  w.factory('Gaussian: dmass_sig_g1     (D0_M, dmass_sig_mu, dmass_sig_sigma1)')
  w.factory('SUM     : d0MassSigModel   (dmass_sig_coreFrac*dmass_sig_g0, dmass_sig_g1)')

  ## D0: d0MassBkgModel
  w.factory('grad [-0.4, -5, -0.05]')
  w.factory('Chebychev: d0MassBkgModel(D0_M, grad)')

  ## D0: d0MassBkgForMultModel
  w.factory('dmass_bkg_exp_decay [-0.0, -20.0, 20.0]')
  w.factory('Exponential: d0MassBkgForMultModel(D0_M, dmass_bkg_exp_decay)')

  ## DeltaMass: delmSigModel
  delmPDG = 145.421
  w.factory('delm_sig_mu       [ %f, %f, %f ]'%(delmPDG, delmPDG-1., delmPDG+1.))
  w.factory('delm_sig_sigma0   [ 0.55, 0.45, 0.65 ]')
  w.factory('delm_sig_s1oS0    [ 2.0 , 1.5 , 2.5  ]')
  w.factory('delm_sig_coreFrac [ 0.55, 0.0 , 1.0  ]')
  w.factory('expr    : delm_sig_sigma1 ("@0 * @1", delm_sig_s1oS0, delm_sig_sigma0)')
  w.factory('Gaussian: delm_sig_gauss0 (deltamass, delm_sig_mu, delm_sig_sigma0)')
  w.factory('Gaussian: delm_sig_gauss1 (deltamass, delm_sig_mu, delm_sig_sigma1)')
  w.factory('SUM     : delmSigModel    (delm_sig_coreFrac*delm_sig_gauss0, delm_sig_gauss1)')

  ## DeltaMass: delmBkgModel
  w.factory('delm_bkg_thresh [4.0, -10.0, 10.0]')
  w.factory('EXPR: delmBkgModel("sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", deltamass, delm_bkg_thresh)')

  ## DeltaMass: delmBkgModelForWS
  w.factory('delm_bkg_threshws [4.0, -10.0, 10.0]')
  w.factory('EXPR: delmBkgModelForWS("sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", deltamass, delm_bkg_threshws)')

  ## 2D PDF
  # - signal
  # - combinatoric background (bkg in D0 mass, bkg in delta mass)
  # - random slow pion bkg (signal in D0 mass, bkg in delta mass)
  # - fake D0 bkg (bkg in D0 mass, signal in delta mass)
  w.factory('PROD: sigModel (d0MassSigModel       , delmSigModel)')
  w.factory('PROD: combModel(d0MassBkgModel       , delmBkgModel)')
  w.factory('PROD: piModel  (d0MassSigModel       , delmBkgModelForWS)')
  w.factory('PROD: d0Model  (d0MassBkgForMultModel, delmSigModel)')
  w.factory('SUM : model    (nsig*sigModel, ncomb*combModel, npi*piModel, nD0*d0Model)')


  # ## all params & snapshot
  # w.defineSet('fit_params', 'nsig0,nsig,nbkg0,nbkg,mean,sigma,width,slope')
  # w.saveSnapshot('init', w.set('fit_params'))

#===============================================================================

def execute_fit(w, ds, dh):
  ## Fit to model
  model = w.pdf('model')
  res   = model.fitTo(dh, sumW2Error=False, save=True, printLevel=1)
  w.Import(res)

  ## sWeight
  splot = ROOT.RooStats.SPlot("sPlot", "An sPlot", ds, model, model.coefList())
  rds_withWeights = splot.GetSDataSet()
  rds_withWeights.name = 'dataset_with_weights'
  w.Import(rds_withWeights)

#===============================================================================

def main_fit():
  ## conf <-- SOMEWHAT TOO SLOW
  # ROOT.RooAbsData.setDefaultStorageType(ROOT.RooAbsData.Tree)

  # Create new Workspace
  w = ROOT.RooWorkspace('hadron')

  ## Load data
  ds, dh = load_data(w)
  n = ds.numEntries()

  ## Prepare model
  prepare_model(w, n)  
  w.Print()

  ## Start fit
  execute_fit(w, ds, dh)

  ## Finally, save workspace
  fout = ROOT.TFile(FIT_OUTPUT, 'recreate')
  fout.cd()
  w.Write()
  fout.Close()

#===============================================================================

def main_export_tree():
  """
  Load the new dataset and convert to TTree instead.
  https://root-forum.cern.ch/t/roodataset-tree-function-now-working/19501/2
  """
  import ROOT
  fin = ROOT.TFile(FIT_OUTPUT)
  w  = fin.Get('hadron')
  ds = w.data('dataset_with_weights')
  ds.name = 'hadron'

  ## It'll be nice to have this dynamically
  # fields1 = 'D0_M', 'Dst_M', 'deltamass', 'PT', 'ETA', 'nTracks', 'TRGHP', 'PROBNNghost', 'passed'
  fields1 = 'D0_M', 'Dst_M', 'deltamass', 'PT', 'ETA', 'nTracks', 'passed'
  fields2 = 'nsig_sw', 'L_nsig', 'ncomb_sw', 'L_ncomb', 'npi_sw', 'L_npi', 'nD0_sw', 'L_nD0'
  fields  = fields1 + fields2

  dataset_to_TFile(ds, fields)

  #---

  ## Byte count too large problem
  # import ROOT
  # fin = ROOT.TFile(FIT_OUTPUT)
  # w   = fin.Get('hadron')
  # ds  = w.data('dataset_with_weights')
  # ds.name = 'hadron'

  # ROOT.RooAbsData.setDefaultStorageType(ROOT.RooAbsData.Tree)
  # ds2 = ROOT.RooDataSet('ds2', 'ds2', ds, ds.get())
  # fout = ROOT.TFile('weighed.root', 'recreate')
  # tree = ds2.tree()
  # tree.Write()
  # fout.Close()

#===============================================================================

if __name__ == '__main__':
  pass

  # main_fit()
  main_export_tree()
