#!/usr/bin/env python
from PyrootCK import *
sys.path.append('..')
from rec_utils import *

## Fit output file
FIT_OUTPUT  = 'fit.root'

#===============================================================================
# ROOFIT BACKGROUND REMOVAL
#===============================================================================

def perform_fit():
  ## Init workspace
  w = ROOT.RooWorkspace('hadron')
  # w.factory('D0_M [1800, 1930]') # 1864
  w.factory('Dst_M  [1940, 2080]') # 2012
  w.factory('PT     [1   , 70  ]')
  w.factory('ETA    [2.0 , 4.5 ]')
  w.factory('nTracks[0   , 600 ]')
  w.factory('passed [0, 1]')
  w.factory('state  [before, after]')
  w.defineSet('params', 'Dst_M,PT,ETA,passed,nTracks')
  w.defineSet('POI'   , 'Dst_M')

  ## define model: BKG + SIGNAL
  w.factory('nsig0 [0   , 1e7 ]')
  w.factory('nsig  [0   , 1e7 ]')
  w.factory('nbkg0 [1, 0, 1e6 ]')
  w.factory('nbkg  [1, 0, 1e6 ]')
  # w.factory('mean  [1850, 1880]')
  w.factory('mean  [1940, 2080]')
  # w.factory('alpha [0   , 10  ]') # CB only
  # w.factory('scale [0   , 10  ]') # CB only
  w.factory('sigma [    8,    0, 20 ]')
  w.factory('width [    2,    0, 10 ]') # Voigtian only
  w.factory('slope [-0.02, -0.1, 0  ]')

  # w.factory('Polynomial:pbkg (D0_M, slope)')
  w.factory('Exponential:pbkg (Dst_M, slope)')
  w.factory('Voigtian:   psig (Dst_M, mean, width, sigma )')
  # w.factory('CBShape:    psig (Dst_M, mean, sigma, alpha, scale)')
  #
  w.factory('SUM:esig0 (nsig0 * psig)')
  w.factory('SUM:ebkg0 (nbkg0 * pbkg)')
  w.factory('SUM:esig  (nsig  * psig)')
  w.factory('SUM:ebkg  (nbkg  * pbkg)')
  w.factory('SUM:model0(nsig0 * psig, nbkg0 * pbkg)')
  w.factory('SUM:model (nsig  * psig, nbkg  * pbkg)')
  w.factory('SIMUL:PDF (state, before=model0, after=model)')

  ## all params & snapshot
  w.defineSet('fit_params', 'nsig0,nsig,nbkg0,nbkg,mean,sigma,width,slope')
  w.saveSnapshot('init', w.set('fit_params'))

  ## Import data
  tree = import_tree('hadron', 'tuple.root')
  ds0  = ROOT.RooDataSet('ds0', 'ds0', tree, w.set('params'))

  ## Save model & prepare output
  fout = ROOT.TFile(FIT_OUTPUT, 'recreate')

  ## Loop over different schemes & bin
  # scheme3 = gen_selections(ETA=BIN_ETA, PT=BIN_PT, nTracks=BIN_NTRACKS) # 3D nominal
  # scheme2 = gen_selections(ETA=SCHEMES_2D['nominal']['ETA'], PT=SCHEMES_2D['nominal']['PT'])
  gen = gen_selections(**SCHEMES_2D[FIT_SPEC_2D])
  # gen = gen_selections(ETA=[2.5, 2.75], PT=[2,3]) # debug
  for key, selection in gen:
    ds_before = ds0.reduce(selection)
    ds_after  = ds_before.reduce('passed')
    data = ROOT.RooDataSet('data', 'data', w.set('params'),
      ROOT.RooFit.Index(w.cat('state')),
      ROOT.RooFit.Import({'before':ds_before, 'after':ds_after}),
    ) # cannot use kwargs here as the order of arg matters

    ## Import to workspace as RooDataHist, for later plot
    dh_before = ROOT.RooDataHist('dh_before_'+key, 'before', w.set('POI'), ds_before)
    dh_after  = ROOT.RooDataHist('dh_after_'+key , 'after' , w.set('POI'), ds_after)
    w.Import(dh_before)
    w.Import(dh_after)

    ## Execute fitting, if it's adequate
    n0 = 1. * ds_before.numEntries()
    n1 = 1. * ds_after.numEntries()
    logger.info('Stats: %s: %i --> %i'%(key, n0, n1))
    if n1 > 400: # 5%
      # massaging range of nsig, nbkg with given data
      # note that the bound is NOT part of the snapshot
      w.var('nsig0').min = 0   # push min to 0 first, to avoid newmax < oldmin
      w.var('nsig').min  = 0
      w.var('nsig0').max = n0  # start with max first, otherwise min error
      w.var('nsig').max  = n1
      w.var('nsig0').min = n0 * 0.7 # expect min purity of 70%
      w.var('nsig').min  = n1 * 0.7
      w.var('nsig0').val = n0 * 0.9 # starting point high
      w.var('nsig').val  = n1 * 0.9
      w.var('nbkg0').max = n0 * 0.3 # impurity (100-70)%
      w.var('nbkg').max  = n1 * 0.3

      ## start fitting
      model = w.pdf('PDF')
      res = model.fitTo(data, save=True)
      res.Write('fit_result_'+key)
      # finally, reset paramss
      w.loadSnapshot('init')

  ## finally, save fit results, DataHist
  fout.Close()
  w.writeToFile(FIT_OUTPUT, False) # recreate

#===============================================================================

def clean_intv(s):
  ## Make the format aligned with pandas's
  v1, v2 = re.findall(r'\[(\S+),(\S+)\]', s)[0]
  v1 = try_int_else_float(v1)
  v2 = try_int_else_float(v2)
  return '(%s, %s]'%(v1, v2)


def _eff_from_fit(genscheme, regex):
  """
  Load the nD eff map from fitting
  """
  ROOT.RooFit # trigger patch
  fin = ROOT.TFile(FIT_OUTPUT)

  ## Collect
  keys = []
  vals = []
  for tag, _ in genscheme:
    ## Report fit result, collect to pd.Series
    key = re.findall(regex, tag)[0]
    key = [clean_intv(s) for s in key]
    res = fin.Get('fit_result_'+tag)
    val = pd.np.NaN
    if res and res.status()==0:
      # print tag, res.status()
      # assert res.status()==0, "Fit not successful, check me."
      se = res.series()
      val = se['nsig'] / se['nsig0']
    keys.append(key)
    vals.append(val)

  ## Collect ePID
  fin.Close()
  index = pd.MultiIndex.from_tuples(keys)
  return pd.Series(vals, index=index)


@memorized
def eff_3D_from_fit():
  gen = gen_selections(ETA=BIN_ETA, PT=BIN_PT, nTracks=BIN_NTRACKS)
  reg = r'ETA(\S+)_PT(\S+)_nTracks(\S+)'
  return _eff_from_fit(gen, reg)


@memorized
def eff_2D_from_fit():
  gen = gen_selections(**SCHEMES_2D[FIT_SPEC_2D])
  reg = r'ETA(\S+)_PT(\S+)'
  eff = _eff_from_fit(gen, reg)
  eff.index.names = 'ETA', 'PT'
  return sort_intv_pt(eff.unstack().T).T

#-------------------------------------------------------------------------------

def read_fit():
  """
  Miscellaneous operation
  """
  ## local import
  import pid_hadron_dd
  scheme = pid_hadron_dd.SCHEMES_2D['nominal']
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Init
  ROOT.RooFit # trigger patch
  fout = ROOT.TFile(FIT_OUTPUT)
  w = fout['hadron']

  ## reset the bound
  for vname in ['nsig0', 'nsig', 'nbkg0', 'nbkg']:
    w.var(vname).min = 0
    w.var(vname).max = 1e7

  ## Start looping
  gen = gen_selections(ETA=[3.25, 3.375], PT=[2, 3]) # just one bin, for one nice plot
  # gen = gen_selections(**scheme)
  for tag, _ in gen:

    ## Load fit result
    res = fout.Get('fit_result_'+tag)
    if not res:
      continue
    print res.series()

    ## Plot data only, for debugging
    frame = w.set('POI').first().frame()
    print frame
    dh = w.data('dh_after_'+tag)
    dh.plotOn(frame)
    # dh.statOn(frame) #, ROOT.RooFit.Layout(0.55,0.99,0.8))

    w.load_from_RooFitResult(res)
    w.PrintVars()

    ## BKG & SIG
    w.pdf('ebkg').plotOn(frame,
      normalization = (1.0, ROOT.RooAbsReal.RelativeExpected),
      lineColor     = ROOT.kRed,
    )
    w.pdf('esig').plotOn(frame,
      normalization = (1.0, ROOT.RooAbsReal.RelativeExpected),
      lineColor     = ROOT.kBlue,
      lineStyle     = 2,
    )
    w.pdf('model').plotOn(frame,
      normalization = (1.0, ROOT.RooAbsReal.RelativeExpected),
      lineColor     = ROOT.kBlack,
      lineStyle     = 2,
    )

    ## params
    w.pdf('model').paramOn(frame, layout=(0.15,0.50))

    ## Finally
    frame.Draw()
    ROOT.gPad.ls()


    ## fine tunining
    hist = ROOT.gPad.primitives[0]
    hist.xaxis.title = 'mass(D*) [MeV/c^{2}]'
    box = ROOT.gPad.FindObject('model_paramBox')
    box.lineColorAlpha = 1, 0.
    ROOT.gPad.topMargin = 0.02
    ROOT.gPad.rightMargin = 0.02
    ROOT.gPad.RedrawAxis()

    ## Finally
    ROOT.gPad.SaveAs('temp/%s.pdf'%tag)

#===============================================================================

if __name__ == '__main__':
  read_fit()
