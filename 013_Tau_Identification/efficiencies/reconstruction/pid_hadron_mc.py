#!/usr/bin/env python

"""

Strategy
- Use data-driven technique to get eff from minibias data.
  However, the statistic is limited to low-PT
- Use the shape from MC to extrapolate into higher-PT regime,
  strongly needed in h1mu case.

"""

from rec_utils import *
from pid_hadron_bins import *


#===============================================================================
# FROM SIMULATION
#===============================================================================

@pickle_dataframe_nondynamic
def eff_pt_eta(name):
  scheme = SCHEMES_2D[name]
  nume, deno = calc_epid_mc_eta_pt_spd_raw('hadrons', bin_eta=scheme['ETA'], bin_pt=scheme['PT'])
  # print nume['hadrons'].dropna().unstack().T
  # print deno['hadrons'].dropna().unstack().T
  return divide_clopper_pearson(nume.stack(), deno.stack()).unstack()

#-------------------------------------------------------------------------------

@memorized
def eff_pt_flatETA():
  """
  Weighted in ETA, leaving PT intact. 
  For visualization & attempt for DD only.
  """
  raise NotImplementedError
  ## Second approach: flat-ETA
  e = completely_correlate(eff_mc_pt_eta().stack()).unstack().T
  return e.apply(lambda se: se.sum()/len(se))

@memorized
def eff_pt_allETA():
  """
  Weighed in ETA of all channel combined, assuming that they are all similar
  enough in order to get a connected dist in PT
  """
  raise NotImplementedError
  e = eff_mc_pt_eta().T
  w = weights_pt_eta()['hadrons'].unstack().T
  eff = (w*e).sum()/w.sum().replace(0., 1.)
  del eff['(70, 100]']
  del eff['(100, 300]']
  return eff


@memorized
def eff_eta_flatPT():
  """
  Like above, flat in PT.
  Note: Kill high-PT ones first. Keep PT<=20
  """
  raise NotImplementedError
  e = completely_correlate(eff_mc_pt_eta().stack()).unstack()
  e = e.drop('(20, 40]')
  e = e.drop('(40, 70]')
  e = e.drop('(70, 100]')
  e = e.drop('(100, 300]')
  return e.apply(lambda se: se.sum()/len(se))


@memorized
def eff_eta_weighed_H1MU():
  """
  Weighed to the PT distribution of tauh1 from h1mu channel.
  """
  spec = 'nominal'
  w = multiweights_pt_eta()[spec].unstack()['h1mu_pi'].dropna().unstack()
  e = completely_correlate(eff_pt_eta(spec)['hadrons'].dropna()).unstack()
  return (w*e).sum()/w.sum().replace(0, 1.)

@memorized
def eff_eta_weighed_H3MU():
  spec = 'nominal'
  w = multiweights_pt_eta()[spec].unstack()['h3mu_pr'].dropna().unstack()
  e = completely_correlate(eff_pt_eta(spec)['hadrons'].dropna()).unstack()
  return (w*e).sum()/w.sum().replace(0, 1.)

@memorized
def eff_pt_weighed_H3MU():
  # spec = 'nominal'
  spec = 'hETA_wPT'
  w = multiweights_pt_eta()[spec].unstack()['h3mu_pr'].dropna().unstack().T
  e = completely_correlate(eff_pt_eta(spec)['hadrons'].dropna()).unstack().T
  return sort_intv_pt((w*e).sum()/w.sum().replace(0, 1.))


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_mc_pt_eta():
  """
  xaxis in eta.
  Collapse some PT bin together for clarity, naive average
  """
  eff  = pd.DataFrame()
  eff0 = eff_mc_pt_eta().T
  eff['(1, 3] GeV']   = (eff0['(1, 2]']   + eff0['(2, 3]'])/2
  eff['(3, 6] GeV']   = (eff0['(3, 4]']   + eff0['(4, 6]'])/2
  eff['(6, 20] GeV']  = (eff0['(6, 10]']  + eff0['(10, 20]'])/2
  eff['(20, 70] GeV'] = (eff0['(20, 40]'] + eff0['(40, 70]'])/2
  eff.columns.name = 'PT'
  #
  gr,leg = drawer_graphs_eff_pt(eff)
  gr.xaxis.title = '#eta(h)'
  gr.yaxis.title = '#varepsilon_{PID, h}'
  gr.maximum     = 1.1
  gr.minimum     = 0.0
  leg.header     = 'LHCb-Simulation'
  leg.x1NDC      = 0.35
  leg.x2NDC      = 0.75
  leg.y1NDC      = 0.20
  leg.y2NDC      = 0.50
  ROOT.gPad.logx = False


@gpad_save
def draw_eff2D_mc_pt_eta():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird)
  ROOT.gStyle.SetPaintTextFormat(".4f")

  eff = eff_mc_pt_eta()
  h   = ROOT.TH2F.from_uframe(eff)
  h.Draw('texte colz')
  h.markerSize          = 1.0 # text size
  h.xaxis.title         = '#eta(h)'
  h.yaxis.title         = 'p_{T}(h) [GeV]'
  h.yaxis.titleOffset   = 1.1
  h.yaxis.labelOffset   = 0.002
  h.yaxis.rangeUser     = 0, 8
  ROOT.gPad.rightMargin = 0.12
  ROOT.gPad.leftMargin  = 0.16
  ROOT.gPad.Update()


#-------------------------------------------------------------------------------

def draw_eff_mc_raw():
  from root_numpy import array2tree
  ## Back to tree, visualize
  df_pi = pd.read_pickle('pid_hadron/muh.df')
  t = array2tree(df_pi.to_records())

  ## by PT
  h = QHist()
  h.trees   = t 
  h.params  = 'pass: PT/1000'
  h.xlog    = True
  h.xbin    = 10
  h.xmin    = 1
  h.xmax    = 70
  h.ymax    = 1
  h.ymin    = 0.5
  h.options = 'prof'
  h.draw()

  ## by ETA
  h = QHist()
  h.trees   = t 
  h.params  = 'pass: ETA'
  h.xbin    = 10
  h.xmin    = 2.25
  h.xmax    = 3.75
  h.ymax    = 1
  h.ymin    = 0.8
  h.options = 'prof'
  h.draw()
  
  ## by ETA
  h = QHist()
  h.trees   = t 
  h.params  = 'pass: nSPDhits'
  h.xbin    = 10
  h.xmin    = 0
  h.xmax    = 600
  h.ymax    = 1
  h.ymin    = 0.8
  h.options = 'prof'
  h.draw()

#===============================================================================

def scalar_results():
  ## Fetch weights & efficiencies
  w = weights_pt_eta()
  e = completely_correlate(eff_mc_pt_eta().stack())
  ## Finally
  eff = w.apply(lambda se: (se*e).sum()/se.sum())
  eff = eff.filter(regex='.*pr[123]|.*pi', axis=0)
  eff.loc['h3mu_tauh3'] = eff.loc['h3mu_pr1'] * eff.loc['h3mu_pr2'] * eff.loc['h3mu_pr3']
  eff.loc['eh3_tauh3']  = eff.loc['eh3_pr1']  * eff.loc['eh3_pr2']  * eff.loc['eh3_pr3']
  return eff


#===============================================================================

def lookup(*args):
  @memorized # For the correct correlation
  def eff():
    return completely_correlate(eff_mc_pt_eta().stack())
  return lookup_eff_generic(__file__, eff(), _PT=BIN_PT, _ETA=BIN_ETA )(*args)
 
#===============================================================================

if __name__ == '__main__':
  pass 

  ## Direct efficiencies
  # print eff_pt_eta('nominal')
  # print eff_pt_eta('hETA_wPT')

  ## Derived eff
  # print eff_eta_weighed_H1MU()
  # print eff_eta_weighed_H3MU()
  print eff_pt_weighed_H3MU()

  ## Drawer
  # draw_eff_mc_pt_eta()
  # draw_eff2D_mc_pt_eta()

  ## Finally
  # print scalar_results()
  # print lookup()
