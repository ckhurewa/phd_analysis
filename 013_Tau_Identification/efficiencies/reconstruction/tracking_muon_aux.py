#!/usr/bin/env python

"""

Handle the aux of muon tracking efficiencies; Drawing & exporting syst

"""

from rec_utils import *
from tracking_bins import *
import tracking_muon_dd
import tracking_muon_mc

#===============================================================================
# TRPCHI2 SYST
#===============================================================================

# @memorized
# def syst_dd_mc_TRPCHI2():
#   """
#   Calculate the data-MC disagreement in bin of ETA (due to TRPCHI2 cut) to be
#   added into etrack of e/h. Syst in all bins are completely correlated
#   - Require etrack from data & MC
#   - Pickle to dataframe as it's used in all channels.
#   """
#   dd = tracking_muon_dd.efficiencies()
#   mc = tracking_muon_mc.eff_etaW()
#   return completely_correlate((abs(dd-mc)/mc).apply(lambda u: ufloat(1., u.n)))

@memorized
def correction_dd_mc_TRPCHI2():
  """
  Experimental down-scale factor due to TRPCHI2
  """
  dd = tracking_muon_dd.efficiencies()
  mc = tracking_muon_mc.eff_etaW()
  return completely_correlate(dd/mc)


def lookup_fix_TRPCHI2(*args):
  eff = correction_dd_mc_TRPCHI2()
  return lookup_eff_generic(__file__, eff, _ETA=BIN_ETA_W)(*args)


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_compare_dd_mc_etaW():
  df = pd.DataFrame({
    # 'DD': eff_dd_sfarry_eta13_corrected(),
    # 'MC': eff_mc_eta13(),
    'Data'      : eff_dd_sfarry_etaW(),
    'Simulation': eff_mc_etaW(),
    # 'Simulation': eff_mc_eta13_raw().loc['litediv', 'muons']
  })
  df.columns.name = '#varepsilon_{track,#mu}'
  df.index.name   = '#eta(#mu)'
  gr, leg     = drawer_graphs_dd_mc(df)
  gr.minimum  = 0.7


@gpad_save
def draw_eff_compare_eta_zmumu():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  df = pd.DataFrame({
    'Data (Zmumu T&P corrected)': eff_dd_sfarry_etaW(),
    'MC (Ztautau)              ': eff_mc_etaW(),
    'MC (Ztautau, reweighted PT,nTracks to Zmumu)': eff_mc_etaW_othertToZmumu()
  })
  df.columns.name = '#varepsilon_{track,#mu}'
  df.index.name   = '#eta_{#mu}'
  print df
  g1,g2,g3 = list(drawer_graphs_raw(df))

  g1.fillColor   = 0
  g2.fillColor   = ROOT.kRed
  g2.fillStyle   = 3345
  g2.markerStyle = 2
  g2.lineColorAlpha = 0,0
  g3.fillColor   = ROOT.kBlue
  g3.fillStyle   = 3354
  g3.markerStyle = 2
  g3.lineColorAlpha = 0,0

  ## Making TGraphErrors
  gr = ROOT.TMultiGraph()
  gr.ownership = False

  gr.Add(g1, 'AP')
  gr.Add(g2, 'AP2')
  gr.Add(g3, 'AP2')
  gr.Draw('A')

  # post-config
  gr.xaxis.title = df.index.name
  gr.yaxis.title = df.columns.name
  gr.minimum  = 0.8
  gr.maximum  = 1.0

  ROOT.gPad.BuildLegend()
  leg = ROOT.gPad.GetPrimitive('TPave')
  leg.header = 'LHCb-Preliminary'
  leg.ownership = False
  leg.x1 = 0.3
  leg.x2 = 0.7
  leg.y1 = 0.2
  leg.y2 = 0.4

  ROOT.gPad.Update()


def draw_eff_dd_pt_explain():
  """
  Separate into different stage, to be explained in report
  - MC
  - low PT
  - high PT
  - interpolation

  Pick mumu_mu2 for normalization in ETA.
  """
  name = 'emu_mu'
  eff0 = eff_mc_eta_pt()[name].unstack()
  eff1 = eff_dd_eta_pt_lowpt_dirty()
  eff2 = eff_dd_eta_pt_highpt()
  eff3 = eff_dd_eta_pt()

  effv = eff_dd_eta_pt_veto()

  ## Reweight those 3 in ETA
  weights = weights_pt_eta_allpt_uniformeta()[name].unstack().T
  
  def reweight_eta(se):
    pt = se.name
    w  = weights[pt]
    e = se.loc[pt]
    tot = w.sum()
    if tot==0.:
      return np.nan
    return (w*e).sum()/tot

  ## Collect together, sanitize
  eff = sort_intv_pt(pd.DataFrame({
    'e0': eff0.groupby(level='PT').apply(reweight_eta),
    'e1': eff1.groupby(level='PT').apply(reweight_eta),
    'e2': eff2.groupby(level='PT').apply(reweight_eta),
    'e3': eff3.groupby(level='PT').apply(reweight_eta),
  })).dropna(how='all')

  eff.columns = [
    'MC', 
    'DD: J/#psi #rightarrow #mu#mu', 
    'DD: Z #rightarrow #mu#mu', 
    'DD: Combined & corrected',
  ]
  print eff

  ## Draw
  gr,leg = drawer_graphs_eff_pt( eff )
  leg.x1 = 0.3
  leg.x2 = 1.3
  leg.y1 = 0.85
  leg.y2 = 0.90  
  gr.maximum = 1.0
  gr.minimum = 0.8
  gr.xaxis.rangeUser      = 0,80
  gr.xaxis.moreLogLabels  = True
  gPad.SetLogx(False)
  gPad.Update()
  gPad.SaveAs('tracking_muon/eff_dd_pt_explain.pdf')
  exit()

#===============================================================================

def scalar_results():
  ## using ETA-PT
  # e = eff_dd_eta_pt().stack()
  # w = weights_pt_eta_allpt_uniformeta()
  # e = eff_dd_eta_pt_highpt().T.stack()
  # w = weights_pt_eta_highpt()

  ## Using ETA13
  w = weights_eta13()
  # e = eff_dd_sfarry_eta13_corrected()

  print 'mumu_mu1', format_eff( w.mumu_mu1, e )
  print 'mumu_mu2', format_eff( w.mumu_mu2, e )
  print 'emu_mu'  , format_eff( w.emu_mu  , e )
  print 'h1mu_mu' , format_eff( w.h1mu_mu, e )
  print 'h3mu_mu' , format_eff( w.h3mu_mu, e )

#===============================================================================

if __name__ == '__main__':
  pass

  ## Corrections
  # print syst_dd_mc_TRPCHI2()
  # print correction_dd_mc_TRPCHI2()
  print lookup_fix_TRPCHI2()

  ## Custom figure
  # draw_eff_dd_pt_explain()
  # draw_eff_compare_dd_mc_eta13()
  # draw_eff_compare_dd_mc_etaW()
  # draw_eff_compare_eta_zmumu()

  ## Final scalar eff
  # scalar_results()

  ## To start interactive shell
  # import IPython; IPython.embed()
