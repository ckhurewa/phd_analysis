#!/usr/bin/env python

from rec_utils import *

BIN_ETA = 2., 2.25, 2.5, 2.75, 3., 3.25, 3.5, 3.75, 4., 4.25, 4.5

# Note: Bin into lower PT to cover the weights. Eff should be zero there.
# BIN_PT  = np.linspace(5, 70 , 13+1)
BIN_PT = 5, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 90, 100, 200, 300

#===============================================================================
# WEIGHTS
#===============================================================================

@pickle_dataframe_nondynamic
def weights_eta_pt():
  return weighter_pt_eta(bin_pt=BIN_PT, bin_eta=BIN_ETA)


#===============================================================================
# TRIGGER EFF FROM SFARRY
#===============================================================================

def _get_TFile_sfarry():
  """
  For consistent use
  """
  fpath = '$DIR13/efficiencies/sfarry/Efficiencies/WEffs/2012/MuonWTrigger2012.root'
  return ROOT.TFile(os.path.expandvars(fpath))

# @auto_draw
@pickle_dataframe_nondynamic
def eff_dd_eta_pt():
  """
  Get the table value from sfarry, used in W/Z 2012 paper
  """
  def intv_eta(s):
    """Extract eta interval from the name of TGraph"""
    s1, s2 = re.findall(r'.*(\d\.\d+)_(\d\.\d+)', s)[0]
    s1 = try_int_else_float(s1)
    s2 = try_int_else_float(s2)
    return '(%s, %s]'%(s1, s2)

  ## Load the input file
  fin = _get_TFile_sfarry()

  ## Simple version
  df_eff  = fin.Get('ETA_PT/EffGraph2D').dataframe()
  df_eff.index = [ x.replace('000','') for x in df_eff.index ]
  df_eff.index.name = 'PT'
  df_eff.columns.name = 'ETA'
  df_eff = df_eff.stack()

  # ## Inject missing index in low-PT region
  # l_pt    = pd.cut(pd.DataFrame(), [pt for pt in BIN_PT if pt<=20]).categories
  # l_eta   = pd.cut(pd.DataFrame(), BIN_ETA).categories
  # index   = pd.MultiIndex.from_product([l_pt, l_eta], names=['PT', 'ETA'])
  # df_low  = pd.DataFrame([0.]*len(index), index=index)
  # return sort_intv_pt(pd.concat([df_low,df_eff]).iloc[:,0].unstack())

  ## Extrapolated version to larger PT region
  bin_pt_mev = [pt*1e3 for pt in BIN_PT]
  tf  = ROOT.TF1("tf","pol1", 15e3, max(bin_pt_mev))
  h   = ROOT.TH1F('h', 'h', len(bin_pt_mev)-1, bin_pt_mev)
  acc = pd.DataFrame()
  for gr in fin.Get('ETA_PT/EffGraphs'):
    gr.Fit('tf', 'Q')
    h.Reset()
    h.Add(tf)
    # p0    = ufloat(tf.GetParameter(0), tf.GetParError(0))
    # p1    = ufloat(tf.GetParameter(1), tf.GetParError(1))
    # rerr  = mathutils.sumq(p0.rerr, p1.rerr)
    rerr  = 0.05 # conservative 5% err
    se    = h.series()
    se    = se.apply(lambda u: ufloat(u.n, u.n*rerr))
    acc[intv_eta(gr.name)] = se
  acc.index   = [ x.replace('000','') for x in acc.index ] # MeV -> GeV
  acc.index.name = 'PT'
  acc.columns.name = 'ETA'
  df_extrapol = acc.stack()

  ## Replace in the extrapolate dataframe
  df_extrapol.update(df_eff)

  ## finally, extract
  fin.Close()
  return sort_intv_pt(df_extrapol.unstack())


def eff_dd_pt():
  """
  Like above, but for drawing only, so no need for full-fledge computation.
  """
  w = weights_eta_pt()['h1mu_mu'].unstack().T
  e = eff_dd_eta_pt().T
  return (w*e).sum()[2:10]/w.sum()[2:10].replace(0, 1)  # Do only [20,60] GeV


def eff_dd_eta():
  """
  Like above, but for drawing only, so no need for full-fledge computation.
  """
  w = weights_eta_pt()['h1mu_mu'].unstack()
  e = eff_dd_eta_pt()
  return (w*e).sum()/w.sum().replace(0, 1)

  # fin = _get_TFile_sfarry()
  # se  = fin.Get('ETA/EfficiencyGraph').series()
  # fin.Close()
  # se.index = se.index.map(ufloat_to_intv) # to intv string
  # return se


@gpad_save
def draw_eff_dd_pt_eta():
  eff     = eff_dd_eta_pt()
  gr,leg  = drawer_graphs_eff_pt(eff)
  gr.yaxis.title = '#varepsilon_{trig, #mu}'
  gr.xaxis.moreLogLabels  = True
  gr.xaxis.rangeUser      = 4,80
  gr.maximum = 1.0
  gr.minimum = 0.0
  leg.x1NDC  = 0.2
  leg.x2NDC  = 0.5
  leg.y1NDC  = 0.2
  leg.y2NDC  = 0.7
  ROOT.gPad.SetLogx(False)


@gpad_save
def draw_eff2D_dd_pt_eta():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird)
  ROOT.gStyle.SetPaintTextFormat(".4f")

  ## Start from PT > 20GeV
  eff = eff_dd_eta_pt().loc['(20, 25]':'(65, 70]']
  h   = ROOT.TH2F.from_uframe(eff)
  h.Draw('texte colz')
  h.markerSize          = 1.0 # text size
  h.xaxis.title         = '#eta(#mu)'
  h.yaxis.title         = 'p_{T}(#mu) [GeV]'
  h.yaxis.titleOffset   = 1.1
  h.yaxis.labelOffset   = 0.002
  # h.yaxis.rangeUser     = 0, 8
  ROOT.gPad.rightMargin = 0.12
  ROOT.gPad.leftMargin  = 0.16
  ROOT.gPad.Update()


#===============================================================================
# DATA-DRIVEN
#===============================================================================

## 160628 half-done
# JIDS = 5025,
## 160706 Fix missing ETA
JIDS = 5038, 5039, 5040, 5041, 5042, 5043

cut_filt = utils.join(
  'Z0_M       > 80e3',
  'Z0_M       < 100e3',
  'Z0_DPHI12  > 2.7',
  'Z0_ENDVERTEX_CHI2 < 9',
  #
  'muminus_PT  > 15e3',
  'muplus_PT   > 15e3',
  'muminus_ETA > 2.0',
  'muplus_ETA  > 2.0',
  'muminus_ETA < 4.5',
  'muplus_ETA  < 4.5',
  #
  '{tag}_TOS_MUON',
  '{tag}_TRPCHI2 > 0.01',
  '{tag}_ISMUON',
  '{tag}_0.50_cc_IT > 0.9',
  #
  '{probe}_TRPCHI2 > 0.01',
  '{probe}_ISMUON',
  '{probe}_0.50_cc_IT > 0.9',  
).format

param = '{probe}_TOS_MUON: {probe}_PT/1e3: {probe}_ETA'.format

#-------------------------------------------------------------------------------

# @auto_draw
@pickle_dataframe_nondynamic
def eff_ddtap_eta_pt():
  tree = import_tree( 'Trigger/Z02MuMu', *JIDS )

  ## Do 2D binning
  tree.Draw( param(probe='muplus') , cut_filt(tag='muminus', probe='muplus') , 'goff' )
  df1 = tree.sliced_dataframe()
  tree.Draw( param(probe='muminus'), cut_filt(tag='muplus' , probe='muminus'), 'goff' )
  df2 = tree.sliced_dataframe()
  df1.columns = 'passing', 'PT', 'ETA'
  df2.columns = 'passing', 'PT', 'ETA'
  df = pd.concat([df1, df2])
  df.passing = df.passing.apply(bool)

  ## Apply division 
  nume = binning_2D( df[df.passing], PT=BIN_PT, ETA=BIN_ETA )
  deno = binning_2D( df            , PT=BIN_PT, ETA=BIN_ETA )
  eff  = divide_clopper_pearson( nume, deno ).unstack().T
  eff.loc['(5, 15]'] = ufloat( 0, 0 ) # kill low-PT row
  eff.loc['(15, 20]'] = ufloat( 0, 0 ) # kill low-PT row
  return eff


#===============================================================================
# SIMULATION
#===============================================================================

@pickle_dataframe_nondynamic
def eff_mc_eta_pt():
  return calc_etrig_mc_eta_pt('muon', bin_eta=BIN_ETA, bin_pt=BIN_PT).T

@pickle_dataframe_nondynamic
def eff_mc_eta():
  return calc_etrig_mc_eta_pt('muon', bin_eta=BIN_ETA)

@pickle_dataframe_nondynamic
def eff_mc_pt():
  return calc_etrig_mc_eta_pt('muon', bin_pt=BIN_PT)


#===============================================================================

def calc_weighted_eff_dd_h1mu_h3mu():
  weights = weights_eta_pt()
  eff     = eff_dd_eta_pt()
  print 'h1mu', format_eff( weights.h1mu_mu , eff )
  print 'h3mu', format_eff( weights.h3mu_mu , eff )


#===============================================================================
# COMPARE
#===============================================================================

@gpad_save
def draw_eff_compare_dd_mc_pt():
  intvs = pd.cut(xrange(15, 65, 5), 9).categories[1:]
  df = pd.DataFrame({
    'Data'      : eff_dd_pt(),
    'Simulation': eff_mc_pt(),
  }).loc[intvs]
  df.columns.name = '#varepsilon_{trig,#mu}'
  df.index.name   = 'p_{T}(#mu) [GeV]'
  gr, leg = drawer_graphs_dd_mc(df)


@gpad_save
def draw_eff_compare_dd_mc_eta():
  df = pd.DataFrame({
    'Data'      : eff_dd_eta(),
    'Simulation': eff_mc_eta(),
  })
  df.columns.name = '#varepsilon_{trig,#mu}'
  df.index.name   = '#eta(#mu)'
  gr, leg = drawer_graphs_dd_mc(df)


#===============================================================================
# FINALLY
#===============================================================================

@memorized # For the correct correlation
def efficiencies():
  eff = eff_dd_eta_pt()
  ## kill low PT again
  # eff.loc['(5, 15]'] = 0.
  # eff.loc['(15, 20]'] = 0.
  return completely_correlate(eff.stack())

def lookup(*args):
  eff = efficiencies()
  return lookup_eff_generic(__file__, eff, _PT=BIN_PT, _ETA=BIN_ETA )(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ##
  # weights_eta_pt()
  # print eff_dd_eta_pt()
  # print eff_dd_eta()
  # print eff_dd_pt()
  # draw_eff_dd_pt_eta()
  # draw_eff2D_dd_pt_eta()
  # calc_weighted_eff_dd_h1mu_h3mu()
  # print eff_ddtap_eta_pt()

  ## MC
  # print eff_mc_eta_pt()
  # print eff_mc_eta()
  # print eff_mc_pt()

  ## Compare
  # draw_eff_compare_dd_mc_pt()
  # draw_eff_compare_dd_mc_eta()

  # >> WAITING BENDER SAMPLE FOR ZTAUTAU0

  ## finally
  # print lookup().unstack().fmt2p


