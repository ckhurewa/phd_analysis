#!/usr/bin/env python

from rec_utils import *
import tracking_muon_aux
from tracking_bins import BIN_ETA_W, BIN_NTRACKS_LITE, multieff

#===============================================================================
# EFF MC
#===============================================================================

@memorized
def get_correction_syst_Zee():
  """
  Added the tracking correction via David's Zee paper

  Quoted
  The ratio between the two, 0.990 +- 0.004 is taken as a correction to etrack
  obtained from simulation, with the full size of the correction taken as a syst
  """
  return ufloat( .990, .004 ) * ufloat( 1.0, .010 )

def eff_mc_eta_ntracks_addedsyst():
  """
  With electron's corr & syst via Zee paper, as well as TRPCHI2 syst.
  No central tracking correction yet.

  Because as checked by eye, there's not so much variation between channels,
  (unlike hadron), this is opt-in for etrack of all electrons combined instead
  in order to have less statistical uncertainties.
  """  
  # 170408: changed from litediv to fulldiv 
  
  eff  = multieff('etaW_nlite').loc[Idx['fulldiv'],'electrons'].unstack()
  mult = get_correction_syst_Zee()              # scalar
  return eff * mult
  # syst = tracking_muon_aux.syst_dd_mc_TRPCHI2() # series # 170408: no more
  # return eff.mul(syst, axis=0) * mult


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_eff_mc_ntracks():
  eff     = eff_mc_ntracks()[['electrons']]
  gr,leg  = drawer_graphs_eff_pt(eff)
  gr.minimum      = 0.0
  gr.maximum      = 1.0
  gr.xaxis.title  = 'nTracks'
  gr.yaxis.title  = 'Electron tracking efficiency'
  leg.Delete()
  gPad.SetLogx(False)


@gpad_save
def draw_eff_mc_eta_ntracks():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gROOT.SetBatch(False)

  eff     = eff_mc_eta_ntracks_corrected().unstack()
  gr,leg  = drawer_graphs_eff(eff)
  gr.xaxis.title = '#eta(e)'
  gr.yaxis.title = '#varepsilon_{track, e}'
  gr.minimum = 0.0
  gr.maximum = 1.0
  leg.header = 'LHCb-Simulation'
  leg.x1NDC  = 0.18
  leg.x2NDC  = 0.52
  leg.y1NDC  = 0.18
  leg.y2NDC  = 0.54
  ROOT.gPad.SetLogx(False)  


#===============================================================================
# FINAL TABLE
#===============================================================================

def lookup(*args):
  # eff = eff_mc_eta_ntracks_corrected()
  eff = eff_mc_eta_ntracks_addedsyst().stack()
  return lookup_eff_generic(__file__, eff, _ETA=BIN_ETA_W, nTracks=BIN_NTRACKS_LITE)(*args)

#===============================================================================

if __name__ == '__main__':
  pass

  ## Raw eff
  # print eff_mc_eta_ntracks_addedsyst()  

  ## Draw
  # draw_eff_mc_ntracks()
  # draw_eff_mc_eta_ntracks()

  ## Finally
  print lookup()
