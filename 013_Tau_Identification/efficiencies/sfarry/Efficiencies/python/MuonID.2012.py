from efficiency import EfficiencyClass
from ROOT import TFile, TCut, TTree, TMath

  
triggercut = TCut("tag_Hlt2SingleMuonHighPTDecision_TOS==1 && tag_Hlt1SingleMuonHighPTDecision_TOS == 1 && tag_L0MuonDecision_TOS ==1")
phicut= TCut("(abs(tag_PHI-probe_PHI)<TMath::Pi() ? abs(tag_PHI-probe_PHI) : 2*TMath::Pi()-abs(tag_PHI-probe_PHI))>2.7")
ptcut = TCut("tag_PT > 20000 && probe_PT > 20000")
isocut = TCut("tag_cpt_0.50 < 2000 && probe_cpt_0.50 < 2000")
trkqual = TCut("tag_TRACK_PCHI2 > 0.001 && probe_TRACK_PCHI2 > 0.001 && (sqrt(tag_PERR2)/tag_P) < 0.1 && (sqrt(probe_PERR2)/probe_P) < 0.1")
chi2cut = TCut("boson_ENDVERTEX_CHI2/boson_ENDVERTEX_NDOF <5")

magup = TCut("Polarity == 1")
magdown = TCut("Polarity == -1 ")
eta = TCut("tag_ETA > 2 && tag_ETA < 4.5 && probe_ETA > 2 && probe_ETA < 4.5");

selcut = ptcut + phicut + isocut + triggercut + trkqual + chi2cut + eta
passcut = TCut("probe_isMuon == 1")

selcutMU = selcut + magup
selcutMD = selcut + magdown

MuonID2012 = EfficiencyClass("MuonID2012")
  
f = TFile("../Tuples/MuonIDEff.2012.MagDown.root")
t = f.Get("PlusTag/DecayTree")
u = f.Get("MinusTag/DecayTree")

g = TFile("../Tuples/MuonIDEff.2012.MagUp.root")
v = g.Get("PlusTag/DecayTree")
w = g.Get("MinusTag/DecayTree")

MuonID2012.SetTree(t)
MuonID2012.SetTree(u)
MuonID2012.SetTree(v)
MuonID2012.SetTree(w)
MuonID2012.SetSelectionCut(selcut)
MuonID2012.SetPassCut(passcut)
MuonID2012.AddVar("ETA", "probe_ETA", 10 , 2 , 4.5 )
MuonID2012.AddVar("PT", "probe_PT", 10 , 20000 , 70000 )
MuonID2012.AddVar("PT2", "probe_PT", 100 , 20000 , 70000 )
MuonID2012.AddVar("P", "probe_P", 8 , 100000 , 500000 )
MuonID2012.AddVar("PHI", "probe_PHI", 10 , -TMath.Pi() , TMath.Pi() )
MuonID2012.AddVar("VeloClusters", "nVeloClusters", 8 , 0 , 4000 , "I")
MuonID2012.AddVar("ITClusters", "nITClusters", 8 , 0 , 2000 , "I")
MuonID2012.AddVar("PVs", "nPVs", 6 , -0.5 , 5.5 , "I")
MuonID2012.AddVar("Tracks", "nTracks", 10 , 0 , 200 , "I")
MuonID2012.AddVar("SPDHits", "nSPDHits", 20 , 0 , 1000 , "I")
MuonID2012.Add2DVar( "ETA", "PHI")
MuonID2012.Add2DVar( "ETA", "PT" )
MuonID2012.Add2DVar( "ETA", "Tracks" )
MuonID2012.SetPltRange("boson_M" , 50 , 60000 , 120000)
MuonID2012.MakeEntryLists()
MuonID2012.MakeHists()
MuonID2012.MakeEfficiencyGraph()
MuonID2012.SaveToFile()
