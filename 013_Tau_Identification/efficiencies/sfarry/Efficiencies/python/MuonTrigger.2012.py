from ROOT import TFile, TCut, TTree, TMath

triggercut = TCut("tag_Hlt2SingleMuonHighPTDecision_TOS==1 && tag_Hlt1SingleMuonHighPTDecision_TOS == 1 && tag_L0MuonDecision_TOS ==1")
trkqual    = TCut("tag_TRACK_PCHI2 > 0.001 && probe_TRACK_PCHI2 > 0.001 && (sqrt(tag_PERR2)/tag_P) < 0.1 && (sqrt(probe_PERR2)/probe_P) < 0.1")
eta        = TCut("tag_ETA > 2 && tag_ETA < 4.5 && probe_ETA > 2 && probe_ETA < 4.5")
vtxcut     = TCut("boson_ENDVERTEX_CHI2/boson_ENDVERTEX_NDOF < 5")
isocut     = TCut("tag_cpt_0.50 < 2000 && probe_cpt_0.50 < 2000")
tck        = TCut("OdinTCK != 7602230")
pt         = TCut("tag_PT > 20000 && probe_PT > 20000" )
mass       = TCut("boson_M > 60000 && boson_M < 120000")

#selection cut
selcut     = pt + mass + triggercut + trkqual + eta + vtxcut + isocut
#passcut
passcut = TCut("probe_Hlt2SingleMuonHighPTDecision_TOS==1 && probe_Hlt1SingleMuonHighPTDecision_TOS == 1 && probe_L0MuonDecision_TOS ==1")


#load file
f = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/user/s/sfarry/EffTuples/MuonTrigger.2012.root")
   
#load plus and minus tag trees
ptagt = f.Get("PlusTag/DecayTree")
mtagt = f.Get("MinusTag/DecayTree")
   
magup = TCut("Polarity == 1")
magdown = TCut("Polarity == -1")

#selcut for mag up and mag down
selcutMU = selcut + magup
selcutMD = selcut + magdown

#get total efficiency
#Ntot = ptagt.GetEntries(selcut.GetTitle()) + mtagt.GetEntries(selcut.GetTitle())
#Npass = ptagt.GetEntries((selcut + passcut).GetTitle()) + mtagt.GetEntries((selcut + passcut).GetTitle())

print "Numbers for Muon Trigger Efficiency in 2012 using tag-and-probe method"
print "Trees are called ptagt (Plus Tag) and mtagt (Minus Tag)"
print "Total and pass cuts are called selcut and passcut"
#print "Total Efficiency is: "+str(float(Npass)/Ntot)
