#!/usr/bin/env python

"""

For the calculation of "Acceptance" efficiency.

Given LHCb fiducial acceptance ( PT(tau)>20, eta(tau) in [2.0,4.5], MM(Z) in [60,120] ),
check how many tau-products fit inside acceptance.

"""

from PythonCK.stringutils import insert_line
from PyrootCK import *
from PyrootCK.mathutils import EffU
from PyrootCK.QHist.CoreV3 import QHist as QHistV3

sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import (
  pd, DTYPES, DTYPE_TO_DT, DT_TO_LATEX, DTYPE_TO_DGROUP,
  LABELS_TAU, LABELS_DITAU_SEP, drawers, inject_midrule,
)

#===============================================================================

fiducial_ll = utils.join(
  'Ztop_M         > 60',
  'Ztop_M         < 120',
  'tau1top_PT     > 20',
  'tau2top_PT     > 20',
  'tau1top_ETA    > 2.0',
  'tau2top_ETA    > 2.0',
  'tau1top_ETA    < 4.5',
  'tau2top_ETA    < 4.5',
)

fiducial_lh = utils.join(
  'Ztop_M       > 60',
  'Ztop_M       < 120',
  'taultop_PT   > 20',
  'tauhtop_PT   > 20',
  'taultop_ETA  > 2.0',
  'tauhtop_ETA  > 2.0',
  'taultop_ETA  < 4.5',
  'tauhtop_ETA  < 4.5',
)

all_fiducial = {
  'e_e'  : fiducial_ll,
  'e_h1' : fiducial_lh,
  'e_h3' : fiducial_lh,
  'e_mu' : fiducial_ll,
  'h1_mu': fiducial_lh,
  'h3_mu': fiducial_lh,
  'mu_mu': fiducial_ll,
}

#----------------------------

presel_ll = [
  {
    ( 'lep1_PT > 20', 'lep2_PT >  5' ),
    ( 'lep1_PT >  5', 'lep2_PT > 20' ),
  },
  'lep1_ETA  > 2.0',
  'lep1_ETA  < 4.5',
  'lep2_ETA  > 2.0',
  'lep2_ETA  < 4.5',
  #
  'ditauch_M  > 20',
  'ditauch_M  < 120',
]

presel_lh1 = [
  'lep_PT   > 20',
  'had_PT   > 10',
  #
  'lep_ETA  > 2.0',
  'lep_ETA  < 4.5',
  'had_ETA  > 2.25',
  'had_ETA  < 3.75',
  #
  'ditauch_M  > 30',
  'ditauch_M  < 120',
]

presel_lh3 = [
  'lep_PT   > 20',
  'lep_ETA  > 2.0',
  'lep_ETA  < 4.5',
  #
  ## PR is already sorted by PT, descending
  'pr1_PT   >  6',
  'pr3_PT   >  1',
  #
  'pr1_ETA  > 2.25',
  'pr1_ETA  < 3.75',
  'pr2_ETA  > 2.25',
  'pr2_ETA  < 3.75',
  'pr3_ETA  > 2.25',
  'pr3_ETA  < 3.75',
  #
  'tauh3ch_PT > 12',
  'tauh3ch_M  > 0.7',
  'tauh3ch_M  < 1.5',
  #
  'ditauch_M >  30',
  'ditauch_M < 120',
]

presels = {
  'e_e'  : presel_ll + ['ditauch_M < 80'],
  'e_h1' : presel_lh1,
  'e_h3' : presel_lh3,
  'e_mu' : presel_ll,
  'h1_mu': presel_lh1,
  'h3_mu': presel_lh3,
  'mu_mu': presel_ll + ['ditauch_M < 80'],
}

duo_prefixes = {  
  'e_e'  : ('lep1'   , 'lep2'),
  'mu_mu': ('lep1'   , 'lep2'),
  'e_mu' : ('lep2'   , 'lep1'), # draw as x=e, y=mu, to show drag horizontally
  'h1_mu': ('had'    , 'lep' ),
  'e_h1' : ('had'    , 'lep' ),
  'h3_mu': ('tauh3ch', 'lep' ),
  'e_h3' : ('tauh3ch', 'lep' ),
}

# NLO-POWHEGBOX:
# 3859 Zg, mumu only
# 4006 Zg, h1mu only
# 4190 all channel small 1Mevt
# 4262 all channel, 500kevt x 200sj = 100Mevt.
def trees(dtype):
  tree = import_tree(dtype, 4262)
  tree.title = 'Z#rightarrow#tau#tau'
  ## alias
  p1,p2 = duo_prefixes[dtype]
  tree.SetAlias('PT1' , '%s_PT'%p1)
  tree.SetAlias('PT2' , '%s_PT'%p2)
  tree.SetAlias('ETA1', '%s_ETA'%p1)
  tree.SetAlias('ETA2', '%s_ETA'%p2)
  ## Finally
  return tree


#===============================================================================
# CALCULATE ACCEPTANCE manually
#===============================================================================

def fraction( n, N ):
  return '{} / {} = {:.4f}'.format(n, N, 1.*n/N)

def calculate(dtype):
  fiducial    = all_fiducial[dtype]
  selected    = utils.join(presels[dtype])
  notselected = '!(%s)'%selected
  tree        = trees(dtype)
  def count( *c ):
    return tree.GetEntries( utils.join(c) )

  print 'di-tau type:', dtype
  print 'Amount of events                              |', count('')
  print 'Acceptance ( simple divide )                  |', fraction( count(selected)            , count(fiducial)  )
  print 'Acceptance ( required tau-cut in numerator )  |', fraction( count(selected,fiducial)   , count(fiducial)  )
  # print 'Leak-in  ( tau was outside, daughters valid ) |', fraction( count(selected,notfiducial), count(notfiducial))
  # print 'Leak-out ( tau was inside, daughter gone out )|', fraction( count(notselected,fiducial), count(fiducial)  )

def calculate_all():
  for dtype in presels:
    calculate( dtype )

#-------------------------------------------------------------------------------

def calc_fast_raw():
  ## Get 2D histogram,
  # JID = 4695
  # JID = 5008
  # JID = 5035 # much larger stats, correct ll channel masscut
  JID = 5223 # different regime (mass window, normal/narrow eta )
  fin = import_file(JID)

  df = pd.DataFrame()
  for dtype in DTYPES:
    # h = f.Get('hist_'+dtype) # old
    # h = f.Get('masswindow_narroweta_hist_'+dtype) # cross-check with old
    h = fin.Get('masswindow_normaleta_hist_'+dtype) # hadron in normal eta
    # h = fin.Get('nomasswindow_normaleta_hist_'+dtype) # hadron in normal eta
    df[DTYPE_TO_DT[dtype]] = [
      # h.GetBinContent(1,1), # nofid_noacc
      h.GetBinContent(2,2), # fid_acc
      h.GetBinContent(2,1), # fid_noacc
      h.GetBinContent(1,2), # nofid_acc
    ]
  fin.Close()
  df.index = 'fid_acc', 'fid_noacc', 'nofid_acc'

  ## Apply division
  df.loc['leakin'] = df.apply(lambda se: EffU(se[0]+se[2], se[2])) # leakin
  df.loc['eff']    = df.apply(lambda se: EffU(se[0]+se[1], se[0]+se[2]))

  # ## ADD 1.61% ERROR FROM PDF, BORROW FROM 7TeV
  # df.loc[4] *= ufloat(1, 0.0161)

  ## Syst from Aurelio, using MSTW2008. Make sure of correlation
  syst_ll  = ufloat(1., 0.013)
  syst_lh1 = ufloat(1., 0.019)
  syst_lh3 = ufloat(1., 0.015)
  syst     = pd.Series({
    'mumu': syst_ll,
    'ee'  : syst_ll,
    'emu' : syst_ll,
    'h1mu': syst_lh1,
    'eh1' : syst_lh1,
    'h3mu': syst_lh3,
    'eh3' : syst_lh3,
  })
  df.loc['eff'] *= syst

  ## Finally
  return df


def to_fast_csc():
  """
  Use a histogram version where cut is already static in the pythia script.
  Less flexible, but faster offline and more accurate.
  """
  print calc_fast_raw().loc['eff'].to_fast_csc('acc')


#-------------------------------------------------------------------------------

def latex_eacc_corrected():
  """
  Table for appendix section. Use the fast version.
  """

  ## String index
  INDICES = {
    'fid_acc'  : 'In fiducial, in acceptance',
    'fid_noacc': 'In fiducial, not in acceptance',
    'nofid_acc': 'Not in fiducial, in acceptance',
    'leakin'   : 'Migration into \\eacc [%]',
    'eff'      : 'Effective \\eacc [%]',
  }

  df = calc_fast_raw().rename(INDICES, columns=DT_TO_LATEX)
  df.iloc[0] = df.iloc[0].apply(int)
  df.iloc[1] = df.iloc[1].apply(int)
  df.iloc[2] = df.iloc[2].apply(int)
  df.iloc[3] = df.iloc[3].fmt2lp
  df.iloc[4] = df.iloc[4].fmt2lp
  df.index.name = ''
  inject_midrule(df, -2)

  ## Finally
  return df.to_markdown(stralign='right')


#===============================================================================
# LOTS OF DRAWING
#===============================================================================

def fmin(s1,s2):
  return '({s1}<={s2})*{s1} + ({s1}>{s2})*{s2}'.format(s1=s1,s2=s2)
def fmax(s1,s2):
  return '({s1}>={s2})*{s1} + ({s1}<{s2})*{s2}'.format(s1=s1,s2=s2)

#-------------------------------------------------------------------------------
# PT
#-------------------------------------------------------------------------------

def draw_pt(dtype):
  ## log
  xmin = ymin = (1e-0 if 'h3' in dtype else 1e-1)
  xmax = ymax = 2e2
  ## non-log
  # xmin = ymin = 0
  # xmax = ymax = 100

  ## Label template
  label0 = 'p_{T}(%s) [GeV/c]'

  ## Array of cuts
  arr = {
    'll' : [(xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,5) , (20,5) , (20,20), (5,20), (5,ymax)],
    'lh1': [(xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,10), (20,10), (20,ymax)],
    'lh3': [(xmin,ymax), (xmin,ymin), (xmax,ymin), (xmax,12), (20,12), (20,ymax)],
  }[DTYPE_TO_DGROUP[dtype]]

  ## Post-processor
  def postproc(h):
    ## Tune spacing
    h.anchors.stack.xaxis.labelOffset   = -0.01
    h.anchors.stack.xaxis.moreLogLabels = False
    h.anchors.stack.yaxis.moreLogLabels = False
    ## same box in WEH patch
    Box(arr).Draw('f')

  ## Start drawing
  h = QHistV3()
  h.prefix   = DTYPE_TO_DT[dtype]
  h.name     = 'pt'
  h.filters  = all_fiducial[dtype]
  h.trees    = trees(dtype)
  h.params   = 'PT1:PT2'
  h.xlog     = True
  h.ylog     = True
  h.xmin     = xmin
  h.ymin     = ymin
  h.xmax     = xmax
  h.ymax     = ymax
  h.option   = 'col'
  h.xlabel   = label0%LABELS_DITAU_SEP[dtype][0]
  h.ylabel   = label0%LABELS_DITAU_SEP[dtype][1]
  h.postproc = postproc
  h.draw()


def draw_pt_all():
  for dtype in presels:
    draw_pt(dtype)


def draw_pr1_pr3_pt():
  """
  Particular case. Draw only from h3mu
  """
  def postproc(h):
    Box([(6,1), (6,6), (2E2,2E2), (1,2e2), (1,1e-1), (2e2,1e-1), (2E2,1)]).Draw('f')
    ## Tune spacing
    h.anchors.stack.xaxis.labelOffset   = -0.01
    h.anchors.stack.xaxis.moreLogLabels = False
    h.anchors.stack.yaxis.moreLogLabels = False

  ## Start drawing  
  h = QHistV3()
  h.name     = 'tauh3_pr_pt'
  h.filters  = all_fiducial['h3_mu']
  h.trees    = trees('h3_mu')
  h.params   = 'pr3_PT: pr1_PT'
  h.xlog     = True
  h.ylog     = True
  h.zlog     = False
  h.xmin     = 1E0
  h.ymin     = 1E-1
  h.xmax     = 2E2
  h.ymax     = 2E2
  h.option   = 'col'
  h.xlabel   = 'max(p_{T}(prong)) [GeV/c]'
  h.ylabel   = 'min(p_{T}(prong)) [GeV/c]'
  h.postproc = postproc
  h.draw()

#-------------------------------------------------------------------------------
# ETA
#-------------------------------------------------------------------------------

def draw_eta(dtype):
  ## Square frame
  XMIN = YMIN = 1.5
  XMAX = YMAX = 5

  def reverse_box(xmin, ymin, xmax, ymax):
    return (xmin,ymin), (xmin,ymax), (xmax,ymax), (xmax,ymin), (xmin,ymin), (XMIN,ymin), (XMIN,YMAX), (XMAX,YMAX), (XMAX,YMIN), (XMIN,YMIN), (XMIN,ymin)

  def postproc(h):
    ## same box in WEH patch
    Box(reverse_box(2, 2, 4.5, 4.5)).Draw('f')

  ## Start drawing
  h = QHistV3()
  h.prefix   = DTYPE_TO_DT[dtype]
  h.name     = 'eta'
  h.filters  = all_fiducial[dtype]
  h.trees    = trees(dtype)
  h.params   = 'ETA1:ETA2'
  # h.zlog     = True
  h.zlog     = False
  h.xmin     = XMIN
  h.ymin     = YMIN
  h.xmax     = XMAX
  h.ymax     = YMAX
  h.option   = 'col'
  h.xlabel   = '#eta(%s)'%LABELS_DITAU_SEP[dtype][0]
  h.ylabel   = '#eta(%s)'%LABELS_DITAU_SEP[dtype][1]
  h.postproc = postproc
  h.draw()


def draw_eta_all():
  for dtype in presels:
    if 'h3' not in dtype:
      draw_eta(dtype)

#-------------------------------------------------------------------------------
# MASS
#-------------------------------------------------------------------------------

def draw_ditau_mass():
  t1 = trees('e_mu')
  t1.SetAlias('filt', utils.join(fiducial_ll, [ c for c in presel_ll if '_M' not in c ]))
  t1.title = LABELS_TAU['l']+LABELS_TAU['l']

  t2 = trees('h1_mu')
  t2.SetAlias('filt', utils.join(fiducial_lh, [ c for c in presel_lh1 if '_M' not in c ]))
  t2.title = LABELS_TAU['l']+LABELS_TAU['h1']

  t3 = trees('h3_mu')
  t3.SetAlias('filt', utils.join(fiducial_lh, [ c for c in presel_lh3 if 'ditauch_M' not in c ]))
  t3.title = LABELS_TAU['l']+LABELS_TAU['h3']

  def tune_legend(h):
    h.anchors.legend.header = 'LHCb 8TeV'
    h.anchors.legend.x1NDC  = 0.72
    h.anchors.legend.x2NDC  = 1.05
    h.anchors.legend.y1NDC  = 0.60
    h.anchors.legend.y2NDC  = 0.92

  ## Start drawing
  h = QHistV3()
  h.name      = 'ditau_mass'
  h.filters   = 'filt'
  h.trees     = t1,t2,t3
  h.params    = 'ditauch_M'
  h.xmin      = 0
  h.xmax      = 120
  h.xlabel    = 'Invariant mass(#tau#tau) [GeV/c^{2}]'
  h.st_marker = False
  h.st_color  = True
  h.st_fill   = False
  h.batch     = True
  h.postproc  = tune_legend
  h.draw()


def draw_tauh3_mass(dtype):
  ## Get all cuts except mass
  cut = utils.join([ c for c in presel_lh3 if '_M' not in c ])

  ## boxing
  XMIN = 0
  XMAX = 120
  YMIN = 0.4
  YMAX = 1.8
  def reverse_box(xmin, ymin, xmax, ymax):
    return (xmin,ymin), (xmin,ymax), (xmax,ymax), (xmax,ymin), (xmin,ymin), (XMIN,ymin), (XMIN,YMAX), (XMAX,YMAX), (XMAX,YMIN), (XMIN,YMIN), (XMIN,ymin)

  def postproc(h):
    Box(reverse_box(40, 0.7, XMAX, 1.5)).Draw('f')
    h.anchors.stack.yaxis.titleOffset = 1.1

  ## Start drawing
  h = QHistV3()
  h.prefix  = DTYPE_TO_DT[dtype]
  h.name    = 'mass'
  h.filters = all_fiducial[dtype]
  h.trees   = trees(dtype)
  h.params  = 'tauh3ch_M: ditauch_M'
  h.cuts    = cut
  h.xmin    = XMIN
  h.ymin    = YMIN
  h.xmax    = XMAX
  h.ymax    = YMAX
  h.xlabel  = 'm(%s%s) [GeV/c^{2}]'%(LABELS_TAU['l'], LABELS_TAU['h3'])
  h.ylabel  = 'm(%s) [GeV/c^{2}]'%LABELS_TAU['h3']
  h.option  = 'col'
  h.postproc= postproc
  h.draw()

def draw_tauh3_mass_all():
  draw_tauh3_mass('h3_mu')
  draw_tauh3_mass('e_h3')

#===============================================================================

if __name__ == '__main__':
  ## SETUP
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors
  ## Mostly square plot
  drawers.set_style_square()

  ## Calculate numeric value of acceptance
  # calculate('mu_mu')
  # calculate_all()
  # print calc_fast_raw()
  # to_fast_csc()
  # print latex_eacc_corrected()

  ## PT
  # draw_pt('h3_mu')
  # draw_pt_all()
  # draw_pr1_pr3_pt()

  ## ETA
  # draw_eta('e_e')
  # draw_eta_all()

  ## MASS
  draw_ditau_mass()
  # draw_tauh3_mass_all()

  # ## Patch to save all new color (especially with transparency) into tfile
  # import itertools
  # fin = ROOT.TFile.Open('acceptance/acceptance.root', 'UPDATE')
  # d = fin.Get('colors')
  # if not d:
  #   d = fin.mkdir('colors')
  # d.cd()
  # for color in itertools.islice( ROOT.gROOT.colors, 600, None):
  #   if color.alpha:
  #     print 'Saving custom color with transparency:', color
  #     color.Write(str(color.number)+'_'+color.name, ROOT.TObject.kOverwrite)
  # fin.Close()

