#!/bin/tcsh

echo `date`
source ~/bin/setup_powheg-box.csh

## Prep environ, get the ready-grid.
cp ~/analysis/013_Tau_Identification/efficiencies/nlo-acceptance/powheg/* .
cp ~/analysis/013_Tau_Identification/efficiencies/nlo-acceptance/pythia.py .
rm *.lhe

mv usegrid-powheg.input powheg.input

## Add the unique gen seed
echo "\nnumevts $1" >> powheg.input
echo "\niseed $2"   >> powheg.input

## Run 
Z powheg.input

## Use Pythia to filter, shower, and write to ROOT.
./pythia.py

