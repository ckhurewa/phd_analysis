#!/usr/bin/env python

# NOTE: 10kevt ~ 10Mb lhe ~ 114kb tuple.root
# --> 500kevt ~ 500Mb lhe ~ 5.7Mb tuple per subjob
# >>> 100Mevt ~           ~ 1.14Gb

NEVT     = 1e6
NSUBJOBS = 500

j = Job()
j.name    = 'NLO.acc Ztau 8TeV'
j.comment = 'Powheg+Pythia (1Mevt)x500: Flex mass/eta.'
# j.comment = 'Powheg+Pythia (1Mevt)x500: NoDitauMass for HMT'

j.application = Executable( exe=File('powheg_pythia8.csh') )
j.backend     = PBS(extraopts='--mem=2900 -t 1-0:0:0 --exclude=lphe01,lphe02,lphe03,lphe04,lphe05,lphe06,lphe07,lphe08,lphe09,lphe10')
j.splitter    = ArgSplitter(args=[[str(int(NEVT)),str(i)] for i in xrange(int(NSUBJOBS)) ]) # Seed in arg
j.outputfiles = [ '*.root' ] # MassStorageFile

j.submit()
