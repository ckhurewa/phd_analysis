#!/usr/bin/env python

"""

"""

import itertools
from collections import OrderedDict
from PyPythia8 import Pythia, Particle, UserHooks, Event
#
from rootpy.tree import Tree
from rootpy.io import root_open
import rootpy.ROOT as ROOT

DTYPES = 'e_e', 'e_h1', 'e_h3', 'e_mu', 'h1_h1', 'h1_h3', 'h1_mu', 'h3_h3', 'h3_mu', 'mu_mu'


#===============================================================================
# Define the dtype-dependent schema

schema0       = 'Ztop', 'Zbot', 'ditauch'
schema_LL     = 'tau1top', 'tau1bot', 'tau2top', 'tau2bot', 'lep1', 'lep2'
schema_LH1    = 'taultop', 'taulbot', 'tauhtop', 'tauhbot', 'lep' , 'had'
schema_LH3    = 'taultop', 'taulbot', 'tauhtop', 'tauhbot', 'lep' , 'pr1' , 'pr2', 'pr3', 'tauh3ch', 'pipi_low', 'pipi_high'

## contain only working channels
all_particles = {
  'e_e'   : schema0 + schema_LL,
  'e_mu'  : schema0 + schema_LL,
  'mu_mu' : schema0 + schema_LL,
  'e_h1'  : schema0 + schema_LH1,
  'h1_mu' : schema0 + schema_LH1,
  'e_h3'  : schema0 + schema_LH3,
  'h3_mu' : schema0 + schema_LH3,
}

## Attributes of Vec4 to collect
attrs         = 'pT', 'eta', 'phi', 'm'
all_branches  = { dtype:{p+'_'+a.upper():'F' for p in particles for a in attrs} for dtype,particles in all_particles.iteritems() }

## 
fill_modes = {
  'masswindow_narroweta'  : {'ditau_mass_window':True , 'narrow_eta_hadron': True },
  'masswindow_normaleta'  : {'ditau_mass_window':True , 'narrow_eta_hadron': False},
  'nomasswindow_narroweta': {'ditau_mass_window':False, 'narrow_eta_hadron': True },
  'nomasswindow_normaleta': {'ditau_mass_window':False, 'narrow_eta_hadron': False},
}


#===============================================================================

class Veto_fiducial(UserHooks):
  """
  This hook with only select Z within LHCb fiducial acceptance. 
  Its number becomes the denominator in acceptance efficiency term.

  Note: Do veto at Parton level (not process), this is closer to the truth.
  Note: Do kill this in WIDE cut only. Don't trust the kinematic at this stage.
  - PT>20 --> PT>18
  - eta in [2.0,4.5] --> [1.8,4.7]
  """

  def doVetoProcessLevel(self, process):
    """
    Do a very loose cut to safe CPU. 
    """
    Z     = process.find(id=23)
    tau1  = Z.children(id=15)[0]
    tau2  = Z.children(id=-15)[0]
    if tau1.pT < 18.: return True
    if tau2.pT < 18.: return True
    if tau1.eta < 1.5: return True
    if tau2.eta < 1.5: return True
    return False

  def doVetoPartonLevel(self, process):
    """
    Do loose cut closer to actual fiducial cut.
    """
    Z     = process.find(id=23)
    tau1  = Z.children(id=15)[0]
    tau2  = Z.children(id=-15)[0]
    ## Veto ( see LHCb's fiducial region for Z )
    if not ( 60. < Z.m < 120. ):
      return True
    if tau1.pT < 18.:
      return True
    if tau2.pT < 18.:
      return True
    if tau1.eta < 1.5:
      return True
    if tau2.eta < 1.5:
      return True
    ## No veto, go go go.
    return False

#===============================================================================

def tauitype( tau ):
  """
  return one of type (e, h1, h3, mu, other) of given Particle.
  """
  nQ = 0
  for c in tau.children():
    idabs = c.idAbs
    if idabs == 11:
      return 0
    if idabs == 13:
      return 3
    nQ += int(c.isCharged)
  if nQ==1:
    return 1 # tauh1
  if nQ==3:
    return 2 # tauh3
  return 4 # Other

def tautype( tau ):
  return ('e','h1','h3','mu','other')[tauitype(tau)]

def ditautype( ditau ):
  """Return ditautype, such as 'e_e', 'h3_mu'"""
  tau1,tau2 = sorted(ditau.botCopyId().children(), key=tauitype)
  return '_'.join([tautype(tau1),tautype(tau2)])

def extract_vec4ch( tau ):
  """
  Given tau, try to return the vectorial sum of charge decay product at 2 stages,
  initial and after FSR.

  For simple tau, it can be simply electron, muon, pion/kaon.
  For tauh3, the Vec4 sum is returned. Thus, other operation using result of this
  function should be based on methods available in Vec4, not Particle class.
  """
  l = tau.children(isCharged=True)
  if len(l)==1: # Simple case
    return l[0].p(), l[0].botCopyId().p()
  if len(l)==3: # tauh3
    p1,p2,p3 = l
    veci = p1.p() + p2.p() + p3.p()
    vecf = p1.botCopyId().p() + p2.botCopyId().p() + p3.botCopyId().p()
    return veci, vecf
  print tau
  print tau.children(isCharged=True)
  raise NotImplementedError


#===============================================================================

def identify( dtype, Z ):
  """
  Return a dictionary, where each key is the particles requested in schema,
  and values are respective particles. 
  """

  Ztop = Z.topCopyId()
  Zbot = Ztop.botCopyId()

  tau1top, tau2top = sorted(Ztop.children(), key=tauitype)
  tau1bot = tau1top.botCopyId()
  tau2bot = tau2top.botCopyId()

  ## Populate variables required by the schema
  if dtype in ('e_e', 'e_mu', 'mu_mu'):
    taul1top  = tau1top
    taul2top  = tau2top
    taul1bot  = tau1bot
    taul2bot  = tau2bot
    lep1      = tau1bot.children(isCharged=True)[0]
    lep2      = tau2bot.children(isCharged=True)[0]
    ditauch   = lep1.p + lep2.p

  if dtype in ('e_h1', 'h1_mu'):
    if dtype=='e_h1':
      taultop = tau1top
      tauhtop = tau2top 
    elif dtype=='h1_mu':
      tauhtop = tau1top 
      taultop = tau2top
    taulbot = taultop.botCopyId()
    tauhbot = tauhtop.botCopyId()
    lep     = taulbot.children(isCharged=True)[0]
    had     = tauhbot.children(isCharged=True)[0]
    ditauch = lep.p + had.p

  if dtype in ('e_h3', 'h3_mu'):
    if dtype=='e_h3':
      taultop = tau1top
      tauhtop = tau2top 
    elif dtype=='h3_mu':
      tauhtop = tau1top 
      taultop = tau2top
    taulbot     = taultop.botCopyId()
    tauhbot     = tauhtop.botCopyId()
    lep         = taulbot.children(isCharged=True)[0]
    pr1,pr2,pr3 = sorted(tauhbot.children(isCharged=True), key=lambda p: p.pT, reverse=True)
    tauh3ch     = pr1.p + pr2.p + pr3.p
    ditauch     = lep.p + tauh3ch
    # identify rho1,rho2. Sorted by foreign change (opposite of tau), then same (expected 2)
    pq1,pq2,pq3 = sorted([pr1,pr2,pr3], key=lambda p: p.charge==tauhbot.charge)
    rho1        = pq1.p+pq2.p
    rho2        = pq1.p+pq3.p
    pipi_low,pipi_high = sorted([rho1,rho2], key=lambda p: p.mCalc)

  ## Finally, pack the result.
  context = locals()
  return { name:context[name] for name in all_particles[dtype] }


#===============================================================================
# FIDUCIAL
#===============================================================================

def in_fiducial( Z ):
  """
  Return True if given Z is inside required acceptance.
  """
  tau1  = Z.children(id=15)[0]
  tau2  = Z.children(id=-15)[0]
  if not ( 60. < Z.m < 120. ):
    return False
  if tau1.pT < 20.:
    return False
  if tau2.pT < 20.:
    return False
  if not ( 2.0 < tau1.eta < 4.5 ):
    return False
  if not ( 2.0 < tau2.eta < 4.5 ):
    return False
  return True

def in_acceptance( dtype, particles, ditau_mass_window=True, narrow_eta_hadron=False ):
  """
  Return True if given set of particles passes the acceptance selection as 
  dictated by given dtype spec.
  
  It doesn't care whether the candidates came from Ztautau or not, just provide
  the check.

  Note: The flag `ditau_mass_window` will set mass limit at the di-tau level.
  This is disabled in the Higgs->mutau analysis, using the same script.

  """
  ditauch = particles['ditauch']

  if dtype in ('e_e', 'e_mu', 'mu_mu'):
    lep1  = particles['lep1']
    lep2  = particles['lep2']
    ## TOPO
    if not lep1.isFinal or not lep2.isFinal:
      return False
    if lep1.index == lep2.index:
      return False
    if lep1.charge * lep2.charge >= 0:
      return False
    ## PT
    if lep1.pT < 5 or lep2.pT < 5:
      return False 
    if lep1.pT < 20 and lep2.pT < 20:
      return False
    ## ETA
    if not ( 2.0 < lep1.eta < 4.5 ):
      return False
    if not ( 2.0 < lep2.eta < 4.5 ):
      return False
    ## MASS
    if ditau_mass_window:
      if (dtype=='e_mu'):
        if not (20 < ditauch.m < 120):
          return False
      if (dtype!='e_mu'):
        if not (20 < ditauch.m < 80):
          return False
    ## Finally
    return True

  if dtype in ('e_h1', 'h1_mu'):
    lep = particles['lep']
    had = particles['had']
    ## TOPO
    if not lep.isFinal or not had.isFinal:
      return False
    if lep.index == had.index:
      return False
    if lep.charge * had.charge >= 0:
      return False
    ## PT
    if lep.pT < 20:
      return False
    if had.pT < 10:
      return False
    ## ETA
    if not ( 2.0 < lep.eta < 4.5 ):
      return False
    if narrow_eta_hadron:
      if not ( 2.25 < had.eta < 3.75 ):
        return False
    if not narrow_eta_hadron:
      if not ( 2.0 < had.eta < 4.5 ):
        return False
    ## MASS
    if ditau_mass_window:
      if not ( 30 < ditauch.m < 120 ):
        return False
    ## Finally
    return True

  if dtype in ('e_h3', 'h3_mu'):
    lep     = particles['lep']
    pr1     = particles['pr1']
    pr2     = particles['pr2']
    pr3     = particles['pr3']
    tauh3ch = particles['tauh3ch']
    ## TOPO
    if not lep.isFinal or not pr1.isFinal or not pr2.isFinal or not pr3.isFinal:
      return False
    if len({lep.index, pr1.index, pr2.index, pr3.index})!=4:
      return False
    prcharge = pr1.charge + pr2.charge + pr3.charge
    if abs(prcharge) != 1:
      return False
    if lep.charge * prcharge >= 0:
      return False
    ## PT
    # sort again, decreasing in PT
    pr1pT, pr2pT, pr3pT = sorted([ pr1.pT, pr2.pT, pr3.pT], reverse=True)
    if lep.pT < 20:
      return False
    if pr1pT < 6:
      return False
    if pr3pT < 1:
      return False
    if tauh3ch.pT < 12:
      return False
    ## ETA
    if not ( 2.0 < lep.eta < 4.5 ):
      return False
    if narrow_eta_hadron:
      if not ( 2.25 < pr1.eta < 3.75 ):
        return False
      if not ( 2.25 < pr2.eta < 3.75 ):
        return False
      if not ( 2.25 < pr3.eta < 3.75 ):
        return False
    if not narrow_eta_hadron:
      if not ( 2.0 < pr1.eta < 4.5 ):
        return False
      if not ( 2.0 < pr2.eta < 4.5 ):
        return False
      if not ( 2.0 < pr3.eta < 4.5 ):
        return False      
    ## Mass
    if not ( 0.7 < tauh3ch.m < 1.5 ):
      return False
    if ditau_mass_window:
      if not ( 30 < ditauch.m < 120 ):
        return False
    ## Finally
    return True

#===============================================================================
# Making the arbitary candidates, for cross-feed study
#===============================================================================

def pt_filter(arr, minpt):
  """
  Filter array of particles by minpt, also sorted descending.
  """
  return sorted([ p for p in arr if p.pT>minpt ], key=lambda p: p.pT, reverse=True)


def yield_acc_candidates_e_e( event ):
  arr1 = pt_filter(event.findall( isFinal=True, id=11  ), 5 )
  arr2 = pt_filter(event.findall( isFinal=True, id=-11 ), 5 )
  for l1,l2 in itertools.product(arr1, arr2):
    ditauch = l1.p + l2.p
    yield {'lep1':l1, 'lep2':l2, 'ditauch':ditauch}

def yield_acc_candidates_mu_mu( event ):
  arr1 = pt_filter(event.findall( isFinal=True, id=13  ), 5 )
  arr2 = pt_filter(event.findall( isFinal=True, id=-13 ), 5 )
  for l1,l2 in itertools.product(arr1, arr2):
    ditauch = l1.p + l2.p
    yield {'lep1':l1, 'lep2':l2, 'ditauch':ditauch}

def yield_acc_candidates_e_mu( event ):
  arr1 = pt_filter(event.findall( isFinal=True, idAbs=11 ), 5 )
  arr2 = pt_filter(event.findall( isFinal=True, idAbs=13 ), 5 )
  for l1,l2 in itertools.product(arr1, arr2):
    ditauch = l1.p + l2.p
    yield {'lep1':l1, 'lep2':l2, 'ditauch':ditauch}

def yield_acc_candidates_lh1( event, lep_idAbs ):
  ## Raw
  arr_lep = event.findall( isFinal=True, idAbs=lep_idAbs )
  arr_had = event.findall( isFinal=True, isHadron=True, isCharged=True )
  ## PT prefilter
  arr_lep = pt_filter( arr_lep, 20 )
  arr_had = pt_filter( arr_had, 10 )
  ## Yield the queue for detailed check
  for lep,had in itertools.product(arr_lep, arr_had):
    ditauch = lep.p + had.p
    yield {'lep':lep, 'had':had, 'ditauch':ditauch}

def yield_acc_candidates_e_h1( event ):
  return yield_acc_candidates_lh1( event, 11 )
def yield_acc_candidates_h1_mu( event ):
  return yield_acc_candidates_lh1( event, 13 )

def yield_acc_candidates_lh3( event, lep_idAbs ):
  ## Raw
  arr_lep = event.findall( isFinal=True, idAbs=lep_idAbs )
  arr_had = event.findall( isFinal=True, isHadron=True, isCharged=True )
  ## PT prefilter
  arr_lep = pt_filter( arr_lep, 20 )
  arr_had = pt_filter( arr_had, 1  )  
  ## Yield the queue for detailed check
  for lep in arr_lep:
    for (pr1,pr2,pr3) in itertools.combinations(arr_had,3):
      tauh3ch = pr1.p + pr2.p + pr3.p
      ditauch = lep.p + tauh3ch
      yield {'lep':lep, 'pr1':pr1, 'pr2':pr2, 'pr3':pr3, 'tauh3ch':tauh3ch, 'ditauch':ditauch}

def yield_acc_candidates_e_h3( event ):
  return yield_acc_candidates_lh3( event, 11 )
def yield_acc_candidates_h3_mu( event ):
  return yield_acc_candidates_lh3( event, 13 )


def count_acc_candidates( product, event, ditau_mass_window, narrow_eta_hadron ):
  """
  Given the requested type of candidate, return number of candidates in acc
  which can be found in the given event.
  The candidates (via generator) does not necessarily came from Ztautau, as it's 
  designed to compute the cross-feed.
  """
  genname       = 'yield_acc_candidates_'+product
  arr_particles = globals()[genname](event)
  return sum(in_acceptance(product, particles, ditau_mass_window, narrow_eta_hadron) for particles in arr_particles)

  # ## DEBUG: 
  # for particles in arr_particles:
  #   if in_acceptance( product, particles ):
  #     for name,p in particles.iteritems():
  #       print name, p.pT, p.eta



def calc_2bodies_mass(tauh3):
  """
  Given a tauh3, return two 2-bodies mass of pi+pi-.

  - mnear, mfar: Former has closer mass to rho(770) = 775.26 MeV
  """
  print tauh3


#===============================================================================

def main(maxevt=None):

  ## Prepare Pythia
  pythia = Pythia()
  pythia.readString('Next:numberShowEvent = 0')
  # pythia.readString('15:offIfAll = 16 -211 -211 -211 211 211') # turnoff 5-prongs
  pythia.readString('Beams:frameType = 4')
  pythia.readString('Beams:LHEF = pwgevents.lhe')

  ## Condense h1mu
  pythia.readString('15:onMode = off')
  pythia.readString('15:onIfMatch = 16 13 -14')          # mu
  pythia.readString('15:onIfMatch = 16 -211 111')        # pi- pi0        25.37%
  pythia.readString('15:onIfMatch = 16 -211')            # pi-            10.77%
  pythia.readString('15:onIfMatch = 16 -211 111 111')    # pi- pi0 pi0     9.25%
  pythia.readString('15:onIfMatch = 16 -211 111 111 111')# pi- pi0x3       1.04%
  pythia.readString('15:onIfMatch = 16 -211 -311')       # pi- K0          0.84%
  pythia.readString('15:onIfMatch = 16 -321')            # K-              0.70%
  pythia.readString('15:onIfMatch = 16 -321 111')        # K- pi0          0.43%
  pythia.readString('15:onIfMatch = 16 -211 -311 111')   # pi- pi0 K0      0.40%

  # ## TRY NO FSR
  # pythia.readString('PartonLevel:FSR = off')

  ## Process-level veto (Loose LHCb Fiducial)
  veto = Veto_fiducial()  # get ownership
  pythia.setUserHooksPtr(veto)

  ## Ready, init pythia
  pythia.init()

  ## Prepare output
  fout  = root_open('tuple.root', 'recreate')

  # ## Making empty trees
  ## Disable if there's no need to collect full event
  # trees = dict()
  # for dtype,branches in all_branches.iteritems():
  #   spec = OrderedDict(sorted(branches.items()))
  #   tree = Tree(dtype)
  #   tree.create_branches(spec)
  #   trees[dtype] = tree


  ## 2. For leak-in study
  hists_leakin = {key:{} for key in fill_modes}  
  for prefix, hists in hists_leakin.iteritems():
    for dtype in all_branches:
      name = prefix+'_hist_'+dtype
      h = ROOT.TH2I(name, name, 2,0,2, 2,0,2)
      h.xaxis.title = 'Fiducial'
      h.yaxis.title = 'Acceptance'
      hists[dtype] = h


  ## 3. For contamination study. 
  # Note: Indexed by the dtype of candidate, NOT by channel of origin.
  hists_contam = {key:{} for key in fill_modes}
  for prefix, hists in hists_contam.iteritems():
    name1 = prefix+'_contamination_ent'
    name2 = prefix+'_contamination_evt'
    h1    = ROOT.TH2I( name1, name1, 10,0,10, 10,0,10 )
    h2    = ROOT.TH2I( name2, name2, 10,0,10, 10,0,10 )
    h1.xaxis.title = 'Product'
    h1.yaxis.title = 'Origin'
    h2.xaxis.title = 'Product'
    h2.yaxis.title = 'Origin'
    hists['ent'] = h1
    hists['evt'] = h2
    # init the array
    for h in [h1,h2]:
      for dt1,dt2 in itertools.product(DTYPES,DTYPES):
        h.Fill( dt1, dt2, 0 )


  ## Ready to loop!
  i = 0
  while pythia.next():
    i+=1
    if i%1000==1:
      print 'Processing event: ', i
    ## optionally break if reaches max event.
    if maxevt is not None and i>=maxevt:
      break

    ## Get hard event particles
    Z = pythia.event.find(id=23).topCopyId()
    dtype       = ditautype(Z)
    Ztau_infid  = in_fiducial(Z) # calculate once!

    # ## DEBUG: I suspect that good tauh3 will be contamination for tauh1
    # if dtype!='h3_mu':
    #   continue
    # if in_fiducial(Z):
    #   particles = identify( dtype, Z )
    #   if in_acceptance( dtype, particles ):
    #     print '---'
    #     for key,p in sorted(identify( dtype, Z ).iteritems()):
    #       print key, 'PT=',p.pT, 'ETA=',p.eta


    ## Study#3:: Contamination from different channel
    # Only account for those from Z already inside fiducial
    # Loop over the product type, which can come from any channel of origin.
    # if Ztau_infid:
    for fmode, kwargs in fill_modes.iteritems():
      for product in all_particles:
        nent = count_acc_candidates( product, pythia.event, **kwargs )
        nevt = int(nent>0) # 0-or-1
        hists_contam[fmode]['ent'].Fill( product, dtype, nent )
        hists_contam[fmode]['evt'].Fill( product, dtype, nent )

    ## Now, skip the unused channel
    if dtype not in all_particles:
      continue

    ## Identify the list of particles of interest into struct.
    particles = identify( dtype, Z )

    ## Study#2:: Collect digested result on fid+acc calculation
    ## Note: Need a fixed value of cuts
    for fmode, kwargs in fill_modes.iteritems():
      Ztau_inacc = in_acceptance( dtype, particles, **kwargs )
      hists_leakin[fmode][dtype].Fill( int(Ztau_infid), int(Ztau_inacc) )

    # ## Study#1:: Fill the output!
    # tree = trees[dtype]
    # for pname,part in particles.iteritems():
    #   for attr in attrs:
    #     key = pname+'_'+attr.upper()
    #     val = getattr( part, attr )
    #     setattr( tree, key, val )
    # tree.fill()


  ## Closing
  # for tree in trees.values():
  #   tree.write()
  for hists0 in [hists_leakin, hists_contam]:
    for hists in hists0.values():
      for h in hists.values():
        h.Write()


  ## Finally
  fout.close()
  pythia.stat()

#===============================================================================

if __name__ == '__main__':
  pass

  main()
