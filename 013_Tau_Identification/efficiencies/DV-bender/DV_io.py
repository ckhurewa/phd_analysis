
#--- IOHelper

from GaudiConf import IOHelper
from glob import glob
IOHelper().inputFiles(

  ## sample S20.EW
  # [
  #   '/panfs/khurewat/sample_Data2012_S20/00020198_00000100_1.ew.dst',
  #   'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/LHCb/Collision12/EW.DST/00020456/0000/00020456_00000067_1.ew.dst',
  # ]

  ## S21.PID.MDST ( for low-PT muon ePID by Jpsi )
  # ['root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/PID.MDST/00041836/0000/00041836_00000289_1.pid.mdst',]

  ## S21.MINIBIAS.DST ( for hadron ePID )
  # ['root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/MINIBIAS.DST/00041836/0000/00041836_00004299_1.minibias.dst',]
  ## S20.MINIBIAS.DST ( for hadron ePID )
  # ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00000141_1.minibias.dst']

  ## S20r1.MINIBIAS.DST ( 7 TeV! )
  # [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision11/MINIBIAS.DST/00022727/0000/00022727_00004515_1.minibias.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision11/MINIBIAS.DST/00022727/0000/00022727_00004008_1.minibias.dst',
  # ]

  ## S20.PID
  [
    'root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000077_1.pid.mdst',
    'root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000090_1.pid.mdst',
    'root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000103_1.pid.mdst',
    'root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000116_1.pid.mdst',
    'root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000129_1.pid.mdst',
    # 'file:///storage/gpfs_lhcb/lhcb/disk/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000011_1.pid.mdst',
    # 'file:///storage/gpfs_lhcb/lhcb/disk/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000024_1.pid.mdst',
    # 'file:///storage/gpfs_lhcb/lhcb/disk/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000037_1.pid.mdst',
    # 'file:///storage/gpfs_lhcb/lhcb/disk/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000050_1.pid.mdst',
    # 'file:///storage/gpfs_lhcb/lhcb/disk/LHCb/Collision12/PID.MDST/00020198/0000/00020198_00000063_1.pid.mdst',
  ]

  #----#
  # MC #
  #----#

  ## S20 MCMB
  # ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00026814/0000/00026814_00000001_1.allstreams.dst']
)
