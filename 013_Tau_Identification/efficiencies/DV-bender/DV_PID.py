from Configurables import DaVinci
from Configurables import CombineParticles
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__EvtTupleTool
from Configurables import TupleToolConeIsolation
from DecayTreeTuple.Configuration import DecayTreeTuple
from PhysConf.Filters import LoKi_Filters


preambulo = """
from LoKiPhys.decorators import *
from LoKiCore.functions import *
from LoKiProtoParticles.decorators import *

TOS_MUON0 = TOS('L0MuonDecision', 'L0TriggerTisTos' )
TOS_MUON1 = TOS('Hlt1SingleMuonHighPTDecision', 'Hlt1TriggerTisTos' )
TOS_MUON2 = TOS('Hlt2SingleMuonHighPTDecision', 'Hlt2TriggerTisTos' )
TOS_MUON  = TOS_MUON0 & TOS_MUON1 & TOS_MUON2

TOS_ELEC0 = TOS('L0ElectronDecision', 'L0TriggerTisTos' )
TOS_ELEC1 = TOS('Hlt1SingleElectronNoIPDecision', 'Hlt1TriggerTisTos' )
TOS_ELEC2 = TOS('Hlt2SingleTFVHighPtElectronDecision', 'Hlt2TriggerTisTos' )
TOS_ELEC  = TOS_ELEC0 & TOS_ELEC1 & TOS_ELEC2

ISHADRON    = (~ISLOOSEMUON) & (PPFUN(PP_CaloHcalE, -1)/P > 0.05) 
ISELECTRON  = (~ISLOOSEMUON) & (PPFUN(PP_CaloHcalE, -1)/P < 0.05) & (PPFUN(PP_CaloEcalE,-1)/P>0.1) & (PPFUN(PP_CaloPrsE, -1)>50)

def _ChildrenDPHI(i1,i2):
  from LoKiPhys.decorators import CHILD,PHI  
  from LoKiCore.math import cos,acos
  DPHI = abs(CHILD(PHI,i1)-CHILD(PHI,i2))
  DPHI = acos(cos(DPHI))  # mod pi       
  return DPHI
def _ChildrenAPT(i1,i2):
  from LoKiPhys.decorators import CHILD, PT
  pt1 = CHILD(PT,i1)
  pt2 = CHILD(PT,i2)
  return abs(pt1-pt2)/(pt1+pt2)

DPHI12 = _ChildrenDPHI(1,2)
APT    = _ChildrenAPT(1,2)
""".split('\n')


#===============================================================================

def branching_tag_probe( tup, branches ):
  tup.ToolList = []
  for bname, br in branches.iteritems():
    tool = br.addTupleTool( LoKi__Hybrid__TupleTool, 'ttloki_'+bname )
    tool.Preambulo = preambulo
    if bname == 'Z0':
      tool.Variables = {
        'M'         : 'M',
        'P'         : 'P',
        'PT'        : 'PT',
        'PZ'        : 'PZ',
        'ETA'       : 'ETA',
        'PHI'       : 'PHI',
        'DPHI'      : 'DPHI12',
        'APT'       : 'APT',
        'DOCA'      : 'PFUNA(ADOCAMIN(""))',
        'DOCACHI2'  : 'PFUNA(ADOCACHI2(""))',
        'VCHI2PDOF' : 'VFASPF(VCHI2/VDOF)',
        'BPVVDCHI2' : 'BPVVDCHI2',
      }
    elif bname in ('tag','probe'):
      tool.Variables = {
        'P'          : 'P',
        'PT'         : 'PT',
        'ETA'        : 'ETA',
        'PHI'        : 'PHI',
        'PERR2'      : 'PERR2',
        'TRPCHI2'    : 'TRPCHI2',
        'TRGHP'      : 'TRGHP',
        'ISMUON'     : 'switch( ISMUON     , 1, 0)',
        'ISMUONLOOSE': 'switch( ISMUONLOOSE, 1, 0)',
        'ISHADRON'   : 'switch( ISHADRON   , 1, 0)',
        'ISELECTRON' : 'switch( ISELECTRON , 1, 0)',
        'TOS_MUON'   : 'switch( TOS_MUON   , 1, 0)',
        'TOS_ELEC'   : 'switch( TOS_ELEC   , 1, 0)',
        'CaloHcalE'  : 'PPFUN(PP_CaloHcalE, -1)',
        'CaloEcalE'  : 'PPFUN(PP_CaloEcalE, -1)',
        'CaloPrsE'   : 'PPFUN(PP_CaloPrsE , -1)',
        'BPVIP'      : 'BPVIP()',
      }
      #
      tool = br.addTupleTool( TupleToolConeIsolation, bname )
      tool.MinConeSize = 0.5
      tool.MaxConeSize = 0.5
      tool.OutputLevel = 10
  tool = tup.addTupleTool( LoKi__Hybrid__EvtTupleTool, 'ttlokievt' )
  tool.VOID_Variables = {
    'nPVs'    : 'RECSUMMARY(  0, -1)',
    'nTracks' : 'RECSUMMARY( 16, -1)',
    'nSPDhits': 'RECSUMMARY( 70, -1)',
  }


#===============================================================================
# PARTICLE IDENTIFICATION
#===============================================================================

## TAG
dcuts_mu = '(%s)'%')&('.join([
  # Kine
  'PT > 15*GeV', # slightly lower, tighten later
  'in_range( 2.0, ETA, 4.5 )',
  # Trigger
  'TOS_MUON',
  # Tracks
  'TRPCHI2 > 0.01',
  # PID
  'ISMUON',
])

## TAG
dcuts_e = '(%s)'%')&('.join([
  # Kine
  'PT > 15*GeV', # slightly lower, tighten later
  'in_range( 2.0, ETA, 4.5 )',
  # Trigger
  'TOS_ELEC',
  # Track
  'TRPCHI2 > 0.01',
  # PID
  'ISELECTRON',
])

## PROBE
dcuts_pi = '(%s)'%')&('.join([
  # Kine
  'PT > 1*GeV',
  'in_range( 2.0, ETA, 4.5 )',
  # Tracks
  'TRPCHI2 > 0.01',
])

#===============================================================================

def seq_muon():
  algo = CombineParticles('CombPID_Z02MuMu')
  algo.DaughtersCuts    = { 'mu-': dcuts_mu, 'mu+': dcuts_mu, 'pi-': dcuts_pi, 'pi+': dcuts_pi }
  algo.DecayDescriptor  = '[Z0 -> mu- pi+]cc'
  algo.MotherCut        = 'in_range( 60*GeV, M, 120*GeV )'
  algo.Preambulo        = preambulo
  algo.Inputs = [ 'Phys/StdAllNoPIDsPions/Particles', 'Phys/StdAllLooseMuons/Particles' ]

  tup = DecayTreeTuple( 'PID_muon', TupleName='Z02MuMu', NTupleDir='PID' )
  tup.Decay   = '[Z0 -> ^mu- ^pi+]CC'
  tup.Inputs  = [ 'Phys/%s/Particles'%algo.name() ]
  branches    = tup.addBranches({
    'Z0'    : '^X0',
    'tag'   : '[Z0 -> ^mu-  pi+]CC',
    'probe' : '[Z0 ->  mu- ^pi+]CC',
  })
  branching_tag_probe( tup, branches )

  seq = LoKi_Filters(
    Preambulo   = preambulo,
    STRIP_Code  = "HLT_PASS('StrippingWMuLineDecision')",
  ).sequencer('PID_Z02MuMu')
  seq.Members += [ algo, tup ]
  return seq

#-------------------------------------------------------------------------------

def seq_electron():
  algo = CombineParticles('CombPID_Z02ee')
  algo.DaughtersCuts    = { 'e-': dcuts_e, 'e+': dcuts_e, 'pi-': dcuts_pi, 'pi+': dcuts_pi }
  algo.DecayDescriptor  = '[Z0 -> e- pi+]cc'
  algo.MotherCut        = 'in_range( 60*GeV, M, 120*GeV )'
  algo.Preambulo        = preambulo
  algo.Inputs = [ 'Phys/StdAllNoPIDsPions/Particles', 'Phys/StdAllLooseElectrons/Particles' ]

  tup = DecayTreeTuple( 'PID_elec', TupleName='Z02ee', NTupleDir='PID' )
  tup.Decay   = '[Z0 -> ^e- ^pi+]CC'
  tup.Inputs  = [ 'Phys/%s/Particles'%algo.name() ]
  branches    = tup.addBranches({
    'Z0'    : '^X0',
    'tag'   : '[Z0 -> ^e-  pi+]CC',
    'probe' : '[Z0 ->  e- ^pi+]CC',
  })
  branching_tag_probe( tup, branches )

  seq = LoKi_Filters(
    Preambulo   = preambulo,
    STRIP_Code  = "HLT_PASS('StrippingWeLineDecision')",
  ).sequencer('PID_Z02ee')
  seq.Members += [ algo, tup ]
  return seq


#-------------------------------------------------------------------------------

DaVinci().UserAlgorithms += [ seq_muon(), seq_electron() ]

#-------------------------------------------------------------------------------



