
# from PyrootCK.QHistUtils import join

from PhysConf.Filters import LoKi_Filters
from Configurables import DaVinci
from Configurables import EventTuple, TupleToolTrigger
from Configurables import LoKi__Hybrid__EvtTupleTool
from DecayTreeTuple.Configuration import DecayTreeTuple

def join(*args):
  return ' & '.join('(%s)'%x for x in args)

preambulo = """
from LoKiPhys.decorators import *
from LoKiCore.functions import *
from LoKiNumbers.functions import RECSUMMARY
""".split('\n')


#===============================================================================
# GLOBAL EVENT CUT
#===============================================================================

cuts = join(
  'in_range( 60*GeV, M, 120*GeV )',
  'CHILDCUT( PT>20*GeV, 1 )',
  'CHILDCUT( PT>20*GeV, 2 )',
)

def GecTuple( name, tes ):
  tup = EventTuple( 'EventTupleGEC_'+name, TupleName=name, NTupleDir='GEC' )
  tup.ToolList = [ 
    'TupleToolTrigger',
    'LoKi__Hybrid__EvtTupleTool/tt',
  ]
  tup.addTool( TupleToolTrigger )
  tup.TupleToolTrigger.FillHlt1   = False
  tup.TupleToolTrigger.FillHlt2   = False
  tup.TupleToolTrigger.VerboseL0  = True
  tup.TupleToolTrigger.TriggerList = [
    'L0ElectronDecision',
    'L0MuonDecision',
    'L0DiMuonDecision',
  ]
  tup.addTool( LoKi__Hybrid__EvtTupleTool, 'tt' )
  tup.tt.VOID_Variables = { 'nSPDHits': 'RECSUMMARY(70, -1)' } # 70 is LHCb.RecSummary.nSPDhits
  #
  tup2 = DecayTreeTuple( 'GECDTT_'+name, TupleName=name, NTupleDir='GEC/DTT' )
  tup2.Decay  = 'X0'
  tup2.Inputs = [ tes+'/Particles' ]  # Strictly Z02MuMu or Z02ee

  return [ tup, tup2 ]


#--------------------------------------

queue = {
  'Z02MuMu' : 'EW/Phys/Z02MuMuLine',
  'Z02ee'   : 'EW/Phys/Z02eeLine',
}

for zname, tes in queue.iteritems():

  seq = LoKi_Filters(
    Preambulo   = preambulo,
    STRIP_Code  = "HLT_PASS('Stripping%sLineDecision')" % zname,
    VOID_Code   = "SOURCE( '%s', ( %s )) >> ( SIZE >= 1 )" % (tes,cuts),
  ).sequence('GEC_'+zname)

  seq.Members += GecTuple( zname, tes )
  DaVinci().UserAlgorithms += [ seq ]

