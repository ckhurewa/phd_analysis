from BenderCK import *
from BenderCK.Tau import *

#==============================================================================

class OldHadronPIDAlgo(AlgoMC):
  """
  Check in MCMB 30000000 whether the (highest-PT) long track is hadron or not.
  """

  CUT = (ISLONG & (TRPCHI2>0.01) & (TRGHP<0.2) & (ETA>2.25) & (ETA<3.75) & (PT>1*GeV))

  @property
  def tt_eventinfo(self):
    return self.tool('IEventTupleTool', 'TupleToolEventInfo')

  def find_mcp_all(self, reco):
    for mcp in self.get('MC/Particles'):
      # print mcp
      matcher = MCTRUTH( self.mcTruth(), mcp )
      if matcher(reco):
        yield mcp

  def fill(self, tname, reco):
    with self.enhancedTuple(tname) as tup:
      tup.column('TRPCHI2'    , TRPCHI2(reco))
      tup.column('ETA'        , ETA(reco))
      tup.column('PHI'        , PHI(reco))
      tup.column('PT'         , PT(reco))
      tup.column('P'          , P(reco))
      tup.column('CaloHcalE'  , (PPFUN(PP_CaloHcalE))(reco))
      tup.column('HCALFrac'   , (PPFUN(PP_CaloHcalE)/P)(reco))
      tup.column('ISLOOSEMUON', ISLOOSEMUON(reco))
      #
      tup.column('TRGHP'      , TRGHP(reco))
      tup.column('TRCHI2DOF'  , TRCHI2DOF(reco))
      tup.column('PROBNNghost', PROBNNghost(reco))
      #
      self.tool('IEventTupleTool'   , 'TupleToolEventInfo').fill( tup )
      self.tool('IEventTupleTool'   , 'TupleToolRecoStats').fill( tup )
      self.tool('IParticleTupleTool', 'TupleToolPid').fill( None, reco, 'pi', tup )

  # def analyse_alllongtracks(self):
  #   # print self.event_break
  #   cut = (PT>5E3) & ISLONG & (TRPCHI2>0.01) & (ETA>2.0) & (ETA<4.5)
  #   l   = SOURCE('Phys/StdAllNoPIDsPions', cut )()
  #   for reco in l:
  #     # mcps = [ MCID(mcp) for mcp in self.find_mcp_all(reco) ]
  #     # print 'PT=%.2f'%(PT(reco)/1000), 'ETA=%.2f'%ETA(reco), (PPFUN(PP_CaloHcalE)/P>0.05)(reco), mcps
  #     self.fill( 'alllongtracks', reco )


  def analyse_hardest_cutlater(self):
    l = (SOURCE('Phys/StdAllNoPIDsPions') >> max_element(PT))()
    if len(l)==0:
      return
    p = l[0]
    if not self.CUT(p):
      return
    self.fill( 'hardest_cutlater', p )

  def analyse_hardest_cutearly(self):
    l   = (SOURCE('Phys/StdAllNoPIDsPions', self.CUT ) >> max_element(PT))()
    if len(l)==0:
      return
    p = l[0]
    self.fill( 'hardest_cutearly', p )


  @pass_success
  def analyse(self):
    # self.analyse_alllongtracks()
    self.analyse_hardest_cutearly()
    self.analyse_hardest_cutlater()


#===============================================================================

class LeptonMisidCheck(AlgoMC):
  """
  Aim to get the muon-misid-as-e/h rate in MC for Ztautau analysis.
  This should supercede the one using just Ztautau sample which has low stats.
  """

  FL_MCP  = 'MCID', 'MCP', 'MCPT', 'MCPZ', 'MCETA', 'MCPHI', 'MC3Q'
  FL_RECO = 'P', 'PT', 'PZ', 'ETA', 'PHI', 'Q', 'TRGHP', 'TRPCHI2', 'PERR2'
  FD_RECO = {
    'BPVIP'       : BPVIP(),
    'ISMUON'      : switch(ISMUON     , 1, 0),
    'ISLOOSEMUON' : switch(ISLOOSEMUON, 1, 0),
  }

  ## Configure before appMgr
  from Configurables import TupleToolConeIsolation
  conf = TupleToolConeIsolation('LeptonMisidCheck.TupleToolConeIsolation')
  conf.MinConeSize = 0.5
  conf.MaxConeSize = 0.5
  conf.OutputLevel = 10

  def TOS_MUON( self, p ):
    t0 = TOS('L0MuonDecision', 'L0TriggerTisTos' )
    t1 = TOS('Hlt1SingleMuonHighPTDecision', 'Hlt1TriggerTisTos' )
    t2 = TOS('Hlt2SingleMuonHighPTDecision', 'Hlt2TriggerTisTos' )
    return int((t0 & t1 & t2)(p))

  def TOS_ELEC(self, p):
    t0 = TOS('L0ElectronDecision','L0TriggerTisTos')
    t1 = TOS('Hlt1SingleElectronNoIPDecision','Hlt1TriggerTisTos')
    t2 = TOS('Hlt2SingleTFVHighPtElectronDecision','Hlt2TriggerTisTos')
    return int((t0 & t1 & t2)(p))

  @property
  def tt_cone_isolation(self):
    return self.tool('IParticleTupleTool', 'TupleToolConeIsolation')

  def find_reco_best( self, tes, mcp ):
    recos = list(self.find_reco_all( tes, mcp ))
    return None if not recos else sorted(recos, key=-PT)[0]


  def fill_biased( self, name, mcp, reco ):
    with self.enhancedTuple(name+'_biased') as tup:
      tup.column_uint('nPVs'     , self.nPVs     )
      tup.column_uint('nTracks'  , self.nTracks  )
      tup.column_uint('nSPDhits' , self.nSPDhits )
      #
      tup.column_uint('TOS_MUON' , self.TOS_MUON(reco))
      tup.column_uint('TOS_ELEC' , self.TOS_ELEC(reco))
      #
      tup.fill_funclist( self.FL_MCP      , mcp  )
      tup.fill_funclist( self.FL_RECO     , reco )
      tup.fill_funcdict( self.FD_RECO     , reco )
      tup.fill_funcdict( Functors.FD_PROTO, reco )
      #
      self.tt_cone_isolation.fill( reco, reco, 'lep', tup )

  def fill_unbiased( self, name, tag, probe ):
    v4mom = tag.momentum() + probe.momentum()
    with self.enhancedTuple(name+'_unbiased') as tup:
      tup.column_uint('nPVs'     , self.nPVs     )
      tup.column_uint('nTracks'  , self.nTracks  )
      tup.column_uint('nSPDhits' , self.nSPDhits )
      #
      tup.column_float( 'Z0_M', v4mom.M())
      tup.column_float( 'DPHI', abs(DPHI(tag)(probe)))
      #
      tup.column_uint('tag_TOS_MUON'   , self.TOS_MUON(tag))
      tup.column_uint('tag_TOS_ELEC'   , self.TOS_ELEC(tag))
      tup.column_uint('probe_TOS_MUON' , self.TOS_MUON(probe))
      tup.column_uint('probe_TOS_ELEC' , self.TOS_ELEC(probe))
      #
      tup.fill_funclist( self.FL_RECO     , tag  , prefix='tag'   )
      tup.fill_funcdict( self.FD_RECO     , tag  , prefix='tag'   )
      tup.fill_funcdict( Functors.FD_PROTO, tag  , prefix='tag'   )
      tup.fill_funclist( self.FL_RECO     , probe, prefix='probe' )
      tup.fill_funcdict( self.FD_RECO     , probe, prefix='probe' )
      tup.fill_funcdict( Functors.FD_PROTO, probe, prefix='probe' )
      #
      self.tt_cone_isolation.fill( tag  , tag  , 'tag'  , tup )
      self.tt_cone_isolation.fill( probe, probe, 'probe', tup )

  @pass_success
  def analyse(self):

    ## static
    recsummary    = self.get('Rec/Summary')
    self.nPVs     = recsummary.info(LHCb.RecSummary.nPVs     , -1)
    self.nTracks  = recsummary.info(LHCb.RecSummary.nTracks  , -1)
    self.nSPDhits = recsummary.info(LHCb.RecSummary.nSPDhits , -1)

    queues = [
      ('Z02mumu', 'Z0 -> mu- mu+', 'Phys/StdAllNoPIDsMuons'    ),
      ('Z02ee'  , 'Z0 -> e- e+'  , 'Phys/StdAllNoPIDsElectrons'),
    ]

    print self.event_break
    print self.mcselect('Z0', MCABSID==23)
    return

    for name, decay, tes in queues:
      for Z0 in self.mcselect(name, decay):
        mcp1, mcp2 = Z0.children()
        reco1 = self.find_reco_best( tes, mcp1 )
        reco2 = self.find_reco_best( tes, mcp2 )
        ## Part 1: Biased study
        for mcp,reco in [(mcp1,reco1), (mcp2,reco2)]:
          if reco is not None:
            self.fill_biased( name, mcp, reco )
        ## Part2: Unbiased study, do tag & probe
        if reco1 is None or reco2 is None:
          continue
        self.fill_unbiased( name, tag=reco1, probe=reco2 )
        self.fill_unbiased( name, tag=reco2, probe=reco1 )

#===============================================================================
