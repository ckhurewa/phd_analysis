"""

Study charged hadron PID using minimumbias.

Need MINIBIAS.DST dataset, line `?`

"""

from Configurables import DaVinci
from Configurables import FilterDesktop
from PhysConf.Filters import LoKi_Filters
from DecayTreeTuple.Configuration import DecayTreeTuple, EventTuple

preambulo = """
from LoKiCore.functions import *
from LoKiPhys.decorators import *
from LoKiProtoParticles.functions import *

# GoodPi = (PT>5E3)
# GoodPi = (PT>5E3) & (TRPCHI2>0.01) & (ISLONG)
# GoodPi = (PT>5E3) & (ETA>2.25) & (ETA<3.75) & (TRPCHI2>0.01) & (ISLONG)
GoodPi = (PT>1E3) & (ETA>2.25) & (ETA<3.75) & (TRPCHI2>0.01) & (ISLONG) & (TRGHP<0.2) & (PROBNNghost<0.4)
""".split('\n')


column = "SOURCE('Phys/StdAllNoPIDsPions',GoodPi) >> max_element(PT) >> max_value({})".format

basics    = 'P','PT','ETA','PHI','PERR2','TRPCHI2','TRGHP','TRCHI2DOF','PROBNNghost'
variables = {x:x for x in basics}
variables.update({
  'ISMUONLOOSE': 'switch( ISMUONLOOSE, 1, 0)',
  'ISLONG'     : 'switch( ISLONG     , 1, 0)',
  'CaloHcalE'  : 'PPFUN(PP_CaloHcalE)',
  'HCALFrac'   : 'PPFUN(PP_CaloHcalE)/P',
})

def add_tt_trigger(tup):
  tool = tup.addTupleTool('TupleToolTrigger')
  tool.VerboseL0    = True
  tool.VerboseHlt1  = True
  tool.TriggerList  = [
    'L0HadronDecision',
    'L0HadronNoSPDDecision',
    'L0MuonDecision',
    'L0MUON,minbiasDecision',
    'Hlt1L0AnyNoSPDRateLimitedDecision',
    'Hlt1L0AnyNoSPDDecision',
    'Hlt1L0AnyRateLimitedDecision',
    'Hlt1L0AnyDecision',
    'Hlt1MBNoBiasDecision',
    'Hlt1MBMicroBiasVeloDecision',
    'Hlt1VeloClosingMicroBiasDecision',
    'Hlt1TrackAllL0Decision',
  ]
  return tup

def add_tt_stripping(tup):
  tool = tup.addTupleTool('TupleToolStripping')
  tool.StrippingList = [
    'StrippingHlt1L0AnyPrescaledDecision',
    'StrippingHlt1L0AnyRateLimitedDecision',
    'StrippingMBNoBiasDecision',
  ]

#===============================================================================

def EvtTup():
  tup = EventTuple( 'evttup_PID_had', TupleName='had' )
  tup.ToolList += [
    'TupleToolRecoStats',
  ]
  tool = tup.addTupleTool('LoKi::Hybrid::EvtTupleTool')
  tool.Preambulo      = preambulo
  tool.VOID_Variables = { key:column(val) for key,val in variables.iteritems() }
  #
  add_tt_trigger(tup)
  add_tt_stripping(tup)
  return tup

filt = FilterDesktop('filt_had')
filt.Inputs = [ 'Phys/StdAllNoPIDsPions/Particles' ]
filt.Code   = 'GoodPi'

def PartTup():
  tup = DecayTreeTuple('parttup_PID_had', TupleName='had')
  tup.Inputs = [ 'Phys/filt_had/Particles' ]
  tup.Decay  = 'X'
  tup.ToolList = [
    'TupleToolRecoStats',
    'TupleToolEventInfo',
  ]
  tool = tup.addTupleTool('LoKi::Hybrid::TupleTool')
  tool.Preambulo = preambulo
  tool.Variables = variables
  tool = tup.addTupleTool('TupleToolMCTruth')
  #
  add_tt_trigger(tup)
  add_tt_stripping(tup)
  return tup

#===============================================================================

seq = LoKi_Filters(
  Preambulo   = preambulo,
  # HLT_Code    = "HLT_PASS_RE('Hlt1.*Velo.*')",
  # STRIP_Code  = "HLT_PASS('StrippingHlt1L0AnyPrescaledDecision')",
  # STRIP_Code  = "HLT_PASS('StrippingMBNoBiasDecision')",
  # STRIP_Code  = "HLT_PASS('StrippingMBNoBiasDecision')|HLT_PASS('StrippingHlt1L0AnyPrescaledDecision')",
  VOID_Code   = "SOURCE('Phys/StdAllNoPIDsPions', GoodPi ) >> (SIZE>=1)",
).sequencer('seq_had')

# seq.Members += [ EvtTup() ]
seq.Members += [ EvtTup(), filt, PartTup() ]
DaVinci().UserAlgorithms += [ seq ]
