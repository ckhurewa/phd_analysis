"""
Attempt for data-driven trigger eff.
"""

from PhysConf.Filters import LoKi_Filters
from Configurables import DaVinci
from Configurables import TupleToolTISTOS, LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import DecayTreeTuple

preambulo = """
from LoKiPhys.decorators import *
from LoKiCore.functions import *
from LoKiProtoParticles.decorators import *

TOS_MUON0 = TOS('L0MuonDecision', 'L0TriggerTisTos' )
TOS_MUON1 = TOS('Hlt1SingleMuonHighPTDecision', 'Hlt1TriggerTisTos' )
TOS_MUON2 = TOS('Hlt2SingleMuonHighPTDecision', 'Hlt2TriggerTisTos' )
TOS_MUON  = TOS_MUON0 & TOS_MUON1 & TOS_MUON2

TOS_ELEC0 = TOS('L0ElectronDecision', 'L0TriggerTisTos' )
TOS_ELEC1 = TOS('Hlt1SingleElectronNoIPDecision', 'Hlt1TriggerTisTos' )
TOS_ELEC2 = TOS('Hlt2SingleTFVHighPtElectronDecision', 'Hlt2TriggerTisTos' )
TOS_ELEC  = TOS_ELEC0 & TOS_ELEC1 & TOS_ELEC2
"""

#===============================================================================
# TRIGGER
# - Use tag-and-probe on Z02ll
# - Tag  : Trigger, Track, ID
# - Probe:    --- , Track, ID
#===============================================================================

cuts = join(
  'in_range( 60*GeV, M, 120*GeV )',
  'CHILDCUT( PT>10*GeV, 1 )',
  'CHILDCUT( PT>10*GeV, 2 )',
)

tup = DecayTreeTuple( 'Trig_muon', TupleName='Z02MuMu', NTupleDir='Trigger' )
tup.Decay   = 'Z0 -> ^mu+ ^mu-'
tup.Inputs  = [ 'EW/Phys/Z02MuMuLine/Particles' ]
tool = tup.addTupleTool( TupleToolTISTOS, name='tistos_mu' )
tool.TriggerList = [  
  'L0MuonDecision',
  'L0MuonNoSPDDecision',
  #
  'Hlt1SingleMuonNoIPDecision',
  'Hlt1SingleMuonHighPTDecision',
  #
  'Hlt2SingleMuonDecision',  # ps 0.5
  'Hlt2SingleMuonHighPTDecision',
]
tool.VerboseL0 = True
tool.VerboseHlt1 = True
tool.VerboseHlt2 = True
tool = tup.addTupleTool( LoKi__Hybrid__TupleTool, name='tt_trig_muon' )
tool.Preambulo = preambulo
tool.Variables = {
  'TOS_MUON': 'switch( TOS_MUON, 1, 0)',
}

seq = LoKi_Filters(
  Preambulo   = preambulo,
  STRIP_Code  = "HLT_PASS('StrippingZ02MuMuLineDecision')",
  VOID_Code   = "SOURCE( 'EW/Phys/Z02MuMuLine', ( %s )) >> ( SIZE >= 1 )" % cuts,
).sequencer('Trig_Z02MuMu')

seq.Members += [ tup ]
DaVinci().UserAlgorithms += [ seq ]

#--------------------------------------

tup = DecayTreeTuple( 'Trig_electron', TupleName='Z02ee', NTupleDir='Trigger' )
tup.Decay   = 'Z0 -> ^e+ ^e-'
tup.Inputs  = [ 'EW/Phys/Z02eeLine/Particles' ]
tool = tup.addTupleTool( TupleToolTISTOS, name='tistos_e' )
tool.TriggerList = [  
  'L0ElectronDecision',
  #
  'Hlt1SingleElectronNoIPDecision',
  #
  'Hlt2SingleTFElectronDecision',
  'Hlt2SingleTFVHighPtElectronDecision',
  'Hlt2SingleElectronTFHighPtDecision',
  'Hlt2SingleElectronTFLowPtDecision',
]
tool.VerboseL0 = True
tool.VerboseHlt1 = True
tool.VerboseHlt2 = True
tool = tup.addTupleTool( LoKi__Hybrid__TupleTool, name='tt_trig_electron' )
tool.Preambulo = preambulo
tool.Variables = {
  'TOS_ELEC': 'switch( TOS_ELEC, 1, 0)',
}

seq = LoKi_Filters(
  Preambulo   = preambulo,
  STRIP_Code  = "HLT_PASS('StrippingZ02eeLineDecision')",
  VOID_Code   = "SOURCE( 'EW/Phys/Z02eeLine', ( %s )) >> ( SIZE >= 1 )" % cuts,
).sequencer('Trig_Z02ee')

seq.Members += [ tup ]
DaVinci().UserAlgorithms += [ seq ]
