
"""

Taken from ~sfarry/public/forWGuys/MuonTracking.TestStripping.2011.py

Locations
---------
Tracks   : Rec/Track/MuonTT
ProtoP   : Rec/ProtoP/MuonTT
Particles: Phys/MuonTT/Particles

...

Usage:
  >> from DV_MuonTT import seq
  >> DaVinci....

"""

__all__ = ('seq')

from Configurables import GaudiSequencer
seq = GaudiSequencer("DV_MuonTT")

from Configurables import MuonTTTrack, MuonNNetRec, PatAddTTCoord, TrackMasterFitter, MuonCombRec, TrackMasterExtrapolator

"""
Make a muonTT track out of hits in the muon station and TT, 
and give it some options to configure.
Use Michel's Options in Stripping, with his parameters
"""

XTol        = 25.0
MaxChi2Tol  = 7.0
MinAxProj   = 5.5
MajAxProj   = 25.0

algo = MuonTTTrack("MuonTTTrack")
algo.ToolName = "MuonCombRec"
algo.addTool( MuonCombRec )
algo.MuonCombRec.MeasureTime    = True
algo.MuonCombRec.CloneKiller    = False
algo.MuonCombRec.SkipStation    = -1 # -1=no skip, 0=M1, 1=M2, 2=M3, 3=M4, 4=M5
algo.MuonCombRec.DecodingTool   = "MuonHitDecode"
algo.MuonCombRec.PadRecTool     = "MuonPadRec"
algo.MuonCombRec.ClusterTool    = "MuonClusterRec" # to enable: "MuonClusterRec"
algo.MuonCombRec.PhysicsTiming  = True
algo.MuonCombRec.AssumeCosmics  = False
algo.MuonCombRec.AssumePhysics  = True
algo.MuonCombRec.StrongCloneKiller = False
algo.MuonCombRec.SeedStation = 4 # default seed station is 2 - M3, better is 4 - M5
# #############################################################
algo.addTool( PatAddTTCoord )
algo.PatAddTTCoord.YTolSlope  = 400000.0
algo.PatAddTTCoord.XTol       = XTol
algo.PatAddTTCoord.XTolSlope  = 400000.0
algo.PatAddTTCoord.MinAxProj  = MinAxProj
algo.PatAddTTCoord.MajAxProj  = MajAxProj
algo.PatAddTTCoord.MaxChi2Tol = MaxChi2Tol
# ################################################################
algo.addTool( TrackMasterFitter )
#algo.TrackMasterFitter.MaterialLocator = "SimplifiedMaterialLocator"
algo.addTool( TrackMasterExtrapolator )
#algo.TrackMasterExtrapolator.MaterialLocator = "SimplifiedMaterialLocator"
# ################################################################
# algo.FillMuonStubInfo = True
algo.AddTTHits    = True
algo.MC           = False
# algo.OutputLevel  = 2
algo.Output       = "Rec/Track/MuonTT"
seq.Members += [algo]


# from PhysSelPython.Wrappers import Selection, DataOnDemand, ChargedProtoParticleSelection
from Configurables import ChargedProtoParticleMaker
"""
Make ProtoParticles out of MuonTT Tracks
"""
algo = ChargedProtoParticleMaker()
algo.Inputs = [ "Rec/Track/MuonTT" ]
algo.Output = "Rec/ProtoP/MuonTT"
seq.Members += [algo]

from Configurables import NoPIDsParticleMaker, TrackSelector
"""
Make Particles out of the muonTT ProtoParticles
"""
algo = NoPIDsParticleMaker('MuonTT')
algo.addTool( TrackSelector )
algo.TrackSelector.TrackTypes = [ "Long" ]
algo.Particle = 'muon'
algo.Input    =  "Rec/ProtoP/MuonTT"
# algo.OutputLevel = 4
seq.Members += [ algo]

