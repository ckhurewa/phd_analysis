#!/usr/bin/env python

## Patch for remote
import os, sys
sys.path.insert(0, os.path.join(os.getcwd(),'BenderCK.zip'))

import itertools
from BenderCK import *
from BenderCK.Tau import *

## Get BaseDitauRecoStats class
path = '/home/khurewat/analysis/013_Tau_Identification/efficiencies/DV-bender'
sys.path.insert(0, path)
from RecoStats import BaseDitauRecoStats, Particle_dummy # has base class
from AlgoPID import HadronPID

#==============================================================================

class DitauRecoStats(BaseDitauRecoStats):
  """
  Checking 'Kinematic efficiency', which is number of events fulfilling requirement
  ( PT, ETA, M ) at reco level divided by events fulfilled at generated level
  of simulation.

  As well as eff_reco = eff_tracking * eff_trigger *  eff_PID
  """

  ## Configure before appMgr
  from Configurables import TupleToolConeIsolation
  conf = TupleToolConeIsolation('DitauRecoStats.TupleToolConeIsolation')
  conf.MinConeSize = 0.5
  conf.MaxConeSize = 0.5
  conf.OutputLevel = 10

  def queues(self, ditau):
    """
    Yield (prefix, mcp, reco).
    """
    yield 'ditau', ditau     , None
    yield 'tau1' , ditau.tau1, None
    yield 'tau2' , ditau.tau2, None
    #
    dtype = ditau.type
    if 'h3' in dtype:
      if dtype == 'e_h3':
        taul,tauh3 = ditau
      elif dtype == 'h3_mu':
        tauh3,taul = ditau

      ## MCP & their 4vec
      lep          = taul.children(CHARGED)[0]
      pr1,pr2,pr3  = tauh3.children(CHARGED)
      ditauch_MCV4 = lep.momentum()+pr1.momentum()+pr2.momentum()+pr3.momentum()
      tauh3ch      = LHCb.MCParticle()
      tauh3ch.setMomentum(tauh3.vec4_children_charged) # Get a MCP version, with just charged children

      ## Find reco best on lepton
      tes_lep = self._containers[taul.type]
      recolep = self.find_reco_best( tes_lep, lep )

      ## Helper fetch method for tauh3 (complicate)
      recotauh3ch, recopr1, recopr2, recopr3 = self.fetch_reco_best_tauh3(tauh3)

      ## Finally
      ditauch_V4 = recolep.momentum() + recotauh3ch.momentum()
      yield 'lep'    , lep         , recolep
      yield 'pr1'    , pr1         , recopr1
      yield 'pr2'    , pr2         , recopr2
      yield 'pr3'    , pr3         , recopr3
      yield 'tauh3ch', tauh3ch     , recotauh3ch
      yield 'ditauch', ditauch_MCV4, ditauch_V4
    #
    else:
      # Get MC tau & their child
      tau1ch       = ditau.tau1.children(CHARGED)[0]
      tau2ch       = ditau.tau2.children(CHARGED)[0]
      tes1         = self._containers[ditau.tau1.type]
      tes2         = self._containers[ditau.tau2.type]
      reco1        = self.find_reco_best( tes1, tau1ch )
      reco2        = self.find_reco_best( tes2, tau2ch )
      ditauch_MCV4 = tau1ch.momentum()+ tau2ch.momentum()
      ditauch_V4   = reco1.momentum() + reco2.momentum()
      #
      yield 'ditauch', ditauch_MCV4, ditauch_V4
      if dtype in ('e_e','e_mu','mu_mu'):
        yield 'lep1', tau1ch, reco1
        yield 'lep2', tau2ch, reco2
      if dtype == 'e_h1':
        yield 'lep', tau1ch, reco1
        yield 'had', tau2ch, reco2
      if dtype == 'h1_mu':
        yield 'lep', tau2ch, reco2
        yield 'had', tau1ch, reco1

  @pass_success
  def analyse(self):
    for ditau in self.list_ditau:
      ## Upsilon(1S) -> tautau can happens
      if MCID(ditau) != 23: # Z0 only
        logger.info('Found non-Z0 di-tau')
        logger.info(ditau)
        continue
      ## skip hh, other
      dtype = ditau.type
      if 'other' in dtype or dtype in ('h1_h1', 'h1_h3', 'h3_h3'):
        continue
      tname  = ditau.type
      queues = self.queues(ditau)
      self.write(tname, queues)

#==============================================================================

def in_fiducial( ditau ):
  if not ( 60.*GeV < MCM(ditau) < 120.*GeV ):
    return False
  if MCPT(ditau.tau1) < 20.*GeV:
    return False
  if MCPT(ditau.tau2) < 20.*GeV:
    return False
  if not (2.0 < MCETA(ditau.tau1) < 4.5 ):
    return False
  if not (2.0 < MCETA(ditau.tau2) < 4.5 ):
    return False
  return True

def in_acceptance_ll( lep1, lep2, product ):
  ## TOPO
  if lep1.index() == lep2.index():
    return False
  if MC3Q(lep1) * MC3Q(lep2) >= 0:
    return False
  ## PT
  if MCPT(lep1)<5*GeV or MCPT(lep2) < 5*GeV:
    return False
  if MCPT(lep1)<20*GeV and MCPT(lep2) < 20*GeV:
    return False
  ## ETA
  if not ( 2.0 < MCETA(lep1) < 4.5 ):
    return False
  if not ( 2.0 < MCETA(lep2) < 4.5 ):
    return False
  ## MASS
  m = (lep1.momentum()+lep2.momentum()).M()
  if (product=='emu') and not (20*GeV < m < 120*GeV):
    return False
  if (product!='emu') and not (20*GeV < m < 80*GeV):
    return False
  # print 'PASS ll:', lep1, lep2
  return True


def in_acceptance_lh1( lep, had ):
  ## TOPO
  if len({ lep.index(), had.index() })!= 2:
    return False
  if MC3Q(lep)*MC3Q(had) >= 0:
    return False
  ## PT
  if MCPT(lep) < 20*GeV:
    return False
  if MCPT(had) < 10*GeV:
    return False
  ## ETA
  if not ( 2.0 < MCETA(lep) < 4.5 ):
    return False
  if not ( 2.25 < MCETA(had) < 3.75 ):
    return False
  ## MASS
  m = (lep.momentum() + had.momentum()).M()
  if not ( 30*GeV < m < 120*GeV ):
    return False
  ## Finally
  # print 'PASS lh1:', lep, had
  return True

def in_acceptance_lh3( lep, pr1, pr2, pr3 ):
  ## TOPO
  if len({ lep.index(), pr1.index(), pr2.index(), pr3.index() })!= 4:
    return False
  prcharge = (MC3Q(pr1) + MC3Q(pr2) + MC3Q(pr3))/3
  if abs(prcharge) != 1:
    return False
  if MC3Q(lep)*prcharge >= 0: # same sign
    return False
  ## PT
  # sort again, decreasing in PT
  pr1pT, pr2pT, pr3pT = sorted([ MCPT(pr1), MCPT(pr2), MCPT(pr3)], reverse=True)
  if MCPT(lep) < 20*GeV:
    return False
  if pr1pT < 6*GeV:
    return False
  if pr3pT < 1*GeV:
    return False
  tauh3ch = (pr1.momentum() + pr2.momentum() + pr3.momentum())
  if tauh3ch.Pt() < 12*GeV:
    return False
  ## ETA
  if not ( 2.0 < MCETA(lep) < 4.5 ):
    return False
  if not ( 2.25 < MCETA(pr1) < 3.75 ):
    return False
  if not ( 2.25 < MCETA(pr2) < 3.75 ):
    return False
  if not ( 2.25 < MCETA(pr3) < 3.75 ):
    return False
  ## Mass
  if not ( 0.7*GeV < tauh3ch.M() < 1.5*GeV ):
    return False
  ditauch = tauh3ch + lep.momentum()
  if not ( 30*GeV < ditauch.M() < 120*GeV ):
    return False
  ## Finally
  # print 'PASS lh3:', lep, pr1, pr2, pr3
  return True

def is_from_ditau( mcp ):
  """
  Return True is there's ditau (Z) in ancestor
  """
  cut1 = MCINANCESTORS(MCABSID==15) & MCINANCESTORS(MCABSID==23)
  # Exclude conversion
  cut2 = ~MCINANCESTORS(MCABSID==22)
  return (cut1&cut2)(mcp)

class DitauAccContamination(BaseTauAlgoMC):

  def findall_acc_cands( self, product ):
    sort    = lambda l: sorted(list(l), key=lambda p: MCPT(p), reverse=True)

    ## Break generator into list + Sorting
    sel_e   = sort(self.mcselected( 'e' ))
    sel_mu  = sort(self.mcselected( 'mu' ))
    sel_pi  = sort(self.mcselected( 'pi' ))
    sel_h3  = list(itertools.combinations( sel_pi, 3 ))
    hard_e  = sort(self.mcselected( 'hard_e' ))
    hard_mu = sort(self.mcselected( 'hard_mu' ))
    hard_pi = sort(self.mcselected( 'hard_pi' ))

    if product in ('eh1', 'h1mu'):
      hard_l = hard_e if product=='eh1' else hard_mu
      for lep,had in itertools.product( hard_l, hard_pi ):
        if in_acceptance_lh1( lep, had ):
          yield lep,had

    if product in ('eh3', 'h3mu'):
      hard_l = hard_e if product=='eh3' else hard_mu
      for lep, prongs in itertools.product( hard_l, sel_h3 ):
        pr1, pr2, pr3 = sort(prongs)
        if in_acceptance_lh3( lep, pr1, pr2, pr3 ):
          yield lep, pr1, pr2, pr3

    if product in ('ee', 'emu', 'mumu'):
      if product == 'ee':
        gen = itertools.combinations( sel_e, 2 )
      if product == 'mumu':
        gen = itertools.combinations( sel_mu, 2 )
      if product == 'emu':
        gen = itertools.product( sel_e, sel_mu )
      for lep1,lep2 in gen:
        if in_acceptance_ll( lep1, lep2, product ):
          yield lep1, lep2


  @pass_success
  def analyse(self):
    ## Reject those with more than one ditau, ignore it.
    all_ditau = list(self.list_ditau)
    if len(all_ditau) != 1:
      return
    ditau   = all_ditau[0]
    origin  = ditau.itype  # This is our channel of origin

    ## Do only in fiducial
    if not in_fiducial( ditau ):
      return

    # print self.event_break
    # print 'Origin:', origin, ditau.type

    ## Prepare single particles for loop
    sel_e  = self.mcselect('e' , (MCABSID==11) & (MCPT>5*GeV) & (MCETA>2.0) & (MCETA<4.5))
    sel_mu = self.mcselect('mu', (MCABSID==13) & (MCPT>5*GeV) & (MCETA>2.0) & (MCETA<4.5))
    sel_pi = self.mcselect('pi', ((MCABSID==211)|(MCABSID==321)) & (MCPT>1*GeV) & (MCETA>2.25) & (MCETA<3.75))

    ## Early reduce the combi
    sel_hard_e  = self.mcselect('hard_e' , sel_e , MCPT>20*GeV )
    sel_hard_mu = self.mcselect('hard_mu', sel_mu, MCPT>20*GeV )
    sel_hard_pi = self.mcselect('hard_pi', sel_pi, MCPT>10*GeV )

    ## Generate h1mu candidate arbitarily
    products = 'ee', 'eh1', 'eh3', 'emu', 'h1mu', 'h3mu', 'mumu'
    # products = 'ee', 'emu', 'mumu'
    for product in products:
      ncand_any   = 0
      ncand_ditau = 0
      for particles in self.findall_acc_cands( product ):
        ncand_any += 1
        if all( is_from_ditau(p) for p in particles ):
          ncand_ditau += 1

          # print 'From DITAU', ditau

      ## Each row(entry) is already per-event.
      with self.enhancedTuple(product) as tup:
        tup.column_int( 'origin'     , origin      )
        tup.column_int( 'ncand_any'  , ncand_any   )
        tup.column_int( 'ncand_ditau', ncand_ditau )


#==============================================================================

def configure ( inputdata, catalogs = [], castor = True ):
  from PhysConf.Filters import LoKi_Filters

  ## debug
  print inputdata
  print catalogs
  print castor

  ## Filter for tracking eff study.
  # filt = LoKi_Filters(STRIP_Code = "HLT_PASS_RE('StrippingTrackEffMuonTT_.*')")
  # DaVinci().EventPreFilters += [ filt.sequence('Prefilter') ]

  # ## Filter for hadron PID study
  # filt = LoKi_Filters(STRIP_Code="HLT_PASS_RE('.*NoBias.*')")
  filt = LoKi_Filters(STRIP_Code="HLT_PASS_RE('.*NoPIDDstarWithD02RSKPiLine.*)")
  DaVinci().EventPreFilters += [ filt.sequence('Prefilter') ]

  ## Attach input, clean due to incompat with Ganga 6.3.1 + EOS files
  if isinstance(inputdata, basestring):
    inputdata = [inputdata]
  inputdata = [x.replace('file:///root://', 'root://') for x in inputdata]
  setData( inputdata, catalogs, castor )

  seq = simple_configure(IS_MC=False)

  seq.Members += [
    # DitauRecoStats().name(),
    # HadronPIDAlgo().name(),
    # DitauAccContamination().name(),
    # TestMuonTT().name(),
    # TrackingEffAlgo().name(),
    # LeptonMisidCheck().name(),
    HadronPID().name()
  ]

  return SUCCESS

#--------------------------------------------------------------

if __name__ == '__main__':

  ## Some local sample
  # uri = '/panfs/khurewat/sample_Data2012_S20/00020198_00000100_1.ew.dst'

  ## Some more large remote samples
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00021317/0000/00021317_00003429_1.ew.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00021317/0000/00021317_00003442_1.ew.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00021317/0000/00021317_00003455_1.ew.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00021317/0000/00021317_00003468_1.ew.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00021317/0000/00021317_00003481_1.ew.dst',
  # ]

  ## S21.Calib
  # uri = 'root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/CALIBRATION.DST/00041834/0010/00041834_00101638_1.calibration.dst'

  ## MC: Zg tautau
  # uri = [ 'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/4279/%i/000000.AllStreams.dst'%i for i in xrange(75) ]

  ## MC: S20 MCMB 30000000
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00026814/0000/00026814_00000001_1.allstreams.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00026814/0000/00026814_00000002_1.allstreams.dst',
  # ]

  ## MC: HardQCD
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00038773/0000/00038773_00000001_2.AllStreams.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00038773/0000/00038773_00000002_2.AllStreams.dst',
  # ]

  ## Data: S20 Minibias
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00000076_1.minibias.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00000141_1.minibias.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00000271_1.minibias.dst',
  #   'root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00000362_1.minibias.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00000754_1.minibias.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020198/0000/00020198_00001327_1.minibias.dst',
  #   #
  #   "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020241/0000/00020241_00000010_1.minibias.dst",
  #   "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020241/0000/00020241_00000023_1.minibias.dst",
  #   "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020241/0000/00020241_00000036_1.minibias.dst",
  #   "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020241/0000/00020241_00000049_1.minibias.dst",
  #   "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/MINIBIAS.DST/00020241/0000/00020241_00000062_1.minibias.dst",
  # ]

  ## 20+Mevt Z->mumu 8TeV
  # uri = ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00047433/0000/00047433_00000001_2.AllStreams.dst']
  ## Zg_ee40GeV
  # uri = ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00029572/0000/00029572_00000001_1.allstreams.dst']
  # debugging bad zee
  # uri = [' root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00038700/0000/00038700_00000028_2.AllStreams.dst']

  ## Ztautau0
  # uri = '/lhcb/MC/2012/ALLSTREAMS.DST/00033128/0000/00033128_00000001_1.allstreams.dst'

  ## debug recod
  # uri = 'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/4083/56/000000.AllStreams.dst'

  ## Lower-PT hadron (via PIDCalib)
  

  configure(uri)
  run(-1)
