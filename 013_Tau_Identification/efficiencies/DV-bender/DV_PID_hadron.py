"""

Study charged hadron PID using minimumbias.

"""

from Configurables import DaVinci
from PhysConf.Filters import LoKi_Filters
from DecayTreeTuple.Configuration import DecayTreeTuple, EventTuple

preambulo = """
from LoKiCore.functions import *
from LoKiPhys.decorators import *
from LoKiProtoParticles.functions import *
""".split('\n')

#===============================================================================

vars_child = 'Q P PT ETA PHI PERR2 TRTYPE TRPCHI2 TRGHP TRCHI2DOF PROBNNghost'.split()
vars_child = {x:x for x in vars_child}
vars_child.update({
  'ISMUONLOOSE' : 'switch( ISMUONLOOSE, 1, 0)',
  'ISLONG'      : 'switch( ISLONG     , 1, 0)',
  'PP_CaloEcalE': 'PPFUN(PP_CaloEcalE)',
  'PP_CaloHcalE': 'PPFUN(PP_CaloHcalE)',
  'PP_CaloPrsE' : 'PPFUN(PP_CaloPrsE)',
  'PP_CaloSpdE' : 'PPFUN(PP_CaloSpdE)',
  'PP_InAccMuon': 'PPFUN(PP_InAccMuon)',
  'PP_InAccEcal': 'PPFUN(PP_InAccEcal)',
  'PP_InAccHcal': 'PPFUN(PP_InAccHcal)',
})

vars_mom = 'M P PT ETA PHI PERR2'.split()
vars_mom = {x:x for x in vars_mom}

def set_vars_child(br):
  tool = br.addTupleTool('LoKi::Hybrid::TupleTool')
  tool.Preambulo = preambulo
  tool.Variables = vars_child

def set_vars_mom(br):
  br.ToolList = [
    'TupleToolGeometry',
  ]
  tool = br.addTupleTool('LoKi::Hybrid::TupleTool')
  tool.Preambulo = preambulo
  tool.Variables = vars_mom

#===============================================================================

def PartTup():
  tup = DecayTreeTuple('PID_hadron', TupleName='hadron')
  tup.Inputs = ['PID/Phys/NoPIDDstarWithD02RSKPiLine/Particles']
  tup.Decay  = '[D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+]CC'
  tup.addBranches({
    'Dst': '^([D*(2010)+ ->  (D0 ->  K-  pi+)  pi+]CC)',
    'D0' : '  [D*(2010)+ -> ^(D0 ->  K-  pi+)  pi+]CC ',
    'K'  : '  [D*(2010)+ ->  (D0 -> ^K-  pi+)  pi+]CC ',
    'Pi' : '  [D*(2010)+ ->  (D0 ->  K- ^pi+)  pi+]CC ',
    'Pi2': '  [D*(2010)+ ->  (D0 ->  K-  pi+) ^pi+]CC ',
  })
  tup.ToolList = [
    'TupleToolRecoStats',
    'TupleToolEventInfo',
  ]
  set_vars_child(tup.K)
  set_vars_child(tup.Pi)
  set_vars_child(tup.Pi2)
  set_vars_mom(tup.Dst)
  set_vars_mom(tup.D0)
  return tup

#===============================================================================

seq = LoKi_Filters(
  # Preambulo   = preambulo,
  # HLT_Code    = "HLT_PASS_RE('Hlt2Dst2PiD02KPi.*')",
  STRIP_Code  = "HLT_PASS_RE('.*NoPIDDstarWithD02RSKPi.*')",
  # VOID_Code   = "SOURCE('Phys/StdAllNoPIDsPions', GoodPi ) >> (SIZE>=1)",
).sequencer('seq_had')

# # seq.Members += [ EvtTup() ]
# seq.Members += [ EvtTup(), filt, PartTup() ]
seq.Members += [PartTup()]

#===============================================================================

DaVinci().UserAlgorithms += [ seq ]
