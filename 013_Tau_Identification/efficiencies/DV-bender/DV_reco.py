
# """

# Aim to calculate 2 things

# 1. MC reconstruction efficiency. ( trigger + track + PID )

# 2. Kinematic eff (how much eta/PT degrades )
# Calculated using MC but can be used in real.

# """

# from Configurables import DaVinci
# from DecayTreeTuple.Configuration import MCDecayTreeTuple

# tup = MCDecayTreeTuple( 'h1mu', TupleName='Z02MuMu', NTupleDir='reco' )


# taumuminus  = '^(tau- -> ^mu- ...)'
# taumuplus   = '^(tau+ -> ^mu+ ...)'
# tauh1minus  = '[^(tau- -> nu_tau~ pi- {X0} {X0} {X0} ), ^(tau- -> nu_tau~ K- {X0} {X0} {X0} )]'
# tauh1plus   = '[^(tau+ -> nu_tau~ pi+ {X0} {X0} {X0} ), ^(tau+ -> nu_tau~ K+ {X0} {X0} {X0} )]'

# # tup.Inputs  = [ 'EW/Phys/Z02TauTau_MuXLine/Particles' ]
# tup.Decay   = '[Z0 --> {} {}]CC'.format( taumuminus, tauh1plus )
# # tup.Decay   = '[(Z0 --> {} {}), (Z0 --> {} {})]'.format( taumuminus, tauh1plus, taumuplus, tauh1minus )

# # tool = tup.addTupleTool( TupleToolTISTOS, name='tistos_mu' )
# # tool.TriggerList = [  
# #   'L0MuonDecision',
# #   'L0MuonNoSPDDecision',
# #   #
# #   'Hlt1SingleMuonNoIPDecision',
# #   'Hlt1SingleMuonHighPTDecision',
# #   #
# #   'Hlt2SingleMuonDecision',  # ps 0.5
# #   'Hlt2SingleMuonHighPTDecision',
# # ]
# # tool.VerboseL0 = True
# # tool.VerboseHlt1 = True
# # tool.VerboseHlt2 = True
# # tool = tup.addTupleTool( LoKi__Hybrid__TupleTool, name='tt_trig_muon' )
# # tool.Preambulo = preambulo
# # tool.Variables = {
# #   'TOS_MUON': 'switch( TOS_MUON, 1, 0)',
# # }


# DaVinci().UserAlgorithms += [ tup ]


