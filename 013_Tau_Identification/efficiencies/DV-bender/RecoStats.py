from BenderCK import *
from BenderCK.Tau import *

## class reference
Vec4 = ROOT.Math.LorentzVector("ROOT::Math::PxPyPzE4D<double>")

#===============================================================================

def adapter(vec4, IS_MC=True):
  """
  Helper to yield list of entry obtained from Vec4 but LoKi's compat naming.
  """
  mcprefix = 'MC' if IS_MC else ''
  yield mcprefix+'M'   , vec4.M()
  yield mcprefix+'P'   , vec4.P()
  yield mcprefix+'PT'  , vec4.Pt()
  yield mcprefix+'ETA' , vec4.Eta()
  yield mcprefix+'PHI' , vec4.Phi()


def Particle_dummy(pid=0):
  """
  Return dummy LHCb.Particle, ready for agressive functors.
  Don't provide deeper vars, as some functor BPVCORRM is smart enough to 
  handle null particle, but abort for partial-info particle.
  """
  # t  = LHCb.Track()
  # pp = LHCb.ProtoParticle()
  # mp = LHCb.MuonPID()
  # pp.setMuonPID(mp)
  # pp.setTrack(t)
  # p.setProto(pp)
  p  = LHCb.Particle(LHCb.ParticleID(int(pid)))
  v  = LHCb.Vertex()
  p.setEndVertex(v)
  return p

#===============================================================================

class BaseDitauRecoStats(BaseTauAlgoMC):
  """
  Provide common utilities for the RecoStats, for both Ztautau and HiggsMuTau
  analyses
  """
  _containers = {
    'e' : 'Phys/StdAllNoPIDsElectrons',
    'h1': 'Phys/StdAllNoPIDsPions', # prefer h1 instead of pi for tau.ttype 
    'mu': 'Phys/StdAllNoPIDsMuons',  # to ignore ISMUON from StdAllLooseMuons
  }

  ## Mark particle info
  INFO_FIND_SIMILAR = LHCb.Particle.LastGlobal+1

  @property
  def tt_cone_isolation(self):
    return self.tool('IParticleTupleTool', 'TupleToolConeIsolation')

  ## IMCReconstructible
  def recob(self, mcp):
    # Is better to typecast them to simplest types for safety
    tool = self.tool('IMCReconstructible', 'MCReconstructible')
    return int(tool.reconstructible(mcp))

  ## IMCReconstructed
  def recod(self, mcp):
    tool = self.tool('IMCReconstructed', 'MCReconstructed')
    return int(tool.reconstructed(mcp))

  def find_reco_best(self, tes, mcp):
    """
    Keep only the first result or empty Particle.

    Additionally, add the key LastGlobal+1 to mark whether it's been agressively
    find or not.
    """
    ## Normal search first, return if found
    res = sorted(list(self.find_reco_all( tes, mcp, find_similar=False )), key=-PT)
    if res:
      p = res[0]
      p.addInfo(self.INFO_FIND_SIMILAR, 0.)
      return p
    ## Fallback to more agressive search.
    res = sorted(list(self.find_reco_all( tes, mcp, find_similar=True )), key=-PT)
    if res:
      p = res[0]
      p.addInfo(self.INFO_FIND_SIMILAR, 1.)
      return p
    ## not found, return the dummy one
    return Particle_dummy()

  def fetch_reco_best_tauh3(self, tauh3):
    """
    Wrapper around `find_reco_best_tauh3`, handle the case of incomplete,failed
    fit to correctly populate the Tuple schema.

    Return 4 reconstructed particle: mother, and 3 prongs
    """
    pr1, pr2, pr3 = tauh3.children(CHARGED)
    tes_pr        = self._containers['h1']
    recotauh3ch   = self.find_reco_best_tauh3(tes_pr, tauh3, self.INFO_FIND_SIMILAR)
    if recotauh3ch is not None and len(recotauh3ch.children())==3: # successful fit
      recopr1 = recotauh3ch.children()[0]
      recopr2 = recotauh3ch.children()[1]
      recopr3 = recotauh3ch.children()[2]
    else:
      # provide individual result
      # whilst make sure the mom is null to discontinue, whlist providing the
      # full schema...
      recotauh3ch = Particle_dummy(MCID(tauh3)) # need correct ID to populate schema
      recopr1     = self.find_reco_best(tes_pr, pr1)
      recopr2     = self.find_reco_best(tes_pr, pr2)
      recopr3     = self.find_reco_best(tes_pr, pr3)
    return recotauh3ch, recopr1, recopr2, recopr3

  def write(self, tname, queues):
    """
    Queues is the iterable that yield (prefix, mcp, reco)
    """
    ## Event-vars
    recsummary = self.get('Rec/Summary')
    nPVs       = recsummary.info(LHCb.RecSummary.nPVs     , -1)
    nTracks    = recsummary.info(LHCb.RecSummary.nTracks  , -1)
    nSPDhits   = recsummary.info(LHCb.RecSummary.nSPDhits , -1)
    run, evt   = self.runevt

    ## Start writing
    with self.enhancedTuple(tname) as tup:
      ## Header
      tup.column_ulonglong('RunNumber', run )
      tup.column_ulonglong('EvtNumber', evt )
      #
      tup.column_uint('nPVs'    , nPVs     )
      tup.column_uint('nTracks' , nTracks  )
      tup.column_uint('nSPDhits', nSPDhits )

      ## Start loop over given queues
      for prefix, mcp, reco in queues:

        # ## DEBUG
        # print prefix, mcp, reco

        ## This is 4vec quantity, needs an adapter
        if isinstance(mcp, Vec4) and isinstance(reco, Vec4):
          tup.fill_gen(adapter( mcp , IS_MC=True  ), prefix )
          tup.fill_gen(adapter( reco, IS_MC=False ), prefix )
          continue
        #
        if mcp is not None:
          tup.fill_funcdict( Functors.FD_MCKINEMATICS, mcp, prefix )
          # check recob,recod if this is not artificial
          if MCABSID(mcp) not in (0, 15, 23, 25):
            tup.column_int( prefix+'_recob', self.recob( mcp ))
            tup.column_int( prefix+'_recod', self.recod( mcp ))
        #
        if reco is not None:
          tup.fill_funcdict( Functors.FD_KINEMATIC, reco, prefix )
          tup.column_int( prefix+'_agressive_find', int(reco.info(self.INFO_FIND_SIMILAR, -1)))
          #
          ## tauh3 should skip recob,recod, but having composite-related vars
          if ABSID(reco)==15:
            tup.fill_funcdict( Functors.FD_MOM3CHILDREN, reco, prefix )
            tup.fill_funcdict( Functors.FD_VERTEX      , reco, prefix )
            tup.fill_funcdict( Functors.FD_BPVSERIES   , reco, prefix )

          ## For mu,e,pi,prongs
          else:
            tup.fill_funcdict( Functors.FD_TRACK   , reco, prefix )
            tup.fill_funcdict( Functors.FD_PID     , reco, prefix )
            tup.fill_funcdict( Functors.FD_PROTOPID, reco, prefix )
            ## Trigger
            tup.fill_funcdict( self.FD_TOS_HIGHPTLEPTON, reco, prefix )
            ## Isolation
            self.tt_cone_isolation.fill( reco, reco, prefix, tup )
