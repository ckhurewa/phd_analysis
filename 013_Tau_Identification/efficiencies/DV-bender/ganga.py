import os, sys
sys.path.append('../..')
import bookmarks

ds = LHCbDataset.new( bookmarks.DATA_S20_PID )
# ds = LHCbDataset.new( bookmarks.Zg_tautau )
# ds = LHCbDataset.new( bookmarks.ID_42100000 )
# ds = LHCbDataset.new( bookmarks.ID_42112011 )[0:100] # MC DrellYan_mumu=10GeV
# ds = LHCbDataset.new( bookmarks.ID_42122001 ) # MC Zg_ee40GeV
# ds = LHCbDataset.new( bookmarks.ID_42112001 ) # MC Zg_mumu40GeV

#-------------------------------------------------------------------------------

## DAVINCI
# app = DaVinci(version='v42r1')
# app = prepareGaudiExec('DaVinci', 'v42r1')
app = GaudiExec(directory='/home/khurewat/cmtuser/DaVinciDev_v42r1')
app.options = [
  '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r317/options/DaVinci/DataType-2012.py',
  '/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v41r0/Kernel/XMLSummaryKernel/options/add-XMLSummary.py',
  'DV_conf_MDST.py',
  'DV_PID_hadron.py',
  'DV_PID_electron.py',
#   'DV_PID.py',
#   # 'DV_PID_Jpsi.py',
]

## BENDER
# app = Bender(version='v29r7', module='bender.py')

#-------------------------------------------------------------------------------

j = Job()
# j.name    = 'TrackingEff'
j.name    = 'EffPID'
# j.name    = 'Ztau-RecoStats'
# j.name    = 'lepton-MC-misid'
# j.name    = 'Ztau-Contamination'
# j.comment = 'MuonTT (Aevt/10)[3k:4k]'
# j.comment = 'Test chaining DV'
# j.comment = 'EFF: Tracking (Aevt/40)[0:6k]'
# j.comment = 'EFF: Kinematic (1kevt/1)[3k:4k]'
# j.comment = 'EFF: GEC (Akevt/50)[0:7k]'
# j.comment = 'PID low-PT muon (Akevt/40).'
# j.comment = 'PID hadron TEST BenderDebug'
# j.comment = 'PID hadron [1k:]/20. S20.MINIBIAS.DST. noghost'
# j.comment = 'PID hadron (Aevt/10). MC 30000000'
# j.comment = 'Revert to simple mcMatch (1Mevt)/1. TEST'
# j.comment = 'Contamination check at MCP-level. 1Mevt'
# j.comment = 'Adding Isolation info'
# j.comment = 'Checking electron-misid'
# j.comment = 'With biased/unbiased: Zg_ee40GeV'
# j.comment = 'PID muon+electron. Retry for misid.'
# j.comment = 'Run on Ztautau0, for etrig check.'
# j.comment = 'Fix tauh3 find_reco_best'
j.comment = 'Low-PT sample for elec/had. Aevt/20'

j.application = app
# j.backend     = PBS(extraopts='--mem=4830 -t 1-0:0:0 --exclude=lphe01,lphe02,lphe03')
j.backend     = Dirac()
j.inputdata   = ds
j.splitter    = SplitByFiles(filesPerJob=20)
j.outputfiles = ['*.root', 'summary.xml']

## For Bender+Grid only
# j.attach_mylib()

# queues.add(j.submit)
j.submit()

