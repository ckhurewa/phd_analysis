
#==============================================================================

class TrackingEffAlgo(AlgoMC):
  """

  Provide study on StrippingTrackEffMuonTT_.* series for Z02TauTau analysis.

  """

  @static_algo_property
  def TOS_L0Muon(self):
    return TOS('L0MuonDecision', 'L0TriggerTisTos' )
  @static_algo_property
  def TOS_Hlt1SingleMuon(self):
    return TOS('Hlt1SingleMuonDecision', 'Hlt1TriggerTisTos' )
  @static_algo_property
  def TOS_Hlt1SingleMuonHighPT(self):
    return TOS('Hlt1SingleMuonHighPTDecision', 'Hlt1TriggerTisTos' )
  @static_algo_property
  def TOS_Hlt2SingleMuonHighPT(self):
    return TOS('Hlt2SingleMuonHighPTDecision', 'Hlt2TriggerTisTos' )
  def TOS_MUON_HIGHPT( self, p ):
    return self.TOS_L0Muon(p) and self.TOS_Hlt1SingleMuonHighPT(p) and self.TOS_Hlt2SingleMuonHighPT(p)

  def queues_shared_fraction( self, probe ):
    """
    Given a probe (Particle made from custom MuonTT track), this method yields
    list of the best-matched long tracks, along with the fraction in muon,TT station.
    See more from paper "Measurement of the track reco eff at LHCb".
    """
    def queue():
      """Yield MuonPID that has non-null associated track."""
      l = self.get(LHCb.MuonPIDLocation.Offline)
      if l:
        for pid in l:
          if pid.muonTrack() and pid.idTrack():
            yield pid
    def shared( probe_track, mupid ):
      ## Check if there's any TT his first.
      hasTTHits = any( lhcbid.isTT() for lhcbid in mupid.idTrack().lhcbIDs() )
      ## Determine overlapping
      # numerator
      count_muon_match = mupid.muonTrack().nCommonLhcbIDs( probe_track )
      count_tt_match   = mupid.idTrack().nCommonLhcbIDs( probe_track )
      # denominator
      count_muon = sum(lhcbid.isMuon() for lhcbid in probe_track.lhcbIDs())
      count_tt   = sum(lhcbid.isTT()   for lhcbid in probe_track.lhcbIDs())
      # value
      fraction_mu = 1. * count_muon_match / count_muon
      fraction_tt = (1. * count_tt_match / count_tt) if hasTTHits else (-1.) # No hits
      ## Finally
      return mupid, fraction_mu, fraction_tt
    #
    # Finally
    for mupid in queue():
      yield shared( probe.proto().track(), mupid )

  def pass_shared_fraction( self, probe ):
    """
    Return True if there's at least one good MuonTT matched, in this case:
    frac_mu >= 0.4, and frac_tt >= 0.4

    Return highest-momentum one.
    If not found, return empty track
    """
    # print '-'*30
    # for mupid, frac_mu, frac_tt in self.queues_shared_fraction(probe):
    #   print frac_mu, frac_tt
    queue   = self.queues_shared_fraction(probe)
    valid   = lambda _,mu,tt: (mu>=0.7) and (tt==-1 or tt>=0.6)
    sorter  = lambda args: -TrP(args[0].idTrack())
    acc     = sorted([ args for args in queue if valid(*args) ], key=sorter)
    return len(acc)>0
    # return (True, acc[0][0].idTrack()) if acc else (False, LHCb.Track())


  @property
  def queue_Jpsi(self):
    """
    Yield trio ( mom, tag, probe ) in this strict order.

    Essential to re-order this since in strippingline[1,2], the order is swapping.
    TrackEffMuonTT_.*Line1 = PROBE + TAG
    TrackEffMuonTT_.*Line2 = TAG + PROBE
    """
    ## Prepare TAG & PROBE
    for p in self.selectTES('Calibration/Phys/TrackEffMuonTT_JpsiLine1'):
      yield p[0], p[2], p[1]
    for p in self.selectTES('Calibration/Phys/TrackEffMuonTT_JpsiLine2'):
      yield p[0], p[1], p[2]

  @property
  def queue_Upsilon(self):
    for p in self.selectTES('Calibration/Phys/TrackEffMuonTT_UpsilonLine1'):
      yield p[0], p[2], p[1]
    for p in self.selectTES('Calibration/Phys/TrackEffMuonTT_UpsilonLine2'):
      yield p[0], p[1], p[2]

  @property
  def queue_Z(self):
    """Yield trio ( mom, tag, probe ) in this strict order."""
    for p in self.selectTES('Calibration/Phys/TrackEffMuonTT_ZLine1'):
      yield p[0], p[2], p[1]
    for p in self.selectTES('Calibration/Phys/TrackEffMuonTT_ZLine2'):
      yield p[0], p[1], p[2]

  ## Functors
  @static_algo_property
  def fl(self):
    return [ 'P','PT','PERR2','Q','ETA','PHI','TRTYPE','TRPCHI2','TRCHI2DOF','ISMUON','ISMUONLOOSE' ]

  @static_algo_property
  def fd(self):
    return {
      'BPVIP'                   : BPVIP(),
      'BPVIPCHI2'               : BPVIPCHI2(),
      'TOS_L0Muon'              : self.TOS_L0Muon,
      'TOS_Hlt1SingleMuon'      : self.TOS_Hlt1SingleMuon,
      'TOS_Hlt1SingleMuonHighPT': self.TOS_Hlt1SingleMuonHighPT,
      'TOS_Hlt2SingleMuonHighPT': self.TOS_Hlt2SingleMuonHighPT,
      'TOS_MUON_HIGHPT'         : self.TOS_MUON_HIGHPT,
    }

  def write(self, tname, comb ):
    run,evt = self.runevt
    mom     = comb[0]
    tag     = comb[1]
    probe   = comb[2]
    v       = mom.endVertex()

    # muid, frac_mu, frac_tt = self.max_shared_fraction(probe)
    # match_track = muid.idTrack()

    match = self.pass_shared_fraction(probe)

    with self.enhancedTuple( tname ) as tup:
      tup.column( 'M'           , M(mom)              )
      tup.column( 'P'           , P(mom)              )
      tup.column( 'PT'          , PT(mom)             )
      tup.column( 'BPVIP'       , BPVIP()(mom)        )
      tup.column( 'MPVIP'       , MIPDV(PRIMARY)(mom) )
      tup.column( 'VCHI2'       , VCHI2(v)            )
      tup.column( 'VCHI2PDOF'   , VCHI2PDOF(v)        )
      tup.column( 'DPHI'        , abs(DPHI(tag)(probe)))
      #
      tup.fill_funclist( self.fl, tag  , prefix='tag'    )
      tup.fill_funclist( self.fl, probe, prefix='probe'  )
      tup.fill_funcdict( self.fd, tag  , prefix='tag'    )
      tup.fill_funcdict( self.fd, probe, prefix='probe'  )
      #
      ## the matched long track is NOT neceaasry equal to probe's
      tup.column_bool( 'match', match )
      #
      # tup.column( 'match_P'           , TrP         (match_track))
      # tup.column( 'match_PT'          , TrPT        (match_track))
      # tup.column( 'match_TrTYPE'      , TrTYPE      (match_track))
      # tup.column( 'match_TrCHI2PDOF'  , TrCHI2PDOF  (match_track))
      # tup.column( 'match_TrPROBCHI2'  , TrPROBCHI2  (match_track))
      # tup.column( 'match_TrGHOSTPROB' , TrGHOSTPROB (match_track))
      # tup.column( 'match_TrHASTT'     , TrHASTT     (match_track))
      # tup.column( 'match_TrNTHITS'    , TrNTHITS    (match_track))
      #
      # tup.column( 'match_fracmu'      , frac_mu )
      # tup.column( 'match_fractt'      , frac_tt )
      #
      tup.column_ulonglong('RunNumber', run           )
      tup.column_ulonglong('EvtNumber', evt           )
      tup.column_uint     ('nPVs'     , self.nPVs     )
      tup.column_uint     ('nTracks'  , self.nTracks  )
      tup.column_uint     ('nSPDhits' , self.nSPDhits )


  @pass_success
  def analyse(self):

    ## Event-vars
    recsummary    = self.get('Rec/Summary')
    self.nPVs     = recsummary.info(LHCb.RecSummary.nPVs     , -1)
    self.nTracks  = recsummary.info(LHCb.RecSummary.nTracks  , -1)
    self.nSPDhits = recsummary.info(LHCb.RecSummary.nSPDhits , -1)

    # ## Scan Jpsi, validate, and write
    for comb in self.queue_Jpsi:
      probe = comb[2]
      if PT(probe) > 1*GeV:  # Need to kill unused portion to save space.
        self.write( 'Jpsi', comb )
    for comb in self.queue_Upsilon:
      self.write( 'Upsilon', comb )
    for comb in self.queue_Z:
      self.write( 'Z', comb )

