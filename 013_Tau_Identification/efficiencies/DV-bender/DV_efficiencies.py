# #!/usr/bin/env gaudirun.py

# """

# REF:
# http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/stripping/config/stripping20/ew/strippingz02mumuline.html

# """

# from Configurables import DaVinci

# # from PhysConf.Filters import LoKi_Filters

# # def join(*args):
# #   return ' & '.join('(%s)'%x for x in args)

# ## Prefilters
# # filt = LoKi_Filters(STRIP_Code = "HLT_PASS_RE('StrippingZ0.*')|HLT_PASS_RE('StrippingWMuLine.*')" ) # Careful about SS lines too.
# # filt = LoKi_Filters(STRIP_Code = "HLT_PASS_RE('StrippingZ02TauTau_MuX.*')" ) # Careful about SS lines too.
# # DaVinci().EventPreFilters += [ filt.sequence('PreFilters') ]

# ## Configure DaVinci
# DaVinci().TupleFile     = 'tuple.root'
# DaVinci().DataType      = "2012"

# ## Moved to be inside data file
# # DaVinci().EvtMax = -1
# # DaVinci().Simulation = False
# # DaVinci().Simulation = True

