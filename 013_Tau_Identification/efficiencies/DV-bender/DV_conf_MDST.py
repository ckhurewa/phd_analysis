## Kill DAQ nodes in MDST
from Configurables import DaVinci
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['DAQ','pRec']
DaVinci().UserAlgorithms += [eventNodeKiller]   

## Setup DaVinci
from Configurables import DaVinci
DaVinci().Simulation    = False
DaVinci().Lumi          = True
DaVinci().EvtMax        = -1
DaVinci().TupleFile     = 'tuple.root'
DaVinci().HistogramFile = 'hist.root'
DaVinci().InputType     = 'MDST'
DaVinci().DataType      = '2012'
