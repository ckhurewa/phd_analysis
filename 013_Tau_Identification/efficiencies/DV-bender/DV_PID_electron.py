from Configurables import DaVinci
from PhysConf.Filters import LoKi_Filters
from DecayTreeTuple.Configuration import DecayTreeTuple, EventTuple

preambulo = """
from LoKiCore.functions import *
from LoKiPhys.decorators import *
from LoKiProtoParticles.functions import *
Good = (CHILDCUT(PT>5e3, 1, 1)|CHILDCUT(PT>5e3, 1, 2))
""".split('\n')

#===============================================================================

vars_child = 'Q P PT ETA PHI PERR2 TRTYPE TRPCHI2 TRGHP TRCHI2DOF PROBNNghost'.split()
vars_child = {x:x for x in vars_child}
vars_child.update({
  'ISMUONLOOSE' : 'switch( ISMUONLOOSE, 1, 0)',
  'ISLONG'      : 'switch( ISLONG     , 1, 0)',
  'PP_CaloEcalE': 'PPFUN(PP_CaloEcalE)',
  'PP_CaloHcalE': 'PPFUN(PP_CaloHcalE)',
  'PP_CaloPrsE' : 'PPFUN(PP_CaloPrsE)',
  'PP_CaloSpdE' : 'PPFUN(PP_CaloSpdE)',
  'PP_InAccMuon': 'PPFUN(PP_InAccMuon)',
  'PP_InAccEcal': 'PPFUN(PP_InAccEcal)',
  'PP_InAccHcal': 'PPFUN(PP_InAccHcal)',
})

vars_mom = 'M P PT ETA PHI PERR2'.split()
vars_mom = {x:x for x in vars_mom}

def set_vars_child(br):
  tool = br.addTupleTool('LoKi::Hybrid::TupleTool')
  tool.Preambulo = preambulo
  tool.Variables = vars_child

def set_vars_mom(br):
  br.ToolList = [
    'TupleToolGeometry',
  ]
  tool = br.addTupleTool('LoKi::Hybrid::TupleTool')
  tool.Preambulo = preambulo
  tool.Variables = vars_mom

#===============================================================================

def PartTup():
  tup = DecayTreeTuple('PID_electron', TupleName='electron')
  tup.Inputs = ['PID/Phys/Jpsi2eeForElectronIDBu2JpsiKLine/Particles']
  tup.Decay  = '[B+ -> ^(J/psi(1S) -> ^e+ ^e-) ^K+]CC'
  tup.addBranches({
    'B'   : '^([B+ ->  (J/psi(1S) ->  e+  e-)  K+]CC)',
    'Jpsi': '  [B+ -> ^(J/psi(1S) ->  e+  e-)  K+]CC ',
    'e1'  : '  [B+ ->  (J/psi(1S) -> ^e+  e-)  K+]CC ',
    'e2'  : '  [B+ ->  (J/psi(1S) ->  e+ ^e-)  K+]CC ',
    'K'   : '  [B+ ->  (J/psi(1S) ->  e+  e-) ^K+]CC ',
  })
  tup.ToolList = [
    'TupleToolRecoStats',
    'TupleToolEventInfo',
  ]
  set_vars_child(tup.K)
  set_vars_child(tup.e1)
  set_vars_child(tup.e2)
  set_vars_mom(tup.Jpsi)
  set_vars_mom(tup.B)
  return tup

#===============================================================================

seq = LoKi_Filters(
  Preambulo   = preambulo,
  STRIP_Code  = "HLT_PASS_RE('.*Jpsi2eeForElectronIDBu2JpsiK.*')",
  VOID_Code   = "SOURCE('PID/Phys/Jpsi2eeForElectronIDBu2JpsiKLine', Good) >> (SIZE>=1)",
).sequencer('seq_elec')

seq.Members += [PartTup()]

DaVinci().UserAlgorithms += [ seq ]
