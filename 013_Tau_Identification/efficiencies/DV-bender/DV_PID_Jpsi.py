
"""

Study muon PID using Jpsi.

Need PID.DST dataset, line `StrippingMuIDCalib_JpsiFromBNoPIDNoMip`

"""

from Configurables import DaVinci
from Configurables import GaudiSequencer
from Configurables import FilterDesktop
from Configurables import LoKi__Hybrid__TupleTool, LoKi__Hybrid__EvtTupleTool
from PhysConf.Filters import LoKi_Filters
from DecayTreeTuple.Configuration import DecayTreeTuple

preambulo = """
from LoKiPhys.decorators import *

def _ChildrenDPHI(i1,i2):
  from LoKiPhys.decorators import CHILD,PHI  
  from LoKiCore.math import cos,acos
  DPHI = abs(CHILD(PHI,i1)-CHILD(PHI,i2))
  DPHI = acos(cos(DPHI))  # mod pi       
  return DPHI
DPHI12 = _ChildrenDPHI(1,2)

GoodJpsi = CHILDCUT((PT>5E3),1) | CHILDCUT((PT>5E3),2)

GoodTag  = (ISMUON) & (ISLONG) & (ETA>2.0) & (ETA<4.5)

GoodProbe = (PT>5E3) & (ISLONG) & (ETA>2.0) & (ETA<4.5)

""".split('\n')

def Ntuple(src, spec):
  """src is used as unique id"""
  tup = DecayTreeTuple( 'tup_PID_Jpsi_muon_'+src, TupleName='jpsi_'+src, NTupleDir='tup_PID' )
  tup.Decay     = 'J/psi(1S) -> ^mu+ ^mu-'
  tup.Inputs    = [ 'Phys/%s/Particles'%src ]
  tup.ToolList  = []
  branches      = tup.addBranches(spec)
  for bname, br in branches.iteritems():
    if bname == 'jpsi':
      tool = br.addTupleTool( LoKi__Hybrid__TupleTool, 'ttloki_'+bname )
      tool.Preambulo = preambulo
      tool.Variables = {
        'M'          : 'M',
        'P'          : 'P',
        'PT'         : 'PT',
        'ETA'        : 'ETA',
        'PHI'        : 'PHI',
        'DPHI12'     : 'DPHI12',
        'VCHI2PDOF'  : 'VFASPF(VCHI2/VDOF)',  
        'BPVVDCHI2'  : 'BPVVDCHI2',
      }
    elif bname == 'tag':
      tool = br.addTupleTool( LoKi__Hybrid__TupleTool, 'ttloki_'+bname )
      tool.Preambulo = preambulo
      tool.Variables = {
        'P'          : 'P',
        'PT'         : 'PT',
        'ETA'        : 'ETA',
        'PHI'        : 'PHI',
        'PERR2'      : 'PERR2',
        'TRPCHI2'    : 'TRPCHI2',
      }  
    elif bname == 'probe':
      tool = br.addTupleTool( LoKi__Hybrid__TupleTool, 'ttloki_'+bname )
      tool.Preambulo = preambulo
      tool.Variables = {
        'P'          : 'P',
        'PT'         : 'PT',
        'ETA'        : 'ETA',
        'PHI'        : 'PHI',
        'PERR2'      : 'PERR2',
        'TRPCHI2'    : 'TRPCHI2',
        'ISMUON'     : 'switch( ISMUON     , 1, 0)',
        'ISMUONLOOSE': 'switch( ISMUONLOOSE, 1, 0)',
      }
  tool = tup.addTupleTool( LoKi__Hybrid__EvtTupleTool, 'ttlokievt' )
  tool.VOID_Variables = {
    'nPVs'    : 'RECSUMMARY(  0, -1)',
    'nTracks' : 'RECSUMMARY( 16, -1)',
    'nSPDhits': 'RECSUMMARY( 70, -1)',
  }
  return tup
  

filt1 = FilterDesktop('filt_probe_plus')
filt1.Inputs = [ 'PID/Phys/MuIDCalib_JpsiFromBNoPIDNoMip/Particles' ]
filt1.Code   = 'CHILDCUT(GoodProbe,1) & CHILDCUT(GoodTag,2)'

filt2 = FilterDesktop('filt_probe_minus')
filt2.Inputs = [ 'PID/Phys/MuIDCalib_JpsiFromBNoPIDNoMip/Particles' ]
filt2.Code   = 'CHILDCUT(GoodProbe,2) & CHILDCUT(GoodTag,1)'

tup1 = Ntuple('filt_probe_plus', {
  'jpsi'  : '^X0',
  'probe' : ' X0 -> ^mu+  mu-',
  'tag'   : ' X0 ->  mu+ ^mu-',
})
tup2 = Ntuple('filt_probe_minus', {
  'jpsi'  : '^X0',
  'tag'   : ' X0 -> ^mu+  mu-',
  'probe' : ' X0 ->  mu+ ^mu-',
})


seq1 = GaudiSequencer('seq1')
seq1.Members = [ filt1, tup1 ]

seq2 = GaudiSequencer('seq2')
seq2.Members = [ filt2, tup2 ]

seq0 = GaudiSequencer('seqOR')
seq0.IgnoreFilterPassed = True
seq0.Members = [ seq1, seq2 ]

seq = LoKi_Filters(
  Preambulo   = preambulo,
  STRIP_Code  = "HLT_PASS('StrippingMuIDCalib_JpsiFromBNoPIDNoMipDecision')",
  VOID_Code   = "SOURCE('PID/Phys/MuIDCalib_JpsiFromBNoPIDNoMip', GoodJpsi) >> (SIZE>=1)"
).sequencer('seq_Jpsi_muon')

seq.Members += [ seq0 ]
DaVinci().UserAlgorithms += [ seq ]
