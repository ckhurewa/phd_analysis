#!/usr/bin/env python

"""

For Ztautau contamination study.

"""

from rec_utils import *
from PyrootCK import import_file
from PyrootCK.mathutils import EffU

#===============================================================================

def normalize_colwise(df):
  """
  Return a dataframe of same dimension, but each column is normalize such that
  the sum of column == 1
  """
  return df / df.sum().apply(uncertainties.nominal_value)

#===============================================================================

def calc_acc_contamination():
  ## Get 2D histogram, 
  # JID = 5008
  JID = 5035 # larger stat
  fin = import_file(JID)
  h   = fin.Get('contamination_evt')

  ## Convert to compat dataframe
  res = {}
  for i in xrange(1,h.xaxis.nbins+1): # product
    product = h.xaxis.GetBinLabel(i)
    if product not in DTYPES: # skip some
      continue
    origins = {}
    for j in xrange(1,h.yaxis.nbins+1): # origin
      origin = h.yaxis.GetBinLabel(j)
      origins[origin] = h.GetBinContent( i,j )
    res[product] = origins
  df = pd.DataFrame(res).rename(DTYPE_TO_DT, columns=DTYPE_TO_DT)

  ## Normalize for each product type
  df  = df.applymap(var) # attach syst as err.
  df  = normalize_colwise(df)
  return df

@gpad_save
def draw_acc():
  def veto(x):
    if x<0.001:
      return 0.
    return x
  ## Veto low value
  df = calc_acc_contamination().applymap(veto)*100
  print df

  ## Change the label to latex
  df.columns  = [ latex_cols[x] for x in df.columns ]
  df.index    = [ latex_cols[x] for x in df.index   ]

  ## Also do the 2D figure
  h = ROOT.TH2F.from_uframe(df)
  h.name = 'cont'
  h.Draw('texte col')
  # h.xaxis.titleOffset = 1.5
  h.xaxis.labelSize   = 0.07
  h.yaxis.labelSize   = 0.07
  h.xaxis.title       = 'Product'
  h.yaxis.title       = 'Origin'

  ROOT.gStyle.SetPaintTextFormat('4.2f')
  ROOT.gStyle.SetPalette(ROOT.kBird)
  ROOT.gPad.SetBottomMargin(0.15)
  ROOT.gPad.SetLeftMargin  (0.14)
  ROOT.gPad.SetRightMargin (0.05)
  ROOT.gPad.Update()


#===============================================================================

def calc_esel():
  """
  Calculate from context above.
  """
  ## Load the dataframe
  df = df_from_path('check_ztau_seleff', '$DIR13/identification/plot/ditau_background')
  df = df.xs('evt', level=1)

  ## Calculate the selection efficiency
  calc  = lambda se: EffU( se.presel_anych, se.sel_anych ) if se.presel_anych>0 else ufloat(0.,0.)
  acc   = {}
  for product,df2 in df.groupby(level=0):
    df2 = df2.loc[product].iloc[:,1:]
    acc[product] = df2.apply(calc)
  df = pd.DataFrame(acc).rename(DTYPE_TO_DT)
  df.index.name   = 'Origin'
  df.columns.name = 'Product'
  return df

  # return

  # ## Convert to dataframe
  # result = {}
  # for product in channels:
  #   result[product] = {}
  #   name  = 'product_%s'%product
  #   dat   = globals()[name]
  #   for origin in dtypes:
  #     sel     = dat[origin]['selected'][1]
  #     presel  = dat[origin]['presel'][1]
  #     esel    = ufloat(0., 0.)
  #     if presel > 10.: # min size to consider
  #       esel = EffU( presel, sel )
  #     result[product][origin] = esel
  # df = pd.DataFrame(result)
  # return df


@gpad_save
def draw_esel():
  def veto(x):
    if x<0.01:
      return 0.
    return x

  ## Veto low value
  df = calc_esel().applymap(veto)*100
  print df

  ## Prepare latex labels
  df = df.rename(index=LABELS_DITAU, columns=LABELS_DITAU)

  ## Also do the 2D figure
  h = ROOT.TH2F.from_uframe(df)
  h.name  = 'esel'
  h.Draw('texte col')
  h.xaxis.labelSize   = 0.07
  h.yaxis.labelSize   = 0.07

  ROOT.gStyle.SetPaintTextFormat('4.2f')
  ROOT.gStyle.SetPalette(ROOT.kBird)
  ROOT.gPad.SetBottomMargin(0.15)
  ROOT.gPad.SetLeftMargin  (0.14)
  ROOT.gPad.SetRightMargin (0.05)


#===============================================================================

def main_purity():
  df1 = calc_acc_contamination()
  df2 = calc_esel()
  df  = normalize_colwise(df1 * df2)

  ## Print main table in latex compat format
  def formatter(x):
    return '-' if x<1e-4 else ufloat_to_latex_percent2(x)
  df2 = df.applymap(formatter)
  print df2

  ## Reprint purity again in flat format, for latex
  dat = [ (ch,df.loc[ch,ch]*100) for ch in CHANNELS ]
  header, arr = zip(*dat)
  print 
  print ('{:11} '*7).format(*header)
  print ('{:4.2f} '*7).format(*arr)
  print

  ## Print in Series, for fast_xc
  print
  print "DF['purity'] = pd.Series({"
  for name, val in dat:
    val /= 100
    print '  %-6r: ufloat( %.6f, %.6f, "purity" ),'%( name.replace('_',''), val.n, val.s )
  print "})"
  print

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.SetBatch(True)

  # print calc_acc_contamination().applymap('{:.4f}'.format)
  # print calc_esel()
  # draw_acc()
  # draw_esel()
  main_purity()
