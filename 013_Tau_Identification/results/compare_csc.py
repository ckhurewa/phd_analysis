#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Draw a visualization of cross-section comparison between observed & theory
from all sources.

"""

from collections import OrderedDict
from PyrootCK import ROOT
import combine_csc
from ditau_utils import LABELS_TAU

#===============================================================================

def main():

  ## Relabels & orders
  indices = OrderedDict([
    ('mumu'   , '#it{Z} #rightarrow #it{#tau}_{#it{#mu}}#it{#tau}_{#it{#mu}}'),
    ('h1mu'   , '#it{Z} #rightarrow #it{#tau}_{#it{#mu}}#it{#tau}_{#it{h}1}' ),
    ('h3mu'   , '#it{Z} #rightarrow #it{#tau}_{#it{#mu}}#it{#tau}_{#it{h}3}' ),
    ('ee'     , '#it{Z} #rightarrow #it{#tau}_{e}#it{#tau}_{e}'    ),
    ('eh1'    , '#it{Z} #rightarrow #it{#tau}_{e}#it{#tau}_{#it{h}1}'   ),
    ('eh3'    , '#it{Z} #rightarrow #it{#tau}_{e}#it{#tau}_{#it{h}3}'   ),
    ('emu'    , '#it{Z} #rightarrow #it{#tau}_{#it{#mu}}#it{#tau}_{e}'  ),
    ('ztautau', '#it{Z} #rightarrow #it{#tau}#it{#tau}'            ),
    ('zmumu'  , '#it{Z} #rightarrow #it{#mu}#it{#mu}'              ),
    ('zee'    , '#it{Z} #rightarrow #it{ee}'                  ),
  ])

  ## Prepare the labels
  df     = combine_csc.calc_repack_zll()
  inputs = df.loc[indices.keys(), ['val', 'stat', 'error_total']]
  inputs = inputs.rename(indices)
  labels = tuple(inputs.index) + tuple(combine_csc.ZLL_THEO_CSC.index)

  LHCb_FONT_FIXED = 133
  LHCb_SIZE_FIXED = 20

  ROOT.gStyle.SetFillColor(0)
  # ROOT.gStyle.SetFillStyle(1001)
  # ROOT.gStyle.SetFrameFillColor(0);
  # ROOT.gStyle.SetFrameBorderMode(0);
  # ROOT.gStyle.SetPadBorderMode(0);
  # ROOT.gStyle.SetPadColor(0);
  ROOT.gStyle.SetFrameFillColor(0)
  ROOT.gStyle.SetLegendFillColor(0)
  ROOT.gStyle.SetOptTitle(0)
  ROOT.gStyle.SetEndErrorSize(8)
  ROOT.gStyle.SetLegendFont(LHCb_FONT_FIXED)
  ROOT.gStyle.SetLegendTextSize(LHCb_SIZE_FIXED)
  ROOT.gROOT.SetBatch(True)


  def adapter_sym(list_csc, errs):
    return adapter_asym(list_csc, errs, errs)

  def adapter_asym(list_csc, errs_low, errs_high):
    """
    Adapter for the TGraphAsymmErrors constructor

    n, x, y, exl, exh, eyl, eyh
    """
    n = len(list_csc)
    # return n, list_csc, range(1,n+1), list_err, [0.]*n
    return n, list_csc, range(n, 0, -1), errs_low, errs_high, [0.]*n, [0.]*n

  def adapter_band(nom, sdv):
    return 2, [nom-sdv, nom+sdv], [-1.]*2, [0.]*2, [0.]*2, [0.]*2, [len(labels)+1.75]*2

  # Prepare the inputs. combining 2 sources: exp + theo
  csc1, stats1, errs1 = zip(*inputs.values)
  #
  csc       = csc1   + tuple(combine_csc.ZLL_THEO_CSC['val'])
  stats     = stats1 + tuple(combine_csc.ZLL_THEO_CSC['stat'])
  errs_low  = errs1  + tuple(combine_csc.ZLL_THEO_CSC['errs_low'])
  errs_high = errs1  + tuple(combine_csc.ZLL_THEO_CSC['errs_high'])
  #
  args_stat     = adapter_sym (csc, stats)
  args_all      = adapter_asym(csc, errs_low, errs_high)
  args_ztaustat = adapter_band(csc[7], stats[7])  # @ Ztautau
  args_ztautot  = adapter_band(csc[7], errs1[7])  # @ Ztautau

  ## Prepare canvas
  c = ROOT.TCanvas('name', 'title', 900, 600)
  c.ownership = False

  ## All errors, draw as back grey
  g1 = ROOT.TGraphAsymmErrors(*args_all)
  g1.ownership      = False
  g1.lineColorAlpha = 1, 1
  g1.lineWidth      = 1

  ## Stats only, draw as fore black
  g2 = ROOT.TGraphAsymmErrors(*args_stat)
  g2.ownership    = False
  g2.markerStyle  = 8
  g2.markerSize   = 0.5
  g2.lineStyle    = 3
  g2.lineWidth    = 2

  ## The error bands
  g3 = ROOT.TGraphAsymmErrors(*args_ztaustat)
  g3.fillColor = ROOT.kOrange-3
  g3.lineStyle = 3
  g4 = ROOT.TGraphAsymmErrors(*args_ztautot)
  g4.fillColor = ROOT.kOrange

  ## Putting them together
  g = ROOT.TMultiGraph()
  g.ownership = False
  # result band
  g.Add(g4, '3')
  g.Add(g3, '3')
  # points
  g.Add(g1, 'AP')
  g.Add(g2, 'AP')
  g.Draw('A')

  ## shared axis limit
  limits = 55, 132

  g.minimum           = 0
  g.maximum           = len(labels)+0.8
  g.xaxis.limits      = limits
  g.xaxis.title       = '#it{#sigma(pp #rightarrow Z)} [pb]'
  g.xaxis.labelFont   = LHCb_FONT_FIXED
  g.xaxis.labelSize   = LHCb_SIZE_FIXED * 1.2
  g.xaxis.titleFont   = LHCb_FONT_FIXED
  g.xaxis.titleSize   = LHCb_SIZE_FIXED * 1.35
  g.xaxis.titleOffset = 0.9
  g.yaxis.tickSize    = 0
  g.yaxis.labelSize   = 0

  ## Legend1
  t = ROOT.TPaveText(0.08, 0.12, 0.3, 0.975, 'NDC')
  t.ownership       = False
  t.fillColorAlpha  = 0, 0.
  t.textFont        = LHCb_FONT_FIXED
  t.textSize        = LHCb_SIZE_FIXED * 1.2
  t.textAlign = 12
  for lab in labels:
    t.AddText(lab)
  t.Draw('NB')

  # ## Legend: top left
  # text2 = ROOT.TPaveText(0.15, 0.94, 0.20, 0.97, 'NDC')
  # text2.AddText('LHCb, #sqrt{s} = 8 TeV, 2 fb^{-1}')
  # text2.textFont = LHCb_FONT_FIXED
  # text2.textSize = LHCb_SIZE_FIXED * 1.2
  # text2.Draw('NB')

  ## Legend: top right
  leg = ROOT.TLegend(0.87, 0.85, 0.98, 0.97)
  leg.AddEntry(g3, 'Stat. unc.', 'f')
  leg.AddEntry(g4, 'Tot. unc.' , 'f')
  leg.borderSize = 0
  leg.textAlign  = 32
  leg.Draw()

  ## Legend: bottom right
  text3 = ROOT.TPaveText(0.70, 0.14, 0.98, 0.45, 'NDC')
  text3.AddText('LHCb, #sqrt{#it{s}} = 8 TeV, 2 fb^{-1}')
  text3.AddText('#font[12]{l} = #font[12]{e, #it{#mu}, #it{#tau}}')
  text3.AddText('#font[12]{p}_{T}^{#font[12]{l}} > 20 GeV/#font[12]{c}')
  text3.AddText('2.0 < #eta^{#font[12]{l}} < 4.5')
  text3.AddText('60 < #font[12]{M}_{#font[12]{ll}} < 120 GeV/#font[12]{c}^{2}')
  text3.textFont  = LHCb_FONT_FIXED
  text3.textSize  = LHCb_SIZE_FIXED * 1.3
  text3.textAlign = 32
  text3.Draw('NB')

  ## Some linebreak
  l1 = ROOT.TPolyLine( 2, limits, [7.5,7.5])  # above 7 theo
  l2 = ROOT.TPolyLine( 2, limits, [10.5,10.5])  # above 7+3 summary
  l1.lineColorAlpha = 1, 0.7
  l2.lineColorAlpha = 1, 0.2
  l1.Draw() 
  l2.Draw() 

  ## Legend: measurement, theory (on top of linebreak)
  def draw_vertical_label(ymin, ymax, text):
    text1 = ROOT.TPaveText(0.02, ymin, 0.08, ymax, 'NDC')
    text1.ownership       = False
    text1.fillColorAlpha  = ROOT.kWhite, 1.
    text1.textFont        = LHCb_FONT_FIXED
    text1.textSize        = LHCb_SIZE_FIXED * 1.2
    text1.textAlign = 22
    t = text1.AddText(text)
    t.textAngle = 90
    text1.Draw('NB')
  draw_vertical_label(0.50, 0.95, 'LHCb measurements')
  draw_vertical_label(0.12, 0.45, 'PDF predictions')

  ## Finally
  c.topMargin    = 0.01
  c.leftMargin   = 0.02
  c.bottomMargin = 0.10
  c.rightMargin  = 0.01
  c.RedrawAxis()
  c.Update()
  c.SaveAs('compare_csc.pdf')
  c.SaveAs('compare_csc.eps')
  c.SaveAs('compare_csc.png')
  c.SaveAs('compare_csc.C')

#===============================================================================

if __name__ == '__main__':
  main()
