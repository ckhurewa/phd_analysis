#!/usr/bin/env python
"""
Study the result with tau braching ratio as POI, using the 7 cross-sections 
as the observables instead, per Roger's request.

http://tesla.desy.de/~pcastro/example_progs/fit/minuit_c_style/fit_minuit.cxx
"""

from PyrootCK import *
from array import array
from uncertainties import ufloat, nominal_value
import pandas as pd

## Data andle
import fast_csc
import combine_csc
from ditau_utils import Br_tau, pickle_dataframe_nondynamic, gpad_save
COVAR_MATS = combine_csc.cov_matrices()

CSC0 = combine_csc.ZLL_CSC.loc['val', 'zmumu']

#===============================================================================

def calc_chi_square(npar, gin, res, par, iflag):
  """
  (int&)     npar : Number of parameters
  (double*)  gin  : ???
  (double&)  res  : Result to minimize
  (double[]) par  : List of parameters
  (int)      iflag: Current status flag
  """
  ## Grab the tau BR, then di-tau
  # br_e, br_mu, br_h1 = par[0], par[1], par[2]
  # br_h3 = 1. - br_e - br_mu - br_h1
  br_e, br_h1, br_h3, br_mu = par[0], par[1], par[2], par[3]
  br = pd.Series({
    'mumu': br_mu * br_mu,
    'h1mu': br_h1 * br_mu * 2.,
    'h3mu': br_h3 * br_mu * 2.,
    'ee'  : br_e * br_e,
    'eh1' : br_e * br_h1 * 2.,
    'eh3' : br_e * br_h3 * 2.,
    'emu' : br_e * br_mu * 2.,
  })

  ## Inject into dataframe, compute new csc
  df = fast_csc.DF.copy()
  df['br'] = br
  csc = df.nsig / df.lumi / df.br / df.acc / df.rec / df.sel # * DF.purity

  ## Combined cross-section with BLUE
  # be careful of the order, must be canonical
  comb = combine_csc.combine_BLUE(csc.apply(nominal_value), stat=COVAR_MATS['stat'], syst=COVAR_MATS['syst'])
  csc_comb = ufloat(comb['nominal_value'], comb['error'])

  # print br, (csc-CSC0)
  # print (csc-CSC0).apply(lambda u: (u.n/u.s)**2)
  # print abs(nominal_value(csc_comb)-CSC0), (csc-CSC0).apply(lambda u: (u.n/u.s)**2).sum()

  ## Collect chi2 together
  chisq = 0
  # chisq += ((csc_comb.n-CSC0)/csc_comb.s)**2 # difference between combined and Zmumu
  chisq += (csc-CSC0).apply(lambda u: (u.n/u.s)**2).sum() # difference in each channel
  # print chisq
  res[0] = chisq


@pickle_dataframe_nondynamic
def fit_result():

  minuit = ROOT.TMinuit(4) # number of POI
  minuit.SetFCN(calc_chi_square)
  minuit.SetPrintLevel(0)

  flag = array('i', [0])
  minuit.mnparm(0, 'e' , .1783, 0.00001, .1783*0.9, .1783*1.1, flag)
  minuit.mnparm(1, 'h1', .5011, 0.00001, .5011*0.9, .5011*1.1, flag)
  minuit.mnparm(2, 'h3', .1457, 0.00001, .1457*0.9, .1457*1.1, flag)
  minuit.mnparm(3, 'mu', .1741, 0.00001, .1741*0.9, .1741*1.1, flag)

  ## Run & return
  minuit.SetMaxIterations(10000)
  minuit.Migrad()
  se = pd.Series(minuit.parameters)
  se.index = 'e', 'h1', 'h3', 'mu'
  return se

#===============================================================================

@gpad_save
def draw_fit_tau_br():
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetEndErrorSize(10)

  ## Prepare canvas
  c = ROOT.TCanvas('brtau', 'brtau', 1200, 300)
  c.ownership = False

  xarr = fit_result().values * 100.
  yarr = [4, 3, 2, 1]  
  g1 = ROOT.TGraphErrors.from_pair_ufloats(xarr, yarr)
  g1.ownership = False
  g1.markerSize = 0 

  xarr = Br_tau().values * 100.
  yarr = [4.3, 3.3, 2.3, 1.3]
  g2 = ROOT.TGraphErrors.from_pair_ufloats(xarr, yarr)
  g2.ownership   = False
  g2.markerStyle = 0 
  g2.fillColor   = 0
  g2.lineColor   = ROOT.kRed
  g2.lineWidth   = 1
  g2.markerColor = ROOT.kRed

  g = ROOT.TMultiGraph()
  g.ownership = False
  g.Add(g1, 'AP')
  g.Add(g2, 'AP')
  g.Draw('A')

  g.minimum = 0
  g.maximum = 5
  g.xaxis.title = 'BR(#tau#rightarrow X) [%]'
  g.yaxis.tickSize    = 0
  g.yaxis.labelSize   = 0
  g.xaxis.labelSize  = 0.10
  g.xaxis.titleSize  = 0.10

  leg = ROOT.TLegend(0.82, 0.24, 0.94, 0.50)
  leg.ownership = False
  leg.AddEntry(g1, 'Fit Z#rightarrow#tau#tau', 'lp')
  leg.AddEntry(g2, 'PDG' , 'lp')
  leg.borderSize = 0
  leg.textAlign  = 32
  leg.textSize = 0.1
  leg.Draw()

  text_taue = ROOT.TPaveText(0.20, 0.76, 0.25, 0.98, 'NDC')
  text_taue.AddText('#tau#scale[0.6]{#lower[0.4]{e}}')
  text_taue.ownership = False
  text_taue.fillColorAlpha = 1, 0
  text_taue.textSize = 0.15
  text_taue.textAlign = 13
  text_taue.Draw('NB')

  text_tauh1 = ROOT.TPaveText(0.70, 0.56, 0.95, 0.78, 'NDC')
  text_tauh1.AddText('#tau#scale[0.6]{#lower[0.4]{h1}}')
  text_tauh1.ownership = False
  text_tauh1.fillColorAlpha = 1, 0
  text_tauh1.textSize = 0.15
  text_tauh1.textAlign = 13
  text_tauh1.Draw('NB')

  text_tauh3 = ROOT.TPaveText(0.20, 0.46, 0.25, 0.68, 'NDC')
  text_tauh3.AddText('#tau#scale[0.6]{#lower[0.4]{h3}}')
  text_tauh3.ownership = False
  text_tauh3.fillColorAlpha = 1, 0
  text_tauh3.textSize = 0.15
  text_tauh3.textAlign = 13
  text_tauh3.Draw('NB')

  text_taumu = ROOT.TPaveText(0.20, 0.28, 0.25, 0.50, 'NDC')
  text_taumu.AddText('#tau#scale[0.6]{#lower[0.4]{#mu}}')
  text_taumu.ownership = False
  text_taumu.fillColorAlpha = 1, 0
  text_taumu.textSize  = 0.15
  text_taumu.textAlign = 13
  text_taumu.Draw('NB')


  c.leftMargin   = 0.005
  c.rightMargin  = 0.005
  c.topMargin    = 0.015
  c.bottomMargin = 0.2
  c.Update()

#===============================================================================

if __name__ == '__main__':
  print fit_result().fmt2p
  # print Br_tau().fmt2p
  # draw_fit_tau_br()
