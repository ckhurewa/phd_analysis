#!/usr/bin/env python
"""

NUMBER OF SAMPLES

Used as denominator in the MC expected-candidates computation.

"""

import re
import pandas as pd

#===============================================================================
# Total from bookkeeping
#===============================================================================

## Custom
ztautau        = 0 # place holder
ztautau0       = 4046990
we20jet        = 1994790  # LOCAL, need number before DV strip (mark [0:2M])
wmumujet       = 69950    # LOCAL
mcmb_mux       = 119089   # LOCAL

## Take total from bookeeping
# ttbar_41900006 = 2007684
# ttbar_41900007 = 2050937
# ttbar_41900010 = 4042228
# WW_ll          = 211233
# WW_lx          = 223475
# WZ_lx          = 238199
# ztaujet        = 3030718
# dymu           = 20571464
# zmu17jet       = 5073236
# zmu17cjet      = 2019302
# zmu17bjet      = 2011907
# dye40          = 2126272  # 3118918
# dye10          = 2369246
# zbb            = 10399697
# wtaujet        = 2576506
# wmujet         = 2039268
# wmu17jet       = 10015768
# we             = 5031270
# wejet          = 2080560
# # wtaubb         = 0        # NA
# hardqcd        = 1021185
# cc_harde       = 1057501
# cc_hardmu      = 1167321
# bb_harde       = 1033611
# bb_hardmu      = 1118554

#===============================================================================
# From J5435, 5438 
# Manual counter
#===============================================================================

ttbar_41900006 = 2007684 # 41900006
ttbar_41900007 = 2050937 # 41900007
ttbar_41900010 = 4042228 # 41900010
WW_ll          = 211233 # 41922002
WW_lx          = 223475 # 42021000
WZ_lx          = 238199 # 42021001
ztaujet        = 3030718 # 42100020
dymu           = 20571464 # 42112011
zmu17jet       = 5073236 # 42112022
zmu17cjet      = 2019302 # 42112052
zmu17bjet      = 2011907 # 42112053
dye40          = 1978685 # 42122001
dye10          = 2369246 # 42122011
zbb            = 10399697 # 42150000
wtaujet        = 2576506 # 42300010
wmujet         = 2039268 # 42311010
wmu17jet       = 10034658 # 42311011
we             = 5031270 # 42321000
wejet          = 2080560 # 42321010
wtaubb         = 4032982 # 42901000
hardqcd        = 1021185 # 49001000
cc_hardmu      = 1167321 # 49011004
bb_hardmu      = 1118554 # 49011005
cc_harde       = 1057501 # 49021004
bb_harde       = 1033611 # 49021005

#===============================================================================
# UTILS
#===============================================================================

def read_xml(jid):
  pat  = r'DecIdCounter/nevt_by_type/(\d+)">(\d+)'
  path = '/home/khurewat/gangadir/workspace/khurewat/LocalXML/%i/output/summary.xml'%jid
  acc  = pd.Series()
  with open(path) as fin:
    for line in fin:
      res = re.findall(pat, line)
      if res:
        decid = res[0][0]
        count = int(res[0][1])
        acc[decid] = count
  return acc

#===============================================================================

if __name__ == '__main__':
  print read_xml(5438)+read_xml(5435)
