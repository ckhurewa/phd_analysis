#!/usr/bin/env python
# -*-*- encoding=utf-8 -*-*-

"""

Calculate normalized expected candidate, given theoretical CSC & eGEN.

Aim to take result from ditau_background.py, and feed to latex.

"""

__all__ = ['calc_single_array'] # in candidates.py

from PyrootCK import *
from PyrootCK.mathutils import EffU, Eff, asymvar

## Secondary tools
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import (
  CHANNELS, TREENAME_TO_LATEX, TREENAME_TO_DECID, DT_TO_LATEX,
  Lumi, pd, memorized, df_from_path, inject_midrule,
)

## Local conf
import csc_egen  # cross-section x generator-efficiency
import nevents

## Import channel-INDEPEDENT luminosity
# Base unit in this context is millibarn (from picobarn)
lumi = asymvar.from_ufloat(Lumi() * 1E9)

## List of tables available for normalizer
# the name is dictated from ditau_background.py script.
TAGS = [
  'check_mc_nos_nss',
  'check_antiiso_contamination',
  'check_mc_selnoiso',
  'check_dd_mixiso',
  'check_zpeak_contamination',
  'check_mc_zllmisid',
  'check_sel_masswindow',
]

#===============================================================================
# LIST OF SAMPLES
#===============================================================================

TNAME_TO_LATEX = {
  ## Treename       , Latex alias
  # 'ztautau'       : None                 ,
  'ztautau0'      : '\ztautau  42100000' , #Central version, flat egen
  'dymu'          : '\zmumu    42112011' ,
  'dye10'         : '\zee      42122011' ,
  # 'dye40'         : '          42122001' , # removed
  'zbb'           : '\zbb      42150000' ,
  'wtaujet'       : '\wtaujet  42300010' ,
  # 'wmujet'        : None                 ,
  'wmu17jet'      : '\wmujet   42311011' ,
  'we'            : '\we       42321000' ,
  'wejet'         : '\wejet    42321010' ,
  'we20jet'       : 'WE20JET'            ,
  'wmumujet'      : '\wmumujet 42311012' ,
  # 'wtaubb'        : None                 ,
  'ztaujet'       : '\ztaujet  42100020' ,
  'zmu17jet'      : '\zmujet   42112022' ,
  'zmu17cjet'     : 'ZMU17CJET'          ,
  'zmu17bjet'     : 'ZMU17BJET'          ,
  'hardqcd'       : 'HARDQCD'            ,
  'cc_harde'      : '\ccbare   49021004' ,
  'cc_hardmu'     : '\ccbarmu  49011004' ,
  'bb_harde'      : '\\bbbare   49021005' ,
  'bb_hardmu'     : '\\bbbarmu  49011005' ,
  'mcmb_mux'      : 'MCMBMUX'             ,
  # 'ttbar_41900006': '\\ttbar    41900006' ,
  # 'ttbar_41900007': '          41900007' ,
  'ttbar_41900010': '\\ttbar   41900010' ,
  'WW_ll'         : '\wwll     41922002' ,
  'WW_lx'         : '\wwlx     42021000' ,
  'WZ_lx'         : '\wzlx     42021001' ,
}

## Ordered list of items to appear in Ztautau note.
LIST_LATEX_ZTAUTAU = [
  'ztautau0',
  'dymu',
  'dye10',
  # 'dye40', # removed
  'zbb',
  'zmu17jet', # redundant
  'wtaujet',
  'wmu17jet',
  'we',
  'wejet',
  'wmumujet',
  # 'ztaujet',  # redundant
  'cc_harde',
  'cc_hardmu',
  'bb_harde',
  'bb_hardmu',
  # 'ttbar_41900006',
  # 'ttbar_41900007',
  'ttbar_41900010',
  'WW_ll',
  'WW_lx',
  'WZ_lx',
  #
  'real', # DATA
]

#===============================================================================

def fmt_dual(rawcount, result, width=28):
  """
  Print the value with uncertainty compat with latex table.
  """
  s0  = '\\tc{{%s}}{{%s}}{{%s}}{{{}}}'
  v1  = float(result)
  v2  = result.ehigh
  v3  = abs(result.elow)
  if v1 > 1E3: v1 = int(v1)
  if v2 > 1E3: v2 = int(v2)
  if v3 > 1E3: v3 = int(v3)
  fm1 = '{:.1f}' if v1<1E3 else '{}'
  fm2 = '{:.1f}' if v2<1E3 else '{}'
  fm3 = '{:.1f}' if v3<1E3 else '{}'
  s0  = s0%(fm1,fm2,fm3)
  s   = s0.format(v1, v2, v3, rawcount)
  t   = '{:%i}'%width
  return t.format(s)


def sort_column_channels(df):
  """
  Sort the column to my prefered latex order.
  Add fix to ignore extra columns
  """
  cols1 = [x for x in CHANNELS if x in df.columns]     # Follow CHANNELS if possible
  cols2 = [x for x in df.columns if x not in CHANNELS] # retain original for the rest
  return df[cols1+cols2]


#===============================================================================

def loading_context_pickle(dpath, mask=LIST_LATEX_ZTAUTAU):
  """
  Load the context from pickled results
  
  Also cast into legacy format, which is a dict of dataframe.
  rows are processes, columns are channels. Key is tag_sign.
  Discard the entries here, keep only event
  """
  acc = {}
  for tag in TAGS:
    # load, repack, sort
    df = df_from_path(tag, dpath)
    if df is not None:
      df = df['evt']
      acc[tag+'_os'] = sort_column_channels(df['OS'].unstack().T).loc[mask].fillna(0).applymap(int)
      acc[tag+'_ss'] = sort_column_channels(df['SS'].unstack().T).loc[mask].fillna(0).applymap(int)
  ## finally
  return acc
  

@memorized
def loading_cscegen_nevts():
  # for cscgen, cast to (asymmetric) asymvar.from_ufloat(cscgen)
  acc = pd.DataFrame(index=['tex', 'cscgen', 'nevts'])
  for key, tex in TNAME_TO_LATEX.iteritems():
    c = asymvar.from_ufloat(getattr(csc_egen, key))
    n = getattr(nevents, key)
    acc[key] = [tex, c, n]
  return acc.T


#===============================================================================
# CORE COMPUTATION
#===============================================================================

def calc_single_array(tname, context):
  """
  Calculate the number of expected candidates in desired table.

  Note: In a single cell, there are 3 items: 
  1. raw count
  2. seleff
  3. normalized count

  One should use df.applymap to select only desired info for later
  """
  acc = {}
  dat = loading_cscegen_nevts()
  for dt, n in context[tname].iteritems():
    df         = pd.concat([dat,n], axis=1, join='inner')
    df['esel'] = df.apply(lambda se: Eff(se.nevts, se[dt]), axis=1)
    df['norm'] = df.apply(lambda se: se.esel * se.cscgen * lumi, axis=1)
    acc[dt]    = df.apply(lambda se: tuple(se[[dt,'esel','norm']].values), axis=1 )
  return sort_column_channels(pd.DataFrame(acc))


def calc_single_array_expected(tname, sign, context, add_data=True):
  """
  Specialization of above method for the expected number of candidates,
  partial formatted to latex.
  """
  ## prep, keep data away first, before it losts inside calc_single_array
  fullname = tname+'_'+sign
  se_data  = context[fullname].loc['real']

  ## Parse all count and normalize to integrated luminosity
  df = calc_single_array(fullname, context)

  ## Extract the ID into another column
  df.insert(0, 'Process', [TREENAME_TO_LATEX[s] for s in df.index])
  df.insert(1, 'ID'     , [str(TREENAME_TO_DECID[s]) for s in df.index])

  ## Sort result by ID
  df = df.sort_values('ID')

  ## prepare printer, apply only to numbers, skip string
  printer = lambda args: args if isinstance(args, basestring) else fmt_dual(args[0], args[2])
  dfout = df.applymap(printer)

  ## Add the data into last row after printer
  ## optionally a data row
  if add_data:
    dfout.loc['data'] = se_data
    dfout.loc['data','Process'] = 'Data'
    dfout.loc['data','ID'     ] = ''

  ## finally
  return dfout


#===============================================================================
# EXPORTS
#===============================================================================

def save_latex_to_file(df, fullname):
  ## debug print
  if '-v' in sys.argv:
    print df 
  ## convert to latex
  msg = df.to_latex(escape=False, index=False, na_rep='')
  # optional line if there's data
  if df.iloc[-1,0]=='Data':
    msg = inject_midrule(msg, -4)
  ## Make a target then save
  fname = os.path.join('normalizer', fullname+'.txt')
  with open(fname, 'w') as fout:
    fout.write(msg)
  logger.info(os.path.abspath(fname))


def latex_table_osss(tname, context, preproc=None):
  """
  Given an array prefix `aname`, print all normalized cross-section from number
  of selected cand in this array, across all ditau channel.
  """
  for sign in ('os', 'ss'):
    df = calc_single_array_expected(tname, sign, context)
    df = df.rename(columns=DT_TO_LATEX) # late column rename after all joins
    ## Apply optional preprocessor on the dataframe
    if preproc:
      df = preproc(df)
    ## finally
    save_latex_to_file(df, tname+'_'+sign)


def latex_misid_zmumu(context):
  """
  This has only 2 columns, unlike those guys above.
  """
  tname = 'check_mc_zllmisid'
  df    = calc_single_array_expected(tname, 'os', context)
  df    = df.rename(columns={'mumu': 'di-muon', 'ee': 'di-electron'})
  save_latex_to_file(df, tname)


#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    path    = '$DIR13/identification/plot/bkg_context'
    context = loading_context_pickle(path)
    ## Compute & save tables
    latex_table_osss('check_mc_nos_nss'           , context)
    latex_table_osss('check_antiiso_contamination', context)
    latex_table_osss('check_mc_selnoiso'          , context)
    latex_table_osss('check_zpeak_contamination'  , context)
    latex_table_osss('check_dd_mixiso'            , context)
    latex_table_osss('check_sel_masswindow'       , context)
    latex_misid_zmumu(context)
    
  ## DEV
  path    = '$DIR13/identification/plot/bkg_context'
  # context = loading_context_pickle(path)
  # print context['check_mc_nos_nss_os']
  # print loading_cscegen_nevts()

  # print calc_single_array('check_mc_nos_nss_os', context)
  # print calc_single_array_expected('check_antiiso_contamination', 'os', context)
  # print df_from_path('check_data_nos_nss', path)
