#!/usr/bin/env python

"""

Finally combine the cross-section from all channels, 
as well as performing lepton universality test.

"""

import uncertainties as uncer
from uncertainties.unumpy import matrix

## Tool
from PyrootCK import *
from PyrootCK.mathutils import sumq, asymvar, combine_BLUE
from PyrootCK.mathutils import covariance_matrix as covar
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import pd, memorized

## Data dependency
import fast_csc

#===============================================================================
# OBSERVABLES
#===============================================================================

## Result from another analysis
ZLL_CSC = pd.DataFrame({
  'zmumu': ( 95.0 , 0.3 , 0.7 , 1.1 ,  1.1  ),
  'zee'  : ( 93.81, 0.41, 1.48, 1.08,  1.14 ),
}, index=['val', 'stat', 'syst', 'beam', 'lumi'])


## Like above, but for 7TeV results
ZLL7_CSC = pd.DataFrame({
  'zee'    : ( 76.0, 0.8, 2.0, 2.6, None, 'LHCb-PAPER-2012-036' ),
  'zmumu'  : ( 76.0, 0.3, 0.5, 1.3, 1.0 , 'LHCb-PAPER-2015-001' ),
  'ztautau': ( 71.4, 3.5, 2.8, 2.5, None, 'LHCb-PAPER-2012-029' ),
}, index=['val', 'stat', 'syst', 'lumi', 'beam', 'ref'])


## From Katharina
ZLL_THEO_CSC = pd.DataFrame({
  # PDF        nominal   stat        pdf+      pdf- (68%CL)
  'MSTW08' : ( 94.7139 , 0.0819357 , 1.97321 , 1.76943 ),
  'NNPDF30': ( 92.6211 , 0.109097  , 2.0747  , 2.0747  ),
  'CT10'   : ( 94.252  , 0.0821569 , 3.56697 , 3.59106 ),
  'CT14'   : ( 93.9498 , 0.107521  , 2.5019  , 2.1011  ),
  'ABM12'  : ( 93.9673 , 0.0664061 , 1.13218 , 1.13218 ),
  'HERA15' : ( 97.3904 , 0.0804132 , 1.39495 , 1.39495 ),
  'MMHT14' : ( 94.8812 , 0.0919579 , 1.97459 , 1.94782 ),
}, index=['val', 'stat', 'pdf_high', 'pdf_low']).T
# preprocess total err
ZLL_THEO_CSC['errs_low']  = (ZLL_THEO_CSC['stat']**2 + ZLL_THEO_CSC['pdf_low']**2)**0.5
ZLL_THEO_CSC['errs_high'] = (ZLL_THEO_CSC['stat']**2 + ZLL_THEO_CSC['pdf_high']**2)**0.5

## From 7TeV paper
# https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/Summer2012
ZLL7_THEO_CSC = pd.DataFrame({
  # PDF     nom   pdf+  pdf-
  'MSTW08': (74.3, 1.9, 2.1),
}, index=['val', 'errs_high', 'errs_low']).T

#===============================================================================

## Cache & guard missing
@memorized
def CORR_BR():
  return fast_csc.corr_br()

@memorized
def CORR_EREC():
  return pd.read_pickle(os.path.expandvars('$DIR13/efficiencies/reconstruction/rec/export_corr_matrix.df')).values

@memorized
def CORR_ESEL():
  return pd.read_pickle(os.path.expandvars('$DIR13/efficiencies/selection/sel/export_corr_matrix.df')).values


#===============================================================================
# COVARIANCE MATRIX
#===============================================================================

@memorized
def cov_matrices(collapse_syst=True):
  """
  Develop a dict of covar matrices of the 7 csc measurements.
  """

  ## nominals values & const
  vals  = fast_csc.CSC_ALL.loc['val']
  N     = len(vals)
  O     = pd.np.ones((N,N))  # square of ones , for completely correlated values
  I     = pd.np.identity(N)  # identity matrix, for completely uncorrelated values

  ## mapping the source of uncertainty to correlation matrix
  corr_map = {
    'acc'   : O,
    'beam'  : O,
    'br'    : CORR_BR(),
    'lumi'  : O,
    'nsig'  : I,
    'rec'   : CORR_EREC(),
    'sel'   : CORR_ESEL(),
    'stat'  : I,
  }

  ## The error type not in this list will be treated as systematics
  not_for_syst = 'beam', 'stat', 'lumi'

  ## Expand the covar matrices, collect syst together
  list_cov_syst  = []
  covar_matrices = {}
  for name, corr in corr_map.iteritems():
    errs  = fast_csc.CSC_ALL.loc[name]
    cov   = covar(vals, errs, corr)
    if (name in not_for_syst) or (not collapse_syst):
      covar_matrices[name] = cov 
    else:
      list_cov_syst.append(cov)
  ## make the sum of syst if req
  if collapse_syst:
    covar_matrices['syst'] = sum(list_cov_syst)

  ## Finally
  return covar_matrices


def correlation_matrix():
  """
  Single final correlation matrix over all channels.
  """
  cov   = sum(cov_matrices().values()).tolist()
  vals  = list(fast_csc.CSC_ALL.loc['val'].values)
  vals  = uncertainties.correlated_values(vals, cov)
  return uncertainties.correlation_matrix(vals)


#===============================================================================
# COMBINING CSC
#===============================================================================

def latex_csc_full():
  """
  Combined Z->tautau cross-section from all channels into one, 
  with expanded uncertainties from all sources.
  """
  result = get_result()
  msg0   = """
$$
\\sigma_{\\decay{\\Z}{\\tautau}} = %s
$$
"""
  msg1 = '{:.1f} \pm {stat:.1f} \pm {syst:.1f} \pm {beam:.1f} \pm {lumi:.1f} \pb'
  msg1 = msg1.format(result['nominal_value'], **result['errors'])
  return msg0%msg1


def latex_csc_compact():
  """
  Like above, but combine all sources of uncertainty into one. 
  For the conclusion paragraph.
  """
  result = get_result()
  csc    = ufloat(result['nominal_value'], sumq(result['errors'].values()))
  return '\\num{{{:.1f}}}\\pb'.format(csc).replace('+/-', ' +- ')


def main_report_combination():
  result = get_result()
  print '\nResult summary'
  print latex_csc_full()
  print 'Chi2/ndf: {:7.3f}'.format(result['chi2pdof'])
  print latex_csc_compact()


@memorized
def calc_repack_zll():
  """
  Return a dataframe of consistent structure: val, stat, syst, beam, lumi, error_total
  """ 
  result = get_result()

  ## Calc the total error
  df = ZLL_CSC.copy()

  ## Package ztautau
  ztautau = pd.Series({
    'val' : result['nominal_value'],
    'stat': result['errors']['stat'],
    'syst': result['errors']['syst'],
    'beam': result['errors']['beam'],
    'lumi': result['errors']['lumi'],
    # 'error_total': mathutils.sumq(*result['errors'].values()),
  })
  df['ztautau'] = ztautau

  ## Package the channel of di-tau, collapse the syst together
  taus = fast_csc.CSC_ALL.T.copy()
  keys = list(set(taus.columns) - {'stat','lumi','beam','val'})
  taus['syst'] = taus.apply(lambda se: sumq(se[keys]), axis=1)
  df2 = taus[['val', 'stat', 'syst', 'beam', 'lumi']]

  ## finally, concat, reorder (di-tau, tautau, mumu, ee)
  df = pd.concat([df2, df.T.iloc[::-1]])
  df['error_total'] = df.apply(lambda se: sumq(se.values[1:]), axis=1)
  return df

#===============================================================================
# LEPTON UNIVERSALITY TEST
#===============================================================================

def repack_correlation_lepuniv(df_repack):
  """
  Repack with specific correlation between leptons of same year
  
  Input:
  - Rows   : zee, zmumu, ztautau
  - Columns: val, stat, syst, beam, lumi

  """
  ## Base corr mat
  N = 3 # e, mu, tau
  O = pd.np.ones((N,N))  # square of ones , for completely correlated values
  I = pd.np.identity(N)  # identity matrix, for completely uncorrelated values

  leptons = df_repack.loc[['ztautau', 'zmumu', 'zee']]
  csc     = [ ufloat(x,0.) for x in leptons['val'] ]
  stat    = uncer.correlated_values_norm([(0.,x) for x in leptons['stat']], I)   # uncor
  syst    = uncer.correlated_values_norm([(0.,x) for x in leptons['syst']], I)   # uncor
  lumi    = uncer.correlated_values_norm([(0.,x) for x in leptons['lumi']], O)   # cor
  queues = [csc, stat, syst, lumi]

  ## Add beam energy if available
  if all(x for x in leptons['beam']):
    beam = uncer.correlated_values_norm([(0.,x) for x in leptons['beam']], O)   # cor
    queues.append(beam)

  return [sum(l) for l in zip(*queues)]


def calc_lepton_univ():
  """
  Return a repacked DF with both 7,8 TeV
  """
  def zll_to_series(df0):
    """
    Required a packed dataframe ready for `repack_correlation_lepuniv`
    """
    ztautau, zmumu, zee = repack_correlation_lepuniv(df0)
    return pd.Series({
      'ze'    : zee,
      'zmu'   : zmumu,
      'ztau'  : ztautau,
      'mu/e'  : zmumu   / zee,
      'tau/mu': ztautau / zmumu,
      'tau/e' : ztautau / zee,
    })
  return pd.DataFrame({
    7: zll_to_series(ZLL7_CSC.T),
    8: zll_to_series(calc_repack_zll()),
  })


def main_lepton_univ():
  """
  Pretty print in console
  """
  columns = {
    7: '7 TeV',
    8: '8 TeV',
  }
  indices = {
    'ze'    : 'CSC : Z -> ee',
    'zmu'   : 'CSC : Z -> mumu',
    'ztau'  : 'CSC : Z -> tautau',
    'mu/e'  : 'UNIV: mu/e',
    'tau/e' : 'UNIV: tau/e',
    'tau/mu': 'UNIV: tau/mu',
  }
  df = calc_lepton_univ().rename(indices, columns)
  print df.applymap('{:.4f}'.format)


def _latex_lepton_univ(energy):
  """
  For latex
  """
  msg = """
$$
\\frac{{\\sigma^{{{0}\\tev}}_{{\\ztautau}} }}{{\\sigma^{{{0}\\tev}}_{{\\zmumu }} }} = {1}
\\quad , \\quad
\\frac{{\\sigma^{{{0}\\tev}}_{{\\ztautau}} }}{{\\sigma^{{{0}\\tev}}_{{\\zee }} }} = {2}
\\quad , \\quad
\\frac{{\\sigma^{{{0}\\tev}}_{{\\zmumu}} }}{{\\sigma^{{{0}\\tev}}_{{\\zee }} }} = {3}
$$
""".strip()
  se = calc_lepton_univ()[energy]

  fmt = lambda u: '{:.1f}'.format(u).replace('+/-', ' \\pm ')

  ## Ready
  return msg.format(energy,
    fmt(se['tau/mu']),
    fmt(se['tau/e']),
    fmt(se['mu/e']),
  )

latex_lepton_univ7 = lambda: _latex_lepton_univ(7)
latex_lepton_univ8 = lambda: _latex_lepton_univ(8)


#===============================================================================
# 8 TeV / 7 TeV
#===============================================================================

def repack_correlation_8to7(se8, se7):
  """
  """
  ## Base corr mat
  N = 2 # 8, 7
  O = pd.np.ones((N,N))  # square of ones , for completely correlated values
  I = pd.np.identity(N)  # identity matrix, for completely uncorrelated values

  raws  = pd.DataFrame({'8': se8, '7': se7}).T
  csc   = [ ufloat(x,0.) for x in raws['val'] ]
  stat  = uncer.correlated_values_norm([(0.,x) for x in raws['stat']], I) # uncor
  syst  = uncer.correlated_values_norm([(0.,x) for x in raws['syst']], O) # cor
  lumi  = uncer.correlated_values_norm([(0.,x) for x in raws['lumi']], I) # uncor
  queues = [csc, stat, syst, lumi]

  ## Add beam energy if available
  if all(x for x in raws['beam']):
    beam = uncer.correlated_values_norm([(0.,x) for x in raws['beam']], I) # uncor
    queues.append(beam)

  ## Finally
  return [sum(l) for l in zip(*queues)]

def _div_fullcorr_8to7(se8, se7):
  """
  ad-hoc method to calculate theoretical csc ratio.
  I'm so tired now.
  """
  O = pd.np.ones((2,2))  # square of ones, for completely correlated values
  high8, high7 = uncer.correlated_values_norm([(se.val, se.errs_high) for se in [se8,se7]], O)
  low8 , low7  = uncer.correlated_values_norm([(se.val, se.errs_low)  for se in [se8,se7]], O)
  # for simplicity sake, collapse asym to sym
  rh = high8/high7
  rl = low8/low7
  return asymvar(rh.n, rh.s, rl.s).to_ufloat()


def calc_corr_8to7():
  tup = lambda zll: repack_correlation_8to7(calc_repack_zll().loc[zll], ZLL7_CSC[zll])
  df  = pd.DataFrame({zll:tup(zll) for zll in ('zee', 'zmumu', 'ztautau')}).T
  ## Ready, assign ratio
  df.columns = 7, 8
  df['ratio'] = df[8]/df[7]
  ## add theoretical ratio, assume fully corr
  se8   = ZLL_THEO_CSC.loc['MSTW08']
  se7   = ZLL7_THEO_CSC.loc['MSTW08']
  ratio = _div_fullcorr_8to7(se8, se7)
  val8  = asymvar(se8.val, se8.errs_high, se8.errs_low)
  val7  = asymvar(se7.val, se7.errs_high, se7.errs_low)
  df.loc['MSTW08'] = (val8, val7, ratio)
  return df

def _fmt_8to7(se):
  fmt = '\\frac{{\\sigma^{{8\\tev}}_{{\\{0} }}}}{{\\sigma^{{7\\tev}}_{{\\{0} }}}} = {1:.3f}'
  return fmt.format(se.name, se['ratio']).replace('+/-', ' \\pm ')

def latex_8to7_ztau():
  return '$$\n%s\n$$'%_fmt_8to7(calc_corr_8to7().loc['ztautau'])

def latex_8to7_zmue():
  s1 = _fmt_8to7(calc_corr_8to7().loc['zee'])
  s2 = _fmt_8to7(calc_corr_8to7().loc['zmumu'])
  return '$$\n%s\n \quad , \quad \n%s\n$$'%(s1, s2)


#===============================================================================
# Corr printers 
#===============================================================================

def latex_chi2pdof():
  ## Just value chi2/dof
  return '%.1f'%get_result()['chi2pdof']

def _latex_mat(mat, force_diagonal=None):
  """
  Correlation matrix for the appendix
  """
  fmt = lambda x: '1' if x==1. else '0' if x==0. else '%.4f'%x
  df  = pd.DataFrame(mat).applymap(fmt)
  # force again 1. in the diagonal
  if force_diagonal:
    for i in xrange(7):
      df.iloc[i,i] = force_diagonal
  l   = df.to_latex(index=False).split('\n')[4:-3]
  msg = ''
  msg += '\\begin{pmatrix*}[r]'
  msg += '\n'.join(l)
  msg += '\\end{pmatrix*}'
  return msg

def latex_corrmat_br():
  return _latex_mat(CORR_BR(), force_diagonal='1')
def latex_corrmat_erec():
  return _latex_mat(CORR_EREC(), force_diagonal='1')
def latex_corrmat_esel():
  return _latex_mat(CORR_ESEL(), force_diagonal='1')
def latex_covmat():
  return _latex_mat(sum(cov_matrices().values()))
def latex_weights():
  return _latex_mat(get_result()['weights'])
def latex_corrmat():
  return _latex_mat(correlation_matrix(), force_diagonal='1')

#===============================================================================

@memorized
def get_result(collapse_syst=True):
  ## Perform the combine
  return combine_BLUE(fast_csc.CSC_ALL.loc['val'], **cov_matrices(collapse_syst))

def get_result_debug():
  """
  Debugging, interest in chi2 of stat-only combination
  """
  def calc(tag):
    kwargs = {tag:cov_matrices()[tag]}
    # print kwargs
    res = combine_BLUE(fast_csc.CSC_ALL.loc['val'], **kwargs)
    print res['weights']
    return pd.Series({
      'csc'     : res['nominal_value'],
      'err'     : res['errors'].values()[0],
      'chi2pdof': res['chi2pdof'],
    })

  acc = {}
  acc['stat-only'] = calc('stat')
  # acc['syst-only'] = calc('syst')
  print pd.DataFrame(acc)

 
#===============================================================================

def test_blue():
  """
  Against example in Valassi
  "Combining correlated measurements of several different physical quantities"
  """
  ## Expect: (10.96 +- 0.87)%, chi2/dof = 2.19/3
  values = 0.1050, 0.1350, 0.0950, 0.1400
  covar = matrix([
    [1e-4, 0, 0, 0],
    [0, 9e-4, 0, 0],
    [0, 0, 9e-4, 0],
    [0, 0, 0, 9e-4],
  ])
  print combine_BLUE(values, err=covar)

  ## Expect: (10.87 +- 0.89)%, chi2/dof = 2.32/3
  covar = matrix([
    [1e-4, 0.45e-4, 0, 0],
    [0.45e-4, 9e-4, 0, 0],
    [0, 0, 9e-4, 0],
    [0, 0, 0, 9e-4],
  ])
  print combine_BLUE(values, err=covar)

  ## Expect: (10.71 +- 0.90)%, chi2/dof = 4.01/3
  covar = matrix([
    [1e-4, 0, 0, 0],
    [0, 9e-4, 0, 8.96e-4],
    [0, 0, 9e-4, 0],
    [0, 8.96e-4, 0, 9e-4],
  ])
  print combine_BLUE(values, err=covar)

  ## Dummy vars
  values = 95, 100
  covar = matrix([
    [5**2, 0],
    [0, 5**2],
  ])
  print combine_BLUE(values, err=covar)

#===============================================================================

if __name__ == '__main__':
  pass

  # ## DEV
  # print cov_matrices(False)
  # print cov_matrices()['stat']
  # print calc_repack_zll()
  # print get_result()
  # print (pd.Series(get_result(False)['errors'])/get_result(False)['nominal_value']).fmt2p
  # test_blue()
  # print get_result_debug()

  # ## report the result
  main_report_combination()

  # ## Lepton univ test
  # main_lepton_univ()

  # ## 8/7 TeV test
  # print calc_corr_8to7()

  ## Correlation matrices
  # print latex_chi2pdof()
  # print latex_corrmat_br()
  # print latex_corrmat_erec()
  # print latex_corrmat_esel()
  # print latex_covmat()
  # print latex_weights()
  # print latex_corrmat()
  # print correlation_matrix()
  # print latex_lepton_univ7()
  # print latex_lepton_univ8()
  # print latex_8to7_ztau()
  # print latex_8to7_zmue()

  # # ## Manual stat (uncorr) computation
  # # val = pd.Series([97.51, 95.60, 88.27, 90.25, 95.71, 101.92, 91.36])
  # # err = pd.Series([ 6.76,  3.58,  7.11, 15.89,  6.33,  13.35,  3.15])
  # val = fast_csc.CSC_ALL.loc['val']
  # err = fast_csc.CSC_ALL.loc['stat']
  # # print err**-2
  # w   = (1./err)**2
  # # print w.sum()
  # w  /= w.sum()
  # # print w
  # mean = (val*w).sum()
  # # print mean
  # chi2 = ((val-mean)**2 / err**2).sum()
  # print chi2, chi2/6
