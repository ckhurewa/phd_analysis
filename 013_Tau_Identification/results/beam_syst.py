#!/usr/bin/env python

import re
import pandas as pd
from uncertainties import ufloat
from PyrootCK import *

def load():
  acc = {}
  pat = re.compile(r'/default-(?P<energy>\w+)/stdout Cross section is    (?P<nom>\S+) \+/-   (?P<err>\S+) fb')
  with open('beam_syst/result.txt') as fin:
    for line in fin:
      result = pat.search(line).groupdict()
      energy = int(result['energy'])
      nom    = float(result['nom'])
      err    = float(result['err'])
      acc[energy] = ufloat(nom, err)/1e3
  return pd.Series(acc)

def main():
  ## Load & draw & fit
  data = load()
  gr = ROOT.TGraphErrors.from_pair_ufloats(*zip(*data.iteritems()))
  gr.Draw('AP')
  gr.markerSize = 1.0
  gr.xaxis.title = '#sqrt{s} [GeV]'
  gr.yaxis.title = '#sigma_{Zll}^{LHCb} [pb]'
  gr.Fit('pol1')

  ## Decorate the fit line
  fit = gr.functions.FindObject("pol1")
  fit.lineColor = ROOT.kRed
  p0 = fit.GetParameter(0)
  p1 = fit.GetParameter(1)

  text = ROOT.TPaveText(0.2, 0.8, 0.7, 0.9, 'NDC')
  text.AddText('#sigma = %.3f + %.3f*#sqrt{s}'%(p0, p1))
  text.fillColor = 4000
  text.fillStyle = 0
  text.borderSize = 0
  text.Draw()

  ## Finally, save
  ROOT.gPad.Update()
  ROOT.gPad.SaveAs('beam_syst/beam_cscbr.pdf')

  ## calc beam uncer from fit result
  calc_csc = lambda energy: fit.GetParameter(0) + fit.GetParameter(1)*energy
  calc_syst = lambda esyst: calc_csc(8000*(1.+esyst))/calc_csc(8000) - 1.
  print 'Beam rerr 0.65%% --> csc rerr %.2f%%'%(calc_syst(0.0065)*100.)
  print 'Beam rerr 0.10%% --> csc rerr %.2f%%'%(calc_syst(0.0010)*100.)


if __name__ == '__main__':
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  main()  
