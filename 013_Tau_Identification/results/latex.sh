#!/bin/bash

## Vars
PROJ=${HOME_EPFL}/Papers/004_Z02TauTau_2012
DATE=`date +%y%m%d`

## Perform necessary protocols to update result from this area to latex
## Update normalizer, archive & cp
./normalizer.py --recalc
rm -vrf normalizer/$DATE
mkdir normalizer/$DATE
mv -v normalizer/*.txt normalizer/$DATE
rm -vrf $PROJ/tables/*.txt  # assume there's only normalizer
cp -v normalizer/$DATE/*.txt $PROJ/tables

## Cross-section figures
./combine_csc.py
cp -v compare_csc.pdf ${PROJ}/figs/compare_csc/${DATE}.pdf
cp -v compare_csc.pdf ${PROJ}/figs/compare_csc/latest.pdf

# delete *.tex cache, so that the markdown source pick up new value
# note : normalizer doesn't need markdown refresh, it's injected directly in latex.
rm -vf $PROJ/ANA/30_results.tex

echo
echo "Don't forget to replace csc in title page!"
