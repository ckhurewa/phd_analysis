#!/usr/bin/env python
"""

CROSS-SECTION x GENERATOR-EFFICIENCY
Unit in millibarn, as output by Pythia

"""

from PyrootCK import ufloat

#===============================================================================

ztautau0       = ufloat( 8.568E-07, 3.16E-09 ) * ufloat( 3.70E-01, 2.94E-03 ) # NOTE: Central version, not custom
# ztautau0       = ufloat( 9.437E-07, 3.16E-09 ) * ufloat( 3.70E-01, 2.94E-03 ) # Replaced CSC to NLO

dymu           = ufloat( 4.466E-06, 4.58E-08 ) * ufloat( 3.91E-01, 9.65E-03 )
dye10          = ufloat( 4.385E-06, 4.46E-08 ) * ufloat( 3.83E-01, 9.51E-03 )
# dye40          = ufloat( 9.555E-07, 3.09E-09 ) * ufloat( 3.68E-01, 2.93E-03 ) # removed
zbb            = (3.8480E-6)                   * ufloat( 1.61E-01, 4.68E-03 )
wtaujet        = ufloat( 1.313E-05, 1.11E-07 ) * ufloat( 2.81E-01, 7.54E-03 )
wmujet         = ufloat( 1.317E-05, 1.09E-07 ) * ufloat( 2.72E-01, 7.34E-03 )
wmu17jet       = ufloat( 1.318E-05, 1.02E-07 ) * ufloat( 2.36E-01, 6.52E-03 )
we             = ufloat( 9.310E-06, 2.84E-08 ) * ufloat( 2.59E-01, 2.23E-03 )
wejet          = ufloat( 1.315E-05, 1.10E-07 ) * ufloat( 2.77E-01, 7.44E-03 )
we20jet        = ufloat( 1.315E-05, 1.10E-07 ) * ufloat( 2.55E-01, 1.56E-02 )
wmumujet       = (1.3180E-5)                   * (7.45E-05)
# wtaubb         = -1 # need alpgen, come back for me later
ztaujet        = ufloat( 1.256E-06, 1.23E-08 ) * ufloat( 3.76E-01, 9.39E-03 )
zmu17jet       = ufloat( 1.239E-06, 1.15E-08 ) * ufloat( 3.36E-01, 8.67E-03 )
zmu17cjet      = ufloat( 5.717E-07, 1.02E-09 ) * ufloat( 1.22E-02, 3.83E-04 )
zmu17bjet      = ufloat( 5.714E-07, 6.53E-10 ) * ufloat( 5.05E-03, 1.59E-04 ) 
hardqcd        = ufloat( 4.341E-01, 2.06E-04 ) * ufloat( 7.79E-03, 7.76E-05 )
cc_harde       = ufloat( 1.641E-03, 1.12E-06 ) * ufloat( 1.52E-04, 1.64E-05 )
cc_hardmu      = ufloat( 1.644E-03, 1.17E-06 ) * ufloat( 1.78E-04, 1.79E-05 )
bb_harde       = ufloat( 1.542E-03, 7.80E-07 ) * ufloat( 8.89E-04, 2.81E-05 )
bb_hardmu      = ufloat( 1.544E-03, 8.05E-07 ) * ufloat( 9.44E-04, 2.98E-05 )
mcmb_mux       = ufloat( 4.064E-01, 1.44E-04 ) * ufloat( 2.16E-05, 3.05E-06 )
ttbar_41900006 = ufloat( 1.521E-07, 5.31E-10 ) * ufloat( 4.92E-02, 1.52E-03 )
ttbar_41900007 = ufloat( 4.360E-08, 1.97E-10 ) * ufloat( 7.85E-02, 2.38E-03 )
ttbar_41900010 = ufloat( 2.114E-07, 1.83E-10 ) * ufloat( 3.00E-02, 5.39E-03 ) 
WW_ll          = ufloat( 1.597E-09, 4.06E-12 ) * ufloat( 2.99E-01, 2.51E-03 )
WW_lx          = ufloat( 3.433E-08, 4.45E-11 ) * ufloat( 7.78E-02, 7.47E-04 )
WZ_lx          = ufloat( 1.228E-08, 1.34E-11 ) * ufloat( 5.55E-02, 5.40E-04 )

#===============================================================================
