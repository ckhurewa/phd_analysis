#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fast_csc # tethered
from ditau_utils import (
  pd, ROOT, CHANNELS, COMPUTE_LABELS,LABELS_DITAU, 
  ufloat, drawers, gpad_save, get_current_dtype_dt,
)
from PyrootCK import isinstance_ufloat
from uncertainties import ufloat, nominal_value
nan = pd.np.nan

#-------------------------------------------------------------------------------

def propagate_rerr_downward(df):
  """
  For each column, iterate through each row and propagate the rerr downward.
  Skip the NaN in-between.
  """
  for dt,se in df.iteritems():
    ## keep record of current rerr, separated by tags
    list_rerr = {}
    for i,(st,val) in enumerate(se.iteritems()):
      # If found an ufloat, update current rerr
      if isinstance_ufloat(val):
        for tag in val.tags:
          list_rerr[tag] = val.get_rerr(tag)
      # If found good float, add current rerr.
      elif not pd.np.isnan(val):
        newval = ufloat(val, 0)
        for tag, rerr in list_rerr.iteritems():
          newval *= ufloat(1, rerr, tag)
        df.loc[st,dt] = newval

def _manual_propg_rerr(df, dt, src, target):
  """
  Manually apply rerr from src to target,
  make sure the contributions from different sources carry forward.
  """
  target0 = df.loc[target,dt]
  src0    = df.loc[src,dt]
  newval  = ufloat(target0.n, 0)
  for tag in src0.tags:
    newval *= ufloat(1, src0.get_rerr(tag), tag)
  df.loc[target,dt] = newval  

#-------------------------------------------------------------------------------

nsig = pd.DataFrame({
  #   mumu    h1mu     h3mu    ee      eh1     eh3    emu
  1: [213.39,  930.58, 172.39,  96.97, 323.22, 77.38,  755.33],
  2: [245.99, 1153.91, 203.94, 133.77, 480.10, 88.63, 1039.89],
  3: [213.42, nan    , nan   ,  55.07, nan   , nan  , nan    ],
  4: [nan   , 942.49 , nan   ,  nan  , 421.14, nan  , nan    ],
  5: [250.09, 947.79 , nan   ,  64.88, 420.22, nan  , nan    ],
  6: [427.67, nan    , nan   ,  81.62, nan   , nan  , nan    ],
  7: [369.89, nan    , nan   ,  75.73, nan   , nan  , nan    ],
  8: [nan   , nan    , 216.01,  nan  , nan   , 96.77, nan    ],
  9: [nan   , nan    , 181.19,  nan  , nan   , 73.64, nan    ],
  10:[nan   , nan    , 171.36,  nan  , nan   , 68.66, nan    ],
  #
  14: [376.13, nan   , nan   , 145.70, nan   , nan  , nan    ],
  15: [nan   , 959.66, 166.02, nan   , 424.51, 74.56, 1034.49],
  16: [nan   , 965.88, 163.92, 127.56, 427.51, 72.79, 1014.28],
  17: [nan   , nan   , nan   , 127.32, nan   , nan  , 1003.05],
}, index=CHANNELS).T

## V1
nsig.loc[1,  'mumu'] = 213.39420 + ufloat( 0,  15.82606, "stat" ) + ufloat( 0,   6.44652, "nsig" )
nsig.loc[1,  'h1mu'] = 930.58133 + ufloat( 0,  35.98558, "stat" ) + ufloat( 0,  24.08866, "nsig" )
nsig.loc[1,  'h3mu'] = 172.39482 + ufloat( 0,  14.70132, "stat" ) + ufloat( 0,   8.92660, "nsig" )
nsig.loc[1,  'ee'  ] =  96.97283 + ufloat( 0,  15.70171, "stat" ) + ufloat( 0,  20.20866, "nsig" )
nsig.loc[1,  'eh1' ] = 323.21503 + ufloat( 0,  29.74554, "stat" ) + ufloat( 0, 101.76308, "nsig" )
nsig.loc[1,  'eh3' ] =  77.38076 + ufloat( 0,  11.03449, "stat" ) + ufloat( 0,  10.90720, "nsig" )
nsig.loc[1,  'emu' ] = 755.32693 + ufloat( 0,  28.96512, "stat" ) + ufloat( 0,  35.47702, "nsig" )
## V2
nsig.loc[7, 'mumu'] =  369.90144 + ufloat( 0,  27.61869, "stat" ) + ufloat( 0,  20.39235, "nsig" )
nsig.loc[5, 'h1mu'] =  947.79100 + ufloat( 0,  36.18753, "stat" ) + ufloat( 0,  63.40336, "nsig" )
nsig.loc[10,'h3mu'] =  171.36186 + ufloat( 0,  13.47001, "stat" ) + ufloat( 0,   8.87173, "nsig" )
nsig.loc[7, 'ee'  ] =   75.73367 + ufloat( 0,  14.39169, "stat" ) + ufloat( 0,  18.31402, "nsig" )
nsig.loc[5, 'eh1' ] =  420.22221 + ufloat( 0,  28.18966, "stat" ) + ufloat( 0,  38.07349, "nsig" )
nsig.loc[10,'eh3' ] =   68.66147 + ufloat( 0,   9.58730, "stat" ) + ufloat( 0,  14.52662, "nsig" )
nsig.loc[2, 'emu' ] = 1039.89333 + ufloat( 0,  35.79332, "stat" ) + ufloat( 0,  44.16395, "nsig" )
## current=v4=v3
nsig.loc[14, 'mumu'] =  376.1313077 + ufloat( 0, 26.069721, 'stat') + ufloat( 0, 12.691492, 'nsig')
nsig.loc[16, 'h1mu'] =  965.8769483 + ufloat( 0, 36.220706, 'stat') + ufloat( 0, 37.493549, 'nsig')
nsig.loc[16, 'h3mu'] =  163.9169310 + ufloat( 0, 13.198909, 'stat') + ufloat( 0,  5.299801, 'nsig')
nsig.loc[17, 'ee'  ] =  127.3239310 + ufloat( 0, 22.415402, 'stat') + ufloat( 0, 24.246759, 'nsig')
nsig.loc[16, 'eh1' ] =  427.5087351 + ufloat( 0, 28.255370, 'stat') + ufloat( 0, 22.006251, 'nsig')
nsig.loc[16, 'eh3' ] =   72.7892643 + ufloat( 0,  9.537230, 'stat') + ufloat( 0,  5.806995, 'nsig')
nsig.loc[17, 'emu' ] = 1003.0522079 + ufloat( 0, 34.550536, 'stat') + ufloat( 0, 23.637354, 'nsig')

## Propagate uncertainty downward, ignore nan
propagate_rerr_downward(nsig)

## Fine-tune rerr to be less misleading
_manual_propg_rerr(nsig, 'mumu', src=7, target=6) # DOCACHI2 syst
_manual_propg_rerr(nsig, 'ee'  , src=7, target=6)
_manual_propg_rerr(nsig, 'h1mu', src=5, target=4) # tauh1 IP cut
_manual_propg_rerr(nsig, 'eh1' , src=5, target=4)
_manual_propg_rerr(nsig, 'eh1' , src=5, target=2) # taue IP cut
_manual_propg_rerr(nsig, 'h3mu', src=16, target=10) # from ZXC frac, but delegated to assisted
_manual_propg_rerr(nsig, 'eh3' , src=16, target=10)
_manual_propg_rerr(nsig, 'h3mu', src=16, target=15)
_manual_propg_rerr(nsig, 'eh3' , src=16, target=15)
_manual_propg_rerr(nsig, 'ee'  , src=17, target=16) # stripbug
_manual_propg_rerr(nsig, 'emu' , src=17, target=16)

#-------------------------------------------------------------------------------

esel = pd.DataFrame({
  1: [14.40, 32.10, 29.50, 17.00, 31.20, 27.00, 36.70],
  #
  2: [15.9611, 39.1745, 35.4277, 19.6433, 38.2746, 33.1364, 49.80],
  3: [13.8392, nan    , nan    ,  8.8306, nan    , nan    , nan  ],
  4: [nan    , 35.6341, nan    , nan    , 34.7260, nan    , nan  ],
  5: [15.5633, 35.7916, nan    ,  9.4518, 34.8193, nan    , nan  ],
  6: [27.5816, nan    , nan    , 11.3946, nan    , nan    , nan  ],
  7: [23.4704, nan    , nan    ,  9.7034, nan    , nan    , nan  ],
  8: [nan    , nan    , 37.5472, nan    , nan    , 34.8313, nan  ],
  9: [nan    , nan    , 30.6300, nan    , nan    , 28.7172, nan  ],
  10:[nan    , nan    , 29.5435, nan    , nan    , 27.7826, nan  ],
  #
  12: [23.10, 34.75, 29.13, 10.85, 33.83, 27.35, 49.15],
  13: [23.14, 35.77, 32.92, 10.97, 34.83, 30.97, 49.93],
  14: [23.33, nan  , nan  , 20.26, nan  , nan  , nan  ],
  15: [nan  , 35.92, 33.99, nan  , 34.93, 31.71, 50.11],
  16: [nan  , nan  , nan  , nan  , nan  , nan  , 50.58],
  23: [22.74, 35.16, nan  , 19.71, 34.20, nan  , nan  ],
  24: [22.78, 35.17, 33.92, 19.73, 34.18, 31.65, 50.59],
  #
  25: [22.66, 35.00, 33.78, 19.58, 34.02, 31.48, 50.28], # v4.4, DPHI
}, index=CHANNELS).T / 100. # in percent

## V1
esel.loc[1, 'mumu'] = ufloat( 0.143893, 0.004594, 'sel' )
esel.loc[1, 'h1mu'] = ufloat( 0.320709, 0.007086, 'sel' )
esel.loc[1, 'h3mu'] = ufloat( 0.295192, 0.006779, 'sel' )
esel.loc[1, 'ee'  ] = ufloat( 0.170172, 0.006221, 'sel' )
esel.loc[1, 'eh1' ] = ufloat( 0.311515, 0.007507, 'sel' )
esel.loc[1, 'eh3' ] = ufloat( 0.269562, 0.008053, 'sel' )
esel.loc[1, 'emu' ] = ufloat( 0.366958, 0.008422, 'sel' )
## V2
esel.loc[7 , 'mumu'] = ufloat( 0.2346802, 0.0130820, 'sel' )
esel.loc[5 , 'h1mu'] = ufloat( 0.3579235, 0.0177365, 'sel' )
esel.loc[10, 'h3mu'] = ufloat( 0.2953520, 0.0117917, 'sel' )
esel.loc[7 , 'ee'  ] = ufloat( 0.0970128, 0.0067894, 'sel' )
esel.loc[5 , 'eh1' ] = ufloat( 0.3481581, 0.0170356, 'sel' )
esel.loc[10, 'eh3' ] = ufloat( 0.2777126, 0.0122487, 'sel' )
esel.loc[2 , 'emu' ] = ufloat( 0.4980490, 0.0200577, 'sel' )
## current=v4
esel.loc[24, 'mumu'] = ufloat( 0.2277769, 0.0114463, 'sel' )
esel.loc[24, 'h1mu'] = ufloat( 0.3516573, 0.0123052, 'sel' )
esel.loc[24, 'h3mu'] = ufloat( 0.3391706, 0.0160808, 'sel' )
esel.loc[24, 'ee'  ] = ufloat( 0.1973441, 0.0112193, 'sel' )
esel.loc[24, 'eh1' ] = ufloat( 0.3418114, 0.0120586, 'sel' )
esel.loc[24, 'eh3' ] = ufloat( 0.3164674, 0.0159816, 'sel' )
esel.loc[24, 'emu' ] = ufloat( 0.5059364, 0.0195091, 'sel' )
## v4.4
esel.loc[25, 'mumu'] = ufloat( 0.2265838, 0.0108404, 'sel' )
esel.loc[25, 'h1mu'] = ufloat( 0.3499581, 0.0111089, 'sel' )
esel.loc[25, 'h3mu'] = ufloat( 0.3377921, 0.0153578, 'sel' )
esel.loc[25, 'ee'  ] = ufloat( 0.1958455, 0.0105011, 'sel' )
esel.loc[25, 'eh1' ] = ufloat( 0.3402090, 0.0109431, 'sel' )
esel.loc[25, 'eh3' ] = ufloat( 0.3148111, 0.0151440, 'sel' )
esel.loc[25, 'emu' ] = ufloat( 0.5027637, 0.0174102, 'sel' )

## Propagate uncertainty upward, ignore nan
propagate_rerr_downward(esel)

#-------------------------------------------------------------------------------

erec = pd.DataFrame({
  1: [69.10, 52.90, 25.80, 29.70, 23.40, 10.80, 46.70],
  2: [nan  , 53.58, 25.93, nan  , 23.86, 11.22, 46.78],
  6: [66.58, nan  , nan  , 31.97, nan  , nan  , nan  ],
  #
  11: [nan  , nan  , 24.00, nan  , nan  , 10.35, nan  ],
  #
  14: [65.88, nan  , nan  , 30.78, nan  , nan  , nan  ],
  16: [nan  , nan  , nan  , nan  , nan  , nan  , 46.64],
  17: [nan  , nan  , nan  , 29.00, nan  , nan  , 46.09],
  18: [nan  , 51.94, 22.45, nan  , 22.46, 9.43 , nan  ],
  19: [nan  , nan  , nan  , 27.72, 21.71, 9.16 , 44.97],
  20: [nan  , 53.11, 20.94, nan  , 22.83, 8.68 , nan  ],
  21: [nan  , nan  , nan  , 28.77, 23.28, 8.89 , 46.25],
  22: [64.76, 52.63, 20.75, nan  , nan  , nan  , 45.80],
}, index=CHANNELS).T / 100. # in percent

## V1
erec.loc[1, 'mumu'] = ufloat( 0.690692, 0.013616, "rec" )
erec.loc[1, 'h1mu'] = ufloat( 0.529249, 0.015393, "rec" )
erec.loc[1, 'h3mu'] = ufloat( 0.258195, 0.014846, "rec" )
erec.loc[1, 'ee'  ] = ufloat( 0.297047, 0.016860, "rec" )
erec.loc[1, 'eh1' ] = ufloat( 0.234277, 0.012117, "rec" )
erec.loc[1, 'eh3' ] = ufloat( 0.108351, 0.008787, "rec" )
erec.loc[1, 'emu' ] = ufloat( 0.466892, 0.013779, "rec" )
## V2
erec.loc[6 , 'mumu'] = ufloat( 0.6657600, 0.0134435, 'rec' )
erec.loc[2 , 'h1mu'] = ufloat( 0.5357726, 0.0159556, 'rec' )
erec.loc[11, 'h3mu'] = ufloat( 0.2399802, 0.0113480, 'rec' )
erec.loc[6 , 'ee'  ] = ufloat( 0.3197058, 0.0216762, 'rec' )
erec.loc[2 , 'eh1' ] = ufloat( 0.2385637, 0.0148425, 'rec' )
erec.loc[11, 'eh3' ] = ufloat( 0.1034642, 0.0079187, 'rec' )
erec.loc[2 , 'emu' ] = ufloat( 0.4678470, 0.0154322, 'rec' )
## current=V3=V4
erec.loc[22, 'mumu'] = ufloat( 0.6476000, 0.0138251, 'rec' )
erec.loc[22, 'h1mu'] = ufloat( 0.5262882, 0.0162238, 'rec' )
erec.loc[22, 'h3mu'] = ufloat( 0.2075162, 0.0115360, 'rec' )
erec.loc[21, 'ee'  ] = ufloat( 0.2877137, 0.0130345, 'rec' )
erec.loc[21, 'eh1' ] = ufloat( 0.2327911, 0.0125841, 'rec' )
erec.loc[21, 'eh3' ] = ufloat( 0.0888795, 0.0062594, 'rec' )
erec.loc[22, 'emu' ] = ufloat( 0.4580826, 0.0122818, 'rec' )

## Propagate uncertainty downward, ignore nan
propagate_rerr_downward(erec)

#-------------------------------------------------------------------------------
# COLLECT
#-------------------------------------------------------------------------------

eacc = fast_csc.DF['acc']
br   = fast_csc.DF['br']
lumi = fast_csc.Lumi()
Csc  = lambda se: se.nsig/lumi/se.br/se.eacc/se.erec/se.esel
NMAX = max([nsig.index.max(), esel.index.max(), erec.index.max()])

## Collect together
DF = pd.concat({
  'nsig': nsig, 
  'erec': erec, 
  'esel': esel,
  'eacc': pd.DataFrame({i:eacc for i in xrange(1, NMAX+1)}).T,
  'br'  : pd.DataFrame({i:br   for i in xrange(1, NMAX+1)}).T,
}, axis=1, names=['field', 'channel'])

## Forward fill the components, for cross-section.
DF = DF.fillna(method='ffill')
DF = DF.stack('channel')
DF['csc'] = DF.apply(Csc, axis=1)
DF = DF.unstack('channel')
DF.index.name = 'stage'
nmax = DF.index[-1]

## Only for cross-section, remove the duplicate to be nan. Opposite of ffill

def get_nan_indices(dt):
  s0 = set(range(1, nmax+1))
  s1 = set(erec[dt].dropna().index)
  s2 = set(esel[dt].dropna().index)
  s3 = set(nsig[dt].dropna().index)
  return sorted(list(s0-(s1|s2|s3)))

for dt in CHANNELS:
  DF.loc[get_nan_indices(dt), ('csc', dt)] = nan

#===============================================================================

def ufloat_stat(u):
  """
  Return new instance of ufloat with only statistical uncertainty,
  handle null entry.
  """
  return u if pd.np.isnan(nominal_value(u)) else ufloat(u.n, u.get_error('stat'))

def add_hook(i,g):
  """
  Handle hook for drawers.graphs_eff
  """
  g.lineWidth   = 1
  g.markerStyle = 0

def add_hook_stat_total(i,g):
  """
  Handle hook for drawers.graphs_eff,
  in case where stat+total uncertainties are separated.
  """
  g.lineWidth = 1
  g.lineColor = ROOT.kBlack
  if i==1:
    g.lineColorAlpha = 1, 0.5
  g.markerStyle = 0
  g.markerColor = ROOT.kBlack

def _draw_value_labels(se, ypos, fmt='{:.1f}', color=ROOT.kBlack):
  """
  Given series of values, draw list of text at given height ypot
  """
  for i,val in se.dropna().iteritems():
    text = ROOT.TText(i, ypos, fmt.format(nominal_value(val)))
    text.ownership = False
    text.textAlign = 21
    text.textColor = color
    text.textSize  *= 0.9
    text.Draw()

def _draw_line_version(ymin=0, ymax=100, skip_v3=True):
  """
  Draw a vertical line signifying the version
  """
  for vname, xmark in [('V1', 1.), ('V2', 11.), ('V3', 22), ('V4.4', 25)]:
    if skip_v3 and vname=='V3':
      continue
    pl = drawers.Line([(xmark,ymin), (xmark,ymax)])
    pl.lineWidth      = 1
    pl.lineColorAlpha = ROOT.kBlack, 0.3
    pl.Draw()
    text = ROOT.TText(xmark, ymax, vname)
    text.ownership = False
    text.textAlign = 13
    text.textColor = ROOT.kBlack
    text.Draw()

def _draw_extending_last(se0):
  se = se0.dropna()
  lx = [se.index[-1], NMAX]
  ly = [se.iloc[-1] , se.iloc[-1]]
  gr = ROOT.TGraphErrors.from_pair_ufloats(lx, ly)
  gr.ownership  = False
  gr.markerSize = 0
  gr.lineStyle  = 3
  gr.lineWidth  = 1
  gr.lineColorAlpha = ROOT.kBlack, 0.6
  gr.Draw()

#-------------------------------------------------------------------------------

CSC_YAXIS = {
  'ee'  : (10, 180),
  'eh1' : (10, 180),
  'eh3' : (10, 180),
  'emu' : (60, 140),
  'h1mu': (60, 140),
  'h3mu': (60, 140),
  'mumu': (70, 140),
}

@gpad_save
def draw_changes(dt):
  ## Base canvas
  c0 = ROOT.TCanvas(dt, dt)
  c0.ownership = False
  c0.Divide(1,3, 0.01,0.001)

  #---------------#
  # CROSS-SECTION #
  #---------------#

  c = c0.cd(1)
  c.topMargin    *= 0.3
  c.leftMargin   *= 0.5
  c.rightMargin  *= 0.0
  c.bottomMargin *= 0.7
  ymin,ymax = CSC_YAXIS[dt]
  
  ## Get the cross-section, separated errors
  se_csc = DF.xs(dt, level='channel', axis=1)['csc']
  df = pd.DataFrame({
    'total': se_csc,
    'stat' : se_csc.apply(ufloat_stat),
  })[['stat', 'total']]
  gr, leg = drawers.graphs_eff(df, add_hook=add_hook_stat_total)
  gr.minimum = ymin
  gr.maximum = ymax
  gr.xaxis.ndivisions  = 40
  gr.yaxis.title       = '#sigma [pb]'
  gr.yaxis.titleOffset *= 0.40
  leg.header   = 'Uncertainties'
  leg.x1NDC    = 0.90
  leg.x2NDC    = 1.00
  leg.y1NDC    = 0.73
  leg.y2NDC    = 0.90
  leg.textSize = 0.075
  _draw_value_labels(df.iloc[:,0], ypos=ymin+(ymax-ymin)*0.08)
  _draw_line_version(ymin=ymin, ymax=ymax)

  ## Show channel
  text = drawers.note(LABELS_DITAU[dt], '')
  text.x1 = 0.15
  text.x2 = 0.21
  text.y1 = 0.75
  text.y2 = 0.90

  #------------#
  # EREC, ESEL #
  #------------#
  ## Get the pre-ffill version to have no redundant entries.

  c = c0.cd(2)
  c.topMargin    *= 0.2
  c.leftMargin   *= 0.5
  c.rightMargin  *= 0.0
  c.bottomMargin *= 0.7
  df = pd.concat({'esel':esel[dt], 'erec':erec[dt]}, axis=1)*100.
  gr, leg = drawers.graphs_eff(df.rename(columns={'erec':'#varepsilon_{rec}','esel':'#varepsilon_{sel}'}), add_hook=add_hook)
  gr.maximum = 100
  gr.xaxis.ndivisions  = 40
  gr.yaxis.title       = 'Efficiencies [%]'
  gr.yaxis.titleOffset *= 0.40
  leg.header = ''
  leg.x1NDC  = 0.92
  leg.x2NDC  = 1.10
  leg.y1NDC  = 0.70
  leg.y2NDC  = 0.98
  #
  ymax_esel = df['esel'].max().n * 1.1
  ymax_erec = df['erec'].max().n * 1.1
  _draw_value_labels(df['erec'], ypos=ymax_erec)
  _draw_value_labels(df['esel'], ypos=ymax_esel, color=ROOT.kRed)
  _draw_line_version()
  _draw_extending_last(df['erec'])

  #------#
  # NSIG #
  #------#

  c = c0.cd(3)
  c.topMargin    *= 0.2
  c.leftMargin   *= 0.5
  c.rightMargin  *= 0.0
  c.bottomMargin *= 0.7
  
  ## Two for stat + total uncertainty
  df = pd.DataFrame({
    'stat' : nsig[dt].apply(ufloat_stat),
    'total': nsig[dt],
  })[['stat', 'total']]
  gr, leg = drawers.graphs_eff(df, add_hook=add_hook_stat_total)
  ## min/max value
  vmax = df['total'].apply(lambda u: u.high if isinstance_ufloat(u) else u).max()
  vmin = df['total'].apply(lambda u: u.low  if isinstance_ufloat(u) else u).min()
  ## tune ymax/ymin to have nice bottom margin
  ymax = gr.maximum = vmax * 1.05
  # ymin = gr.minimum = vmin - (vmax-vmin)*0.3 # relative to span of max-min. Nicer, but easily misleading.
  ymin = 0
  gr.xaxis.limits      = -0.2, NMAX+1.2
  gr.xaxis.ndivisions  = 40
  gr.yaxis.title       = 'N_signal'
  gr.yaxis.titleOffset *= 0.40
  leg.header   = 'Uncertainties'
  leg.x1NDC    = 0.90
  leg.x2NDC    = 1.00
  leg.y1NDC    = 0.73
  leg.y2NDC    = 0.90
  leg.textSize = 0.075
  _draw_value_labels(df['total'], ypos=ymin+(ymax-ymin)*0.06)
  _draw_line_version(ymax=ymax, skip_v3=False)
  _draw_extending_last(df['total'])

  ## Move to global pad to save
  c0.cd()

#===============================================================================

if __name__ == '__main__':
  ## some styling interfere in a loop... I'm not sure why...
  dt = get_current_dtype_dt()[1]
  draw_changes(dt)
