#!/usr/bin/env python
"""

FAST cross-section calculation.

"""

import uncertainties
from PyrootCK import *
from PyrootCK.mathutils import sumq
sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import pd, CHANNELS, DT_TO_LATEX, COMPUTE_LABELS, Lumi, Br_ditau, inject_midrule

## Prepare empty dataframe to hold result
DF = pd.DataFrame(index=CHANNELS)

## recommended order for the relative systs table
RERR_ORDER = 'br', 'acc', 'rec', 'sel', 'nsig', 'syst', 'beam', 'lumi', 'stat'

#===============================================================================
# LUMINOSITY & BRANCHING RATIO
#===============================================================================

DF['lumi'] = Lumi()      # Channel-independent lumi
DF['br']   = Br_ditau()  # Grab the single-tau branching ratio

#===============================================================================
# ACCEPTANCE
#===============================================================================

## J4190, low stats. To be replaced
## J4262: 100Mevt, exclude the leakin
## 160531: Include leak-in correction.
# >> Note: has minor incorrect mass cut logic in ll channel. to be fixed

## 160709, J5035: Large stat, fix ll small masscut mistake.
# add 1.61% syst borrow previous paper
# DF['acc'] = pd.Series({
#   'mumu': ufloat( 0.389621, 0.006273, "acc" ),
#   'h1mu': ufloat( 0.116294, 0.001872, "acc" ),
#   'h3mu': ufloat( 0.181774, 0.002927, "acc" ),
#   'ee'  : ufloat( 0.378022, 0.006086, "acc" ),
#   'eh1' : ufloat( 0.114486, 0.001843, "acc" ),
#   'eh3' : ufloat( 0.178769, 0.002878, "acc" ),
#   'emu' : ufloat( 0.384642, 0.006193, "acc" ),
# })

## 161212: Widening hadron's eta
# DF['acc'] = pd.Series({
#   'mumu': ufloat( 0.389548, 0.006287, "acc" ),
#   'h1mu': ufloat( 0.153081, 0.002468, "acc" ),
#   'h3mu': ufloat( 0.245252, 0.003960, "acc" ),
#   'ee'  : ufloat( 0.378924, 0.006116, "acc" ),
#   'eh1' : ufloat( 0.150666, 0.002429, "acc" ),
#   'eh3' : ufloat( 0.241641, 0.003901, "acc" ),
#   'emu' : ufloat( 0.385519, 0.006215, "acc" ),
# })

## ## 170217: Syst from Aurelio with MSTW2008 variation
DF['acc'] = pd.Series({
  'mumu': ufloat( 0.3895477, 0.0050836, 'acc' ),
  'h1mu': ufloat( 0.1530808, 0.0029117, 'acc' ),
  'h3mu': ufloat( 0.2452522, 0.0036909, 'acc' ),
  'ee'  : ufloat( 0.3789245, 0.0049449, 'acc' ),
  'eh1' : ufloat( 0.1506655, 0.0028658, 'acc' ),
  'eh3' : ufloat( 0.2416412, 0.0036365, 'acc' ),
  'emu' : ufloat( 0.3855189, 0.0050213, 'acc' ),
})


#===============================================================================
# RECONSTRUCTION EFFICIENCY
# (presel INCLUDED)
#===============================================================================

## J4141, ~400kevt
## J4297 ~ 1Mevt, with TRGHP<0.2
## 160410 ee,mumu mass [20,80], ltauh3 mass [30,120]
## 160619: Uses data-driven eff
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.674997, 0.021306, "rec" ),
#   'h1mu': ufloat( 0.549999, 0.018633, "rec" ),
#   'h3mu': ufloat( 0.229938, 0.021568, "rec" ),
#   'ee'  : ufloat( 0.240438, 0.020241, "rec" ),
#   'eh1' : ufloat( 0.251202, 0.011553, "rec" ),
#   'eh3' : ufloat( 0.104284, 0.010138, "rec" ),
#   'emu' : ufloat( 0.433254, 0.015533, "rec" ),
# })

## v160813: Fixed eGEC
## 160813: DOCACHI2>15 for mumu,ee, APT<0.5 for emu
## 160818: APT<0.5 for emu, DRoPT<0.012&DRtPT<5 for h3
## 160822; partial corr, C-P uncer
## 160826: Fix etrack_mc & remove mcMatch eff (pure mc for h,e), etrackmu uses W
## 160827: pid_muon, trig_muon also uses eff from W
## 160828: fix ekine to include migration

# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.662026, 0.007913, "rec" ),
#   'h1mu': ufloat( 0.564968, 0.009826, "rec" ),
#   'h3mu': ufloat( 0.266378, 0.013585, "rec" ),
#   'ee'  : ufloat( 0.268178, 0.011495, "rec" ),
#   'eh1' : ufloat( 0.274785, 0.005927, "rec" ),
#   'eh3' : ufloat( 0.132816, 0.006470, "rec" ),
#   'emu' : ufloat( 0.462119, 0.007793, "rec" ),
# })

## 160831: Revamped SS
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.660913, 0.007898, "rec" ),
#   'h1mu': ufloat( 0.564720, 0.009752, "rec" ),
#   'h3mu': ufloat( 0.270589, 0.013243, "rec" ),
#   'ee'  : ufloat( 0.263972, 0.011160, "rec" ),
#   'eh1' : ufloat( 0.277108, 0.006030, "rec" ),
#   'eh3' : ufloat( 0.107188, 0.007650, "rec" ),
#   'emu' : ufloat( 0.455672, 0.007648, "rec" ),
# })

## 160902: DOCACHI2>0.1 for non-ll channel
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.660913, 0.007898, "rec" ),
#   'h1mu': ufloat( 0.565201, 0.009761, "rec" ),
#   'h3mu': ufloat( 0.267899, 0.013226, "rec" ),
#   'ee'  : ufloat( 0.263972, 0.011160, "rec" ),
#   'eh1' : ufloat( 0.274207, 0.006095, "rec" ),
#   'eh3' : ufloat( 0.105728, 0.007882, "rec" ),
#   'emu' : ufloat( 0.460255, 0.007339, "rec" ),
# })

## 161011: No soft DOCACHI2>0.1, tight >20, tauh3 BPVLTIME, const lumi, Zee norm corr, revise rZ
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.658083, 0.007870, "rec" ),
#   'h1mu': ufloat( 0.565258, 0.009709, "rec" ),
#   'h3mu': ufloat( 0.273208, 0.013146, "rec" ),
#   'ee'  : ufloat( 0.296043, 0.007238, "rec" ),
#   'eh1' : ufloat( 0.273521, 0.006279, "rec" ),
#   'eh3' : ufloat( 0.114018, 0.007915, "rec" ),
#   'emu' : ufloat( 0.454571, 0.007488, "rec" ),
# })

## 161106: Fix etrack MC, trigger+, etrack e/h = litediv, misid mu->h now MC
# remove dd/mc etrack correction, add lh3 correlation correction, fix ekine
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.670921, 0.024244, "rec" ),
#   'h1mu': ufloat( 0.549878, 0.021557, "rec" ),
#   'h3mu': ufloat( 0.263929, 0.016542, "rec" ),
#   'ee'  : ufloat( 0.278360, 0.018707, "rec" ),
#   'eh1' : ufloat( 0.264723, 0.013364, "rec" ),
#   'eh3' : ufloat( 0.108883, 0.009895, "rec" ),
#   'emu' : ufloat( 0.466621, 0.018497, "rec" ),
# })

## 161212: Widening hadron's eta, trigger-, AH-regime
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.690692, 0.013616, "rec" ),
#   'h1mu': ufloat( 0.529249, 0.015393, "rec" ),
#   'h3mu': ufloat( 0.258195, 0.014846, "rec" ),
#   'ee'  : ufloat( 0.297047, 0.016860, "rec" ),
#   'eh1' : ufloat( 0.234277, 0.012117, "rec" ),
#   'eh3' : ufloat( 0.108351, 0.008787, "rec" ),
#   'emu' : ufloat( 0.466892, 0.013779, "rec" ),
# })

# ## 170216: From assisted
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.6657600, 0.0134435, 'rec' ),
#   'h1mu': ufloat( 0.5357726, 0.0159556, 'rec' ),
#   'h3mu': ufloat( 0.2399802, 0.0113480, 'rec' ),
#   'ee'  : ufloat( 0.3197058, 0.0216762, 'rec' ),
#   'eh1' : ufloat( 0.2385637, 0.0148425, 'rec' ),
#   'eh3' : ufloat( 0.1034642, 0.0079187, 'rec' ),
#   'emu' : ufloat( 0.4678470, 0.0154322, 'rec' ),
# })

## 170402: ePIDh with sw, tracking+TRPCHI2 correction, eTrkMC fulldiv
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.7048062, 0.0109624, 'rec' ),
#   'h1mu': ufloat( 0.5307236, 0.0194847, 'rec' ),
#   'h3mu': ufloat( 0.2075279, 0.0135946, 'rec' ),
#   'ee'  : ufloat( 0.3152656, 0.0228813, 'rec' ),
#   'eh1' : ufloat( 0.2322125, 0.0160230, 'rec' ),
#   'eh3' : ufloat( 0.0891320, 0.0071645, 'rec' ),
#   'emu' : ufloat( 0.4594208, 0.0157819, 'rec' ),
# })

## 170409: ePIDe hybrid
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.7048062, 0.0109624, 'rec' ),
#   'h1mu': ufloat( 0.5307236, 0.0159018, 'rec' ),
#   'h3mu': ufloat( 0.2075279, 0.0115378, 'rec' ),
#   'ee'  : ufloat( 0.3161268, 0.0160365, 'rec' ),
#   'eh1' : ufloat( 0.2321231, 0.0123776, 'rec' ),
#   'eh3' : ufloat( 0.0881584, 0.0061929, 'rec' ),
#   'emu' : ufloat( 0.4604025, 0.0148668, 'rec' ),
# })

# ## 170410 fix masswindow bug, ee-other inf bug
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.6656741, 0.0136506, 'rec' ),
#   'h1mu': ufloat( 0.5326291, 0.0159906, 'rec' ),
#   'h3mu': ufloat( 0.2096085, 0.0116475, 'rec' ),
#   'ee'  : ufloat( 0.2768467, 0.0142108, 'rec' ),
#   'eh1' : ufloat( 0.2321567, 0.0122237, 'rec' ),
#   'eh3' : ufloat( 0.0903500, 0.0063248, 'rec' ),
#   'emu' : ufloat( 0.4578001, 0.0148178, 'rec' ),
# })

## 170412: Fix ePIDe to DD
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.6656741, 0.0136506, 'rec' ),
#   'h1mu': ufloat( 0.5326291, 0.0159906, 'rec' ),
#   'h3mu': ufloat( 0.2096085, 0.0116475, 'rec' ),
#   'ee'  : ufloat( 0.2872588, 0.0141761, 'rec' ),
#   'eh1' : ufloat( 0.2338452, 0.0123475, 'rec' ),
#   'eh3' : ufloat( 0.0913360, 0.0063886, 'rec' ),
#   'emu' : ufloat( 0.4651953, 0.0124602, 'rec' ),
# })

## 170413: Fix missing InAcc[E,H]cal, SingleStrip
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.6648761, 0.0135504, 'rec' ),
#   'h1mu': ufloat( 0.5324452, 0.0159547, 'rec' ),
#   'h3mu': ufloat( 0.2100045, 0.0116680, 'rec' ),
#   'ee'  : ufloat( 0.2900529, 0.0132015, 'rec' ),
#   'eh1' : ufloat( 0.2335779, 0.0124135, 'rec' ),
#   'eh3' : ufloat( 0.0923027, 0.0064513, 'rec' ),
#   'emu' : ufloat( 0.4463285, 0.0120613, 'rec' ),
# })

## 170414: ll60
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.6588336, 0.0138628, 'rec' ),
#   'h1mu': ufloat( 0.5324452, 0.0159547, 'rec' ),
#   'h3mu': ufloat( 0.2100045, 0.0116680, 'rec' ),
#   'ee'  : ufloat( 0.2877112, 0.0129837, 'rec' ),
#   'eh1' : ufloat( 0.2335779, 0.0124135, 'rec' ),
#   'eh3' : ufloat( 0.0923027, 0.0064513, 'rec' ),
#   'emu' : ufloat( 0.4463285, 0.0120613, 'rec' ),
# })

## 170419: better ePIDmu (sWeight)
# DF['rec'] = pd.Series({
#   'mumu': ufloat( 0.6475953, 0.0138259, 'rec' ),
#   'h1mu': ufloat( 0.5270813, 0.0158403, 'rec' ),
#   'h3mu': ufloat( 0.2087069, 0.0116014, 'rec' ),
#   'ee'  : ufloat( 0.2877112, 0.0129837, 'rec' ),
#   'eh1' : ufloat( 0.2335779, 0.0124135, 'rec' ),
#   'eh3' : ufloat( 0.0923027, 0.0064513, 'rec' ),
#   'emu' : ufloat( 0.4420365, 0.0119906, 'rec' ),
# })

## 170422: Keep mass window only mumu, ee
DF['rec'] = pd.Series({
  'mumu': ufloat( 0.6476000, 0.0138251, 'rec' ),
  'h1mu': ufloat( 0.5262882, 0.0162238, 'rec' ),
  'h3mu': ufloat( 0.2075162, 0.0115360, 'rec' ),
  'ee'  : ufloat( 0.2877137, 0.0130345, 'rec' ),
  'eh1' : ufloat( 0.2327911, 0.0125841, 'rec' ),
  'eh3' : ufloat( 0.0888795, 0.0062594, 'rec' ),
  'emu' : ufloat( 0.4580826, 0.0122818, 'rec' ),
})

#===============================================================================
# SELECTION EFFICIENCY
#===============================================================================

## 160419 APT major change
## 160529: Back to S20, truth channeling.
## 160606: Retuned after Stephane's comment
## 160704: dilepton to mode C
## 160711: Removed TRGHP (dilepton mode C)
# DF['sel'] = pd.Series({
#   'mumu': EffU( 27539,  4745, 'sel' ),
#   'h1mu': EffU( 39539,  9944, 'sel' ),
#   'h3mu': EffU(  8497,  2468, 'sel' ),
#   'ee'  : EffU(  9955,  1536, 'sel' ),
#   'eh1' : EffU( 18318,  4576, 'sel' ),
#   'eh3' : EffU(  3950,  1050, 'sel' ),
#   'emu' : EffU( 35923, 10191, 'sel' ),
# })

## 160813: APT<0.5 for emu, DRoPT<0.007&DRtPT<4 for h3
# DF['sel'] = pd.Series({
#   'mumu': EffU( 27539,  4745, 'sel' ),
#   'h1mu': EffU( 39539,  9944, 'sel' ),
#   'h3mu': EffU(  8497,  2330, 'sel' ),
#   'ee'  : EffU(  9955,  1536, 'sel' ),
#   'eh1' : EffU( 18318,  4576, 'sel' ),
#   'eh3' : EffU(  3950,  1007, 'sel' ),
#   'emu' : EffU( 35923,  8945, 'sel' ),
# })

## 160830: Revamped SS to include Ztau, lots of cuts loosened
# DF['sel'] = pd.Series({
#   'ee'  : EffU(  9955,  1663, 'sel' ),
#   'eh1' : EffU( 18318,  5755, 'sel' ),
#   'eh3' : EffU(  3950,  1174, 'sel' ),
#   'emu' : EffU( 35923, 12963, 'sel' ),
#   'h1mu': EffU( 39539, 12748, 'sel' ),
#   'h3mu': EffU(  8497,  2752, 'sel' ),
#   'mumu': EffU( 27539,  5199, 'sel' ),
# })

# ## 160902: DOCACHI2>0.1 for non-ll channel
# DF['sel'] = pd.Series({
#   'ee'  : EffU(  9955,  1663, 'sel' ),
#   'eh1' : EffU( 18318,  5547, 'sel' ),
#   'eh3' : EffU(  3950,  1087, 'sel' ),
#   'emu' : EffU( 35923, 12286, 'sel' ),
#   'h1mu': EffU( 39539, 12162, 'sel' ),
#   'h3mu': EffU(  8497,  2554, 'sel' ),
#   'mumu': EffU( 27539,  5199, 'sel' ),
# })

## 161011: No soft DOCACHI2>0.1, tight >20, tauh3 BPVLTIME, const lumi, Zee norm corr, revise rZ
# now with the new uncertainties
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.144021, 0.004612, 'sel' ),
#   'h1mu': ufloat( 0.317957, 0.007149, 'sel' ),
#   'h3mu': ufloat( 0.306400, 0.007357, 'sel' ),
#   'ee'  : ufloat( 0.123995, 0.004948, 'sel' ),
#   'eh1' : ufloat( 0.309321, 0.007675, 'sel' ),
#   'eh3' : ufloat( 0.280048, 0.008949, 'sel' ),
#   'emu' : ufloat( 0.367025, 0.008435, 'sel' ),
# })

## 161112: ee DOCACHI2>10 (because DOCACHI2 & esel is not nice with each other)
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.144021, 0.004612, 'sel' ),
#   'h1mu': ufloat( 0.317957, 0.007149, 'sel' ),
#   'h3mu': ufloat( 0.306400, 0.007357, 'sel' ),
#   'ee'  : ufloat( 0.169387, 0.006186, 'sel' ),
#   'eh1' : ufloat( 0.309321, 0.007675, 'sel' ),
#   'eh3' : ufloat( 0.280048, 0.008949, 'sel' ),
#   'emu' : ufloat( 0.367025, 0.008435, 'sel' ),
# })

# # 161212: Widening hadron's eta
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.143893, 0.004594, 'sel' ),
#   'h1mu': ufloat( 0.320709, 0.007086, 'sel' ),
#   'h3mu': ufloat( 0.295192, 0.006779, 'sel' ),
#   'ee'  : ufloat( 0.170172, 0.006221, 'sel' ),
#   'eh1' : ufloat( 0.311515, 0.007507, 'sel' ),
#   'eh3' : ufloat( 0.269562, 0.008053, 'sel' ),
#   'emu' : ufloat( 0.366958, 0.008422, 'sel' ),
# })

# ## 170215: Assisted
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2346802, 0.0130820, 'sel' ),
#   'h1mu': ufloat( 0.3579235, 0.0177365, 'sel' ),
#   'h3mu': ufloat( 0.2953520, 0.0117917, 'sel' ),
#   'ee'  : ufloat( 0.0970128, 0.0067894, 'sel' ),
#   'eh1' : ufloat( 0.3481581, 0.0170356, 'sel' ),
#   'eh3' : ufloat( 0.2777126, 0.0122487, 'sel' ),
#   'emu' : ufloat( 0.4980490, 0.0200577, 'sel' ),
# })

## 170323: Fixed syst in IP,DPHI.
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2309804, 0.0142607, 'sel' ),
#   'h1mu': ufloat( 0.3474824, 0.0166426, 'sel' ),
#   'h3mu': ufloat( 0.2912815, 0.0117156, 'sel' ),
#   'ee'  : ufloat( 0.1084681, 0.0084693, 'sel' ),
#   'eh1' : ufloat( 0.3382758, 0.0160584, 'sel' ),
#   'eh3' : ufloat( 0.2734978, 0.0118638, 'sel' ),
#   'emu' : ufloat( 0.4914631, 0.0196298, 'sel' ),
# })

## 170406: With mass cut, optimize syst, fix tauh3 bug, optimize APT syst
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2484373, 0.0139431, 'sel' ),
#   'h1mu': ufloat( 0.3577158, 0.0148430, 'sel' ),
#   'h3mu': ufloat( 0.3236324, 0.0115584, 'sel' ),
#   'ee'  : ufloat( 0.1965142, 0.0121457, 'sel' ),
#   'eh1' : ufloat( 0.3482928, 0.0143854, 'sel' ),
#   'eh3' : ufloat( 0.3096628, 0.0124185, 'sel' ),
#   'emu' : ufloat( 0.4992817, 0.0189721, 'sel' ),
# })

## 170413: Fix missing InAcc[E,H]cal, SingleStrip
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2483819, 0.0137392, 'sel' ),
#   'h1mu': ufloat( 0.3577353, 0.0145709, 'sel' ),
#   'h3mu': ufloat( 0.3292018, 0.0117731, 'sel' ),
#   'ee'  : ufloat( 0.1967656, 0.0121474, 'sel' ),
#   'eh1' : ufloat( 0.3482928, 0.0143854, 'sel' ),
#   'eh3' : ufloat( 0.3096628, 0.0124185, 'sel' ),
#   'emu' : ufloat( 0.5040063, 0.0191549, 'sel' ),
# })

## 170414: ll60
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2332928, 0.0129234, 'sel' ),
#   'h1mu': ufloat( 0.3577353, 0.0145709, 'sel' ),
#   'h3mu': ufloat( 0.3292018, 0.0117731, 'sel' ),
#   'ee'  : ufloat( 0.2025922, 0.0124597, 'sel' ),
#   'eh1' : ufloat( 0.3482928, 0.0143854, 'sel' ),
#   'eh3' : ufloat( 0.3096628, 0.0124185, 'sel' ),
#   'emu' : ufloat( 0.5040063, 0.0191549, 'sel' ),
# })

## 170422: Keep mass window only mumu, ee
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2332928, 0.0129234, 'sel' ),
#   'h1mu': ufloat( 0.3591987, 0.0146228, 'sel' ),
#   'h3mu': ufloat( 0.3399032, 0.0122318, 'sel' ),
#   'ee'  : ufloat( 0.2025922, 0.0124597, 'sel' ),
#   'eh1' : ufloat( 0.3493257, 0.0144313, 'sel' ),
#   'eh3' : ufloat( 0.3171156, 0.0126566, 'sel' ),
#   'emu' : ufloat( 0.5057886, 0.0192216, 'sel' ),
# })

## 170424: Neutral ztautau0
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2552752, 0.0141412, 'sel' ),
#   'h1mu': ufloat( 0.3637021, 0.0148061, 'sel' ),
#   'h3mu': ufloat( 0.3639557, 0.0130974, 'sel' ),
#   'ee'  : ufloat( 0.2059748, 0.0126677, 'sel' ),
#   'eh1' : ufloat( 0.3606475, 0.0148990, 'sel' ),
#   'eh3' : ufloat( 0.3238789, 0.0129266, 'sel' ),
#   'emu' : ufloat( 0.5067031, 0.0192564, 'sel' ),
# })

## 170717: Fix for noteV4, back to high-stat version
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2332928, 0.0129234, 'sel' ),
#   'h1mu': ufloat( 0.3591987, 0.0146228, 'sel' ),
#   'h3mu': ufloat( 0.3399032, 0.0122318, 'sel' ),
#   'ee'  : ufloat( 0.2025922, 0.0124597, 'sel' ),
#   'eh1' : ufloat( 0.3493257, 0.0144313, 'sel' ),
#   'eh3' : ufloat( 0.3171156, 0.0126566, 'sel' ),
#   'emu' : ufloat( 0.5057886, 0.0192216, 'sel' ),
# })

## 170719: For v4.1, smeared-IP, scaled-DPHI (still waiting Roger answer)
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2277769, 0.0112624, 'sel' ),
#   'h1mu': ufloat( 0.3516573, 0.0123052, 'sel' ),
#   'h3mu': ufloat( 0.3391706, 0.0118463, 'sel' ),
#   'ee'  : ufloat( 0.1973441, 0.0108513, 'sel' ),
#   'eh1' : ufloat( 0.3418114, 0.0120586, 'sel' ),
#   'eh3' : ufloat( 0.3164674, 0.0123473, 'sel' ),
#   'emu' : ufloat( 0.5059364, 0.0195091, 'sel' ),
# })

# ## 170724: For v4.2: More syst on mass, tauh3 varsx4
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2277769, 0.0114463, 'sel' ),
#   'h1mu': ufloat( 0.3516573, 0.0123052, 'sel' ),
#   'h3mu': ufloat( 0.3391706, 0.0160808, 'sel' ),
#   'ee'  : ufloat( 0.1973441, 0.0112193, 'sel' ),
#   'eh1' : ufloat( 0.3418114, 0.0120586, 'sel' ),
#   'eh3' : ufloat( 0.3164674, 0.0159816, 'sel' ),
#   'emu' : ufloat( 0.5059364, 0.0195091, 'sel' ),
# })

# ## 170920; Changing esel DPHI method from scaling to nudging
# DF['sel'] = pd.Series({
#   'mumu': ufloat( 0.2248844, 0.0102569, 'sel' ),
#   'h1mu': ufloat( 0.3470850, 0.0097193, 'sel' ),
#   'h3mu': ufloat( 0.3347386, 0.0143102, 'sel' ),
#   'ee'  : ufloat( 0.1953528, 0.0103370, 'sel' ),
#   'eh1' : ufloat( 0.3373911, 0.0096004, 'sel' ),
#   'eh3' : ufloat( 0.3131979, 0.0145651, 'sel' ),
#   'emu' : ufloat( 0.4990800, 0.0157370, 'sel' ),
# })

## 171106: Changing esel DPHI method to Roger's suggestion
DF['sel'] = pd.Series({
  'mumu': ufloat( 0.2265838, 0.0108404, 'sel' ),
  'h1mu': ufloat( 0.3499581, 0.0111089, 'sel' ),
  'h3mu': ufloat( 0.3377921, 0.0153578, 'sel' ),
  'ee'  : ufloat( 0.1958455, 0.0105011, 'sel' ),
  'eh1' : ufloat( 0.3402090, 0.0109431, 'sel' ),
  'eh3' : ufloat( 0.3148111, 0.0151440, 'sel' ),
  'emu' : ufloat( 0.5027637, 0.0174102, 'sel' ),
})

#===============================================================================
# NUMBER OF SIGNAL
#===============================================================================

## 160408: ee,mumu mass [20,80] DOCACHI2>10, APT>0.1, tauh3 mass 40->30,
## 160412: Using weighted-r for EWK background
## 160414: Use we as template in SS \ditauee instead of dye40
## 160417: Excluded bad run
## 160417: Correlated SSfit, S21, Strip-OR-WZ
## 160418: emu 'DOCACHI2 > 1
## 160418: APT major changes
## 160429: S20, evt
## 160606: Retuned after Stephane's comment

## 160704: dilepton to mode C
# DF['nsig'] = pd.Series({
#   'mumu': 271.32711 + ufloat( 0,  20.59126, "stat" ) + ufloat( 0,  14.42138, "nsig" ),
#   'h1mu': 559.37833 + ufloat( 0,  27.03701, "stat" ) + ufloat( 0,  21.32889, "nsig" ),
#   'h3mu': 135.27849 + ufloat( 0,  12.44990, "stat" ) + ufloat( 0,  12.83680, "nsig" ),
#   'ee'  :  87.74929 + ufloat( 0,  16.97056, "stat" ) + ufloat( 0,  17.21296, "nsig" ),
#   'eh1' : 256.06629 + ufloat( 0,  21.00000, "stat" ) + ufloat( 0,  15.08956, "nsig" ),
#   'eh3' :  53.62225 + ufloat( 0,   8.88819, "stat" ) + ufloat( 0,   5.23242, "nsig" ),
#   'emu' : 633.51772 + ufloat( 0,  27.09243, "stat" ) + ufloat( 0,  12.90470, "nsig" ),
# })

# ## 160812: Fix misid as suggested by Phil
# DF['nsig'] = pd.Series({
#   'mumu': 271.32711 + ufloat( 0,  20.59126, "stat" ) + ufloat( 0,  14.42138, "nsig" ),
#   'h1mu': 559.11708 + ufloat( 0,  27.03701, "stat" ) + ufloat( 0,  21.33620, "nsig" ),
#   'h3mu': 135.27849 + ufloat( 0,  12.44990, "stat" ) + ufloat( 0,  12.83680, "nsig" ),
#   'ee'  :  87.74929 + ufloat( 0,  16.97056, "stat" ) + ufloat( 0,  17.21296, "nsig" ),
#   'eh1' : 232.22660 + ufloat( 0,  21.00000, "stat" ) + ufloat( 0,  15.71126, "nsig" ),
#   'eh3' :  53.62225 + ufloat( 0,   8.88819, "stat" ) + ufloat( 0,   5.23242, "nsig" ),
#   'emu' : 617.56521 + ufloat( 0,  27.09243, "stat" ) + ufloat( 0,  13.63717, "nsig" ),
# })

## 160813: APT<0.5 for emu, DRoPT<0.007&DRtPT<4 for h3
## 160816: TFractionFitter restricted rangeX
# DF['nsig'] = pd.Series({
#   'mumu': 272.14229 + ufloat( 0,  20.59126, "stat" ) + ufloat( 0,   9.82964, "nsig" ),
#   'h1mu': 558.64939 + ufloat( 0,  27.03701, "stat" ) + ufloat( 0,  21.17448, "nsig" ),
#   'h3mu': 126.01619 + ufloat( 0,  12.00000, "stat" ) + ufloat( 0,   4.66615, "nsig" ),
#   'ee'  :  87.81325 + ufloat( 0,  16.97056, "stat" ) + ufloat( 0,  17.27138, "nsig" ),
#   'eh1' : 238.73534 + ufloat( 0,  21.00000, "stat" ) + ufloat( 0,  14.01457, "nsig" ),
#   'eh3' :  52.22656 + ufloat( 0,   8.71780, "stat" ) + ufloat( 0,   4.59642, "nsig" ),
#   'emu' : 539.88433 + ufloat( 0,  24.97999, "stat" ) + ufloat( 0,  10.21919, "nsig" ),
# })


## 160830: Revamped SS to include Ztau, lots of cuts loosened
# DF['nsig'] = pd.Series({
#   'mumu': 293.73525 + ufloat( 0,  20.95828, "stat" ) + ufloat( 0,  10.64274, "nsig" ),
#   'h1mu': 741.70665 + ufloat( 0,  31.59582, "stat" ) + ufloat( 0,  22.76953, "nsig" ),
#   'h3mu': 145.84784 + ufloat( 0,  13.52197, "stat" ) + ufloat( 0,   6.06980, "nsig" ),
#   'ee'  :  98.51018 + ufloat( 0,  16.79289, "stat" ) + ufloat( 0,  19.56166, "nsig" ),
#   'eh1' : 289.88686 + ufloat( 0,  24.99196, "stat" ) + ufloat( 0,  25.82276, "nsig" ),
#   'eh3' :  53.41223 + ufloat( 0,  10.32107, "stat" ) + ufloat( 0,   9.93840, "nsig" ),
#   'emu' : 754.84953 + ufloat( 0,  30.52896, "stat" ) + ufloat( 0,  22.80878, "nsig" ),
# })

# ## 160902: DOCACHI2>0.1 for non-ll channel
# DF['nsig'] = pd.Series({
#   'mumu': 293.73525 + ufloat( 0,  20.95828, "stat" ) + ufloat( 0,  10.64274, "nsig" ),
#   'h1mu': 725.78409 + ufloat( 0,  30.79238, "stat" ) + ufloat( 0,  21.11506, "nsig" ),
#   'h3mu': 133.78773 + ufloat( 0,  12.92992, "stat" ) + ufloat( 0,   5.81043, "nsig" ),
#   'ee'  :  98.51018 + ufloat( 0,  16.79289, "stat" ) + ufloat( 0,  19.56166, "nsig" ),
#   'eh1' : 282.87159 + ufloat( 0,  24.15263, "stat" ) + ufloat( 0,  18.01066, "nsig" ),
#   'eh3' :  51.23036 + ufloat( 0,   9.87676, "stat" ) + ufloat( 0,   8.60846, "nsig" ),
#   'emu' : 706.28251 + ufloat( 0,  29.49621, "stat" ) + ufloat( 0,  26.07853, "nsig" ),
# })

## 161011: No soft DOCACHI2>0.1, tight >20, tauh3 BPVLTIME, const lumi, Zee norm corr, revise rZ
## Zll misid correction (both number OS-SS and corrected prob),
# DF['nsig'] = pd.Series({
#   'mumu': 219.53791 + ufloat( 0,  15.99902, "stat" ) + ufloat( 0,   6.56970, "nsig" ),
#   'h1mu': 754.49038 + ufloat( 0,  31.59582, "stat" ) + ufloat( 0,  22.63692, "nsig" ),
#   'h3mu': 155.20376 + ufloat( 0,  14.02320, "stat" ) + ufloat( 0,   6.66271, "nsig" ),
#   'ee'  :  64.26540 + ufloat( 0,  10.45319, "stat" ) + ufloat( 0,  10.25590, "nsig" ),
#   'eh1' : 330.72468 + ufloat( 0,  24.99196, "stat" ) + ufloat( 0,  18.22298, "nsig" ),
#   'eh3' :  61.42922 + ufloat( 0,  10.42220, "stat" ) + ufloat( 0,   7.94210, "nsig" ),
#   'emu' : 761.82375 + ufloat( 0,  30.52896, "stat" ) + ufloat( 0,  23.94963, "nsig" ),
# })

## 161110: mu->x misid now use fully mcp
# DF['nsig'] = pd.Series({
#   'mumu': 219.53791 + ufloat( 0,  15.99902, "stat" ) + ufloat( 0,   6.56970, "nsig" ),
#   'h1mu': 758.90360 + ufloat( 0,  31.59582, "stat" ) + ufloat( 0,  22.59615, "nsig" ),
#   'h3mu': 155.20376 + ufloat( 0,  14.02320, "stat" ) + ufloat( 0,   6.66271, "nsig" ),
#   'ee'  :  64.26540 + ufloat( 0,  10.45319, "stat" ) + ufloat( 0,  10.25590, "nsig" ),
#   'eh1' : 330.72468 + ufloat( 0,  24.99196, "stat" ) + ufloat( 0,  18.22298, "nsig" ),
#   'eh3' :  61.42922 + ufloat( 0,  10.42220, "stat" ) + ufloat( 0,   7.94210, "nsig" ),
#   'emu' : 768.19352 + ufloat( 0,  30.52896, "stat" ) + ufloat( 0,  23.86639, "nsig" ),
# })

## 161112: ee DOCACHI2>10, fix ZXC with /egen/eacc
# DF['nsig'] = pd.Series({
#   'mumu': 216.47922 + ufloat( 0,  15.77612, "stat" ) + ufloat( 0,   6.46763, "nsig" ),
#   'h1mu': 761.77553 + ufloat( 0,  31.77362, "stat" ) + ufloat( 0,  19.05397, "nsig" ),
#   'h3mu': 154.57868 + ufloat( 0,  13.96673, "stat" ) + ufloat( 0,   6.62641, "nsig" ),
#   'ee'  : 103.24405 + ufloat( 0,  16.06730, "stat" ) + ufloat( 0,  19.96035, "nsig" ),
#   'eh1' : 332.84563 + ufloat( 0,  25.15224, "stat" ) + ufloat( 0,  18.33275, "nsig" ),
#   'eh3' :  61.32526 + ufloat( 0,  10.40456, "stat" ) + ufloat( 0,   7.92608, "nsig" ),
#   'emu' : 741.63166 + ufloat( 0,  29.47336, "stat" ) + ufloat( 0,  23.02239, "nsig" ),
# })

## 161212: Widening hadron's eta, ZXC numerator no fid
# DF['nsig'] = pd.Series({
#   'mumu': 217.28214 + ufloat( 0,  15.83463, "stat" ) + ufloat( 0,   6.49156, "nsig" ),
#   'h1mu': 932.80459 + ufloat( 0,  35.98962, "stat" ) + ufloat( 0,  24.18032, "nsig" ),
#   'h3mu': 172.85554 + ufloat( 0,  14.70793, "stat" ) + ufloat( 0,   9.97919, "nsig" ),
#   'ee'  :  94.93347 + ufloat( 0,  15.72137, "stat" ) + ufloat( 0,  19.15597, "nsig" ),
#   'eh1' : 384.72458 + ufloat( 0,  29.75340, "stat" ) + ufloat( 0, 141.33767, "nsig" ),
#   'eh3' :  75.82721 + ufloat( 0,  11.04859, "stat" ) + ufloat( 0,  10.17284, "nsig" ),
#   'emu' : 725.85257 + ufloat( 0,  28.96944, "stat" ) + ufloat( 0,  36.80706, "nsig" ),
# })

# ## 161219: MC-WEH patched, ZXC syst
# DF['nsig'] = pd.Series({
#   'mumu': 213.39420 + ufloat( 0,  15.82606, "stat" ) + ufloat( 0,   6.44652, "nsig" ),
#   'h1mu': 930.58133 + ufloat( 0,  35.98558, "stat" ) + ufloat( 0,  24.08866, "nsig" ),
#   'h3mu': 172.39482 + ufloat( 0,  14.70132, "stat" ) + ufloat( 0,   8.92660, "nsig" ),
#   'ee'  :  96.97283 + ufloat( 0,  15.70171, "stat" ) + ufloat( 0,  20.20866, "nsig" ),
#   'eh1' : 323.21503 + ufloat( 0,  29.74554, "stat" ) + ufloat( 0, 101.76308, "nsig" ),
#   'eh3' :  77.38076 + ufloat( 0,  11.03449, "stat" ) + ufloat( 0,  10.90720, "nsig" ),
#   'emu' : 755.32693 + ufloat( 0,  28.96512, "stat" ) + ufloat( 0,  35.47702, "nsig" ),
# })

# ## 170205: More robust ssfit checker
# DF['nsig'] = pd.Series({
#   'mumu': 211.82148 + ufloat( 0,  15.82606, "stat" ) + ufloat( 0,   9.15605, "nsig" ),
#   'h1mu': 899.63383 + ufloat( 0,  35.98558, "stat" ) + ufloat( 0,  52.41360, "nsig" ),
#   'h3mu': 171.31567 + ufloat( 0,  14.70132, "stat" ) + ufloat( 0,   9.99310, "nsig" ),
#   'ee'  :  96.01011 + ufloat( 0,  15.70171, "stat" ) + ufloat( 0,  19.15782, "nsig" ),
#   'eh1' : 424.89635 + ufloat( 0,  29.74554, "stat" ) + ufloat( 0,  51.01098, "nsig" ),
#   'eh3' :  80.16826 + ufloat( 0,  11.03449, "stat" ) + ufloat( 0,  14.29974, "nsig" ),
#   'emu' : 738.01036 + ufloat( 0,  28.96512, "stat" ) + ufloat( 0,  42.14105, "nsig" ),
# })

# 170215: Assisted
# DF['nsig'] = pd.Series({
#   'mumu':  369.90144 + ufloat( 0,  27.61869, "stat" ) + ufloat( 0,  20.39235, "nsig" ),
#   'h1mu':  947.79100 + ufloat( 0,  36.18753, "stat" ) + ufloat( 0,  63.40336, "nsig" ),
#   'h3mu':  171.36186 + ufloat( 0,  13.47001, "stat" ) + ufloat( 0,   8.87173, "nsig" ),
#   'ee'  :   75.73367 + ufloat( 0,  14.39169, "stat" ) + ufloat( 0,  18.31402, "nsig" ),
#   'eh1' :  420.22221 + ufloat( 0,  28.18966, "stat" ) + ufloat( 0,  38.07349, "nsig" ),
#   'eh3' :   68.66147 + ufloat( 0,   9.58730, "stat" ) + ufloat( 0,  14.52662, "nsig" ),
#   'emu' : 1039.89333 + ufloat( 0,  35.79332, "stat" ) + ufloat( 0,  44.16395, "nsig" ),
# })

## 170406: Mass window, IPmu==IPe, add Zbb, ZXCfrac wmw
# DF['nsig'] = pd.Series({
#   'mumu':  395.8474028 + ufloat( 0, 18.157045, 'stat') + ufloat( 0, 14.591397, 'nsig'),
#   'h1mu':  949.0462804 + ufloat( 0, 35.038883, 'stat') + ufloat( 0, 31.503522, 'nsig'),
#   'h3mu':  159.3996799 + ufloat( 0, 12.582495, 'stat') + ufloat( 0,  5.023200, 'nsig'),
#   'ee'  :  126.6999360 + ufloat( 0, 12.747675, 'stat') + ufloat( 0, 19.762953, 'nsig'),
#   'eh1' :  392.2412605 + ufloat( 0, 26.972922, 'stat') + ufloat( 0, 30.700886, 'nsig'),
#   'eh3' :   61.9412243 + ufloat( 0,  8.414962, 'stat') + ufloat( 0,  5.911063, 'nsig'),
#   'emu' : 1039.4660604 + ufloat( 0, 34.765224, 'stat') + ufloat( 0, 24.489432, 'nsig'),
# })

## 170413: Fix missing InAcc[E,H]cal, SingleStrip
# DF['nsig'] = pd.Series({
#   'mumu':  397.7729976 + ufloat( 0, 18.112477, 'stat') + ufloat( 0, 14.340882, 'nsig'),
#   'h1mu':  954.0346918 + ufloat( 0, 35.008905, 'stat') + ufloat( 0, 31.320689, 'nsig'),
#   'h3mu':  157.3526622 + ufloat( 0, 12.198021, 'stat') + ufloat( 0,  6.476788, 'nsig'),
#   'ee'  :  107.5729551 + ufloat( 0, 11.742553, 'stat') + ufloat( 0, 18.582767, 'nsig'),
#   'eh1' :  405.5919395 + ufloat( 0, 26.938607, 'stat') + ufloat( 0, 17.706666, 'nsig'),
#   'eh3' :   59.7093735 + ufloat( 0,  8.217943, 'stat') + ufloat( 0,  6.008480, 'nsig'),
#   'emu' :  976.4378150 + ufloat( 0, 33.398146, 'stat') + ufloat( 0, 24.281200, 'nsig'),
# })

## 170414: ll60
# DF['nsig'] = pd.Series({
#   'mumu':  376.3234409 + ufloat( 0, 26.069721, 'stat') + ufloat( 0, 12.679542, 'nsig'),
#   'h1mu':  954.0346918 + ufloat( 0, 35.610521, 'stat') + ufloat( 0, 31.321046, 'nsig'),
#   'h3mu':  153.5833688 + ufloat( 0, 12.703851, 'stat') + ufloat( 0,  4.563871, 'nsig'),
#   'ee'  :  127.5585413 + ufloat( 0, 22.415402, 'stat') + ufloat( 0, 24.245588, 'nsig'),
#   'eh1' :  405.5919395 + ufloat( 0, 27.587042, 'stat') + ufloat( 0, 17.708157, 'nsig'),
#   'eh3' :   59.7093735 + ufloat( 0,  8.842961, 'stat') + ufloat( 0,  6.017743, 'nsig'),
#   'emu' :  976.4378150 + ufloat( 0, 33.968210, 'stat') + ufloat( 0, 24.283374, 'nsig'),
# })

## 170422: Keep mass window only mumu, ee
# DF['nsig'] = pd.Series({
#   'mumu':  376.2233230 + ufloat( 0, 26.069721, 'stat') + ufloat( 0, 12.692343, 'nsig'),
#   'h1mu':  965.8795380 + ufloat( 0, 36.220706, 'stat') + ufloat( 0, 37.927670, 'nsig'),
#   'h3mu':  163.9706845 + ufloat( 0, 13.198909, 'stat') + ufloat( 0,  5.137192, 'nsig'),
#   'ee'  :  127.4778656 + ufloat( 0, 22.415402, 'stat') + ufloat( 0, 24.246690, 'nsig'),
#   'eh1' :  424.5097534 + ufloat( 0, 28.255370, 'stat') + ufloat( 0, 19.780341, 'nsig'),
#   'eh3' :   72.3192502 + ufloat( 0,  9.537230, 'stat') + ufloat( 0,  6.183191, 'nsig'),
#   'emu' :  991.9164327 + ufloat( 0, 34.550536, 'stat') + ufloat( 0, 28.005594, 'nsig'),
# })

## 170424: Tune same-sign Ztautau, auto normalized
DF['nsig'] = pd.Series({
  'mumu':  376.1313077 + ufloat( 0, 26.069721, 'stat') + ufloat( 0, 12.691492, 'nsig'),
  'h1mu':  965.8769483 + ufloat( 0, 36.220706, 'stat') + ufloat( 0, 37.493549, 'nsig'),
  'h3mu':  163.9169310 + ufloat( 0, 13.198909, 'stat') + ufloat( 0,  5.299801, 'nsig'),
  'ee'  :  127.3239310 + ufloat( 0, 22.415402, 'stat') + ufloat( 0, 24.246759, 'nsig'),
  'eh1' :  427.5087351 + ufloat( 0, 28.255370, 'stat') + ufloat( 0, 22.006251, 'nsig'),
  'eh3' :   72.7892643 + ufloat( 0,  9.537230, 'stat') + ufloat( 0,  5.806995, 'nsig'),
  'emu' : 1003.0522079 + ufloat( 0, 34.550536, 'stat') + ufloat( 0, 23.637354, 'nsig'),
})

#===============================================================================
# SEPARATING UNCERTAINTIES
#===============================================================================

def resolve_all(x):
  """
  Split the single instance of ufloat into Series.
  If same tag existed, combine in quadrature.
  """
  se = pd.Series()
  se['val'] = x.n
  for val,err in x.error_components().items():
    key = val.tag
    if key not in se:
      se[key] = err
    else:
      se[key] = (se[key]**2 + err**2)**0.5
  return se

def resolve_std(x):
  """
  Like above, but group into only stat, syst, beam, lumi.
  Collect all unclassified into syst.
  """
  se = pd.Series()
  se['val'] = x.n
  for val,err in x.error_components().items():
    key = val.tag
    if key not in ('lumi', 'stat', 'beam'):
      key = 'syst'
    if key not in se:
      se[key] = err
    else:
      se[key] = (se[key]**2 + err**2)**0.5
  return se


CSC_RAW = DF.nsig / DF.lumi / DF.br / DF.acc / DF.rec / DF.sel # * DF.purity

CSC_ALL = CSC_RAW.apply(resolve_all).T

CSC_FINAL = CSC_RAW.apply(resolve_std).T

#===============================================================================

def corr_br():
  """
  Return the correlation matrix of the BR term across channel.
  """
  return uncertainties.correlation_matrix(DF['br'].values)


def latex_calculation():
  """
  For table of components used in the calculation of csc
  """
  def printer(x):
    return '&{0.n:6.2f} &{0.s:5.2f}'.format(x*100)

  df = DF[['br','acc','rec','sel']].copy().T
  ## inject etot
  etot = df.loc[['acc','rec','sel']].product()
  df = df.T; df.insert(1, 'efftot', etot); df = df.T
  ## Rename
  df = df.rename(index=COMPUTE_LABELS, columns=DT_TO_LATEX)
  ## latex, midrule, return
  msg = (df*100.).to_bicol_latex(decimals=[2]*7)
  return inject_midrule(msg, 23)
  # ## Print to desire format
  # df = df.applymap(printer)
  # inject_midrule(df, 2)
  # msg = df.to_string().split('\n')
  # l   = [ line+' \\\\' for line in msg ]
  # return '\n'.join(l[1:]) # exclude the header


def latex_csc_all_channels():
  """
  For final cross-section lines
  """
  fmt = r'& \sigma({0:6s}) &&=& {1.val:6.2f} &\;\pm& {1.stat:5.2f} &\;\pm& {1.syst:5.2f} &\;\pm& {1.beam:4.2f} &\;\pm& {1.lumi:4.2f} \pb \\'
  df  = CSC_FINAL.T.rename(DT_TO_LATEX)

  msg = ''
  msg += '\n$$'
  msg += '\n\\begin{aligned}'
  for dt,se in df.iterrows():
    msg += '\n'+fmt.format( dt, se )
  msg += '\n\\end{aligned}'
  msg += '\n$$'
  return msg


def latex_relative_uncertainties():
  ## sum up the total syst
  df = CSC_ALL.copy()
  df.loc['syst'] = CSC_ALL.copy().loc[list(RERR_ORDER)[:-4]].apply(sumq)
  df = df.loc[list(RERR_ORDER)]

  ## flush
  df = df / CSC_ALL.loc['val']
  df = df.rename(index=COMPUTE_LABELS, columns=DT_TO_LATEX)
  df = df.rename({'\\eacc': '\\eacc (PDF)'}) # Only here in rerr syst table.
  df.index.name = '\\cscrerr'
  # add total, from last 4 entries, don't double count
  df.loc['Total'] = df.apply(lambda se: sumq(se[-4:]))
  inject_midrule(df, -1)
  return df.fmt1p.to_markdown(floatfmt='.1f')


#===============================================================================

if __name__ == '__main__':
  pass

  ## Individual components, for debugging
  # print
  # print DF
  print CSC_ALL

  ## For latex report
  # print latex_calculation()
  # print latex_csc_all_channels()
  print latex_relative_uncertainties()

  ## For the combine_csc
  # print corr_br()
