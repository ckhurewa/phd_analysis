#!/usr/bin/env python

import sys
from array import array
from PyrootCK import *

# http://code.metager.de/source/xref/cern/root/tmva/test/TMVAMultipleBackgroundExample.cxx

dtype = 'h1_mu'

real = 3488, 3489, 3490, 3491, 3492, 3495, 3496, 3503, 3505, 3506, 3507, 3509


tname_os = 'DitauCandTupleWriter/%s'%dtype
tname_ss = 'DitauCandTupleWriter/%s_ss'%dtype
tname_oa = 'DitauCandTupleWriter/%s_antiiso'%dtype
tname_sa = 'DitauCandTupleWriter/%s_ss_antiiso'%dtype


t_real      = import_tree( tname_os, *real ).SetTitle('Real')
t_real_ss   = import_tree( tname_ss, *real ).SetTitle('Real (SS)')
t_real_oa   = import_tree( tname_oa, *real ).SetTitle('Real (antiiso)')
t_real_sa   = import_tree( tname_sa, *real ).SetTitle('Real (SS,antiiso)')

t_ztautau   = import_tree( tname_os, 3544  ).SetTitle('ztautau')
t_dytau     = import_tree( tname_os, 3545  ).SetTitle('DY tautau')
t_dymu      = import_tree( tname_os, 3546  ).SetTitle('DY mumu')
# t_dye       = import_tree( tname_os, 3416  ).SetTitle('DY ee')
t_cc_hardmu = import_tree( tname_os, 3552  ).SetTitle('ccbar (hard mu)')
t_bb_hardmu = import_tree( tname_os, 3553  ).SetTitle('bbbar (hard mu)')
# t_cc_harde  = import_tree( tname_os, 3419  ).SetTitle('ccbar (hard e)')
t_wtaujet   = import_tree( tname_os, 3548  ).SetTitle('Wtau+Jet')
t_wmu17jet  = import_tree( tname_os, 3549  ).SetTitle('Wmu17+jet')
# t_wejet     = import_tree( tname_os, 3423  ).SetTitle('We+jet')


weight_sig = 1.22E-06*3.67E-01

T_backgrounds = [ # ProductionXS x GenEff
  ( t_dymu         ,     4.57E-6 * 4.00E-1 ),
  ( t_wtaujet      ,     3.67E-5 * 2.65E-1 ),
  ( t_wmu17jet     ,     3.76E-5 * 2.31E-1 ),
  ( t_cc_hardmu    ,     1.76E-3 * 1.78E-4 ),
  ( t_bb_hardmu    ,     1.66E-3 * 8.67E-4 ),
]



# 0,0,0,0,0
# n_backgrounds = [
#   853. / 5966885. * T_backgrounds[0][1] * LUMI,
#    96. / 2576506  * T_backgrounds[1][1] * LUMI,
#  1844. / 10034658 * T_backgrounds[2][1] * LUMI,
#   111. / 1167321  * T_backgrounds[3][1] * LUMI,
#   255. / 1118554  * T_backgrounds[4][1] * LUMI,
# ]

# print n_backgrounds
# sys.exit()

#-------------------------------------------------------------------------------


class FD_Adapter(object):
  f = []
  d = []
  
  def __init__(self, *varnames):
    self.varnames = varnames
    for name in varnames:
      self.f.append( array('f', [0.]) )
      self.d.append( array('d', [0.]) )

  def attach_factory( self, factory ):
    """Loop over AddVariable call. Ignore other fields. Default to float 'F' """
    for vname in self.varnames:
      factory.AddVariable( vname, 'F' )

  def attach_reader( self, reader ):

    """To export value in arrar.f to reader."""
    for i,vname in enumerate(self.varnames):
      reader.AddVariable( vname, self.f[i] )

  def attach_tree( self, tree ):
    """To import value to arrar.d from Tree."""
    for i,vname in enumerate(self.varnames):
      tree.SetBranchAddress(vname, self.d[i] )

  def sync(self):
    """Do sync: Import value from tree, then export to reader."""
    for af,ad in zip(self.f, self.d):
      af[0] = ad[0] 

params = [
  # 'nPVs',
  'M',
  'PT',
  'APT',
  'DOCA',
  'DPHI', 
  'DR',
  'MINIPS',
  #
  'mu_PT',
  'pi_PT',
  'mu_BPVIP',
  'pi_BPVIP',
  #
  'mu_PTFrac05C',
  'mu_PTCone05C',
  'pi_PTFrac05C',
  'pi_PTCone05C',
]

adapter = FD_Adapter(*params)  

preselection = {
  'h1_mu': [
    'MATCH',
    'StrippingZ02TauTau_MuXLineDecision',
    'mu_PT > 15000',
    'pi_PT >  5000',
    # 'M     > 25000',
    #
    'mu_PTFrac05C > 0.8',
    'pi_PTFrac05C > 0.8',
    #
    # 'mu_BPVIP   > 0.001',
    # 'mu_BPVIP   < 1',
    # 'pi_BPVIP   > 0.001',
    # 'pi_BPVIP   < 1',
  ],
}[dtype]

#-------------------------------------------------------------------------------

# def main_training2():

#   ## Init
#   options = "!V:!Silent:Transformations=I;D;P;G,D:AnalysisType=Classification"
#   fout    = ROOT.TFile.Open( 'bg.root', 'RECREATE' )
#   factory = ROOT.TMVA.Factory('Factory', fout, options )
#   adapter.attach_factory( factory )

#   ## Add Trees
#   factory.AddSignalTree    ( t_ztautau   , 1. )
#   for t_background in T_backgrounds:
#     factory.AddBackgroundTree( t_background, 1. )

#   # factory.SetBackgroundWeightExpression("weight")
#   # cuts = 'StrippingZ02TauTau_MuXLineDecision & MATCH'
#   cuts = QHistUtils.join( preselection )

#   # // tell the factory to use all remaining events in the trees after training for testing:
#   factory.PrepareTrainingAndTestTree( ROOT.TCut(cuts),
#                                       "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V" )

#   # // Boosted Decision Trees
#   factory.BookMethod( ROOT.TMVA.Types.kBDT, "BDTG",
#     "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.30:UseBaggedGrad:GradBaggingFraction=0.6:SeparationType=GiniIndex:nCuts=20:NNodesMax=5" )
#   factory.TrainAllMethods()
#   factory.TestAllMethods()
#   factory.EvaluateAllMethods()

#   fout.Close()
#   del factory


#-------------------------------------------------------------------------------

def main_training():

  options = "!V:!Silent:Transformations=I;D;P;G,D:AnalysisType=Classification"

  for i,(t_background,weight) in enumerate(T_backgrounds):

    print i, t_background

    fout    = ROOT.TFile.Open( 'bg%i.root'%i, 'RECREATE' )
    factory = ROOT.TMVA.Factory('Factory%i'%i, fout, options )
    adapter.attach_factory( factory )

    factory.AddSignalTree    ( t_dytau     , weight_sig )
    factory.AddBackgroundTree( t_background, weight )

    # factory.SetBackgroundWeightExpression("weight")
    # cuts = 'StrippingZ02TauTau_MuXLineDecision & MATCH'
    cuts = QHistUtils.join( preselection )



    # // tell the factory to use all remaining events in the trees after training for testing:
    factory.PrepareTrainingAndTestTree( ROOT.TCut(cuts),
                                        "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V" )

    # factory.BookMethod( ROOT.TMVA.Types.kCuts, "Cuts",
    #   "!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart:CreateMVAPdfs=True")

    factory.BookMethod( ROOT.TMVA.Types.kBDT, "BDT",
      # "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.30:UseBaggedGrad:GradBaggingFraction=0.6:SeparationType=GiniIndex:nCuts=20:NNodesMax=5" )
      "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20")

    # // Boosted Decision Trees
    # factory.BookMethod( ROOT.TMVA.Types.kBDT, "BDTG",
      # "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.30:UseBaggedGrad:GradBaggingFraction=0.6:SeparationType=GiniIndex:nCuts=20:NNodesMax=5" )

    factory.TrainAllMethods()
    factory.TestAllMethods()
    factory.EvaluateAllMethods()

    fout.Close()
    del factory


#-------------------------------------------------------------------------------

def main_merge():
  from rootpy.io import root_open
  from rootpy.tree import Tree

  method_name = 'BDT method'

  ## Prepare outputs
  fout = root_open( 'merged.root', 'RECREATE' )
  tree = Tree('merged')
  tree.create_branches({
    # Unique int for each sample. 0 is real, 1 is signal, and the rest are BG
    'classID' : 'I',
    #
    'weight'  : 'F',
    # Each classifier for each BG...
    'cls0'    : 'F',
    'cls1'    : 'F',
    'cls2'    : 'F',
    'cls3'    : 'F',
    'cls4'    : 'F',
    'M'       : 'F',
  })

  ## Prepare Readers (one for each background)
  readers = []
  for i in xrange(len(T_backgrounds)):
    reader = ROOT.TMVA.Reader( "!Color:!Silent" )
    adapter.attach_reader( reader )
    # reader.AddVariable( 'pi_PTFrac05C', adapter.f[0] )
    reader.BookMVA( method_name, 'weights/Factory%i_BDT.weights.xml'%i )
    readers.append( reader )

  M = array('d', [0.])

  ## Load input
  for i,(tin,weight) in enumerate([ (t_real,1.), (t_dytau, 1.) ] + T_backgrounds ):
    print 'Parsing tree: ', tin.GetTitle()

    tree.classID = i
    adapter.attach_tree( tin )  ## Called SetBranchAddress

    tin.SetBranchAddress( 'M' , M )

    nevt = tin.GetEntries()
    for j in xrange(nevt):
      if j%1000==0:
        print 'Parsing evt', j
      # if i!=0:
      #   ## PRESCALE 0.01 for fast TMVA! ( except real data, not used )
      #   if nevt > 1E4 and j%100!=0:
      #     continue 

      tin.GetEntry(j)
      adapter.sync()  ## Get double, then export to float for Reader

      tree.weight = weight
      tree.cls0   = readers[0].EvaluateMVA( method_name )
      tree.cls1   = readers[1].EvaluateMVA( method_name )
      tree.cls2   = readers[2].EvaluateMVA( method_name )
      tree.cls3   = readers[3].EvaluateMVA( method_name )
      tree.cls4   = readers[4].EvaluateMVA( method_name )

      tree.M    = M[0]
      
      tree.Fill()

  fout.Write()
  fout.Close()

  del readers

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------

# def main_fit():

#   tree = import_tree( 'multi_bkg', 'merged.root' )
#   fout = ROOT.TFile.Open( 'pass2.root', 'RECREATE' )

#   opts    = "!V:!Silent:Transformations=I;D;P;G,D:AnalysisType=Classification"
#   factory = ROOT.TMVA.Factory( 'pass2', fout, opts )
#   factory.AddVariable( 'cls0', 'cls0', '', 'F' )
#   factory.AddVariable( 'cls1', 'cls1', '', 'F' )
#   factory.AddSignalTree    ( tree, 1. )
#   factory.AddBackgroundTree( tree, 1. )

#   opts = "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V"
#   factory.PrepareTrainingAndTestTree( ROOT.TCut('classID==0'), ROOT.TCut('classID>0'), opts)

#   factory.BookMethod( ROOT.TMVA.Types.kBDT, "BDTG",
#     "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.30:UseBaggedGrad:GradBaggingFraction=0.6:SeparationType=GiniIndex:nCuts=20:NNodesMax=5" )
#   factory.TrainAllMethods()
#   factory.TestAllMethods()
#   factory.EvaluateAllMethods()

#   fout.Close()
#   del factory


#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------

# class MyFitness( ROOT.TMVA.IFitterTarget ):

#   def __init__(self, tree):
#     super( MyFitness, self).__init__()
#     self.tree    = tree 
#     self.hSignal = ROOT.TH1F( 'hsignal', 'hsignal', 100, -1, 1 )
#     self.hFP     = ROOT.TH1F( 'hfp'    , 'hfp'    , 100, -1, 1 )
#     self.hTP     = ROOT.TH1F( 'htp'    , 'htp'    , 100, -1, 1 )
#     #
#     tree.Draw( 'Entry$/Entries$ >> hsignal', 'classID==0', 'goff' )
#     self.weightsSignal = self.hSignal.Integral()

#   def EstimatorFunction( factors ):
#     print 'EstimatorFunction', factors
#     return -1.

#   def ProgressNotifier( sender, progress ):
#     print locals()

ROOT.gROOT.ProcessLine("""
namespace TMVA {
class MyFitness : public IFitterTarget {
public:
   // constructor
   MyFitness( TChain* _chain ) : IFitterTarget() {
      chain = _chain;

      hSignal = new TH1F("hsignal","hsignal",100,-1,1);
      hFP = new TH1F("hfp","hfp",100,-1,1);
      hTP = new TH1F("htp","htp",100,-1,1);

      TString cutsAndWeightSignal  = "(classID==1)";
      nSignal = chain->Draw("Entry$/Entries$>>hsignal",cutsAndWeightSignal,"goff");
      weightsSignal = hSignal->Integral();

   }

   // the output of this function will be minimized
   Double_t EstimatorFunction( std::vector<Double_t> & factors ){

      TString cutsAndWeightTruePositive  = Form(" weight * ((classID==1) && cls0>%f && cls1>%f && cls2>%f && cls3>%f && cls4>%f )",factors.at(0), factors.at(1), factors.at(2), factors.at(3), factors.at(4));
      TString cutsAndWeightFalsePositive = Form(" weight * ((classID >1) && cls0>%f && cls1>%f && cls2>%f && cls3>%f && cls4>%f )",factors.at(0), factors.at(1), factors.at(2), factors.at(3), factors.at(4));

      // Entry$/Entries$ just draws something reasonable. Could in principle anything
      Float_t nTP = chain->Draw("Entry$/Entries$>>htp",cutsAndWeightTruePositive,"goff");
      Float_t nFP = chain->Draw("Entry$/Entries$>>hfp",cutsAndWeightFalsePositive,"goff");

      weightsTruePositive = hTP->Integral();
      weightsFalsePositive = hFP->Integral();

      efficiency = 0;
      if( weightsSignal > 0 )
          efficiency = weightsTruePositive/weightsSignal;

      purity = 0;
      if( weightsTruePositive+weightsFalsePositive > 0 )
          purity = weightsTruePositive/(weightsTruePositive+weightsFalsePositive);

      Float_t effTimesPur = efficiency*purity;

      Float_t toMinimize = std::numeric_limits<float>::max(); // set to the highest existing number
      if( effTimesPur > 0 ) // if larger than 0, take 1/x. This is the value to minimize
          toMinimize = 1./(effTimesPur); // we want to minimize 1/efficiency*purity

      return toMinimize;
   }
    
    void Print(){
        std::cout << std::endl;
        std::cout << "======================" << std::endl
        << "Efficiency : " << efficiency << std::endl
        << "Purity     : " << purity << std::endl << std::endl
        << "True positive weights : " << weightsTruePositive << std::endl
        << "False positive weights: " << weightsFalsePositive << std::endl
        << "Signal weights        : " << weightsSignal << std::endl;
    }
    
    Float_t nSignal;
    
    Float_t efficiency;
    Float_t purity;
    Float_t weightsTruePositive;
    Float_t weightsFalsePositive;
    Float_t weightsSignal;
    
    
private:
   TChain* chain;
   TH1F* hSignal;
   TH1F* hFP;
   TH1F* hTP;
    
};
}
""")

from ROOT.TMVA import MyFitness

def main_fit():
  """Try to find optimal BDT cuts where global efficiency is maximized..."""

  tree      = import_tree( 'merged', 'merged_prescale.root' )
  estimator = MyFitness( tree )
  
  ## Equal numbers of BKG
  ranges    = ROOT.std.vector('TMVA::Interval*')()
  ranges.push_back( ROOT.TMVA.Interval(-1,1) )
  ranges.push_back( ROOT.TMVA.Interval(-1,1) )
  ranges.push_back( ROOT.TMVA.Interval(-1,1) )
  ranges.push_back( ROOT.TMVA.Interval(-1,1) )
  ranges.push_back( ROOT.TMVA.Interval(-1,1) )
  # ranges.push_back( ROOT.TMVA.Interval(-1,1) )

  opts      = 'PopSize=100:Steps=30'
  fitter    = ROOT.TMVA.GeneticFitter( estimator, 'FitterName', ranges, opts )
  result    = ROOT.std.vector('Double_t')()
  # result.push_back(0.)
  # result.push_back(0.)
  # result.push_back(0.)
  # result    = array('d', [0., 0., 0.])

  res = fitter.Run( result )
  print 'res', res
  estimator.Print() 
  for x in result:
    print x 

#===============================================================================

# Dummy lumi (default unit)
LUMI = 816.87E12 / 1000  # XS unit in [mb]

# 0,0,0,0,0
n_backgrounds = [
   26. /  5966885. * T_backgrounds[0][1] * LUMI,
    9. /  2576506. * T_backgrounds[1][1] * LUMI,
   19. / 10034658. * T_backgrounds[2][1] * LUMI,
   41. /  1167321. * T_backgrounds[3][1] * LUMI,
    9. /  1118554. * T_backgrounds[4][1] * LUMI,
]

def main_draw():
  h = QHist()
  h.trees  = import_tree( 'merged', 'merged_full.root' )
  h.params = 'M'
  # h.cuts   = [ 'classID==%i'%i for i in xrange(0,1+1) ]
  h.cuts   = [ 'classID==%i'%i for i in xrange(0,6+1) ]
  # h.master_cuts = 'cls0 > 0.0132123669687', 'cls1 > -0.120196864383', 'cls2>0.127709465025', 'cls3>-0.18032471789', 'cls4>-0.150404592366'
  h.master_cuts = "cls0>0.2 & cls1>0.2 & cls2>0.1 & cls3>0.2 & cls4>0.1"
  # h.master_cuts = "cls0>0.8 & cls1>0.8 & cls2>0.8 & cls3>0.8 & cls4>0.8"
  # h.master_cuts = 'cls0>-0.84 & cls1>-0.9657 & cls2>-0.804 & cls3>-0.742 & cls4>0.937'
  h.xmin   = 0
  h.xmax   = 120E3
  # h.normalize = [ 1318 ] + [ 1318 - sum(n_backgrounds) ] + n_backgrounds
  # h.st_code = 2
  h.draw()

  # QHistUtils.fraction_fit(*h)

  raw_input()


if __name__ == '__main__':
  # main_training()
  # main_training2()
  # main_merge()
  # main_fit()
  main_draw()