
from Configurables import LoKi__Hybrid__PlotTool as PlotTool 
from Configurables import LoKi__Hybrid__TupleTool as TupleTool 
from Configurables import TupleToolDecay  # So the branch can be operated on
from DecayTreeTuple.Configuration import DecayTreeTuple  # Has extra pythonic method.


#-----------#
# TUPLETOOL # =================================================================
#-----------#

# def fill_funcdict(tup, funcdict):
#   ## Helper for Hybrid::tupleTool, it seems somewhat a singleton item...
#   # tool = tup.addTupleTool('LoKi::Hybrid::TupleTool', tname)  # not working
#   # tool = tup.addTupleTool(TupleTool(tname))  # not wkr
#   tname = 'MyHTT'
#   tool  = getattr(tup, tname, None)
#   if tool is None:
#     tool = tup.addTupleTool('LoKi::Hybrid::TupleTool', tname)
#     tool.Preambulo = preambulo
#   ## Support compat for simple list into dict.
#   if isinstance( funcdict, (list,tuple)):
#     funcdict = {s:s for s in funcdict}
#   ##
#   tool.Variables.update(funcdict)


# #-----#
# # TAU #
# #-----#

# ### Generate the DTT of tau candidates
# for ttype,sel in TAU_SELECTIONS.iteritems():
#   if ttype != 'h3':
#     tname         = 'tau_'+ttype
#     tup           = DecayTreeTuple( tname, TupleName=tname, NTupleDir='LooseTau', Decay='X' )
#     tup.Inputs    = [ sel.outputLocation() ]
#     tup.Branches  = {'reco':'^(X)'}

#     ## Use Refitted Vertices
#     tup.UseP2PVRelations   = True
#     tup.InputPrimaryVertices = 'Phys/%s/_RefitPVs'%sel.name()
#     tup.P2PVInputLocations = [ 'Phys/%s/Particle2VertexRelations'%sel.name() ]
#     #

#     # tup.ToolList = []
#     tup.ToolList = [ 'TupleToolRecoStats' ] #, 'TupleToolPrimaries' ]
#     # Apply to all
#     fill_funcdict( tup, Functors.FD_ISOLATION )
#     fill_funcdict( tup, Functors.FD_TRACK     )
#     fill_funcdict( tup, [ 'BPVIP', 'BPVIPCHI2', 'BPVSIP', 'PT', 'ETA' ] )
#     fill_funcdict( tup, { 
#       'MIPDV'     :'MIPDV(PRIMARY)',
#       'MIPCHI2DV' :'MIPCHI2DV(PRIMARY)',
#     })
#     #
#     # tool = tup.addTupleTool('TupleToolGeometry', 'TTGeometry')
#     # tool.Verbose = True
#     #
#     # Finally
#     # seq.Members.append(tup)

#     ## Second debug version to compare without P2PV correction.
#     tname += '_NoRefit'
#     tup = DecayTreeTuple( tname, TupleName=tname, NTupleDir='LooseTau', Decay='X' )
#     tup.Inputs    = [ sel.outputLocation() ]
#     tup.Branches  = {'reco':'^(X)'}
#     tup.ToolList  = [ 'TupleToolRecoStats' ] #, 'TupleToolPrimaries' ]
#     fill_funcdict( tup, Functors.FD_ISOLATION )
#     fill_funcdict( tup, [ 'BPVIP', 'BPVSIP', 'PT', 'ETA' ] )
#     fill_funcdict( tup, {'MIPDV':'MIPDV(PRIMARY)'})

#     #
#     # tool = tup.addTupleTool('TupleToolGeometry', 'TTGeometry2')
#     # tool.Verbose = True
#     #
#     # Finally
#     # seq.Members.append(tup)


# # ### tau_h3 is special...
# # tup = DecayTreeTuple( 'tau_h3', TupleName='tau_h3')
# # tup.Decay     = '[ tau- -> ^pi- ^pi- ^pi+ ]CC'
# # tup.Inputs    = [ TAU_SELECTIONS['h3'].outputLocation() ]
# # tup.ToolList += [ 'TupleToolTrackInfo' ]
# # branches      = tup.addBranches({
# #   'recotau': '^([ tau- ->  pi-  pi-  pi+ ]CC)',
# #   'pi1'    : '  [ tau- -> ^pi-  pi-  pi+ ]CC ',
# #   'pi2'    : '  [ tau- ->  pi- ^pi-  pi+ ]CC ',
# #   'pi3'    : '  [ tau- ->  pi-  pi- ^pi+ ]CC ',
# # })
# # # mother only
# # fill_isocone_suite(branches['recotau'])
# # fill_trio(branches['recotau'])
# # # child only
# # fill_mctruth(branches['pi1'])
# # fill_mctruth(branches['pi2'])
# # fill_mctruth(branches['pi3'])
# # finally
# # seq.Members.append(tup)  # comment to skip


# #-------#
# # DITAU #
# #-------#

# def subdesc(desc):
#   """
#   Given a marked decay chain, yield new decay chain made from master but
#   contain only single caret '^'. The first one is always presented.

#   Usage:
#       >>> for x in subdesc( 'Z0 -> ^e-  ^e+' ): print x 
#       ^( Z0 -> e-  e+ )
#       Z0 -> ^e-   e+
#       Z0 ->  e-  ^e+
#   """
#   ## first one
#   yield '^( %s )'%(desc.replace('^',''))
#   ## The rest 
#   for m in re.finditer(r'\^', desc):
#     index = m.start()
#     res   = list(desc.replace('^', ' '))
#     res[index:index+1] = '^'
#     res   = ''.join(res)
#     yield res


# ## Define the branches
# DITAU_DECAYS_DESC = {
  # 'e_e'     : '  Z0 -> ^e-                       ^e+                          ',
  # 'e_e_ss'  : '[ Z0 -> ^e-                       ^e-                       ]CC',
  # 'e_h1'    : '[ Z0 -> ^e-                       ^pi+                      ]CC',
  # 'e_h1_ss' : '[ Z0 -> ^e-                       ^pi-                      ]CC',
  # 'e_h3'    : '[ Z0 -> ^e-                       ^(tau+ -> ^pi+ ^pi+ ^pi-) ]CC',
  # 'e_h3_ss' : '[ Z0 -> ^e-                       ^(tau- -> ^pi- ^pi- ^pi+) ]CC',
  # 'e_mu'    : '[ Z0 -> ^e-                       ^mu+                      ]CC',
  # 'e_mu_ss' : '[ Z0 -> ^e-                       ^mu-                      ]CC',
  # 'h1_h1'   : '  Z0 -> ^pi-                      ^pi+                         ',
  # 'h1_h1_ss': '[ Z0 -> ^pi-                      ^pi-                      ]CC',
  # 'h1_h3'   : '[ Z0 -> ^pi-                      ^(tau+ -> ^pi+ ^pi+ ^pi-) ]CC',
  # 'h1_h3_ss': '[ Z0 -> ^pi-                      ^(tau- -> ^pi- ^pi- ^pi+) ]CC',
  # 'h1_mu'   : '[ Z0 -> ^pi-                      ^mu+                      ]CC',
  # 'h1_mu_ss': '[ Z0 -> ^pi-                      ^mu-                      ]CC',
  # 'h3_h3'   : '  Z0 -> ^(tau- -> ^pi- ^pi- ^pi+) ^(tau+ -> ^pi+ ^pi+ ^pi-)    ',
  # 'h3_h3_ss': '[ Z0 -> ^(tau- -> ^pi- ^pi- ^pi+) ^(tau- -> ^pi- ^pi- ^pi+) ]CC',
  # 'h3_mu'   : '[ Z0 -> ^(tau- -> ^pi- ^pi- ^pi+) ^mu+                      ]CC',
  # 'h3_mu_ss': '[ Z0 -> ^(tau- -> ^pi- ^pi- ^pi+) ^mu-                      ]CC',
  # 'mu_mu'   : '  Z0 -> ^mu-                      ^mu+                         ',
  # 'mu_mu_ss': '[ Z0 -> ^mu-                      ^mu-                      ]CC',
# }

# ## Naming logic note:
# # - I need the branches name before appMgr will run, in order to deterministally
# #   provide relevant TupleTool to appropriate branches. (Less confusion from 
# #   over-provided data, as wel as less storage usage too).
# # - Naming will apply to same-sign branches too, think about this.
# DITAU_BRANCHNAMES = {
#   'e_e'   : ( 'ditau', 'e1'   , 'e2'  ),
#   'e_h1'  : ( 'ditau', 'e'    , 'pi'  ),
#   'e_h3'  : ( 'ditau', 'e'    , 'tau', 'pr1', 'pr2', 'pr3' ),
#   'e_mu'  : ( 'ditau', 'e'    , 'mu'  ),
#   'h1_h1' : ( 'ditau', 'pi1'  , 'pi2' ),
#   'h1_h3' : ( 'ditau', 'pi'   , 'tau', 'pr1', 'pr2', 'pr3' ),
#   'h1_mu' : ( 'ditau', 'pi'   , 'mu'  ),
#   'h3_h3' : ( 'ditau', 'tau1' , 'tau1pr1', 'tau1pr2', 'tau1pr3', 'tau2', 'tau2pr1', 'tau2pr2', 'tau2pr3' ),
#   'h3_mu' : ( 'ditau', 'tau'  , 'pr1' , 'pr2' , 'pr3' , 'mu' ),
#   'mu_mu' : ( 'ditau', 'mu1'  , 'mu2' )
# }



### Make respective DTT for Ditau
# for nickname, desc in DITAU_DECAYS_DESC.iteritems():
#   tname     = 'ditau_'+nickname        # New name on ntuple
#   src       = 'DitauCand_'+nickname    # MUST MATCHES with the Selection-CombineParticles
#   tup       = DecayTreeTuple(tname, TupleName=tname, NTupleDir='DitauCand', Decay=desc, Inputs=['Phys/'+src])
#   branching = dict(zip( DITAU_BRANCHNAMES[nickname.replace('_ss','')], subdesc(desc) ))
#   branches  = tup.addBranches(branching)

#   ## Global tools for every branches
#   ## Need to redefine the default due to inhomogeneity against default-auto tools
#   tup.ToolList = [ 
#     'TupleToolRecoStats',  # To get nPV
#     # 'TupleToolEventInfo',
#     # 'TupleToolKinematic',   # MMERR is non-uniform here, can silently crash, careful
#     # 'TupleToolGeometry',    # Careful, Not work on mmt-combined ditau
#   ]

#   # tup.P2PVInputLocations = [ "Phys/LooseTau_mu/PVRefit_P2PV_sorted" ]
#   tup.P2PVInputLocations = [ 
#     "Phys/LooseTau_mu/Particle2VertexRelations",
#     "Phys/LooseTau_h1/Particle2VertexRelations",
#     "Phys/LooseTau_e/Particle2VertexRelations",
#   ]

#   ## Custom tool: TTStripping
#   from whitelist import LIST_STRIPPING  # External file
#   tool = tup.addTupleTool('TupleToolStripping', 'TTStripping')
#   tool.StrippingList = LIST_STRIPPING

#   ## Selective applies
#   for bname, branch in branches.iteritems():

#     ## Global
#     fill_funcdict( branch, [ 'PT', 'MM', 'ETA' ] ) # FD_KINEMATIC

#     ## Exclusive to ditau 
#     if bname == 'ditau':
#       # fill_funcdict( branch, Functors.FD_VERTEX       ) # minz
#       # fill_funcdict( branch, Functors.FD_BPVSERIES    ) # minz
#       fill_funcdict( branch, Functors.FD_MOM2CHILDREN )

#     ## Exclusive to tau_h3 -- mom
#     elif bname.startswith('tau') and 'pr' not in bname:
#       fill_funcdict( branch, Functors.FD_VERTEX       )
#       fill_funcdict( branch, Functors.FD_BPVSERIES    )
#       fill_funcdict( branch, Functors.FD_MOM3CHILDREN )
#       fill_funcdict( branch, Functors.FD_ISOLATION    )

#     ## Exclusive to children ( e, mu, pi ), but not h3's prongs
#     elif 'pr' not in bname :
#       branch.ToolList += [ 'TupleToolGeometry' ]
#       fill_funcdict( branch, Functors.FD_ISOLATION )
#       fill_funcdict( branch, Functors.FD_TRACK     )
#       # fill_funcdict( branch, Functors.FD_PROTO     ) # minz
#       fill_funcdict( branch, [
#         'BPVDLS', 'BPVIP', 'BPVIPCHI2', 'BPVSIP',
#         'HCALFrac', 'ECALFrac',
#       ])
#       fill_funcdict( branch, { 
#         'MIPDV'     :'MIPDV(PRIMARY)',
#         'MIPCHI2DV' :'MIPCHI2DV(PRIMARY)',
#       })

#   # Finally  
#   # seq.Members.append(tup)
