
import re

from PhysSelPython.Wrappers import Selection, SimpleSelection
from StandardParticles import PFParticles, StdAllLooseMuons, StdAllNoPIDsPions, StdAllNoPIDsElectrons
# from Configurables import FilterDesktop, CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

from Configurables import LoKi__Hybrid__PlotTool as PlotTool 
from Configurables import LoKi__Hybrid__TupleTool as TupleTool 
from Configurables import TupleToolDecay  # So the branch can be operated on
from DecayTreeTuple.Configuration import DecayTreeTuple  # Has extra pythonic method.

from BenderCK import Functors 

preambulo = """

from LoKiMC.decorators import *      # MCMOTHER
from LoKiPhysMC.decorators import *  # MCSELMATCH
from LoKiCore.math import log10

from BenderCK.Functors import *

TOS_MUON0   = TOS('L0MuonDecision','L0TriggerTisTos')
TOS_MUON1   = TOS('Hlt1SingleMuonHighPTDecision','Hlt1TriggerTisTos')
TOS_MUON2   = TOS('Hlt2SingleMuonHighPTDecision','Hlt2TriggerTisTos')
TOS_MUON    = TOS_MUON0 & TOS_MUON1 & TOS_MUON2

TOS_ELECTRON0   = TOS('L0ElectronDecision','L0TriggerTisTos')
TOS_ELECTRON1   = TOS('Hlt1SingleElectronNoIPDecision','Hlt1TriggerTisTos')
TOS_ELECTRON2   = TOS('Hlt2SingleTFVHighPtElectronDecision','Hlt2TriggerTisTos')
TOS_ELECTRON    = TOS_ELECTRON0 & TOS_ELECTRON1 & TOS_ELECTRON2

""".split('\n')


def join(cuts):
  return '('+')&('.join(cuts)+')'


### Helper method to create MultSeq from list of Sel
from PhysSelPython.Wrappers import SelectionSequence, MultiSelectionSequence
def MultiSeq(name, list_sel):
  """Shortcut method to wrap MultiSelSeq of list_seq from list_sel."""
  return MultiSelectionSequence(name, [ SelectionSequence('Seq'+sel.name(), sel) for sel in list_sel ])


#==============================================================================

## ATTACH 

from Configurables import GaudiSequencer
masterseq = GaudiSequencer('MasterSeq')

from Configurables import DaVinci
DaVinci().UserAlgorithms += [ masterseq ]

#==============================================================================

## Spec for single particle monitoring
dplots = {
  'log10(PT)'               : ( 'log10(PT)'       ,  1, 5   ),
  'log10(TRPCHI2)'          : ( 'log10(TRPCHI2)'  , -5, 0   ),
  'ETA'                     : ( 'ETA'             ,  0, 6   ),
  'PPFUN(PP_CaloPrsE)'      : ( 'CaloPrsE'        ,  0, 300 ),
  'TRGHOSTPROB'             : ( 'TRGHOSTPROB'     ,  0, 1   ),
  'HCALFrac'                : ( 'HCALFrac'        , -1, 2   ),
  'ECALFrac'                : ( 'ECALFrac'        , -1, 2   ),
  'switch(ISMUON,1,0)'      : ( 'ISMUON'          , -1, 2   ),
  'switch(ISLOOSEMUON,1,0)' : ( 'ISLOOSEMUON'     , -1, 2   ),
}

#===============================================================================

## Particle identification only (pi is shared to h1, h3)
# removed tracking
PID_CUTS = {
  'mu': [
    'ETA > 2.0',
    'ETA < 4.5',
    'ISMUON',
  ],
  'e': [
    'ETA        > 2.0',
    'ETA        < 4.5',
    'PPFUN(PP_CaloPrsE) > 50',        # 0.05 GeV
    'PPFUN(PP_CaloEcalE,0)/P > 0.1',
    'PPFUN(PP_CaloHcalE,99999)/P < 0.05',
    '~ISLOOSEMUON',
  ],
  'pi': [
    'ETA > 2.0',
    'ETA < 4.5',
    'PPFUN(PP_CaloHcalE,0)/P > 0.05',
    '~ISLOOSEMUON',
  ],
}

# Flexible selection cut at single-tau level
TAU_SELECTIONS_CUTS = {
  'e' : PID_CUTS['e' ] + [ 'PT > 5000' ],
  'mu': PID_CUTS['mu'] + [ 'PT > 5000' ],
  'h1': PID_CUTS['pi'] + [ 'PT > 5000' ],
  'h3': {
    'dcuts': PID_CUTS['pi'],
    'mcuts': [ 
      'PT  > 10000',  # lower than final 12*GeV
      'M   > 600',    # lower than final 700
      'M   < 1500',
      'PTTRIOMIN > 1E3',
    ],
  },
}


def TauSelection( ttype ): # code_pid
  """ Real PID selection for tau cand. """

  # ## Make a PID filter with no cut first, just to prep for monitoring
  # sel = SimpleSelection( 'PidPreselectionPF_'+ttype, FilterDesktop, [PFParticles], 
  #   'FiltPidPreselectionPF_'+ttype, Code=code_pid, Preambulo=preambulo ) 
  sel = {
    'e' : StdAllNoPIDsElectrons,
    'h1': StdAllNoPIDsPions,
    'mu': StdAllLooseMuons,
  }[ttype]

  ## Then, the real selection 
  return SimpleSelection('Tau_'+ttype, FilterDesktop, [sel],
    Preambulo = preambulo,
    Code      = join(TAU_SELECTIONS_CUTS[ttype]),
  )

  # algo.addTool( PlotTool ( 'InputPlots' ))
  # algo.addTool( PlotTool ( 'OutputPlots' ))
  # algo.Monitor            = True
  # algo.HistoProduce       = True
  # algo.InputPlots.Histos  = dplots  # before Code
  # algo.OutputPlots.Histos = dplots  # after Code (i.e., before ditau)
  #
  # algo.ReFitPVs     = True
  # algo.IgnoreP2PVFromInputLocations = True
  #


### tau_h3
def TauH3Selection(name, desc):
  dcut = join(TAU_SELECTIONS_CUTS['h3']['dcuts'])
  return SimpleSelection(name, CombineParticles, [ StdAllNoPIDsPions ],
    Preambulo        = preambulo,
    DecayDescriptor  = desc,
    DaughtersCuts    = {'pi-': dcut, 'pi+':dcut },
    MotherCut        = join(TAU_SELECTIONS_CUTS['h3']['mcuts']),
  )

sel_tauh3  = TauH3Selection('Tau_h3' , '[ tau- -> pi- pi- pi+ ]cc')
sel_tauh3s = TauH3Selection('Tau_h3s', '[ tau- -> pi- pi- pi- ]cc')

TAU_SELECTIONS = {
  'e'  : TauSelection( 'e'  ),
  'mu' : TauSelection( 'mu' ),
  'h1' : TauSelection( 'h1' ),
  'h3' : sel_tauh3,
  'h3s': sel_tauh3s, # control for combi
}

seq0_tau = MultiSeq('SeqTauMaker', TAU_SELECTIONS.values())

#===============================================================================

#-------------------------------------------#
# DITAU - Preselection                      #
# the actual selection will be done offline #
#-------------------------------------------#

## Control the generation of each dtype here
## Disable the HH modes in Run-I analysis

DITAU_DECAYS_COMBINE = {
  ## Core chanels
  'e_e'  : '  Z0 -> e-   e+      ',
  'e_h1' : '[ Z0 -> e-   pi+  ]cc',
  'e_h3' : '[ Z0 -> e-   tau+ ]cc',
  'e_mu' : '[ Z0 -> e-   mu+  ]cc',
  'h1_mu': '[ Z0 -> pi-  mu+  ]cc',
  'h3_mu': '[ Z0 -> tau- mu+  ]cc',
  'mu_mu': '  Z0 -> mu-  mu+     ',
  #
  # Unused pure hadron channels
  # 'h1_h1': '  Z0 -> pi-  pi+     ',
  # 'h1_h3': '[ Z0 -> pi-  tau+ ]cc',
  # 'h3_h3': '  Z0 -> tau- tau+    ',
  #
  # More control channels
  'h3s_mu': '[ Z0 -> tau- mu+  ]cc',
  'e_h3s' : '[ Z0 -> e-   tau+ ]cc',
}

has_good_elec = 'AHASCHILD((ABSID==11) & (PT>15000) & TOS_ELECTRON)'
has_good_muon = 'AHASCHILD((ABSID==13) & (PT>15000) & TOS_MUON)'

DITAU_PRESELECTIONS_CCUTS = {
  'e_e'   : has_good_elec,
  'e_h1'  : has_good_elec,
  'e_h3'  : has_good_elec,
  'e_mu'  : has_good_elec + '|' + has_good_muon,
  'h1_mu' : has_good_muon,
  'h3_mu' : has_good_muon,
  'mu_mu' : has_good_muon,
  #
  'h3s_mu': has_good_muon,
  'e_h3s' : has_good_elec,
}


def flip_opsign_to_samesign(decay):
  """
  Given a decay descriptor, flip all the charge signs.
  """
  decay = decay.replace('+', '-')
  if ']cc' not in decay: # flipped self-conjugate lost its symmetry
    decay = '[ %s ]cc'%decay
  return decay


def MakeCombDitau( dtype, decay, inputs, suffix ):
  name = 'ditau_' + dtype + suffix
  return SimpleSelection(name, CombineParticles, inputs,
    Preambulo          = preambulo,
    DecayDescriptor    = decay,
    CombinationCut     = DITAU_PRESELECTIONS_CCUTS[dtype],
    MotherCut          = 'ALL', # no mass cut to allow scan
    ParticleCombiners  = {'':'MomentumCombiner:PUBLIC'},
  )

  # algo.HistoProduce       = True
  # algo.Monitor            = True
  # algo.addTool( PlotTool ( 'DaughtersPlots'   ))
  # algo.addTool( PlotTool ( 'CombinationPlots' ))
  # algo.DaughtersPlots.Histos    = dplots # Not needed, since daughter is mixed
  # algo.CombinationPlots.Histos  = {   # Plot after ccuts, before mcuts
  #   'MM' : ( 'MM', 0, 120000 ),
  # }
  #
  # algo.ReFitPVs     = True
  # algo.IgnoreP2PVFromInputLocations = True
  # algo.OutputLevel = 0


## Generate list of all ditau Selections
sel_ditau = []
for dtype, decay in DITAU_DECAYS_COMBINE.iteritems():
  decayss         = flip_opsign_to_samesign(decay)
  inputs_normal   = [ sel for ttype,sel in TAU_SELECTIONS.iteritems() if ttype in dtype.split('_') ]
  sel_ditau.append(MakeCombDitau( dtype, decay  , inputs_normal , ''            ))
  sel_ditau.append(MakeCombDitau( dtype, decayss, inputs_normal , '_ss'         ))


### Finally, the candidate seq
seq0_ditau = MultiSeq('SeqDitauMaker', sel_ditau)
seq_ditau  = seq0_ditau.sequence()  # Ditau

masterseq.Members += [ seq_ditau ]


#-------------------------------------------------------------------------------
# PVReFitterAlg
#-------------------------------------------------------------------------------

refit_locations = []
# refit_locations += seq0_loosetau.outputLocations()  ## NEEDS ONLY FOR LOOSE-STUDY
refit_locations += seq0_tau.outputLocations()       ## Need for tau-level cut
refit_locations += seq0_ditau.outputLocations()     ## Need for ditau-level cut


## Phys/TightTau_%s/PVR_withpvo_P2PV
from Configurables import PVReFitterAlg
algo = PVReFitterAlg('PVR_withpvo')
algo.UseIPVOfflineTool      = True
algo.ParticleInputLocations = refit_locations
masterseq.Members += [ algo ]


## Need single relator for each species
from Configurables import PVRelatorAlg, BestPVAlg

## Output to Phys/TightTau_h1/BestPVAlg_P2PV
algo = BestPVAlg("BestPVAlg")
algo.P2PVRelationsInputLocations = [ s.replace('/Particles', '/PVR_withpvo_P2PV' ) for s in refit_locations ]
masterseq.Members += [ algo ]


#===============================================================================
