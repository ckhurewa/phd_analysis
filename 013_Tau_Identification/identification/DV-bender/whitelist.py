#!/usr/bin/env python

"""
The list is reused several times around. So modified with care.
"""

LIST_L0 = (
  'L0ElectronDecision',
  'L0PhotonDecision',
  'L0HadronDecision',
  'L0MuonDecision',
  'L0DiMuonDecision',
  'L0Muon,lowMultDecision',
  'L0DiMuon,lowMultDecision',
  'L0Electron,lowMultDecision',
  'L0Photon,lowMultDecision',
  'L0DiEM,lowMultDecision',
  'L0DiHadron,lowMultDecision',
)

LIST_HLT1 = (
  'Hlt1SingleElectronNoIPDecision',
  'Hlt1SingleMuonHighPTDecision',
  'Hlt1TrackMuonDecision',
  'Hlt1TrackPhotonDecision',
  'Hlt1TrackAllL0Decision',
  'Hlt1TrackAllL0TightDecision',
)

LIST_HLT2 = (
  'Hlt2DiElectronBDecision',
  'Hlt2DiMuonBDecision',
  'Hlt2DiMuonDY3Decision',
  #
  'Hlt2SingleMuonHighPTDecision',
  'Hlt2SingleMuonVHighPTDecision',
  'Hlt2SingleTFElectronDecision',
  'Hlt2SingleTFVHighPtElectronDecision',
  #
  'Hlt2CharmHadD02HHXDst_hhXDecision',
  'Hlt2CharmHadD02HHXDst_hhXWideMassDecision',
  'Hlt2CharmHadD02HH_D02KKDecision',
  'Hlt2CharmHadD02HH_D02KKWideMassDecision',
  'Hlt2CharmHadD02HH_D02KPiDecision',
  'Hlt2CharmHadD02HH_D02KPiWideMassDecision',
  'Hlt2CharmHadD02HH_D02PiPiDecision',
  'Hlt2CharmHadD02HH_D02PiPiWideMassDecision',
  # 'Hlt2CharmHadD02HHHDecision',  ## Where are you???
  #
  'Hlt2Topo2BodyBBDTDecision',
  'Hlt2Topo3BodyBBDTDecision',
  'Hlt2Topo4BodyBBDTDecision',
  'Hlt2TopoE2BodyBBDTDecision',
  'Hlt2TopoE3BodyBBDTDecision',
  'Hlt2TopoE4BodyBBDTDecision',
  'Hlt2TopoMu2BodyBBDTDecision',
  'Hlt2TopoMu3BodyBBDTDecision',
  'Hlt2TopoMu4BodyBBDTDecision',
)


LIST_HLT2_RUN2 = (
  #
)

# Use area
# - Event prefilter
# - enhancedTuple tupletool ()

LIST_STRIPPING = list({
  # ## S20 - Prospective Guess
  # 'StrippingZ02TauTauProng_LineDecision',
  # 'StrippingZ02TauTauProng_SameSignAll_LineDecision',
  # 'StrippingZ02TauTauProng_SameSignTau_LineDecision',
  # 'StrippingZ02TauTauProng_SameSign_LineDecision',
  'StrippingZ02TauTau_EXLineDecision',
  'StrippingZ02TauTau_EXLineSSDecision',
  'StrippingZ02TauTau_MuXLineDecision',
  'StrippingZ02TauTau_MuXLineSSDecision',
  'StrippingZ02MuMuLineDecision',
  'StrippingZ02eeLineDecision',
  'StrippingZ02eeSSLineDecision',
  
  ## S21 - Prospective Guess
  'StrippingDitau_MuXLineDecision',
  'StrippingDitau_MuXssLineDecision',
  'StrippingDitau_EXLineDecision',
  'StrippingDitau_EXssLineDecision',
  # #
  # 'StrippingZ02MuMuLineDecision',
  # 'StrippingZ02eeLineDecision',
  # #
  'StrippingWMuControl10LineDecision', # For QCD-shape background
  'StrippingWMuLineDecision',      # PT>20
  'StrippingWMuLowLineDecision',   # PT>15, prescale 0.1
  'StrippingWeLineDecision',       # PT>20
  'StrippingWeLowLineDecision',    # PT>15, prescale 0.1

  # ## S21: Top list of Z02TauTau
  # "StrippingWMuLineDecision",                 # 715
  # "StrippingDitau_MuXLineDecision",           # 630
  # "StrippingWMuControl4800LineDecision",      # 1038  X  0.40
  # "StrippingWeLineDecision",                  # 382
  # "StrippingDitau_EXLineDecision",            # 308
  # "StrippingDitau_MuXssLineDecision",         # 187
  # "StrippingDitau_EXssLineDecision",          # 144
  #
  # "StrippingSingleTrackTISLineDecision",      # 283   X  0.10
  # "StrippingWMuLowLineDecision",              # 119   X  0.10
  # "StrippingDisplVerticesLinesHLTPSDecision", # 90    X  0.20
  # "StrippingWeLowLineDecision",               # 87    X  0.10
  # "StrippingWmuAKTJetsLineDecision",          # 82
  # "StrippingWeAKTJetsLineDecision",           # 76

  # ## S21: Top list of H2AA (PRELIM)
  # 'StrippingDisplVerticesLinesJetSingleHighMassDecision',      # 14491
  # 'StrippingDisplVerticesLinesDoubleDecision',                 # 11708
  # 'StrippingWMuControl4800LineDecision',                       #  8695  X 0.40
  # 'StrippingWmuAKTJetsLineDecision',                           #  4592
  # 'StrippingMicroDSTDiMuonDiMuonIncLineDecision',              #  4373
  # 'StrippingDisplVerticesLinesCharmHLTDecision',               #  4329
  # 'StrippingWMuLineDecision',                                  #  4053
  # 'StrippingDitau_MuXLineDecision',                            #  3748
  # 'StrippingWeAKTJetsLineDecision',                            #  3028
  # 'StrippingDisplVerticesLinesHLTPSDecision',                  #  2652  X 0.20
  # 'StrippingDitau_MuXssLineDecision',                          #  2460
  # 'StrippingDoubleTopoLineDecision',                           #  2436
  # 'StrippingWeLineDecision',                                   #  2175
  # 'StrippingDitau_EXLineDecision',                             #  1976
  # 'StrippingPseudoDoubleTopoLineDecision',                     #  1760
  # 'StrippingDitau_EXssLineDecision',                           #  1486
  
  # Lambdac2PHHLambdac2PPiPiLine                             3460
  # B2ppipiSigmacmm_Lcpi_PartRecoWS_SC_Line                  2391
  # B2ppipiSigmacmm_Lcpi_PartReco_Line                       2389
  # DstarPromptWithD02HHHHLine                               2274
  # D2hhh_HHHIncLine                                         2081
  # Bu2MuNuSignalLine                                        2057
  # HighPtGammaLine                                          1953
  # LTUnbCharmDs2KKPiLine                                    1942
  # LTUnbCharmD2KPiTISLine                                   1899
  # SingleTrackTISLine                                       1859
  # Lambdac2PHHLambdac2PKPiLine                              1835
  # LTUnbCharmLc2pKPiLine                                    1819
  # LTUnbCharmD2KPiPiLine                                    1768
  # B2XGammapi_Lambda_Line                                   1668
  # B2XGamma2pi_pi0R_Line                                    1656
  # Lambdac2PHHLambdac2PPiKWSLine                            1570
  # B2D0KPi0ResolvedD2HHBeauty2CharmLine                     1507
  # B02D0KKD2Pi0KPiResolvedBeauty2CharmLine                  1468
  # B2DDKBeauty2CharmLine                                    1460
  # B02D0KPiD2Pi0KPiResolvedBeauty2CharmLine                 1452
  # B2D0PiPi0ResolvedD2HHBeauty2CharmLine                    1447
  # B2XGamma3pi_Line                                         1443
  # B2ppipiSigmacmm_Lcpi_PartRecoWS_Line                     1443
  # B2XGammapi_Ks0_Line                                      1415
  # B2XGamma2pi_pi0M_Line                                    1277
  # B2XGamma3pi_pi0R_Line                                    1209
  # Ks2PiPiee_PiPiLine                                       1195
  # Jets_3jets_Pt7_3sv                                       1174
  # JetsbJetPT90                                             1168
  # B2XGamma3pi_pi0M_Line                                    1156
  # B02DKPi0ResolvedD2HHHCFPIDBeauty2CharmLine               1132
  # Xib2XicKKXic2PKPiBeauty2CharmLine                        1117
  # DY2MuMuLine3                                             1111
  # DY2MuMuLine2Hlt                                          1108
  # B02D0PiPiD2Pi0KPiResolvedBeauty2CharmLine                1104
  # FullDSTDiMuonDiMuonHighMassLine                          1099

})
