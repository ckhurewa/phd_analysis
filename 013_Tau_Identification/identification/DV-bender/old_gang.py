import sys
import os
from glob import glob

#===============================================================================

t = JobTemplate()
t.name        = 'Z02TauTau 8TeV'
# t.application = Bender(version='v25r7p4', module='bender.py')
t.application = Bender(version='v28r3', module=File('bender.py'))
# t.backend     = PBS(extraopts='--mem=4140 -t 1-0:0:0 --exclude=lphe01,lphe02,lphe03')
t.backend     = Dirac()

t.attach_mylib() # only for dirac

t.inputfiles += [ # For 6.1
  os.path.expandvars('$DIR13/identification/DV-bender/whitelist.py'),
  os.path.expandvars('$DIR13/identification/DV-bender/DV_tau_cand_maker.py'),
]

t.outputfiles = [ '*.root', 'summary.xml' ] # MassStorageFile


#===============================================================================
# Multiple submission
#===============================================================================

def flush_bookmark(name, uri):
  ## Get nickname, if any
  if name.startswith('ID_'):
    decid     = name.replace('ID_','')
    nickname  = Gauss.nickname(decid)
    comment   = decid+': '+nickname
  else:
    return # dont do it
  ## Verify dataset, variable split-size
  ds  = LHCbDataset.new(uri) 
  fpj = max(int(len(ds.files)**0.4),1)
  ## Then, create a job.
  j = Job(t)
  j.comment   = 'DitauCandTupleWriter: '+comment
  j.inputdata = ds
  j.splitter  = SplitByFiles( filesPerJob=fpj )
  queues.add( j.submit )

# ## Take all available data in bookmarks.py and run them all, one job each.
sys.path.append('../..')
import bookmarks
# for key,val in sorted(vars(bookmarks).iteritems()):
#   if not key.startswith('_'):
#     # flush_bookmark( key, val )
#     pass

# sys.exit()

#===============================================================================
# Vanilla submission
#===============================================================================

ds = LHCbDataset.new(bookmarks.DATA_S20_EW)
# ds = LHCbDataset.new(bookmarks.Zg_tautau) # Needs fiducial filter
# ds = LHCbDataset.new(bookmarks.Wmumujet)
# ds = LHCbDataset.new(bookmarks.We20jet)
# ds = LHCbDataset.new(bookmarks.ID_42321000) # We, inclusive

j = Job(t)
# j.comment     = 'DitauDebugger: 42150000 Zg2bb (Akevt/4)'
# j.comment     = 'DitauCandTupleWriter: S21 (Akevt)/20'
j.comment     = 'DitauCandTupleWriter: custom Zg2TauTau (1Mevt)/4 (fid+acc)'
# j.comment     = 'DitauCandTupleWriter: Custom We20+jet (2Mevt)/4'
# j.comment     = 'DitauCandTupleWriter: Custom minbias_mux (J4151,J4289) (120kevt)/5'
# j.comment     = 'DitauCandTupleWriter: 42100000: Z_tautau (no fid+acc filter)'
# j.comment     = 'DitauCandTupleWriter: 42321000: W_enue'
j.inputdata   = ds
j.splitter    = SplitByFiles(filesPerJob=4)

# j.submit()
queues.add(j.submit)

