
from JetAccessories.JetMaker_Config import JetMakerConf 

conf = JetMakerConf('HadronicJets',
  Inputs    = [ 'Phys/PFParticles/Particles' ],
  PFTypes   = [ 'ChargedHadron', 'Photon', 'Pi0', 'HCALNeutrals', 'NeutralRecovery' ],
  R         = 0.2,
  PtMin     = 500.,
  AssociateWithPV         = True,
  JetEnergyCorrection     = False ,
  JetIDCut                = False,
  jetidnumber             = 98 ,
  onlySaveB               = False,
  pvassociationMinIPChi2  = True,
  algtype                 = "Cambridge", 
  # algtype                 = "anti-kt", 
)

## ATTACH 
from Configurables import DaVinci
DaVinci().UserAlgorithms += conf.algorithms

