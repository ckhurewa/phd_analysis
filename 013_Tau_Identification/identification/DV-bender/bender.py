#!/usr/bin/env python

## Patch for remote
import os, sys
sys.path.insert(0, os.path.join(os.getcwd(),'BenderCK.zip'))

from collections import defaultdict
from random import random

from BenderCK import *
from BenderCK.Tau import *

## Need to borrow these def
sys.path.insert(0, os.path.expandvars('$DIR13/overview'))
from whitelist import LIST_L0, LIST_HLT1, LIST_HLT2, LIST_STRIPPING


#==============================================================================

class CheckPurity(BaseTauAlgoMC):
  """
  The purity of emu channel, even AFTER selection is stragely low
  (expect ~95%, but I have 8315/10889).
  """

  @pass_success
  def analyse(self):

    def selected(reco):
      """
      Return True if this reconstructed ditau of type emu passes final selection
      """
      pass

    ## Early abort if no good reco
    list_reco = self.get('Phys/ditau_e_mu/Particles')
    if not list_reco or len(list_reco)==0:
      return

    print self.event_break
    for ditau in self.list_ditau:
      if ditau.type != 'e_mu':
        continue
      print ditau

    for reco in list_reco:
      print reco
      print 'mcMatch_ditau:', self.mcMatch_ditau( reco )

#==============================================================================

class CheckSamepvIso(BaseTauAlgoMC):
  """
  The new behavior of checking-same-pv and isolation-neighbor does not seems to
  works well in harmony. Check me.

  For now, compare the isolation of h1 with,w/o needind neighbor to be from
  the same PV as core.
  """

  Inputs = [
    'Phys/TightTau_e',
    'Phys/TightTau_mu',
    'Phys/TightTau_h1',
  ]
  P2PVInputLocations = [
    'Phys/TightTau_e/PVR_withpvo_P2PV',
    'Phys/TightTau_mu/PVR_withpvo_P2PV',
    'Phys/TightTau_h1/PVR_withpvo_P2PV',
  ]

  @pass_success
  def analyse(self):
    for cpath in self.Inputs:
      ttype = cpath.split('_')[-1]
      for reco in self.selectTES( cpath ):
        #
        sumx1 = sumy1 = sumpt1 = 0.   # ignore PV
        sumx2 = sumy2 = 0.            # Ask for same PV
        sumx3 = sumy3 = sumpt3 = 0.   # ignorePV, DR-weighted
        ##
        Dr = DR2(reco)**0.5
        Hasproto  = HASPROTOS(reco)
        Vsim      = Functions.VSIM( self.bestPV(reco), minfrac = 0.7 )
        for p in self.selectTES('Phys/PFParticles'):
          if Q(p)==0: # Ignore neutral here
            continue
          dr = Dr(p)
          if dr > 0.5:
            continue
          if Hasproto(p):
            continue
          ## Sum up!
          px,py,pt = PX(p), PY(p), PT(p)
          sumx1   += px
          sumy1   += py
          sumpt1  += pt
          if Vsim(self.bestPV(p)):
            sumx2 += px
            sumy2 += py
          weightA = 1. - dr*2.  # (Anti-) Linear weight
          # weightB =
          sumx3   += weightA*px
          sumy3   += weightA*py
          sumpt3  += weightA*pt
        ## Finally, write
        ptcone1 = (sumx1**2 + sumy1**2)**0.5
        ptcone2 = (sumx2**2 + sumy2**2)**0.5
        ptcone3 = (sumx3**2 + sumy3**2)**0.5
        pt = PT(reco)
        with self.enhancedTuple( ttype ) as tup:
          tup.column('MATCH'             , self.mcMatch_tau(reco) is not None )
          tup.column('PT'                , pt )
          tup.column('PTFrac05C_ignorepv', pt/( pt+ptcone1 ))
          tup.column('PtFrac05C_ignorepv', pt/( pt+sumpt1  ))
          tup.column('PTFrac05C_samebpv' , pt/( pt+ptcone2 ))
          tup.column('PTFrac05C_weighted', pt/( pt+ptcone3 ))  # Vectorial sum
          tup.column('PtFrac05C_weighted', pt/( pt+sumpt3  ))  # Scalar sum


#==============================================================================

class TestTauMatcher(BaseTauAlgoMC):
  """
  Alternative approach for checking MC-match, with no dependence on choice
  of reco container.
  """

  Inputs = [
    'Phys/PFParticles',
    'Phys/StdAllLooseMuons',
    'Phys/StdAllNoPIDsElectrons',
  ]

  def valid(self, reco):
    ## CAREFUL! for some reason, '&' works, but 'and' does not!
    if ABSID(reco)!=11:
      return False
    if PT(reco)<5E3:
      return False
    cut = ((PPFUN(PP_CaloPrsE)>50) & (PPFUN(PP_CaloEcalE)/P>0.1) & (PPFUN(PP_CaloHcalE)>0) & (PPFUN(PP_CaloHcalE)/P<0.05) & (~ISLOOSEMUON))
    return cut(reco)

  @pass_success
  def analyse(self):
    # print self.event_break
    valid = False
    for tau in self.list_tau:
      if tau.type.e:
        mcp = tau.children(CHARGED)[0]
        if MCPT(mcp)>5E3:
          valid = True
          # print tau

    if not valid:
      return

    # >>> When there are 2 tau_e, how to check them?

    for tesname in ('PFParticles', 'StdAllNoPIDsElectrons'):
      found   = False
      purity  = self.Counter('PT15_purity/'+tesname)
      quality = self.Counter('PT15_quality/'+tesname)
      for reco in self.selectTES(tesname):
        if self.valid(reco):
          match = self.mcMatch_tau(reco)
          found |= match
          purity += int(match)
          if match:
            mcp = self.find_mcp_best(reco)
            quality += PT(reco)/MCPT(mcp)
      count_eff = self.Counter('PT15_efficiency/'+tesname)
      count_eff += int(found)

#==============================================================================

def printcol(cname, msg):
  """
  - Helper method to print things in 2 columns.
  - Long message will be split into chunks
  """
  LWIDTH = 120
  lmsg   = len(msg)
  cname  = '{:12}: '.format(cname)  # Add empty string to adjust constant width
  cblank = ' '*len(cname)
  if '\n' in msg: # Already has line-break
    for i,line in enumerate(msg.split('\n')):
      if i==0:
        print cname, line
      else:
        print cblank, line
  else:
    for i in xrange(lmsg/LWIDTH+1):
      istart = i*LWIDTH
      istop  = min((i+1)*LWIDTH, lmsg)
      submsg = msg[istart:istop]
      if i==0:
        print cname, submsg
      else:
        print cblank, submsg

class CheckFakeTauH3Tight(BaseTauAlgoMC):
  """
  Aim: Given the tight-cut tau_h3, debug for the origin of fake ones &
  possibilities to eliminate them.

  Need tau_h3 candidate from DV_tau_cand_maker
  """

  Inputs = ['Phys/TightTau_h3']

  P2PVInputLocations = [
    'Phys/TightTau_h3/BestPVAlg_P2PV',
  ]

  def valid(self, tauh3):
    """
    Put the final selection on tau-h3 here
    """
    ## mother cut
    cut1 = (PT>10E3) & (M>700) & (M<1500) & (VFASPF(VCHI2PDOF)<20) & (BPVCORRM<3E3)
    cut2 = (BPVVD>4) & (BPVVD<100)
    cut3 = (Functors.PTTRIOMIN>1E3) & (Functors.PTTRIOMAX>5E3)
    cut4 = (Functors.PTFrac05C > 0.8) # approximation
    return (cut1&cut2&cut3&cut4)(tauh3)

  @pass_success
  def analyse(self):
    """In the scheme of early-selecting tight tau_h3, see where the fake are from."""

    ## Prep cand
    cands_true = []
    cands_fake = []
    for cand in self.selectTES('Phys/TightTau_h3'):
      if self.valid( cand ):
        mcp = self.mcMatch_tau(cand)
        c = self.Counter('TightTau_h3 purity')
        if mcp is not None:
          cands_true.append(cand)
          c += 1
        else:
          cands_fake.append(cand)
          c += 0

    ## I just want to see how PVs look like
    for cand in cands_true:
      print '------------- cands_true -------------'
      print cand
      print self.bestPV(cand).position()
      for c in cand.children():
        print self.bestPV(c).position()

    ## Print the truth ondemand, if there's any fake cand
    if not cands_fake:
      return

    ## Look on the fake in more detail
    print self.event_break
    print '-------- list tau --------'
    for tau in self.list_tau:
      if tau.type.h3:
        print tau
        for child in tau.children(CHARGED&HADRON):
          print 'child-initial-reco: '
          for x in self.find_reco_all('Phys/StdAllNoPIDsPions', child):
            print str(x).strip()
        for child in tau.children(CHARGED&HADRON, final=True):
          print 'child-final-reco: '
          for x in self.find_reco_all('Phys/StdAllNoPIDsPions', child):
            print str(x).strip()


    for cand in cands_fake:
      print '------------- cands_fake -------------'
      print cand
      for c in cand.children():
        print self.bestPV(c).position()
      # for prong in cand.children():
      #   print '----------'
        # mcp   = self.p2mc(prong)
    #     mcp = self.find_mcp_best(prong)
    #     str(mcp);str(mcp)  # To flush error msg
    #     match = any(SAME(prong)(pi) for threepi in self._list_tau_h3_true_threepi for pi in threepi)
    #     print
    #     print 'prong      : ', str(prong).strip()
    #     printcol('prong-proto'  , str(prong.proto()).strip())
    #     printcol('prong-mcp'    , str(mcp).strip())
    #     if mcp:
    #       printcol('prong-mcp-mom', str(mcp.mother()).strip())
    #       mcpanc = mcp.ancestors()
    #       if mcpanc and len(list(mcpanc))>0:
    #         mcp = Functions.iBotCopyId(mcpanc[-1])
    #         print 'prong-mcp-ancestor[-1]', mcp
    #         print 'prong-mcpancmom', Functions.iTopCopyId(mcp).mother()

#------------------------------------------------------------------------------

class BaseTauCandTupleWriter(BaseTauAlgoMC):
  """
  Taken the candidate make from DV-tauMaker, provide the better-fine-tuned
  nTuple for those candidate. The most prominient feature is the accurate
  mcMatching against true tau in order to yield precise selectino cut.
  This also removes the load for `SetAlias` at ROOT-ntuple level too.
  """

  # def manual_check_allsamebpv( self, mother ):
  #   """
  #   TODO: Shouldn't this also include the PVRefitting correction.
  #   TODO: Can this be used on the ditau level? debug me...
  #   """
  #   if mother is None:
  #     return False
  #   vertices = [ self.bestPV(p) for p in mother.children() ]
  #   return all( v==vertices[0] for v in vertices )

  Inputs = []              ## DEFINE ME!
  P2PVInputLocations = []  ## DEFINE ME!

  ## Create functors once!
  StrippingList = { s:HLT_PASS(s) for s in LIST_STRIPPING }

  @pass_success
  def analyse(self):
    IS_ZTAUTAU = (self.event_type == 42100000)
    for cpath in self.Inputs:
      tname = cpath.split('_')[-1]  # Hopefully the ttype is embeded in cpath
      for reco in self.selectTES(cpath):
        ## Write only mcMatched, in case of Z02TauTau
        MATCH = (self.mcMatch_tau( reco ) is not None) if IS_ZTAUTAU else True
        if MATCH:
          self.write_tau_nomcp( tname, reco )

        ## Skip MC-Matching for eveything else
        # mcp = self.mcMatch_tau( reco ) if IS_ZTAUTAU else None

  def gen_stripping_decreports(self):
    ## It's sparse, that's why.
    reports = self.get('Strip/Phys/DecReports')
    if bool(reports):
      for sname,scut in sorted(self.StrippingList.iteritems()):
        yield sname, scut(reports)

  @lazy_algo_property
  def npv(self):
    return self.get('Rec/Summary').info(LHCb.RecSummary.nPVs, -1)

  def write_tau_nomcp( self, tname, reco ):
    # hasMC     = mcp  is not None
    # hasRECO   = reco is not None
    run,evt  = self.runevt

    ## Finally, write
    with self.enhancedTuple(tname) as tup:
      # tup.EventInfo.fill()
      # tup.RecoStats.fill()
      # tup.column       ( 'IS_MC'     , self.is_MC        )
      # tup.column       ( 'EVTYPE'    , self.event_type   )
      # tup.column       ( 'MATCH'     , hasMC and hasRECO )
      # tup.column       ( 'hasMC'     , hasMC             )
      # tup.column       ( 'hasRECO'   , hasRECO           )
      tup.column  ( 'RunNumber' , run               )
      tup.column  ( 'EvtNumber' , evt               )
      tup.column  ( 'nPVs'      , self.npv          )
      tup.fill_gen( self.tau_event_summary          )
      tup.fill_gen( self.gen_stripping_decreports() )
      #
      # tup.fill_funcdict( Functors.FD_MCKINEMATICS, mcp   )
      # tup.fill_funcdict( Functors.FD_MCPID       , mcp   )
      #
      tup.fill_funcdict ( self.FD_TOS             , reco  )
      tup.fill_funcdict ( Functors.FD_KINEMATIC   , reco  )
      tup.fill_funcdict ( Functors.FD_ISOLATION   , reco  )
      tup.column        ( 'BPVIP'     , BPVIP()    (reco) )
      tup.column        ( 'BPVIPCHI2' , BPVIPCHI2()(reco) )
      # tup.fill_gen( self.gen_isolation(reco) )
      #
      if tname.startswith('h3'):
        tup.fill_funcdict( Functors.FD_VERTEX       , reco  )
        tup.fill_funcdict( Functors.FD_MOM3CHILDREN , reco  )
        tup.column('BPVCORRM'      , BPVCORRM  (reco) )
        tup.column('BPVDLS'        , BPVDLS    (reco) )
        tup.column('BPVVD'         , BPVVD     (reco) )
        tup.column('BPVLTIME'      , BPVLTIME()(reco) )


#--------------------------------

class OldLooseTauCandTupleWriter(BaseTauCandTupleWriter):
  """
  More complex handler for mcMatch of tauh3.... Depreciated Soon.
  """

  Inputs = [
    'Phys/LooseTau_e',
    'Phys/LooseTau_mu',
    'Phys/LooseTau_h1',
    'Phys/LooseTau_h3',
  ]

  def treat_twoway_tausimple(self):
    for cname in ['LooseTau_e', 'LooseTau_mu', 'LooseTau_h1']:
      ttype = cname.split('_')[-1]
      table = self.get('Phys/'+cname+'/PVRefit_default_P2PV')
      for mcp, reco in self.twoway_mcMatch_tau( ttype, cname ):
        ## For e/mu, do nothing extra
        if ttype in ('mu','e'):
          self.write_tau( ttype, mcp, reco, table )
          return
        if ttype == 'h1':
          ## nobias, with prescale
          if (mcp is not None) or ( random()<0.01 ):
            self.write_tau( 'h1_nobias_prescale1Em2', mcp, reco, table )
          ## PT>4GeV cut
          if PT(reco) < 4000:
            reco = None
          if not (mcp is None and reco is None):
            self.write_tau( 'h1_PT4', mcp, reco, table )


  def treat_twoway_tauh3(self):
    """
    More sophisticate & performance friendly ( in expense for more complex code ).
    """

    def same_tauh3(p1,p2):
      """Custom check for equality, test by trio of children's indices."""
      return set(p.index() for p in p1.children()) == set(p.index() for p in p2.children())

    def pass_weak_drtriocuts(reco):
      return (reco is not None) and (Functors.DRTRIOMAX(reco)<0.4) and (Functors.DRTRIOMID(reco)<0.3) and (Functors.DRTRIOMIN(reco)<0.2)

    ## Prepare list of truereco, contain tuple of (MCP,RECO)
    list_true_reco_tauh3 = []
    for tau in self.list_tau:
      if tau.type.h3:
        truereco = self.find_reco_best_tauh3('StdAllNoPIDsPions', tau)
        if truereco is not None:
          list_true_reco_tauh3.append(( tau, truereco ))

    ## Write list of reco, fast-fail using exhaustion strategy.
    N_TRUE = len(list_true_reco_tauh3) # exhausting search for performance gain
    table  = self.get('Phys/LooseTau_h3/PVRefit_default_P2PV')
    for reco in self.selectTES('LooseTau_h3'):
      mcp = None
      if N_TRUE > 0:
        for i, (truemcp,truereco) in enumerate(list_true_reco_tauh3):
          if same_tauh3( reco, truereco ):
            N_TRUE -= 1
            mcp = truemcp
            log.debug('reco: %s' % str(reco).strip())
            log.debug('mcp : %s' % str(mcp ).strip())
            break # Stop the loop
        # Delete this entry, using (i) which is the last success elem
        if mcp is not None:
          list_true_reco_tauh3.pop(i)
      ## 1st, no bias: Do with prescale
      if (mcp is not None) or (random() < 0.01):
        self.write_tau( 'h3_nobias_prescale1Em2', mcp, reco, table )
      ## 2nd, with DRTRIOMAX cut
      if (mcp is not None) or pass_weak_drtriocuts(reco):
        self.write_tau( 'h3_DRTRIOCUTS', mcp, reco, table )

    ## Finally, two-way write the MCP component into all h3-ntuple
    for mcp,_ in list_true_reco_tauh3:
      log.debug('MCP not picked: %s'%str(mcp).strip())
      self.write_tau( 'h3_nobias_prescale1Em2' , mcp, None, table )
      self.write_tau( 'h3_DRTRIOCUTS'          , mcp, None, table )


  @pass_success
  def analyse(self):
    raise NotImplementedError('Broken by new PV scheme')
    self.treat_twoway_tausimple()
    self.treat_twoway_tauh3()

#---------------------------------------------------------

class LooseTauCandTupleWriter(BaseTauCandTupleWriter):
  """
  Can be large/slow! Make sure to use with some filter (like stripping) only!
  """
  # OutputLevel = 1
  Inputs = [
    # 'Phys/LooseTau_e',
    # 'Phys/LooseTau_mu',
    # 'Phys/LooseTau_h1',
    'Phys/LooseTau_h3',
  ]
  P2PVInputLocations = [ s+'/BestPVAlg_P2PV' for s in Inputs ]


class TightTauCandTupleWriter(BaseTauCandTupleWriter):
  Inputs = [
    'Phys/TightTau_e',
    'Phys/TightTau_mu',
    'Phys/TightTau_h1',
    'Phys/TightTau_h3',
  ]
  P2PVInputLocations = [ s+'/BestPVAlg_P2PV' for s in Inputs ]


#==============================================================================

class FilterFiducialAccentance(BaseTauAlgoMC):
  """
  Put this class on MC of Zg->TauTau to filter only event passing fiducial
  region and inside selected acceptance.

  NOTE: Use lep_PT>20 in this study.

  - Check nothing on the reconstructed particle.
  - Note: Don't kill HH channel, since in reality this is the contamination to
    be accounted for.
  """

  cut_geo_normal = (MCETA>2.0)  & (MCETA<4.5)
  # cut_geo_hcal   = (MCETA>2.25) & (MCETA<3.75)

  cut_fid_ditau  = (MCM>60E3) & (MCM<120E3)
  cut_fid_tau    = (MCPT>20E3) & cut_geo_normal

  def valid_fiducial(self):
    """
    Require the ditau inside standard LHCb Z-fiducial.
    """
    for ditau in self.list_ditau:
      if not self.cut_fid_ditau(ditau):
        return False
      for tau in ditau:
        if not self.cut_fid_tau(tau):
          return False
    return True

  def valid_acceptance_ll(self, ditau):
    m = ditau.vec4_children_charged.M()
    if (m<20E3) or (m>120E3):
      return False
    ## extra for mumu, ee, exclude Z peak down to 80
    if ditau.type in ('mu_mu', 'e_e'):
      if (m>80E3):
        return False
    ## Check on individual lepton
    ll    = [ c for tau in ditau for c in tau.children(CHARGED&LEPTON) ]
    l1,l2 = sorted(ll, key=MCPT, reverse=True)
    if MCPT(l1)<20E3:
      return False
    if MCPT(l2)<5E3:
      return False
    if not self.cut_geo_normal(l1):
      return False
    if not self.cut_geo_normal(l2):
      return False
    return True

  def valid_acceptance_lh1(self, ditau):
    m = ditau.vec4_children_charged.M()
    if (m<30E3) or (m>120E3):
      return False
    ## lepton from taul
    lep = [ c for tau in ditau for c in tau.children(CHARGED&LEPTON) ][0]
    if MCPT(lep)<20E3:
      return False
    if not self.cut_geo_normal(lep):
      return False
    ## hadron from tauh1
    had = [ c for tau in ditau for c in tau.children(CHARGED&HADRON) ][0]
    if MCPT(had)<10E3:
      return False
    if not self.cut_geo_normal(had):
      return False
    return True

  def valid_acceptance_lh3(self, ditau):
    ## ditau level
    m = ditau.vec4_children_charged.M()
    if (m<30E3) or (m>120E3):
      return False
    ## lepton
    lep = [ c for tau in ditau for c in tau.children(CHARGED&LEPTON) ][0]
    if MCPT(lep)<20E3:
      return False
    if not self.cut_geo_normal(lep):
      return False
    ## single prong
    prongs    = [ c for tau in ditau for c in tau.children(CHARGED&HADRON) ]
    p1,p2,p3  = sorted(prongs, key=MCPT, reverse=True)
    if MCPT(p3)<1E3:
      return False
    if MCPT(p1)<6E3:
      return False
    if not self.cut_geo_normal(p1):
      return False
    if not self.cut_geo_normal(p2):
      return False
    if not self.cut_geo_normal(p3):
      return False
    ## tauh3 (consider only charged component)
    tauh3 = [ tau for tau in ditau if tau.type.h3 ][0]
    vec4  = tauh3.vec4_children_charged
    if vec4.pt() < 12E3:
      return False
    if (vec4.M()<700) or (vec4.M()>1500):
      return False
    return True

  @count
  def valid_acceptance(self, ditau):
    """
    Provide a channel-dependent counter here to find out the alternate eacc
    that presents here.
    """
    dtype = ditau.type
    self.valid_acceptance.sharding = dtype
    if dtype in ('e_e', 'e_mu', 'mu_mu'):
      if not self.valid_acceptance_ll(ditau):
        return False
    if dtype in ('e_h1', 'h1_mu'):
      if not self.valid_acceptance_lh1(ditau):
        return False
    if dtype in ('e_h3', 'h3_mu'):
      if not self.valid_acceptance_lh3(ditau):
        return False
    return True


  def analyse(self):

    ## Check at fiducal level (independent of dtype)
    if not self.valid_fiducial():
      return SUCCESS

    ## Check acceptance cut (depends on which dtype)
    # Need at least one good ditau
    inacc = False
    try:
      for ditau in self.list_ditau:
        dtype = ditau.type
        inacc |= self.valid_acceptance(ditau)

    except ValueError, e:
      print self.event_break
      for tau in self.list_tau:
        print tau
        print tau.type
        print tau.decay()
      raise

    ## Need at least one good di-tau
    if not inacc:
      return SUCCESS

    ## Finally passing all
    self.setFilterPassed(True)
    return SUCCESS

#===============================================================================

def prefixes(dtype):
  """
  Replicate the auto-prefixing functionality seen in DTT

  >>> prefixes('e_h1')
  ['e', 'pi']
  >>> prefixes('h1_h3')
  ['pi', 'tau']
  >>> prefixes('mu_mu')
  ['mu1', 'mu2']
  >>> prefixes('mu_mu_ss_antiiso')
  ['mu1', 'mu2']
  """
  l = dtype.replace('h1', 'pi').replace('h3', 'tau').replace('_ss','').replace('_antiiso','').split('_')
  if len(set(l))==2:  # no mult problem.
    return l
  return l[0]+'1', l[1]+'2'


def custom_enumerate(dtype, mother):
  """
  Yield (prefix, particle) custom sort by type, then by PT
  (in case of same particle, like ditau_mu_mu).
  """
  pf1, pf2 = prefixes(dtype)
  c1 , c2  = mother.children()
  if pf1.endswith('1'):  # same kind
    c1,c2 = sorted([c1,c2], key=PT, reverse=True)  # Decrease in PT
  yield pf1, c1
  yield pf2, c2


def HMT_mass_binning(mcp):
  """
  Given the Higgs MCParticle, return the discreticized value of mass
  binning that it should be. This is an unfortunate ugly workaround.

  Basically, it retrieve the rest mass and return the nearest rounded mass.
  """
  BINS = xrange(25, 500, 10)
  mass = MCM(mcp)/1e3
  l = [(abs(mark-mass),mark) for mark in BINS]
  return sorted(l, key=lambda x:x[0])[0][-1]

class DitauCandTupleWriter(BaseTauAlgoMC):

  ## dummy one, see below
  inputs = [
    ## Tight selection
    'Phys/ditau_e_e',
    'Phys/ditau_e_h1',
    'Phys/ditau_e_h3',
    'Phys/ditau_e_mu',
    'Phys/ditau_h1_mu',
    'Phys/ditau_h3_mu',
    'Phys/ditau_mu_mu',
    #
    'Phys/ditau_h3s_mu',
    'Phys/ditau_e_h3s',
  ]

  ## Propagate with full of them
  Inputs = [ s+suffix for s in inputs for suffix in ('', '_ss' ) ]

  P2PVInputLocations = [ s+'/BestPVAlg_P2PV' for s in Inputs ] + [
    # For single-tau PV calculation
    'Phys/Tau_e/BestPVAlg_P2PV',
    'Phys/Tau_mu/BestPVAlg_P2PV',
    'Phys/Tau_h1/BestPVAlg_P2PV',
    'Phys/Tau_h3/BestPVAlg_P2PV',
    #
    'Phys/Tau_h3s/BestPVAlg_P2PV',
  ]

  FL_tracked  = [ 'PERR2', 'TRPCHI2', 'TRCHI2', 'TRCHI2DOF', 'TRGHP', 'PROBNNghost' ]
  FL_single   = [ 'ETA', 'P', 'PT', 'M', 'PHI', 'Y', 'Y0', 'Q' ]
  FL_prongs   = [ 'ETA', 'P', 'PT', 'PX', 'PY', 'PZ', 'E', 'PHI', 'Q']

  ## Configure before appMgr
  from Configurables import TupleToolConeIsolation
  conf = TupleToolConeIsolation('DitauCandTupleWriter.TupleToolConeIsolation')
  conf.MinConeSize = 0.5
  conf.MaxConeSize = 0.5
  conf.OutputLevel = 10

  from Configurables import TupleToolStripping
  conf = TupleToolStripping('DitauCandTupleWriter.TupleToolStripping')
  conf.StrippingList = sorted(LIST_STRIPPING)

  from DecayTreeTuple.Configuration import TupleToolMCTruth
  conf = TupleToolMCTruth('DitauCandTupleWriter.TupleToolMCTruth')
  conf.ToolList = [
    'MCTupleToolHierarchy',
    'MCTupleToolReconstructed',
  ]
  tt = conf.addTupleTool( 'LoKi::Hybrid::MCTupleTool', name='mtt_higgs')
  tt.Variables = {
    'MCID'  : 'MCID',
    'MCPT'  : 'MCPT',
    'MCP'   : 'MCP',
    'MCPZ'  : 'MCPZ',
    'MCQ'   : 'MC3Q/3',
    'MCETA' : 'MCETA',
  }


  @property
  def tt_cone_isolation(self):
    return self.tool('IParticleTupleTool', 'TupleToolConeIsolation')

  @property
  def tt_mctruth(self):
    return self.tool('IParticleTupleTool', 'TupleToolMCTruth')

  @property
  def tt_stripping(self):
    return self.tool('IEventTupleTool', 'TupleToolStripping')

  @property
  def tt_eventinfo(self):
    return self.tool('IEventTupleTool', 'TupleToolEventInfo')

  @count
  def filt_ditau( self, dtype, ditau ):
    """
    Filter di-tau candidate in DATA before writing to ntuple, reducing size.
    Only possible once selection cuts are stable.

    Return True if this is go-to-ntuple.

    Note:
    - Remove mass cut to allow full HMT mass scan.
  
    Update 170214: remove these cut for full compat (tag&probe, TMVA, etc.)
    - DPHI > 2.7
    - BPVCORRM < 3e3
    - VCHI2PDOF < 20

    """
    self.filt_ditau.sharding = dtype
    cut_tauh3 = (PT>12E3) & (M>700) & (M<1500)
    if dtype in ('mu_mu', 'e_e', 'e_mu'):
      pt1   = CHILDCUT( PT>15E3, 1 ) & CHILDCUT( PT>5E3, 2 )
      pt2   = CHILDCUT( PT>15E3, 2 ) & CHILDCUT( PT>5E3, 1 )
      func  = (pt1|pt2)
    elif dtype == 'h1_mu':
      func  = CHILDCUT(PT>10E3,1) & CHILDCUT(PT>15E3,2)
    elif dtype == 'e_h1':
      func  = CHILDCUT(PT>15E3,1) & CHILDCUT(PT>10E3,2)
    elif dtype == 'h3_mu':
      func  = CHILDCUT(cut_tauh3,1) & CHILDCUT(PT>15E3,2)
    elif dtype == 'e_h3':
      func  = CHILDCUT(PT>15E3,1) & CHILDCUT(cut_tauh3,2)
    return func(ditau)


  def write_ditau( self, tname, dtype, reco ):
    with self.enhancedTuple( tname ) as tup:
      tup.column_uint('nPVs'     , self.nPVs     )
      tup.column_uint('nTracks'  , self.nTracks  )
      tup.column_uint('nSPDhits' , self.nSPDhits )
      self.tt_eventinfo.fill( tup )
      self.tt_stripping.fill( tup )

      ## Always on: Truth of current ditau channel.
      tup.fill_gen( self.tau_event_summary )

      ## Fill ditau
      tup.fill_funclist( self.FL_single           , reco )
      tup.fill_funcdict( Functors.FD_MOM2CHILDREN , reco )

      ## Do this only in Z_tautau case
      match = True
      if self.event_type in (42100000, 42102013): # Z, Zg
        match = self.mcMatch_ditau( reco )   # old version of ditau, unlike mcMatch_tau, return boolean
      tup.column( 'MATCH' , match ) # Write True immediately in other event for practical cutting.

      ## Do this only in HMT case
      if self.event_type == 43901000:
        higgs = self.get('MC/Particles')[0]
        mbin  = HMT_mass_binning(higgs)
        tup.column('HMT_mass', mbin)
        tup.column('HMT_MCM' , MCM(higgs))
        tup.column('HMT_MCP' , MCP(higgs))
        tup.column('HMT_MCPT', MCPT(higgs))
        tup.column('HMT_MCID', MCID(higgs))

      ## Fill 2 children
      for prefix,child in custom_enumerate( dtype, reco ):
        tup.fill_funclist( self.FL_single          , child, prefix=prefix )
        tup.fill_funclist( self.FL_tracked         , child, prefix=prefix )
        tup.fill_funcdict( self.FD_TOS_HIGHPTLEPTON, child, prefix=prefix )
        tup.fill_funcdict( Functors.FD_PROTOPID    , child, prefix=prefix )
        tup.column( prefix+'_BPVIP'    , BPVIP()(child))
        tup.column( prefix+'_BPVIPCHI2', BPVIPCHI2()(child))
        self.tt_cone_isolation.fill( reco, child, prefix, tup )
        self.tt_mctruth.fill       ( reco, child, prefix, tup )
        #
        if 'tau' in prefix: ## TAUH3
          tup.column( prefix+'_BPVVD'   , BPVVD     (child))
          tup.column( prefix+'_BPVDLS'  , BPVDLS    (child))
          tup.column( prefix+'_BPVCORRM', BPVCORRM  (child))
          tup.column( prefix+'_BPVLTIME', BPVLTIME()(child))
          #
          tup.fill_funcdict( Functors.FD_VERTEX       , child, prefix=prefix )
          tup.fill_funcdict( Functors.FD_MOM3CHILDREN , child, prefix=prefix )
          #
          # More info on prong, highest-PT firsqst
          for i,prong in enumerate(sorted(child.children(), key=PT, reverse=True)):
            prefix = 'pr%i'%(i+1)
            tup.fill_funclist(self.FL_prongs      , prong, prefix=prefix )
            tup.fill_funclist(self.FL_tracked     , prong, prefix=prefix )
            tup.fill_funcdict(Functors.FD_PROTOPID, prong, prefix=prefix )
            self.tt_mctruth.fill( child, prong, prefix, tup )

  @count
  def nevt_by_type(self):
    """
    Attempt to count the number of events, groupped by MC event ID.
    To be cross-checked against the denominator used in the normalizer
    """
    return str(self.event_type)

  @pass_success
  def analyse(self):

    ## static
    recsummary    = self.get('Rec/Summary')
    self.nPVs     = recsummary.info(LHCb.RecSummary.nPVs     , -1)
    self.nTracks  = recsummary.info(LHCb.RecSummary.nTracks  , -1)
    self.nSPDhits = recsummary.info(LHCb.RecSummary.nSPDhits , -1)

    ## Count, ONCE PER EVT!
    self.nevt_by_type()

    ## Loop over all ditau channel
    for cpath in self.Inputs:
      if 'ditau' not in cpath:
        continue
      tname = cpath.split('/')[-1].replace('ditau_', '')
      dtype = tname.replace('_ss', '').replace('h3s', 'h3')

      ## Do this only in HMT case: Split tree into dir by HMT mass
      if self.event_type == 43901000:
        higgs = self.get('MC/Particles')[0]
        mbin  = HMT_mass_binning(higgs)
        tname = '%i/%s'%(mbin, tname)
      elif self.event_type: # this is null for data. For MC, add evtID
        tname = '%i/%s'%(self.event_type, tname)

      ## Start filling the ntuple
      for reco in self.selectTES( cpath ):
        if self.is_MC or self.filt_ditau( dtype, reco ):
          self.write_ditau( tname, dtype, reco )


#===============================================================================

class DebugSamesign(BaseTauAlgoMC):

  def report(self, reco):
    print 'reco: ', reco
    mcp = list(self.find_mcp_all(reco))
    print 'mcp:', mcp
    moms = [ x.mother() for x in mcp ]
    print 'moms:', moms
    siblings = [ mom.children() for mom in moms ]
    print 'siblings:', siblings
    grandmoms = [ x.mother() for x in moms if x ]
    print 'grandmoms:', grandmoms
    ggmoms = [ x.mother() for x in grandmoms if x ]
    print 'grandgrandmoms:', ggmoms

  def report_recur(self, reco):
    l = reco.children()
    if len(l)==0:
      self.report(reco)
    else:
      for x in l:
        self.report_recur(x)

  @pass_success
  def analyse(self):
    l = self.get('Phys/ditau_mu_mu_ss/Particles')
    if not l:
      return
    if len(l)==0:
      return
    print self.event_break
    print 'True di-tau'
    for x in self.list_ditau:
      print x
    for cand in l:
      print cand
      self.report_recur(cand)


#===============================================================================

class DecIdCounter(AlgoMC):

  @count
  def nevt_by_type(self):
    """
    Attempt to count the number of events, groupped by MC event ID.
    To be cross-checked against the denominator used in the normalizer
    """
    return str(self.event_type)

  @pass_success
  def analyse(self):
    ## Count, ONCE PER EVT!
    self.nevt_by_type()

#===============================================================================

def configure( inputdata, catalogs = [], castor = True ):
  print inputdata
  print catalogs

  ## Compat support to receive force-LFN from Ganga
  if isinstance(inputdata, basestring):
    inputdata = [inputdata]
  import re
  inputdata2 = []
  with open('lfns', 'w') as fout:
    for uri in inputdata:
      if 'lazy://' in uri:
        # uri = re.findall(r"lazy://(\S+)'", uri)[0] # DaVinci verbose syntax
        uri = re.findall(r"lazy://(\S+)", uri)[0] # compact
      inputdata2.append(uri)
      fout.write(uri+'\n')
  inputdata = inputdata2
  import subprocess
  args = 'lb-run', 'LHCbDirac', 'dirac-bookkeeping-genXMLCatalog', '--File', 'lfns'
  print subprocess.check_output(args)
  catalogs = ['xmlcatalog_file:pool_xml_catalog.xml']
  print 'After patching'
  print inputdata
  print catalogs

  ## Inject BenderAlgo into early monitoring
  from Configurables import GaudiSequencer
  moni_name = 'MyMoniSequence'  

  ## LoKi filtering
  from PhysConf.Filters import LoKi_Filters
  filt = LoKi_Filters(
    L0DU_Code   = "L0_CHANNEL('Electron')|L0_CHANNEL('Muon')",
    HLT2_Code   = "HLT_PASS('Hlt2SingleMuonHighPTDecision')|HLT_PASS('Hlt2SingleTFVHighPtElectronDecision')",
    STRIP_Code  = "HLT_PASS_RE('StrippingZ02TauTau_.*')|HLT_PASS_RE('StrippingWMu.*')|HLT_PASS_RE('StrippingWe.*')",  # Careful about SS lines too.
  )
  DaVinci().EventPreFilters += [GaudiSequencer(moni_name), filt.sequence('Prefilter')]

  ## Set data
  setData( inputdata, catalogs, castor )

  ## Input data is only used to auto-guess the datatype.
  # IS_MC = guess_data_ISMC(inputdata)
  IS_MC = True
  # IS_MC = False


  seq = simple_configure( IS_MC,
    # '$DIR13/identification/DV-bender/DV_tau_cand_maker.py',
  )

  # ## Only to MC OF ZG_TAUTAU, never to data & other MC
  # if IS_MC:
  #   seq.Members += [ FilterFiducialAccentance().name() ]

  seq.Members += [
    # EchoTypeAlgoMC().name(),
    # TauRecoStatsAlgoMC(ntuple=True).name(),
    # CompareIso().name(),
    # FilterFiducialAccentance().name(),
    #
    # LooseTauCandTupleWriter().name(),
    # TightTauCandTupleWriter().name(),
    # DitauCandTupleWriter().name(),
    #
    # DebugSamesign().name(),
    # CheckPurity().name(),
    # CheckFakeTauH3Tight().name()
    # CheckStdLooseDetachedTau3pi().name(),
    # CheckDecay().name(),
  ]

  ## Another entry point for BenderAlgo to Monitor
  seq0 = appMgr().algorithm('MyMoniSequence', createIf=False)
  seq0.Members += [DecIdCounter().name()]

  return SUCCESS

#-------------------------------------------------------------------------------

if __name__ == '__main__':

  ## H2AA: 8TeV
  # uri = glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Brunel/*/Brunel.dst')
  # uri = glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Stripping21/*/*.dst')

  ## H2AA 13TeV
  # uri = glob('/panfs/khurewat/MC/H2AA_H125_A30/13TeV_Nu1.6_25ns_100k/2361/*.dst')

  ## Z0 (S20)
  # uri = '/panfs/khurewat/MC/official_Ztautau_42100000/00033128_00000112_1.allstreams.dst'
  # uri = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00033128/0000/00033128_00000001_1.allstreams.dst'

  ## Z0 8TeV, S21 (J2811,J2812)
  # uri = glob('/panfs/khurewat/ganga_storage/2811/*/*.dst')

  ## 42311011: W_munujet=l17
  # uri = 'root://storage01.lcg.cscs.ch:1094/pnfs/lcg.cscs.ch/lhcb/lhcb/MC/2012/ALLSTREAMS.DST/00043022/0000/00043022_00000009_2.AllStreams.dst'

  ## 30000000: Minimumbias
  # uri = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00032487/0000/00032487_00000001_1.allstreams.dst'

  ## Sample 2012-8TeV S21
  # evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/EW.DST
  # uri = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/LHCb/Collision12/EW.DST/00041836/0010/00041836_00100287_1.ew.dst?svcClass=lhcbDst'

  ## sample_Data2012_S20
  # uri = '/share/panfs/data1/khurewat/sample_Data2012_S20/00020198_00000100_1.ew.dst'

  ## Sample 2012-8TeV S20
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00020456/0000/00020456_00000041_1.ew.dst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00020456/0000/00020456_00000075_1.ew.dst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00020456/0000/00020456_00000092_1.ew.dst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/EW.DST/00020456/0000/00020456_00000125_1.ew.dst',
  # ]

  ## Real sample, fulldst for S23 dev
  # uri = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/freezer/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00047243_1.full.dst'

  # tau 5090
  # uri = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/LDST/00042960/0000/00042960_00000001_1.ldst"

  ## 8TeV MCMB (Sim08c)
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00032487/0000/00032487_00000001_1.allstreams.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00032487/0000/00032487_00000002_1.allstreams.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00032487/0000/00032487_00000003_1.allstreams.dst',
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00032487/0000/00032487_00000005_1.allstreams.dst',
  # ]

  ## Custom Zgtautau
  # uri = [ 'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/4083/%i/000000.AllStreams.dst'%i for i in xrange(10) ] # 100

  ## Original Ztautau 42100000
  # uri = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00033128/0000/00033128_00000001_1.allstreams.dst'

  # # ## Heterogeneous sample
  # uri = [
  #   'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00033128/0000/00033128_00000001_1.allstreams.dst', # Ztautau0
  #   'root://storage01.lcg.cscs.ch:1094/pnfs/lcg.cscs.ch/lhcb/lhcb/MC/2012/ALLSTREAMS.DST/00043022/0000/00043022_00000009_2.AllStreams.dst',
  # ]

  # ## Lazy sample
  # uri = [
  #   # '/lhcb/MC/2012/ALLSTREAMS.DST/00038773/0000/00038773_00000006_2.AllStreams.dst',
  #   '/lhcb/MC/2012/ALLSTREAMS.DST/00046563/0000/00046563_00000042_2.AllStreams.dst', # no EOS
  # ]

  ## debugging: bbbar -> mux 
  uri = [
    'lazy:///lhcb/MC/2012/ALLSTREAMS.DST/00038781/0000/00038781_00000016_2.AllStreams.dst',
    'lazy:///lhcb/MC/2012/ALLSTREAMS.DST/00038781/0000/00038781_00000010_2.AllStreams.dst',
  ]  
  # uri = [ # bbbar -> EX
  #   'lazy:///lhcb/MC/2012/ALLSTREAMS.DST/00039636/0000/00039636_00000009_2.AllStreams.dst',
  #   'lazy:///lhcb/MC/2012/ALLSTREAMS.DST/00039636/0000/00039636_00000017_2.AllStreams.dst',
  # ]

  ## Finally
  configure(uri)
  run(1000)
