#!/usr/bin/env gaudirun.py

from Configurables import LoKi__Hybrid__PlotTool as PlotTool 
from Configurables import LoKi__Hybrid__TupleTool as TupleTool 
from Configurables import TupleToolDecay  # So the branch can be operated on
from DecayTreeTuple.Configuration import DecayTreeTuple  # Has extra pythonic method.

## Filter
from PhysConf.Filters import LoKi_Filters
filt = LoKi_Filters(
  HLT1_Code  = "HLT_PASS('Hlt1SingleMuonHighPTDecision')",
  HLT2_Code  = "HLT_PASS('Hlt2SingleMuonHighPTDecision')",
  STRIP_Code = "HLT_PASS_RE('StrippingZ02TauTau_MuXLine.*')",
)
from Configurables import DaVinci
DaVinci().EventPreFilters += [ filt.sequence('Filt') ]


## ATTACH 
from Configurables import GaudiSequencer
masterseq = GaudiSequencer('MasterSeq')
masterseq.ModeOR = True
masterseq.ShortCircuit = False
DaVinci().UserAlgorithms += [ masterseq ]


#===============================================================================
# PVReFitterAlg
#===============================================================================

from Configurables import PVReFitterAlg

refit_locations = [ 
  'EW/Phys/StdAllLooseMuons/Particles',
  'EW/Phys/StdAllNoPIDsPions/Particles',
  'EW/Phys/Z02TauTau_MuXLine/Particles',
  'EW/Phys/Z02TauTau_MuXSSLine/Particles',
]

## Output = Phys/TightTau_h1/PVR_default_P2PV
# LHCb::Relation1D<LHCb::Particle,LHCb::VertexBase>

## Phys/TightTau_%s/PVR_withpvo_P2PV
algo = PVReFitterAlg('PVR_withpvo')
algo.UseIPVOfflineTool      = True
algo.ParticleInputLocations = refit_locations
# algo.OutputLevel = 2
masterseq.Members += [ algo ]


## Need single relator for each species
from Configurables import PVRelatorAlg, BestPVAlg

## Output to Phys/TightTau_h1/BestPVAlg_P2PV
# for pvrname in [ 'PVR_default', 'PVR_withpvo' ]:
algo = BestPVAlg("BestPVAlg")
algo.P2PVRelationsInputLocations = [ s.replace('/Particles', '/PVR_withpvo_P2PV' ) for s in refit_locations ]
# algo.OutputLevel = 2
masterseq.Members += [ algo ]


#===============================================================================
# TUPLE
#===============================================================================

preambulo = """
from LoKiMC.decorators import *
from LoKiPhys.decorators import *
from LoKiPhysMC.decorators import *

def _ChildrenDPHI(i1,i2):
  from LoKiPhys.decorators import CHILD,PHI  
  from LoKiCore.math import cos,acos
  DPHI = abs(CHILD(PHI,i1)-CHILD(PHI,i2))
  DPHI = acos(cos(DPHI))  # mod pi       
  return DPHI

def _ChildrenAPT(i1,i2):
  from LoKiPhys.decorators import CHILD,PT
  pt1 = CHILD(PT,i1)
  pt2 = CHILD(PT,i2)
  return abs(pt1-pt2)/(pt1+pt2)

APT  = _ChildrenAPT(1,2)
DPHI = _ChildrenDPHI(1,2)
""".split('\n')

def fill(tup, *args, **kwargs):
  """
  Helper for Hybrid::tupleTool, it seems somewhat a singleton item...

  args  : List of string, used directly as functor's name 
  kwargs: Nickname & actual functor call.

  Usage:
  >> fill( tup, 'PT', pz='PZ' )
  """
  # tool = tup.addTupleTool('LoKi::Hybrid::TupleTool', tname)  # not working
  # tool = tup.addTupleTool(TupleTool(tname))  # not wkr
  tname = 'MyHTT'
  tool  = getattr(tup, tname, None)
  if tool is None:
    tool = tup.addTupleTool('LoKi::Hybrid::TupleTool', tname)
    tool.Preambulo = preambulo
  ## Support compat for simple list into dict.
  tool.Variables.update({s:s for s in args})
  tool.Variables.update(kwargs)
  return tup

#-------------------------------------------------------------------------------

fd_tau = {
  'BPVIP'       : 'BPVIP()',
  'BPVIPCHI2'   : 'BPVIPCHI2()',
  'CaloHcalE'   : 'PPFUN(PP_CaloHcalE)',
  'ISMUON'      : 'switch(ISMUON, 1, 0)',
  'ISMUONLOOSE' : 'switch(ISMUONLOOSE, 1, 0)',
  'TRPCHI2'     : 'TRPCHI2',
}

fd_ditau = { 
  'APT'       : 'APT',
  'BPVVD'     : 'BPVVD',
  'BPVVDCHI2' : 'BPVVDCHI2',
  'DOCA'      : 'PFUNA(ADOCAMIN(""))',
  'DOCACHI2'  : 'PFUNA(ADOCACHI2(""))',
  'DPHI'      : 'DPHI',
}

def fill_tau(branch):
  """
  Focus on filling branch of single-tau.
  """
  ## Simples
  fill( branch, **fd_tau )
  ## Iso
  tool = branch.addTupleTool( 'TupleToolConeIsolation' )
  tool.MinConeSize    = 0.5
  tool.MaxConeSize    = 0.5
  tool.FillNeutral    = False
  ## Trigger
  tool = branch.addTupleTool( 'TupleToolTISTOS' )
  tool.FillL0   = False
  tool.FillHlt1 = False
  tool.FillHlt2 = False
  tool.Verbose  = True
  tool.TriggerList = [
    'L0MuonDecision',
    'Hlt1SingleMuonHighPTDecision',
    'Hlt2SingleMuonHighPTDecision',
  ]
  ## MCTruth
  # tool = branch.addTupleTool( 'TupleToolMCTruth' )
  # tool.ToolList = [
  #   'MCTupleToolKinematic',
  # ]

def create( tname, inputs, decay ):
  tup = DecayTreeTuple( tname, TupleName=tname, Inputs=inputs, Decay=decay )
  tup.NTupleDir   = 'DitauCandTupleWriter'
  tup.P2PVInputLocations = [ s.replace('/Particles','/BestPVAlg_P2PV') for s in refit_locations ]
  tup.IgnoreP2PVFromInputLocations = True
  tup.ToolList = [
    'TupleToolEventInfo',
  ]
  #
  tool = tup.addTupleTool( 'LoKi::Hybrid::EvtTupleTool' )
  tool.VOID_Variables = { 
    'nPVs'    : 'RECSUMMARY(0 , -1)',
    'nSPDHits': 'RECSUMMARY(70, -1)',
  } 
  #
  ## Stripping (for cross-check)
  tool = tup.addTupleTool( 'TupleToolStripping' )
  tool.StrippingList = [
    'StrippingZ02TauTau_MuXLineDecision',
    'StrippingZ02TauTau_MuXLineSSDecision',
  ]
  #
  branches  = tup.addBranches({ 
    'Z0' :'^Z0', 
    'mu1':'Z0 -> ^l Hadron', 
    'mu2':'Z0 -> l ^Hadron',
  })
  for bname, branch in branches.iteritems():
    ## In all branches
    fill( branch, 'M', 'P', 'PZ', 'PT', 'ETA', 'PHI' )
    ## To single-tau level
    if bname in ('mu1', 'mu2'):
      fill_tau( branch )
    ## Ditau level
    if bname == 'Z0':
      fill( branch, **fd_ditau )
  return tup

#----------------------------

queue = [
  ( 'mu_mu'     , [ 'EW/Phys/Z02TauTau_MuXLine'   ], '[ Z0 -> ^mu- ^pi+ ]CC' ),
  ( 'mu_mu_ss'  , [ 'EW/Phys/Z02TauTau_MuXLineSS' ], '[ Z0 -> ^mu- ^pi- ]CC' ),
]

## Finally
for args in queue:
  masterseq.Members += [ create(*args) ]

#===============================================================================


## Configure DaVinci
DaVinci().HistogramFile = 'hist.root'
DaVinci().TupleFile     = 'tuple.root'
DaVinci().DataType      = "2012"

## For REAL data
DaVinci().Simulation  = False
DaVinci().Lumi        = True

