#!/usr/bin/env python

from MyBender import *
from MyBender.Tau import *

from collections import defaultdict
from random import random

# log.regex_filter = [ r'TestAlgoMC' ]

### IMPORTANT REMARK ABOUT SUMCONE:
# 1. Input need DR radius SQUARED!
# 2. Uniqueness is check RECURSIVELY on all descendant of cone's core.


## Important container to use in following Algo, main for coning
LIST_BASIC_CHARGE  = [
  'Phys/StdAllLoosePions',
  'Phys/StdAllLooseElectrons',
  'Phys/StdAllLooseMuons',
  'Phys/StdAllLooseKaons',
  'Phys/StdAllLooseProtons',
]

LIST_BASIC_NEUTRAL = [
  'Phys/StdLooseAllPhotons',
  'Phys/StdLooseMergedPi0',
]

Vec = std.vector('string')
VEC_BASIC_CHARGE  = Vec() + LIST_BASIC_CHARGE
VEC_BASIC_NEUTRAL = Vec() + LIST_BASIC_NEUTRAL

SUMCONE05_E_PFCHARGED   = SUMCONE(0.5**2, E , 'Phys/PFParticles', Q!=0)
SUMCONE02_E_PFNEUTRAL   = SUMCONE(0.2**2, E , 'Phys/PFParticles', Q==0)
SUMCONE05_E_STDCHARGED  = SUMCONE(0.5**2, E , VEC_BASIC_CHARGE  )
SUMCONE02_E_STDNEUTRAL  = SUMCONE(0.2**2, E , VEC_BASIC_NEUTRAL )

SUMCONE04_N_PFCHARGED   = SUMCONE(0.4**2, ONE, 'Phys/PFParticles', Q!=0)

#==============================================================

class SelTauCand(BaseTauAlgoMC):

  def __init__(self):
    inputs = [ 'Phys/PFParticles' ]
    super(SelTauCand, self).__init__(Inputs=inputs)

  #--------#
  # ATOMIC #
  #--------#

  DICT_FUNC = {
    ## Basics 
    'E'   : E,
    'P'   : P,
    'PT'  : PT,
    'ETA' : ETA,
    'MM'  : MM,
    'PRER': (PERR2**0.5)/P,

    # Track
    'TRCHI2'      : TRCHI2,
    'TRPCHI2'     : TRPCHI2,
    'TRCHI2DOF'   : TRCHI2DOF,
    'TrNVELO'     : TRFUN(TrIDC('isVelo')),
    'TrNVELOMISS' : TRFUN(TrNVELOMISS),
    'TrTNORMIDC'  : TRFUN(TrTNORMIDC),  # Hits..

    # BPV Series
    'BPVIP'     : BPVIP(),          # Impact parameter
    'BPVIPCHI2' : BPVIPCHI2(),      # Impact parameter CHI2
    'BPVCORRM'  : BPVCORRM,

    # Calo
    'CALO_PRS_E'  : PPINFO(LHCb.ProtoParticle.CaloPrsE,  0),
    'CALO_ECAL_E' : PPINFO(LHCb.ProtoParticle.CaloEcalE, 0),
    'CALO_HCAL_E' : PPINFO(LHCb.ProtoParticle.CaloHcalE, 0), 
  }

  #------#
  # COMB #
  #------#

  DICT_COMB = {
    'VCHI2'     : VCHI2,
    'VPCHI2'    : VPCHI2,
    'VCHI2PDOF' : VCHI2PDOF,
    #
    'SUME'  : SUMTREE(E , ALL, 0),
    'SUMPT' : SUMTREE(PT, ALL, 0),
  }

  #---------------#
  # CONE EXPLORER #
  #---------------#

  # rads = {
  #   '05' : 0.5,
  #   # '04' : 0.4,
  #   # '03' : 0.3,
  #   # '02' : 0.2,  # For inner-cone
  # }
  # vals = {
  #   # 'N' : ONE,
  #   'E' : E,
  #   'PT': PT,
  # }
  # containers = {
  #   # 'NPID': 'Phys/StdAllNoPIDsPions',
  #   'PF'  : 'Phys/PFParticles',
  # }
  # cuts = {
  #   ''          : ALL,
  #   # '_LONG'     : ISLONG,
  #   '_CHARGED'  : Q!=0,
  #   # '_TRP'      : TRPCHI2>0.01,
  #   '_HASPROTO' : HASPROTO,
  #   '_NEUTRAL'  : Q==0,  # For inner-cone of pi0+gamma
  # }

  # ## REMARK1: Radius in DR SQUARED
  # ## REMARK2: Duplicate removal of core is in place...
  # import itertools
  # gen = itertools.product(rads.iteritems(),  vals.iteritems(), containers.iteritems(), cuts.iteritems())
  # DICT_CONES = { 'CONE{}_{}_{}{}'.format(srad,sval,scon,scut):SUMCONE( rad**2, val, con, cut )
  #   for (srad,rad),(sval,val),(scon,con),(scut,cut) in sorted(gen) }


  @static_algo_property
  def p2mc(self):
    return self.tool('IP2MCP', 'MCMatchObjP2MCRelator')

  
  #---------------------------------------------------------

  def analyse_tau_h1_intraprong_children(self):
    """Focus on tau_h1, let's see what are the later evolution of energetic single-pi."""
    for tau in self.list_tau:
      if tau.type == 'h1':
        log.info('Tau:\n %s'%tau)
        pi = tau.children(MC3Q!=0)[0]
        log.info('mcp     :\n %s'%pi)
        log.info('reco    :%s'%self.find_recon(pi))
        log.info('mc children  :%s'%pi.children())
        # log.info('mc descendant:%s'%pi.descendants())


  #------------------------------------------------------------

  def analyse_tau_h1_fake_sing_conereco(self):
    """Focus on the case where FAKE candidates have i/o ratio~=1."""
    for cand in self.list_tau_h1_PF_reco_candidates:
      if not self.match_MC_tau_h1_PF(cand) and PT(cand)>1000:
        cone_stats = dict(self.calculate_cone_suite_lite(cand))
        if cone_stats['EFrac_ProngsOnly_PFCHARGED']>0.99: # Broken one
          print self.event_break
          for tau in self.list_tau:
            print tau
          self.explore_inside_cone(cand)          
          self.report_fake_tau_h1_core_mcp(cand)
          self.report_fake_tau_h1_core_mother(cand)

  @count 
  def report_fake_tau_h1_core_mcp(self, cand):
    """
    Report, in case of tau_h1 broken-cone, the true identity of core.
    Hypothesis: Majority of broken-cone is due to the faking. e.g.,
                hard-isolated-muon faking as hard-iso-pion.
                This is dependent on the container (StdAllLoosePions).
    """
    mcp = self.p2mc(cand)
    return str(int(MCABSID(mcp)))

  @count
  def report_fake_tau_h1_core_mother(self, cand):
    """
    Report, in case of tau_h1 broken-cone, the mother of true identity of core.
    """
    mcp = self.p2mc(cand)
    if mcp:
      return str(int(MCABSID(mcp.mother())))


  #------------------------------------------------------------

  def analyse_tau_h1_poor_isolation(self):
    """Focus on tau_h1 case when isolation is poor, leading to poor selection."""

    for tau in self.list_tau:
      if tau.type.h1:
        mcp   = tau.children(CHARGED)[0]
        reco  = self.find_reco_best('PFParticles', mcp)
        efrac = (E/(E+SUMCONE05_E_PFCHARGED))(reco)
        cut   = (PT>5000) & (TRPCHI2>0.01)
        if cut(reco) and efrac < 0.8:
          print self.event_break
          print tau 
          print reco
          print efrac
          print self.explore_inside_cone(reco)

  #------------------------------------------------------------

  def explore_inside_cone(self, reco):
    """Helper method to printout neighbors of given particle, optimize for verbose debugging."""
    # print self.event_break

    mcp = self.p2mc(reco)
    fDR = DR2(reco)**0.5
    log.info('reco        : %s'%str(reco).strip() + str(ETA(reco)))
    log.info('mcp         : %s'%str(mcp).strip())

    ## Compare against PF
    cont = [ p for p in self.selectTES('PFParticles') if Q(p)!=0 ]
    for neig in sorted(cont, key=E, reverse=True):
      dr = fDR(neig)
      if PT(neig)>1e-3 and ( 0 < dr < 0.5):  # inside
        log.info('neighbor: DR={:.3f} {}'.format(dr, str(neig).strip().split('\n')[0]))  # One-liner
        log.info('proto   : %s'%str(neig.proto()).strip())
        mc_neig = self.p2mc(neig)
        if mc_neig:
          log.info('neighbor mcp :%s'%mc_neig)
          set_anc = set()
          for anc in mc_neig.ancestors():
            anc = Functions.iBotCopyId(anc)
            # if anc.index() not in set_anc:
            # set_anc.add(anc.index())
            log.info('ancestors    :%s'%anc)



  def analyse_tau_h1_conereco(self):
    for ditau in self.list_ditau:
      for tau in ditau:
        if tau.type == 'h1':
          mcp  = tau.children(MC3Q!=0)[0]
          l    = self.find_recon(mcp)  ## Start from those recon I really have, not isReconstructed.
          if len(l)>0:
            reco = l[0]

            ## Pick the broken cone
            esum2   = SUMCONE(0.2**2, E, 'Phys/PFParticles', Q==0 )
            esum5   = SUMCONE(0.5**2, E, 'Phys/PFParticles', Q!=0 )
            # efrac = (E/(E+esum5))(reco)

            # Think carefully about double-counting & charges
            einner  = (esum2+E)(reco)
            eouter  = (esum2+E+esum5)(reco)

            efrac   = einner/eouter

            if efrac < 0.3:
              log.info('='*25 + ' Checking core (Efrac=%.3f)'%efrac + '='*25)
              log.info('inner/outer: %.3f/%.3f'%(einner,eouter))
              log.info('Ditau      :%s'%ditau)
              self.explore_inside_cone(reco)

  #----------------------------------------------
  # EXPLORE: H3
  #----------------------------------------------

  def calculate_cone_suite_lite(self, recotau):
    ## Calculate common quantities first
    ## Inners
    ei_prong  = E(recotau)
    ei_n02pf  = SUMCONE02_E_PFNEUTRAL(recotau)
    ei_n02std = SUMCONE02_E_STDNEUTRAL(recotau)
    ## Outers
    eo_c05pf  = SUMCONE05_E_PFCHARGED(recotau)
    eo_c05std = SUMCONE05_E_STDCHARGED(recotau)
    yield 'EFrac_ProngsOnly_PFCHARGED'  , (ei_prong             ) / (ei_prong             + eo_c05pf  )
    yield 'EFrac_Neutral02_PFCHARGED'   , (ei_prong + ei_n02pf  ) / (ei_prong + ei_n02pf  + eo_c05pf  )
    yield 'EFrac_ProngsOnly_STDCHARGED' , (ei_prong             ) / (ei_prong             + eo_c05std )
    yield 'EFrac_Neutral02_STDCHARGED'  , (ei_prong + ei_n02std ) / (ei_prong + ei_n02std + eo_c05std )
    yield 'ConeN04_PFCHARGED'           , SUMCONE04_N_PFCHARGED(recotau)

  @profile
  def calculate_cone_efrac_suite_old(self, comb):
    """
    Given single comb, return the suite of selected EFrac approaches.
    - Try to loop over PF only once for each core
    - Useful for exploring different cone technique (but not-so-memory-friendly).
    """

    children   = sorted([ comb(1), comb(2), comb(3) ], key=PT, reverse=True) # Hardest first
    dr_inner   = max(Functions.DR_trio(*children))
    hard_prong = children[0]
    func_dr    = lambda p: DR2(hard_prong)(p)**0.5
    dict_icut  = {
      'ProngsOnly'                 : lambda p,dr: False,
      'ProngsWithTightCone'        : lambda p,dr: (dr<dr_inner),
      'ProngsWithTightNeutralCone' : lambda p,dr: (dr<dr_inner) and Q(p)==0,
      'ProngsWithCone02'           : lambda p,dr: (dr<0.2),
      'ProngsWithNeutralCone02'    : lambda p,dr: (dr<0.2) and Q(p)==0,
    }
    dict_ocut  = {
      'PFALL'     : lambda p,dr: (dr<0.5),
      'PFCHARGED' : lambda p,dr: (dr<0.5) and (Q(p)!=0),
    }

    # for (keyi,cuti),(keyo,cuto) in itertools.product(dict_icut.iteritems(), dict_ocut.iteritems()):

    dict_rawres = { (ki,ko):[0.,0.] for ki in dict_icut for ko in dict_ocut }

    ## 1-pass: Loop once over PF
    # for p in sorted(self.get('Phys/PFParticles/Particles'), key=func_dr): # No need to sort!
    # for p in self.get('Phys/PFParticles/Particles'):
    # for p in self.PFParticles:
    for p in self.selectTES('PFParticles'):
      e  = E(p)
      dr = func_dr(p)
      ## INNER
      for keyi,cuti in dict_icut.iteritems():
        pass_inner = (p in children) or cuti(p,dr)
        for keyo, cuto in dict_ocut.iteritems():
          key        = (keyi, keyo)
          pass_outer = cuto(p, dr)
          if pass_inner:
            dict_rawres[key][0] += e 
          elif pass_outer:  # but not in inner
            dict_rawres[key][1] += e

    ## 2-pass: Create the EFrac ratio from collected result
    # for (keyi, vali), (keyo, valo) in itertools.product(dict_inner.iteritems(), dict_outer.iteritems()):
    result = {}
    for (keyi,keyo),(vali,valo) in sorted(dict_rawres.iteritems()):
      key = 'EFrac_' + keyi + '__' + keyo
      val = vali / (vali+valo)
      result[key] = val 
    return result


  def analyse_tau_h3_prelim(self):
    """Exploring with h3 & CommonParticles container."""

    with self.enhancedTuple('tau_h3_cand', auto_write=False) as tup:
      for comb in self.list_tau_h3_reco_candidates:

        ## Pre-Calc
        rawtau    = comb(0)
        children  = sorted([ comb(1), comb(2), comb(3) ], key=PT, reverse=True) # Hardest first
        is_signal = self.match_MC_tau_h3(comb)
        sharding  = 'signal' if is_signal else 'fake'

            # ## EFrac
            # # Think carefully about double-counting & charges
            # # esum2neu  = SUMCONE(0.2**2, E, 'Phys/PFParticles', HASPROTO )
            # esum5     = SUMCONE( 0.5**2, E, 'Phys/PFParticles', HASPROTO )
            # einner    = sum(Es)
            # eouter    = (E + esum5)(hard_prong)
            # efrac     = einner / eouter

            # # if efrac < 0.3:
            # #   log.info(self.event_break)
            # #   log.info(tau)
            # #   log.info(threepi)
            # #   log.info('EFrac: %.3f'%efrac)
            # #   self.explore_inside_cone(hard_prong)

            # ## Debug broken cone
            # if max_dr_efrac70 < 0.1 and PT(rawtau)>10000:
            #   log.info(self.event_break)
            #   log.info(tau)
            #   log.info(rawtau)
            #   self.explore_inside_cone(hard_prong)

        self.pass_preselection_tau_h3.sharding = sharding
        if self.pass_preselection_tau_h3(comb, is_signal):


              # ## Prescale the fake cand a bit
              # weight = 1. if is_signal else 0.5
              # if not is_signal and random() > weight:
              #   continue  # next iteration

          ## Advance calc
          drs             = Functions.DR_trio(*children)
          PTs             = [ PT(x) for x in children ] # Already sort by pt
          Es              = [ E(x)  for x in children ] # Also sort by PT (not by E!)
          hard_prong      = children[0]       # hardest prong
          efrac_suite     = self.calculate_cone_efrac_suite(comb)
          max_dr_efrac70  = self.calculate_invert_cone_efrac70(comb)

          ## Write!
          tup.column( 'MATCH'     , is_signal )
          tup.column( 'DR_min'    , drs[0] )
          tup.column( 'DR_mid'    , drs[1] )
          tup.column( 'DR_max'    , drs[2] )
          tup.column( 'DR_mean'   , sum(drs)/3.)
          tup.column( 'pi_PT1'    , PTs[2])
          tup.column( 'pi_PT2'    , PTs[1])
          tup.column( 'pi_PT3'    , PTs[0])
          tup.column( 'pi_PT_mean', sum(PTs)/3.)
          tup.column( 'pi_E1'     , Es[2]) # E of hardest-pi
          tup.column( 'pi_E2'     , Es[1])
          tup.column( 'pi_E3'     , Es[0])
          tup.column( 'pi_E_mean' , sum(Es)/3.)
          tup.column( 'EFrac70_DR', max_dr_efrac70)  # Slow
          # 
          tup.fill_dict( self.DICT_FUNC, rawtau     , prefix='recotau_' )
          tup.fill_dict( self.DICT_COMB, rawtau     , prefix='recotau_' )
          tup.fill_dict( self.DICT_FUNC, hard_prong , prefix='hardestPi_' )
          #
          tup.fill_dict( efrac_suite )
          #
          tup.EventInfo.fill()
          # 
          tup.write()

  #----------------------------------------------------------------------------


  #-----------------------------#
  # MAKING CANDIDATES: ELECTRON #
  #-----------------------------#

  @property  
  def list_tau_e_reco_candidates(self):
    return self.selectTES('StdAllLooseElectrons')
    # return [ p for p in self.selectTES('PFParticles') if ABSID(p)==11 ]

  @lazy_algo_property
  def _list_tau_e_true_cand(self):
    result = []
    for tau in self.list_tau:
      if tau.type=='e':
        mcp = tau.children(CHARGED)[0]
        result.append(self.find_reco_best('StdAllLooseElectrons', mcp))
        # result.append(self.find_reco_best('PFParticles', mcp))
    return result

  def match_MC_tau_e(self, cand):
    return cand in self._list_tau_e_true_cand

  @count
  def pass_preselection_tau_e(self, cand, is_signal):
    ## Prep counter sharding
    # shard = 'signal' if is_signal else 'fake'
    # self.pass_preselection_tau_e.sharding = shard 
    ## Pass-through
    return True


  #-----------------------#
  # MAKING CANDIDATES: H1 #
  #-----------------------#

  @property
  def list_tau_h1_PF_reco_candidates(self):
    return [ p for p in self.selectTES('PFParticles') if ABSID(p)==211 ] # in (211,321) ]

  @property
  def list_tau_h1_STD_reco_candidates(self):
    return self.selectTES('StdAllLoosePions')

  @lazy_algo_method
  def _list_tau_h1_true_cand_RAW(self, tes):
    result = []
    for tau in self.list_tau:
      if tau.type=='h1':
        mcp = tau.children(CHARGED)[0]
        result.append(self.find_reco_best(tes, mcp))
    return result

  def match_MC_tau_h1_PF(self, cand):
    return cand in self._list_tau_h1_true_cand_RAW('PFParticles')

  def match_MC_tau_h1_STD(self, cand):
    return cand in self._list_tau_h1_true_cand_RAW('StdAllLoosePions')

  @count
  def pass_preselection_tau_h1_PF(self, cand, is_signal):
    tname = 'preselect_tau_h1_PF'
    # shard = 'signal' if is_signal else 'fake'
    # self.pass_preselection_tau_h1_PF.sharding = shard 
    return self.pass_preselection_tau_h1_RAW(cand, is_signal, tname)

  @count
  def pass_preselection_tau_h1_STD(self, cand, is_signal):
    tname = 'preselect_tau_h1_STD'
    # shard = 'signal' if is_signal else 'fake'
    # self.pass_preselection_tau_h1_STD.sharding = shard 
    return self.pass_preselection_tau_h1_RAW(cand, is_signal, tname)


  def pass_preselection_tau_h1_RAW(self, cand, is_signal, tname):
    # ## Prep counter sharding
    # shard = 'signal' if is_signal else 'fake'
    # self.pass_preselection_tau_h1.sharding = shard 

    ## Note down the cut used in preselection, for FYI
    # prescale the non-signal
    with self.enhancedTuple(tname) as tup:
      tup.column( 'MATCH', is_signal)
      tup.column( 'PT'   , PT(cand))
    # soft PT cut
    if PT(cand)<1000:
      return False
    ## Finally
    return True



  #-----------------------#
  # MAKING CANDIDATES: H3 #
  #-----------------------#

  @property 
  def list_tau_h3_reco_candidates(self):
    # self.selectTES('prong', 'Phys/PFParticles/Particles', ((ABSID==211)|(ABSID==321)) & (PT>500))
    # self.select('prong', INTES('PFParticles',False) & ((ABSID==211)|(ABSID==321)) & (PT>500))
    self.select('prong', INTES('PFParticles',False) & (PT>1000))
    for comb in self.loop('prong prong prong', 'tau-'):
      rawtau = comb(0)
      if rawtau:  # Because some comb fail, but valid(), validPointer() method not working as expected
        yield rawtau 


  @lazy_algo_property
  def _list_tau_h3_true_threepi(self):
    # Helper method to return list of [pi,pi,pi]. 
    # Need all three pi MCMATCHed for now.
    result = []
    for tau in self.list_tau:
      if tau.type=='h3':
        threepi = [ self.find_reco_best('PFParticles', mcp) for mcp in tau.children(CHARGED) ]
        if len(threepi)==3 and (None not in threepi):
          result.append(threepi)
    return result

  def match_MC_tau_h3(self, tau):
    def same(l1, l2):
      # Return true if particles inside l1 and l2 are the same
      for p1 in l1:
        f = SAME(p1)
        if not any(f(p2) for p2 in l2):
          return False 
      return True
    return any(same(tau.children(),threepi) for threepi in self._list_tau_h3_true_threepi)

  @count
  @profile
  def pass_preselection_tau_h3(self, tau, is_signal):
    """
    Perform the pre-selection before export. 
    - Write down the efficiencies at this level, sharded by is_signal
    - Do as less cut as possible, but select most powerful possible.
    """

    # ## First, sharding
    # shard = 'signal' if is_signal else 'fake'
    # self.pass_preselection_tau_h3.sharding = shard

    ## Key objects
    drs = Functions.DR_trio(*tau.children())

    ## Note down the cut used in preselection, for FYI
    # prescale the non-signal
    if is_signal or random()<0.01:
      with self.enhancedTuple('preselect_tau_h3') as tup:
        tup.column( 'MATCH'    , is_signal)
        tup.column( 'MM'       , MM(tau))
        tup.column( 'VCHI2PDOF', VCHI2PDOF(tau))
        tup.column( 'DR_min'   , drs[0] )
        tup.column( 'DR_mid'   , drs[1] )
        tup.column( 'DR_max'   , drs[2] )

    ## Correct charge sum (+1, -1)
    if abs(SUMQ(tau))!=1:
      return False
    # Invar mass about tau's
    if not (0 < MM(tau) < 1800):
      return False
    ## Vertex
    if VCHI2PDOF(tau) > 20:
      return False
    ## 3pi are in narrow cone
    if drs[0]>0.2 or drs[2]>0.3:
      return False
    return True

  def fill_candidates_tau_h3(self, tup, recotau):
    """Additional columns for tau_h3"""
    children = recotau.children()
    drs      = Functions.DR_trio(*children)
    PTs      = sorted([ PT(x) for x in children ])
    #
    tup.fill_dict( self.DICT_COMB , recotau, prefix='recotau_' )
    tup.column( 'DR_min'      , drs[0] )
    tup.column( 'DR_mid'      , drs[1] )
    tup.column( 'DR_max'      , drs[2] )
    tup.column( 'DR_mean'     , sum(drs)/3.)
    tup.column( 'pi_PT_min'   , PTs[0] )
    tup.column( 'pi_PT_mid'   , PTs[1] )
    tup.column( 'pi_PT_max'   , PTs[2] )


  #-----------------------#
  # MAKING CANDIDATES: MU #
  #-----------------------#

  @property  
  def list_tau_mu_reco_candidates(self):
    return self.selectTES('StdAllLooseMuons')
    # return [ p for p in self.selectTES('PFParticles') if ABSID(p)==13 ]

  @lazy_algo_property
  def _list_tau_mu_true_cand(self):
    result = []
    for tau in self.list_tau:
      if tau.type=='mu':
        mcpmu = tau.children(CHARGED)[0]
        result.append(self.find_reco_best('StdAllLooseMuons', mcpmu))
    return result

  def match_MC_tau_mu(self, cand):
    return cand in self._list_tau_mu_true_cand

  @count
  def pass_preselection_tau_mu(self, cand, is_signal):
    ## Prep counter sharding
    # shard = 'signal' if is_signal else 'fake'
    # self.pass_preselection_tau_mu.sharding = shard 
    ## Pass-through
    return True

  #------------------------------------------------------------

  def write_candidates_tau_any(self, ttype, recotau, is_signal):
    tname = 'tau_%s_cand'%ttype
    with self.enhancedTuple(tname) as tup:
      tup.column     ( 'MATCH'        , is_signal )
      tup.fill_dict  ( self.DICT_FUNC , recotau, prefix='recotau_' )
      tup.fill_gen   ( self.tau_event_summary )
      tup.fill_gen   ( self.calculate_cone_suite_lite(recotau))
      tup.EventInfo.fill()

      ## Extra pass:
      if ttype=='h3':
        self.fill_candidates_tau_h3(tup, recotau)

  #------------------------------------------------------------

  # def analyse_candidate_maker(self):
  #   """Summarize analyse method for candidate maker of all tau's mode"""

  #   maker_suite = {
  #     'e':(
  #       self.list_tau_e_reco_candidates,
  #       self.match_MC_tau_e,
  #       self.pass_preselection_tau_e,
  #     ),
  #     'h1PF':(
  #       self.list_tau_h1_PF_reco_candidates,
  #       self.match_MC_tau_h1_PF,
  #       self.pass_preselection_tau_h1_PF,
  #     ),
  #     'h1STD':(
  #       self.list_tau_h1_STD_reco_candidates,
  #       self.match_MC_tau_h1_STD,
  #       self.pass_preselection_tau_h1_STD,
  #     ),
  #     'h3':(
  #       self.list_tau_h3_reco_candidates,
  #       self.match_MC_tau_h3,
  #       self.pass_preselection_tau_h3,
  #     ),
  #     'mu':(
  #       self.list_tau_mu_reco_candidates,
  #       self.match_MC_tau_mu,
  #       self.pass_preselection_tau_mu,
  #     ),
  #   }

  #   for ttype, (lister,matcher,preselector) in maker_suite.iteritems():
  #     for cand in lister:
  #       is_signal = matcher(cand)  <<< NOW BROKEN
  #       preselector.sharding = 'signal' if is_signal else 'fake'
  #       if preselector(cand, is_signal):
  #         self.write_candidates_tau_any(ttype, cand, is_signal)

  def analyse(self):

    # print self.event_break

    self.analyse_tau_h3_tightcut_fake()
    # self.analyse_tau_h1_poor_isolation()
    # self.analyse_tau_h1_fake_sing_conereco()
    # self.analyse_tau_h3_prelim()
    # self.analyse_tau_h1_conereco()
    # self.analyse_conemc()
    # self.analyse_efrac_conemc()
    # self.analyse_pi_pi0_separation()
    # self.analyse_tau_h1_conereco()

    # self.analyse_candidate_maker()

    self.setFilterPassed(True)
    return SUCCESS

