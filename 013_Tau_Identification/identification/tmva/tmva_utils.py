#!/usr/bin/env python

"""

Misc utilities for TMVA 

"""

## Augment the path
import sys, os
sys.path.append(os.path.expandvars('$DIR13'))  # ditau_utils
sys.path.append(os.path.expandvars('$DIR13/identification/plot')) # ditau_prep

## Imports
from PyrootCK import import_tree
from ditau_utils import PREFIXES, LABELS, get_current_dtype_dt


#===============================================================================

def load_vars(dt):
  """
  Return semi-static list of all vars, approproate expression & title
  can be reduced to smaller set
  """
  
  p1,p2 = PREFIXES[dt]
  l1,l2 = LABELS[dt]

  return {
    'P1'  : ['TMath::Log10(%s_P)'%p1    , 'p(#tau_{%s})'%l1     ],
    'P2'  : ['TMath::Log10(%s_P)'%p2    , 'p(#tau_{%s})'%l2     ],
    'PT1' : ['TMath::Log10(%s_PT)'%p1   , 'p_{T}(#tau_{%s})'%l1 ],
    'PT2' : ['TMath::Log10(%s_PT)'%p2   , 'p_{T}(#tau_{%s})'%l2 ],
    'IP1' : ['TMath::Log10(%s_BPVIP)'%p1, 'IP(#tau_{%s})'%l1    ],
    'IP2' : ['TMath::Log10(%s_BPVIP)'%p2, 'IP(#tau_{%s})'%l2    ],
    'ISO1': ['%s_0.50_cc_IT'%p1         , 'Iso(#tau_{%s})'%l1   ],
    'ISO2': ['%s_0.50_cc_IT'%p2         , 'Iso(#tau_{%s})'%l2   ],
    #
    # 'IP1oPT': ['TMath::Max(%s_BPVIP, %s_BPVIP)'%(p1,p2), 'IPmax'],
    #
    'tau_BPVCORRM' : ['tau_BPVCORRM'                            , 'm_{corr}(#tau_{h3})'             ],
    'tau_P'        : ['TMath::Log10(tau_P)'                     , 'p(#tau_{h3})'                    ],
    'tau_VCHI2PDOF': ['TMath::Log10(tau_VCHI2PDOF)'             , 'Vertex #chi^{2}/dof(#tau_{h3})'  ],
    'tau_BPVVD'    : ['TMath::Log10(tau_BPVVD)'                 , 'd_{flight}(#tau_{h3})'           ],
    'tau_BPVLTIME' : ['TMath::Log10(tau_BPVLTIME)'              , 'Decay time(#tau_{h3})'           ],
    'tau_DRTRIOMAX': ['tau_DRTRIOMAX'                           , '#Delta R_{max}(#tau_{h3})'       ],
    'tau_DRoPT'    : ['TMath::Log10(tau_DRTRIOMAX/(tau_PT/1e3))', '#Delta R_{max}/p_{T}(#tau_{h3})' ],
    'tau_DRtPT'    : ['TMath::Log10(tau_DRTRIOMAX*(tau_PT/1e3))', '#Delta R_{max}*p_{T}(#tau_{h3})' ],
    #
    'DPHI'    : ['DPHI'                  , '#Delta#phi'    ],
    'APT'     : ['APT'                   , 'A_{PT}'        ],
    'DOCACHI2': ['TMath::Log10(DOCACHI2)', 'DOCA #chi^{2}' ],
  }

#===============================================================================

def add_variables(factory, dt, spec):
  """
  Add multitude of variables as a function of channel
  """
  params0 = load_vars(dt)
  params  = [v for k,v in params0.iteritems() if k in spec]

  ## add back the new val in spec. Raw title
  params += [[k,k] for k in spec if k not in params0]

  ## Finally, add vars
  for expr,title in params:
    factory.AddVariable( expr, title, '', 'F' )

#===============================================================================

def tss_real_unbiased():
  """
  Return te unbiased tree from data (no DPHI, VCHI2PDOF, BPVCORRM cut)
  The number is still partial, use with care
  """
  import id_utils
  import ditau_cuts
  dtype = get_current_dtype_dt()[0]
  t = import_tree('DitauCandTupleWriter/0/%s_ss'%dtype, 5312)
  id_utils.apply_alias(ditau_cuts, dtype, 'tss_real', t)
  return t
