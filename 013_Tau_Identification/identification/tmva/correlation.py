#!/usr/bin/env python

"""

Produce the correlation table at the preselection level.

"""

## Secondary tools
import tmva_utils  # this augment sys.path
from PyrootCK import *
from ditau_utils import CHANNELS, LABELS_DITAU, get_current_dtype_dt

## Filter list of branches interested, can be slightly more than the final usages
SPEC_1P = 'PT1', 'PT2', 'ISO1', 'ISO2', 'DPHI', 'APT', 'DOCACHI2', 'IP1', 'IP2' #, 'IP1oPT', 'IP2oPT', 'IP1tPT', 'IP2tPT'
SPEC_3P = 'PT1', 'PT2', 'ISO1', 'ISO2', 'DPHI', 'APT', 'DOCACHI2', \
  'tau_DRTRIOMAX', 'tau_VCHI2PDOF', 'tau_BPVCORRM', 'tau_DRoPT', 'tau_DRtPT', 'tau_BPVLTIME'


#===============================================================================

def get_trees_sig_bkg(dt):
  from ditau_prep import t_ztautau, tss_real, t_dymu, t_dye40, t_wmu17jet, t_we
  tss_real0 = tmva_utils.tss_real_unbiased()

  # t1,t2 = t_ztautau, tss_real0    # Against same-sign
  # t1,t2 = t_ztautau, t_dymu       # for mumu channel
  # t1,t2 = t_ztautau, t_dye40      # for ee channel
  # t1,t2 = t_ztautau, t_wmu17jet   # for EWKmu
  # t1,t2 = t_ztautau, t_we         # for EWKe

  filt = utils.join('Preselection', '_Ntrim')
  return t1.CopyTree(filt), t2.CopyTree(filt)


#===============================================================================

def main_calculate_corr(dt):
  ## Load the trees
  t_sig, t_bkg = get_trees_sig_bkg(dt)

  # Loads the library
  ROOT.TMVA.Tools.Instance()
  fout    = ROOT.TFile("correlation/%s.root"%dt, "RECREATE")
  factory = ROOT.TMVA.Factory("correlation_%s"%dt, fout )

  ## Add variables
  import tmva_utils
  spec = SPEC_3P if 'h3' in dt else SPEC_1P
  tmva_utils.add_variables(factory, dt, spec)

  ## Add sources
  factory.AddSignalTree(t_sig)
  factory.AddBackgroundTree(t_bkg)
  factory.PrepareTrainingAndTestTree(ROOT.TCut(''), ROOT.TCut(''), '')

  ## RUN
  factory.TrainAllMethods()     # Train MVAs using the set of training events
  factory.TestAllMethods()      # Evaluate all MVAs using the set of test events
  factory.EvaluateAllMethods()  # Evaluate and compare performance of all configured MVAs

  ## Finally
  fout.Close()

#===============================================================================
# CORRELATION PLOT
#===============================================================================

def extract_single(dt):
  """
  Using the correlation result above, export the nice figure for latex
  """
  fpath = 'correlation/%s.root'%dt
  if not os.path.exists(fpath):
    print 'Missing file, skip: %s'%fpath
    return

  fin = ROOT.TFile(fpath)
  h   = fin.Get('CorrelationMatrixS')
  h.title = 'Correlation Matrix: %s'%LABELS_DITAU[dt]
  h.bit   = ROOT.TH1.kNoTitle
  
  ## Canvas adjustment
  if 'h3' in dt:
    # marker = text
    h.markerSize  = 2.4
    h.markerColor = ROOT.kBlack
    h.Draw('text col')
    #
    ROOT.gPad.SetCanvasSize( 720, 360 )
    ROOT.gPad.SetLeftMargin  (0.16)
    ROOT.gPad.SetRightMargin (0.04)
    ROOT.gPad.SetTopMargin   (0.02)
    ROOT.gPad.SetBottomMargin(0.12)
  else:
    h.markerSize      = 2.6
    h.markerColor     = ROOT.kBlack
    h.xaxis.labelSize = 0.08
    h.yaxis.labelSize = 0.08
    h.Draw('text col')
    #
    ROOT.gPad.SetCanvasSize( 720, 280 )
    ROOT.gPad.SetLeftMargin  (0.10)
    ROOT.gPad.SetRightMargin (0.04)
    ROOT.gPad.SetTopMargin   (0.02)
    ROOT.gPad.SetBottomMargin(0.12)

  ## Finally
  ROOT.gPad.Update()
  ROOT.gPad.SaveAs('correlation/corr_%s.pdf'%dt)
  fin.Close()

def main_extract():
  """
  Getting corr matrix as PDF in style.
  """
  ROOT.gROOT.SetBatch(True)
  ROOT.gStyle.SetPalette(ROOT.kTemperatureMap)
  for dt in CHANNELS:
    extract_single(dt)

#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print tree_real_unbiased().entries

  ## 1: Run
  dt = get_current_dtype_dt()[1]
  main_calculate_corr(dt)

  ## 2: Figures
  # main_extract()
