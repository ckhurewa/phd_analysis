#!/usr/bin/env python

from PyrootCK import *
import tmva_utils

from ditau_utils import ufloat, get_current_dtype_dt


def print_entries_after_preselection(dt, *cut0):
  from ditau_prep import t_ztautau, tss_real, t_dymu, t_dye40, t_we, t_real, t_wmu17jet
  cut     = utils.join('Preselection', '_Ntrim', cut0)
  npresel = t_ztautau.GetEntries(utils.join('Preselection', '_Ntrim'))
  nsel    = t_ztautau.GetEntries(cut)

  ## Calc signal
  sys.path.append(os.path.expandvars('$DIR13/efficiencies'))
  import fast_csc
  se = fast_csc.DF.loc[dt]
  se['csc'] = 94.7
  se['sel'] = 1. * nsel / npresel
  del se['nsig']
  
  S = se.prod().n
  # B = tss_real.GetEntries(cut)  # general
  # B = t_real.GetEntries(cut)-S  # Z->ee, most of data
  B = (t_dymu.GetEntries(cut) / 20571464. * 1976e9 * ufloat( 4.466E-06, 4.58E-08 ) * ufloat( 3.91E-01, 9.65E-03 )).n
  # B = (t_dye40.GetEntries(cut)/ 2126272.  * 1976e9 * ufloat( 9.555E-07, 3.09E-09 ) * ufloat( 3.68E-01, 2.93E-03 )).n
  # B = (t_we.GetEntries(cut)/ 5031270.  * 1976e9 * ufloat( 9.310E-06, 2.84E-08 ) * ufloat( 2.59E-01, 2.23E-03 )).n
  # B = (t_wmu17jet.GetEntries(cut)/ 10015768.  * 1976e9 * ufloat( 1.318E-05, 1.02E-07 ) * ufloat( 2.36E-01, 6.52E-03 )).n

  print 'S  :', S
  print 'B  :', B
  print 'FoM:', S/(S+B)**0.5
  print 'S/B:', S/B if B > 0 else 'nan'

#===============================================================================

def read_weight(dt, ibin=0):
  from bs4 import BeautifulSoup as Soup
  path = '$DIR13/identification/tmva/weights/assist_temp_Cuts.weights.xml'
  with open(os.path.expandvars(path)) as fin:
    dat = fin.read()
  soup = Soup(dat)

  print soup.find('html').find('body').find('methodsetup').find('variables')

  print soup.find('html').find('body').find('methodsetup').find('weights').find('bin', ibin=ibin)


#===============================================================================

if __name__ == '__main__':

  dt = get_current_dtype_dt()[1]
  print dt

  ## letloose
  # print_entries_after_preselection(dt)
  
  ## Isodphi
  # print_entries_after_preselection(dt, '_Iso', 'DPHI > 2.7', 'IP2 > 0.03', 'APT < 0.4')

  ## h3
  # print_entries_after_preselection(dt, 'tau_BPVCORRM < 3e3', 'tau_VCHI2PDOF < 20', 'DPHI > 2.7', 'tau_DRoPT < 5e-3', '_Iso', 'tau_BPVLTIME > 60/1e6')

  ## h1
  # print_entries_after_preselection(dt, '_Iso', 'DPHI > 2.7', 'IP2>0.02', 'IP2<0.5') #, 'IP1<0.5')
#
  print_entries_after_preselection(dt, 'DPHI > 2.7')
  # print_entries_after_preselection(dt, 'DPHI > 2.7',     'ISO1 > 9.4616641572614857e-01', 
  #   'ISO2 > 9.3509556254129322e-01',
  #   'IP1  > 0.00012015617960217796', 
  #   'IP2  > 0.0302118654194516',
  # )
  # read_weight(dt, 100)
