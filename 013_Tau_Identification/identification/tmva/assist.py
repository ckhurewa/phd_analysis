#!/usr/bin/env python

"""

NOTE: Still not accurate as VCHI2PDOF, BPVLTIME, DPHI is biased.

"""

## Secondary tools
import tmva_utils  # this augment sys.path
from PyrootCK import *
from ditau_utils import get_current_dtype_dt, CHANNELS

## Filter list of branches interested
SPEC_1P = 'ISO1', 'ISO2', 'DPHI', 'APT', 'IP1', 'IP2'
SPEC_3P = 'ISO1', 'ISO2', 'DPHI', 'APT', 'tau_VCHI2PDOF', 'tau_DRoPT', \
  'tau_BPVCORRM', 'tau_BPVLTIME', 

## Load trees
import ditau_prep
from ditau_prep import t_ztautau, t_dymu, t_dye40, t_wmu17jet, t_we

#===============================================================================

def _main_calculate(tag, dt, spec, t_sig, t_bkg):
  # Loads the library
  ROOT.TMVA.Tools.Instance()
  name    = '%s_%s'%(tag,dt) if tag else 'temp'
  fout    = ROOT.TFile("assist/%s.root"%name, "RECREATE")
  factory = ROOT.TMVA.Factory("assist_%s"%name, fout)

  ## Add variables
  import tmva_utils
  tmva_utils.add_variables(factory, dt, spec)

  ## Add sources
  factory.AddSignalTree(t_sig)
  factory.AddBackgroundTree(t_bkg)
  factory.PrepareTrainingAndTestTree(ROOT.TCut(''), ROOT.TCut(''), '')

  ## Add the methods
  ctype   = ROOT.TMVA.Types.kCuts
  method  = 'Cuts'
  opts    = "!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart:CreateMVAPdfs=True"
  factory.BookMethod(ctype, method, opts)

  # ## Add the methods: BDT
  # ctype   = ROOT.TMVA.Types.kBDT
  # method  = 'BDT'
  # opts    = "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
  # factory.BookMethod(ctype, method, opts)

  ## RUN
  factory.TrainAllMethods()     # Train MVAs using the set of training events
  factory.TestAllMethods()      # Evaluate all MVAs using the set of test events
  factory.EvaluateAllMethods()  # Evaluate and compare performance of all configured MVAs

  ## Finally
  fout.Close()

#===============================================================================

def _do_simultaneous(dt, mode, spec, filt):
  ## Signal 
  t_sig = ditau_prep.t_ztautau.CopyTree(filt)
  run   = lambda tag, t: _main_calculate('simult_%s_%s'%(mode,tag), dt, spec, t_sig, t)

  ## Run SS
  t_bkg = tmva_utils.tss_real_unbiased().CopyTree(filt)
  run('ss', t_bkg)

  ## Run Zll
  if dt in ('mumu', 'ee'):
    if dt == 'mumu':
      t_bkg = ditau_prep.t_dymu.CopyTree(filt)
    else:
      t_bkg = ditau_prep.t_dye40.CopyTree(filt)
    run('zll', t_bkg)

  ## Run EWK
  if 'mu' in dt:
    t_bkg = ditau_prep.t_wmu17jet.CopyTree(filt)
    run('EWKmu', t_bkg)
  if 'e' in dt:
    t_bkg = ditau_prep.t_wejet.CopyTree(filt)
    run('EWKe', t_bkg)


def main_simult(dt):
  ## Default spec & run
  spec = list(SPEC_3P if 'h3' in dt else SPEC_1P)
  filt = utils.join('Preselection & _Ntrim')
  _do_simultaneous(dt, 'letloose', spec, filt)

  #### With Iso + DPHI (and VCHI2PDOF < 20 for tauh3)
  spec.remove('ISO1') 
  spec.remove('ISO2')
  spec.remove('DPHI')
  filt = utils.join(filt, '_Iso', 'DPHI>2.7')
  if 'h3' in dt:
    spec.remove('tau_VCHI2PDOF')
    filt = utils.join(filt, 'tau_VCHI2PDOF < 20')
  _do_simultaneous(dt, 'isodphi', spec, filt)


#-------------------------------------------------------------------------------

def main_custom(dt, i):

  # spec = 'tau_DRoPT',
  # spec = 'tau_BPVLTIME',
  # spec = 'ISO1', 'ISO2'
  # spec = 'IP1', 'IP2' #, 'APT'
  # spec = SPEC_1P + ('DOCACHI2',)

  spec = [
    # 'tau_DRTRIOMAX',
    # 'tau_PT',
    'TMath::Log10(tau_DRTRIOMAX * (tau_PT/1e3)**(-%f))'%i,
  ]

  filt = utils.join(
    'Preselection',
    '_Ntrim',
    'DPHI > 2.7',
    # 'ISO1 > 9.4616641572614857e-01', 
    # 'ISO2 > 9.3509556254129322e-01',
    # 'IP1  > 0.00012015617960217796', 
    # 'IP2  > 0.0302118654194516',
    #
    '_Iso',
    # 'IP2 > 0.05',
    # 'tau_VCHI2PDOF  < 20',
    # 'tau_BPVCORRM   < 3e3',
    # 'tau_DRoPT      < 5e-3',
    # 'tau_BPVLTIME   > 60e-6',
  )

  ## Actual run
  t_sig = ditau_prep.t_ztautau.CopyTree(filt)
  # t_bkg = tmva_utils.tss_real_unbiased().CopyTree(filt)  # If check DPHI
  t_bkg = ditau_prep.tss_real.CopyTree(filt)
  # t_bkg = ditau_prep.t_dymu.CopyTree(filt)
  _main_calculate(str(i), dt, spec, t_sig, t_bkg)


#===============================================================================

def main_test_DRPT(dt):
  assert dt == 'h3mu'

  filt = utils.join(
    'Preselection',
    '_Ntrim',
    'DPHI > 2.7',
    '_Iso',
  )

  ## Actual run
  t_sig = ditau_prep.t_ztautau.CopyTree(filt)
  t_bkg = ditau_prep.tss_real.CopyTree(filt)

  ## loop over different exponent
  for i in [0.05, 0.1, 0.2, 0.5, 1., 1.5, 2., 3., 5., 10.]:
    spec = 'TMath::Log10(tau_DRTRIOMAX * (tau_PT/1e3)**(%f))'%i, 
    _main_calculate(str(i), dt, spec, t_sig, t_bkg)



#===============================================================================

if __name__ == '__main__':

  ## Actually obtain it
  dt = get_current_dtype_dt()[1]

  ## Simultaneous
  # main_simult(dt)

  ## Custom & DEV
  # main_custom(dt)

  ## Find good exponent in DR#PT
  main_test_DRPT(dt)
