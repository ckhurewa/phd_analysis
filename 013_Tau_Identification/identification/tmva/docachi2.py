#!/usr/bin/env python

"""

This was used to answer Wouter's question concerning IP & DOCACHI2
for mumu, ee channel. 
This is depreciated as DOCAHCI2 is no longer used

"""

#===============================================================================

def study_mumu():
  """
  Try to find best discriminant between ztautau-zmumu in mumu channel.
  """
  assert dt=='mumu'

  params = [
    ('TMath::Log10(mu1_BPVIP)', 'IP1'),
    ('TMath::Log10(mu2_BPVIP)', 'IP2'),
    ('TMath::Log10(DOCACHI2)' , 'DOCACHI2'),
    ('APT', 'APT'),
    # ('DPHI', 'DPHI'),
  ]

  # Loads the library, prep output
  ROOT.TMVA.Tools.Instance()
  fout    = ROOT.TFile("docachi2/tmva_mumu.root", "RECREATE")
  factory = ROOT.TMVA.Factory("TMVAClassification", fout, "Transformations=I;D;P;G,D")

  ## Add variables
  for expr,title in params:
    factory.AddVariable( expr, title, '', 'F' )

  ## Add sources
  filt  = utils.join('Preselection & _Ntrim')
  t1    = t_ztautau.CopyTree(filt)
  t2    = t_dymu.CopyTree(filt)
  # t2    = t_real.CopyTree(filt)

  factory.AddSignalTree(t1)
  factory.AddBackgroundTree(t2)
  factory.PrepareTrainingAndTestTree(ROOT.TCut(''), ROOT.TCut(''), '')

  ## Add the methods
  ctype   = ROOT.TMVA.Types.kCuts
  method  = 'Cuts'
  opts    = "!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart:CreateMVAPdfs=True"
  factory.BookMethod( ctype, method, opts )

  ## RUN
  factory.TrainAllMethods()     # Train MVAs using the set of training events
  factory.TestAllMethods()      # Evaluate all MVAs using the set of test events
  factory.EvaluateAllMethods()  # Evaluate and compare performance of all configured MVAs

  ## Finally
  fout.Close()

#-------------------------------------------------------------------------------

def read_mumu():
  from bs4 import BeautifulSoup as Soup

  ## Extract into dataframe
  acc  = []
  path = './weights/TMVAClassification_Cuts.weights.xml'
  soup = Soup(open(path))
  for node in soup.find('weights').find_all('bin'):
    acc.append({
      # 'ibin'        : node.get('ibin'),
      'effs'        : node.get('effs'),
      'effb'        : node.get('effb'),
      'ip1_min'     : node.find('cuts').get('cutmin_0'),
      'ip1_max'     : node.find('cuts').get('cutmax_0'),
      'ip2_min'     : node.find('cuts').get('cutmin_1'),
      'ip2_max'     : node.find('cuts').get('cutmax_1'),
      'docachi2_min': node.find('cuts').get('cutmin_2'),
      'docachi2_max': node.find('cuts').get('cutmax_2'),
      'apt_min'     : node.find('cuts').get('cutmin_3'),
      'apt_max'     : node.find('cuts').get('cutmax_3'),
    })
  df = pd.DataFrame(acc).applymap(float)

  ## Processing
  xarr  = df['effs']
  # g1    = ROOT.TGraph(100, xarr, df['effb'])
  g2    = ROOT.TGraph(100, xarr, 10**df['ip1_min'])
  g3    = ROOT.TGraph(100, xarr, 10**df['ip2_min'])
  g4    = ROOT.TGraph(100, xarr, 10**df['docachi2_min'])
  g     = ROOT.TMultiGraph()
  c     = ROOT.TCanvas('c1', 'c1', 960, 480)
  #
  # g.Add(g1)
  g.Add(g2)
  g.Add(g3)
  g.Add(g4)
  g.Draw('A')
  ROOT.gPad.SetLogy(True)
  ROOT.gPad.SetGridy(True)
  #
  g.xaxis.title = 'Signal Efficiency'
  g.yaxis.title = 'Selection cut (lower bound)'
  g2.title      = 'IP_{1}'
  g3.title      = 'IP_{2}'
  g4.title      = 'DOCACHI2'
  g2.fillColor  = ROOT.kWhite
  g3.fillColor  = ROOT.kWhite
  g4.fillColor  = ROOT.kWhite
  g2.lineColor  = ROOT.kRed
  g3.lineColor  = ROOT.kBlue
  leg = ROOT.gPad.BuildLegend()
  leg.x1 = 0.80
  leg.x2 = 0.98
  leg.y1 = 0.80
  leg.y2 = 0.98

  ROOT.gPad.SaveAs('docachi2/mumu_cuts.pdf')

  exit()

#===============================================================================

if __name__ == '__main__':
  # study_mumu()
  read_mumu()
