#!/usr/bin/env python

import uncertainties
import samesign_utils
import bkg_samesign
from PythonCK.stringutils import concat_multilines, insert_line
from id_utils import (
  pd, CHANNELS, DT_TO_LATEX, tabulate, fmt2cols, fmt2cols2, inject_midrule,
  ufloat_to_latex, ufloat_to_latex2, ufloat_to_latex3,
)

## specialized to columns founds only in ssfit
COL_SSFIT = {
  'NSS'     : '\\nss{}',
  'chi2pdof': 'Fit \chisqndf',
  'QCD'     : '\\nss{QCD}',
  'EWK'     : '\\nss\\Vj',
  'Ztau'    : '\\nss\\ztautau',
  'rQCD'    : '\\rQCD',
  'rEWK'    : '\\rVj',
}

#===============================================================================
# LATEX
#===============================================================================

def latex_ssfit():
  ## Prepare the data
  se_ewk = bkg_samesign.result_ewk_prefinal()['rEWK']
  se_ewk = se_ewk.rename(index={'emu_raw':'emu', 'mue_raw':'mue'})
  df     = bkg_samesign.result_ssfit_raw()[['NSS', 'chi2pdof', 'QCD', 'EWK', 'Ztau']].copy()
  df['rQCD'] = bkg_samesign.result_qcd_detailed()['rQCD']
  df['rEWK'] = se_ewk
  df = df.loc[CHANNELS+['mue']] # Canonical order

  ## Discard redundant number
  df.loc['mue', 'NSS']      = ''
  df.loc['mue', 'chi2pdof'] = ''
  df.loc['mue', 'QCD']      = ''
  df.loc['mue', 'Ztau']     = ''
  df.loc['mue', 'rQCD']     = ''

  ## Customize the mue row
  indices = dict(DT_TO_LATEX) # make a copy
  indices['emu'] = '\dmue{} (hard \elec)'
  indices['mue'] = '\dmue{} (hard \muon)'
  df = df.rename(indices)

  ## Prepare latex bicol
  df['chi2pdof'] = df['chi2pdof'].fmt3f
  df['QCD']      = df['QCD'].apply(fmt2cols)
  df['EWK']      = df['EWK'].apply(fmt2cols)
  df['Ztau']     = df['Ztau'].apply(fmt2cols)
  df['rQCD']     = df['rQCD'].apply(fmt2cols2)
  df['rEWK']     = df['rEWK'].apply(fmt2cols2)

  ## strip header & return
  msg = df.to_latex(escape=False)
  print msg
  return '\n'.join(msg.split('\n')[3:-2])

#-------------------------------------------------------------------------------

def latex_ssfit_corr():
  """
  Focus on the correlation matrix for appendix. Ugly but works
  """
  def fmt1(l):
    return '\n'.join(['{:9.4f}    '.format(x) for x in l])

  label1 = '\n'.join(['QCD', 'EWK'  , 'Ztau '])
  label2 = '\n'.join(['QCD', 'EWKe ', 'EWKmu', 'Ztau'])

  print
  print 'Channel src    Amount                   Correlation matrix'
  print '-'*80
  df = bkg_samesign.result_ssfit_raw()
  for ch in CHANNELS:
    if ch!='emu': # skip for now
      fracs = df.loc[ch,['QCD', 'EWK', 'Ztau']].values
      msg1  = fmt1(fracs)
      mat   = uncertainties.correlation_matrix(fracs)
      msg2  = tabulate(mat, tablefmt='plain', floatfmt='.4f')
      print concat_multilines( '%-7s'%ch, label1, msg1, '|\n'*3, msg2, '|\n'*3 )
      print

  ## Then last one for mue
  fracs = df.loc['emu','QCD'], df.loc['emu','EWK'], df.loc['mue','EWK'], df.loc['emu','Ztau']
  msg1  = fmt1(fracs)
  mat   = uncertainties.correlation_matrix(fracs)
  msg2  = tabulate(mat, tablefmt='plain', floatfmt='.4f')
  print concat_multilines('mue    ', label2, msg1, '|\n'*4, msg2, '|\n'*4 )
  print '-'*80

#-------------------------------------------------------------------------------

def latex_ddqcd_nos_nss():
  """
  For appendix table on how rQCD is computed from
  """
  df = bkg_samesign.result_qcd_detailed()[['OS', 'SS', 'rQCD']].rename(DT_TO_LATEX)
  df = df.rename(columns={'rQCD': '\\rQCD'})
  df.index.name = 'Channel'
  return df.to_markdown(formatters={'\\rQCD': ufloat_to_latex3}, stralign='right')

#-------------------------------------------------------------------------------

def latex_ewk_mc():
  """
  Compatible for appendix section.

  Columns: OS-W, SS-W, SS-Z, rW, rEWK
  """
  rename_columns = {
    'Wos'   : '\\nos{\\W}',
    'Wss'   : '\\nss{\\W}',
    'Zos'   : '\\nos{\\Z}',
    'Zss'   : '\\nss{\\Z}',
    'rW'    : '$r_\W$',
    'rZ'    : '$r_\Z$',
    'rEWK'  : '\\rVj',
    'nSS'   : '\\nss{}',
    'final' : 'Expected EWK',
  }
  # filter & order
  columns = ['Wss', 'Zss', 'Wos', 'rW', 'rZ', 'rEWK']

  formatters = {
    'Wos'   : ufloat_to_latex,
    'Wss'   : ufloat_to_latex,
    'Zss'   : ufloat_to_latex,
    'rW'    : ufloat_to_latex2,
    'rZ'    : ufloat_to_latex2,
    'rEWK'  : ufloat_to_latex2,
  }

  df = bkg_samesign.result_ewk_prefinal()[columns].rename(DT_TO_LATEX)
  df.index.name = 'Channels'
  return df.to_markdown(formatters=formatters, rename_columns=rename_columns)

#-------------------------------------------------------------------------------

def latex_ztau_constraints():
  """
  Report results at different the Ztautau constraints.
  Need manually-collected cache
  """
  ## Grab result
  df = bkg_samesign.compare_ztau_constraints()

  ## Pretty format, then latex
  df['QCD']  = df['QCD'].apply(fmt2cols)
  df['EWK']  = df['EWK'].apply(fmt2cols)
  df['Ztau'] = df['Ztau'].apply(fmt2cols)
  df['NOS']  = df['NOS'].apply(fmt2cols)
  df = df.rename({'emu': '\\dmue (hard \\elec)', 'mue': '\\dmue (hard \\muon)'})
  df = df.rename(index=DT_TO_LATEX, columns=COL_SSFIT)
  df = df.rename(columns={'NOS': '\\nos{QCD+\\Vj}'})
  ## inject bicol into columns
  col = df.columns.tolist()
  col[1] = fmt2cols(col[1])
  col[2] = fmt2cols(col[2])
  col[3] = fmt2cols(col[3])
  col[4] = fmt2cols(col[4])
  df.columns = col
  ## remove table spec, put manually in latex
  # inject_midrule doesn't look pretty in compact multiindex
  msg = df.to_latex(escape=False)
  msg = insert_line(msg, 9, '\\midrule')
  msg = insert_line(msg, 14, '\\midrule')
  msg = insert_line(msg, 19, '\\midrule')
  msg = insert_line(msg, 24, '\\midrule')
  msg = insert_line(msg, 29, '\\midrule')
  msg = insert_line(msg, 34, '\\midrule')
  msg = insert_line(msg, 39, '\\midrule')
  ## collapse index & column names into one row
  lines = msg.split('\n')
  lines[3] = '&'.join(lines[3].split('&')[0:2]+lines[2].split('&')[2:])
  del lines[2]
  return '\n'.join(lines[1:-2])


def markdown_ztau_constraints():
  """
  Lite version for Ronan
  """
  df = bkg_samesign.compare_ztau_constraints()['NOS'].unstack()[['x0', 'x1', 'x2']]
  df = df.rename(DT_TO_LATEX, columns={
    'x0': 'No \\ztautau',
    'x1': 'Constrained',
    'x2': 'Constrained x2',
  })
  return df.fmt1f.to_markdown()


#===============================================================================

if __name__ == '__main__':
  print latex_ssfit()
  # print latex_ssfit_corr()
  # print latex_ddqcd_nos_nss()
  # print latex_ewk_mc()
  # print latex_ztau_constraints()

  ##
  # print markdown_ztau_constraints()
