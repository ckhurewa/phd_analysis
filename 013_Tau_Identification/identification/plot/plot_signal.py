#!/usr/bin/env python

"""

Reproduce `plot_signal` script using QHist v3 package.

"""

import sys
from pyroot_zen import ROOT
from qhist import QHist

import ditau_cuts
import id_utils
import selected
import candidates
from id_utils import pd, CHANNELS, LABELS_DITAU

#===============================================================================
# COMBINE PLOTS
#===============================================================================

def postproc(qhist):

  ## Tune legend position
  leg = qhist.anchors.legend
  leg.header   = 'LHCb#sqrt{#it{s}} = 8 TeV'
  leg.x1NDC    = 0.67
  leg.x2NDC    = 0.92
  leg.y1NDC    = 0.54
  leg.y2NDC    = 0.90
  # leg.textSize = 0.05
  leg.textSize = 0.055
  # Avoid legend over Zmumu peak
  if qhist.name.startswith('mumu_Mnowin'):
    if qhist.suffix == 'zoom':
      leg.x1NDC -= 0.277
      leg.x2NDC -= 0.277
    else:
      leg.x1NDC -= 0.50
      leg.x2NDC -= 0.50
      leg.y1NDC -= 0.07
      leg.y2NDC -= 0.07

  qhist.anchors.mainpad.RedrawAxis()
  qhist.anchors.mainpad.Update()

  ## Optional mask
  if 'Mnowin' in qhist.name:
    dt = qhist.prefix
    h  = qhist.anchors.stack
    mmin, mmax = (20,120) if dt=='emu' else (20,60) if dt in ('mumu', 'ee') else (30,120)
    ymax = qhist.anchors.mainpad.uymax
    box1 = ROOT.TBox( h.xaxis.xmin, 0., mmin, ymax )
    box2 = ROOT.TBox( mmax, 0., h.xaxis.xmax, ymax )
    for box in (box1, box2):
      box.ownership = False
      box.fillStyle = 1001
      box.fillColorAlpha = 1, 0.3
      box.Draw()  

  ## Force no-exponent
  qhist.anchors.stack.xaxis.noExponent = True

#-------------------------------------------------------------------------------

def draw_combine(dt):
  ## Prep data
  trees    = selected.load_selected_trees_comb(dt)
  res_raw  = candidates.results_comb_nomasswindow()[dt]
  ent_nomw = [ res_raw[t.name].n for t in trees] # get ordered expected value for each tree
  res_raw2 = candidates.results_comb_withmasswindow()[dt]
  ent_wmw  = [ res_raw2[t.name].n for t in trees]
  prefixes = id_utils.PREFIXES[dt]
  l1,l2    = id_utils.LABELS[dt]

  ## Mass window cut
  cut_mass_window = 'mass < %i'%ditau_cuts.UPPER_MASS[dt]

  ## Prep histo template
  H0 = QHist()
  H0.trees     = trees
  H0.filters   = 'weight' # for new regime
  H0.prefix    = dt
  H0.ymin      = 1e-3 # To remove zero hairline
  H0.auto_name = True
  H0.batch     = True
  H0.st_code   = 2
  H0.postproc  = postproc

  ## Base mass plot
  Hmass = H0(params='mass', xmin=20, xmax=120, xlabel='#it{m}(%s) [GeV/#it{c}^{2}]'%LABELS_DITAU[dt])

  ## Template with no mass window Only for mass plot
  Hnowm   = Hmass(cuts='', normalize=ent_nomw)
  Hnowm20 = Hnowm(name='Mnowin'  , xbin=20, ylabel='Candidates / (5 GeV/#it{c}^{2})')
  Hnowm50 = Hnowm(name='Mnowin50', xbin=50, ylabel='Candidates / (2 GeV/#it{c}^{2})')
  Hnowm20.draw() # Base mass plot, full range (no mass window)
  # Hnowm50.draw()
  # if dt=='mumu': # zoomed
  #   Hnowm20(suffix='zoom', ymax=146, ylog=False).draw()

  # ## Template with mass window (default)
  # Hwm   = Hmass(cuts=cut_mass_window, normalize=ent_wmw)
  # Hwm20 = Hwm(name='Mwin'  , xbin=20, ylabel='Candidates / (5 GeV/c^{2})')
  # Hwm50 = Hwm(name='Mwin50', xbin=50, ylabel='Candidates / (2 GeV/c^{2})')
  # Hwm20.draw()
  # Hwm50.draw()
  # # Hwm20(ylog=True, ymin=1, suffix='log').draw()

  # ## Other plots
  # H = H0(cuts=cut_mass_window, normalize=ent_wmw, ylabel='Candidates')
  # ## Mother (di-tau)
  # H(params='P/1e3' , xmin=90 , xmax=3e3, xlog=True, xlabel='p [GeV/c]'    ).draw()
  # H(params='PT/1e3', xmin=1  , xmax=500, xlog=True, xlabel='p_{T} [GeV/c]').draw()
  # H(params='APT'   , xmin=0.0, xmax=1.0,            xlabel='A_{pT}'       ).draw()
  # H(params='ETA'   , xmin=2.0, xmax=8.0, xbin=24,   xlabel='#eta'         ).draw()
  # H(params='Y'     , xmin=1.8, xmax=4.6, xbin=14                          ).draw()

  # ## Children
  # minpt = 5 if dt in ('ee','emu','mumu') else 10
  # H(params='PT1' , xmin=20   , xmax=100, xlog=True, xlabel='p_{T}(#tau_{%s})[GeV/c]'%l1).draw()
  # H(params='PT2' , xmin=minpt, xmax=100, xlog=True, xlabel='p_{T}(#tau_{%s})[GeV/c]'%l2).draw()
  # H(params='ETA1', xmin=2.0  , xmax=4.5, xbin=20  , xlabel='#eta(#tau_{%s})'%l1        ).draw()
  # H(params='ETA2', xmin=2.0  , xmax=4.5, xbin=20  , xlabel='#eta(#tau_{%s})'%l2        ).draw()

  # ## tauh3 only
  # if 'h3' in dt:
  #   H(params='tau_BPVCORRM/1e3', xmin=0.5, xmax=3.2, xlabel='#tau_{h3} m_{corr} [GeV/c^{2}]').draw()
  #   H(params='tau_BPVLTIME*1e6', xmin=1e1, xmax=1e4, xlabel='#tau_{h3} Decay time [fs]'     ).draw()
  #   H(params='tau_DRoPT'       , xmin=0, xmax=0.005, xlabel='#tau_{h3} #DeltaR/p_{T} [(GeV/c)^{-1}]').draw()
  #   H(params='tau_DRTRIOMAX'   , xmin=0, xmax=0.2  , xlabel='#tau_{h3} #DeltaR_{max}'       ).draw()
  #   H(params='tau_VCHI2PDOF'   , xmin=0, xmax=20   , xlabel='#tau_{h3} Vertex #chi^{2}'     ).draw()


#===============================================================================

if __name__ == '__main__':
  ## For consistent style
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  # set_qhist_color_palette()

  ## suggested changes during PAPER
  from array import array
  palette = array('i', [1, 2, 4, ROOT.kCyan, 6, ROOT.kGreen+3, ROOT.kAzure+7])
  ROOT.gStyle.SetPalette(len(palette), palette)

  if '--recalc' in sys.argv:
    for dt in CHANNELS:
      draw_combine(dt)
    sys.exit()

  ## DEV
  draw_combine('emu')
  # draw_combine_ss(dt)
