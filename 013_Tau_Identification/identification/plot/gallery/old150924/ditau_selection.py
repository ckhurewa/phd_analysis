#!/usr/bin/env python

import re
import sys
from PyrootCK import *

dtype = 'h1_mu'

tname_os = 'DitauCandTupleWriter/'+dtype
tname_ss = tname_os + '_ss'

real = (
  #
  # '~/Desktop/2928.root',
  # '~/Desktop/2929.root',
  # '~/Desktop/2930.root',
  # '~/Desktop/2931.root',
  #
  # 150924
  3348,
  3349,
  3350,
  3351,
  3352,
  3360,
)

t_real      = import_tree( tname_os, *real  ).SetTitle('Real 2012 (oppsign)')
t_ress      = import_tree( tname_ss, *real  ).SetTitle('Real 2012 (samesign)')
t_ztautau   = import_tree( tname_os, 3376   ).SetTitle('MC Z02TauTau 8TeV')
t_ztautauss = import_tree( tname_ss, 3376   ).SetTitle('MC Z02TauTau 8TeV (ss)')
t_wmuj      = import_tree( tname_os, 3363   ).SetTitle('MC: WMu17 (oppsign)')
t_wmujss    = import_tree( tname_ss, 3363   ).SetTitle('MC: WMu17 (samesign)')
t_wej       = import_tree( tname_os, 3368   ).SetTitle('MC: We (oppsign)')
t_wejss     = import_tree( tname_ss, 3368   ).SetTitle('MC: We (samesign)')
t_wtauj     = import_tree( tname_os, 3362   )
t_dytau     = import_tree( tname_os, 3365   )
t_dymu      = import_tree( tname_os, 3366   )



#==============================================================
# SELECTION CUTS
#==============================================================

master_cuts = {
  'e_h3': [
    'MM           > 35000',
    'e_PT         > 15000',
    'tau_PT       > 20000',
    'C12_APT      < 0.6',
    'C12_DPHI     > 2.5',
    # 'C12_DOCA     > 0.01',
    # 'C12_DOCA     < 1.0',
    #
    'tau_ADOCAMAX  < 1',
    'tau_MM        > 800',
    'tau_MM        < 1600',
    'tau_BPVCORRM  < 2700',
    'tau_DRTRIOMAX < 0.20',
    'tau_DRTRIOMID < 0.15',
    'tau_DRTRIOMIN < 0.10',
    'tau_PTTRIOMAX > 7000',
    'tau_PTTRIOMID > 3000',
    'tau_PTTRIOMIN > 1000',  
  ],
  'h1_mu': [
    'MM           > 20000',
    'mu_PT        > 20000',
    'pi_PT        > 5000',
    'C12_APT      > 0.1',
    'C12_APT      < 0.6',
    'C12_DPHI     > 2.7',
    'C12_DOCA     > 0.01',
    'C12_DOCA     < 1.0',
    'IPS          > 0.5',
    'IPS          < 20',
    #
    'pi_TRGHOSTPROB < 0.1',
  ],
  'h3_mu': [
    'MM           > 35000',
    'mu_PT        > 15000',
    'tau_PT       > 20000',
    'C12_APT      < 0.6',
    'C12_DPHI     > 2.5',
    # # 'IPS          > 3',
    'tau_ADOCAMAX  < 1',
    'tau_MM        > 800',
    'tau_MM        < 1600',
    'tau_BPVCORRM  < 2700',
    'tau_DRTRIOMAX < 0.20',
    'tau_DRTRIOMID < 0.15',
    'tau_DRTRIOMIN < 0.10',
    'tau_PTTRIOMAX > 7000',
    'tau_PTTRIOMID > 3000',
    'tau_PTTRIOMIN > 1000',
  ],
  #
  'mu_mu': [
    'MM         > 20000',
    'mu_hPT     > 20000',
    'mu_lPT     > 5000',
    #
    '(C12_APT     > 0.3)',
    'C12_APT      < 0.7',
    'C12_DPHI     > 2.5',
    '(IPS         > 3)',  # Those in bracket conflicts with data-driven Z02mumu shape.
    'IPS          < 20',
    '(C12_DOCA    > 0.02)',
    'C12_DOCA     < 1.0',
    # 'C12_DOCACHI2 > 0.1',
    '(!Z02mumu_peak)',
  ]
}[dtype]

## Curate the exclusive master_cuts
cut_master_selection    = []
cut_ignorepeak_selection= []
cut_datadriven          = []
for cut in master_cuts:

  ## For master, take all
  cut_master_selection.append( cut )  # Take all anyway

  ## For ignorepeak, ignore peak.
  if 'Z02mumu_peak' not in cut:
    cut_ignorepeak_selection.append(cut)

  ## Finally, for data-driven, take only those without bracket
  if not re.search(r'^\(.*\)$', cut):
    cut_datadriven.append( cut )

## Change them to string
cut_master_selection     = QHistUtils.join( cut_master_selection  )     # selection + extra + offpeak
cut_ignorepeak_selection = QHistUtils.join( cut_ignorepeak_selection )  # selection + extra
cut_datadriven           = QHistUtils.join( cut_datadriven )            # selection



#==============================================================
# ALIASES
#==============================================================

def duomax(s1, s2):
  return "{s1}*({s1}>={s2}) + {s2}*({s2}>{s1})".format(**locals())
def duomin(s1, s2):
  return "{s1}*({s1}<={s2}) + {s2}*({s2}<{s1})".format(**locals())



t_QCD       = t_real.Clone()
t_QCDss     = t_ress.Clone()
t_Z02mumu   = t_real.Clone()
t_Z02mumu2  = t_real.Clone()  # Alternative approach using mass cut, for selection study only, not shape

#----------------#
# Global aliases #
#----------------#

for tname,t in dict(locals()).iteritems():
  if isinstance( t, ROOT.TTree ):

    t.SetAlias('Mass_20_120', '(MM>20000) & (MM<120000)')

    if dtype == 'e_h3':
      t.SetAlias('Isolation'    , '(e_PTFrac05C>0.8) & (tau_PTFrac05C>0.9) & (e_PTCone05C<2000)  & (tau_PTCone05C<1000) ')
      # t.SetAlias('AntiIsolation', '(e_PTFrac05C<0.5) & (tau_PTFrac05C<0.5) & (e_PTCone05C>10000) & (tau_PTCone05C>10000)')
      t.SetAlias('AntiIsolation', '(e_PTCone05C>10000) & (tau_PTCone05C>10000)')

    if dtype == 'h1_mu':
      t.SetAlias('Isolation'    , '(mu_PTFrac05C>0.9) & (pi_EFrac02PN05A>0.8) & (mu_PTCone05C<1000)  & (pi_PTCone05C<1000)  & (mu_ECone05A<100000)') #' & (pi_ECone05A > 50000)')
      t.SetAlias('AntiIsolation', '(mu_PTFrac05C<0.6) & (pi_EFrac02PN05A<0.3) & (mu_PTCone05C>10000) & (pi_PTCone05C>10000) & (mu_ECone05A>300000)')

    ## h3_mu
    if dtype == 'h3_mu':
      t.SetAlias('Isolation'    , '(mu_PTFrac05C>0.9) & (tau_PTFrac05C>0.9) & (mu_PTCone05C<1000)  & (tau_PTCone05C<1000) ')
      t.SetAlias('AntiIsolation', '(mu_PTCone05C>10000) & (tau_PTCone05C>10000)')
      # t.SetAlias('AntiIsolation', '(mu_PTFrac05C<0.4) & (tau_PTFrac05C<0.4) & (mu_PTCone05C>10000) & (tau_PTCone05C>10000)')

    if dtype == 'mu_mu':
      t.SetAlias('Isolation'    , '(mu1_PTFrac05C>0.9) & (mu2_PTFrac05C>0.9) & (mu1_PTCone05C<1000)  & (mu2_PTCone05C<1000)')
      # t.SetAlias('AntiIsolation', '(mu1_PTFrac05C<0.4) & (mu2_PTFrac05C<0.4) & (mu1_PTCone05C>10000) & (mu2_PTCone05C>10000)')
      t.SetAlias('AntiIsolation', '(mu1_PTCone05C>10000) & (mu2_PTCone05C>10000)')
      #
      t.SetAlias('mu_hPT'       , duomax( 'mu1_PT', 'mu2_PT' ))
      t.SetAlias('mu_lPT'       , duomin( 'mu1_PT', 'mu2_PT' ))
      t.SetAlias('Z02mumu_peak' , '(MM > 80000) & (MM < 100000)')


#-----------#
# MX Series #
#-----------#

if dtype == 'e_mu':
  raise NotImplementedError

if 'mu' in dtype:  
  t_W   = t_wmu
  t_Wss = t_wmuss

  t_Z0.SetAlias       ( 'Preselection', 'StrippingDitau_MuXLineDecision && MATCH && ditau_type_'+dtype)
  t_real.SetAlias     ( 'Preselection', 'StrippingDitau_MuXLineDecision'    )
  t_ress.SetAlias     ( 'Preselection', 'StrippingDitau_MuXssLineDecision'  )
  t_W.SetAlias        ( 'Preselection', 'StrippingWMuControl10LineDecision' ) 
  t_Wss.SetAlias      ( 'Preselection', 'StrippingWMuControl10LineDecision' )
  t_Z02mumu2.SetAlias ( 'Preselection', 'StrippingZ02MuMuLineDecision & mu1_PT>20000 & mu2_PT>20000' ) # Wrong, not align, but trying
  t_Z02mumu.SetAlias  ( 'Preselection', 'StrippingWMuLineDecision && C12_DPHI>3.0 && IPS<0.1 && C12_DOCA<0.01 & C12_APT<0.1' ) # Note, cannot use Z02MuMuLine in order to have full shape for mass plot later.
  t_QCD.SetAlias      ( 'Preselection', 'StrippingWMuLowLineDecision')
  t_QCDss.SetAlias    ( 'Preselection', 'StrippingWMuLowLineDecision')

if 'e' in dtype:
  t_W   = t_we
  t_Wss = t_wess

  t_Z0.SetAlias       ( 'Preselection', 'StrippingDitau_EXLineDecision && MATCH && ditau_type_'+dtype)
  t_real.SetAlias     ( 'Preselection', 'StrippingDitau_EXLineDecision'    )
  t_ress.SetAlias     ( 'Preselection', 'StrippingDitau_EXssLineDecision'  )
  t_W.SetAlias        ( 'Preselection', 'StrippingWeLowLineDecision' )
  t_Wss.SetAlias      ( 'Preselection', 'StrippingWeLowLineDecision' )
  # t_Z02mumu2.SetAlias ( 'Preselection', 'StrippingZ02MuMuLineDecision & mu1_PT>20000 & mu2_PT>20000' ) # Wrong, not align, but trying
  # t_Z02mumu.SetAlias  ( 'Preselection', 'StrippingWMuLineDecision && C12_DPHI>3.0 && IPS<0.1 && C12_DOCA<0.01 & C12_APT<0.1' ) # Note, cannot use Z02MuMuLine in order to have full shape for mass plot later.
  t_QCD.SetAlias      ( 'Preselection', 'StrippingWeLowLineDecision')
  t_QCDss.SetAlias    ( 'Preselection', 'StrippingWeLowLineDecision')

  #-----

t_Z0.SetAlias   ('Selection', 'Preselection & Isolation & '+cut_master_selection)

t_real.SetAlias ('SelOffPeak'   , 'Preselection & Isolation & '+cut_master_selection)
t_real.SetAlias ('SelIgnorePeak', 'Preselection & Isolation & '+cut_ignorepeak_selection)
t_real.SetAlias ('SelOnPeak'    , 'SelIgnorePeak & Z02mumu_peak')
t_real.SetAlias ('Selection'    , 'SelIgnorePeak')  # USED AT FINAL MASS PLOT!

## For t_ress, the 'Selection' is unambiguous, which is always offpeak (unlike t_real)
t_ress.SetAlias ('Selection', 'Preselection & Isolation & '+cut_master_selection)


t_QCD.SetAlias  ('Selection', 'Preselection & AntiIsolation & '+cut_master_selection)
t_QCDss.SetAlias('Selection', 'Preselection & AntiIsolation & '+cut_master_selection)

t_W.SetAlias    ('Selection', 'Preselection & Isolation & '+cut_master_selection)
t_Wss.SetAlias  ('Selection', 'Preselection & Isolation & '+cut_master_selection)

# t_real.SetAlias ('QCD_pdf', 'StrippingWMuLowLineDecision & AntiIsolation')
# t_ress.SetAlias ('QCD_pdf', 'StrippingWMuLowLineDecision & AntiIsolation')

# t_W.SetAlias    ('EWK_pdf', 'StrippingWMuControl10LineDecision & Isolation') 
# t_Wss.SetAlias  ('EWK_pdf', 'StrippingWMuControl10LineDecision & Isolation')  # isolation?

t_Z02mumu.SetAlias  ( 'Selection' , 'Preselection & Isolation & '+cut_datadriven)
t_Z02mumu2.SetAlias ( 'Selection' , 'Preselection & Isolation & '+cut_datadriven)



#==============================================================

# ## EX LINES

# t_W   = t_we 
# t_Wss = t_wess

# t_Z0.SetAlias  ('Preselection', 'StrippingDitau_EXLineDecision && MATCH && ditau_type_'+dtype)
# t_ress.SetAlias('Preselection', 'StrippingDitau_EXssLineDecision' ) # AntiIsolation
# t_W.SetAlias   ('Preselection', 'StrippingWeLowLineDecision'      )

# t_Z0.SetAlias  ('DitauOS', 'Preselection                    && Isolation')
# t_real.SetAlias('DitauOS', 'StrippingDitau_EXLineDecision   && Isolation')
# t_ress.SetAlias('DitauSS', 'StrippingDitau_EXssLineDecision && Isolation')

# t_real.SetAlias('QCD_pdf', 'StrippingWeLowLineDecision && AntiIsolation')
# t_ress.SetAlias('QCD_pdf', 'StrippingWeLowLineDecision && AntiIsolation')

# t_W.SetAlias   ('EWK_pdf', 'StrippingWeLowLineDecision & Isolation')
# t_Wss.SetAlias ('EWK_pdf', 'StrippingWeLowLineDecision & Isolation')



#==============================================================
# PRESELECTION study: Single var observation
#==============================================================

QHist.master_cuts = 'Preselection'
# QHist.master_cuts = 'Selection'
QHist.st_line     = False

# QHist.legends = 'Z0', 'REAL', 'QCD'
# QHist.trees   = t_Z0, t_real, t_QCD
# QHist.legends = 'Z0', 'RealData-Samesign (~QCD+...)', 'W+jet'
# QHist.trees   = t_Z0, t_QCDss , t_W #, t_Z02mumu2
QHist.legends = 'Z0', 'RealData-Samesign (~QCD+...)', 'W+jet', 'Z02mumu'
QHist.trees   = t_Z0, t_QCDss , t_W, t_Z02mumu2
QHist.xbin    = 50
QHist.auto_name = True

h = QHist()
h.params  = 'mu_hPT'
h.xmin    = 0
h.xmax    = 1E5
h.xbin    = 40
# h.draw()

h = QHist()
h.params  = 'tau_hPT'
h.xmin    = 0
h.xmax    = 1E5
h.xbin    = 40
# h.draw()

#-----------

h = QHist()
h.params  = 'C12_APT'
# h.draw()

h = QHist()
h.params  = 'C12_DOCA'
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'C12_DOCACHI2'
# h.draw()

h = QHist()
h.params  = 'C12_DPHI'
# h.draw()

h = QHist()
h.params  = 'IPS'
h.xmin    = 1E-2
h.xmax    = 1E3
# h.draw()

# raw_input(); sys.exit(0)

#---------

h = QHist()
h.params  = 'tau_MM'
h.xbin    = 50
# h.xmax    = 6000
# h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'tau_BPVCORRM'
h.xbin    = 50
h.xmax    = 6000
# h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'tau_ADOCAMIN'
# h.xmax    = 0.2
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'tau_ADOCAMAX'
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'tau_VCHI2'
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'tau_DRTRIOMIN'
# h.draw()

h = QHist()
h.params  = 'tau_DRTRIOMID'
# h.draw()

h = QHist()
h.params  = 'tau_DRTRIOMAX'
# h.draw()

h = QHist()
h.params  = 'tau_PTTRIOMIN'
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'tau_PTTRIOMID'
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'tau_PTTRIOMAX'
h.xlog    = True
# h.draw()


#-------------

h = QHist()
h.params  = 'pi_TRGHOSTPROB'
h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'pi_HCALFrac'
# h.xmin    = 0
# h.xmax    = 2
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'pi_ECALFrac'
h.xmin    = 0
h.xmax    = 2
# h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'mu1_PTCone05C'
# h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'mu2_PTCone05C'
# h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'e_PTFrac05C'
# h.ylog    = True
# h.draw()

h = QHist()
h.params  = 'tau_PTFrac05C'
# h.ylog    = True
# h.draw()


h = QHist()
h.params  = 'e_PTCone05C'
h.xmin    = 1E2
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'tau_PTCone05C'
h.xmin    = 1E2
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'pi_ECone02PN'
h.xmin    = 1E4
h.xbin    = 50
h.xlog    = True
# h.draw()

h = QHist()
h.params  = 'pi_EFrac02PN05A'
h.xlog    = False
# h.draw()

# raw_input(); sys.exit(0)



# for p in [
#   # 'pi_ECone02A',
#   # 'pi_ECone02C',
#   # 'pi_ECone02PN',
#   'pi_ECone05A',
#   # 'pi_ECone05C',
#   # 'pi_ECone07A',
#   # 'pi_ECone07C',
#   #
#   'pi_PTCone05A',
#   'pi_PTCone05C',
#   'pi_PTCone05N',
#   #
#   # 'pi_NCone01A',
#   # 'pi_NCone01C',
#   #
#   # 'pi_EFrac02PN05A',
#   # 'pi_EFrac02PN05N',
#   # 'pi_EFrac05A',
#   # 'pi_EFrac05C',
#   # 'pi_PTFrac05A',
#   # 'pi_PTFrac05C',
#   # 'pi_PTFrac05N',
# ]:
#   h = QHist()
#   h.params  = p
#   h.title   = p
#   # h.xlog  = False
#   h.xmin    = 1E2
#   h.xmax    = 1E6
#   h.xlog    = True
#   # h.ylog = True
#   h.draw()

# raw_input(); sys.exit(0) 

#==============================================================

## For theoretical prediction (selection efficiency)

# h = QHist()
# h.trees   = [ t_real, t_Z0 ]
# h.params  = 'MM'
# h.cuts    = 'Isolation'
# h.normalize = ( 11229, 4000 )
# h.legends   = None
# h.xmin      = 20E3
# h.draw()

# raw_input(); sys.exit(0);


#==============================================================

## QCD Shape & EWK Shape -- samesign

QHist.reset()

# param = 'pi_PT - mu_PT'
# param = 'e_PT - tau_PT'
# param = 'tau_PT - mu_PT'
# param = 
# param = 'C12_APT'
# param = 'IPS'


h = QHist()
h.trees   = t_ress, t_QCDss, t_Wss
h.params  = 'C12_APT'
# h.params  = 'mu_hPT - mu_lPT'
h.cuts    = 'Selection'
# h.xmin    = -50E3
# h.xmax    = 50E3
h.xbin    = 30
# h.xbin  = 50
h.xlog    = False
h.legends = 'Real data (SS)', 'QCD-Shape', 'EWK-Shape'
# h.st_code = 1
# h.normalize = False
# h.options = 'goff'
h.draw()

raw_input(); sys.exit(0)


 # MIGRAD MINIMIZATION HAS CONVERGED.
 # MIGRAD WILL VERIFY CONVERGENCE AND ERROR MATRIX.
 # FCN=8.51009 FROM MIGRAD    STATUS=CONVERGED      30 CALLS          31 TOTAL
 #                     EDM=1.66301e-07    STRATEGY= 1      ERROR MATRIX ACCURATE
 #  EXT PARAMETER                                   STEP         FIRST
 #  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE
 #   1  frac0        5.43860e-01   1.01857e+00   2.04396e-03  -2.88901e-04
 #   2  frac1        4.56060e-01   7.09957e-01   3.74165e-03   1.21909e-04
 #                               ERR DEF= 0.5

#-----------

def fit(h0, *pdfs):
  ntot  = h0.GetEntries()

  arr = ROOT.TObjArray(len(pdfs))
  ROOT.SetOwnership( arr, False )
  for pdf in pdfs:
    arr.Add(pdf.Clone())
  fitter = ROOT.TFractionFitter( h0.Clone(), arr )
  ROOT.SetOwnership( fitter, False )
  fitter.Constrain(0, 0.0, 1.0)
  fitter.Constrain(1, 0.0, 1.0)
  fitter.Fit()

  ## Draw
  frac  = ROOT.Double()
  err   = ROOT.Double()

  c = ROOT.TCanvas()
  ROOT.SetOwnership( c, False )
  st = ROOT.THStack()
  ROOT.SetOwnership( st , False ) 
  list_frac = []
  for i in xrange(len(pdfs)):
    htemp = fitter.GetMCPrediction(i)
    ROOT.SetOwnership( htemp, False )
    fitter.GetResult( i, frac, err )
    htemp.Scale(ntot*frac/htemp.Integral())
    st.Add(htemp)
    list_frac.append(float(frac))

  st.Draw()
  # st.GetHistogram().SetTitle(dtype)
  # st.GetHistogram().SetXTitle(param)
  h0 = h0.Clone()
  ROOT.SetOwnership( h0, False )
  h0.Scale(ntot)
  h0.Draw('E1 p same')
  ymax = max([ st.GetMaximum(), h0.GetMaximum() ])
  st.SetMaximum(ymax)
  c.Update()

  return list_frac

FRAC_QCD, FRAC_EWK = fit(*h)
# FRAC_QCD, FRAC_EWK = 1,0

print 'FRAC_QCD', FRAC_QCD
print 'FRAC_EWK', FRAC_EWK

# raw_input(); sys.exit(0)


#--------------------------------------------

QHist.reset()

## QCD oppsign / samesign ratio
# Determine from data's candidate, by putting anti-isolation cut.

h = QHist()
h.trees  = t_QCD, t_QCDss
h.params = 'MM'
h.cuts   = 'Selection'
h.xmin   = 20E3
h.xmax   = 120E3
h.normalize = False
h.draw()

RATIO_QCD = h[0].Integral() / h[1].Integral()
print 'RATIO_QCD', RATIO_QCD

## EWK oppsign / samesign ratio
# Determine from MC's candidate, no need for isolation cut

h = QHist()
h.trees  = t_W, t_Wss
h.params = 'MM'
h.cuts   = 'Selection'
h.xmin   = 20E3
h.xmax   = 120E3
h.normalize = False
h.draw()

RATIO_EWK = h[0].Integral() / h[1].Integral()
print 'RATIO_EWK', RATIO_EWK

raw_input(); sys.exit(0)


#==============================================================

QHist.reset()

def count( tree, *cuts ):
  cut = QHistUtils.join( cuts )
  return float(tree.Draw( 'MM', cut, 'goff' ))


## 1st-pass: Raw N_OPSIGN & N_SAMESIGN 
N_OPSIGN   = count( t_real, 'SelIgnorePeak', 'Mass_20_120' )
N_SAMESIGN = count( t_ress, 'Selection'    , 'Mass_20_120' )

if dtype=='mu_mu':
  N_Z02MUMU_OFFPEAK = count(t_Z02mumu, 'Selection', 'Mass_20_120', '!Z02mumu_peak' )
  N_Z02MUMU_Z0PEAK  = count(t_Z02mumu, 'Selection', 'Mass_20_120', 'Z02mumu_peak'  )
  N_OPSIGN_Z0PEAK   = count(t_real   , 'SelOnPeak', 'Mass_20_120' )

for key,val in sorted(dict(locals()).iteritems()):
  if key.startswith('N_'):
    print key, val

n0 = N_OPSIGN                               # OS total 
n1 = N_SAMESIGN * FRAC_QCD * RATIO_QCD      # OS QCD
n2 = N_SAMESIGN * FRAC_EWK * RATIO_EWK      # OS EWK
# n3 = (N_Z02MUMU_OFFPEAK+N_Z02MUMU_Z0PEAK) * (N_OPSIGN_Z0PEAK / N_Z02MUMU_Z0PEAK)
nx = n0 - n1 - n2 # - n3

norms = ( n0, n1, n2, nx )

print norms

# raw_input(); sys.exit(0)

## Mass plot
h = QHist()
h.name      = 'ditau_MM'
h.suffix    = dtype
h.trees     = t_real, t_QCD, t_W, t_Z0
# h.trees     = t_real, t_QCD, t_W, t_Z02mumu, t_Z0
h.params    = 'MM/1000'
h.cuts      = 'Selection'
h.xmin      = 20
h.xmax      = 120
h.xbin      = 20
h.normalize = norms
# h.normalize = False
h.st_code   = 2
h.legends   = 'Data', 'QCD', 'EWK', 'Signal - Z0'
# h.legends   = 'Data', 'QCD', 'EWK', 'Z02mumu', 'Signal - Z0'
h.xlabel    = 'MM [GeV]'
h.draw()


#==============================================================

raw_input()


