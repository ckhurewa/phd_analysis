"""

Utilities for same-sign computation. 
Shared between Ztautau and Higgs mu tau

"""

from PyrootCK import *
from id_utils import *
import qhist_postprocessors as postprocessors
from PyrootCK.QHist.CoreV3 import QHist as QHistV3
from array import array
import shutil

#===============================================================================
# PREP
#===============================================================================

@memorized
def get_normalizer():
  """
  Helper import module
  """
  sys.path.append(os.path.expandvars('$DIR13/results'))
  import normalizer
  return normalizer


def ztautau_frac_upperlim(path_context):
  """
  Given the path to context, return the upper limit of number of same-sign ztautau
  to be used as constraint in same-sign fitting. The return is normalized to 
  expected number of samesign in data (i.e., fraction).

  Need normalizer
  """
  def get_upperlim(args):
    ## 2sigma above
    u = args[2].to_ufloat()
    return u.n + 2*u.s
  ## get the absolute number first
  tag        = 'check_mc_nos_nss_ss'
  normalizer = get_normalizer()
  context    = normalizer.loading_context_pickle(path_context)
  raw_dat    = normalizer.calc_single_array(tag, context)
  z_upperlim = raw_dat.loc['ztautau0'].apply(get_upperlim)
  ## then, fraction of total same-sign.
  data_ss = context[tag].loc['real']
  return z_upperlim/data_ss


#===============================================================================
# BREAKING SAMESIGN ENGINE
#===============================================================================

def ssfit_engine(tag, trees, param='DPT', filters='', xlabel="#Deltap#lower[0.8]{#scale[0.6]{T}}", ranges=(-80,120), preprocessor=None, maxbin=None):
  """
  Return a dataframe of results.

  For range, recommend providing range such that it's divisible by 30
  for maximum bins flexibility.
  """

  ## Prepare destination name
  frame   = inspect.stack()[1]
  module  = inspect.getmodule(frame[0])
  dpath   = os.path.abspath(module.__file__).replace('.pyc','.py').replace('.py','/ss')
  if not os.path.exists(dpath):
    os.makedirs(dpath)

  def save(name):
    dest = dpath+'/'+'%s.pdf'%name
    ROOT.gPad.SetCanvasSize(480,330)
    ROOT.gPad.canvas.name = name.lower()
    ROOT.gPad.canvas.SaveAs(dest)

  ## INIT
  fout    = {}
  tf_type = True, False, None
  xbins   = 15, 20, 24, 25, 30, 36, 40, 45, 50, 60, 72, 90, 120, 160, 180
  xmin, xmax = ranges

  ## Exception: In case of null SS tree, return null 
  if trees[0] is None:
    logger.info('Found null same-sign data')
    res = {'valid': True, 'floats': [ufloat(0,0), ufloat(0,0)], 'chi2pdof': pd.np.nan}
    return pd.DataFrame({tag+'_null_0': res}).T

  ## Exception: In case of small SS tree, use simplified result
  entries = [t.GetEntries(filters) for t in trees]
  if entries[0] < 10:
    logger.info('Use small stats approximation: %s'%tag)
    nos   = entries[0]
    uzero = ufloat(0, 0)
    uhalf = ufloat(nos/2, nos/2)
    ufull = ufloat(nos, nos**0.5)
    fout[tag+'_small_0_half'] = [True, [uhalf, uhalf], pd.np.nan]
    fout[tag+'_small_0_qcd']  = [True, [ufull, uzero], pd.np.nan]
    fout[tag+'_small_0_ewk']  = [True, [uzero, ufull], pd.np.nan]
    df = pd.DataFrame(fout).T
    df.columns = 'valid', 'floats', 'chi2pdof'
    ## create blank image for now, if library PIL (pillow) is available.
    try:
      from PIL import Image, ImageDraw, ImageFont
      W,H  = 567, 390
      img  = Image.new('RGB', (W,H), 'white')
      draw = ImageDraw.Draw(img)
      font = ImageFont.truetype('/Library/Fonts/Arial.ttf', 12)
      msg  = 'N/A: Small'
      w, h = draw.textsize(msg, font=font)
      draw.text((((W-w)/2, (H-h)/2)), msg, 'black', font=font)
      img.save('rect_samesign/ss/%s_small_0_ewk.pdf'%tag)
    except ImportError, e:
      print e 
    ## finally
    return df

  ## Sanity check: enough statistics
  logger.info('[%s] %s'%(tag, entries))
  assert all(n >= 10 for n in entries[1:]), "Found null/poor histo, abort"

  ## estimate maxbin ~ 2.0 x sample size
  if maxbin is None:
    val = trees[0].GetEntries(filters) * 2.0
    maxbin = min(max(val, min(xbins)), max(xbins))

  ## Start the loop
  QHist.reset()
  for xbin in xbins:
    ## skip if nbin is too large
    if maxbin and xbin > maxbin:
      continue
    h = QHist()
    h.filters   = filters
    h.title     = 'Fraction fitting in same-sign candidates'
    h.trees     = trees
    h.params    = param
    h.xmin      = xmin
    h.xmax      = xmax
    h.xbin      = xbin
    h.xlabel    = xlabel
    h.normalize = False
    h.legends   = [ t.title for t in trees ]
    h.draw()
    logger.debug('Draw complete')

    ## Engine1: TFractionFit
    for range_data in tf_type:
      fitter, res, uarr = postprocessors.fraction_fit(h, range_data, preprocessor=preprocessor)
      chisqpdof = fitter.chisquare/(fitter.NDF or 1.)
      name = ('%s_tf_%i_%s'%(tag, xbin, range_data)).lower()
      save(name)
      valid = (res.IsValid()) and (res.Status()==0) and (res.correlationMatrix.Determinant()>0)
      fout[name] = valid, uarr, chisqpdof

    ## Engine2: RooFit
    res, uarr, chisqpdof = postprocessors.fraction_roofit(h)
    name = ('%s_rf_%i'%(tag, xbin)).lower()
    save(name)
    fout[name] = (res.status()==0 and res.numInvalidNLL()==0), uarr, chisqpdof

    # ## DEBUG
    # sys.exit()

  ## Finally
  df = pd.DataFrame(fout).T
  df.columns = 'valid', 'floats', 'chi2pdof'
  return df

#===============================================================================

def _ssfit_stats(df0, show_all=False):
  """
  Actual computation & masking
  """
  def sum_rerr(se):
    if se.vsum.n==0:
      return 0.
    return sum(x.s**2 for x in se['floats'])**0.5/se.vsum.n

  acc = {}
  for dt,df in df0.groupby(level=0):
    df = df.xs(dt)

    ## Calculate, filter, sort
    df['vsum']       = df.floats.apply(np.sum)
    df['rerr']       = df.vsum.apply(lambda u: u.rerr)
    df['dt']         = [s.split('_')[0] for s in df.index]
    df['chi2pdof']   = df['chi2pdof'].apply(float)
    df['sum_rerr']   = df.apply(sum_rerr, axis=1)
    df['nbins']      = [int(s.split('_')[2]) for s in df.index]
    df['rerr_stats'] = df.vsum.apply(lambda u: 0 if u.n==0 else u.n**-0.5)

    ## Veto, only in general case (not small/null)
    mask     = df.valid
    is_null  = len(df.index)==1 and 'null' in df.index[0]
    is_small = all('small' in s for s in df.index)
    if not show_all and (not is_small and not is_null):
      mask &= (df.rerr <= 2*df.rerr_stats) # less than 2 * stat uncer
      # mask &= (df.chi2pdof > 0.5)          # chi2 itself not too low
      ## sum_rerr not always a good indicator
      # mask &= (df.sum_rerr < 2*(df.vsum**-0.5)) # less than 2 * stat uncer

      ## If it's empty here, raise check
      if not mask.any():
        print df
        assert False, "Mask too tight, no survivors: dt="+dt

      ## Try remove None, True
      ## Allow fallback to contain None,True
      mask2 = mask.copy()
      mask2 &= (1-df.index.str.contains('none')) # 'none' too-low chi2 than 'False'
      mask2 &= (1-df.index.str.contains('true')) # 'slightly more realistic
      if mask2.any():
        mask = mask2

    ## Final format, sorted by CHI2
    df2 = df[mask][['floats', 'nbins', 'vsum', 'rerr', 'chi2pdof', 'sum_rerr']]
    acc[dt] = df2.sort_values(['rerr'])
  return acc

def load_ssfit_stats(path, tag='break_samesign', show_all=False):
  """
  Return computed values of ssfit stats, for printing & post processing
  """
  df0 = df_from_path(tag, path)
  return _ssfit_stats(df0, show_all)


def print_ssfit_stats(path, only_dt=None, show_all=False):
  """
  Showing stats from the shelve file

  If not show_all, it will remove these:
  - rerr > 50%
  """
  for dt, df in load_ssfit_stats(path, show_all=show_all).iteritems():
    if only_dt is not None and only_dt!=dt:
      continue
    ## separate the float columns for print
    df['float0'] = df['floats'].apply(lambda l: '{:5.1f}'.format(l[0]))
    df['float1'] = df['floats'].apply(lambda l: '{:5.1f}'.format(l[1]))
    print df

#-------------------------------------------------------------------------------

def guess_ssfit_spec(path, override={}):
  """
  Helper logic to attempt to pick up the best ssfit spec.
  """
  stats0   = load_ssfit_stats(path)
  channels = stats0.keys() # based on existing cache, compat for both 013, 015
  acc      = pd.Series()
  for dt in channels:
    ## manual override has highest priority
    if dt in override:
      acc[dt] = override[dt]
      continue
    ## looking at stats
    stats = stats0[dt]
    ## Too-many bins is often not too good
    stats = stats[stats.nbins < 150]
    ## Then, as it's sorted by rerr, prefer TF series over RF first
    stats_tf = stats.filter(regex='.*tf.*', axis=0)
    if len(stats_tf.index) > 0:
      # if it's rich, put min nbins
      if dt in ('emu', 'eh1', 'h1mu'):
        stats_tf_fine = stats_tf[stats_tf.nbins > 30]
        if len(stats_tf_fine) > 0:
          acc[dt] = stats_tf_fine.index[0]
          continue
      # throw worst chi2pdof away (~1/3)
      n = len(stats_tf.index)
      stats_tf = stats_tf.sort_values('chi2pdof')[:int(2.*n/3)+1]
      # throw worst rerr away
      n = len(stats_tf.index)
      stats_tf = stats_tf.sort_values('rerr')[:int(2.*n/3)+1]
      # throw worst sum_rerr away
      # n = len(stats_tf.index)
      # stats_tf = stats_tf.sort_values('sum_rerr')[:int(2.*n/3)+1]
      # then just pick first one, by rerr
      
      acc[dt] = stats_tf.sort_values('rerr').index[0]
      continue
    ## last fallback, only RF series left, choose one with smalles chi2pdof
    acc[dt] = stats.sort_values('chi2pdof').index[0]
  return acc


#===============================================================================

def result_ssfit_raw(spec_ssfit, path_breakss, path2):
  """
  Interface to 013_Ztautau where context-path is prefered.
  """
  data_ss = df_from_path('check_data_nos_nss', path2)['SS'].xs('evt',level=1)
  return _result_ssfit_raw(spec_ssfit, path_breakss, data_ss)

def result_ssfit_raw_HMT(spec_ssfit, path_breakss, data_ss, tag):
  """
  Interface to 015_HMT where context-object is prefered.
  """
  # data_ss = data_ss.rename(TT_TO_DT) # when this is used in HMT analysis
  return _result_ssfit_raw(spec_ssfit, path_breakss, data_ss, tag)

def _result_ssfit_raw(spec_ssfit, path_breakss, data_ss, tag='break_samesign'):
  """
  Load the result in shelves into memory, keep only the requested spec,
  reindex into dict of channels.

  Also apply the correction factor such that the sum of fit should be exactly
  equal to observed amount.

  NOTE:
  - In 'EWK' there'll be separated entries for emu+mue.
  - The separated entries, QCD, EWK, Ztau, already has the correction factor
    such that their sum is strictly equals to SS (due to some entries outside
    fitting range).
  """

  ## Load dataframe
  df_ss_raw = df_from_path(tag, path_breakss)

  ## Pickout only the one from requested spec.
  df = pd.DataFrame()
  for dt, df0 in df_ss_raw.groupby(level=0):
    spec        = spec_ssfit[dt]
    df0         = df0.xs(dt).loc[spec]
    df0['spec'] = spec
    df[dt]      = df0
  df = df.T

  ## assert valud
  assert df.valid.all(), 'Found invalid pick.'
  del df['valid']

  ## Inflation
  df['NSS']   = data_ss
  df['vsum']  = df['floats'].apply(np.sum)
  df['QCD']   = df.apply(lambda se: se['floats'][0], axis=1)
  df['EWK']   = df.apply(lambda se: se['floats'][1], axis=1)
  df['Ztau']  = df.apply(lambda se: se['floats'][-1], axis=1)

  # rewire the mue channel, skip for HMT
  if 'emu' in df.index:
    df.loc['mue'] = df.loc['emu']
    df.loc['mue','EWK'] = df.loc['emu','floats'][2]

  ## collapse NSS correction
  df['corr']  = df.apply(lambda se: 1.*se.NSS/se.vsum.n if se.vsum.n>0 else 1., axis=1)
  df['QCD']   *= df['corr']
  df['EWK']   *= df['corr']
  df['Ztau']  *= df['corr']
  del df['corr']

  ## Note the number of bins, useful for unbiased plot
  df['xbin'] = df['spec'].apply(lambda s: int(s.split('_')[-2]))

  ## sort by latex order, careful of the dt, tt convention
  # index = [x for x in ]
  # df = df.reindex(list(CHANNELS)+['mue'])
  
  ## Finally
  return df


#===============================================================================
# EWK BACKGROUND EXTRAPOLATION FACTOR
#===============================================================================

def load_rewk(path_context, spec=None, tag_iso='check_mc_nos_nss', tag_noiso='check_mc_selnoiso'):
  """
  Spec:
  - Pick source to calculate factor r_{EWK}
  - Specify which stage r should be taken from
    - True : Use after isolation
    - False: Use before isolation ( not recommended unless stats is too low after iso )
    - None : Use r=1

  collapse
  If True, do weighted average of rEWK from rW+rZ.
  (False for debugging, True for production).
  """

  ## Required normalizer & context
  # check if it's path or actual context (in case of 015, context is prepped)
  normalizer = get_normalizer()
  if isinstance(path_context, basestring):
    context = normalizer.loading_context_pickle(path_context)
  elif isinstance(path_context, dict):
    context = path_context
  else:
    raise ValueError('Unknown input type: %s'%type(path_context))

  def preprocess(tname):
    table = normalizer.calc_single_array(tname, context)
    return table.applymap(lambda args:args[2].to_ufloat())

  def collapse_rW_rZ(df):
    W,Z = zip(*df.iterrows())[1]
    return weighted_average([[W.r,W.SS], [Z.r,Z.SS]])

  ## Load them all first
  df_iso_os   = preprocess(tag_iso+'_os')
  df_iso_ss   = preprocess(tag_iso+'_ss')
  df_noiso_os = preprocess(tag_noiso+'_os')
  df_noiso_ss = preprocess(tag_noiso+'_ss')

  ## support for HMT
  valid_channels = df_iso_os.columns

  ## Loop over channel
  acc = {}
  for dt in valid_channels:
    df_iso   = pd.concat([df_iso_os[dt]  , df_iso_ss[dt]]  , axis=1)
    df_noiso = pd.concat([df_noiso_os[dt], df_noiso_ss[dt]], axis=1)
    df = pd.concat({'ISO':df_iso, 'NOISO':df_noiso})
    df.columns = ['OS','SS']
    acc[dt] = df

  ## finally
  df = pd.concat(acc)
  df['r'] = df.OS/df.SS

  ## Post-process if there's spec given
  if spec is None:
    return df

  ## Start applying spec
  acc = {}
  for ch, d_ewk in spec.iteritems():

    ## some assertion first
    assert_msg = 'Need un-mixed iso & non-iso regime between W/Z'
    iso_regime = list({ x for x in d_ewk.values() if x is not None })
    assert len(iso_regime)==1, assert_msg
    iso_regime = 'ISO' if iso_regime[0] else 'NOISO'

    ## Reshape
    dt  = 'emu' if ch=='mue' else ch
    dat = df.loc[dt].loc[iso_regime].loc[d_ewk]
    dat.insert(0, 'Src', dat.index)
    dat.insert(0, 'ISO', pd.Series(d_ewk))
    dat.index = [ 'W' if s.startswith('w') else 'Z' for s in dat.index ]

    ## erase info if rZ == 1
    if dat.loc['Z','ISO'] is None:
      dat.loc['Z','OS'] = pd.np.nan
      dat.loc['Z','r']  = dat.loc['Z','SS']/dat.loc['Z','SS'].n

    ## Attach weighted rEWK, then collect
    dat.loc['V','r'] = collapse_rW_rZ(dat)
    acc[ch] = dat

  ## wrap & sort.
  df = pd.concat(acc) #.reindex(list(CHANNELS)+['mue'], level=0) # reindex not compat with HMT
  return df

#===============================================================================

def result_ewk_prefinal(ssfit_raw, rewk_parsed):
  """
  Continue the compuation to get EWK almost ready for any processing.
  Collapse W+Z together, but still keep emu separated for mass window count.
  """
  ## Collapse W+Z together
  df = pd.DataFrame([
    rewk_parsed.xs('W', level=1).OS,
    rewk_parsed.xs('W', level=1).SS,
    rewk_parsed.xs('Z', level=1).SS,
    rewk_parsed.xs('W', level=1).r ,
    rewk_parsed.xs('Z', level=1).r ,
  ]).T
  df.columns = 'Wos', 'Wss', 'Zss', 'rW' , 'rZ'

  ## Add Zos, for frac comutation
  df['Zos'] = df.Zss  * df.rZ

  ## Add the weighted average
  weighter = lambda se: weighted_average([(se.rW,se.Wss), (se.rZ,se.Zss)])  
  df.loc[:,'rEWK']  = df.apply(weighter, axis=1)

  ## Get the nSS from ssfit, then multiply it out, and collapse mue+emu
  df['nSS']   = ssfit_raw['EWK']
  df['final'] = df.nSS * df.rEWK
  df.loc['emu_raw'] = df.loc['emu']
  df.loc['mue_raw'] = df.loc['mue']
  df = df.drop('mue').drop('emu')
  return df


#===============================================================================
# CHECKER
#===============================================================================

def spec_W_Z(key):
  """
  Return appropriate W/Z pair to pick up based on key = dt/tt type.
  """
  if key in ('ee', 'eh1', 'eh3', 'emu'):
    return {'W': 'wejet'   , 'Z': 'dye10'}
  elif key in ('mumu', 'mu'):
    return {'W': 'wmumujet', 'Z': 'zmu17jet'}
  return {'W': 'wmu17jet', 'Z': 'zmu17jet'}


def compat(data, mcW, mcZ):
  """
  Return True if the two results (DD, MC) are deemed compatible.
  Check that the difference between nominal values is less than total err.
  """
  diff_n = abs(mcW.n + mcZ.n - data.n)
  uncer  = mcW.s + mcZ.s + data.s
  return diff_n < uncer


def compare_ewk_mc_ssfit(rewk_raw, result_raw):
  """
  Compare the fit result against MC.
  This helps choosing the spec to be more justifiable & prevent bad one.
  Should be checked before proceeding any further

  rewk_raw   = load_rewk() # no spec
  result_raw = _result_ssfit_raw()

  Note: need rewk_raw because it always need the with-iso version.
  """

  ## Use input result_raw as a base (compat for both 013, 015)
  df = result_raw.copy()[['EWK', 'NSS', 'spec']].rename(columns={'EWK':'Data'})

  ## Retrieve the expected MC amount OS & SS
  dat  = rewk_raw.xs('ISO', level=1)
  get  = lambda V: pd.concat({key:dat.loc[(key.replace('mue','emu'), spec_W_Z(key)[V])] for key in df.index}).unstack()
  df_W = get('W').rename(columns=lambda s: 'W-'+s)
  df_Z = get('Z').rename(columns=lambda s: 'Z-'+s)
  df   = df.join(df_W).join(df_Z)
  df['W+Z (MC)'] = df['W-SS']+df['Z-SS']

  ## Apply compat check, re-order
  df['compat'] = df.apply(lambda se: compat(se['Data'], se['W-SS'], se['Z-SS']), axis=1)
  cols = df.columns.tolist()
  cols = cols[1:-1] + cols[0:1] + cols[-1:] # move data to before last, close to MC estimate.
  df = df[cols]
  return df


def compare_ewk_mc_os(compare_ss, result_prefinal):
  """
  Compare against expected OS number from MC.
  """
  df = pd.DataFrame()
  df['W']      = compare_ss['W-OS']
  df['Z']      = pd.Series({dt:compare_ss['Z-SS' if dt in ('mumu','ee') else 'Z-OS'][dt] for dt in compare_ss.index})
  df['MC']     = df[['W', 'Z']].sum(axis=1)
  df['DD']     = result_prefinal['final'].rename({'emu_raw':'emu', 'mue_raw':'mue'})
  df['compat'] = df.apply(lambda se: compat(se.DD, se.W, se.Z), axis=1)
  return df

#===============================================================================
# DRAWER
#===============================================================================

def _postproc(h, chi2df):
  """
  Postprocessor appropriate for unbias SS-fit.
  To be used for both Ztau and HMT
  """
  ## Nudge the legend
  leg = h.anchors.legend
  leg.header = 'LHCb 8TeV'
  leg.x1NDC  = 0.80
  leg.x2NDC  = 1.05
  leg.y2NDC  = 0.97
  leg.y1NDC  = leg.y2NDC-0.06*leg.nRows  # items-dependent height
  # leg.textAlign = 13 # no, text not align with graphic

  ## Show chi2/dof
  text = h.anchors.text_chi2 = ROOT.TLatex(leg.x1NDC, leg.y1NDC, "#chi^{2}/dof = %.3f"%chi2df)
  text.NDC = True
  text.textAlign = 13
  text.textSize *= 0.7
  text.Draw()

  ## Nudge ytitle
  h.anchors.stack.yaxis.titleSize   *= 0.8
  h.anchors.stack.yaxis.titleOffset *= 0.95

  ## Prepare pad for pull
  c = h.anchors.canvas
  mainpad = h.anchors.mainpad
  mainpad.bottomMargin = 0.31
  mainpad.topMargin    = 0.01
  mainpad.leftMargin   = 0.10
  mainpad.rightMargin  = 0.005
  mainpad.Update()

  ## Make pull pad
  pullpad = h.anchors.pullpad = ROOT.TPad('pull', 'pull', .005, .005, .995, .300)
  pullpad.leftMargin   = 0.095
  pullpad.rightMargin  = 0.
  pullpad.bottomMargin = 0.35
  h_data  = ROOT.TGraphAsymmErrors.from_TH1(h[0])
  h_model = h.anchors.stack.stack.Last()
  h_model.xaxis.title = h.xlabel # steal
  g = postprocessors.make_pullhist(pullpad, h_data, h_model)
  g.xaxis.labelSize   *= 3.0
  g.xaxis.titleOffset *= 1.0
  g.xaxis.titleSize   *= 2.5
  g.yaxis.titleOffset *= 1.4

def draw_ssfit_unbiased(suffix, trees, nmz, xbin, chi2df, colors, xlabel1, xlabel2):
  """
  Draw version of SS = nQCD + nEWK using unbiased template (before TFF).
  """
  ROOT.gROOT.batch = True

  ## Prep xlabel 
  text_pt = '#font[12]{p}#kern[-0.2]{#lower[0.4]{#scale[0.7]{T}}}'
  xlabel  = '{0}({1}) - {0}({2}) [GeV/c]'.format(text_pt, xlabel1, xlabel2)

  ## Prep colors
  palette = array('i', colors)
  ROOT.gStyle.SetPalette(len(palette), palette)

  ## Start drawing
  h = QHistV3()
  h.name      = 'ssfit_unbiased'
  h.suffix    = suffix
  h.trees     = trees
  h.params    = 'DPT'
  h.xmin      = -80
  h.xmax      = 120
  h.xbin      = xbin
  h.ymin      = 1e-3
  h.normalize = [nominal_value(val) for val in nmz]
  h.st_code   = 2
  h.xlabel    = xlabel
  h.ylabel    = 'Candidates/(%.1f GeV/c)'%((h.xmax-h.xmin)/h.xbin)
  h.postproc  = lambda h: _postproc(h, chi2df)
  h.draw()


#===============================================================================
# EXPORT
#===============================================================================

def export_pdf(spec, srcdir):
  """
  Give a spec (dict of key, pdfname), copy & rename it to target dir.
  Used by both 013, 015.

  Note: The default target is subdir in srcdir, can be used in conjunction 
  with latex.sh
  """
  ## Prepare target dir
  target = os.path.join(srcdir, 'selected')
  dpath = os.path.expandvars(target)
  if os.path.exists(dpath):
    for fname in os.listdir(dpath):
      fpath = os.path.join(dpath, fname)
      if os.path.isfile(fpath):
        os.unlink(fpath)    
  else:
    os.makedirs(dpath)
  ## Do actual copy
  logger.info('Source dir: '+srcdir)
  logger.info('Target dir: '+dpath)
  for key, name in spec.iteritems():
    fpath = os.path.expandvars(os.path.join(srcdir, 'ss', name+'.pdf'))
    if not os.path.exists(fpath):
      logger.warning('Not found, skip: '+name)
      continue
    shutil.copy(fpath, os.path.join(dpath, key+'.pdf'))
    logger.info('Copied: '+fpath.replace(srcdir, '.'))

#===============================================================================
