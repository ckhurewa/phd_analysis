#!/usr/bin/env bash

## Vars
PROJ=${HOME_EPFL}/Papers/004_Z02TauTau_2012
DATE=`date +%y%m%d`

## Plot
./plot_signal.py --recalc
rm varbin/*.df # to always redraw
./varbin.py --recalc

## Export to latex: combine plot
make -C plot_signal/
mv -v plot_signal/minipdf plot_signal/$DATE
rm -vrf $PROJ/figs/plot_signal
cp -vR plot_signal/$DATE $PROJ/figs/plot_signal

## Export to latex: varbin
rm -vrf varbin/$DATE
mkdir varbin/$DATE
mv -v varbin/*.pdf varbin/$DATE
rm -vrf $PROJ/figs/varbin
cp -vR varbin/$DATE $PROJ/figs/varbin

## update IPS
rm -vf $PROJ/ANA/11_bkg_zll.tex
rm -vf $PROJ/ANA/12_bkg_samesign.tex
rm -vf $PROJ/ANA/13_bkg_others.tex
rm -vf $PROJ/ANA/14_bkg_summary.tex
rm -vf $PROJ/ANA/28_esel.tex
rm -vf $PROJ/ANA/29_systematics.tex
rm -vf $PROJ/ANA/30_results.tex
rm -vf $PROJ/ANA/73_app_combine.tex
