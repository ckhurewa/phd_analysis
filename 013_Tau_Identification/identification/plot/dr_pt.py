#!/usr/bin/env python

"""

Plot of DR/PT used to reply to Ronan

"""

from PyrootCK import *
ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

path = '$DIR13/identification/tmva/dr_pt/%s_h3mu.root'

st = ROOT.THStack()

# arr = 0.1, 0.2, 0.5, 0.05, 1.0, 1.5, 2.0, 3.0, 5.0, 10.0
arr = -3., -2., -1., -0.5, -0.1, 0.1, 0.5, 1.0, 2.0, 3.0

for i,ex in enumerate(arr):
  fin = ROOT.TFile(os.path.expandvars(path%ex))
  fin.ownership = False
  h = fin.Get('Method_Cuts').Get('Cuts').Get('MVA_Cuts_effB').Clone()
  h.title       = 'DR * (PT ** %s)'%ex
  h.lineColor   = i%4+1
  h.markerStyle = i%5+1
  st.Add(h, 'PLC')

st.Draw('nostack')
st.xaxis.title = 'Signal eff'
st.yaxis.title = 'Background eff'

leg = ROOT.gPad.BuildLegend()
leg.ConvertNDCtoPad()
leg.x1NDC = 0.10
leg.x2NDC = 0.50
leg.y1NDC = 0.35
leg.y2NDC = 0.90
exit()