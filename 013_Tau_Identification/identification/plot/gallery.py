#!/usr/bin/env python

from PyrootCK import * 
from id_utils import PROCESS_ROOTLABELS, LABELS_TAU, drawers
# from ditau_prep import * # comment out when not needed.
from qhist import QHist, utils
from qhist.styles import set_qhist_color_palette

#===============================================================================
# DITAU OBSERVATION
#===============================================================================

def prelim():
  H = QHist()
  # H.trees = tss_real, tssdd_qcd, t_wmu17jet
  # H.trees = tss_wmujet, tss_wmujet_noiso
  H.trees   = t_real, tss_real
  # H.trees   = t_real, t_ztautau, t_dye40
  # H.trees   = tss_real, tssdd_qcd, tss_wmu17jet, tss_ztautau0, t_ztautau0
  # H.trees   = tssdd_qcd, tss_ztautau, tss_ztaujet, tss_wejet, tss_wmu17jet
  # H.trees   = t_wtaujet, tss_wtaujet

  # H.filters   = 'Preselection', 'tau_VCHI2PDOF<20', '_Iso'
  # H.filters   = 'Preselection', '_Iso', 'DPHI > 2.7'
  H.filters = 'Selection' #, 'IP1<1', 'IP2<1'
  # H.filters = 'Sel_Comb'

  # H(params='diff_samesign').draw()
  # H(params='tau_PT/1e3', xmin=0, xmax=60, xbin=20, normalize=False).draw()
  # H(params='tau_VDoP', xlog=True).draw()
  # H(params='tau_BPVLTIME * 1e6', xmin=30, xmax=5e3, xbin=40, xlog=True).draw()
  # H(params='IP1', cuts='IP1>0', xlog=True).draw()
  # H(params='IP2', cuts='IP2 > 0', xlog=True).draw()
  # H(params='M/1000', xmin=20, xmax=120, xbin=20, xlabel='M [GeV]').draw()
  # H(params='DOCACHI2', xlog=True, xlabel='DOCA #chi^{2}').draw()
  H(params='APT', xmin=0., xmax=1., xlabel='A_{PT}', normalize=False).draw()

#===============================================================================
# APPENDICES GALLERY
#===============================================================================

def draw_gallery1_h3_quality():
  assert 'h3' in dt

  filt  = 'Preselection'
  trees = trimmed_trees(filt)
  H = QHist(trees=trees, prefix=dt, auto_name=True)

  H(params='tau_VCHI2PDOF', xlog=True, xmin=1E-2, xmax=1E3, xlabel='Vertex #chi^{2}/ndf').draw()
  H(params='tau_DRoPT', xmin=0, xmax=0.03, xlabel='#DeltaR_{max} / p_{T} [GeV^{-1}]').draw()
  H(params='tau_DRtPT', xmin=0, xmax=7, xlabel='#DeltaR_{max} #scale[0.5]{#bullet} p_{T} [GeV]').draw()
  H(params='tau_DRTRIOMAX', xmin=0, xmax=0.5, xlabel='#DeltaR_{max}').draw()

  ## Show corr: before & after
  H(params='tau_DRTRIOMAX:tau_PT/1000', options='cont3',
    xbin=20, ybin=20, zbin=4, xmin=12, xmax=50, ymin=4e-2, ymax=0.5,
    xlog=True, ylog=True, xlabel='p_{T} [GeV]', ylabel='#DeltaR_{max}').draw()

  H(params='tau_DRoPT:tau_DRtPT', options='cont3',
    xbin=20, ybin=20, zbin=4, xmin=0.5, xmax=6, ymax=0.05, ymin=5e-4, ylog=True, 
    xlabel='#DeltaR_{max} #scale[0.5]{#bullet} p_{T} [GeV]', 
    ylabel='#DeltaR_{max} / p_{T} [GeV^{-1}]').draw()

  ## mass
  H(params='tau_M:tau_DRoPT', options='cont3', 
    xbin=10, xmin=0, xmax=0.03, ybin=10, ymin=700, ymax=1500, zbin=6).draw()

  ## Try
  H(name='h3mu_temp', 
    params='TMath::Log10(tau_DRTRIOMAX):TMath::Log10(tau_PT/1000)', 
    options='cont3', 
    xbin=20, ybin=20, zbin=4, xlog=False, ylog=False, 
    xmin=1., xmax=2., ymin=-1.5, ymax=0).draw()

#===============================================================================

def draw_gallery2_iso():
  filt  = utils.join('Preselection', '_Prongs')
  trees = trimmed_trees(filt)

  H = QHist(prefix=dt, auto_name=True, ylog=True, xmin=0, xmax=1.1, xbin=55)
  for prefix,label in zip([id_utils.PREFIXES[dt], id_utils.LABELS[dt]]):
    h = H(params='%s_0.50_cc_IT'%prefix, xlabel='#hat{I}(#tau_{%s})'%label)
    h.trees = trees[prefix] if dt=='emu' else trees
    h.draw()

#===============================================================================

def draw_gallery3_displ_h3():
  assert 'h3' in dt

  filt = utils.join('Preselection', '_Prongs')
  # filt = utils.join('Preselection', '_Prongs', '_Iso')
  trees = trimmed_trees(filt)
  H = QHist(prefix=dt, suffix='noiso', auto_name=True, trees=trees)

  H(params='tau_BPVLTIME*1E6', xlog=True , xmin=1, xmax=1e4, xbin=40, xlabel='Decay time [fs]').draw()
  H(params='tau_BPVCORRM/1e3', xlog=False, xmin=0, xmax=5  , xbin=25, xlabel='m_{corr} [GeV]').draw()
  H(params='tau_BPVVD', xlog=True , xmin=1e-1, xmax=1e3, xbin=40, xlabel='d_{flight} [mm]').draw()

  ## withiso
  H(params='tau_VDoP', xlog=True, xmin=1e-4, xmax=1e0, xbin=40, 
    xlabel='d_{flight}/P [mm/GeV]').draw()

  ## Show corr: before & after
  H(params='tau_BPVVD:tau_P/1000', options='cont3',
    xbin=10, ybin=10, zbin=3, xmin=90, xmax=2e3, ymin=2e-1, ymax=2e2, 
    xlog=True, ylog=True, xlabel='p [GeV]', ylabel='d_{flight} [mm]').draw()

#===============================================================================

def draw_gallery3_displ():
  filt  = utils.join('Preselection', '_Prongs', '_Iso')
  trees = trimmed_trees(filt)
  H = QHist(prefix=dt, auto_name=True)

  # gStyle.SetPalette(ROOT.kTemperatureMap)

  for p,l in zip(id_utils.PREFIXES[dt], id_utils.LABELS[dt]):
    if p != 'tau':

      ## normal
      H(params='{}_BPVIP'.format(p), 
        xmin=3e-4, xmax=1e1, xbin=30, xlog=True, 
        xlabel='IP(#tau_{%s}) [mm]'%l,
        trees = (trees[p] if dt=='emu' else trees)).draw()

      ## against DOCACHI2
      H(params='DOCACHI2: {}_BPVIP'.format(p), 
        xmin=1e-3, xmax=1, xlog=True,
        ymin=1e-2, ymax=1e3, ylog=True,
        xbin=8, ybin=8, zbin=5, options='cont3').draw()

    # ## Decorrelated with PT
    # h = QHist()
    # h.params = '{0}_BPVIP*{0}_PT/1000'.format(p)
    # # h.xmin   = 1E-5
    # # h.xmax   = 1E0
    # h.xbin   = 30
    # h.xlog   = True
    # h.xlabel = 'IP(#tau_{%s}) [mm]'%l
    # if dt=='emu': h.trees = trees[p]
    # h.draw()

    # ## Correlation: PT
    # QHist(params='{0}_BPVIP: {0}_PT/1000'.format(p),
    #       xlog=True, ylog=True, zlog=True, options='colz',
    #       trees = (trees[p][0] if dt=='emu' else trees[0])).draw()

#===============================================================================

def draw_gallery4_ditau():
  filt  = utils.join('Preselection', '_Prongs', '_Iso', '_Displ')
  trees = trimmed_trees(filt)
  H = QHist(prefix=dt, auto_name=True, xbin=30)
  H.trees = trees['mu'] if dt=='emu' else trees

  H(params='DPHI'    , xmin=0   , xmax=3.4, ymax=0.4 , xlabel='\Delta\phi [rad]').draw()
  H(params='DOCACHI2', xmin=1e-4, xmax=1e5, ymax=0.25, xlabel='DOCA #chi^{2}').draw()
  H(params='APT'     , xmin=0   , xmax=1. , ymax=0.25, xlabel='A_{PT}').draw()

#===============================================================================

def figure_jhep():
  """
  Figure requested by JHEP
  """
  # ## Cache trees
  # filt = utils.join('Preselection', '_Prongs')
  # from ditau_prep import t_real
  # t2 = t_real.drop(filt, ['mu_PT'], [False])
  # t2.name = t2.title = 't_real'
  # trees = trimmed_trees(filt)
  # for i,name in enumerate(['t_ztautau', 't_wmu17jet', 't_dymu', 't_ccbar', 't_bbbar']):
  #   trees[i].name = trees[i].title = name
  # trees.append(t2)
  # ROOT.TFile.export_trees(trees, target='gallery/figure_jhep.root')
  # return

  ## Load & draw
  fin = ROOT.TFile('gallery/figure_jhep.root')
  t_ztautau  = fin.Get('t_ztautau')
  t_wmu17jet = fin.Get('t_wmu17jet')
  # # t_dymu     = fin.Get('DitauCandTupleWriter/42112011/h3_mu_trimmed').SetTitle('Z->mumu')
  # # t_wmu17jet = fin.Get('DitauCandTupleWriter/42311011/h3_mu_trimmed').SetTitle('W+jet')
  # # t_ccbar    = fin.Get('DitauCandTupleWriter/49011004/h3_mu_trimmed').SetTitle('ccbar')
  # # t_bbbar    = fin.Get('DitauCandTupleWriter/49011005/h3_mu_trimmed').SetTitle('bbbar')

  # t_real = fin.Get('t_real')
  ## Load data tree, after all selection
  fin2 = ROOT.TFile('selected/selected.root')
  t_real = fin2.Get('h3mu/t_real')

  # QHist(prefix='h3mu', suffix='noiso', auto_name=True, 
  #   trees=[t_ztautau], params=['tau_BPVCORRM/1e3', 'tau_M/1e3'],
  #   xmin=0.7, xmax=3.0).draw()

  def postpc(h):
    # ## Draw tau lepton mass marker
    # m_tau = 1.77682
    # line = drawers.Line([(m_tau, 0.), (m_tau, 0.3)])
    # line.lineColor = ROOT.kGray+1
    # line.lineStyle = 3
    # line.lineWidth = 2
    # line.Draw()

    ## Adjust style
    h[0].lineColor = ROOT.kBlue
    h[1].lineColor = ROOT.kBlue
    h[2].lineColor = ROOT.kRed
    h[3].lineColor = ROOT.kRed
    h[0].lineStyle = 2
    h[1].lineStyle = 1
    h[2].lineStyle = 2
    h[3].lineStyle = 1
    h[1].fillStyle = 3004
    h[1].fillColor = ROOT.kBlue
    h[3].fillStyle = 3005
    h[3].fillColor = ROOT.kRed
    #
    h[4].lineStyle   = 2
    h[4].lineWidth   = 2
    h[4].lineColor   = ROOT.kGray+2
    h[4].markerColor = ROOT.kGray+2
    h[4].markerStyle = 20
    h[4].markerSize  = 1.0
    #
    h[5].lineStyle   = 1
    h[5].lineWidth   = 2
    h[5].lineColor   = ROOT.kBlack
    h[5].markerColor = ROOT.kBlack
    h[5].markerStyle = 20
    h[5].markerSize  = 1.0

    ## Adjust legend
    leg = h.anchors.legend
    leg.header = 'LHCb#sqrt{#it{s}} = 8 TeV'
    leg.x1NDC = 0.60
    leg.x2NDC = 0.93
    leg.y1NDC = 0.55
    leg.y2NDC = 0.92
    leg.Draw()

    ## Finally
    ROOT.gPad.RedrawAxis()

  QHist(name='jhep_mcorr',
    tpc = [
      (t_ztautau , 'tau_M/1e3'       , ''),
      (t_ztautau , 'tau_BPVCORRM/1e3', ''),
      (t_wmu17jet, 'tau_M/1e3'       , ''),
      (t_wmu17jet, 'tau_BPVCORRM/1e3', ''),
      (t_real    , 'tau_M/1e3'       , ''),
      (t_real    , 'tau_BPVCORRM/1e3', ''),
    ], 
    legends = [
      PROCESS_ROOTLABELS['Ztau'],
      PROCESS_ROOTLABELS['Ztau']+' (with #it{m}_{corr})',
      PROCESS_ROOTLABELS['EWK']+' #times 10',
      PROCESS_ROOTLABELS['EWK']+' #times 10 (with #it{m}_{corr})',
      # PROCESS_ROOTLABELS['EWK']+' #times 10',
      # PROCESS_ROOTLABELS['EWK']+' #times 10 (with #it{m}_{corr})',
      'Data',
      'Data (with #it{m}_{corr})',
    ], 
    normalize = [
      163.9,               # 3. assume final N_exp of Ztautau
      163.9*6328/6126,     # 4. scale such that area [0,3GeV] is same as above
      5.1*1507/1308 * 10., # 6. Magnify 10x
      5.1*1507/1308 * 10., # 5. Magnify 10x
      205,    # 1. assume final N_obs 
      205,    # 2. follow above
    ],
    options = ['H','H','H','H','P','P'],
    xlabel = '#it{m}(%s) [GeV/#it{c}^{2}]'%LABELS_TAU['h3'],
    ylabel = 'Candidates / (0.2 GeV/#it{c}^{2})',
    save_types = ['pdf', 'C', 'png'],
    xmin=0, xmax=3.4, xbin=17, postproc=postpc).draw()

  ## MCORR Normalization note [0, X]
  #              3GeV  3.4GeV  4.0GeV  Total
  # Ztautau      5909  6126    6328    6824
  # Vj           1308  1507    1768    2974

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  set_qhist_color_palette()

  # prelim()
  # draw_gallery1_h3_quality()
  # draw_gallery2_iso()
  # draw_gallery3_displ_h3()
  # draw_gallery3_displ()
  # draw_gallery4_ditau()
  figure_jhep()
