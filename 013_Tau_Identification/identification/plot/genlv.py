#!/usr/bin/env python

"""
Generator-level study
"""

import sys
from PyrootCK import *

ROOT.gROOT.ProcessLine('.L lhcbStyle.C')
ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors


## Ditau generator-level
t_ditau   = import_tree( 'DitauDebugger/Ditau', 2938 )

QHist.trees = t_ditau

h = QHist()
h.name    = 'ditau_MCPT_MCETA'
h.params  = 'ditau_MCETA: ditau_MCPT/1000'
h.xlog    = True
h.zlog    = True
h.xlabel  = '\pt(Z) [\gevc]'
h.ylabel  = '\eta(Z)'
h.options = 'colz'
# h.draw()

h = QHist()
h.name    = 'tau_MCETA'
h.params  = 'tau1top_MCETA:tau2top_MCETA'
h.options = 'colz'
h.xlabel  = '\eta(\tau_{1})'
h.ylabel  = '\eta(\tau_{2})'
h.zlog    = True
# h.draw()

h = QHist()
h.name    = 'tau_MCPT'
h.params  = 'tau1top_MCPT/1000:tau2top_MCPT/1000'
h.options = 'colz'
h.xlog    = True
h.ylog    = True
h.zlog    = True
h.xlabel  = '\pt(\tau_{1}) [\gevc]'
h.ylabel  = '\pt(\tau_{2}) [\gevc]'
# h.draw()

h = QHist()
h.name    = 'tau_MCPT_MCETA'
h.params  = 'tau1top_MCETA: tau1top_MCPT/1000'  # one tau is sufficient
h.xlog    = True
h.zlog    = True
h.xlabel  = '\pt(\tau) [\gevc]'
h.ylabel  = '\eta(\tau)'
h.options = 'colz'
# h.draw()

h = QHist()
h.name    = 'DR_tau1_tau2'
h.params  = 'DR'  # DR between two-taus of same ditau
# h.draw()


# raw_input(); sys.exit()

#==============================================================

QHist.reset()

## RecoStats
t_e  = import_tree( 'TauRecoStats/TauReco_e' , 2934 )
t_mu = import_tree( 'TauRecoStats/TauReco_mu', 2934 )
t_h1 = import_tree( 'TauRecoStats/TauReco_h1', 2934 )
t_h3 = import_tree( 'TauRecoStats/TauReco_h3', 2934 )

t_e.SetAlias ('INACC', 'MCETA_charged>2.0  && MCETA_charged<4.5 ')
t_mu.SetAlias('INACC', 'MCETA_charged>2.0  && MCETA_charged<4.5 ')
t_h1.SetAlias('INACC', 'MCETA_charged>2.25 && MCETA_charged<3.75')
t_h3.SetAlias('INACC', 'MCETA_charged>2.25 && MCETA_charged<3.75')


## For h3 only: individual child INACC count
h = QHist()
h.name    = 'tauh3_count_child_INACC'
h.trees   = t_h3
h.params  = [ 'count_child_INACC==%i : MCPT/1000'%i for i in (0,1,2,3) ]
h.xmin    = 4
h.xmax    = 100
h.ymin    = 0
h.ymax    = 1
h.xlog    = True
h.xlabel  = '\pt(\tau_{h3}) [\gevc]'
h.ylabel  = 'Fraction'
h.legends = 'No prong in acceptance', '1 prong in acceptance', '2 prongs in acceptance', '3 prongs in acceptance'
h.options = 'prof'
# h.draw()



## For all 4 types

#--------------------#
# ACCeptance profile #
#--------------------#


QHist.reset()
QHist.trees   = t_e, t_mu, t_h1, t_h3
QHist.options = 'prof'
QHist.xmin    = 4
QHist.xmax    = 1E2
QHist.ymin    = 0.
QHist.ymax    = 1.
QHist.xlog    = True
QHist.legends = '\tau_{e}', '\tau_{\mu}', '\tau_{h1}', '\tau_{h3}'


h = QHist()
h.name    = 'acceptance'
h.params  = 'INACC: MCPT/1000'
h.xlabel  = '\pt(\tau) [\gevc]'
h.ylabel  = 'Fraction'
h.xmax    = 1E2
# h.draw()


#-------
QHist.master_cuts = 'INACC'
#-------


#----#
# PT #
#----#

h = QHist()
h.name    = 'PTquality_bymom'
h.params  = 'MCPT_charged/MCPT : MCPT/1000'
h.xlabel  = 'MCPT (tau) [GeV]'
h.ylabel  = 'MCPT(children) / MCPT(tau)'
# h.draw()

h = QHist()
h.name    = 'neuloss_tau_PT'
h.params  = 'MCPT_charged/MCPT : MCPT_charged/1000'
h.xlabel  = '\pt(\hat{\tau}) [\gevc]'
h.ylabel  = '\pt(\hat{\tau}) / \pt(\tau)'
# h.draw()

# raw_input(); sys.exit(0)

#----#
# DR #
#----#

h = QHist()
h.name    = 'DR_bymom'
h.params  = '((TMath::Abs(TMath::Abs(MCPHI-MCPHI_charged)-pi)-pi)**2 + (MCETA-MCETA_charged)**2)**0.5 : MCPT/1000'
h.xlabel  = 'MCPT (tau) [GeV]'
h.ylabel  = 'DR(tau,children)'
# h.draw()

h = QHist()
h.name    = 'neuloss_tau_DR'
h.params  = '((TMath::Abs(TMath::Abs(MCPHI-MCPHI_charged)-pi)-pi)**2 + (MCETA-MCETA_charged)**2)**0.5 : MCPT_charged/1000'
h.xlabel  = '\pt(\hat{\tau}) [\gevc]'
h.ylabel  = '\Delta R_{\eta\phi}(\hat{\tau},\tau)'
h.ymax    = 0.12
# h.draw()

# raw_input(); sys.exit(0)

#-------------#
# Neuloss: MM #
#-------------#

QHist.reset()

# 1-pronger, 3-prongers
t_ditau.SetAlias('ditau_type_1p1p', 'ditau_itype!=2 & ditau_itype!=12 & ditau_itype!=22 & ditau_itype!=23')
t_ditau.SetAlias('ditau_type_1p3p', 'ditau_itype==2 | ditau_itype==12 | ditau_itype==23') # eh3, h1h3, h3mu
t_ditau.SetAlias('ditau_type_3p3p', 'ditau_itype==22')  # h3h3
# t_ditau.SetAlias('ditau_type_h3X', 'ditau_itype==2 | ditau_itype==12 | ditau_itype==22 | ditau_itype==23')

## >> Workaround since I don't have mass in RecoStats
## Only one is plenty here.
h = QHist()
h.master_cuts = 'tau1bot_MCETA>2.25', 'tau1bot_MCETA<3.75'
h.name    = 'neuloss_tauh3_PT_MM'
h.trees   = t_ditau
h.params  = 'tau1ch_MM/tau1bot_MCM : tau1ch_PT/1E3'
h.cuts    = 'tau1_itype==2'
h.options = 'colz'
h.xlog    = True 
h.zlog    = True
h.xmin    = 1.
h.ymin    = 0.
h.ymax    = 1.
h.xlabel  = '\pt(\hat{\tau}) [\gevc]'
h.ylabel  = 'm(\hat{\tau}) / m(\tau) [\gevcc]'
h.draw()

tau1p = '(\tau_{e}/\tau_{\mu}/\tau_{h1})'
tau3p = '\tau_{h3}'

h = QHist()
h.master_cuts = 'tau1bot_MCETA>2.25 & tau1bot_MCETA<3.75' # Skewed, incorrect for e/mu/h1
h.name    = 'neuloss_ditau_MM'
h.trees   = t_ditau
h.params  = 'ditau_ch_MM/1000'
# h.cuts   = [ 'ditau_itype==%i'%i for i in (0,1,2,3,11,12,13,22,23,33) ]  # All
# h.cuts   = [ 'ditau_itype==%i'%i for i in (11,33) ]  # h1h3, h3mu
h.cuts    = 'ditau_type_1p1p', 'ditau_type_1p3p', 'ditau_type_3p3p'
# h.cuts   = 'ditau_itype == 23'
h.xlog    = False
h.xmin    = 0
h.xmax    = 140
h.legends = tau1p+'\oplus'+tau1p, tau1p+'\oplus'+tau3p, tau3p+'\oplus'+tau3p
h.xlabel  = 'm(\hat{\tau}\hat{\tau}) [\gevcc]'
# h.draw()

# raw_input(); sys.exit(0)


#---------#
# H3 Cone #
#---------#

t_h3cone  = import_tree( 'TauH3ConeDebugger/tau_h3', 2938 )

QHist.reset()
QHist.trees   = t_h3cone
QHist.cuts    = '2.25 < MCETA_charged & MCETA_charged < 3.75'
QHist.legends = 'min(\DeltaR)', 'mid(\DeltaR)', 'max(\DeltaR)'

DRTRIO = 'MCDR_min', 'MCDR_mid', 'MCDR_max'

h = QHist()
h.name    = 'tauh3_DR'
h.params  = DRTRIO
h.xmin    = 0
h.xmax    = 0.4
# h.ylog    = True
h.xlabel  = '\DeltaR_{\eta\phi}'
h.ylabel  = 'Fraction'
# h.draw()

h = QHist()
h.name    = 'tauh3_DR_PT'
h.params  = ['%s:MCPT_charged/1000'%s for s in DRTRIO]
h.options = 'cont3'
h.xmin    = 9
h.xmax    = 1E2
h.ymin    = 0
h.ymax    = 0.2
h.xbin    = 12
h.ybin    = 12
h.zbin    = 6
h.xlog    = True
h.xlabel  = '\pt(\hat{\tau})'
h.ylabel  = '\DeltaR_{\eta\phi}'
# h.draw()

raw_input()