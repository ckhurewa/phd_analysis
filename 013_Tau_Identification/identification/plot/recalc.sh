#!/usr/bin/env bash

# ## cleanup
# rm -rf */*.df
# rm -rf selected

# ## Rerun selected to save selected tree to ROOT
# ./selected.py mumu --recalc &
# ./selected.py h1mu --recalc &
# ./selected.py h3mu --recalc &
# ./selected.py ee --recalc &
# ./selected.py eh1 --recalc &
# ./selected.py eh3 --recalc &
# ./selected.py emu --recalc &
# wait

# ## Rerun context to get various numbers
# ssh lphe01 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc mumu' &
# ssh lphe02 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc h1mu' &
# ssh lphe03 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc h3mu' &
# ssh lphe04 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc ee' &
# ssh lphe05 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc eh1' &
# ssh lphe06 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc eh3' &
# ssh lphe07 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc emu' &
# wait

# ## Zll background
# ./bkg_zll.py --recalc

# ## Samesign background
# ./bkg_samesign.py --recalc

# ## Summary of backgrounds
# ./candidates.py --recalc

#---------

## Mass-window-only recalc
rm -vrf bkg_context/*mass*.df
rm -vrf bkg_context/check_ztau_seleff.df
rm -vrf candidates/*.df

# ## Rerun context to get various numbers
# ssh lphe01 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc mumu' &
# ssh lphe02 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc h1mu' &
# ssh lphe03 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc h3mu' &
# ssh lphe04 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc ee' &
# ssh lphe05 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc eh1' &
# ssh lphe06 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc eh3' &
# ssh lphe07 'source ~/.login; source ~/.tcshrc; cd ~/analysis/013_Tau_Identification/identification/plot; ./bkg_context.py --recalc emu' &
# wait

./candidates.py --recalc
