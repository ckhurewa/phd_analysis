#!/usr/bin/env python

from PyrootCK import *
import qhist_postprocessors as postprocessors
import id_utils
import selected
import candidates
from id_utils import pd, CHANNELS, DT_TO_LATEX, LABELS_DITAU, pickle_dataframe_nondynamic


#===============================================================================
# DEBUG VARBIN CHI2
#===============================================================================

def _export_fit_tables(dt, xmin):
  """
  For debugging & discussion with Aurelio.
  """
  ## Load list of trees
  trees = load_trees(dt)

  ## Bin them into usual mass range
  h = QHist()
  h.filters   = 'Sel_OScomb' # derivative of 'Selection' to allow no-iso EWK
  h.trees     = trees
  h.params    = 'M/1e3'
  h.xmin      = xmin
  h.xmax      = 120
  h.xbin      = (120-xmin)
  h.normalize = False
  h.batch     = True
  h.draw()

  ## Extract sum of weights to dataframe
  df = pd.DataFrame()
  for h2,t in zip(h, trees):
    df[t.name] = h2.series()

  ## Get the expected number 
  sys.path.append('../../efficiencies')
  import candidates
  se_exp = candidates.results()[dt]

  ## Multiply out the bin-fractional expected values
  def mult(se):
    df2 = pd.DataFrame([se, se_exp]) # align same columns
    del df2['BKG']
    se2 = df2.apply(pd.np.prod)
    obs = se2['OS']
    del se2['OS']
    exp = se2.sum()
    return pd.Series({'obs':obs, 'exp':exp})
  
  se_total  = df.sum()
  df_result = (df/se_total).apply(mult, axis=1)

  ## Compute Goodness-of-Fit
  n    = len(df_result)
  funcs = {
    # '1_expected' : lambda se: (se.obs.n-se.exp.n)**2/se.exp.n,
    '2_stat_only': lambda se: (se.obs.n-se.exp.n)**2/(se.obs.s**2),
    # '3_syst_only': lambda se: (se.obs.n-se.exp.n)**2/(se.exp.s**2/n),
    '4_stat_syst': lambda se: (se.obs.n-se.exp.n)**2/(se.obs.s**2+se.exp.s**2/n),
  }
  print
  print df_result
  print 
  print 'CHI2 / NDOF'
  for name,func in sorted(funcs.iteritems()):
    chi2 = df_result.apply(func, axis=1).sum()
    print '{:10} ({:.3f}) / {} = {:.3f}'.format(name, chi2, n, chi2/n)


#===============================================================================
# COMB WITH VARIABLE BIN WIDTH
#===============================================================================

# @pickle_dataframe_nondynamic
def draw_varbin(dt):
  """
  Draw the mass combination plot with variable binning.
  """
  import ditau_cuts
  ROOT.gROOT.SetBatch(True)

  ## Prepare inputs
  trees    = selected.load_selected_trees_comb(dt)
  res_raw  = candidates.results_comb_withmasswindow()[dt]
  entries  = [ res_raw[t.name] for t in trees] # get expected value for each tree
  xmax     = min(ditau_cuts.UPPER_MASS[dt], 120) # cap at 120 for figure

  ## Prepare struct
  h = QHist()
  h.name      = 'varbin_'+dt
  h.trees     = trees
  h.params    = 'mass: weight'
  h.normalize = entries
  h.xmin      = 30 if ('h1' in dt or 'h3' in dt) else 20
  h.xmax      = xmax
  h.xlog      = True
  h.xlabel    = 'm(%s) [GeV/c^{2}]'%LABELS_DITAU[dt]
  h.ylabel    = 'Candidates'

  ## manual fix nbins in case of low MC stats
  if dt=='ee':
    h.xbin = 12

  ## Finally, run the engine, add more result, persist
  res = postprocessors.variable_binning(h)
  res['nobs'] = entries[0]
  res['nsig'] = entries[1]
  return pd.Series(res)

#-------------------------------------------------------------------------------

def latex_stats():
  """
  Show fit statistics, with chi2/ndf
  """ 
  ## Load & add fields
  df0 = id_utils.df_from_path('draw_varbin', __file__).unstack()
  df  = pd.DataFrame()
  df0['nbkg']             = df0['nobs']-df0['nsig']
  df['ndf']               = df0['ndf'].apply(lambda x: '$%i$'%x)
  df['\\chisq/ndf']       = df0['chi2pdof']
  df['$S/B$']             = df0.apply(lambda se: se.nsig.n / se.nbkg.n, axis=1)
  df['$S/\\sqrt{(S+B)}$'] = df0.apply(lambda se: se.nsig.n / se.nobs.n**0.5, axis=1)

  ## Pick out
  df = df.T[CHANNELS].rename(columns=DT_TO_LATEX)
  return df.fmt2f.to_markdown(floatfmt='.2f', stralign='right')

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    ## For consistent style
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
    for dt in CHANNELS:
      draw_varbin(dt)
    sys.exit()

  ## LATEX
  # print latex_stats()
