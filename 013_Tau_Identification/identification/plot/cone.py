#!/usr/bin/env python

from PyrootCK import import_tree, QHist, ROOT

ROOT.gROOT.SetBatch(True)

#----------#
# H3: CONE #
#----------#

# t = import_tree(2573, 'SelTauCand/tau_h3_cand', title='H2AA') # H2AA
# t = import_tree(2574, 'SelTauCand/tau_h3_cand', title='H2AA') # Z0

t = import_tree( 2752, 'TauCandTupleWriter/LooseTauCand_h3' ) # 13TeV

QHist.trees = t
QHist.master_cuts = 'EVTYPE==43900080'
QHist.cuts        = ( 'MATCH', '!MATCH' )
QHist.legends     = ( 'True', 'FakeComb' ) 
QHist.prefix      = 'H2AA_13TeV_tau_h3'
QHist.auto_save   = True

# for name,p in {
#   'EFrac05A'    : 'EFrac_ProngsOnly__PFALL',
#   'EFrac05C'    : 'EFrac_ProngsOnly__PFCHARGED',
#   'EFrac02PA05A': 'EFrac_ProngsWithCone02__PFALL',
#   'EFrac02PA05C': 'EFrac_ProngsWithCone02__PFCHARGED',
#   'EFrac02PN05A': 'EFrac_ProngsWithNeutralCone02__PFALL',
#   'EFrac02PN05C': 'EFrac_ProngsWithNeutralCone02__PFCHARGED',
#   'EFrac0PA05A' : 'EFrac_ProngsWithTightCone__PFALL',
#   'EFrac0PA05C' : 'EFrac_ProngsWithTightCone__PFCHARGED',
#   'EFrac0PN05A' : 'EFrac_ProngsWithTightNeutralCone__PFALL',
#   'EFrac0PN05C' : 'EFrac_ProngsWithTightNeutralCone__PFCHARGED',
# }.iteritems():
#   h = QHist()
#   h.params = p
#   h.name   = name
#   h.xmin   = 0
#   h.xmax   = 1
#   # h.draw()

## Normal cone
# for p in ('DR_min', 'DR_mid', 'DR_max', 'DR_mean'):
for p in ('DRTRIOMIN', 'DRTRIOMID', 'DRTRIOMAX' ):
  h = QHist()
  h.name    = p
  h.params  = p+':PT'
  h.options = 'CONT1Z'
  h.xmin    = 0
  h.xmax    = 50000
  h.xbin    = 15
  h.ymin    = 0.
  h.ymax    = 0.6
  h.ybin    = 15
  h.xlabel  = 'PT (3 reco-prongs)'
  h.ylabel  = p
  h.draw()

  h = QHist()
  h.name   = p+'/PT'
  h.params = p+'/PT' 
  h.xmin   = 1E-7
  h.draw()


# ## Invert Cone
# h = QHist()
# h.name    = 'InvertCone_EF70'
# h.cuts    = ( 'MATCH', '!MATCH' )
# h.params  = "EFrac70_DR:recotau_PT"
# h.options = 'CONT1Z'
# # h.options = 'COLZ'
# h.xmin    = 0
# h.xmax    = 40000
# h.xbin    = 12
# h.ybin    = 10
# # h.xlog    = True
# h.ymax    = 2
# h.xlabel  = 'PT (3 reco-prongs)'
# h.ylabel  = 'DR'
# h.comment = 'Question: What is the DR-radius required for a cone to satisfied EFracC > 70%'
# h.draw()



# raw_input()