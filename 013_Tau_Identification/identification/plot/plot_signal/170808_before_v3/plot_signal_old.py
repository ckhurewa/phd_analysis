#!/usr/bin/env python

from PyrootCK import * 

import ditau_cuts
import id_utils
import selected
import candidates
from id_utils import pd, CHANNELS, LABELS_DITAU

#===============================================================================
# COMBINE PLOTS
#===============================================================================

def draw_combine(dt):
  ## Prep data
  trees    = selected.load_selected_trees_comb(dt)
  res_raw  = candidates.results_comb_nomasswindow()[dt]
  ent_nomw = [ res_raw[t.name].n for t in trees] # get ordered expected value for each tree
  res_raw2 = candidates.results_comb_withmasswindow()[dt]
  ent_wmw  = [ res_raw2[t.name].n for t in trees]
  prefixes = id_utils.PREFIXES[dt]
  l1,l2    = id_utils.LABELS[dt]

  ## Mass window cut
  cut_mass_window = 'mass < %i'%ditau_cuts.UPPER_MASS[dt]

  ## Prep histo template
  QHist.reset()
  QHist.filters   = 'weight'         # for new regime
  QHist.cuts      = cut_mass_window  # put here, allow sub-override'
  QHist.prefix    = dt
  QHist.trees     = trees
  QHist.normalize = ent_wmw  # By default, has a mass window
  QHist.st_code   = 2
  QHist.xbin      = 20
  QHist.ylabel    = 'Candidates'
  QHist.batch     = True
  QHist.auto_name = True
  QHist.ymin      = 1e-3 # To remove zero hairline

  #------------------------#
  # WITHOUT WINDOW (debug) #
  #------------------------#

  # ## Base mass plot, full range (no mass window)
  # h = QHist()
  # h.name      = 'Mnowin'
  # h.params    = 'mass'
  # h.cuts      = ''
  # h.normalize = ent_nomw
  # h.xmin      = 20
  # h.xmax      = 120
  # h.xbin      = 20
  # h.xlabel    = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # h.ylabel    = 'Candidates / (5 GeV)'
  # h.draw()

  # ## more mass bins, also no mass window
  # h = QHist()
  # h.name      = 'Mnowin50'
  # h.params    = 'mass'
  # h.cuts      = ''
  # h.normalize = ent_nomw
  # h.xmin      = 20
  # h.xmax      = 120
  # h.xbin      = 50
  # h.xlabel    = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # h.ylabel    = 'Candidates / (2 GeV)'
  # h.draw()

  # ## Zoomed version, only needed for mumu
  # if dt == 'mumu':
  #   h = QHist()
  #   h.name      = 'Mzoom'
  #   h.params    = 'mass'
  #   h.cuts      = ''
  #   h.normalize = ent_nomw
  #   h.xmin      = 20
  #   h.xmax      = 120
  #   h.xbin      = 20
  #   h.ymax      = 146  # <--- CONFIGURE ME
  #   h.xlabel    = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  #   h.ylabel    = 'Candidates / (5 GeV)'
  #   h.draw()

  # ## Log scale
  # h = QHist()
  # h.name      = 'MnowinLog'
  # h.params    = 'mass'
  # h.cuts      = ''
  # h.normalize = ent_nomw
  # h.xmin      = 20
  # h.xmax      = 120
  # h.xbin      = 20
  # h.ymin      = 1
  # h.ylog      = True
  # h.xlabel    = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # h.ylabel    = 'Candidates / (5 GeV)'
  # h.draw()

  #-----------------------#
  # WITH WINDOW (default) #
  #-----------------------#

  # h = QHist()
  # h.name      = 'Mwin'
  # h.params    = 'mass'
  # h.xmin      = 20
  # h.xmax      = 120
  # h.xbin      = 20
  # h.xlabel    = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # h.ylabel    = 'Candidates / (5 GeV)'
  # h.draw()

  # ## Log scale
  # h = QHist()
  # h.name    = 'Mlog'
  # h.params  = 'mass'
  # h.xmin    = 20
  # h.xmax    = 120
  # h.xbin    = 20
  # h.ymin    = 1
  # h.ylog    = True
  # h.xlabel  = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # h.ylabel  = 'Candidates / (5 GeV)'
  # h.draw()

  # ## At 50 bins
  # h = QHist()
  # h.name      = 'Mwin50'
  # h.params    = 'mass'
  # h.xmin      = 20
  # h.xmax      = 120
  # h.xbin      = 50
  # h.xlabel    = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # h.ylabel    = 'Candidates / (2 GeV)'
  # h.draw()

  ## Mother
  # QHist().draw(params='P/1e3' , xmin=90 , xmax=3e3, xlog=True, xlabel='p [GeV]')
  # QHist().draw(params='PT/1e3', xmin=1  , xmax=500, xlog=True, xlabel='p_{T} [GeV]')
  # QHist().draw(params='APT'   , xmin=0.0, xmax=1.0,            xlabel='A_{pT}')
  # QHist().draw(params='ETA'   , xmin=2.0, xmax=8.0, xbin=24,   xlabel='#eta')
  # QHist().draw(params='Y'     , xmin=1.8, xmax=4.6, xbin=14)

  ## Children
  # minpt = 5 if dt in ('ee','emu','mumu') else 10
  # QHist().draw(params='PT1' , xmin=20   , xmax=100, xlog=True, xlabel='p_{T}(#tau_{%s})'%l1)
  # QHist().draw(params='PT2' , xmin=minpt, xmax=100, xlog=True, xlabel='p_{T}(#tau_{%s})'%l2)
  # QHist().draw(params='ETA1', xmin=2.0  , xmax=4.5, xbin=20  , xlabel='#eta(#tau_{%s})'%l1)
  # QHist().draw(params='ETA2', xmin=2.0  , xmax=4.5, xbin=20  , xlabel='#eta(#tau_{%s})'%l2)

  ## tauh3 only
  if 'h3' in dt:
    # QHist().draw(params='tau_VCHI2PDOF', xmin=0, xmax=20)
    # QHist().draw(params='tau_DRTRIOMAX', xmin=0, xmax=0.2)
    # QHist().draw(params='tau_DRoPT', xmin=0, xmax=0.01)
    # QHist().draw(params='tau_BPVCORRM/1e3', xmin=0.5, xmax=3.5)
    QHist().draw(params='tau_BPVLTIME*1e6', xmin=1e1, xmax=1e4, xlog=True)

  # ## Editting QHist again, use with care
  # ## Mass, bin=20
  # QHist.params = 'mass'
  # QHist.xmin   = 20
  # QHist.xmax   = 120
  # QHist.xbin   = 20
  # QHist.xlabel = 'm(%s) [GeV]'%LABELS_DITAU[dt]
  # QHist.ylabel = 'Candidates / (5 GeV)'
  # QHist().draw(name='Mwin')                                # With mass window, precursor for zoom version
  # QHist().draw(name='Mlog', ymin=1, ylog=True)             # Y-log scale
  # QHist().draw(name='Mnowin', cuts='', normalize=ent_nomw) # Base mass plot, full range (no mass window)
  # if dt == 'mumu': ## Zoomed version, only needed for mumu
  #   ymax = 146 # <--- CONFIGURE ME
  #   QHist().draw(name='Mzoom', cuts='', normalize=ent_nomw, ymax=ymax)

  # ## more mass bin
  # QHist.xbin   = 50
  # QHist.ylabel = 'Candidates / (2 GeV)'
  # QHist().draw(name='Mwin50')
  # QHist().draw(name='Mnowin50' , cuts='', normalize=ent_nomw) # more mass bins, also no mass window
  # QHist().draw(name='MnowinLog', cuts='', normalize=ent_nomw, ymin=1, ylog=True)


#-------------------------------------------------------------------------------

def draw_combine_ss(dt):
  """
  Like above, but for same-sign candidates
  """ 
  assert False, "Not sightly"

  ## Prep data
  trees    = id_utils.load_selected_trees_comb_ss(dt)
  res_raw  = candidates.results_comb_ss()[dt]
  entries  = [ res_raw[t.name].n for t in trees] # get expected value for each tree
  # prefixes = id_utils.PREFIXES[dt]
  # labels   = id_utils.LABELS[dt]

  ## Prep histo template
  QHist.reset()
  QHist.filters   = 'weight'  # for new regime
  QHist.prefix    = dt+'_combSS'
  QHist.trees     = trees
  QHist.normalize = entries
  QHist.st_code   = 2
  QHist.xbin      = 20
  QHist.ymin      = 1e-3 # To remove zero hairline
  QHist.ylabel    = 'Candidates'
  # QHist.batch     = True
  # QHist.auto_name = True  # to save 

  h = QHist()
  h.params  = 'M/1000'
  h.xmin    = 20
  h.xmax    = 120
  h.xbin    = 20
  h.xlabel  = 'M [GeV]'
  h.ylabel  = 'Candidates / (5 GeV)'
  h.draw()

  exit()


#===============================================================================
# FINALLY
#===============================================================================

if __name__ == '__main__':
  ## For consistent style
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  if '--recalc' in sys.argv:
    for dt in CHANNELS:
      draw_combine(dt)
    sys.exit()

  ## DEV
  draw_combine('h3mu')
  # draw_combine_ss(dt)

