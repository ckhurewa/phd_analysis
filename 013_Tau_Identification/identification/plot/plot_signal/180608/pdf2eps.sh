#!/bin/bash

gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/ee_Mnowin.eps   pdf/ee_Mnowin.pdf
gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/eh1_Mnowin.eps  pdf/eh1_Mnowin.pdf
gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/eh3_Mnowin.eps  pdf/eh3_Mnowin.pdf
gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/emu_Mnowin.eps  pdf/emu_Mnowin.pdf
gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/h1mu_Mnowin.eps pdf/h1mu_Mnowin.pdf
gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/h3mu_Mnowin.eps pdf/h3mu_Mnowin.pdf
gs -q -dNOCACHE -dNOPAUSE -dBATCH -dSAFER -sDEVICE=eps2write -sOutputFile=eps/mumu_Mnowin.eps pdf/mumu_Mnowin.pdf
