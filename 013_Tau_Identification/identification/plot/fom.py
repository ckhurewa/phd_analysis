#!/usr/bin/env python
"""

Handle the auxiliary study of mass window & its figure of merits.
Relies on results from candidates.py and selected.wmw_fraction

"""

from PyrootCK import *
from uncertainties import nominal_value
from id_utils import LABELS_DITAU, CHANNELS, memorized, pd, drawers, gpad_save
import selected
import candidates

#===============================================================================
# FIGURES OF MERITS & MASS WINDOWS
#===============================================================================

@memorized
def FoM_mass_windows():
  """
  The fast version where different massmax is tested.
  Several approximations are made to be this fast.
  Use for appendix and to reply Ronan.
  """
  ## Load data
  all_fracs = selected.fracs_wmw_mapped()
  df0 = candidates.results_nomasswindow().copy().drop(['Ztau', 'BKG'])

  ## Loop 
  acc = {}
  for mass, fracs in all_fracs.groupby(level=0):
    df     = fracs.loc[mass].T * df0
    se_obs = df.loc['OS']
    df     = df.drop('OS')
    se_bkg = df.sum()
    res    = pd.concat({'OBS': se_obs, 'BKG': se_bkg}).unstack().T
    res['SIG']       = res.OBS - res.BKG
    res['S/B']       = res.SIG / res.BKG
    res['S/sqrt(N)'] = res.SIG / res.OBS**0.5
    acc[mass] = res.loc[list(CHANNELS)]
  return pd.concat(acc)


def compare_FoM():
  """
  Compare the figure of merit (S/sqrt(S+B), S/B) with/without mass window.
  """
  funcs = {
    '$S/B$'          : lambda se: se.Ztau/se.BKG,
    '$S/\\sqrt{S+B}$': lambda se: se.Ztau/(se.OS**0.5),
  }
  datasets = {
    'no_upper_mass'  : results_nomasswindow(),
    'with_upper_mass': results_withmasswindow(),
  }
  acc = {}
  for fname, func in funcs.iteritems():
    acc1 = pd.DataFrame()
    for dsname, ds in datasets.iteritems():
      acc1[dsname] = ds.apply(func)
    acc[fname] = acc1.T
  return pd.concat(acc).applymap(nominal_value)


#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_soverb():
  """
  Plot S/B as a function of upper mass cut.
  """
  df = FoM_mass_windows()['S/B'].unstack().applymap(nominal_value)
  df = df.drop([55, 65]).rename(columns=LABELS_DITAU)
  # cap "infty" mass cut to some value for plot
  idx = list(df.index)
  idx[-1] = 150
  df.index = idx
  df.index.name   = 'Upper mass [GeV/c^{2}]'
  df.columns.name = 'S/B'

  ## Actual draw
  gr, leg = drawers.graphs_eff(df)
  gr.minimum = -0.2
  gr.maximum = 4.5
  gr.xaxis.limits = 35, 160
  gr.xaxis.moreLogLabels = True
  ROOT.gPad.logx = True
  leg.x1NDC = 0.15
  leg.x2NDC = 0.50
  leg.y1NDC = 0.40
  leg.y2NDC = 0.94


@gpad_save
def draw_sosqrtn():
  df = FoM_mass_windows()['S/sqrt(N)'].unstack().applymap(nominal_value)
  df = df.drop([55, 65]).rename(columns=LABELS_DITAU)
  # cap "infty" mass cut to some value for plot
  idx = list(df.index)
  idx[-1] = 150
  df.index = idx
  df.index.name   = 'Upper mass [GeV/c^{2}]'
  df.columns.name = 'S/#sqrt{S+B}'

  ## Actual draw
  gr, leg = drawers.graphs_eff(df)
  gr.minimum = 0.5
  gr.maximum = 30
  gr.xaxis.limits = 35, 160
  gr.xaxis.moreLogLabels = True
  ROOT.gPad.logx = True
  leg.x1NDC = 0.15
  leg.x2NDC = 0.50
  leg.y1NDC = 0.40
  leg.y2NDC = 0.94


#===============================================================================
# LATEX
#===============================================================================

def latex_compare_FoM():
  """
  Show figure of merit, for appendix, as the optimization is not that a priority.
  """
  df = compare_FoM().rename(columns=DT_TO_LATEX)
  df.index.names = 'FoM', 'Mass cut'
  return df.to_markdown(floatfmt='.3f').replace('_', ' ')

#===============================================================================

if __name__ == '__main__':
  ## DEV
  # print FoM_mass_windows()

  ## Drawer
  # draw_soverb()
  # draw_sosqrtn()

  ## Latex
  # print compare_FoM().fmt2f
  # print latex_compare_FoM()
