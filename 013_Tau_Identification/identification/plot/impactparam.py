#!/usr/bin/env python

from PyrootCK import *


JID = 3210

t = import_tree( 'CheckPVRefit/h1', JID )

h = QHist()
h.name    = 'MIPcheck1'
h.trees   = t 
h.params  = 'mc', 'norefit', 'autofit', 'related_pvrefit'
h.xmin    = 1E-4
h.xmax    = 1E2
h.xlog    = True
# h.legends = 'MC', 'no PVRefit', 'IPVReFitter', '+ IPVOfflineTool'
h.draw()

h = QHist()
h.name    = 'MIPcheck2'
h.trees   = t 
h.params  = 'mc', '', 'manualfit_pvo'
h.xmin    = 1E-4
h.xmax    = 1E2
h.xlog    = True
# h.legends = 'MC', 'no PVRefit', 'IPVReFitter', '+ IPVOfflineTool'
h.draw()


#-----------------------------------------
# JID   = 3172

# for ttype in ('h1', 'e', 'mu'):
#   t_mc  = import_tree('CheckTauImpactParameter/mcp/'+ttype  , JID ).SetTitle('MC')
#   t_old = import_tree('CheckTauImpactParameter/tau/'+ttype  , JID ).SetTitle('no PV-Refit')
#   t_new = import_tree('CheckTauImpactParameter/tau/'+ttype  , JID ).SetTitle('with PV-Refit')

#   t_mc .SetAlias('param', 'MCIP')
#   t_old.SetAlias('param', 'MIP_norefit')
#   t_new.SetAlias('param', 'MIP')

#   h = QHist()
#   h.name    = 'MIP'
#   h.suffix  = ttype 
#   h.trees   = t_mc, t_old, t_new
#   h.params  = 'param'
#   h.xmin    = 1E-4
#   h.xmax    = 1E1
#   h.xlog    = True
#   h.draw()

# #-----------------

# for dtype in ('e_e', 'e_h1', 'e_mu', 'h1_h1', 'h1_mu', 'mu_mu' ):

#   t = import_tree('CheckTauImpactParameter/ditau/'+dtype, JID )

#   h = QHist()
#   h.name    = 'MIPSIG'
#   h.suffix  = dtype
#   h.trees   = t
#   h.params  = 'MIPSIG_norefit', 'MIPSIG'
#   h.cuts    = 'p1_PT>5000 & p2_PT>5000'
#   h.xmin    = 1E-2
#   h.xmax    = 1E3
#   h.xbin    = 50
#   h.xlog    = True
#   h.draw()

raw_input()
