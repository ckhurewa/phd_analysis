#!/usr/bin/env python

"""
Functions specialized in the tree selection & candidate identification.
This will be shared between Z->tautau and HMT analysis.
"""

import os
import sys
import functools
import inspect

from PythonCK.stringutils import lreplace
from PyrootCK import *
from PyrootCK.mathutils import EffU, weighted_average
from qhist.utils import join

sys.path.append(os.path.expandvars('$DIR13'))
from ditau_utils import *

#===============================================================================
# LITE
#===============================================================================

def disable_cut( s, keyword ):
  """
  Given a string of selection cut, try to suppress the cut involving given var.
  Require it to be inside the bracket for clarity.

  >>> s = '(APT>0.6) & (DPHI   >   2.7)'
  >>> disable_cut( s, 'DPHI' )
  '(APT>0.6) & (1)'
  >>> disable_cut( s, 'APT' )
  '(1) & (DPHI   >   2.7)'
  >>> disable_cut( s, 'PT' )
  '(APT>0.6) & (DPHI   >   2.7)'
  >>> disable_cut( '(APT>0.6) & DPHI   >   2.7', 'DPHI' )
  '(APT>0.6) & DPHI   >   2.7'

  """
  pat = r'\(\s*{}.*?\)'.format(keyword)
  return re.sub( pat, '(1)', s )

#===============================================================================
# ALIASING
#===============================================================================

## Numeric trim for the TMVA stability.
CUTS_NUMERIC_TRIM = {
  'ee': [
    'e1_BPVIP       > 0',
    'e1_BPVIP       < 1e3',
    'e2_BPVIP       > 0',
    'e2_BPVIP       < 1e3',
    'e1_BPVIPCHI2   > 0',
    'e1_BPVIPCHI2   < 1e6',
    'e2_BPVIPCHI2   > 0',
    'e2_BPVIPCHI2   < 1e6',
    'DOCACHI2       > 1e-6',
  ],
  'eh1': [
    'e_BPVIP        > 0',
    'e_BPVIP        < 1e3',
    'pi_BPVIP       > 0',
    'pi_BPVIP       < 1e3',
    'e_BPVIPCHI2    > 0',
    'e_BPVIPCHI2    < 1e6',
    'pi_BPVIPCHI2   > 0',
    'pi_BPVIPCHI2   < 1e6',
    'DOCACHI2       > 1e-6',
  ],
  'eh3': [
    'e_BPVIP        > 0',
    'e_BPVIP        < 1e3',
    'e_BPVIPCHI2    > 0',
    'e_BPVIPCHI2    < 1e6',
    'tau_P          > 0',
    'tau_VCHI2PDOF  < 1e3',
    'tau_DRTRIOMAX  < 1',
    'tau_BPVCORRM   > 0.',
    'tau_BPVCORRM   < 10e3',
    'tau_BPVLTIME   > 0',
    'DOCACHI2       > 1e-6',
  ],
  'emu': [
    'e_BPVIP        > 0',
    'e_BPVIP        < 1e3',
    'mu_BPVIP       > 0',
    'mu_BPVIP       < 1e3',
    'e_BPVIPCHI2    > 0',
    'e_BPVIPCHI2    < 1e6',
    'mu_BPVIPCHI2   > 0',
    'mu_BPVIPCHI2   < 1e6',
    'DOCACHI2       > 1e-6',
  ],
  'h1mu': [
    'pi_BPVIP       > 0',
    'pi_BPVIP       < 1e3',
    'mu_BPVIP       > 0',
    'mu_BPVIP       < 1e3',
    'pi_BPVIPCHI2   > 0',
    'pi_BPVIPCHI2   < 1e6',
    'mu_BPVIPCHI2   > 0',
    'mu_BPVIPCHI2   < 1e6',
    'DOCACHI2       > 1e-6',
  ],
  'h3mu': [
    'mu_BPVIP       > 0',
    'mu_BPVIP       < 1e3',
    'mu_BPVIPCHI2   > 0',
    'mu_BPVIPCHI2   < 1e6',
    'tau_P          > 0',
    'tau_VCHI2PDOF  < 1e3',
    'tau_DRTRIOMAX  < 1',
    'tau_BPVCORRM   > 0.',
    'tau_BPVCORRM   < 10e3',
    'tau_BPVLTIME   > 0',
    'DOCACHI2       > 1e-6',
  ],
  'mumu': [
    'mu1_BPVIP      > 0',
    'mu1_BPVIP      < 1e3',
    'mu2_BPVIP      > 0',
    'mu2_BPVIP      < 1e3',
    'mu1_BPVIPCHI2  > 0',
    'mu1_BPVIPCHI2  < 1e6',
    'mu2_BPVIPCHI2  > 0',
    'mu2_BPVIPCHI2  < 1e6',
    'DOCACHI2       > 1e-6',
  ],
}
CUTS_NUMERIC_TRIM['mumu_withz'] = CUTS_NUMERIC_TRIM['mumu']

# Usage
# esel data-to-mc agreement
CUT_Z02MUMU = [
  'StrippingWMuLineDecision',
  { 'mu1_TOS_MUON', 'mu2_TOS_MUON' },
  'mu1_PT     > 20E3',
  'mu2_PT     > 20E3',
  'M          > 80E3',
  'M          < 100E3',
  'mu1_ETA    > 2.0',
  'mu2_ETA    > 2.0',
  'mu1_ETA    < 4.5',
  'mu2_ETA    < 4.5',
]

#-------------------------------------------------------------------------------

def apply_alias_simple(dt, t):
  """
  Independent of cutmodule & name, just dtype.

  Refactor to be easily used in HMT
  """

  ## Kinematic alias
  t.SetAlias('E' , '(M**2 + P**2)**0.5')
  t.SetAlias('PZ', '(P**2 - PT**2)**0.5')
  t.SetAlias('Y' , '1/2*TMath::Log((E+PZ)/(E-PZ))')
  #
  t.SetAlias('tau_DRoPT', 'tau_DRTRIOMAX/tau_PT*1000' )
  t.SetAlias('tau_DRtPT', 'tau_DRTRIOMAX*tau_PT/1000' )
  t.SetAlias('tau_VDoP' , 'tau_BPVVD/tau_P*1000' )

  ## Homogenisation alias
  # Used in esel recalculation
  pf1, pf2 = PREFIXES[dt]
  t.SetAlias('PT1'    , '%s_PT/1e3'%pf1)
  t.SetAlias('PT2'    , '%s_PT/1e3'%pf2)
  t.SetAlias('ETA1'   , '%s_ETA'%pf1)
  t.SetAlias('ETA2'   , '%s_ETA'%pf2)
  t.SetAlias('IP1'    , '%s_BPVIP'%pf1)
  t.SetAlias('IP2'    , '%s_BPVIP'%pf2)
  t.SetAlias('PRER1'  , '({0}_PERR2/{0}_P/{0}_P)**0.5'.format(pf1)) # relative momentum error
  t.SetAlias('PRER2'  , '({0}_PERR2/{0}_P/{0}_P)**0.5'.format(pf2))
  #
  t.SetAlias('ISO1'   , '%s_0.50_cc_IT'%pf1)
  t.SetAlias('ISO2'   , '%s_0.50_cc_IT'%pf2)
  t.SetAlias('cc_vPT1', '%s_0.50_cc_vPT'%pf1)
  t.SetAlias('cc_vPT2', '%s_0.50_cc_vPT'%pf2)
  t.SetAlias('nc_vPT1', '%s_0.50_nc_vPT'%pf1)
  t.SetAlias('nc_vPT2', '%s_0.50_nc_vPT'%pf2)
  t.SetAlias('IT1'    , '%s_0.50_IT'%pf1)
  t.SetAlias('IT2'    , '%s_0.50_IT'%pf2)
  #
  t.SetAlias('DPT'   , 'PT1-PT2')
  t.SetAlias('IP1oPT', 'IP1/PT1')
  t.SetAlias('IP2oPT', 'IP2/PT2')
  t.SetAlias('IP1tPT', 'IP1*PT1')
  t.SetAlias('IP2tPT', 'IP2*PT2')
  return t

def apply_alias(cutmodule, dtype, tname, t):
  """
  The decision to not implicitly use null value when the cut is missing from the
  cutmodule, so that it implies the cut should explicitly set to blank when 
  not needed by the contex.
  """
  ## Immediately exit for empty tree
  if not t.branches:
    return

  ## Force to dt
  dt    = DTYPE_TO_DT.get(dtype, dtype)
  dtype = DT_TO_DTYPE[dt]

  ## shorthand to cuts. Less optimized for performance, but good for stateless.
  GOODRUN    = cutmodule.CUTS_PRESEL_NOBADRUN
  PRESEL     = cutmodule.CUTS_PRESELECTION[dtype]
  PRONGS     = cutmodule.CUTS_TAUH3_SELECTION if 'h3' in dtype else ['1==1']
  ISO        = cutmodule.CUTS_ISOLATION[dtype]
  ANTIISO    = cutmodule.CUTS_ANTIISOLATION[dtype]
  MIXISO     = cutmodule.CUTS_MIXISOLATION[dtype]
  DISPLACED  = cutmodule.CUTS_IMPPAR_NORMAL[dt] or '1==1'
  IPPROMPT   = cutmodule.CUTS_IMPPAR_PROMPT
  DITAU_APT  = cutmodule.CUTS_DITAU_APT[dt]
  DITAU_DPHI = cutmodule.CUTS_DITAU_DPHI[dt]
  # SELECTION  = cutmodule.CUTS_SELECTION[dt] # no mass here
  UPPERMASS  = cutmodule.CUTS_UPPER_MASS[dt]
  NUMTRIM    = CUTS_NUMERIC_TRIM[dt]
  #
  # PRESEL_MUMU_WITHPEAK  = cutmodule.CUTS_PRESELECTION_MUMU_WITHPEAK
  # PRESEL_EE_WITHPEAK    = cutmodule.CUTS_PRESELECTION_EE_WITHPEAK
  # 
  TRUECHAN = '(ditau_type_%s >= 1)'%dtype.replace('_withz', '')

  ## Apply simple-aliases
  apply_alias_simple(dt, t)

  ## Compatibility alias
  if t.branches:
    for pf in PREFIXES_ALL[dt]:
      bname = pf+'_TRPCHI2'
      if not any(bname==br.name for br in t.branches):
        t.SetAlias(bname, '1==1') # 1 > 0.01

  ## Stringify into immutable
  cuts_presel = join(GOODRUN, PRESEL)

  ## SS/OS  -- Difference in Stripping
  ## Replace the OS-Line with SS-Line
  if tname.startswith('tss'):
    cuts_presel = cuts_presel.replace('XLineDecision', 'XLineSSDecision')

  ## True channel flag for Ztautau (Use only True by default)
  # Apply only to my custom Ztau. The bad central Ztau I don't have compat tuple yet.
  # _true_ztautau_extra = ('ztautau', 'ztautau0', 't_ztautau', 'tss_ztautau', 't_ztautau0', 't_ztautau0_nofidacc')
  t.SetAlias('_TrueChan'        , TRUECHAN     if 'ztautau' in tname else '1==1')
  t.SetAlias('_OtherChan'       , '!_TrueChan' if 'ztautau' in tname else '1==1')
  t.SetAlias('Presel_anychan'   , cuts_presel)
  t.SetAlias('Presel_truechan'  , join(cuts_presel, '_TrueChan' ))
  t.SetAlias('Presel_otherchan' , join(cuts_presel, '_OtherChan'))
  t.SetAlias('Preselection'     , 'Presel_truechan') # By default, have only True channel.

  ## Individual components, hidden, used with care
  t.SetAlias('_Prongs' , join( PRONGS     ))
  t.SetAlias('_Iso'    , join( ISO        ))
  t.SetAlias('_AntiIso', join( ANTIISO    ))
  t.SetAlias('_MixIso' , join( MIXISO     ))
  t.SetAlias('_Displ'  , join( DISPLACED  ))
  t.SetAlias('_APT'    , join( DITAU_APT  ))
  t.SetAlias('_DPHI'   , join( DITAU_DPHI ))
  # t.SetAlias('_Sel'    , join( SELECTION  ))
  t.SetAlias('_Mass'   , join( UPPERMASS  ))
  t.SetAlias('_Ntrim'  , join( NUMTRIM    ))

  ## Selection cuts, with different Data-Driven mixtures
  # The convention is choose to EXCLUDE mass cut for now.
  t.SetAlias('Sel_default'        , join('Preselection'    , '_Prongs', '_Iso'    , '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_noiso'          , join('Preselection'    , '_Prongs',             '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_antiiso'        , join('Preselection'    , '_Prongs', '_AntiIso', '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_mixiso'         , join('Preselection'    , '_Prongs', '_MixIso' , '_Displ', '_APT', '_DPHI' ))
  #
  t.SetAlias('Sel_anychan'        , join('Presel_anychan'  , '_Prongs', '_Iso'    , '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_anychan_noiso'  , join('Presel_anychan'  , '_Prongs',             '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_otherchan'      , join('Presel_otherchan', '_Prongs', '_Iso'    , '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_otherchan_noiso', join('Presel_otherchan', '_Prongs',             '_Displ', '_APT', '_DPHI' ))
  t.SetAlias('Sel_truechan'       , join('Presel_truechan' , '_Prongs', '_Iso'    , '_Displ', '_APT', '_DPHI' ))
  
  ## The absencing variants (esel calib, debug evo)
  t.SetAlias('Sel_nodphi'  , join('Preselection', '_Prongs', '_Iso', '_Displ', '_APT'         )) # For esel DPHI calib
  t.SetAlias('Sel_noapt'   , join('Preselection', '_Prongs', '_Iso', '_Displ',         '_DPHI'))
  t.SetAlias('Sel_nodispl' , join('Preselection', '_Prongs', '_Iso',           '_APT', '_DPHI'))
  t.SetAlias('Sel_noprongs', join('Preselection',            '_Iso', '_Displ', '_APT', '_DPHI'))

  ## THE default 'Selection' cut now differs by name, VERY TRICKY
  if tname in ('tdd_qcd', 'tssdd_qcd'):
    t.SetAlias('Selection', 'Sel_antiiso')
  elif tname.endswith('noiso'):
    t.SetAlias('Selection', 'Sel_noiso')
  else:
    t.SetAlias('Selection', 'Sel_default')

  ## Alias for data-driven Z02mumu, for esel correction
  t.SetAlias('Z02mumu', join(GOODRUN, CUT_Z02MUMU))

#===============================================================================

def indexing_trees_os_ss(d):
  """
  Reindex the collection of trees into 2 dicts of OS,SS
  They will have same key this time.
  Note: Can only change the title, not the name (segfault)
  """
  trees = {}
  os = {}
  ss = {}
  for k,v in dict(d).iteritems(): # loop on a copy
    if isinstance(v, ROOT.TTree):
      trees[k] = v
      if k.startswith('tss'):
        k = lreplace(k, 'tss_', '')
        k = lreplace(k, 'tssdd_', '')
        v.title = k
        ss[k] = v
      else:
        ## use lreplace to make sure that I replcae only the beginning.
        # this caused a problem for wejet_noiso.
        k = lreplace(k, 'tdd_', '')
        k = lreplace(k, 't_', '')
        v.title = k
        os[k] = v
  return trees, os, ss


#===============================================================================
# CAMOFLAGING (misid)
#===============================================================================

def camoflage( t, **kwargs ):
  """
  Provide aliases to some principle branches by list of given prefixes,
  i.e., all the branch that has the name 'oldprefix_XXX' will have an alias
  'newprefix_XXX' automatically.

  Usage:

  camoflage( t_dimuon, mu1='pi', mu2='mu' )
  t_dimuon.Draw('Selection', '', '')
  t_dimuon_fakemu1 = t.sliced_tree('t_dimuon_fakemu1')

  camoflage( t_dimuon, mu1='mu', mu2='pi' )
  ...
  """
  for br in t.branches:
    bname = br.name
    for oldpf, newpf in kwargs.iteritems():
      if bname.startswith(oldpf):
        newbname = PythonCK.stringutils.lreplace( bname, oldpf, newpf )
        t.SetAlias( newbname, bname )


#===============================================================================
# REWEIGHTING MISID ZLL
#===============================================================================

def _sliced_camoflage_dilepton(t, origin, target, filt='Selection'):
  """
  Base method to obtain the dataframe of the di-lepton passing XOR condition.
  This will double the entries, as well as break the symmetry of di-lepton
  to the target channel.
  As this will be attached by some weights (e.g., misid rate),
  neglect the double-product term, i.e., assume P(A|B) = P(A)+P(B)
  """

  ## Prepare the camoflage spec.
  spec1 = dict(zip(PREFIXES[origin], PREFIXES[target]))
  spec2 = dict(zip(PREFIXES[origin], reversed(PREFIXES[target])))

  ## Start camoflaging
  camoflage( t, **spec1 )
  t.Draw(filt, '', 'goff')
  t_fake1 = t.sliced_tree('fake1')
  camoflage( t, **spec2 )
  t.Draw(filt, '', 'goff')
  t_fake2 = t.sliced_tree('fake2')
  t.AddFriend(t_fake1)
  t.AddFriend(t_fake2)

  ## Export all & try to detect the dual fields automatically
  pf1, pf2    = PREFIXES[origin]
  dual_fields = set(br.name.replace(pf+'_','') for br in t.branches for pf in spec1)
  n_before    = t.entries

  ## Get around of having all branches, but NOT using slice framework (beacuse
  # of the limited number of params) while also have the new info on fake selection
  df_fake1 = t.dataframe(selection='(fake1.{})'.format(filt))
  df_fake2 = t.dataframe(selection='(fake2.{})'.format(filt))
  n_fake1  = df_fake1.shape[0]
  n_fake2  = df_fake2.shape[0]
  # n_fake12 = t.GetEntries('(fake1.{0})|(fake2.{0})'.format(filt)) # slow

  ## For fake2only, I only need to swap the field
  fields_swap = {}
  fields_swap.update({pf1+'_'+s: pf2+'_'+s for s in dual_fields})
  fields_swap.update({pf2+'_'+s: pf1+'_'+s for s in dual_fields})
  df_fake2 = df_fake2.rename(columns=fields_swap)

  ## Finally, put together
  # Note: this method will intrinsically have a double-counting candidate in
  # the same event, which is expected. The selection after this point should
  # be able to kill one of that, or eventually drop the double-counted one
  # like the usual treatment of multiple-cand-per-event in data
  fields_swap = {}
  fields_swap.update({pf1+'_'+s: spec1[pf1]+'_'+s for s in dual_fields})
  fields_swap.update({pf2+'_'+s: spec1[pf2]+'_'+s for s in dual_fields})
  acc = df_fake1, df_fake2
  df  = pd.concat(acc, ignore_index=True).rename(columns=fields_swap)
  logger.info('Doubling entries: %i --> %i + %i = %i'%(n_before, n_fake1, n_fake2, n_fake1+n_fake2))
  return df

#-------------------------------------------------------------------------------

def reweighting_misid_zll(tree, target_dt, h_misid, filt="Selection"):
  """
  Given a di-lepton TTree, a target camoflage channel, and the TH1 of the
  lepton misid rate, return the new TTree (of possibly larger size), which
  its camoflaged version satisfied the Selection of the target channel.

  Return a new Tree, suitable for re-packaging.

  Need external lib `root_numpy`
  """
  from root_numpy import array2tree

  ## determine the brancing dtype of tree (mumu for dimuon, ee for dielectron)
  if 'mu_mu' in tree.name:
    src_dt = 'mumu'
    col_pt = 'mu_PT'  # indexing column
  elif 'e_e' in tree.name:
    src_dt = 'ee'
    col_pt = 'e_PT'
  else:
    raise ValueError('Unknown di-lepton tree: %s'%tree.name)

  ## Slice a tree to get a dataframe. This has only essential columns needed
  # for the export. There're 2 more columns (fake1.Selection, fake2.Selection)
  # representing whether which polarity make is passes the target selection.
  df = _sliced_camoflage_dilepton(tree, src_dt, target_dt, filt)

  ## Obtain the misid rate, mapped by PT. Treat this as weight.
  rates = h_misid.vlookup(df[col_pt])
  df['weight']      = rates.apply(uncertainties.nominal_value)
  df['weight_err']  = rates.apply(uncertainties.std_dev)

  ## Finally, return the new tree
  tree = array2tree(df.to_records())
  tree.SetAlias(filt, 'weight')
  return tree


#===============================================================================

if __name__ == '__main__':
  pass
