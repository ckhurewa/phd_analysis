#!/usr/bin/env python

from samesign_utils import export_pdf
from bkg_samesign import PATH_SAMESIGN, parse_spec_ssfit

#===============================================================================

if __name__ == '__main__':
  # Handle method to copy the figures to Desktop.
  export_pdf(parse_spec_ssfit(), PATH_SAMESIGN)
