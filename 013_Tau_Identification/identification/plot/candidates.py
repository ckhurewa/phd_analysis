#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Compute the numbers of expect Z->tautau candidates. 

"""
from PythonCK.stringutils import insert_line
from PyrootCK import *
from PyrootCK.mathutils import EffU, weighted_average

## Secondary tools
from id_utils import *
from ditau_utils import df_from_path, inject_midrule

## Tool specialted in identification
import selected

## Path & context to the pickled background data
sys.path.append(os.path.expandvars('$DIR13/results'))
import normalizer
CONTEXT_PATH = '$DIR13/identification/plot/bkg_context'

## List of Others field to be collapse together
LIST_OTHERS = ['ttbar', 'VV', 'Zbb', 'ZXC']


#===============================================================================
# UTILS
#===============================================================================

@memorized
def get_context():
  return normalizer.loading_context_pickle(CONTEXT_PATH)

@memorized
def cache_table(tablename):
  return normalizer.calc_single_array(tablename, get_context())


def get_expected_from_normalizer(tablename, process):
  """
  Get the expected amount from MC samples, convert to Series of ufloat
  """
  df = cache_table(tablename)
  se = df.loc[process].apply(lambda args:args[2].to_ufloat())
  return se


#===============================================================================
# DATA OS, SS
#===============================================================================

@memorized
def result_data_nos_nss_nomasswindow():
  """
  Return the number of selected candidate in data, both OS and SS.
  """
  # fast mode
  df0 = df_from_path('check_data_nos_nss', CONTEXT_PATH)
  return df0.xs('evt', level=1).loc[list(CHANNELS)].applymap(lambda x: var(x, 'stat'))


@memorized
def result_data_nos_nss_withmasswindow():
  """
  Return the number of selected candidate in data, both OS and SS.
  """
  df0 = df_from_path('check_data_withmasswindow', CONTEXT_PATH)
  return df0.xs('evt', level=1).loc[list(CHANNELS)].applymap(lambda x: var(x, 'stat'))


#===============================================================================
# COLLECT TOGETHER
#===============================================================================

def _finalize(df, obsv):
  """
  Stage before Ztau extra-channel purify
  """

  ## Ztautau cross-feed
  # Unlike the other, this "BKG" will be subtracted from the signal amount after
  # removed all external backgrounds. So this only return the expected factor
  import cross_feed
  zxc_frac = cross_feed.results()

  ## Process the cross-feed
  prepure_bkg     = df.sum()
  prepure_ztau    = obsv - prepure_bkg
  extrachan_ztau  = zxc_frac * prepure_ztau
  df.loc['ZXC']   = extrachan_ztau
  df.loc['BKG']   = prepure_bkg + extrachan_ztau
  df.loc['OS']    = obsv
  df.loc['Ztau']  = prepure_ztau - extrachan_ztau
  return df


@pickle_dataframe_nondynamic
def results_nomasswindow():
  """
  Summary of all results, in python (DataFrame). No mass window applied
  """
  import bkg_zll
  import bkg_samesign

  ## shortcut
  get_mc = lambda tag: get_expected_from_normalizer('check_mc_nos_nss_os', tag)
  ttbar1 = get_mc('ttbar_41900006')
  ttbar2 = get_mc('ttbar_41900007')
  ttbar3 = get_mc('ttbar_41900010')

  ## Everything is pd.Series for consistent joining.
  df = pd.concat({
    'Zll'   : bkg_zll.results_nomasswin(),
    'QCD'   : bkg_samesign.result_qcd_nomasswin(),
    'EWK'   : bkg_samesign.result_ewk_nomasswin(),
    'VV'    : get_mc('WW_lx') + get_mc('WZ_lx'),
    'ttbar' : ttbar1 + ttbar2 + ttbar3,
    'Zbb'   : get_mc('zbb'),
  }, axis=1)

  ## Reorder to canonical order
  df = df[['Zll', 'QCD', 'EWK', 'VV', 'ttbar', 'Zbb']].T[CHANNELS]

  ## Summarize into dict, attributed to statistical uncer
  obsv = result_data_nos_nss_nomasswindow()['OS']

  ## ZXC, Signal, ordering
  return _finalize(df, obsv)


@pickle_dataframe_nondynamic
def results_withmasswindow():
  """
  This is the version with nominal mass window (60 in mumu,ee).
  """
  import bkg_zll
  import bkg_samesign

  ## shortcut + fraction
  fracs = selected.fraction_mass_window()
  get_mc = lambda tag: get_expected_from_normalizer('check_mc_nos_nss_os', tag) * fracs.loc['t_'+tag]
  ttbar1 = get_mc('ttbar_41900006')
  ttbar2 = get_mc('ttbar_41900007')
  ttbar3 = get_mc('ttbar_41900010')

  ## Collect together
  df = pd.concat({
    'Zll'   : bkg_zll.results_withmasswin(),
    'QCD'   : bkg_samesign.result_qcd_withmasswin(),
    'EWK'   : bkg_samesign.result_ewk_withmasswin(),
    'VV'    : get_mc('WW_lx') + get_mc('WZ_lx'),
    'ttbar' : ttbar1 + ttbar2 + ttbar3,
    'Zbb'   : get_mc('zbb'),
  }, axis=1)

  ## Reorder to canonical order
  df = df[['Zll', 'QCD', 'EWK', 'VV', 'ttbar', 'Zbb']].T[CHANNELS]

  ## Summarize into dict, attributed to statistical uncer
  obsv = result_data_nos_nss_withmasswindow()['OS']

  ## ZXC, Signal, ordering
  return _finalize(df, obsv)


#-------------------------------------------------------------------------------
# FOR COMBINATION PLOT
#-------------------------------------------------------------------------------

@memorized
def results_comb_nomasswindow():
  """
  Compact form of above where some entries are collapsed together.
  Less elements (ttbar, VV, ZXC are combined to "Other"), suitable for plotting.
  """
  df = results_nomasswindow().T.copy()
  ## Collapse
  df['Other'] = df[LIST_OTHERS].sum(axis=1)
  df.drop(LIST_OTHERS, axis=1, inplace=True)
  return df.T


@memorized
def results_comb_withmasswindow():
  """
  Suitable for combine plot.
  """
  df = results_withmasswindow().T.copy()
  ## Collapse
  df['Other'] = df[LIST_OTHERS].sum(axis=1)
  df.drop(LIST_OTHERS, axis=1, inplace=True)
  return df.T

#-------------------------------------------------------------------------------
# FOR RECONSTRUCTION EFF
#-------------------------------------------------------------------------------

@memorized
def results_signed_for_erec():
  """
  Used for erec computation:
  Detailed version of results function above with Zll splitted into Zmumu, Zee.
  The mass window is applied, and the number of candidates is signed:
  - OS  : positive
  - bkg : negative
  - Ztau: 0

  Discard the statistical uncertainty here, which is not needed in the
  erec computation
  """
  import bkg_zll
  import bkg_samesign

  ## Expand the detail emu/mue
  df      = results_withmasswindow().copy()
  raw_ewk = bkg_samesign.result_ewk_withmasswin(collapse=False)
  raw_zll = bkg_zll.results_withmasswin(collapse_emu=False).copy()
  #
  df.loc['Other']        = df.loc[['ttbar','VV','ZXC']].sum()
  df.loc['Zmu'  , 'emu'] = raw_zll['emu']
  df.loc['Ze'   , 'emu'] = raw_zll['mue']
  df.loc['EWKe' , 'emu'] = raw_ewk['emu_raw']
  df.loc['EWKmu', 'emu'] = raw_ewk['mue_raw']
  df.loc['Zll'  , 'emu'] = pd.np.nan
  df.loc['EWK'  , 'emu'] = pd.np.nan
  df = df.drop(['BKG', 'ttbar', 'VV', 'ZXC'])

  ## Apply the sign
  signer = lambda x,s: 0. if s=='Ztau' else x*(1. if s=='OS' else -1)
  acc    = pd.DataFrame()
  for index, se in df.iterrows():
    acc[index] = se.apply(lambda x: uncertainties.nominal_value(signer(x, index)))
  return acc.T

#===============================================================================

def print_result_python():
  """
  Result with uncertainties, suitable for cross-section calculation.
  """
  nztau     = results_withmasswindow().loc['Ztau']
  errs_stat = nztau.apply(lambda u: u.get_error('stat'))
  errs_nsig = results_withmasswindow().loc['BKG'].apply(std_dev) # explicit version

  tem0 = "\nDF['nsig'] = pd.Series({{\n{}\n}})\n".format
  tem1 = "  {!r:6}: {.n:12.7f} + ufloat( 0, {:9.6f}, 'stat') + ufloat( 0, {:9.6f}, 'nsig'),".format
  acc  = [tem1(dt, nztau[dt], errs_stat[dt], errs_nsig[dt]) for dt in CHANNELS]
  print tem0('\n'.join(acc))


#===============================================================================
# LATEX
#===============================================================================

def _results_postpc(df):
  """
  Add line break & env. 
  Also force obs to single int here, by forcing it to string, thus override the
  mechanism in fmt2cols.
  """
  df.loc['OS'] = df.loc['OS'].apply(lambda u: str(int(u.n)))
  print df
  df = df.applymap(fmt2cols).rename(PROCESS_LATEX, DT_TO_LATEX)
  inject_midrule(df, -1, -2, -3)
  msg = df.to_latex(escape=False)
  msg = '\n'.join(msg.split('\n')[4:-3])
  return '\\begin{ditautabular}\n'+msg+'\n\\end{ditautabular}'

def latex_result():
  """
  Table compat with latex multicol report.
  """
  return _results_postpc(results_withmasswindow())

def latex_result_nomasswin():
  """
  Same as above, no mass window for appendix
  """
  return _results_postpc(results_nomasswindow())

def latex_relative_uncertainties():
  """
  Relative uncertainties from each process
  """
  df0  = results_withmasswindow()
  df   = df0.iloc[:-2].applymap(uncertainties.std_dev)
  nsig = df0.loc['Ztau'].apply(uncertainties.nominal_value)
  df   = (df/nsig).rename(columns=DT_TO_LATEX, index=PROCESS_LATEX)
  df   = df.replace(0, '---') # suppress null uncertainties
  ##
  df.index.name = '\\cscrerr'
  inject_midrule(df, -1)
  return df.fmt2p.to_markdown(stralign='right')


#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print results_nomasswindow().fmt2f
    print results_withmasswindow().fmt2f
    sys.exit()

  ## Components
  # print result_data_nos_nss_nomasswindow().fmt1f
  # print result_data_nos_nss_withmasswindow().fmt1f
  # print result_ztaucont_frac()

  ## FINAL
  # print results_nomasswindow().fmt2f
  # print results_withmasswindow().fmt2f
  # print results_comb().fmt2f
  # print results_signed_for_erec().fmt2f
  # print results_comb_nomasswindow().fmt2f
  # print results_comb_withmasswindow().fmt2f
  # print_result_python()

  ## Latex
  # print latex_result()
  # print latex_result_nomasswin()
  print latex_relative_uncertainties()
