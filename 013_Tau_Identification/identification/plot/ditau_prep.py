#!/usr/bin/env python

"""

Prepare the selection on trees, cached on TFile for actual analysis.

One can simply call `from ditau_prep import *` to import all aliased trees.

"""

## Load the module containing cuts, VERY FIRST
# It's possible to use another one by overriding the sys.path
import ditau_cuts

## no dependency, just tools
from PyrootCK import *
import id_utils
from qhist import utils

## Current dtype under study
dtype, dt = id_utils.get_current_dtype_dt()


#===============================================================================
# LOADING TREES
#===============================================================================

## Tree names from dtype
tname_os = 'DitauCandTupleWriter/%s'%dtype
tname_ss = 'DitauCandTupleWriter/%s_ss'%dtype


## Batch151008 (making candidate offline)
# real = 3557, 3558, 3559, 3560, 3561, 3562, 3563, 3564, 3565, 3567, 3568, 3569, 3570, 3571, 3572, 3573, 3574, 3575, 3576, 3577, 3580, 3581, 3582, 3583
## Batch160201: Fix PHI,Iso
# real = 4191,  # Single job with multiple ntuples hadded.
## Batch160314: Properly loosen tauh3, large
# real = 4294, 4295, 4296, 4298, 4299, 4300, 4301, 4302, 4303, 4304, 4305, 4306
## Batch160417: S21, Filtered on high-PT to have slim size. Added rapidity
# real = 4562,
## Batch1611: uncut hadron ETA, TRPCHI2
# real = 5204, 5205, 5206, 5207, 5208, 5209, 5210, 5211, 5212, 5213, 5214, 5215, 5216, 5217
real = '5204-5217',

## Data
t_real    = import_tree( tname_os, *real ).SetTitle('Real (OS)')
tss_real  = import_tree( tname_ss, *real ).SetTitle('Real (SS)')

## data-driven QCD shape
tdd_qcd   = import_tree( tname_os, *real ).SetTitle('DD ~ QCD (OS)')
tssdd_qcd = import_tree( tname_ss, *real ).SetTitle('DD ~ QCD (SS)')

## Fixed dtype independent of the current channel
# to be used with camoflage for misid calculation.
t_dimuon        = import_tree( 'DitauCandTupleWriter/mu_mu'   , *real ).SetTitle('DiMuon (DD)')
tss_dimuon      = import_tree( 'DitauCandTupleWriter/mu_mu_ss', *real ).SetTitle('DiMuon (DD-SS)')
t_dielectron    = import_tree( 'DitauCandTupleWriter/e_e'     , *real ).SetTitle('DiElectron (DD)')
tss_dielectron  = import_tree( 'DitauCandTupleWriter/e_e_ss'  , *real ).SetTitle('DiElectron (DD-SS)')

#-------------------------------------------------------------------------------

# ## Batch1702: Removed biased cut (DPHI,CORRM,VCHI2PDOF) for the same of HMT tag & probe
# # note: except the last one, wrong IS_MC=True flag. May/maynot have a problem,
# #       and dname is slightly different (with 0/ prefix)
# def data_importer(tree, dtype, ss=False):
#   suffix = '_ss' if ss else ''
#   ## Custom workaround
#   fpath = '/panfs/khurewat/ganga_storage/5312-5313'
#   tree.Add('%s/tuple13.root/DitauCandTupleWriter/%s'      %(fpath, dtype+suffix))
#   tree.Add('%s/tuple0?.root/DitauCandTupleWriter/0/%s'    %(fpath, dtype+suffix))
#   tree.Add('%s/tuple1[0-2].root/DitauCandTupleWriter/0/%s'%(fpath, dtype+suffix))
#   tree.ownership = False
#   tree.estimate = tree.entries+1
#   return tree

# t_real    = data_importer(ROOT.TChain('t_real'   , 'Real (OS)'), dtype)
# tss_real  = data_importer(ROOT.TChain('tss_real' , 'Real (SS)'), dtype, ss=True)
# tdd_qcd   = data_importer(ROOT.TChain('tdd_qcd'  , 'DD ~ QCD (OS)'), dtype)
# tssdd_qcd = data_importer(ROOT.TChain('tssdd_qcd', 'DD ~ QCD (SS)'), dtype, ss=True)

# t_dimuon       = data_importer(ROOT.TChain('mu_mu'   , 'DiMuon (DD)'       ), 'mu_mu')
# tss_dimuon     = data_importer(ROOT.TChain('mu_mu_ss', 'DiMuon (DD-SS)'    ), 'mu_mu', ss=True)
# t_dielectron   = data_importer(ROOT.TChain('e_e'     , 'DiElectron (DD)'   ), 'e_e')
# tss_dielectron = data_importer(ROOT.TChain('e_e_ss'  , 'DiElectron (DD-SS)'), 'e_e', ss=True)

def T_data_nobias(dtype, tname, title):
  """
  Reminder: This data ntuple has biased selection removed (DPHI>2.7,tau_VCHI2PDOF<20, tau_BPVCORRM<3e3),
  but the tuples are incomplete, so it's not usable for normalized result.
  Only to be used for shape study.
  """
  srcs = [
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple00.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple01.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple04.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple05.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple06.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple07.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple08.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple09.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple10.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple11.root',
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5312/tuple12.root',
    ## ignore tuple13.root
  ]
  chain = ROOT.TChain(tname, title)
  for src in srcs:
    chain.Add('%s/DitauCandTupleWriter/0/%s'%(src,dtype))
  # chain.ownership = False # segfault
  chain.estimate = chain.entries+1
  ## aliasing
  id_utils.apply_alias(ditau_cuts, dtype, tname, chain)
  return chain

#-------------------------------------------------------------------------------

## 3584: Old, with no gamma*
## 3964 has only mumu, but plenty
## 4000 has all channel (but 200kevt in total)
## 4085 all channels, 400kevt
## 4104 Fixed missing PHI, new isolation with TTConeIsolation
## 4281 Now with 1Mevt stats
## 4292 Fix too-tight tauh3, fix mcMatching
## 4333 With fiducial & acceptance cut in place.
## 4674 Like before, but now with truth di-tau event-info
## 5230 Edit acceptance for WideEtaHadron. note that 4674 is okay
t_ztautau   = import_tree( tname_os, 5230 ).SetTitle('Zg tautau')
tss_ztautau = import_tree( tname_ss, 5230 ).SetTitle('Zg tautau(SS)')

## Another version from central, may/maynot have gamma*
# But have consistent egen suitable for estimation table.
# TODO: check the gen in J4415 to be sure
# 4417: Old
# 5234: Add tau header for cross-feed study, with fidacc (5233 without)
t_ztautau0          = import_tree( tname_os, 5234 ).SetTitle('Z tautau')
tss_ztautau0        = import_tree( tname_ss, 5234 ).SetTitle('Z tautau')
t_ztautau0_nofidacc = import_tree( tname_os, 5233 ).SetTitle('Z tautau')

## Custom
if dt == 'mumu':
  t_wmumujet    = import_tree( tname_os, 4331 ).SetTitle('Wmu20+mujet')
  tss_wmumujet  = import_tree( tname_ss, 4331 ).SetTitle('Wmu20+mujet (SS)')

# t_mcmb_mux    = import_tree( tname_os, 4332 ).SetTitle('MCMB MuX')
# tss_mcmb_mux  = import_tree( tname_ss, 4332 ).SetTitle('MCMB MuX (SS)')

## 4533 <- 4404
# t_we20jet     = import_tree( tname_os, 4533 ).SetTitle('We20+jet')
# tss_we20jet   = import_tree( tname_ss, 4533 ).SetTitle('We20+jet (SS)')

#-------------------------------------------------------------------------------

# JIDS_MULTI = 5237,  # first one
# JIDS_MULTI = 5420, 5422, 5423, 5428, 5434 # complete the missing ones from previous batch. More processes
JIDS_MULTI = 5435, 5438 # Rerun with explicit event counter
tname2_os  = 'DitauCandTupleWriter/%s/'+dtype
tname2_ss  = tname2_os+'_ss'

## ttbar
t_ttbar_41900006    = import_tree(tname2_os % 41900006, *JIDS_MULTI).SetTitle('ttbar 41900006')
tss_ttbar_41900006  = import_tree(tname2_ss % 41900006, *JIDS_MULTI).SetTitle('ttbar 41900006 (SS)')
t_ttbar_41900007    = import_tree(tname2_os % 41900007, *JIDS_MULTI).SetTitle('ttbar 41900007')
tss_ttbar_41900007  = import_tree(tname2_ss % 41900007, *JIDS_MULTI).SetTitle('ttbar 41900007 (SS)')
t_ttbar_41900010    = import_tree(tname2_os % 41900010, *JIDS_MULTI).SetTitle('ttbar 41900010')
tss_ttbar_41900010  = import_tree(tname2_ss % 41900010, *JIDS_MULTI).SetTitle('ttbar 41900010 (SS)')

## DiBosons
t_WW_ll       = import_tree(tname2_os % 41922002, *JIDS_MULTI).SetTitle('WW_ll 41922002')
tss_WW_ll     = import_tree(tname2_ss % 41922002, *JIDS_MULTI).SetTitle('WW_ll 41922002 (SS)')
t_WW_lx       = import_tree(tname2_os % 42021000, *JIDS_MULTI).SetTitle('WW_lx 42021000')
tss_WW_lx     = import_tree(tname2_ss % 42021000, *JIDS_MULTI).SetTitle('WW_lx 42021000 (SS)')
t_WZ_lx       = import_tree(tname2_os % 42021001, *JIDS_MULTI).SetTitle('WZ_lx 42021001')
tss_WZ_lx     = import_tree(tname2_ss % 42021001, *JIDS_MULTI).SetTitle('WZ_lx 42021001 (SS)')

## Z
t_ztaujet     = import_tree(tname2_os % 42100020, *JIDS_MULTI).SetTitle('Ztautau+Jet')
tss_ztaujet   = import_tree(tname2_ss % 42100020, *JIDS_MULTI).SetTitle('Ztautau+Jet (SS)')
t_dymu        = import_tree(tname2_os % 42112011, *JIDS_MULTI).SetTitle('Zmumu')
tss_dymu      = import_tree(tname2_ss % 42112011, *JIDS_MULTI).SetTitle('Zmumu (SS)')
t_zmu17jet    = import_tree(tname2_os % 42112022, *JIDS_MULTI).SetTitle('Zmumu17+Jet')
tss_zmu17jet  = import_tree(tname2_ss % 42112022, *JIDS_MULTI).SetTitle('Zmumu17+Jet (SS)')
t_zmu17cjet   = import_tree(tname2_os % 42112052, *JIDS_MULTI).SetTitle('Zmumu17+cjet')
tss_zmu17cjet = import_tree(tname2_ss % 42112052, *JIDS_MULTI).SetTitle('Zmumu17+cjet (SS)')
t_zmu17bjet   = import_tree(tname2_os % 42112053, *JIDS_MULTI).SetTitle('Zmumu17+bjet')
tss_zmu17bjet = import_tree(tname2_ss % 42112053, *JIDS_MULTI).SetTitle('Zmumu17+bjet (SS)')
t_dye40       = import_tree(tname2_os % 42122001, *JIDS_MULTI).SetTitle('Zee (m40)')
tss_dye40     = import_tree(tname2_ss % 42122001, *JIDS_MULTI).SetTitle('Zee (m40) (SS)')
t_dye10       = import_tree(tname2_os % 42122011, *JIDS_MULTI).SetTitle('DY ee (m10)')
tss_dye10     = import_tree(tname2_ss % 42122011, *JIDS_MULTI).SetTitle('DY ee (m10) (SS)')
t_zbb         = import_tree(tname2_os % 42150000, *JIDS_MULTI).SetTitle('Zbb')
tss_zbb       = import_tree(tname2_ss % 42150000, *JIDS_MULTI).SetTitle('Zbb (SS)')

## W
t_wtaujet    = import_tree(tname2_os % 42300010, *JIDS_MULTI).SetTitle('Wtau+Jet')
tss_wtaujet  = import_tree(tname2_ss % 42300010, *JIDS_MULTI).SetTitle('Wtau+Jet (SS)')
t_wmujet     = import_tree(tname2_os % 42311010, *JIDS_MULTI).SetTitle('Wmu+jet')
tss_wmujet   = import_tree(tname2_ss % 42311010, *JIDS_MULTI).SetTitle('Wmu+jet (SS)')
t_wmu17jet   = import_tree(tname2_os % 42311011, *JIDS_MULTI).SetTitle('Wmu17+jet')
tss_wmu17jet = import_tree(tname2_ss % 42311011, *JIDS_MULTI).SetTitle('Wmu17+jet (SS)')
t_we         = import_tree(tname2_os % 42321000, *JIDS_MULTI).SetTitle('We (incl)')
tss_we       = import_tree(tname2_ss % 42321000, *JIDS_MULTI).SetTitle('We (incl) (SS)')
t_wejet      = import_tree(tname2_os % 42321010, *JIDS_MULTI).SetTitle('We+jet')
tss_wejet    = import_tree(tname2_ss % 42321010, *JIDS_MULTI).SetTitle('We+jet (SS)')
t_wtaubb     = import_tree(tname2_os % 42901000, *JIDS_MULTI).SetTitle('Wtau+bbbar')
tss_wtaubb   = import_tree(tname2_ss % 42901000, *JIDS_MULTI).SetTitle('Wtau+bbbar (SS)')

## HardQCD
t_hardqcd     = import_tree(tname2_os % 49001000, *JIDS_MULTI).SetTitle('HardQCD')
tss_hardqcd   = import_tree(tname2_os % 49001000, *JIDS_MULTI).SetTitle('HardQCD (SS)')
t_cc_hardmu   = import_tree(tname2_os % 49011004, *JIDS_MULTI).SetTitle('ccbar-mu')
tss_cc_hardmu = import_tree(tname2_ss % 49011004, *JIDS_MULTI).SetTitle('ccbar-mu (SS)')
t_bb_hardmu   = import_tree(tname2_os % 49011005, *JIDS_MULTI).SetTitle('bbbar-mu')
tss_bb_hardmu = import_tree(tname2_ss % 49011005, *JIDS_MULTI).SetTitle('bbbar-mu (SS)')
t_cc_harde    = import_tree(tname2_os % 49021004, *JIDS_MULTI).SetTitle('ccbar-e')
tss_cc_harde  = import_tree(tname2_ss % 49021004, *JIDS_MULTI).SetTitle('ccbar-e (SS)')
t_bb_harde    = import_tree(tname2_os % 49021005, *JIDS_MULTI).SetTitle('bbbar-e')
tss_bb_harde  = import_tree(tname2_ss % 49021005, *JIDS_MULTI).SetTitle('bbbar-e (SS)')

# #-------- FALLBACK from incomplete new multi-MC sample
# t_dymu  = import_tree( tname_os, 4314 ).SetTitle('DY mumu (MC)')
# t_dye10 = import_tree( tname_os, 4319 ).SetTitle('DY ee (m10)')
# t_we    = import_tree( tname_os, 4555 ).SetTitle('We (incl)')

# NO-ISOLATION TREES
# To be used for breaking ssfit
# These tree's default "Selection" will pick to noiso cut
t_wtaujet_noiso     = t_wtaujet.Clone()
tss_wtaujet_noiso   = tss_wtaujet.Clone()
t_wmujet_noiso      = t_wmujet.Clone()
tss_wmujet_noiso    = tss_wmujet.Clone()
t_wmu17jet_noiso    = t_wmu17jet.Clone()
tss_wmu17jet_noiso  = tss_wmu17jet.Clone()
t_we_noiso          = t_we.Clone()
tss_we_noiso        = tss_we.Clone()
t_wejet_noiso       = t_wejet.Clone()
tss_wejet_noiso     = tss_wejet.Clone()
if dt == 'mumu':
  t_wmumujet_noiso    = t_wmumujet.Clone()
  tss_wmumujet_noiso  = tss_wmumujet.Clone()
#
t_ztautau0_noiso    = t_ztautau0.Clone()
tss_ztautau0_noiso  = tss_ztautau0.Clone()
t_ztaujet_noiso     = t_ztaujet.Clone()
tss_ztaujet_noiso   = tss_ztaujet.Clone()
t_zmu17jet_noiso    = t_zmu17jet.Clone()
tss_zmu17jet_noiso  = tss_zmu17jet.Clone()
t_dymu_noiso        = t_dymu.Clone()
tss_dymu_noiso      = tss_dymu.Clone()
t_dye10_noiso       = t_dye10.Clone()
tss_dye10_noiso     = tss_dye10.Clone()
t_dye40_noiso       = t_dye40.Clone()
tss_dye40_noiso     = tss_dye40.Clone()


#===============================================================================
# TRIMMED TREES
#===============================================================================

def trimmed_trees(cut, first_only=False):
  """
  Fixed list of trees tailored per channel.
  Suitable for the gallery
  """

  ## Kill numeric bad value, not physic ones.
  filt = utils.join( '_Ntrim', cut )

  ## Specification for sorting index
  by, asc = zip(*{
    'mumu': [
      ('mu1_PT', False),
      ('mu2_PT', False),
    ],
    'ee': [
      ('e1_PT', False),
      ('e2_PT', False),
    ],
    'emu': [
      ('mu_PT', False),
      ('e_PT' , False),
    ],
    'h1mu': [
      ('mu_PT', False),
      ('pi_PT', False),
    ],
    'eh1': [
      ('e_PT' , False),
      ('pi_PT', False),
    ],
    'h3mu': [
      ('mu_PT'        , False),
      ('tau_VCHI2PDOF', True),
      ('tau_PT'       , False),
    ],
    'eh3': [
      ('e_PT'         , False),
      ('tau_VCHI2PDOF', True),
      ('tau_PT'       , False),
    ],
  }[dt])

  # I can safely cut tree once.
  t0 = t_ztautau.drop( filt, by, asc ).SetTitle('Z #rightarrow #tau#tau')

  ## Quick return single tree, used in corr
  if first_only:
    return t0

  ## Prepare common deck
  if 'mu' in dt:
    deck_mu = [
      t0,
      t_wmu17jet.drop ( filt, by, asc ).SetTitle('W+Jet'),
      t_dymu.drop     ( filt, by, asc ).SetTitle('Z #rightarrow #mu#mu'),
      t_cc_hardmu.drop( filt, by, asc ).SetTitle('ccbar'),
      t_bb_hardmu.drop( filt, by, asc ).SetTitle('bbar'),
    ]
  if 'e' in dt:
    deck_e = [
      t0,
      t_we.drop       ( filt, by, asc ).SetTitle('W+Jet'),
      t_dye40.drop    ( filt, by, asc ).SetTitle('Z #rightarrow ee'),
      t_cc_harde.drop ( filt, by, asc ).SetTitle('ccbar'),
      t_bb_harde.drop ( filt, by, asc ).SetTitle('bbar'),
    ]

  ## Return appropriate set
  if dt in ('mumu'):
    res = list(deck_mu)
    res[1] = t_wmumujet.drop ( filt, by, asc ).SetTitle('W+Jet')
    return res
  if dt in ('h1mu', 'h3mu'):
    return deck_mu
  if dt in ('ee', 'eh1', 'eh3'):
    return deck_e
  if dt == 'emu':
    return { 'e': deck_e, 'mu': deck_mu }


#===============================================================================
# ALIAS TREES
#===============================================================================

_trees = {k:v for k,v in dict(locals()).iteritems() if isinstance(v, ROOT.TTree)}
for tname, tree in _trees.iteritems():
  id_utils.apply_alias(ditau_cuts, dtype, tname, tree)
del tname, tree

## also turn off unused-for-sure branches

#===============================================================================
# Misid Zll tree
# This is applied after the aliasing above.
# Note: The new misid tree will also have a branch named "Selection" which is
# a (float) weight of misid rate.
#===============================================================================

## skip these procedure ifu used from 015_HMT
if '015' in os.path.abspath(sys.argv[0]):
  logger.info('Found 015 in execpath, skip lepton-misid trees.')
elif '--skip-misid' in sys.argv:
  logger.info('Found --skip-misid, skip lepton-misid trees.')
elif ('h3' in dt) or (dt in ('mumu', 'ee')):
  logger.info('Channel=%s, skip lepton-misid trees'%dt)
else:
  logger.info('Processing lepton-misid trees, slow.')

  ## Get the misid rate
  import lepton_misid
  misid_rates = lepton_misid.rates()
  reweighter  = lambda t, src: id_utils.reweighting_misid_zll(t, dt, misid_rates[src], 'Selection')

  if dt == 'h1mu':
    t_dimuon_misid   = reweighter(t_dimuon,   'muon_as_hadron')
    tss_dimuon_misid = reweighter(tss_dimuon, 'muon_as_hadron')

  if dt == 'eh1':
    t_dielectron_misid   = reweighter(t_dielectron  , 'elec_as_hadron')
    tss_dielectron_misid = reweighter(tss_dielectron, 'elec_as_hadron')

  if dt == 'emu':
    t_dimuon_misid        = reweighter(t_dimuon      , 'muon_as_elec')
    tss_dimuon_misid      = reweighter(tss_dimuon    , 'muon_as_elec')
    t_dielectron_misid    = reweighter(t_dielectron  , 'elec_as_muon')
    tss_dielectron_misid  = reweighter(tss_dielectron, 'elec_as_muon')


  ## Additional alias of Sel_Comb <-- Selection, ready for caching
  for tname, tree in dict(locals()).iteritems():
    if isinstance(tree, ROOT.TTree) and tname.endswith('misid'):
      id_utils.apply_alias_simple(dt, tree)
      id_utils.apply_alias(ditau_cuts, dtype, tname, tree)
      # tree.SetAlias('Sel_Comb', 'Selection')  ## no longer need. weight is official

  ## discard global vars
  del reweighter
  del misid_rates
  del lepton_misid
  del tname, tree

#===============================================================================
# INDEXING
## Fetch all trees, apply the alias, and index them
#===============================================================================

ALLTREES, ALLTREES_OS, ALLTREES_SS = id_utils.indexing_trees_os_ss(locals())

#===============================================================================

if __name__ == '__main__':
  pass

  # print ALLTREES
  # print ALLTREES_OS['real'].title
  # print ALLTREES_SS

  ## To start interactive shell
  # import IPython; IPython.embed()

  # t_bbbar = t_bb_hardmu
  # t_bbbar = t_bb_harde
  # tree = t_ztautau0

  # print tree.count_ent_evt()
  # print tree.count_ent_evt('Preselection')
  # print tree.count_ent_evt('Selection')

  # print t_ztautau.count_ent_evt('Preselection')
  # print t_ztautau.count_ent_evt('Selection & _Mass')
  # print t_ztautau0.count_ent_evt('Preselection')
  # print t_ztautau0.count_ent_evt('Selection & _Mass')

  # print t_ztautau.count_ent_evt('Preselection       & (mu1_BPVIPCHI2 < 1e4) & (mu2_BPVIPCHI2 < 1e4)')
  # print t_ztautau.count_ent_evt('Selection & _Mass  & (mu1_BPVIPCHI2 < 1e4) & (mu2_BPVIPCHI2 < 1e4)')
  # print t_ztautau0.count_ent_evt('Preselection      & (mu1_BPVIPCHI2 < 1e4) & (mu2_BPVIPCHI2 < 1e4)')
  # print t_ztautau0.count_ent_evt('Selection & _Mass & (mu1_BPVIPCHI2 < 1e4) & (mu2_BPVIPCHI2 < 1e4)')

  ## JHEP: tauh3 eff
  """
  ???
  (7792, 7490)
  (6213, 6010)
  (9961, 6498)
  (3216, 2582)
  (9762, 6505)
  (9762, 6505)
  """
  # print t_ztautau.count_ent_evt('Preselection & _Prongs')
  # print t_ztautau.count_ent_evt('Preselection & _Prongs & (tau_BPVCORRM < 3000)')
  # print t_wmu17jet.count_ent_evt('Preselection & _Prongs')
  # print t_wmu17jet.count_ent_evt('Preselection & _Prongs & (tau_BPVCORRM < 3000)')
  # print tdd_qcd.count_ent_evt('Preselection & _Prongs & _AntiIso')
  # print tdd_qcd.count_ent_evt('Preselection & _Prongs & _AntiIso & (tau_BPVCORRM < 3000)')
