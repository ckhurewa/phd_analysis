#!/usr/bin/env python

"""

zmumu_misid

Focus on the muon-misidentified-as-hadron rate study (mu->h) using tag&probe
method in data. This quantity is used to assert the data-MC agreement in the 
mu->h rate, used for the Z->ll background in the Z->tautau->lh candidates.

"""

from PyrootCK import *
from PyrootCK.mathutils import Eff, sumq

## more tools
from id_utils import *

## Absolute path to storing file
DPATH = os.path.splitext(os.path.abspath(__file__))[0]
FDEST = os.path.join(DPATH, 'zmumu_misid.root')

## Binning spec
BINS = 20, 30, 40, 70

#-------------------------------------------------------------------------------

def categorize():
  """
  Return the TCut string categorize the binning spec
  """
  acc = []
  msg = '%i * ((probe_PT > %ie3) & (probe_PT < %ie3))'
  for i in xrange(0, len(BINS)-1):
    acc.append(msg%(i+1, BINS[i], BINS[i+1]))
  return '+'.join(acc)

def get_label(i):
  return 'Probe PT [%i, %i] GeV'%BINS[i-1:i+1]

def get_tree():
  t = import_tree('PID/Z02MuMu', 5105)

  cut = utils.join([
    'Z0_VCHI2PDOF < 5',
    # 'Z0_DPHI < 2.7',
    'tag_PT > 20e3',
    'tag_ISMUON',
    'tag_TOS_MUON',
    #
    ## Leave this to the categorization below
    # 'probe_PT>20e3',
    # 'probe_PT<35e3',
    #
    # 'probe_ISHADRON',
    # 'probe_ISMUON',
    # 'probe_ISELECTRON',
    #
    # 'tag_0.50_cc_IT   > 0.9',
    # 'probe_0.50_cc_IT > 0.9',
  ])

  param = ':'.join([
    'Z0_M/1e3',
    'probe_ISHADRON',
    categorize(),
  ])

  columns = 'M', 'ishad', 'ptbin'

  t.Draw(param, cut, 'goff para')
  t2 = t.sliced_tree(rename_branches=columns)
  print 'Total entries:', t2.entries
  return t2

#-------------------------------------------------------------------------------

def do_fit(name, x, ds, model):
  ## Report
  print 'Start fitting.'
  print 'subdataset size:', ds.numEntries()

  ## Fit
  res = model.fitTo(ds, RooFit.Save())
  frame = x.frame()
  frame.title = name
  ds.plotOn(frame) # must do this before model to set the normalization
  # model.plotOn(frame)

  ## Some numeric result
  raws  = res.floatParsFinal()
  val0  = ufloat(raws[0].valV, raws[0].error)
  val1  = ufloat(raws[1].valV, raws[1].error)

  ## Plot bkg component
  pdf_prb = model.pdfList()[0] # probe
  pdf_bkg = model.pdfList()[1] # background
  comp    = RooFit.Components(pdf_bkg.name)
  model.plotOn(frame, comp)

  ## Plot the total component as red-dashed (excess)
  line_st = RooFit.LineStyle(ROOT.kDashed)
  line_cl = RooFit.LineColor(ROOT.kRed)
  model.plotOn(frame, line_st, line_cl)

  ## Add annotation
  text = ROOT.TPaveText(.55, .85, .98, .98, 'NDC')
  text.AddText(ds.title)
  text.AddText('probes: {:7.2f}'.format(val1))
  text.AddText('bkg   : {:7.2f}'.format(val0))
  text.textFont = 82
  text.textAlign= 12
  frame.addObject(text)

  ## Do Draw
  frame.Draw()
  ROOT.gPad.Update()

  ## save figure
  ROOT.gPad.SaveAs('zmumu_misid/%s.pdf'%name)

  ## save fit result
  fout = ROOT.TFile(FDEST, 'update')
  res.Write('fitresult_'+name)
  ROOT.gPad.Write(name)
  fout.Close()


#===============================================================================

def main_fitting():
  ## Prepare clean output file
  fout = ROOT.TFile(FDEST, 'recreate')
  fout.Close()

  ## Prepare an input tree
  t   = get_tree()
  n0  = t.entries

  ## Base vars
  mass  = RooFit.RooRealVar('M'    , 'M'    , 70, 110)
  ishad = RooFit.RooRealVar('ishad', 'ishad', 0 , 1)
  ptbin = RooFit.RooRealVar('ptbin', 'ptbin', 0 , 10)
  ds0   = RooFit.RooDataSet('ds0'  , 'ds0'  , t , {mass, ishad, ptbin})


  ## Loop study the fitting
  for ishad in [True, None]:

    ## Adjust drawing bin (not affecting the result)
    mass.bins = 30 if ishad else 60

    for i in xrange(1,len(BINS)):

      # # # ## break & override
      # ishad = None
      # i=3

      ## Prepare appropriate dataset & cut
      if ishad is True:
        name  = 'fit_ishad_%i'%i
        cut   = '(ishad==1) && (ptbin==%i)'%i 
      else:
        name  = 'fit_unprobed_%i'%i
        cut   = 'ptbin==%i'%i
      print cut
      ds = ds0.reduce(cut)
      n  = ds.numEntries()
      ds.title = get_label(i)

      ## Prepare vars
      ## Expo background
      p0 = RooFit.RooRealVar('p0', 'p0', -1, 0. )
      # count: unprobed
      n1 = RooFit.RooRealVar('n1', 'bkg'  , 0, n )
      n2 = RooFit.RooRealVar('n2', 'probe', 0, n )
      # f1 = RooFit.RooRealVar('f1', 'f1', 0, 0.1 )
      # count: ishad
      n3 = RooFit.RooRealVar('n3', 'bkg'  , 0., n       )
      n4 = RooFit.RooRealVar('n4', 'probe', 0., n*0.001 )
      # f2 = RooFit.RooRealVar('f2', 'f2', 0, 1 )

      ## Gauss
      p1 = RooFit.RooRealVar('p1', 'p1', 89, 93 )
      p2 = RooFit.RooRealVar('p2', 'p2', 1., 3.  ) # gauss width

      # ## Crystall-Ball
      p3 = RooFit.RooRealVar('p3', 'p3', 90, 94 ) # gauss position
      p4 = RooFit.RooRealVar('p4', 'p4', 1., 4. ) # gauss width
      p5 = RooFit.RooRealVar('p5', 'p5', 0 , 10 ) # CB threshold (alpha)
      p6 = RooFit.RooRealVar('p6', 'p6', 0 , 1  ) # CB scale (n)

      ## RooVoigtian
      p7 = RooFit.RooRealVar('p7', 'p7', 89, 93  ) # gauss position
      p8 = RooFit.RooRealVar('p8', 'p8', 1., 4.  ) # gauss width
      p9 = RooFit.RooRealVar('p9', 'p9', 1., 4.  ) # sigma

      ## Pdf
      m_expo  = RooFit.RooExponential('m_expo' , 'm_expo' , mass, p0      )
      m_gauss = RooFit.RooGaussian   ('m_gauss', 'm_gauss', mass, p1, p2  )
      m_cb    = RooFit.RooCBShape    ('m_cb'   , 'm_cb'   , mass, p3, p4, p5, p6 )
      m_voig  = RooFit.RooVoigtian   ('m_voig' , 'm_voig' , mass, p7, p8, p9  )

      ## Switch the model      
      if ishad is True:
        model = RooFit.RooAddPdf('model_ishad', 'model_ishad', [m_gauss, m_expo], [n4,n3])
      else:
        if i==3:
          model = RooFit.RooAddPdf('model_main' , 'model_main' , [m_voig, m_expo], [n2,n1])
        else:
          model = RooFit.RooAddPdf('model_main' , 'model_main' , [m_cb, m_expo], [n2,n1])
      
      ## Main method
      do_fit(name, mass, ds, model)

    #   break
    # break

#===============================================================================

@pickle_dataframe_nondynamic
def main_read_results():
  """
  Pick the result from previous method. 
  Need for latex
  """
  fin = ROOT.TFile(FDEST)

  ## Loop over all fit result
  acc = pd.DataFrame(columns=['ptmin', 'ptmax', 'total', 'pass', 'bkg_total', 'bkg_pass'], index=xrange(1,len(BINS)))
  for key in fin.keys:
    if key.name.startswith('fitresult'):
      res = fin.Get(key.name)
      assert res.status()==0, "Fit failed. Check me."

      ## Prepare values to persist
      i     = int(key.name.split('_')[-1])-1
      raws  = res.floatParsFinal()
      bkg   = ufloat(raws[0].getValV(), raws[0].getError())
      val   = ufloat(raws[1].getValV(), raws[1].getError())
      col   = 'pass' if 'ishad' in key.name else 'total'
      #
      acc.iloc[i]['ptmin']    = BINS[i]
      acc.iloc[i]['ptmax']    = BINS[i+1]
      acc.iloc[i][col]        = val
      acc.iloc[i]['bkg_'+col] = bkg

  ## Calculate the uncertainty.
  # Use Clopper-Pearson unc, rounded numerator down to int for conservative err.
  def calc_err(se):
    nume  = se['pass']
    deno  = se['total']
    eff0  = nume / deno
    effcp = Eff(int(deno.n)+1, int(nume.n))
    errl  = sumq([eff0.s, abs(effcp.elow)])
    errh  = sumq([eff0.s, abs(effcp.ehigh)])
    return eff0.n, errl, errh

  raw = acc.apply(calc_err, axis=1)
  acc['rate']   = raw.apply(lambda x: x[0])
  acc['elow']   = raw.apply(lambda x: x[1])
  acc['ehigh']  = raw.apply(lambda x: x[2])
  return acc

def graph():
  """
  Parsed the dataframe result to TGraph, ready to be plotted.
  """
  df  = main_read_results()
  x   = (df.ptmax + df.ptmin)/2
  ex  = (df.ptmax - df.ptmin)/2
  y   = df.rate.apply(uncertainties.nominal_value)
  eyl = df['elow']
  eyh = df['ehigh']
  return ROOT.TGraphAsymmErrors(len(x), x, y, ex, ex, eyl, eyh)

def latex():
  """
  Pretty-format for the latex appendix.
  """
  df0 = main_read_results()
  df  = pd.DataFrame()
  df['Interval'] = df0.apply(lambda se: '[%i, %i]'%(se.ptmin, se.ptmax), axis=1)
  df['Unprobed: $n_\\text{probe}$'] = df0['total'    ].fmt1l
  df['Unprobed: $n_\\text{bkg}$'  ] = df0['bkg_total'].fmt1l
  df['Probed: $n_\\text{probe}$'  ] = df0['pass'     ].fmt2l
  df['Probed: $n_\\text{bkg}$'    ] = df0['bkg_pass' ].fmt1l
  df['Misid rate $\\times 10^4$'  ] = df0.apply(lambda se: '\\tol{%.2f}{%.2f}{%.2f}'%(se.rate*1e4, se.ehigh*1e4, se.elow*1e4), axis=1)
  df = df.set_index('Interval')
  return df.to_markdown()

#===============================================================================

if __name__ == '__main__':
  # print categorize()
  # print get_label(1)
  # main_fitting()
  # print main_read_results()
  # print graph()
  print latex()
