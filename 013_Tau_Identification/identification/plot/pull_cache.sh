rsync -rav --update \
  --include=*/ \
  --include='*/*.df' \
  --include='*/*.root' \
  --exclude=* \
  lpheb:~/analysis/013_Tau_Identification/identification/plot/ .

## TODO: Use rsync
# scp lpheb:~/analysis/013_Tau_Identification/identification/plot/selected/selected.root selected/
# scp -r lpheb:~/analysis/013_Tau_Identification/identification/plot/bkg_samesign/ss bkg_samesign/ss/
