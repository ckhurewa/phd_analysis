#!/usr/bin/env python

import sys
from PyrootCK import import_tree, QHist, ROOT
ROOT.gROOT.SetBatch(True)

JID,QHist.prefix = 2898, 'Z0_8TeV'
# JID,QHist.prefix = ???, 'H2AA_8TeV'

for ttype in ( 'mu', 'e', 'h1' ):
  t_mc    = import_tree( JID, 'CheckTauImpactParameter/mcp_'+ttype )
  t_reco  = import_tree( JID, 'CheckTauImpactParameter/'+ttype     )

  h = QHist()
  h.expansion = [
    ( t_mc    , 'TMath::Log10(MCIP)'          , '' ),
    ( t_reco  , 'TMath::Log10(MIP_norefit)'   , '' ),
    ( t_reco  , 'TMath::Log10(MIP)'           , '' ),
  ]
  h.legends = [ 'MC', 'reco: w/o PVRefit', 'reco: PVRefitted' ]
  h.name    = 'MIP_tau'
  h.suffix  = ttype
  h.xmin    = -4
  h.xmax    = 1
  h.st_line = False
  # h.draw()

  h = QHist()
  h.expansion = [
    ( t_mc    , 'MCSIP'        , '' ),
    ( t_reco  , 'SMIP_norefit' , '' ),
    ( t_reco  , 'SMIP'         , '' ),
  ]
  h.legends = [ 'MC', 'reco: w/o PVRefit', 'reco: PVRefitted' ]
  h.name    = 'SMIP_tau'
  h.suffix  = ttype
  h.ylog    = True
  h.xmin    = -0.5
  h.xmax    = +0.5
  h.st_line = False
  # h.draw()


  #----------------------
  # Shift in uncertainty
  #----------------------

  h = QHist()
  h.name    = 'ImpParUncertainty2_tau'
  h.suffix  = ttype
  h.trees   = t_reco
  h.params  = [ 'TRUNCER2', 'MIPERR2' ]
  h.xmin    = 1E-5
  h.xmax    = 1E-2
  h.xlog    = True
  h.ylog    = True
  # h.draw()

# sys.exit(0)

#=======#
# DITAU #
#=======#

for dtype in ( 'e_e', 'e_h1', 'e_mu', 'h1_mu', 'mu_mu' ):
  t_mc    = import_tree( JID, 'CheckTauImpactParameter/mcp_'+dtype )
  t_reco  = import_tree( JID, 'CheckTauImpactParameter/'+dtype )

  # ## double-check single leg
  # h = QHist()
  # h.expansion = [
  #   ( t_mc  , 'TMath::Log10(MCIP1)'          , '' ),
  #   ( t_reco, 'TMath::Log10(MIP1_norefit)' , '' ),
  #   ( t_reco, 'TMath::Log10(MIP1)'         , '' ),
  # ]
  # h.xmin  = -4
  # h.xmax  = +2
  # h.draw()

  #------------
  # sum( SIP )
  #------------

  h = QHist()
  h.expansion = [
    ( t_mc  , 'MCSIP1        + MCSIP2'        , '' ),
    ( t_reco, 'SMIP1_norefit + SMIP2_norefit' , '' ),
    ( t_reco, 'SMIP1         + SMIP2'         , '' ),
  ]
  h.legends = [ 'MC', 'w/o PVRefit', 'PVRefitted' ]
  h.name    = 'sumSIP_ditau'
  h.suffix  = dtype
  h.ylog    = True
  h.xmin    = -0.5
  h.xmax    = +0.5
  h.xbin    = 50
  h.st_line = False
  # h.draw()

  #-----------------
  # abs(sum( SIP ))
  #-----------------

  h = QHist()
  h.expansion = [
    ( t_mc  , 'TMath::Log10(TMath::Abs(MCSIP1        + MCSIP2))'        , '' ),
    ( t_reco, 'TMath::Log10(TMath::Abs(SMIP1_norefit + SMIP2_norefit))' , '' ),
    ( t_reco, 'TMath::Log10(TMath::Abs(SMIP1         + SMIP2))'         , '' ),
  ]
  h.legends = [ 'MC', 'w/o PVRefit', 'PVRefitted' ]
  h.name    = 'abssumSIP_ditau'
  h.suffix  = dtype
  h.xmin    = -4
  h.xmax    = +2
  h.xbin    = 50
  h.ylog    = True
  h.st_line = False
  # h.draw()

  #--------------------
  # IPS -- with IPCHI2
  #--------------------

  h = QHist()
  # h.master_cuts = (
  #   'MIP1       > 0',
  #   'MIP2       > 0',
  #   'TRUNCER2_1 > 0',
  #   'TRUNCER2_2 > 0',
  # )
  h.trees   = t_reco
  h.params  = 'MIPSIG'
  h.name    = 'Ditau_IPS'
  h.suffix  = dtype
  h.xlog    = False
  h.ylog    = True
  h.xmin    = 0
  h.xmax    = 40
  h.xbin    = 40
  h.draw()


# h = QHist()
# h.trees = t_after
# h.params = 'TMath::Log10(reco_BPVIP) : reco_PTFrac05C'
# h.zlog    = True
# h.xlog   = False
# h.options = 'colz'
# h.draw()

raw_input()