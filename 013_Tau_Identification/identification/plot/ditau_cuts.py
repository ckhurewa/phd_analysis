#!/usr/bin/env python

from PythonCK import logger
from qhist.utils import join
logger.info('Loading cuts: %s'%__file__.replace('/share/lphe',''))


## Prefixes used as name of the branches
# Choose the higher-PT one up front, except for emu
prefixes = {
  'e_e'   : ( 'e1' , 'e2'  ),
  'e_h1'  : ( 'e'  , 'pi'  ),
  'e_h3'  : ( 'e'  , 'tau' ),
  'e_mu'  : ( 'mu' , 'e'   ),
  'h1_mu' : ( 'mu' , 'pi'  ),
  'h3_mu' : ( 'mu' , 'tau' ),
  'mu_mu' : ( 'mu1', 'mu2' ),  
}

#===============================================================================

cuts_nobadrun1 = {'runNumber < 111802', '111890 < runNumber'} # excluded in Zee 8TeV
cuts_nobadrun2 = {'runNumber < 126124', '126160 < runNumber'} # excluded in Zee 8TeV, Zmumu 8TeV
cuts_nobadrun3 = {'runNumber < 129530', '129539 < runNumber'} # excluded in ?

## All the same in all channels
CUTS_PRESEL_NOBADRUN = (
  'nSPDhits<=600',
  cuts_nobadrun1,
  cuts_nobadrun2, 
  cuts_nobadrun3,
)

#===============================================================================
#
# Note: Use join for joining cuts
# TUPLE for AND
# SET   for OR
#
CUTS_PRESELECTION = {
  'e_e': [
    # { 'StrippingZ02TauTau_EXLineDecision', 'StrippingWeLineDecision' }, # OLD
    'StrippingZ02TauTau_EXLineDecision',
    {
      ( 'e1_PT > 20E3', 'e2_PT >  5E3' ),
      ( 'e1_PT >  5E3', 'e2_PT > 20E3' ),
    },
    { 
      ( 'e1_TOS_ELECTRON', 'e1_PT > 20e3' ),
      ( 'e2_TOS_ELECTRON', 'e2_PT > 20e3' ),
    },  
    'e1_PT        > e2_PT',
    'e1_ETA       > 2.0',
    'e2_ETA       > 2.0',
    'e1_ETA       < 4.5',
    'e2_ETA       < 4.5',
    'e1_TRPCHI2   > 0.01',
    'e2_TRPCHI2   > 0.01',
    #
    'e1_PP_InAccEcal',
    'e1_PP_InAccHcal',
    'e2_PP_InAccEcal',
    'e2_PP_InAccHcal',
    #
    'M > 20E3', # no upper mass cut here
    # 'M < 120e3', # OLD
  ],
  'e_h1': [
    # { 'StrippingZ02TauTau_EXLineDecision', 'StrippingWeLineDecision' }, # OLD
    'StrippingZ02TauTau_EXLineDecision',
    'e_TOS_ELECTRON',
    'e_PT       > 20E3',
    'e_ETA      > 2.0',
    'e_ETA      < 4.5',
    'e_TRPCHI2  > 0.01',
    'e_PP_InAccEcal',
    'e_PP_InAccHcal',
    #
    'pi_PT      > 10E3',
    'pi_ETA     > 2.00',
    'pi_ETA     < 4.50',
    'pi_TRPCHI2 > 0.01',
    'pi_PP_InAccHcal',
    #
    'M > 30E3', # no upper mass cut here
    # 'M < 120e3', # OLD
  ],
  'e_h3': [
    # { 'StrippingZ02TauTau_EXLineDecision', 'StrippingWeLineDecision' }, # OLD
    'StrippingZ02TauTau_EXLineDecision',
    'e_TOS_ELECTRON',
    'e_PT       > 20E3',
    'e_ETA      > 2.0',
    'e_ETA      < 4.5',
    'e_TRPCHI2  > 0.01',
    'e_PP_InAccEcal',
    'e_PP_InAccHcal',    
    #
    'pr1_ETA      > 2.00',
    'pr2_ETA      > 2.00',
    'pr3_ETA      > 2.00',
    'pr1_ETA      < 4.50',
    'pr2_ETA      < 4.50',
    'pr3_ETA      < 4.50',
    'pr1_TRPCHI2  > 0.01',
    'pr2_TRPCHI2  > 0.01',
    'pr3_TRPCHI2  > 0.01',
    'pr1_PP_InAccHcal',
    'pr2_PP_InAccHcal',
    'pr3_PP_InAccHcal',
    #
    'tau_PTTRIOMIN  > 1E3',
    'tau_PTTRIOMAX  > 6E3',
    'tau_PT         > 12E3',
    'tau_M          > 700',
    'tau_M          < 1500',
    #
    'M > 30E3', # no upper mass cut here
    # 'M < 120e3', # OLD
  ],
  'e_mu': [
    {
      (
        # 'StrippingZ02TauTau_EXLineDecision || StrippingWeLineDecision', # BUG
        'StrippingZ02TauTau_EXLineDecision',
        'e_PT  > 20E3',
        'mu_PT >  5E3',
      ),
      (
        # 'StrippingZ02TauTau_MuXLineDecision || StrippingWMuLineDecision', # BUG
        'StrippingZ02TauTau_MuXLineDecision',
        'e_PT  >  5E3',
        'mu_PT > 20E3',
      )
    },
    { 
      ( 'e_TOS_ELECTRON', 'e_PT  > 20E3' ),
      ( 'mu_TOS_MUON'   , 'mu_PT > 20E3' ),
    },
    'e_ETA      > 2.0',
    'e_ETA      < 4.5',
    'e_TRPCHI2  > 0.01',
    'e_PP_InAccEcal',
    'e_PP_InAccHcal',
    #
    'mu_ETA     > 2.0',
    'mu_ETA     < 4.5',
    'mu_TRPCHI2 > 0.01',
    #
    'M > 20E3', # no upper mass cut here
    # 'M < 120e3', # OLD
  ],
  'h1_mu': [
    # { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' }, # OLD
    'StrippingZ02TauTau_MuXLineDecision',
    'mu_TOS_MUON',
    'mu_PT      > 20E3',
    'mu_ETA     > 2.0',
    'mu_ETA     < 4.5',
    'mu_TRPCHI2 > 0.01',
    #
    'pi_PT      > 10E3',
    'pi_ETA     > 2.00',
    'pi_ETA     < 4.50',
    'pi_TRPCHI2 > 0.01',
    'pi_PP_InAccHcal',
    #
    'M > 30E3', # no upper mass cut here
    # 'M < 120e3', # OLD
  ],
  'h3_mu': [
    # { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' }, # OLD
    'StrippingZ02TauTau_MuXLineDecision',
    'mu_TOS_MUON',
    'mu_PT      > 20E3',
    'mu_ETA     > 2.0',
    'mu_ETA     < 4.5',
    'mu_TRPCHI2 > 0.01',
    #
    'pr1_ETA      > 2.00',
    'pr2_ETA      > 2.00',
    'pr3_ETA      > 2.00',
    'pr1_ETA      < 4.50',
    'pr2_ETA      < 4.50',
    'pr3_ETA      < 4.50',
    'pr1_TRPCHI2  > 0.01',
    'pr2_TRPCHI2  > 0.01',
    'pr3_TRPCHI2  > 0.01',
    #
    'tau_PTTRIOMIN  > 1E3',
    'tau_PTTRIOMAX  > 6E3',
    'tau_PT         > 12E3',
    'tau_M          > 700',
    'tau_M          < 1500',
    'pr1_PP_InAccHcal',
    'pr2_PP_InAccHcal',
    'pr3_PP_InAccHcal',
    #
    'M > 30E3', # no upper mass cut here
    # 'M < 120e3', # OLD
  ],
  'mu_mu': [
    # { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' }, # OLD
    'StrippingZ02TauTau_MuXLineDecision',
    {
      ( 'mu1_PT > 20E3', 'mu2_PT >  5E3' ),
      ( 'mu1_PT >  5E3', 'mu2_PT > 20E3' ),
    },
    { 
      ( 'mu1_TOS_MUON', 'mu1_PT > 20e3' ),
      ( 'mu2_TOS_MUON', 'mu2_PT > 20e3' ),
    },
    'mu1_PT       > mu2_PT', # to ensure there's no double-counting
    'mu1_ETA      > 2.0',
    'mu2_ETA      > 2.0',
    'mu1_ETA      < 4.5',
    'mu2_ETA      < 4.5',
    'mu1_TRPCHI2  > 0.01',
    'mu2_TRPCHI2  > 0.01',
    #
    'M > 20E3', # no upper mass cut here
    # 'M < 80e3', # OLD
  ]
}

#===============================================================================

## Another layer of selection after kinematic, but before di-tau level.
CUTS_TAUH3_SELECTION = [
  'tau_VCHI2PDOF  < 20',
  'tau_DRoPT      < 0.005',
]

#===============================================================================

CUTS_ISOLATION = {
  'mu_mu': [
    'mu1_0.50_cc_IT   > 0.9',
    'mu2_0.50_cc_IT   > 0.9',
  ],
  'h1_mu': [
    'mu_0.50_cc_IT    > 0.9',
    'pi_0.50_cc_IT    > 0.9',
  ],
  'h3_mu': [
    # 'mu_0.50_cc_vPT   < 2000',
    # 'tau_0.50_cc_vPT  < 3000', # says, the shower
    'mu_0.50_cc_IT    > 0.9',
    'tau_0.50_cc_IT   > 0.9',
  ],  
  'e_e': [
    'e1_0.50_cc_IT    > 0.9',
    'e2_0.50_cc_IT    > 0.9',
  ],
  'e_h1': [
    'e_0.50_cc_IT     > 0.9',
    'pi_0.50_cc_IT    > 0.9',
  ],
  'e_h3': [
    'e_0.50_cc_IT     > 0.9',
    'tau_0.50_cc_IT   > 0.9',
  ],  
  'e_mu': [
    'e_0.50_cc_IT     > 0.9',
    'mu_0.50_cc_IT    > 0.9',
  ],
}

CUT_ANTIISOLATION = join(
  '{0}_0.50_cc_IT   < 0.6',
  '{1}_0.50_cc_IT   < 0.6',
  '{0}_0.50_cc_vPT  > 10E3',
  '{1}_0.50_cc_vPT  > 10E3',
).format

CUTS_ANTIISOLATION = { dtype:CUT_ANTIISOLATION(*p) for dtype,p in prefixes.iteritems() }

cut_mixiso = join(
  '{0}_0.50_cc_IT  > 0.9',
  '{1}_0.50_cc_IT  < 0.6',
  '{1}_0.50_cc_vPT > 10E3',  
).format

CUTS_MIXISOLATION = {
  'e_e'   : { cut_mixiso('e1','e2'), cut_mixiso('e2','e1') },
  'e_h1'  : cut_mixiso('e','pi'),
  'e_h3'  : cut_mixiso('e','tau'),
  'e_mu'  : { cut_mixiso('e','mu'), cut_mixiso('mu','e') },
  'h1_mu' : cut_mixiso('mu','pi'),
  'h3_mu' : cut_mixiso('mu','tau'),
  'mu_mu' : { cut_mixiso('mu1','mu2'), cut_mixiso('mu2','mu1') },
}

#===============================================================================
#
# IMPACT PARAM is reversed in data-driven Z0->mumu shape
#
CUTS_IMPPAR_NORMAL = {
  'mumu': [
    'IP2 > 0.05',
  ],
  'h1mu': [
    'IP2 > 0.03',
  ],
  'h3mu': [
    'tau_BPVLTIME*1e6 > 60', # femtosecond
    'tau_BPVCORRM     < 3000',
  ],
  'ee': [
    'IP2 > 0.05',
  ],
  'eh1': [
    'IP2 > 0.03',
  ],
  'eh3': [
    'tau_BPVLTIME*1e6 > 60',
    'tau_BPVCORRM     < 3000',
  ],
  'emu': [
  ],
}

## Use on tdd_dymu ONLY
CUTS_IMPPAR_PROMPT = [
  ## DISABLE == mimic signal selection
  'mu1_BPVIP   > 0.01',
  'mu1_BPVIP   < 0.3',
  'mu2_BPVIP   > 0.01',
  'mu2_BPVIP   < 0.3',
]

#===============================================================================

CUTS_DITAU_APT = {
  'ee'  : 'APT > 0.1',  
  'mumu': 'APT > 0.1',
  'emu' : 'APT < 0.6',
  'eh1' : '1==1',
  'eh3' : '1==1',
  'h1mu': '1==1',
  'h3mu': '1==1',
}

CUTS_DITAU_DPHI = {dt:'DPHI > 2.7' for dt in CUTS_DITAU_APT}

## Chosen mass window
UPPER_MASS = {
  'mumu': 60,
  'h1mu': 9999,
  'h3mu': 9999,
  'ee'  : 60,
  'eh1' : 9999,
  'eh3' : 9999,
  'emu' : 9999,
  'mue' : 9999,
}
# UPPER_MASS = { # OLD
#   'mumu': 9999,
#   'h1mu': 9999,
#   'h3mu': 9999,
#   'ee'  : 9999,
#   'eh1' : 9999,
#   'eh3' : 9999,
#   'emu' : 9999,
#   'mue' : 9999,
# }
CUTS_UPPER_MASS = {dt: 'M/1e3 < %i'%mass for dt,mass in UPPER_MASS.iteritems()}

#===============================================================================
