#!/usr/bin/env python

"""

Auxiliary plots for background calculation.

"""
## Primary tools
# from PyrootCK import *
import sys
from PyrootCK.mathutils import EffU

## Secondary tools
import id_utils
from id_utils import (
  pd, deco, pickle_dataframe_nondynamic, 
  DTYPE_TO_DT, DTYPES_ALL, df_from_path,
)

## Trees
# from ditau_prep import *

# ## Fast tree, signal only
# import ditau_cuts
# dtype, dt = id_utils.get_current_dtype_dt()
# tname_os = 'DitauCandTupleWriter/%s'%dtype
# t_ztautau = import_tree( tname_os, 5230 ).SetTitle('Zg tautau')
# id_utils.apply_alias(ditau_cuts, dtype, 't_ztautau', t_ztautau)


#===============================================================================
# CHECK DATA NOS/NSS (evt)
#===============================================================================

@pickle_dataframe_nondynamic
def check_data_nos_nss(dt):
  """
  Still used by the normalizer, and result_ssfit_raw.
  """
  return pd.DataFrame({
    'OS': t_real.count_ent_evt  ('Selection'),
    'SS': tss_real.count_ent_evt('Selection'),
  }, index=['ent','evt'])


@pickle_dataframe_nondynamic
def check_data_withmasswindow(dt):
  """
  For normalizer row
  """
  return pd.DataFrame({
    'OS': t_real.count_ent_evt  ('Selection & _Mass'),
    'SS': tss_real.count_ent_evt('Selection & _Mass'),
  }, index=['ent','evt'])


#===============================================================================
# GET SELECTION EFFICIENCIES
# Use for cross-feed
#===============================================================================

@pickle_dataframe_nondynamic
def check_ztau_seleff(dt):
  """
  Return simple esel from true channel.
  Use primarily by HMT for estimating the Ztautau background.

  Note: This is the uncorrected version. The more-complicate corrected version
  is in another script.
  """
  trees = {
    't_ztautau'          : t_ztautau,
    't_ztautau0'         : t_ztautau0,
    't_ztautau0_nofidacc': t_ztautau0_nofidacc,
  }
  cuts = {
    'presel'  : 'Presel_truechan',
    # 'prongs'  : 'Presel_truechan & _Prongs', # detailed below for tauh3
    'iso'     : 'Presel_truechan & _Iso',
    'displ'   : 'Presel_truechan & _Displ',
    'apt'     : 'Presel_truechan & _APT',
    'dphi'    : 'Presel_truechan & _DPHI',
    'mass'    : 'Presel_truechan & _Mass',
    'sel_nomw': 'Sel_truechan',
    'sel_wmw' : 'Sel_truechan & _Mass',
    #
    # MANUAL CUT, use with care
    'iso1': 'Presel_truechan & (ISO1>0.9)',
    'iso2': 'Presel_truechan & (ISO2>0.9)',
  }
  # more cut, manually for h3. This has to be updated manually
  if 'h3' in dt:
    cuts.update({
      'vertex': 'Presel_truechan & (tau_VCHI2PDOF    < 20)',
      'dr'    : 'Presel_truechan & (tau_DRoPT        < 0.005)',
      'ltime' : 'Presel_truechan & (tau_BPVLTIME*1e6 > 60)',
      'corrm' : 'Presel_truechan & (tau_BPVCORRM     < 3000)',
    })
  ## Loop over all
  acc = {}
  for tname, tree in trees.iteritems():
    df = pd.DataFrame({key:tree.count_ent_evt(cut) for key,cut in cuts.iteritems()})
    df.index = ['ent', 'evt']
    acc[tname] = df.T
  return pd.concat(acc)


@pickle_dataframe_nondynamic
def check_ztau0_selected(dt):
  """
  Count the number of selected candidates given that.
  - The Ztautau tree used is central production, hence all channels available
  - with/without fid+acc
  - came from true / other / any channel of origin.

  This is used for checking cross-feed.
  """
  trees = {
    'with_fidacc'   : t_ztautau0,
    'without_fidacc': t_ztautau0_nofidacc,
  }
  ## list of params to sieve out
  params = {'from_'+DTYPE_TO_DT[dtype]: 'Sel_anychan & ditau_type_%s >0'%dtype for dtype in DTYPES_ALL}
  params.update({
    'anychan'  : 'Sel_anychan',
    'otherchan': 'Sel_otherchan',
    'truechan' : 'Sel_truechan',
  })

  ## Loop over 2 mass regimes
  acc0 = {}
  for regime, rcut in {'nomw':'', 'wmw': '_Mass'}.iteritems():
    acc1 = {}
    for key1, tree in trees.iteritems():
      acc2 = {}
      for key2, cut in params.iteritems():
        ent, evt = tree.count_ent_evt(utils.join(rcut, cut))
        acc2[key2] = pd.Series({'ent':ent, 'evt':evt})
      acc1[key1] = pd.concat(acc2)
    acc0[regime] = pd.concat(acc1).unstack()
  return pd.concat(acc0)


@pickle_dataframe_nondynamic
def check_ztau_match(dt):
  """
  Like above, but for the Ztautau of other channel than this prime one.
  """
  cuts = {
    'sel_otherchan'       : 'Sel_otherchan',
    'sel_anychan'         : 'Sel_anychan',
    'sel_anychan_match'   : 'Sel_anychan & MATCH',
    'sel_anychan_nomatch' : 'Sel_anychan & !MATCH',
    'sel_truechan'        : 'Sel_truechan',
    'sel_truechan_match'  : 'Sel_truechan & MATCH',
    'sel_truechan_nomatch': 'Sel_truechan & !MATCH',
  }

  acc = {}
  for k,v in cuts.iteritems():
    nos = t_ztautau.count_ent_evt(v)[1]
    nss = tss_ztautau.count_ent_evt(v)[1]
    acc[k] = nos,nss
  return pd.DataFrame(acc, index=['OS','SS']).T


#===============================================================================
# - GET RAW NUMBER OF MC BG SELECTED ( for quick estimation section )
# - CHECKING CONTAMINATION in DD QCD
# - CHECKING CONTAMINATION in DD MIXISO
#===============================================================================

@deco.concurrent(processes=8)
def _count_worker(tree, cut):
  logger.debug('[%s] %s'%(cut, tree.title))
  ent, evt = tree.count_ent_evt(cut)
  return pd.Series({'ent': ent, 'evt': evt})


@deco.synchronized
def _count_sync(trees_dict, cut):
  """
  Keep it simple
  """
  acc = {}
  gen = sorted(trees_dict.items(), key=lambda kv:kv[1].entries, reverse=True) # largest trees first
  for tname, tree in gen:
    if tname in ('dimuon', 'dielectron'):
      continue
    acc[tname] = _count_worker(tree, cut)
  return pd.DataFrame(acc).stack()

#---

def check_alltrees(cut):
  """
  Perform count of entries/events at given cut for all trees, OS & SS.
  """
  ## calculate
  df_os = _count_sync(ALLTREES_OS, cut)
  df_ss = _count_sync(ALLTREES_SS, cut)
  df    = pd.concat({'OS':df_os, 'SS':df_ss})

  ## Package to legacy format: indices=tname, cols=[[ent,evt], [OS,SS]]
  # fillna=0 for missing tree
  df = df.unstack().reorder_levels([1,0]).sortlevel().T
  return df.fillna(0).applymap(int)


@pickle_dataframe_nondynamic
def check_mc_nos_nss(dt):
  return check_alltrees('Selection')

@pickle_dataframe_nondynamic
def check_sel_masswindow(dt):
  """
  Need this for the latex normalizer table (not necessary for candidates.py)
  """
  return check_alltrees('Selection & _Mass')

@pickle_dataframe_nondynamic
def check_mc_selnoiso(dt):
  return check_alltrees('Sel_noiso')

@pickle_dataframe_nondynamic
def check_antiiso_contamination(dt):
  return check_alltrees('Sel_antiiso')

@pickle_dataframe_nondynamic
def check_dd_mixiso(dt):
  return check_alltrees('Sel_mixiso')

@pickle_dataframe_nondynamic
def check_mc_zllmisid(dt):
  """
  Like to final Selection, but include as well the Zpeak region.
  This is to prove that in the second part of Zll->h1mu background,
  the multiplying number should be very rich in dimuon.

  Technically I should do camoflaging for dimuon but with the rest of selection
  under h1mu cuts. Instead, I'll just do single channel mumu withz (to get PID),
  but exlcude the '_Sel' part with has strong DOCACHI2>20 cut
  """
  if dt not in ('mumu', 'ee'): return
  return check_alltrees('Preselection & _Prongs & _Iso & _Displ')

@pickle_dataframe_nondynamic
def check_antiiso_zllmisid(dt):
  """
  Like above, but reverse to anti-isolated mumu/ee in region including Zpeak.
  """
  if dt not in ('mumu', 'ee'): return
  return check_alltrees('Preselection & _Prongs & _AntiIso & _Displ')


@pickle_dataframe_nondynamic
def check_zpeak_contamination(dt):
  """
  Verify that inside [80,100], [70,100] region we expected negligible signal
  with respect to Zll
  """
  if dt == 'mumu':
    cut = 'Preselection & (M>80e3) & (M<100e3)'
  elif dt == 'ee':
    cut = 'Preselection & (M>70e3) & (M<100e3)'
  else:
    return
  return check_alltrees(cut)


#===============================================================================
# Reporting number of bad run to be excluded
#===============================================================================

def check_bad_run():
  ## https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/BadRuns2012

  def bad(run, dt):
    bad1 = lambda x: 111802 < x and x < 111890
    bad2 = lambda x: 126124 < x and x < 126160
    bad3 = lambda x: 129530 < x and x < 129539
    if dtype in ('ee', 'eh1', 'eh3', 'emu'):
      return bad1(run) or bad2(run) or bad3(run)
    else:
      return bad2(run)

  def read( t, prefix ):
    n = t.Draw('runNumber : eventNumber', 'Selection', 'goff')
    ## Counting 3 quantities here: #run, #evt, #cand
    run_before  = set()
    run_after   = set()
    evt_before  = set()
    evt_after   = set()
    cand_before = 0
    cand_after  = 0
    for i in xrange(n):
      run = t.V1[i]
      evt = (run,t.V2[i])
      run_before.add(run)
      evt_before.add(evt)
      cand_before += 1
      if not bad(run,dt):
        run_after.add(run)
        evt_after.add(evt)
        cand_after += 1

    s = '{}_{:4} = [ ( {:4d}, {:4d}, {:4d} ), ( {:4d}, {:4d}, {:4d} ) ]'
    print s.format( prefix, dt, len(run_before), len(evt_before), cand_before, len(run_after), len(evt_after), cand_after )

  read( t_real  , 'badrun_os' )
  read( tss_real, 'badrun_ss' )

  exit()


#===============================================================================
# QCD: Compare W+Jet and Z+Jet same-sign template
# Aim: If equal, I can use larger one in fitting. It's more accurate that way.
#===============================================================================

def draw():

  param = 'diff_samesign'

  trees = {
    'mumu': [ tss_dymu, tss_zmu17jet, tss_wmumujet ],
    'h1mu': [ tss_dymu, tss_zmu17jet, tss_wmu17jet ],
    'h3mu': [ tss_dymu, tss_zmu17jet, tss_wmu17jet ],
    'ee'  : [ tss_dye10, tss_dye40, tss_wejet ],
    'eh1' : [ tss_dye10, tss_dye40, tss_wejet ],
    'eh3' : [ tss_dye10, tss_dye40, tss_wejet ],
    'emu' : [ tss_dye10, tss_dye40, tss_wejet, tss_dymu, tss_zmu17jet, tss_wmu17jet ],
  }[dt]

  legmu = [ 'Z(#rightarrow #mu#mu) + jet', 'W #rightarrow #mu#nu' ]
  # lege  = [ 'Z(#rightarrow #mu#mu) + jet', 'W #rightarrow #mu#nu' ]
  leg   = {
    'mumu': ['QCD (DD)'] + legmu,
    'h1mu': ['QCD (DD)'] + legmu,
    'h3mu': ['QCD (DD)'] + legmu,
    'ee'  : None,
    'eh1' : None,
    'eh3' : None,
    'emu' : None,
  }[dt]

  exp = [
    ( tssdd_qcd, param, 'Selection' ),
  ] + [ (t,param,'Sel_noiso') for t in trees ]


  QHist.reset()

  h = QHist()
  h.prefix    = dt
  # h.name    = 'shape_ss'
  # h.trees   = tss_wmumujet, tss_wmu17jet, tss_zmu17jet, tss_dymu  # debugging mumu only
  # h.trees   = tssdd_qcd, tss_wmumujet, tss_zmu17jet  # mumu only
  # h.trees   = tssdd_qcd, tss_wmu17jet, tss_zmu17jet
  h.expansion = exp
  h.xlog      = False
  h.xmin      = -100
  h.xmax      = 100
  h.xbin      = 50
  # h.legends   = leg
  # h.xlabel  = '#Delta p_{T} [GeV]'
  h.legends   = [ t.title for t,_,_ in exp ]
  h.draw()

  exit()

# draw()

#===============================================================================
# COMPARE DATA-DRIVEN QCD template
# Aim: To validate that DD QCD shape make sense, compared against MC.
#===============================================================================

def draw(cls):
  QHist.reset()

  h = cls()
  h.params  = 'M/1000'
  h.xmin    = 0
  h.xmax    = 120
  h.xlabel  = 'm [GeV]'
  h.draw()
  #
  h = cls()
  h.params  = 'PT/1000'
  h.xlog    = True
  h.xmin    = 1
  h.xmax    = 100
  h.xlabel  = 'PT [GeV]'
  h.draw()
  #
  h = cls()
  h.params  = 'APT'
  h.xlabel  = 'A_{PT}'
  h.xmin    = 0
  h.xmax    = 1
  h.draw()

  h = cls()
  h.params  = 'DOCACHI2'
  h.xmin    = 1E-3
  h.xmax    = 1E5
  h.xlabel  = '#chi^{2}_{DOCA}'
  h.draw()

  QHist.filters   = 'Presel_antiiso'
  # QHist.filters   = 'Sel_antiiso'
  QHist.xbin      = 30
  # QHist.auto_name = True

  QHist.trees     = tssdd_qcd, tss_cc_hardmu, tss_bb_hardmu
  QHist.prefix    = dt + '_ddqcd_ss'
  # draw(QHist)

  QHist.trees     = tdd_qcd, t_cc_hardmu, t_bb_hardmu
  QHist.prefix    = dt + '_ddqcd_os'
  # draw(QHist)


#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    ## High priority
    print check_data_nos_nss(dt)
    print check_data_withmasswindow(dt)
    print check_ztau_seleff(dt) # for HMT Ztau bkg
    print check_ztau0_selected(dt)

    ## Normalizer context
    print check_mc_nos_nss(dt)
    print check_mc_selnoiso(dt)
    print check_sel_masswindow(dt)

    ## low priority, (cross-check latex normalizer table) can do on second pass
    print check_ztau_match(dt)
    print check_zpeak_contamination(dt)
    print check_antiiso_contamination(dt)
    print check_mc_zllmisid(dt)
    print check_dd_mixiso(dt)
    print check_antiiso_zllmisid(dt)
    sys.exit()

  ## DEV
  dt = 'mumu'
  # print check_data_nos_nss(dt)
  # print check_ztau_seleff(dt)
  # print check_mc_nos_nss(dt)
  # print check_mc_selnoiso(dt)
  print check_antiiso_contamination(dt)

  # print _count_sync(ALLTREES_OS, 'Selection')
  # print check_alltrees('Selection')
  # print 'completed'

  #----------------------

  # for dtype in 'mu_mu', 'h1_mu', 'h3_mu', 'e_e', 'e_h1', 'e_h3', 'e_mu':
  # # for dtype in 'e_mu',:

  #   # tree = import_tree('DitauCandTupleWriter/'+dtype, 5230) # ztautau, local, v2
  #   tree = import_tree('DitauCandTupleWriter/'+dtype, 5234) # ztautau, central, (allegedly) used in v3

  #   ## mumu
  #   # print tree.count_ent_evt("(StrippingWMuLineDecision|StrippingZ02TauTau_MuXLineDecision) & (nSPDhits<=600) & ((runNumber < 111802)|(111890 < runNumber)) & ((runNumber < 126124)|(126160 < runNumber)) & ((runNumber < 129530)|(129539 < runNumber)) & (mu1_TOS_MUON|mu2_TOS_MUON) & (mu1_PT > 20E3) & (mu2_PT > 5E3) & (mu1_TRPCHI2  > 0.01) & (mu2_TRPCHI2 > 0.01) & (M > 20E3) & (M<80e3) & (mu1_0.50_cc_IT>0.9) & (mu2_0.50_cc_IT>0.9) & (mu2_BPVIP > 0.05) & (APT > 0.1) & (DPHI > 2.7) & (mu2_BPVIP<0.5)")
  #   # print tree.count_ent_evt("(StrippingWMuLineDecision|StrippingZ02TauTau_MuXLineDecision) & (nSPDhits<=600) & ((runNumber < 111802)|(111890 < runNumber)) & ((runNumber < 126124)|(126160 < runNumber)) & ((runNumber < 129530)|(129539 < runNumber)) & (mu1_TOS_MUON|mu2_TOS_MUON) & (mu1_PT > 20E3) & (mu2_PT > 5E3) & (mu1_TRPCHI2  > 0.01) & (mu2_TRPCHI2 > 0.01) & (M > 20E3)")

  #   import ditau_cuts
  #   id_utils.apply_alias(ditau_cuts, dtype, 't_ztautau', tree)
  #   d0, d1 = tree.count_ent_evt('Presel_truechan')
  #   n0, n1 = tree.count_ent_evt('Sel_truechan & _Mass') # NEW (V3)
  #   # n0, n1 = tree.count_ent_evt('Sel_truechan') # OLD (V2)
  #   # print d0,d1, '-->', n0, n1
  #   # print n1, '/', d1, '=', EffU(d1, n1)*100, '%'  # nobug
  #   # print n1, '/', d0, '=', '{:.1f}'.format(EffU(d0, n1)*100) # withbug
  #   print n1, '/', d1, '=', '{:.1f}'.format(EffU(d1, n1)*100) # no bug
