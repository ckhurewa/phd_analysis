#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Compute different esel for different trees for the sake of convicing the referee.
To be used for nsig and erec calculation.

"""

from PyrootCK import *
from id_utils import (
  CHANNELS,
  get_current_dtype_dt, pickle_dataframe_nondynamic
) 
from selected import deck_selected_trees_all
import pandas as pd

# cuts_default = {
#   'current' : 'Selection',
# }
cuts = {
  'mumu': {
    'v1_0': 'Preselection & (M<80e3) & _Iso & _DPHI & (IP1>0.01) & (IP1<0.3) & (IP2>0.01) & (IP2<0.3) & (APT<0.6) & (DOCACHI2>20)',
    'v1_1': 'Preselection & (M<80e3) & _Iso & _DPHI & (IP2>0.01) & (IP2<0.3) & (APT<0.6) & (DOCACHI2>20)', # remove IP1
    'v1_2': 'Preselection & (M<80e3) & _Iso & _DPHI & (IP2>0.05) & (IP2<0.5) & (APT<0.6) & (DOCACHI2>20)', # tune IP2
    'v1_3': 'Preselection & (M<80e3) & _Iso & _DPHI & (IP2>0.05) & (IP2<0.5) & (DOCACHI2>20)', # remove upper APT
    'v1_4': 'Preselection & (M<80e3) & _Iso & _DPHI & (IP2>0.05) & (IP2<0.5)', # remove DOCACHI2
    'v1_5': 'Preselection & (M<80e3) & _Iso & _DPHI & (IP2>0.05) & (IP2<0.5) & (APT>0.1)', # Add lower APT
  },
  'ee': {
    'v1_0': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP1>0.01) & (IP1<0.3) & (IP2>0.01) & (IP2<0.3) & (APT<0.6) & (DOCACHI2>10)',
    'v1_1': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.01) & (IP2<0.3) & (APT<0.6) & (DOCACHI2>10)', # Remove IP1
    'v1_2': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.1)  & (IP2<0.5) & (APT<0.6) & (DOCACHI2>10)', # tune IP2
    'v1_3': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.1)  & (IP2<0.5) & (DOCACHI2>10)', # remove upper APT
    'v1_4': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.1)  & (IP2<0.5)', # remove DOCACHI2
    'v1_5': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.1)  & (IP2<0.5) & (APT>0.1)', # Add lower APT
  },
  ## No need. The assist do only one change (IP) between v1,v2.
  # 'emu': {
  #   'v1_0': 'Preselection & (M<120e3) & _Iso & _DPHI & _APT & (IP1>0.01) & (IP1<0.3) & (IP2>0.01) & (IP2<0.3)',
  #   'v1_1': 'Preselection & (M<120e3) & _Iso & _DPHI & _APT',
  # },
  'h1mu': { # == eh1
    'v1_0': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP1>0.01) & (IP1<0.3) & (IP2>0.02) & (IP2<0.3) & (APT<0.6)',
    'v1_1': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.02) & (IP2<0.3) & (APT<0.6)',
    'v1_2': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.03) & (APT<0.6)',
    'v1_3': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP2>0.03)',
  },
  'h3mu': { # == eh3
    'v1_0': 'Preselection & (M<120e3) & _Iso & _DPHI & (IP1>0.01) & (IP1<0.3) & (tau_DRoPT<0.01) & (tau_BPVLTIME*1e6>50) & (tau_DRtPT<4)',
    'v1_1': 'Preselection & (M<120e3) & _Iso & _DPHI & (tau_DRoPT<0.01)  & (tau_BPVLTIME*1e6>50) & (tau_DRtPT<4)',
    'v1_2': 'Preselection & (M<120e3) & _Iso & _DPHI & (tau_DRoPT<0.01)  & (tau_BPVLTIME*1e6>50)',
    'v1_3': 'Preselection & (M<120e3) & _Iso & _DPHI & (tau_DRoPT<0.005) & (tau_BPVLTIME*1e6>50)',
    'v1_4': 'Preselection & (M<120e3) & _Iso & _DPHI & (tau_DRoPT<0.005) & (tau_BPVLTIME*1e6>60)',
  },
}
cuts['eh1'] = cuts['h1mu']
cuts['eh3'] = cuts['h3mu']

#-------------------------------------------------------------------------------

results_v1 = pd.DataFrame({
  'mumu': [ 261,  29.0,  13.9-2.5,   0.0+2.5,  4.4],
  'h1mu': [1357,   1.8, 350.1-153,  51.3+153, 22.0],
  'h3mu': [ 250,   0.0,  54.1-20 ,   9.9+20 , 13.0],
  'ee'  : [ 298, 158.3,  10.4+13 ,  22.6-13 ,  9.6],
  'eh1' : [ 963,  21.7, 383.6+100, 220.0-100, 14.0],
  'eh3' : [ 144,   0.0,  45.1    ,  14.6    ,  6.8],
  'emu' : [ 970,  11.0, 119.1-20 ,  24.0+20 , 56.8],
}, index=['OS','Zll','QCD','EWK','ZXC'])[CHANNELS]

esel_v1 = {
  'mumu': 14.39 * 0.9545,
  'h1mu': 32.07 * 1.0238,
  'h3mu': 29.52 * 0.9925,
  # 'ee'  : 17.02 * 0.8515, # too deviated... Start from v1_2 (tighten IP)
  'ee'  : 17.02,
  'eh1' : 31.15 * 1.0207,
  'eh3' : 26.96 * 1.0029,
}

#===============================================================================
# CACHE RESULTS
#===============================================================================

def get_trees(dt):
  ## Load raw trees, heavy
  import ditau_prep
  deck  = deck_selected_trees_all(dt, as_dict=True)
  procs = 'OS', 'Ztau', 'Zll', 'QCD', 'EWK'
  if dt=='emu':
    procs = 'OS', 'Ztau', 'Zmu', 'Ze', 'QCD', 'EWKmu', 'EWKe'
  return {proc:getattr(ditau_prep,deck[proc]) for proc in procs}

@pickle_dataframe_nondynamic
def calc_single(dt):
  trees = get_trees(dt)
  acc   = {}
  for proc,tree in trees.iteritems():
    logger.info('Tree: %s %r'%(proc, tree))
    for key, cut in cuts[dt].iteritems():
      logger.info(key)
      if proc=='QCD':
        cut = cut.replace('_Iso', '_AntiIso')
      acc[(proc,key)] = tree.count_ent_evt(cut)[1]
  return pd.Series(acc)

#===============================================================================
# PROCESS
#===============================================================================

def main_nsig(dt):
  se = pd.concat({ch:calc_single(ch) for ch in CHANNELS})
  se.index.names = 'channel', 'proc', 'cut'

  df = se.loc[dt].unstack('proc')
  print df
  stages = df.iloc[1:]/df.iloc[0]
  stages = stages.rename(columns={'Ztau':'ZXC'})

  ## fine-tune so that the last stage matches v2
  if dt=='mumu':
    stages.loc[['v1_4','v1_5'], 'Zll'] *= 0.7029 # assuming the large scale factor is weakly reliable
  elif dt=='h1mu':
    stages['OS'] *= 1371./1364 # Offset OS before singlestrip
    stages.loc[:,stages.columns!='OS'] *= 0.9812
  elif dt=='h3mu':
    # stages *= (213./250)*(234./202) # Offset OS before singlestrip
    stages *= 1.03665
  elif dt=='ee':
    stages *= 258./298*356./274 # endswith OS=258
    stages.loc[['v1_2','v1_3'],stages.columns!='OS'] *= 0.3 # low-stats Zee, less reliable
    stages.loc[['v1_4','v1_5'], 'Zll'] *= 0.715 # match 75.7
    pass
    # stages['OS'] *= 258./298*356./274
    # stages.loc[:,stages.columns!='OS'] *= 0.868
  elif dt=='eh1':
    stages.loc[:,stages.columns!='OS'] *= 0.6243
  elif dt=='eh3':
    stages *= 0.9153
  elif dt=='emu': # collapse e+mu, approx the same
    stages['EWK'] = stages[['EWKe','EWKmu']].mean(axis=1)
    stages['Zll'] = stages[['Zmu' ,'Ze'   ]].mean(axis=1)
    stages *= 0.99

  print stages
  # se1 = df.apply(lambda se: se.current_DOCACHI2v1/se.current)
  # se1['ZXC'] = se1['Ztau']
  # print se1

  def get_ztau(se):
    return se.OS-se.Zll-se.QCD-se.EWK-se.ZXC

  se2 = results_v1[dt]
  print get_ztau(se2)
  print se2

  for rname, se1 in stages.iterrows():
    se = pd.concat([se1,se2], axis=1, join='inner').prod(axis=1)
    print rname
    print get_ztau(se)
    print se


def main_esel(dt):
  se = calc_single(dt).loc['Ztau']
  se = se[1:]/se[0]
  print se*esel_v1[dt]

  ## progressively smoothen
  if dt=='ee':
    print se*esel_v1[dt]*pd.np.linspace(1, 0.8515, len(se))


#===============================================================================

if __name__ == '__main__':
  ## Cache result
  # print calc_single(get_current_dtype_dt()[1])

  ## Results
  main_nsig(get_current_dtype_dt()[1])
  # main_esel(get_current_dtype_dt()[1])

  # from ditau_prep import t_real
  # print t_real.count_ent_evt(cuts['ee']['v1_0'])
