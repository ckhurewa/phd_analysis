#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ROOT
from collections import OrderedDict
from uncertainties import ufloat
from qhist import logger, utils

#===============================================================================

def _approx_k(N, mincontent=5.):
  """
  From James, p307.

  lb05  = 0
  lb02  = 0.84
  lb005 = 1.64
  lb001 = 2.33

  """
  b = 2  # suggested 2-4
  l_alpha = 1.64
  l_1_p0  = 0.84
  k = int(b*(2**0.5 * (N-1) / (l_alpha+l_1_p0))**(2./5))

  ## computation
  k = k if (1.*N/k)>mincontent else int(N/mincontent)
  assert k>0, "Bad k-binning computation. Check input"
  return k


def _pvalue(chi2, ndf):
  from scipy.stats import chi2 as Chi2 
  return 1-Chi2.cdf(chi2, ndf)

#===============================================================================

def variable_binning(qhist):
  """
  - name: target pdf filename
  - trees: First one is data (point), the rest are template
  - params: Required 2 params: The variable to plot:weight.
  - normalize: The integral of each histo, with appropriate uncertainty attached.
  - xmin, xmax: The xaxis limit
  - xlabel, ylabel
  - xlog
  """
  ## Importing 4th-party packages
  import pandas as pd
  from uncertainties import nominal_value as Nom
  from uncertainties import std_dev as Sdv

  ## Asserts
  assert utils.dim(qhist.params)==2
  assert len(qhist.trees) == len(qhist.normalize)
  assert qhist.name is not None
  assert qhist.xmin is not None
  assert qhist.xmax is not None

  ## Fixed number of entries, trees (1 data + templates)
  N = qhist.trees[0].entries

  ## Convert each tree into series, with mass & weight, then combine into one.
  acc = []
  for i, (tree, expected) in enumerate(zip(qhist.trees[1:], qhist.normalize[1:])):
    p0  = utils.split_dim(qhist.params)[0]
    cut = '({0} > {1}) & ({0} < {2})'.format(p0, qhist.xmin, qhist.xmax)
    tree.Draw(qhist.params, cut, 'goff')
    df = tree.sliced_dataframe()
    df.columns = ['value', 'weight']
    df['weight'] /= df['weight'].sum()  # renorm sum of weight to one first
    df['weight'] *= Nom(expected)       # then bring it up to the expected val
    acc.append(df)
    logger.info('Tree [size]: %10s [%5i]'%(tree.name, df.size))

  ## Sort by mass, then print accumulate sum of weight
  df = pd.concat(acc).sort_values('value')
  w  = df['weight'].sum()
  df['cum'] = list(df.cumsum()['weight']/w)

  ## Group into equidist cum, correct the edge
  k         = (qhist.xbin or _approx_k(N))   # Appropriate number of bins
  arr       = pd.np.linspace(0., 1., k+1)
  varbin    = [x.iloc[0,0] for _,x in df.groupby(pd.cut(df['cum'], arr))]
  varbin[0] = qhist.xmin-0.01     # nudge left for xaxis label 
  varbin.append(qhist.xmax+0.01)  # nidge right

  logger.info('Entries = nbins * content: %i = %i * %.2f'%(N, k, 1.*N/k))

  ## Prepare the binning histo, to be filled & refilled
  h_fill = ROOT.TH1F('fill', 'fill', k, varbin)

  ## Loop over tree, collect the cloned histogram
  p1,p2      = utils.split_dim(qhist.params)
  histos     = OrderedDict()
  varbin_exp = []
  for i, (tree, expected) in enumerate(zip(qhist.trees, qhist.normalize)):
    name = tree.name
    h_fill.Reset()
    tree.Draw('%s >> fill'%p1, p2, 'goff')
    h = h_fill.Clone('h_'+name)
    h.Scale(Nom(expected)/h.Integral())
    h.ownership   = False
    h.title       = tree.title
    histos[name]  = h
    # also save the expected amount with error
    # discard the error from TH1, rewrite manually from se_exp
    # same relative uncertainty in amm bin, splitted equally
    if i!=0:
      rerr = ufloat(1, (Sdv(expected)**2/k)**0.5/Nom(expected) if Nom(expected)>0 else 0.) 
      se   = h.series().apply(lambda u: u.n*rerr)
      varbin_exp.append(se)
  
  ## Finally, actual drawing
  c  = ROOT.TCanvas('c2', 'c2')
  st = ROOT.THStack('st', '')
  for i,(name, h) in enumerate(histos.iteritems()):
    if i==0:
      continue  # skip the OS histo for now, draw later
    h.markerSize = 0
    h.lineColor  = utils.COLORS(i)
    h.fillColor  = utils.COLORS(i)
    h.fillStyle  = utils.FILLS(i)
    st.Add(h, 'hist')
  st.Draw()

  ## Error of expected number
  h_err = st.stack.Last().Clone('h_err')
  h_err.title = 'Expected'
  h_err.fillColorAlpha = ROOT.kYellow, 0.3
  h_err.fillStyle      = 1001
  h_err.lineWidth      = 0
  h_err.Draw('E2 same')

  ## Data point (observation), slightly different style
  h0 = histos.values()[0]
  h0.title = 'Data'
  h0.binErrorOption = ROOT.TH1.kPoisson
  h0.Draw('PE same')

  ## More styling on the rest, included the Poisson error
  ymax = max(st.maximum, h0.maximum)
  st.maximum           = (ymax + ymax**0.5) * 1.1
  # st.xaxis.labelOffset = -0.01
  st.xaxis.titleOffset = 1.0
  st.xaxis.title       = qhist.xlabel
  st.yaxis.title       = qhist.ylabel
  st.xaxis.moreLogLabels = True
  st.xaxis.noExponent  = True
  #
  leg = ROOT.gPad.BuildLegend()
  leg.ConvertNDCtoPad()
  leg.x1NDC = 0.00
  leg.x2NDC = 0.25
  leg.y1NDC = 0.10
  leg.y2NDC = 0.60
  leg.fillColorAlpha = 0, 0

  ## Compute the Chi2, finally
  vbin_obs  = h0.series()
  vbin_exp  = pd.DataFrame(varbin_exp).sum()
  df   = pd.DataFrame({'obs':vbin_obs, 'exp':vbin_exp})
  func = lambda se: (se.obs.n-se.exp.n)**2 / (se.obs.s**2+se.exp.s**2)
  chi2 = df.apply(func, axis=1).sum()
  pval = _pvalue(chi2, k)

  ## Add chi2 to plot
  text = ROOT.TPaveText(0.01, 0.0, 0.2, 0.1, 'NDC NB')
  text.AddText("#chi^{2} / %i = %.3f"%(k, chi2/k))
  text.fillColor = 4000
  text.Draw()

  ## finally, save
  c.logx        = bool(qhist.xlog)
  c.leftMargin  = 0.24
  c.topMargin   = 0.01
  c.rightMargin = 0.04
  c.Update()
  c.SaveAs(qhist.fullpath_safe)

  ## Return stats
  return {
    'ndf'     : k,
    'chi2'    : chi2,
    'chi2pdof': chi2/k,
    'pval'    : pval,
    'bins'    : varbin,
  }

#===============================================================================
