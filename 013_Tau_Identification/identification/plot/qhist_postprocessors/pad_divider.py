#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import ROOT

#===============================================================================

def pad_divider( qhist, npads=2, keep_first=True ):
  """
  Nice if there are > 3 items superimposed. It'll split into more canvases.

  If keep_first; then the first histogram will always be drawn on every canvas.
  """

  N = len(qhist)
  if keep_first:
    N -= 1
  n = int(math.ceil(1.*N/npads))

  # ROOT.gStyle.SetPaperSize(64,24)

  ## Prepare new blank canvas
  title = qhist.c.title
  # c.Divide( npads, 1 )
  for i in xrange(npads):
    name = 'paddiv%i_%s'%(i,qhist.name)
    c = ROOT.TCanvas(name, title, 600, 450) # Thumbnail 4:3 ratio
    c.ownership = False
    # c.cd(i+1)
    st = ROOT.THStack()
    st.ownership = False
    ## Perform keep_first
    if keep_first:
      st.Add(qhist[0])
      for h in qhist[1+n*i:1+n*(i+1)]:
        st.Add(h)
    else:
      raise NotImplementedError
    st.Draw('nostack')
    ## Steal the title, axis, ...
    st.title = title
    st.xaxis.title =  h.xaxis.title
    ROOT.gPad.BuildLegend()

    ## TODO: Steal Log scale, with more-robust method
    ROOT.gPad.logx = int(qhist.xlog) # ++ method for var bin size

    c.Update()

    ## SAVE
    # pdfpath = os.path.join( qhist.output_path, 'paddiv', qhist.name+'.pdf' )
    # c.SaveAs(pdfpath)

  # return c

#===============================================================================
