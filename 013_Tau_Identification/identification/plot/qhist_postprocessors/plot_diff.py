#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ROOT

#===============================================================================

def plot_diff( qhist ):
  """
  Draw a ratio plot, from qhist[0] against (sum of) qhist[1:]
  """

  c = ROOT.TCanvas()
  c.ownership = False

  h1 = qhist[0].Clone('h1')
  h2 = qhist[1].Clone('h2')
  h1.ownership = False
  h2.ownership = False

  h2.Reset()
  for h in qhist[1:]:
    h2.Add(h)
  h1.Sumw2()
  h1.stats = 0
  h1.markerStyle = 21
  h1.Add(h2, -1)
  h1.Draw('ep')

  c.Update()

#===============================================================================
