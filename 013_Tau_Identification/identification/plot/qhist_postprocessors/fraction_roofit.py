#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ROOT
import uncertainties
from qhist import utils
from qhist.styles import set_lhcb_style_lite, LHCb_SIZE_FIXED, LHCb_FONT_FIXED

#===============================================================================

def roofitresult_to_uarr(res):
  """
  taken from old pyroot_ck, avoid cyclic dependency.
  """
  raws = res.floatParsFinal()
  acc  = [(raws[i].valV, raws[i].error) for i in xrange(len(raws))]
  corr = [list(row) for row in res.correlationMatrix()]
  return uncertainties.correlated_values_norm(acc, corr)

#===============================================================================

def fraction_roofit(qhist):
  """
  Perform fractional fitting, using RooFit as engine.
  See also "Building extended composite models" section in RooFit manual.
  """

  ## CAREFUL, when use to fraction_fit, the style settings are repeats.

  ## Early apply style, LHCb is a good starting point
  set_lhcb_style_lite()

  ## Guard: The input qhist[0] should NOT be normalized (expected int cand)
  if qhist[0].Integral() == 1.00:
    raise ValueError('Need unnormalize QHist instance.')

  ## Make extended pdf
  x       = ROOT.RooRealVar('x', qhist.xlabel, qhist.xmin, qhist.xmax)
  inputs  = []
  pdfs    = []
  numbers = []
  epdfs   = []
  maxnum  = qhist[0].entries
  for i,h in enumerate(qhist):
    s    = str(i)
    dat  = ROOT.RooDataHist ('dat'+s , 'dat'+s , [x] , h     )
    pdf  = ROOT.RooHistPdf  ('pdf'+s , 'pdf'+s , {x} , dat   )
    num  = ROOT.RooRealVar  ('num'+s , 'num'+s , 0.  , maxnum)
    epdf = ROOT.RooExtendPdf('epdf'+s, 'epdf'+s, pdf , num   )
    inputs.append(dat)
    pdfs.append(pdf)
    numbers.append(num)
    epdfs.append(epdf)

  ## Model & fit!
  data  = inputs[0]
  model = ROOT.RooAddPdf('model', 'model', epdfs[1:])
  res   = model.fitTo(data, save=True)

  ## Start visualization
  c    = ROOT.TCanvas('c_fraction_roofit', 'roofit', 800, 480)
  pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.30, 1.0, 1.00)
  pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.00, 1.0, 0.30)
  c.ownership       = False
  pad1.ownership    = False
  pad1.topMargin    = 0.02
  pad1.bottomMargin = 0.02
  pad1.leftMargin   = 0.09
  pad1.rightMargin  = 0.01
  pad1.fillStyle    = 4000
  pad2.ownership    = False
  pad2.TopMargin    = 0.02
  pad2.BottomMargin = 0.45
  pad2.leftMargin   = 0.09
  pad2.rightMargin  = 0.01
  pad2.fillStyle    = 4000
  pad1.Draw()
  pad2.Draw()

  ## Upper pad for stacks
  pad1.cd()
  frame = x.frame(title=qhist.title)
  data.plotOn(frame, name='data', drawOption='X')

  ## Loop over components
  # model.paramOn( frame, data ) #, RooFit.Layout(0.65, 0.98, 0.88))
  model.plotOn(frame, name='model', lineColor=ROOT.kBlack) # need name for chi2
  for i,epdf in enumerate(epdfs):
    model.plotOn(frame,
      components=epdf.name,
      lineWidth=3,
      lineStyle=ROOT.kDashed,
      lineColor=utils.COLORS(i),
    )
  
  ## Finally
  frame.Draw()

  ## Get Chi2 and DOF
  # https://root-forum.cern.ch/t/roofit-goodness-of-fit-chi2-in-unbinned-likelihood/6298/5
  chi2pdof = frame.chiSquare('model', 'data', len(epdfs)-1) # ( curvename, histname )
  obs = ROOT.RooArgSet()
  dof = len(model.getParameters(obs).selectByAttrib("Constant", False))

  ## Draw the data point, by transfering to TGraphAsymmErrors
  ## in order to have Poissonian error but exclude empty bin.
  h0 = ROOT.TGraphAsymmErrors.from_TH1(qhist[0])
  h0.ownership   = False
  h0.markerStyle = 8
  h0.markerSize  = 0.8
  h0.Draw('same P')

  ## Adjust the histo after drawing
  hframe = pad1.primitives[0]
  hframe.yaxis.titleOffset = 0.6
  hframe.xaxis.labelSize   = 0 # hide it, refer in pad2 instead

  ## Legend, add entry manually
  leg = ROOT.TLegend(0.70,0.60,0.98,0.9)
  leg.ownership = False
  leg.AddEntry( frame.getObject(0), 'Data' , 'lP')
  leg.AddEntry( frame.getObject(1), 'Model', 'l' )
  for i in xrange(len(epdfs)-1):
    leg.AddEntry( frame.getObject(i+2), qhist.legends[i+1], 'l' )
  leg.Draw()
  leg.Paint('NDC')
  # reposition
  leg.X1NDC = 0.75
  leg.X2NDC = 0.98
  leg.Y1NDC = 0.96 - leg.NRows*0.10
  leg.Y2NDC = 0.96

  ## Add extra stats labels, if converge:
  success = res.status()==0 and res.numInvalidNLL()==0
  height  = 0.07 * (len(numbers) if success else 1)
  text    = ROOT.TPaveText(.70, leg.Y1NDC-height, .98, leg.Y1NDC, 'NDC')
  text.ownership  = False
  text.fillColor  = 0
  text.fillStyle  = 0
  text.borderSize = 0
  if success:
    text.AddText( "#chi^{2}/%i = %.3f, N = %i"%(dof, chi2pdof, maxnum))
    for x in numbers[1:]:
      text.AddText('%s: %.2f #pm %.2f'%(x.name.replace('num',''), x.getValV(), x.getError()))
  else:
    text.AddText('STATUS ERROR')
  text.Draw()

  ## Pull histogram
  pad2.cd()
  pad2.grid = True
  g = frame.pullHist('data', 'model') # (histname,pdfname)
  g.title = ''
  g.markerSize = 0.8
  #
  g.xaxis.title       = qhist.xlabel
  g.xaxis.titleOffset = 3.0
  g.xaxis.titleSize   = LHCb_SIZE_FIXED
  g.xaxis.titleFont   = LHCb_FONT_FIXED
  g.xaxis.labelSize   = LHCb_SIZE_FIXED
  g.xaxis.labelFont   = LHCb_FONT_FIXED
  #
  g.yaxis.title       = 'Pull'
  g.yaxis.titleOffset = 0.7
  g.yaxis.titleSize   = LHCb_SIZE_FIXED * 0.8
  g.yaxis.titleFont   = LHCb_FONT_FIXED
  g.yaxis.labelSize   = LHCb_SIZE_FIXED * 0.8
  g.yaxis.labelFont   = LHCb_FONT_FIXED
  #
  g.Draw('AP')

  ## Remove empty point, works backward
  for i in reversed(xrange(g.n)): # ROOT index1
    if g.EYlow[i] == 0:
      g.RemovePoint(i)

  ## more customization
  g.xaxis.rangeUser  = qhist.xmin, qhist.xmax
  g.yaxis.ndivisions = 205

  ## Line at y=0
  line = ROOT.TLine(qhist.xmin, 0, qhist.xmax, 0)
  line.ownership = False
  line.lineWidth = 2
  line.Draw()

  ## Computed the array of ufloat, with correlation.
  uarr = roofitresult_to_uarr(res)

  ## Finally
  for pad in pad1, pad2:
    pad.RedrawAxis()
    pad.Modified()
    pad.Update()
  return frame, res, uarr, chi2pdof

#===============================================================================
