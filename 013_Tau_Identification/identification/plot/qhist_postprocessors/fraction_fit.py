#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ROOT
from uncertainties import correlated_values_norm
from qhist import utils, 
from qhist.styles import set_lhcb_style_lite, LHCb_SIZE_FIXED, LHCb_FONT_FIXED
from .make_pullhist import make_pullhist

#===============================================================================

def fraction_fit(qhist, range_data=False, preprocessor=None):
  """
  Wrapper around TFractionFitter for qhist instance.
  This will fit qhist[0] by the rest of histos qhist[1:]

  Args:
    range_data (True, False, None)
      True: Limit the range to histo of data only
      False: Limit the range to cover data & all pdf
      None: To range limit.
    
  """

  ## Early apply style, LHCb is a good starting point
  set_lhcb_style_lite()

  ## Calculation, also getting rangeX min/max
  h0   = qhist[0]
  pdfs = qhist[1:]
  ntot = h0.entries
  arr  = ROOT.TObjArray(len(pdfs))
  arr.ownership = False
  for pdf in pdfs:
    arr.Add(pdf.Clone())
  fitter = ROOT.TFractionFitter(h0.Clone(), arr)
  fitter.ownership = False
  for i in xrange(len(pdfs)):
    fitter.Constrain(i, 0.0, 1.0)

  ## Slim the range
  # >>> Try to automatically slim the rangeX
  if range_data is None:
    pass
  elif range_data is True:
    fitter.rangeX = h0.FindFirstBinAbove(), h0.FindLastBinAbove()
  elif range_data is False:
    rmin = min(h.FindFirstBinAbove() for h in qhist)
    rmax = max(h.FindLastBinAbove() for h in qhist)
    fitter.rangeX = rmin, rmax

  ## Apply preprocessor, if any
  if preprocessor is not None:
    fitter = preprocessor(fitter)

  ## Ready, Go
  res = fitter.Fit()

  ## Start visualization
  c    = ROOT.TCanvas('c_fraction_fit', 'tfrac', 800, 480)
  pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.30, 1.0, 1.0)
  pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.0 , 1.0, 0.3)
  c.ownership       = False
  pad1.ownership    = False
  pad1.topMargin    = 0.02
  pad1.bottomMargin = 0.02
  pad1.leftMargin   = 0.09
  pad1.rightMargin  = 0.01
  pad1.fillStyle    = 4000
  pad2.ownership    = False
  pad2.TopMargin    = 0.02
  pad2.BottomMargin = 0.45
  pad2.leftMargin   = 0.09
  pad2.rightMargin  = 0.01
  pad2.fillStyle    = 4000
  pad1.Draw()
  pad2.Draw()
  pad1.cd()

  ## Draw the data point, by transfering to TGraphAsymmErrors
  ## in order to have Poissonian error but exclude empty bin.
  h0 = ROOT.TGraphAsymmErrors.from_TH1(qhist[0])
  h0.ownership   = False
  h0.markerStyle = 8
  h0.markerSize  = 0.8
  h0.Draw('AP')

  ## Draw the sum (behind comp)
  p = fitter.plot
  p.ownership = False
  p.lineWidth = 3
  p.lineColor = 1
  p.lineStyle = 0
  p.title     = 'Model'
  p.Draw('same')

  ## Collect & Draw component
  frac  = ROOT.Double()
  err   = ROOT.Double()
  list_frac = []
  list_err  = []
  for i,pdf in enumerate(pdfs):
    # collect
    fitter.GetResult(i, frac, err)
    list_frac.append(float(frac))
    list_err.append (float(err))
    # draw
    htemp = fitter.GetMCPrediction(i)
    htemp.ownership = False
    htemp.title     = pdf.title
    htemp.lineWidth = 3
    htemp.lineStyle = 2
    htemp.lineColor = utils.COLORS(i+1)
    intg  = htemp.Integral()
    scale = 0. if intg==0. else (ntot*frac/intg)
    htemp.Scale(scale)
    htemp.Draw('hist same')

  ## Reposition TLegend
  leg = ROOT.gPad.BuildLegend()
  leg.Paint('NDC')
  leg.X1NDC = 0.75
  leg.X2NDC = 0.98
  leg.Y1NDC = 0.96 - leg.NRows*0.10
  leg.Y2NDC = 0.96

  ## Adjust title (after legend drawed)
  h0.title = qhist.title

  ## Adjust height after plot
  ymax = max(h0.Y[i]+h0.EYhigh[i] for i in xrange(h0.N))
  h0.maximum = max([ ymax, p.maximum ])*1.1

  ## Adjust labels
  width = float(qhist.xmax - qhist.xmin) / (qhist.xbin)
  width = str(int(width)) if width.is_integer() else '%.2f'%width
  h0.yaxis.title        = 'Events / (%s)'%width
  h0.yaxis.titleOffset  = 0.6
  h0.xaxis.labelSize    = 0 # hide it, refer in pad2 instead

  ## Calc chi2pdof
  chi2pdof = None
  if int(res)==0: # in C++, 0 is okay
    chi2pdof = fitter.chisquare/(fitter.NDF or 1)

  ## Add some stats on-screen, below legend
  height = 0.07 * (len(list_frac)+1 if int(res)==0 else 1)
  text   = ROOT.TPaveText(.7, leg.Y1NDC-height, .98, leg.Y1NDC, 'NDC')
  text.ownership  = False
  text.fillColor  = 0
  text.fillStyle  = 0
  text.borderSize = 0
  if int(res)==0: # in C++, 0 is okay
    text.AddText( "#chi^{2}/%i = %.3f, N = %i"%(fitter.NDF, chi2pdof, ntot))
    for i,(val,err) in enumerate(zip(list_frac, list_err)):
      text.AddText("%i: %7.2f #pm %6.2f"%(i+1,val*ntot,err*ntot))
  else:
    text.AddText('STATUS ERROR')
  text.Draw()

  ## Calculate pull histogram, a la RooFit
  g = make_pullhist(pad2, h_data=h0, h_model=p)
  g.xaxis.titleOffset = 3.0
  g.xaxis.titleSize   = LHCb_SIZE_FIXED
  g.xaxis.titleFont   = LHCb_FONT_FIXED
  g.xaxis.labelSize   = LHCb_SIZE_FIXED
  g.xaxis.labelFont   = LHCb_FONT_FIXED
  g.yaxis.titleOffset = 0.7
  g.yaxis.titleSize   = LHCb_SIZE_FIXED * 0.8
  g.yaxis.titleFont   = LHCb_FONT_FIXED
  g.yaxis.labelSize   = LHCb_SIZE_FIXED * 0.8
  g.yaxis.labelFont   = LHCb_FONT_FIXED
  g.markerSize        = 0.8
  g.xaxis.title = qhist.params

  ## Computed the array of ufloat, with correlation.
  acc  = [ (res.params[i]*ntot, res.errors[i]*ntot) for i in xrange(res.NPar()) ]
  corr = [list(row) for row in res.correlationMatrix]
  uarr = correlated_values_norm(acc, corr)

  ## Finally
  for pad in pad1, pad2:
    pad.RedrawAxis()
    pad.Modified()
    pad.Update()
  return fitter, res, uarr, chi2pdof

  # n = res.NPar()
  # print 'Chi2  :', res.Chi2()
  # print 'Edm   :', res.Edm()
  # print res.correlationMatrix 
  # print 'errors:', [ res.errors[i] for i in xrange(n) ]
  # print 'params:', [ res.params[i] for i in xrange(n) ]
  # print 'Ndf   :', res.Ndf()
  # print res.NFreeParameters()
  # print res.NPar()
  # print res.NTotalParameters()
  # print 'Params:', [ res.Parameters()[i] for i in xrange(n) ]
  # print res.Prob()

#===============================================================================
