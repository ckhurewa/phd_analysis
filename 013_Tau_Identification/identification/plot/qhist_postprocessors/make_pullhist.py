#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ROOT

#===============================================================================

def make_pullhist(pad, h_data, h_model):
  """
  Return a pad containing pull histo.

  # https://root.cern.ch/doc/master/RooHist_8cxx_source.html#l00702
  # RooPlot.pullHist( histname, pdfname, useAverage=False )
  # residHist( .., .., normalize=true, false )
  # normalize=True, useAverage=False

  Args:
    - pad: TPad instance to be drawn on
    - h_data: TGraphAsymmErrors, preferably with zero-content bins removed.
    - h_model: TH1F

  Note:
  - h_data and h_model should be normalized to be compatible with each other.

  """

  ## Local const
  xmin = h_model.xaxis.xmin
  xmax = h_model.xaxis.xmax

  ## Recieve a pad to draw on
  pad.cd()
  pad.Draw()
  pad.grid = True

  ## Need to make a new copy
  points = []
  X = h_data.X
  Y = h_data.Y
  U = h_data.EYhigh
  L = h_data.EYlow
  for i in xrange(h_data.n):
    x     = X[i]
    y     = Y[i]
    u     = U[i]
    l     = L[i]
    ibin  = h_model.xaxis.FindBin(x)
    y0    = h_model.GetBinContent(ibin)
    dy    = y-y0
    norm  = l if dy>0 else u
    dy   /= norm
    u    /= norm
    l    /= norm
    points.append([x,dy,0.,0.,l,u])

  ## Draw pull
  g = ROOT.TGraphAsymmErrors(len(points), *zip(*points))
  g.UseCurrentStyle()
  g.ownership   = False
  g.title       = ''
  g.markerStyle = 20
  g.xaxis.limits = xmin, xmax
  g.Draw('AP')

  ## customization
  font = h_model.yaxis.labelFont # inherit
  g.yaxis.ndivisions  = 205
  g.yaxis.labelSize   = 0.2
  g.yaxis.labelFont   = font
  g.yaxis.titleOffset = 0.15
  g.yaxis.titleSize   = 0.20
  g.yaxis.titleFont   = font
  g.yaxis.title       = 'Pull'
  #
  g.xaxis.title = h_model.xaxis.title

  ## Line at y=0
  line = ROOT.TLine( xmin, 0, xmax, 0 )
  line.ownership = False
  line.lineWidth = 2
  line.Draw()

  return g

#===============================================================================
