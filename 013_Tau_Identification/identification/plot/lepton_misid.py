#!/usr/bin/env python

"""

Dive in detail the lepton misidentification, in data and MC, both e/mu.

"""

__all__ = 'rates', 'rates_detailed'

from PyrootCK import *
from math import log10
from id_utils import memorized

## tag & probe from data, same one as muon/electron PID eff
# JIDS_DATA = 5027, 5028, 5029, 5036
JIDS_DATA = 5093, # retry with rechecked selection

## MC samples
JID_MC_ZMUMU = 5083
JID_MC_ZEE   = 5084


#===============================================================================
# CUTS
#===============================================================================

sel_fmt = utils.join(
  'DPHI > 2.7',
  'Z0_M > 80e3',
  'Z0_M < 100e3',
  'Z0_VCHI2PDOF < 5',
  #
  'tag_IS{0}',
  'tag_TOS_{0}',
  'tag_ETA        > 2.0',
  'tag_ETA        < 4.5',
  'tag_PT         > 20e3',
  'tag_TRPCHI2    > 0.01',
  'tag_0.50_cc_IT > 0.9',
  'tag_P > 0',
  'tag_PERR2/tag_P**2 < 0.01', # new
  #
  'probe_ETA        > {1}',
  'probe_ETA        < {2}',
  'probe_TRPCHI2    > 0.01',
  'probe_0.50_cc_IT > 0.9',
  # 'probe_BPVIP > 0.02', # TODO
  # 'probe_BPVIP < 0.3',
  'probe_P > 0',
  'probe_PERR2/probe_P**2 < 0.01', # new

).format

sel_muon_misid_etanarrow  = sel_fmt( 'MUON'    , 2.25, 3.75 )
sel_muon_misid_etanormal  = sel_fmt( 'MUON'    , 2.0 , 4.5  )
sel_elec_misid_etanarrow  = sel_fmt( 'ELECTRON', 2.25, 3.75 )
sel_elec_misid_etanormal  = sel_fmt( 'ELECTRON', 2.0 , 4.5  )

#===============================================================================
# ALIASES
#===============================================================================

def aliasing_data(t):
  t.SetAlias('DPHI', 'Z0_DPHI')
  t.SetAlias('tag_TOS_ELECTRON' , 'tag_TOS_ELEC')

  t.SetAlias('PRESEL_MUON_AS_HADRON', sel_muon_misid_etanarrow)
  t.SetAlias('PRESEL_MUON_AS_ELEC'  , sel_muon_misid_etanormal)
  t.SetAlias('PRESEL_MUON_AS_MUON'  , sel_muon_misid_etanormal)
  t.SetAlias('PRESEL_ELEC_AS_HADRON', sel_elec_misid_etanarrow)
  t.SetAlias('PRESEL_ELEC_AS_ELEC'  , sel_elec_misid_etanormal)
  t.SetAlias('PRESEL_ELEC_AS_MUON'  , sel_elec_misid_etanormal)


def aliasing_mc_tap(t):
  ## missing, yield true (<5) for now
  t.SetAlias('Z0_VCHI2PDOF', '1+1')

  t.SetAlias('tag_TOS_ELECTRON' , 'tag_TOS_ELEC')
  t.SetAlias('tag_ISMUONLOOSE'  , 'tag_ISLOOSEMUON'  )
  t.SetAlias('probe_ISMUONLOOSE', 'probe_ISLOOSEMUON')
  t.SetAlias('probe_ISHADRON', utils.join(
    '!probe_ISMUONLOOSE',
    'probe_HCALFrac > 0.05', 
  ))
  t.SetAlias('probe_ISELECTRON', utils.join( 
    '!probe_ISMUONLOOSE',
    'probe_HCALFrac     < 0.05', 
    'probe_ECALFrac     > 0.1', 
    'probe_PP_CaloPrsE  > 50',
  ))
  t.SetAlias('tag_ISELECTRON', utils.join( 
    '!tag_ISMUONLOOSE',
    'tag_HCALFrac < 0.05', 
    'tag_ECALFrac > 0.1', 
    'tag_PP_CaloPrsE > 50',
  ))

  t.SetAlias('PRESEL_MUON_AS_HADRON', sel_muon_misid_etanarrow)
  t.SetAlias('PRESEL_MUON_AS_ELEC'  , sel_muon_misid_etanormal)
  t.SetAlias('PRESEL_MUON_AS_MUON'  , sel_muon_misid_etanormal)
  t.SetAlias('PRESEL_ELEC_AS_HADRON', sel_elec_misid_etanarrow)
  t.SetAlias('PRESEL_ELEC_AS_ELEC'  , sel_elec_misid_etanormal)
  t.SetAlias('PRESEL_ELEC_AS_MUON'  , sel_elec_misid_etanormal)


def aliasing_mc_mcp(t):
  ## missing, yield true (<5) for now
  t.SetAlias('Z0_VCHI2PDOF', '1+1')
  t.SetAlias('tag_PERR2'   , 'PERR2')
  t.SetAlias('tag_P'       , 'P')
  t.SetAlias('probe_PERR2' , '0+0')
  t.SetAlias('probe_P'     , '0+1')


  t.SetAlias('probe_PT' , 'PT')
  t.SetAlias('probe_ETA', 'ETA')
  t.SetAlias('probe_BPVIP', 'BPVIP')
  t.SetAlias('probe_ISMUON', 'ISMUON')
  t.SetAlias('probe_ISHADRON', utils.join(
    '!ISLOOSEMUON',
    'HCALFrac > 0.05', 
  ))
  t.SetAlias('probe_ISELECTRON', utils.join( 
    '!ISLOOSEMUON',
    'HCALFrac     < 0.05', 
    'ECALFrac     > 0.1', 
    'PP_CaloPrsE  > 50',
  ))

  eta_generic = utils.join(
    'nSPDhits < 600',
    'ETA  > {0}',
    'ETA  < {1}',
    'TRPCHI2 > 0.01',
    #
    'lep_0.50_cc_IT>0.9',
    'BPVIP > 0.02',
    'BPVIP < 0.3',

  ).format
  eta_narrow = eta_generic(2.25, 3.75)
  eta_normal = eta_generic(2.00, 4.50)

  t.SetAlias('PRESEL_MUON_AS_HADRON', eta_narrow)
  t.SetAlias('PRESEL_MUON_AS_ELEC'  , eta_normal)
  t.SetAlias('PRESEL_MUON_AS_MUON'  , eta_normal)
  t.SetAlias('PRESEL_ELEC_AS_HADRON', eta_narrow)
  t.SetAlias('PRESEL_ELEC_AS_ELEC'  , eta_normal)
  t.SetAlias('PRESEL_ELEC_AS_MUON'  , eta_normal)

#===============================================================================

def load_trees():
  t_muon_data = import_tree( 'PID/Z02MuMu', *JIDS_DATA ).SetTitle('data')
  t_elec_data = import_tree( 'PID/Z02ee'  , *JIDS_DATA ).SetTitle('data')

  t_muon_mc_mcp = import_tree( 'LeptonMisidCheck/Z02mumu_biased'  , JID_MC_ZMUMU ).SetTitle('unbias (MCP)')
  t_muon_mc_tap = import_tree( 'LeptonMisidCheck/Z02mumu_unbiased', JID_MC_ZMUMU ).SetTitle('biased (t&p)')
  t_elec_mc_mcp = import_tree( 'LeptonMisidCheck/Z02ee_biased'    , JID_MC_ZEE   ).SetTitle('unbias (MCP)')
  t_elec_mc_tap = import_tree( 'LeptonMisidCheck/Z02ee_unbiased'  , JID_MC_ZEE   ).SetTitle('biased (t&p)')

  trees_elec = t_elec_data, t_elec_mc_tap, t_elec_mc_mcp
  trees_muon = t_muon_data, t_muon_mc_tap, t_muon_mc_mcp

  ## do some aliasing
  aliasing_data(t_muon_data)
  aliasing_data(t_elec_data)
  aliasing_mc_tap(t_muon_mc_tap)
  aliasing_mc_tap(t_elec_mc_tap)
  aliasing_mc_mcp(t_muon_mc_mcp)
  aliasing_mc_mcp(t_elec_mc_mcp)

  return locals()

#===============================================================================

def main_misid_scalar():
  """
  Return the scalar value, no binning. For quick check.
  """
  t = t_muon_data
  d = t.GetEntries('PRESEL_MUON_AS_HADRON')
  n = t.GetEntries('PRESEL_MUON_AS_HADRON & probe_ISHADRON')
  print n, d, 1.*n/d


#===============================================================================

def main_draw_misid_raw():
  """
  Plot the raw figure, as a based for binned misid calculation.
  """

  QHist.reset()
  QHist.xbin      = 6
  QHist.xmin      = 10
  QHist.xmax      = 70
  QHist.ymax      = 1
  QHist.options   = 'prof'
  QHist.st_line   = False
  QHist.st_marker = True

  h = QHist()
  h.name    = 'muon_as_hadron'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_muon
  h.params  = 'probe_ISHADRON: probe_PT/1e3'
  h.xbin    = 6
  h.xmin    = 10
  h.ymin    = 1e-6
  h.xmax    = 100
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'muon_as_hadron'
  h.suffix  = 'detailed'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_muon
  h.params  = 'probe_ISHADRON: probe_PT/1e3'
  h.xbin    = 10
  h.xmin    = 5
  h.xmax    = 100
  h.ymin    = 1e-6
  h.xlog    = True
  h.ylog    = True
  h.draw()


  h = QHist()
  h.name    = 'muon_as_elec'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_muon
  h.params  = 'probe_ISELECTRON: probe_PT/1e3'
  h.xbin    = 6
  h.xmin    = 10
  h.xmax    = 100
  h.ymin    = 1e-6
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'muon_as_elec'
  h.suffix  = 'detailed'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_muon
  h.params  = 'probe_ISELECTRON: probe_PT/1e3'
  h.xbin    = 20
  h.xmin    = 5
  h.xmax    = 100
  h.ymin    = 1e-6
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'muon_as_muon'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_muon
  h.params  = 'probe_ISMUON: probe_PT/1e3'
  h.ymin    = 0.7
  h.xbin    = 24
  # h.draw()

  h = QHist()
  h.name    = 'elec_as_hadron'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_elec
  h.params  = 'probe_ISHADRON: probe_PT/1e3'
  h.xbin    = 6
  h.xmin    = 10
  h.xmax    = 100
  h.ymin    = 1e-5
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'elec_as_hadron'
  h.suffix  = 'detailed'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_elec
  h.params  = 'probe_ISHADRON: probe_PT/1e3'
  h.xbin    = 20
  h.xmin    = 5
  h.xmax    = 100
  h.ymin    = 1e-6
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'elec_as_muon'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_elec
  h.params  = 'probe_ISMUON: probe_PT/1e3'
  h.xbin    = 6
  h.xmin    = 10
  h.xmax    = 100
  h.ymin    = 1e-5
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'elec_as_muon'
  h.suffix  = 'detailed'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_elec
  h.params  = 'probe_ISMUON: probe_PT/1e3'
  h.xbin    = 20
  h.xmin    = 5
  h.xmax    = 100
  h.ymin    = 1e-6
  h.xlog    = True
  h.ylog    = True
  # h.draw()

  h = QHist()
  h.name    = 'elec_as_elec'
  h.filters = 'PRESEL_'+h.name.upper()
  h.trees   = trees_elec
  h.params  = 'probe_ISELECTRON: probe_PT/1e3'
  h.ymin    = 0.7
  h.xbin    = 24
  # h.draw()

  ## 2D plot separately
  for t,s in zip(trees_muon, ['data', 'mctap', 'mcp']):
    h = QHist()
    h.name    = 'muon_as_hadron'
    h.suffix  = 'etapt_'+s
    h.filters = 'PRESEL_'+h.name.upper()
    h.trees   = t
    h.params  = 'probe_ISHADRON: probe_ETA: probe_PT/1e3'
    h.ymin    = 2.25
    h.ymax    = 3.75
    h.ybin    = 3
    h.options = 'prof colz texte'
    # h.draw()

    h = QHist()
    h.name    = 'muon_as_elec'
    h.suffix  = 'etapt_'+s
    h.filters = 'PRESEL_'+h.name.upper()
    h.trees   = t
    h.params  = 'probe_ISELECTRON: probe_ETA: probe_PT/1e3'
    h.ymin    = 2.0
    h.ymax    = 4.5
    h.ybin    = 5
    h.options = 'prof colz texte'
    # h.draw()

#===============================================================================

def main_draw_misid_mc():
  """
  Note: Draw in MeV to be compat (less problems) with reweighting stage
  """

  trees = load_trees()
  t_elec_mc_mcp = trees['t_elec_mc_mcp']  
  t_muon_mc_mcp = trees['t_muon_mc_mcp']  

  QHist.reset()
  QHist.xbin      = 10  # low nbin to avoid zero-content bin
  QHist.xmin      = 5e3
  QHist.xmax      = 80e3
  QHist.xlog      = True
  QHist.ylog      = True
  QHist.ymax      = 1
  QHist.ymin      = 1e-6
  QHist.options   = 'prof'
  QHist.st_line   = False
  QHist.st_marker = True
  QHist.legends   = '#mu #rightarrow h', 'e #rightarrow h', '#mu #rightarrow e', 'e #rightarrow #mu'

  ## First one in MeV for the computation
  h = QHist()
  h.name = 'misid_mc'
  h.expansion = [
    ( t_muon_mc_mcp, 'probe_ISHADRON  : probe_PT', 'PRESEL_MUON_AS_HADRON'),
    ( t_elec_mc_mcp, 'probe_ISHADRON  : probe_PT', 'PRESEL_ELEC_AS_HADRON'),
    ( t_muon_mc_mcp, 'probe_ISELECTRON: probe_PT', 'PRESEL_MUON_AS_ELEC'  ),
    ( t_elec_mc_mcp, 'probe_ISMUON    : probe_PT', 'PRESEL_ELEC_AS_MUON'  ),
  ]
  h.draw()

  ## Another one in GeV for figure
  h = QHist()
  h.name = 'misid_mc_gev'
  h.xmin = h.xmin/1e3
  h.xmax = h.xmax/1e3
  h.expansion = [
    ( t_muon_mc_mcp, 'probe_ISHADRON  : probe_PT/1e3', 'PRESEL_MUON_AS_HADRON'),
    ( t_elec_mc_mcp, 'probe_ISHADRON  : probe_PT/1e3', 'PRESEL_ELEC_AS_HADRON'),
    ( t_muon_mc_mcp, 'probe_ISELECTRON: probe_PT/1e3', 'PRESEL_MUON_AS_ELEC'  ),
    ( t_elec_mc_mcp, 'probe_ISMUON    : probe_PT/1e3', 'PRESEL_ELEC_AS_MUON'  ),
  ]
  h.draw()

  QHist.reset()

#===============================================================================
# CORRECTION
# now depreciated
#===============================================================================

def _scaleup_correction(name):
  """
  Provide extra correction to muon-misid-as-hadron
  """
  fin   = ROOT.TFile('lepton_misid/lepton_misid.root')
  c     = fin.Get(name)

  ## grab the source from less-binning 
  h_data  = c.FindObject(name+'_hc_00')
  h_mcp   = c.FindObject(name+'_hc_02')
  ibin    = h_data.FindBin(45.) - 1
  v_data  = h_data.series()[ibin]
  v_mcp   = h_mcp.series()[ibin]
  print '{:.6f}'.format(v_data), '{:.6f}'.format(v_mcp)

  # ## do average in log scale, obtain the correction factor needed
  # # Approach to uncertainty: Do the relative err in log space first,
  # # then once convert to normal space, retain the worse side (conserative).
  # n1,n2   = log10(v_data.n), log10(v_mcp.n)
  # nmean   = (n1+n2)/2
  # rerrlog = abs(abs(n1-n2)/2/nmean)
  # rerr    = (10**(1+rerrlog)-10)/10 * 2 # mult by 2 to be super consv
  # print n1, n2, nmean, rerrlog, rerr, nmean*(1.+rerr), nmean*(1.-rerr)

  # rerr_d  = ufloat(1., v_data.s/v_data.n)
  # rerr_m  = ufloat(1., v_mcp.s /v_mcp.n )
  # vnew    = 10**nmean * ufloat(1., rerr) * rerr_d * rerr_m
  # corr    = vnew / v_mcp
  # print '{:.6f}'.format(vnew)
  # print '{:.6f}'.format(corr)
  corr    = v_data / v_mcp

  ## Apply the correction factor to mcp, fill to new histo
  c       = fin.Get(name+'_detailed').Clone()
  c.name  = name+'_corrected'
  c.title = name+'_corrected'
  pad     = c.FindObject('upperPad')
  h_mcp0  = c.FindObject(name+'_detailed_hc_02')
  se_mcp0 = h_mcp0.series()
  se_mcp  = se_mcp0 * corr
  h_mcp   = h_mcp0.Clone('h_mcp').ProjectionX()
  h_mcp.name = name+'_detailed_hc_03'
  h_mcp.Reset()
  for i,val in enumerate(se_mcp):
    h_mcp.SetBinContent(i+1, val.n)
    h_mcp.SetBinError  (i+1, val.s)

  ## Draw & decorate. Save overriding the old canvas, just having 4th member.
  c.Draw()
  # while c.FindObject(name+'_detailed_hc_03'):
  #   x = c.FindObject(name+'_detailed_hc_03')
  #   x.Delete()

  pad.cd()  
  h_mcp.title     = 'Corrected misid'
  h_mcp.lineColor = ROOT.kGreen+2
  h_mcp.lineStyle = 2
  h_mcp.Draw('sames')
  c.SaveAs('lepton_misid/%s_corrected.pdf'%name)
  fout  = ROOT.TFile('lepton_misid/lepton_misid.root', 'UPDATE')
  c.Write('', ROOT.TObject.kOverwrite)
  fout.Close()


def MH_misid_correction():
  _scaleup_correction('muon_as_hadron')
def ME_misid_correction():
  _scaleup_correction('muon_as_elec')
def EH_misid_correction():
  _scaleup_correction('elec_as_hadron')
def EM_misid_correction():
  _scaleup_correction('elec_as_muon')


#===============================================================================

def get_misid_rate(hname):
  """
  Retrieve the TProfile rate from persisted file 
  """
  fin   = ROOT.TFile('lepton_misid/lepton_misid.root')
  cname = hname.replace('detailed', 'corrected') # limitation that I cloned canvas
  c     = fin.Get(cname)
  return {
    'data'    : c.FindObject(hname+'_hc_00'),
    'mctap'   : c.FindObject(hname+'_hc_01'),
    'mcp'     : c.FindObject(hname+'_hc_02'),
    'mcpcorr' : c.FindObject(hname+'_hc_03'),
  }

#===============================================================================

def draw_misid_MH_compare():
  """
  Plot the misid as a function of PT for the latex figure.
  Focus on mu->h and compare between 3 kinds: MC truth, MC T&P, data T&P
  """
  ## get the source
  import zmumu_misid
  prof  = get_misid_rate('muon_as_hadron_detailed')['mcp']
  mctap = get_misid_rate('muon_as_hadron_detailed')['mctap']
  ddtap = zmumu_misid.graph()

  ## Force style
  ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
  ROOT.gStyle.lineWidth = 1
  ROOT.gStyle.frameLineWidth = 1

  ## Draw
  c = ROOT.TCanvas('c', 'c', 960, 320)
  c.Draw()
  prof.UseCurrentStyle()
  prof.Draw('PE')
  mctap.Draw('same')
  ddtap.Draw('PE same')

  ## Components stylize
  prof.title  = 'MC truth'
  mctap.title = 'Tag & probe (MC)'
  ddtap.title = 'Tag & probe (data)'
  prof.lineColor        = ROOT.kBlack
  ddtap.lineColor       = ROOT.kBlue
  ddtap.lineWidth       = 2
  ddtap.fillColorAlpha  = 0, 0
  ddtap.markerStyle     = 5
  ddtap.markerColor     = ROOT.kBlue

  ## More Stylize
  prof.xaxis.title        = 'p_{T} [GeV]'
  prof.xaxis.titleOffset  = 1.06
  prof.xaxis.titleSize    = 0.09
  prof.xaxis.labelSize    = 0.07
  prof.xaxis.rangeUser    = 5.1, 70
  prof.yaxis.title        = 'Misidentification rate'
  prof.yaxis.titleOffset  = 0.54
  prof.yaxis.titleSize    = 0.08
  prof.yaxis.labelSize    = 0.07
  prof.maximum    = 1.0
  prof.minimum    = 1e-6
  c.leftMargin    = 0.08
  c.bottomMargin  = 0.21
  c.rightMargin   = 0.02
  c.logx  = True
  c.logy  = True
  c.gridx = True
  c.gridy = True

  ## Legend
  leg = ROOT.gPad.BuildLegend()
  # leg.fillColor = 0
  # leg.fillStyle = 4000
  leg.header = 'LHCb-Preliminary'
  leg.X1 = 0.70
  leg.X2 = 0.98
  leg.Y1 = 0.60
  leg.Y2 = 0.94
  leg.Draw()

  ## Finally
  c.RedrawAxis()
  c.Update()
  c.SaveAs('lepton_misid/misid_MH_compare.pdf')



#===============================================================================

def draw_misid_with_pt():
  """
  Do this after getting trees from ditau_background.py

  Draw the misid overlaid with PT distribution of the candidates.
  """
  ROOT.gROOT.SetBatch(True)

  queues = [
    ('muon_as_hadron', '_detailed', 'mcp', 'pi_PT', 'muon-misid-as-hadron rate'     ),
    ('muon_as_elec'  , '_detailed', 'mcp', 'e_PT' , 'muon-misid-as-electron rate'   ),
    ('elec_as_hadron', '_detailed', 'mcp', 'pi_PT', 'electron-misid-as-hadron rate' ),
    ('elec_as_muon'  , '_detailed', 'mcp', 'mu_PT', 'electron-misid-as-muon rate'   ),
  ]

  for basename, binning, src, bname, title in queues:
    fnameout = 'comb_'+title.replace(' ','-')

    ## Get the rate
    h_rate = get_misid_rate(basename+binning)[src].Clone('h_rate')

    ## Get the weight, bin to the TProfile
    fname = 'ditau_background/ditau_background.root'
    tname = basename+'_OS'
    tree  = import_tree(tname, fname)
    if isinstance( h_rate, ROOT.TProfile ):
      h_pt  = h_rate.ProjectionX()
      h_pt.name = 'h_pt_'+basename
    else:
      h_pt = h_rate.Clone('h_pt_'+basename)
    h_pt.Reset()
    tree.Draw( bname+'/1e3 >> '+h_pt.name)

    ## Prep & draw first histo
    c = ROOT.TCanvas( fnameout, fnameout, 720, 480 )
    h_rate.title = ''
    h_rate.Draw('PE')
    h_rate.lineColor          = ROOT.kBlue
    h_rate.lineStyle          = 1
    h_rate.markerColor        = ROOT.kBlue
    h_rate.minimum            = 1e-6
    h_rate.maximum            = 1

    h_rate.yaxis.title        = 'Misidentification rate'
    h_rate.yaxis.labelColor   = ROOT.kBlue
    h_rate.yaxis.titleColor   = ROOT.kBlue
    h_rate.yaxis.titleSize    = 30
    h_rate.yaxis.titleFont    = 133
    h_rate.yaxis.labelSize    = 30
    h_rate.yaxis.labelFont    = 133

    h_rate.xaxis.title        = 'p_{T} [GeV]'
    h_rate.xaxis.titleSize    = 30
    h_rate.xaxis.titleFont    = 133
    h_rate.xaxis.titleOffset  = 1.0
    h_rate.xaxis.labelSize    = 20
    h_rate.xaxis.labelFont    = 133

    ## now the second histo
    c.Update() # this will update user coord
    p = ROOT.gPad.func()

    rightmax = 2 * h_pt.maximum
    scale    = p.uymax / rightmax
    h_pt.Scale(scale)
    h_pt.lineColor  = ROOT.kBlack
    h_pt.lineStyle  = 1
    h_pt.Draw('hist0 same')

    axis = ROOT.TGaxis(p.uxmax, p.uymin, p.uxmax, p.uymax, 1, rightmax, 510, '+LG')
    axis.title        = 'Candidates'
    axis.titleOffset  = 0.85
    axis.titleSize    = 28
    axis.titleFont    = 133
    axis.labelSize    = 24
    axis.labelFont    = 133
    axis.Draw()

    p.logx  = True 
    p.logy  = True
    p.gridx = True
    p.gridy = True
    p.topMargin     = 0.02
    p.bottomMargin  = 0.15
    p.leftMargin    = 0.14
    p.rightMargin   = 0.11

    ## finally
    ROOT.gPad.Update()
    ROOT.gPad.SaveAs('lepton_misid/'+fnameout+'.pdf')

#-------------------------------------------------------------------------------

def draw_mass_reweighted():
  """
  Draw the mass reweight
  The tree is XOR-inflated with weight attached

  Choose the src=MCP here.
  """

  queues = [
    ( 'muon_as_hadron', 100 ),
    ( 'muon_as_elec'  , 100 ),
    ( 'elec_as_hadron', 50  ),
    ( 'elec_as_muon'  , 50  ),
  ]

  fname = 'ditau_background/ditau_background.root'
  for basename, xbin in queues:
    tree = import_tree(basename+'_OS', fname)
    tss  = import_tree(basename+'_SS', fname)

    nos = tree.entries
    nss = tss.entries

    exp = [
      ( tree, 'M/1e3', '' ),
      ( tree, 'M/1e3', 'mcp_detailed_misid_n'), 
      ( tss , 'M/1e3', 'mcp_detailed_misid_n'),
    ]

    h = QHist()
    h.name      = 'mass'
    h.suffix    = basename
    h.expansion = exp
    h.legends   = 'Before reweight', 'After reweight', 'Same-sign'
    h.xbin      = xbin
    h.xmin      = 20
    h.xmax      = 120
    h.xlabel    = 'Mass [GeV]'
    h.normalize = nos, nos, nss
    h.draw()


#-------------------------------------------------------------------------------

def draw_mass_compare():
  """
  Tag & probe mass peak.
  For Wouter's question 161028
  """

  h = QHist()
  h.name    = 'mass_compare'
  h.filters = 'PRESEL_MUON_AS_HADRON'
  h.trees   = t_muon_data
  h.params  = 'Z0_M/1e3'
  h.cuts    = 'probe_ISMUON', 'probe_ISHADRON'
  h.xbin    = 50
  h.xmin    = 80
  h.xmax    = 100
  h.draw()

  exit()

#===============================================================================
# FOR PUBLIC
#===============================================================================

@memorized
def rates():
  """
  Return the final specification of the misid rate.
  Use the mcp detailed version. It has data-MC agreement, and no null bin.
  """
  names = {
    'muon_as_hadron': 'misid_mc_hc_00',
    'elec_as_hadron': 'misid_mc_hc_01',
    'muon_as_elec'  : 'misid_mc_hc_02',
    'elec_as_muon'  : 'misid_mc_hc_03',
  }

  fpath = '$DIR13/identification/plot/lepton_misid/lepton_misid.root'
  fin   = ROOT.TFile(os.path.expandvars(fpath))
  c     = fin.Get('misid_mc')

  ## Correct zero-content bin: take previous value
  acc = {}
  for name, hname in names.iteritems():
    h = c.FindObject(hname)
    se = h.series()
    val0 = None
    for i,val in enumerate(se):
      if val==0:
        h.SetBinEntries(i+1, 1.)
        h.SetBinContent(i+1, val0.n)
      else:
        val0 = val # store previously good value
    acc[name] = h
  return acc


#===============================================================================

if __name__ == '__main__':
  pass
  # main_draw_misid_raw()
  # main_draw_misid_mc()

  # draw_misid_MH_compare()
  # draw_misid_with_pt()
  # draw_mass_reweighted()
  # draw_mass_compare()

  # print rates()

  for key, prof in rates().iteritems():
    print key
    print prof.series().fmt4p
