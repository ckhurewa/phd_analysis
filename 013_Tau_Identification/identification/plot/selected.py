#!/usr/bin/env python

"""

Handle the pick & persist of selected tree

"""

import os
import ROOT
from collections import OrderedDict
from PyrootCK import *
from id_utils import (
  pd, deco, PREFIXES, PREFIXES_ALL, PROCESS_ROOTLABELS, CHANNELS,
  get_current_dtype_dt, memorized, EffU, apply_alias, apply_alias_simple,
  pickle_dataframe_nondynamic,
)

## Default destination & source
_selected_default = '$DIR13/identification/plot/selected/selected.root'

#===============================================================================
# EXPORT
#===============================================================================

def export_selected_trees(dt, trees, target, dname=None):
  """
  Handle the preparation of trees to be persisted into root file.

  Note: Shared to both Ztautau & HMT analyses, handle with care.
  - There's a dependency on PREFIXES, PREFIXES_ALL, they normally a map by 'dt'.
    This is possible to override and make it a map by 'tt' instead.

  Requirement:
  - trees: Dict of (tname, tree)
  """

  def preprocess(tname, t):
    """
    More patching for new var & ignore missing vars
    """
    ##
    logger.info('Preprocessing: %s (rawsize=%i)'%(tname, t.entries))
    t.SetAlias('mass', 'M/1e3')
    brnames = [br.name for br in t.branches] 

    ## alias weight if not existed
    if 'weight' not in brnames:
      t.SetAlias('weight', 'Selection')
    ## alias for prong's momentum if missing. Needed for etrack correction.
    for pf in ('pr1', 'pr2', 'pr3'):
      if (pf+'_ETA' in brnames) and (pf+'_PT' in brnames) and (pf+'_P' not in brnames):
        t.SetAlias(pf+'_THETA', '2*TMath::ATan(TMath::Exp(-%s_ETA))'%pf)
        t.SetAlias(pf+'_P', '{0}_PT/TMath::Sin({0}_THETA)'.format(pf))

    ## patching, for missing vars
    semi_ignore1 = ['weight_err']
    semi_ignore2 = [s+'_TRPCHI2' for s in PREFIXES_ALL[dt]]
    for bname in semi_ignore1+semi_ignore2:
      if not any(br.name == bname for arr in [t.branches, t.aliases] for br in arr):
        logger.info('Ignoring: %s in %s'%(bname, tname))
        t.SetAlias(bname, '1==1.') # use 1 for TRPCHI2 > 0.01 to always be true
    return t

  def postprocess(tname, t):
    """
    Do the drop of duplicated candidate in an event.
    """
    ## Prepare the drop params
    drop_by = [ pf+'_PT' for pf in PREFIXES[dt] ]
    t2 = t.drop('weight', by=drop_by, ascending=[False, False])
    t2.ownership = False
    t2.name   = tname
    # t2.title  = PROCESS_ROOTLABELS.get(tname, tname) # do later
    return t2

  ## Prepare list of branches
  params1 = [
    'mass',
    'weight',
    'weight_err',
    'runNumber',
    'eventNumber',
    'nTracks',
    'P', # for track rec correction
    'PT',
    'ETA',
    'Y',
    'DPT', # for break-samesign
    'IP1',
    'IP2',
    'ISO1',
    'ISO2',
    'cc_vPT1',
    'cc_vPT2',
    'nc_vPT1',
    'nc_vPT2',
  ]
  params2 = [s1+s2 for s1 in PREFIXES_ALL[dt] for s2 in ('_P', '_PT', '_ETA')]
  #
  params = params1 + params2
  if 'h3' in dt:
    params += ['tau_VCHI2PDOF', 'tau_DRTRIOMAX', 'tau_BPVCORRM', 'tau_BPVLTIME', 'tau_M']

  ## Start actual export
  ROOT.TFile.export_trees(
    trees  = trees,
    params = params,
    filt   = 'weight * Selection', # In HMT, both weight & Selection exists.
    target = target,
    dname  = dname or dt,
    prepc  = preprocess,
    postpc = postprocess,
  )

#-------------------------------------------------------------------------------

def _export_queues(dt):
  """
  Return dict of (tname, tree) to be exported.

  Can be reused by HMT
  """
  from ditau_prep import ALLTREES

  ## prep queues, by channel
  queues = [
    't_real',
    't_ztautau',
    't_ztautau0',
    't_ztaujet',
    't_ztautau0_nofidacc', # for cross-feed
    'tdd_qcd',
    #
    't_WW_ll',
    't_WW_lx',
    't_WZ_lx',
    't_ttbar_41900006',
    't_ttbar_41900007',
    't_ttbar_41900010',
    't_zbb',
    't_wtaujet',
    't_hardqcd',
    #
    'tss_real',
    'tss_ztautau',
    'tss_ztautau0',
    'tss_ztaujet',
    'tssdd_qcd',
    'tss_zbb',
    'tss_wtaujet',
    'tss_hardqcd',
    #
    't_ztautau0_noiso',
    't_ztaujet_noiso',
    't_wtaujet_noiso',
    #
    'tss_ztautau0_noiso',
    'tss_ztaujet_noiso',
    'tss_wtaujet_noiso',
  ]
  if 'mu' in dt:
    queues += [
      't_dymu',
      't_wmu17jet',
      't_zmu17jet',
      't_dimuon_misid',
      't_bb_hardmu',
      't_cc_hardmu',
      #
      'tss_dymu',
      'tss_wmu17jet',
      'tss_zmu17jet',
      'tss_dimuon_misid',
      'tss_bb_hardmu',
      'tss_cc_hardmu',
      #
      't_dymu_noiso',
      't_wmu17jet_noiso',
      't_zmu17jet_noiso',
      #
      'tss_dymu_noiso',
      'tss_wmu17jet_noiso',
      'tss_zmu17jet_noiso',
    ]
    if dt == 'mumu':
      queues += [
        't_wmumujet',
        'tss_wmumujet',
        't_wmumujet_noiso',
        'tss_wmumujet_noiso',
      ]
  if 'e' in dt:
    queues += [
      't_dye10',
      't_dye40',
      't_we',
      't_wejet',
      't_dielectron_misid',
      't_bb_harde',
      't_cc_harde',
      #
      'tss_dye10',
      'tss_dye40',
      'tss_we',
      'tss_wejet',
      'tss_dielectron_misid',
      'tss_bb_harde',
      'tss_cc_harde',
      #
      't_dye10_noiso',
      't_dye40_noiso',
      't_we_noiso',
      't_wejet_noiso',
      #
      'tss_dye10_noiso',
      'tss_dye40_noiso',
      'tss_we_noiso',
      'tss_wejet_noiso',
    ]
  return {k:v for k,v in ALLTREES.iteritems() if k in queues and v.entries>0}

#-------------------------------------------------------------------------------

def main_export(dt):
  ## Call the underlying method.
  trees = _export_queues(dt) # get tree with channel-dependent cuts/alias
  export_selected_trees(dt, trees, 'selected/staging/%s.root'%dt)

#===============================================================================
# LOAD THE EXPORTED TREES
#===============================================================================

@memorized
def deck_selected_trees_all(dt, as_dict=False):
  """
  Return the deck for the combination, i.e., mapping of process nickname & treename
  """
  ## common (channel-independent mapping)
  common = {
    'OS'     : 't_real',
    'Ztau'   : 't_ztautau',
    'QCD'    : 'tdd_qcd',
    'VV'     : 't_WW_lx',
    'ttbar'  : 't_ttbar_41900010',
    'ZXC'    : 't_ztautau0_noiso',
    'OtherMU': 't_ttbar_41900010', # good stats, for combine-plot
    'OtherE' : 't_ztaujet',
  }

  ## Full fledge deck of sorted trees
  queues = {
    'mumu': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zll'  , 't_dymu'            ),
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_wmumujet_noiso'  ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ), # not enough stat for tss_ztautau0_noiso
      ('Other', common['OtherMU']   ),
    ],
    'h1mu': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zll'  , 't_dimuon_misid'    ),
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_wmu17jet'        ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ),
      ('Other', common['OtherMU']   ),
    ],
    'h3mu': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zll'  , 't_dymu_noiso'      ), # doesn't matter, skipped anyway
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_wmu17jet_noiso'  ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ),
      ('Other', common['OtherMU']   ),
    ],
    'ee': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zll'  , 't_dye10'           ), # compat with dymu
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_we_noiso'        ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ),
      ('Other', common['OtherE']    ),
    ],
    'eh1': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zll'  , 't_dielectron_misid'),
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_wejet_noiso'     ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ),
      ('Other', common['OtherE']    ),
    ],
    'eh3': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zll'  , 't_wejet_noiso'     ), # doesn't matter, skipped anyway. Just anything large for asserter
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_we_noiso'        ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ),
      ('Other', common['OtherE']    ),
    ],
    'emu': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      ('Zmu'  , 't_dimuon_misid'    ),
      ('Ze'   , 't_dielectron_misid'),
      ('Zll'  , 't_dielectron_misid'), # single repr
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_wmu17jet_noiso'  ),
      ('EWKmu', 't_wmu17jet'        ),
      ('EWKe' , 't_we'              ),
      ('VV'   , common['VV']        ),
      ('ttbar', common['ttbar']     ),
      ('ZXC'  , common['ZXC']       ),
      ('Other', common['OtherE']    ),
    ],
  }
  return OrderedDict(queues[dt]) if as_dict else queues[dt]


@memorized
def deck_selected_trees_comb(dt):
  """
  Use above method, but collapse some entries together suitable for plotting
  (not for fitting)

  - Other: tt+VV+ZXC together as "other"
  - Zll  : Zmu + Ze (in mue channel)

  In case of similar trees, prefer one with larger stats to have a nice 
  visualization. 
  These trees should not be used directly in the numerical computation.
  """
  deck = deck_selected_trees_all(dt, as_dict=True)
  ## Other = VV + ttbar + ZXC
  del deck['VV'   ]
  del deck['ttbar']
  del deck['ZXC'  ]
  ## Zll = Zmu + Ze
  for pname in ['Zmu', 'Ze', 'EWKmu', 'EWKe']:
    if pname in deck:
      del deck[pname]

  ## fine-tune for plotting
  # if dt == 'h3mu':
  #   deck['Other'] = 't_ttbar_41900010'
  # if dt == 'ee':
  #   deck['Other'] = 't_ttbar_41900010'

  ## Finally
  return deck.items()


@memorized
def load_selected_trees_dict(dt, src=_selected_default):
  """
  Note: This is quite generic, just loading trees from single file.
  I may generalize it to PyrootCK in the future.
  """
  ## Get the TFile
  logger.info('Loading selected trees from directory: [%s] %s'%(dt,src))
  fpath = os.path.expandvars(src)
  fin   = ROOT.TFile(fpath)
  fin.ownership = False

  ## Pick up all trees
  acc = {}
  for key in fin.Get(dt).keys:
    obj = fin.Get(dt+'/'+key.name)
    if isinstance(obj, ROOT.TTree):
      # ## load the tree
      # obj.ownership = False
      # acc[obj.name] = obj
      ## load tree as chain, more functionality
      tree = ROOT.TChain(obj.name, obj.title)
      tree.Add(fpath+'/'+dt+'/'+key.name)
      tree.ownership = False
      ## aliasing
      apply_alias_simple(dt, tree) # useful enough in simple cases
      tree.SetAlias('APT', 'TMath::Abs(DPT)/(PT1+PT2)') # more alias
      ## finally
      acc[obj.name] = tree
  return acc


def load_selected_trees_comb(dt):
  """
  Trees suitable for the plotting.
  In case of similar trees, prefer one with larger stats to have a nice 
  visualization. 
  These trees should not be used directly in the numerical computation
  """
  trees  = load_selected_trees_dict(dt)
  queues = deck_selected_trees_comb(dt)
  acc    = []
  for processname, tname in queues:
    tree = trees[tname].Clone(processname)
    tree.title = PROCESS_ROOTLABELS[processname]
    acc.append(tree)
  return acc


@memorized
def load_selected_trees_comb_ss(dt):
  trees  = load_selected_trees_dict(dt)
  queues = {
    'h1mu': [
      ('Data', 'tss_real'    ),
      ('QCD' , 'tssdd_qcd'   ),
      ('EWK' , 'tss_wmu17jet'),
    ],
  }[dt]
  acc = []
  for processname, tname in queues:
    t = trees[tname].Clone(processname)
    t.title = PROCESS_ROOTLABELS[processname]
    acc.append(t)
  return acc


@memorized
def load_selected_tree_erec(dt):
  """
  Suitable for erec calculation. Already has mass window applied.
  """
  ## Needed for mass cut
  import ditau_cuts

  ## Prep trees
  trees  = load_selected_trees_dict(dt)
  queues = deck_selected_trees_all(dt, as_dict=True)
  queues.pop('VV'   , None)
  queues.pop('ttbar', None)
  queues.pop('ZXC'  , None)
  if dt == 'emu':
    del queues['Zll']
    del queues['EWK']
  if 'h3' in dt: # remove Zll from calculation
    del queues['Zll']

  ## Collect, prep cache TFile
  ROOT.gROOT.cd()
  filt = 'mass < %f'%ditau_cuts.UPPER_MASS[dt]
  for processname, tname in queues.iteritems():
    tree = trees[tname].CopyTree(filt)
    tree.name = processname
    tree.title = PROCESS_ROOTLABELS[processname]
    apply_alias_simple(dt, tree) # useful enough in simple cases
    yield tree


# @memorized
# def load_selected_tree_wmw(dt, tname):
#   """
#   Suitable for detailed Zll table
#   """
#   ## Needed for mass cut
#   import ditau_cuts
#   trees  = load_selected_trees_dict(dt)
#   ROOT.gROOT.cd()
#   filt = 'mass < %f'%ditau_cuts.UPPER_MASS[dt]
#   tree = trees[tname].CopyTree(filt)
#   # tree.name = processname
#   # tree.title = PROCESS_ROOTLABELS[processname]
#   apply_alias_simple(dt, tree) # useful enough in simple cases
#   return tree



#===============================================================================

MASSWINS = [50, 55, 60, 65, 70, 80, 90, 100, 120, 9999] # 9999 as infty

def _fraction(tree):
  acc = pd.Series()
  tree.Draw('mass: weight', '', 'para goff')
  df = tree.sliced_dataframe()
  deno = df.weight.sum()
  ## Loop over different upper mass
  for mass in MASSWINS:
    nume = df[df.mass <= mass].weight.sum()
    acc[str(int(mass))] = nume / deno
  ## add also misc
  acc['size']    = len(df.index)
  acc['maxmass'] = max(df.mass)
  return acc


@deco.concurrent
def fraction_mass_window_single(dt):
  ## Loop over all selected trees
  trees = load_selected_trees_dict(dt)
  return pd.concat({tname:_fraction(tree) for tname,tree in trees.iteritems()})


@pickle_dataframe_nondynamic
def fraction_mass_window_all():
  """
  - Make sure that the fraction is computed taking weight into account.
  - Check at diffrent upper mass point (for Zee possiblity).
  """
  @deco.synchronized
  def wrap():
    ## Loop over 7 channels
    acc = {}
    for dt in CHANNELS:
      acc[dt] = fraction_mass_window_single(dt)
    return acc
  ## finally, package, order string-numerically
  cols = ['size', 'maxmass']+[str(int(mass)) for mass in MASSWINS]
  return pd.concat(wrap()).unstack()[cols]


@pickle_dataframe_nondynamic
def fraction_mass_window():
  """
  With choice of mass window applied
  """
  from ditau_cuts import UPPER_MASS
  df  = fraction_mass_window_all()
  acc = pd.concat({dt: df.loc[dt, str(UPPER_MASS[dt])] for dt in CHANNELS}, axis=1)
  # also fill NA with zero. These are mostly eliminated process
  acc = acc.fillna(1.)
  return acc[list(CHANNELS)]


_WMW_COLS = ['Zll', 'QCD', 'EWK', 'VV', 'ttbar', 'Zbb', 'ZXC']
_FRAC_WMW_MAP = {
  'mumu': ['t_dymu' , 'tdd_qcd', 't_wmumujet', 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
  'h1mu': ['t_dymu' , 'tdd_qcd', 't_wmu17jet', 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
  'h3mu': ['t_dymu' , 'tdd_qcd', 't_wmu17jet', 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
  'ee'  : ['t_dye10', 'tdd_qcd', 't_we'      , 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
  'eh1' : ['t_dye10', 'tdd_qcd', 't_we'      , 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
  'eh3' : ['t_dye10', 'tdd_qcd', 't_we'      , 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
  'emu' : ['t_dye10', 'tdd_qcd', 't_wmu17jet', 't_WW_lx', 't_ttbar_49100010', 't_zbb', 't_ztautau'],
}

@memorized
def fracs_wmw_mapped():
  """
  Simplified version where the process names are already mapped to final ones
  found in candidates.py, suitable to show mass window optimization.
  """
  acc   = {}
  fracs = fraction_mass_window_all()
  for mass in MASSWINS:
    df = fracs[str(mass)].unstack()
    for dt in CHANNELS:
      eff = df.loc[dt, _FRAC_WMW_MAP[dt]]
      eff.index = _WMW_COLS
      eff['OS'] = df.loc[dt, 't_real']
      acc[(mass, dt)] = eff.fillna(1.)
  return pd.concat(acc).unstack()


#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    ## Cache the tree, once the selection cut is stable
    ## mainly for draw_combined, computing reconstruction eff.
    dt = get_current_dtype_dt()[1]
    main_export(dt)
    sys.exit()    

  ## load
  # print deck_selected_trees_all(dt)
  # print deck_selected_trees_comb(dt)
  # print load_selected_trees_dict(dt)
  # print load_selected_trees_comb('emu')
  # print load_selected_trees_comb_ss(dt)
  # for x in load_selected_tree_erec('ee'):
  #   print x, x.entries

  ## mass window
  # print fraction_mass_window_all().to_string()
  # print fraction_mass_window()
  # print fracs_wmw_mapped()
