#!/usr/bin/env python

from PyrootCK import *
from PyrootCK.QHist.CoreV3 import QHist as QHistV3

## Trees & tools
import selected
from id_utils import *
from ditau_utils import ufloat_to_latex1

#-------------------------------------------------------------------------------

def postproc_legend(h):
  h.anchors.legend.textSize = 0.10
  h.anchors.legend.header = 'LHCb 8TeV'
  h.anchors.legend.x1NDC = 0.1
  h.anchors.legend.x2NDC = 0.3
  h.anchors.legend.y1NDC = 0.50
  h.anchors.legend.y2NDC = 0.95

#===============================================================================
# MU_MU: Z -> mumu Background
#===============================================================================

@pickle_dataframe_nondynamic
def calc_zmumu_mumu():
  ## Load the selected tree
  import selected
  trees = selected.load_selected_trees_dict('mumu')
  t_dymu    = trees['t_dymu']
  t_ztautau = trees['t_ztautau']
  t_real    = trees['t_real']

  ## 1. Get num of cand in signal[20-80] region
  # cut = utils.join('Selection', 'M>20e3', 'M<80e3')
  # num_mc_signal = t_dymu.count_ent_evt(cut)[1]
  # num_template_dd_signal = tdd_dymu.count_ent_evt(cut)[1]

  ## 2. Scale such that the area under Z-peak is equal to data's
  cut_masswin    = utils.join('mass > 80', 'mass < 100')
  num_real_peak0 = t_real.count_ent_evt(cut_masswin)[1]
  num_mc_peak    = t_dymu.count_ent_evt(cut_masswin)[1]

  ## 3. Scale factor due to mass resolution, fast workaround
  cut = utils.join('mass > 60', 'mass < 120')
  s1  = 1.* num_real_peak0 / num_mc_peak
  s2  = 1.* t_real.count_ent_evt(cut)[1] / t_dymu.count_ent_evt(cut)[1]

  ## 4. Full range (no mass window) for reference & cross-check
  num_real_full = t_real.count_ent_evt()[1]
  num_mc_full   = t_dymu.count_ent_evt()[1]

  ## Systematics from both peak stats and template stats
  num_real_peak = int(num_real_peak0 * s2/s1)
  num_exp_full  = var(num_real_peak) * var(num_mc_full) / ufloat(num_mc_peak, 0)

  ## Visualization on [20,100]
  h = QHistV3()
  h.name = 'mumu_dymu_shape_M'
  h.tpc  = [
    ( t_real   , 'mass', cut_masswin),  # For data, show overlay only at peak
    ( t_dymu   , 'mass', '' ), # 
    ( t_ztautau, 'mass', '' ), # for reference
  ]
  h.normalize = [
    num_real_peak,
    t_dymu.entries * num_real_peak/num_mc_peak,
    num_real_peak, # arbitary choice for Ztau
  ]
  h.legends = 'Data', 'Z #rightarrow #mu#mu', 'Z #rightarrow #tau_{#mu}#tau_{#mu}'
  h.xmin    = 0
  h.xmax    = 120
  h.xbin    = 60
  h.st_code = 1
  h.xlabel  = 'mass(%s) [GeV/c^{2}]'%LABELS_DITAU['mumu']
  h.ylabel  = 'Candidates / 1 GeV/c^{2}'
  h.postproc= postproc_legend
  h.draw()

  ## Provide more metadata for latex printout.
  return pd.Series({
    'peak'      : '[80, 100]',
    'data_peak' : num_real_peak,
    'data_full' : num_real_full,
    'mc_peak'   : num_mc_peak,
    'mc_full'   : num_mc_full,
    'exp_full'  : num_exp_full,
  })

#===============================================================================
# E_E: Z -> ee  Background
#===============================================================================

@pickle_dataframe_nondynamic
def calc_zee_ee():
  ## Load the selected tree
  import selected
  trees = selected.load_selected_trees_dict('ee')
  t_dye10   = trees['t_dye10']
  t_ztautau = trees['t_ztautau']
  t_real    = trees['t_real']
  tss_real  = trees['tss_real']

  # ## 1. Get num of cand in signal[20-80] region from MC
  # cut = utils.join( 'Selection', 'M>20e3', 'M<80e3' )
  # num_mc_signal = t_dye10.count_ent_evt(cut)[1]

  ## 2. Get num of cand under peak[70,100] from data/MC
  cut_masswin    = utils.join('mass > 70', 'mass < 100')
  num_mc_peak    = t_dye10.count_ent_evt (cut_masswin)[1]
  area_peak_real = t_real.count_ent_evt  (cut_masswin)[1]
  area_peak_ress = tss_real.count_ent_evt(cut_masswin)[1]

  ## 3. Full range (no mass window) for reference & cross-check
  num_real_full = t_real.count_ent_evt()[1]
  num_ress_full = tss_real.count_ent_evt()[1]
  num_mc_full   = t_dye10.count_ent_evt()[1]

  ## Draw: Full [20,100] area, for visual
  h = QHistV3()
  h.name = 'ee_dye_shape_M'
  h.tpc  = [
    ( t_real   , 'mass', cut_masswin ),  # For data, show overlay only at peak
    ( t_dye10  , 'mass', '' ),
    ( t_ztautau, 'mass', '' ), # for reference
    ( tss_real , 'mass', '' ),
  ]
  h.normalize = [
    area_peak_real,
    t_dye10.entries *area_peak_real/num_mc_peak,
    area_peak_real, # arbitary choice
    False,
  ]
  h.legends = 'Data', 'Z #rightarrow ee', 'Z #rightarrow #tau_{e}#tau_{e}', 'e^{#pm}e^{#pm}'
  h.xmin    = 0
  h.xmax    = 120
  h.xbin    = 24
  h.st_code = 1
  h.xlabel  = 'mass(#tau_{e}#tau_{e}) [GeV/c^{2}]'
  h.ylabel  = 'Candidates / 5 GeV/c^{2}'
  h.postproc= postproc_legend
  h.draw()

  ## Calculation
  # print 'num_mc_signal :', num_mc_signal
  print 'num_mc_peak   :', num_mc_peak
  print 'area_peak_real:', var(area_peak_real)
  print 'area_peak_ress:', var(area_peak_ress)

  # ## Print out python string for next stage.
  exp_full = (var(area_peak_real)-var(area_peak_ress)) * var(num_mc_full) / num_mc_peak

  ## Provide more metadata for latex printout.
  return pd.Series({
    'peak'        : '[70, 100]',
    'data_peak'   : area_peak_real,
    'datass_peak' : area_peak_ress,
    'data_full'   : num_real_full,
    'datass_full' : num_ress_full,
    'mc_peak'     : num_mc_peak,
    'mc_full'     : num_mc_full,
    'exp_full'    : exp_full,
  })

#===============================================================================
# PACKAGE THE RESULTS
#===============================================================================

## Prefer this specific order
## With homogenized tree regime
SPEC_ZLL = [
  ( 'h1mu', 'h1mu', 't_dimuon_misid'    , '\\zmumu'),
  ( 'mue' , 'emu' , 't_dimuon_misid'    , '\\zmumu'),
  ( 'eh1' , 'eh1' , 't_dielectron_misid', '\\zee'  ),
  ( 'emu' , 'emu' , 't_dielectron_misid', '\\zee'  ),
]


@memorized
def results_misid_nomasswin():
  """
  Collect only the misid Zll in 3 channels.
  """
  df = pd.DataFrame()
  for key, dt, tname_os, bkg in SPEC_ZLL:
    tname_ss = tname_os.replace('t_', 'tss_')
    trees    = selected.load_selected_trees_dict(dt)
    df_OS    = trees[tname_os].dataframe()
    df_SS    = trees[tname_ss].dataframe()
    misid_OS = ufloat(df_OS['weight'].sum(), df_OS['weight_err'].sum())
    misid_SS = ufloat(df_SS['weight'].sum(), df_SS['weight_err'].sum())
    size     = df_OS.shape[0]
    rate     = misid_OS / size  # has only misid error, no stat error
    exp_OS   = misid_OS * ufloat(1, var(df_OS.shape[0]).rerr)
    exp_SS   = misid_SS * ufloat(1, var(df_SS.shape[0]).rerr)
    exp      = exp_OS - exp_SS
    df[key]  = [dt, bkg, rate, size, exp, exp_OS, exp_SS]
  ## Package it
  df.index = ['dt', 'bkg', 'rate', 'size', 'expected', 'nos', 'nss']
  df.columns.name = 'channel'
  return df.T


@memorized
def results_raw():
  """
  Before combining emu+mue. For mass window calc.
  """ 
  res_misid = results_misid_nomasswin()['expected']
  return pd.Series({
    'mumu': df_from_path('calc_zmumu', __file__)['mumu']['exp_full'],
    'ee'  : df_from_path('calc_zee'  , __file__)['ee']['exp_full'],
    'h1mu': res_misid['h1mu'],
    'eh1' : res_misid['eh1'],
    'emu' : res_misid['emu'],
    'mue' : res_misid['mue'],
    'h3mu': ufloat(0., 0.),
    'eh3' : ufloat(0., 0.),
  })


@memorized
def results_nomasswin():
  df = results_raw().copy()
  df['emu'] += df['mue']
  del df['mue']
  return df[list(CHANNELS)]


@memorized
def frac_masswin():
  """
  Return the fraction of Zll inside mass window from total.
  """
  df = selected.fraction_mass_window()
  return pd.Series({
    'mumu': df.loc['t_dymu'            , 'mumu'],
    'h1mu': df.loc['t_dimuon_misid'    , 'h1mu'],
    'mue' : df.loc['t_dimuon_misid'    , 'emu' ], 
    'ee'  : df.loc['t_dye10'           , 'ee'  ], # Use dye10, same as Zee fit above
    'eh1' : df.loc['t_dielectron_misid', 'eh1' ],
    'emu' : df.loc['t_dielectron_misid', 'emu' ],
    'h3mu': 0.,
    'eh3' : 0.,
  })


@memorized
def results_misid_withmasswin():
  """
  For the sake of latex table. Rough approximation.
  """
  df = results_misid_nomasswin().drop('rate', axis=1)
  df['size'] *= frac_masswin()
  df['expected'] *= frac_masswin()
  df['nos'] *= frac_masswin()
  df['nss'] *= frac_masswin()
  df['size'] = df['size'].apply(int)
  df['rate'] = df['nos'] / df['size']
  return df

@memorized
def results_withmasswin(collapse_emu=True):
  df = results_raw() * frac_masswin()
  if collapse_emu:
    df['emu'] += df['mue']
    del df['mue']
    return df[list(CHANNELS)]
  else:
    return df


def latex_genuine():
  """
  For the table in \dmumu, \dee section.
  """
  reswin = results_withmasswin()
  zmu = df_from_path('calc_zmumu', __file__)['mumu']
  ze  = df_from_path('calc_zee'  , __file__)['ee']
  msg = """
Background                                         \zmumu          \zee
-------------------------------------------------- --------------- -----------------
\Z-peak range for normalization [GeV]              [80,100]        [70,100]
Number of candidates in data inside \Z-peak        {0:15} {1:15}
Estimation of the \zll background in signal region {2:15} {3:15}
--------------------------------------------------------------------------------
"""
  return msg.format(
    ufloat_to_latex1(var(zmu.data_peak)),
    ufloat_to_latex1(var(ze.data_peak)),
    ufloat_to_latex1(reswin.mumu),
    ufloat_to_latex1(reswin.ee),
  )


def latex_misid():
  """
  Stats of misid fake Zll background
  """
  df0 = results_misid_withmasswin().rename(DT_TO_LATEX)
  df  = df0[['bkg', 'size', 'expected', 'rate']].copy()
  df.insert(2, 'impure', df0['nss']/df0['nos'])
  df.columns    = ['Background', 'Selected fake', 'SS/OS [%]', 'Expected \zll', 'Mean mis-id [%]']
  df.index.name = 'Channel'
  df.iloc[:,2]  = df.iloc[:,2].fmt2lp
  df.iloc[:,3]  = df.iloc[:,3].fmt2l
  df.iloc[:,4]  = df.iloc[:,4].fmt3lp
  msg = df.to_markdown(stralign='left')
  return msg

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  if '--recalc' in sys.argv:
    ## Drell-Yan backgrond
    print calc_zmumu_mumu()
    print calc_zee_ee()
    sys.exit()

  ## SETUP for drawing
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.CanvasDefH = 180
  ROOT.gStyle.CanvasDefW = 720
  ROOT.gStyle.padRightMargin  = 0.015
  ROOT.gStyle.padTopMargin    = 0.01
  ROOT.gStyle.padBottomMargin = 0.25
  ROOT.gStyle.padLeftMargin   = 0.065
  ROOT.gStyle.titleOffset     = 0.32, 'Y'
  ROOT.gStyle.ndivisions      = 510, 'x'
  drawers.set_global_font_size(0.09)
  drawers.set_global_line_width(1)
  ROOT.gStyle.histLineWidth = 2

  ## DEV
  # print df_from_path('calc_zmumu', __file__)['mumu']

  ## Main
  print calc_zee_ee()

  # ## Expose results
  # print results_misid_nomasswin()
  # print results_misid_withmasswin()
  # print results_raw().fmt2f
  # print frac_masswin()
  # print results_nomasswin().fmt2f
  # print results_withmasswin().fmt2f

  ## Latex
  # print latex_genuine()
  # print latex_misid()
