#!/usr/bin/env python


CHILDTYPE_LISTTES = {
  'e_e':(
    'PFParticles',
    'StdAllNoPIDsElectrons',
    'StdAllLooseElectrons',
    # 'StdLooseANNElectrons',
    'StdTightANNElectrons',
  ),
  'h1_pi':(
    'PFParticles',
    'StdAllLoosePions',
    'StdAllLooseANNPions',
    'StdAllNoPIDsPions',
    'StdTightPions',
    'StdTightANNPions',
  ),
  'h3_pi':(
    'PFParticles',
    'StdAllNoPIDsPions',
    'StdAllLoosePions',
    'StdAllLooseANNPions',
    'StdTightPions',
    'StdTightANNPions',
  ),
  'mu_mu':(
    'PFParticles',
    'StdAllLooseMuons',
    'StdAllVeryLooseMuons',
    'StdTightMuons',
    'StdTightANNMuons',
  )
}