#!/usr/bin/env python

from PyrootCK import import_tree, QHist, ROOT

ROOT.gROOT.SetBatch(True)

#-----------#
# RECOSTATS #
#-----------#

import config

for jid,title in [(2591, 'H2AA'), (2592, 'Z0')]:
  for nickname,list_tes in config.CHILDTYPE_LISTTES.iteritems():
    ttype = nickname.split('_')[0]

    t = import_tree(jid, 'TauRecoStats/TauReco_%s'%ttype, title=title)

    ## Reco-efficiency as a func of MCPT (charged track only)
    h = QHist()
    h.prefix  = title
    h.name    = 'RecoEffMCPT_tau_'+ttype
    h.trees   = t
    h.params  = ['count_child_%s==nChildren_charged:MCPT_charged'%tes for tes in list_tes]
    h.cuts    = 'count_child_RECOA==nChildren_charged'  # i.e., given the particles inAcc & recoa
    h.legends = list_tes
    h.options = 'prof'
    h.xlog    = True
    h.xmin    = 99
    h.xmax    = 1E5
    h.xbin    = 24
    h.xlabel  = 'MCPT charged track(s)'
    h.ylabel  = 'Fraction'
    h.comment = 'InAcc,Triggered, and IMCRecoa >= 2'
    h.hasLineStyle = False
    h.hasMarkerStyle = True
    h.draw()

