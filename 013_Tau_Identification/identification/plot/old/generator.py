#!/usr/bin/env python

"""
Generator-level study
"""

import sys
from PyrootCK import *

## Ditau generator-level
t_ditau   = import_tree( 'DitauDebugger/Ditau', 2938 )

QHist.prefix    = 'genlv'
# QHist.auto_name = True
QHist.trees     = t_ditau

h = QHist()
h.name    = 'ditau_MCPT_MCETA'
h.params  = 'ditau_MCETA: ditau_MCPT/1000'
h.xlog    = True
h.zlog    = True
h.xlabel  = 'PT [GeV]'
h.ylabel  = 'ETA'
h.options = 'colz'
# h.draw()

h = QHist()
h.name    = 'tau_MCETA'
h.params  = 'tau1top_MCETA:tau2top_MCETA'
h.options = 'colz'
h.xlabel  = 'ETA(tau1)'
h.ylabel  = 'ETA(tau2)'
h.zlog    = True
# h.draw()

h = QHist()
h.name    = 'tau_MCPT'
h.params  = 'tau1top_MCPT/1000:tau2top_MCPT/1000'
h.options = 'colz'
h.xlog    = True
h.ylog    = True
h.zlog    = True
h.xlabel  = 'PT(tau1) [GeV]'
h.ylabel  = 'PT(tau2) [GeV]'
# h.draw()

h = QHist()
h.name    = 'tau_MCPT_MCETA'
h.params  = 'tau1top_MCETA: tau1top_MCPT/1000'  # one tau is sufficient
h.xlog    = True
h.zlog    = True
h.xlabel  = 'PT [GeV]'
h.ylabel  = 'ETA'
h.options = 'colz'
# h.draw()

h = QHist()
h.name    = 'DR_tau1_tau2'
h.params  = 'DR'  # DR between two-taus of same ditau
# h.draw()



#==============================================================

## RecoStats
t_e  = import_tree( 'TauRecoStats/TauReco_e' , 2934 )
t_mu = import_tree( 'TauRecoStats/TauReco_mu', 2934 )
t_h1 = import_tree( 'TauRecoStats/TauReco_h1', 2934 )
t_h3 = import_tree( 'TauRecoStats/TauReco_h3', 2934 )

t_e.SetAlias ('INACC', 'MCETA_charged>2.0  && MCETA_charged<4.5 ')
t_mu.SetAlias('INACC', 'MCETA_charged>2.0  && MCETA_charged<4.5 ')
t_h1.SetAlias('INACC', 'MCETA_charged>2.25 && MCETA_charged<3.75')
t_h3.SetAlias('INACC', 'MCETA_charged>2.25 && MCETA_charged<3.75')

QHist.reset()
QHist.trees   = t_e, t_mu, t_h1, t_h3
QHist.options = 'prof'
QHist.xmin    = 1
QHist.xmax    = 1E2
QHist.ymin    = 0.
QHist.xlog    = True
QHist.legends = 'tau_e', 'tau_mu', 'tau_h1', 'tau_h3'
QHist.prefix  = 'genlv'


h = QHist()
h.name    = 'acceptance'
h.params  = 'INACC: MCPT/1000'
h.xlabel  = 'MCPT (tau) [GeV]'
h.ylabel  = 'All charged children in acceptance'
h.xmax    = 2E2
h.ymax    = 1.
# h.draw()

# raw_input(); sys.exit(0)


#-------
QHist.master_cuts = 'INACC'
#-------


#----#
# PT #
#----#

h = QHist()
h.name    = 'PTquality_bymom'
h.params  = 'MCPT_charged/MCPT : MCPT/1000'
h.xlabel  = 'MCPT (tau) [GeV]'
h.ylabel  = 'MCPT(children) / MCPT(tau)'
h.ymax    = 1.
# h.draw()

h = QHist()
h.name    = 'PTquality_bychildren'
h.params  = 'MCPT_charged/MCPT : MCPT_charged/1000'
h.xlabel  = 'MCPT (children) [GeV]'
h.ylabel  = 'MCPT(children) / MCPT(tau)'
h.ymax    = 1.
# h.draw()

#----#
# DR #
#----#

h = QHist()
h.name    = 'DR_bymom'
h.params  = '((TMath::Abs(TMath::Abs(MCPHI-MCPHI_charged)-pi)-pi)**2 + (MCETA-MCETA_charged)**2)**0.5 : MCPT/1000'
h.xlabel  = 'MCPT (tau) [GeV]'
h.ylabel  = 'DR(tau,children)'
# h.draw()

h = QHist()
h.name    = 'DR_bychildren'
h.params  = '((TMath::Abs(TMath::Abs(MCPHI-MCPHI_charged)-pi)-pi)**2 + (MCETA-MCETA_charged)**2)**0.5 : MCPT_charged/1000'
h.xlabel  = 'MCPT (children) [GeV]'
h.ylabel  = 'DR(tau,children)'
# h.draw()

# raw_input(); sys.exit(0)

#-------------#
# Neuloss: MM #
#-------------#

QHist.reset()
QHist.prefix  = 'genlv'

# 1-pronger, 3-prongers
t_ditau.SetAlias('ditau_type_1p1p', 'ditau_itype!=2 & ditau_itype!=12 & ditau_itype!=22 & ditau_itype!=23')
t_ditau.SetAlias('ditau_type_1p3p', 'ditau_itype==2 | ditau_itype==12 | ditau_itype==23') # eh3, h1h3, h3mu
t_ditau.SetAlias('ditau_type_3p3p', 'ditau_itype==22')  # h3h3
# t_ditau.SetAlias('ditau_type_h3X', 'ditau_itype==2 | ditau_itype==12 | ditau_itype==22 | ditau_itype==23')

h = QHist()
h.master_cuts = 'tau1bot_MCETA>2.25 & tau1bot_MCETA<3.75'
h.trees   = t_ditau
h.name    = 'tauh3_PT_MM'
h.params  = 'tau1ch_MM/1E3 : tau1ch_PT/1E3'
h.cuts    = 'tau1_itype==2'
h.options = 'colz'
h.xlog    = True 
h.zlog    = True
h.xlabel  = 'PT (3prongs) [GeV]'
h.ylabel  = 'Invariant mass (3prongs) [GeV]'
# h.draw()

h = QHist()
h.master_cuts = 'tau1bot_MCETA>2.0 & tau1bot_MCETA<4.5'
h.trees   = t_ditau
h.name    = 'ditau_neuloss_MM'
h.params  = 'ditau_ch_MM/1000'
# h.cuts   = [ 'ditau_itype==%i'%i for i in (0,1,2,3,11,12,13,22,23,33) ]  # All
# h.cuts   = [ 'ditau_itype==%i'%i for i in (11,33) ]  # h1h3, h3mu
h.cuts    = 'ditau_type_1p1p', 'ditau_type_1p3p', 'ditau_type_3p3p'
# h.cuts   = 'ditau_itype == 23'
h.xlog    = False
h.xmin    = 0
h.xmax    = 120
h.legends = '(e/mu/h1) + (e/mu/h1)', '(e/mu/h1) + h3', 'h3 + h3'
h.xlabel  = 'Ditau invariant mass [GeV]'
# h.draw()

# raw_input(); sys.exit(0)


#---------#
# H3 Cone #
#---------#

t_h3cone  = import_tree( 'TauH3ConeDebugger/tau_h3', 2938 )

QHist.reset()
QHist.prefix  = 'genlv'
QHist.trees   = t_h3cone
QHist.cuts    = '2.25 < MCETA_charged & MCETA_charged < 3.75'
QHist.legends = 'min(DR)', 'mid(DR)', 'max(DR)'

DRTRIO = 'MCDR_min', 'MCDR_mid', 'MCDR_max'

h = QHist()
h.name    = 'tauh3_DR'
h.params  = DRTRIO
h.xmin    = 0
h.xmax    = 0.4
h.ylog    = True
h.xlabel  = 'DR'
h.draw()

h = QHist()
h.name    = 'tauh3_DR_PT'
h.params  = ['%s:MCPT_charged/1000'%s for s in DRTRIO]
h.options = 'cont3'
h.xmin    = 9
h.xmax    = 1E2
h.ymin    = 0
h.ymax    = 0.2
h.xbin    = 12
h.ybin    = 12
h.zbin    = 6
h.xlog    = True
h.xlabel  = 'PT (3prongs)'
h.ylabel  = 'DR'
# h.draw()


raw_input()