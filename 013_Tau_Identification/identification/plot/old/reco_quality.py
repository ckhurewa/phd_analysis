#!/usr/bin/env python

from PyrootCK import import_tree, QHist, ROOT
from config import CHILDTYPE_LISTTES

ROOT.gROOT.SetBatch(True)

for jid,title in [(2591, 'H2AA'), (2592, 'Z0')]:
  for nickname, list_tes in CHILDTYPE_LISTTES.iteritems():

    ### Reco Resolution & Quality
    t = import_tree(jid, 'TauRecoStats/TauChildReco__%s'%nickname, title=title)

    h = QHist()
    h.master_cuts = 'IMCReconstructible_CODE==2'
    h.expansion   = [(t, '%s_PT/MCPT: MCPT'%tes, '%s_ID!=0'%tes) for tes in list_tes]
    h.prefix    = title
    h.name      = 'RecoQualityPT_tau_'+nickname.split('_')[0]
    h.suffix    = 'LONGonly'
    h.options   = 'prof'
    h.xlog      = True
    h.xmin      = 1E3-1
    h.xmax      = 1E5
    h.xbin      = 30
    h.ymin      = 0
    h.xlabel    = 'MCPT'
    h.ylabel    = 'PT / MCPT'
    h.legends   = list_tes
    h.st_line   = False
    # h.st_marker = True
    h.draw()


    ### As a func of track type
    QHist.reset()
    QHist.prefix  = 'RecoQualityByType_tau_'+title
    QHist.name    = nickname.split('_')[0]
    QHist.trees   = t
    QHist.cuts    = (
      'IMCReconstructible_CODE == 2',
      'IMCReconstructible_CODE == 3',
      'IMCReconstructible_CODE == 4',
      # 'IMCReconstructible_CODE == 5',
      'IMCReconstructible_CODE == 6',
    )
    QHist.legends = (
      'Long',
      'Downstream',
      'Upstream',
      # 'TTrack',
      'VELO',
    )
    QHist.options = 'prof'
    QHist.xlabel  = 'MCPT'
    QHist.ylabel  = 'PT / MCPT'
    QHist.xmin    = 1E3-1
    QHist.xmax    = 1E5
    QHist.xlog    = True
    QHist.xbin    = 30
    QHist.ymin    = 0
    QHist.ymax    = 1.2
    QHist.st_line = False
    
    for tes in list_tes:
      h = QHist()
      h.master_cuts = '%s_ID!=0'%tes
      h.suffix  = tes
      h.params  = '%s_PT/MCPT:MCPT'%tes
      h.draw()
