#!/usr/bin/env python

"""

Identification at tau level, check against MC of other background.

"""

from PyrootCK import *
# from PyrootCK.QHist.CoreV3 import QHist

# ROOT.gROOT.ProcessLine('.L lhcbStyle.C')

ttype = 'mu'

tname = 'TightTauCandTupleWriter/'+ttype

## These are with STIPPING FILTERED + PVR fixed
t_ztautau   = import_tree( tname, 3601 ).SetTitle('Z02TauTau')
t_wtaujet   = import_tree( tname, 3602 ).SetTitle('Wtau+Jet')
t_wmujet    = import_tree( tname, 3603 ).SetTitle('Wmu+Jet')
t_wejet     = import_tree( tname, 3604 ).SetTitle('We+Jet')
t_wmu17jet  = import_tree( tname, 3605 ).SetTitle('Wmu17+Jet')
t_dytau     = import_tree( tname, 3606 ).SetTitle('DYtau')
t_dymu      = import_tree( tname, 3607 ).SetTitle('DYmu')
t_dye       = import_tree( tname, 3608 ).SetTitle('DYe')
t_hardqcd   = import_tree( tname, 3609 ).SetTitle('HardQCD')
t_cc2mux    = import_tree( tname, 3610 ).SetTitle('cc2mux')
t_bb2mux    = import_tree( tname, 3611 ).SetTitle('bb2mux')
t_cc2ex     = import_tree( tname, 3612 ).SetTitle('cc2ex')
t_bb2ex     = import_tree( tname, 3613 ).SetTitle('bb2ex')


t_wjet = ROOT.TChain('Wlep+Jet', 'Wlep+Jet')
t_wjet.Add( t_wtaujet   )
t_wjet.Add( t_wmujet    )
t_wjet.Add( t_wmu17jet  )
t_wjet.Add( t_wejet     )

t_bbbar = ROOT.TChain('bbbar', 'bbbar')
t_bbbar.Add( t_bb2mux )
t_bbbar.Add( t_bb2ex  )

t_ccbar = ROOT.TChain('ccbar', 'ccbar')
t_ccbar.Add( t_cc2mux )
t_ccbar.Add( t_cc2ex  )

QHist.filters = {
  'e' : [
    'StrippingZ02TauTau_EXLineDecision',
    'PT     > 5000',
    'BPVIP  > 0.01',
    'BPVIP  < 0.2',
  ],
  'mu': [ 
    'StrippingZ02TauTau_MuXLineDecision',
    'PT     > 5000',
    # 'BPVIP  > 0.01',
    # 'BPVIP  < 0.4',
  ],
  'h1': [ 
    'StrippingZ02TauTau_MuXLineDecision | StrippingZ02TauTau_EXLineDecision',
    'PT     > 5000',
    'BPVIP  > 0.01',
    'BPVIP  < 0.2',
  ],
  'h3': [ 
    'StrippingZ02TauTau_MuXLineDecision | StrippingZ02TauTau_EXLineDecision',
    'PT > 10000',
    # 'DRTRIOMIN < 0.10',
    # 'DRTRIOMID < 0.15',
    # 'DRTRIOMAX < 0.20',
    # 'PTTRIOMIN > 1000',
    # 'PTTRIOMID > 4000',
    # 'PTTRIOMAX > 8000',
    # 'M  > 900',
    # 'M  < 1400',
    # 'VCHI2PDOF < 10',
    # #
    # 'BPVVD    > 4',
    # 'BPVVD    < 100',
    # 'BPVCORRM > 1000',
    # 'BPVCORRM < 2400',
  ],
}[ttype]
 # + [ 'PTFrac05C > 0.9','PTCone05C<2000' ]  # Remove me in Isolation plot

QHist.prefix      = ttype
# QHist.auto_name   = True
# QHist.st_line     = False
# QHist.st_fill     = False
# QHist.st_marker   = True
QHist.trees       = {
  'e' : [ t_ztautau, t_wejet   , t_dye     , t_bb2ex  , t_cc2ex  ], # t_wtaujet
  # 'mu': [ t_ztautau, t_dymu    , t_bb2mux , t_cc2mux ], # t_wtaujet
  'mu': [ t_ztautau, t_wmu17jet, t_dymu    , t_bb2mux , t_cc2mux ], # t_wtaujet
  # 'mu': t_ztautau,
  'h1': [ t_ztautau, t_wjet    , t_bbbar   , t_ccbar  ], # t_hardQCD
  'h3': [ t_ztautau, t_wjet    , t_bbbar   , t_ccbar  ],
}[ttype] 

#------------

## Interesting, but got masked a lot by generator level cuts

h = QHist()
h.params = 'PT/1000'
h.xmin   = 1
h.xmax   = 50
# h.xbin   = 40
# h.xlog   = True
h.xlabel = '\pt [\gevc]'
# h.xlabel = "#font[12]{p}_{T} [GeV/#font[12]{c}^{2}]"
# h.draw()

# QHistUtils.pad_divider( h )

# raw_input(); sys.exit()

#---------#
# H3 only #
#---------#

h = QHist()
h.params = 'DRTRIOMIN'
h.xmin   = 0
h.xmax   = 0.2
# h.draw()
h = QHist()
h.params = 'DRTRIOMID'
h.xmin   = 0
h.xmax   = 0.2
# h.draw()
h = QHist()
h.params = 'DRTRIOMAX'
h.xmin   = 0
h.xmax   = 0.2
# h.draw()

h = QHist()
h.params = 'PTTRIOMIN/1000'
h.xlog   = True
h.xmin   = 1
h.xmax   = 20
h.xlabel = 'min(p_{T}) [MeV/c]'
# h.draw()
h = QHist()
h.params = 'PTTRIOMID/1000'
h.xlog   = True
h.xmin   = 1
h.xmax   = 30
h.xlabel = 'mid(p_{T}) [MeV/c]'
# h.draw()
h = QHist()
h.params = 'PTTRIOMAX/1000'
h.xlog   = True
h.xmin   = 1
h.xmax   = 50
h.xlabel = 'max(p_{T}) [MeV/c]'
# h.draw()

#-----

h = QHist()
h.params = 'M'
h.xmin   = 700
h.xmax   = 1500
h.xlabel = 'Mass [MeV/c^{2}]'
# h.draw()

h = QHist()
h.params = 'ADOCAMAX'
h.xlog   = True
h.xmin   = 1E-3
h.xmax   = 2E-1
h.xlabel = 'max(DOCA) [mm]'
# h.draw()

h = QHist()
h.params = 'VCHI2PDOF'
h.xlog   = True
h.xmin   = 2E-2
h.xmax   = 20
# h.draw()

# QHist.master_cuts += ['VCHI2PDOF < 5']


# QHist.master_cuts += ['PTFrac05C>0.9', 'MIP>0.01', 'MIP<0.1', 'VCHI2PDOF<5', 'DRTRIOMIN<0.1', 'DRTRIOMID<0.12', 'DRTRIOMAX<0.15']

h = QHist()
h.params = 'BPVCORRM'
# h.xlog   = True
h.xmin   = 1000
h.xmax   = 2700
h.xbin   = 50
h.xlabel = 'Corrected Mass [\mevcc]'
# h.draw()

h = QHist()
h.params = 'BPVVD'
h.xlabel = 'Flight distance [mm]'
h.xmin   = 1E-1 
h.xmax   = 2E2
h.xbin   = 50
# h.draw()

# exit()

#------------------#
# Impact Parameter #
#------------------#

h = QHist()
h.name   = 'BPVIP'
h.params = 'BPVIP'
h.xlog   = True
h.xmin   = 3E-4
h.xmax   = 3E0
h.xlabel = 'Impact parameter [mm]'
h.st_fill = False
h.st_marker = True
h.draw()


h = QHist()
h.name      = 'BPVIP__PT'
h.params    = 'TMath::Log10(BPVIP) : PT/1000'
h.xmin      = 5
h.xmax      = 100
h.xbin      = 50
h.ymin      = -2.5
h.ymax      = 0
h.xlog      = True
# h.ylog      = True
h.st_code   = 1
h.st_marker = True
h.options   = 'prof'
h.xlabel    = 'PT [GeV]'
h.ylabel    = 'log10(Impact parameter) [mm]'
h.draw()

h = QHist()
h.name      = 'BPVIP__ETA'
h.params    = 'TMath::Log10(BPVIP) : ETA'
# h.params    = 'BPVIP : ETA'
h.xmin      = 2
h.xmax      = 4.5
h.xbin      = 50
h.ymin      = 1E-2
h.ymax      = 3E0
# h.ylog      = True
h.ymin      = -2.5
h.ymax      = 0
h.st_code   = 1
h.st_marker = True
h.options   = 'prof'
h.xlabel    = 'ETA'
h.ylabel    = 'log10(Impact parameter) [mm]'
h.draw()


exit()

#------------------------------------------------------------

#-----------#
# ISOLATION #
#-----------#

## Just for show
h = QHist()
h.params = 'PTFrac05C'
h.xmin   = -0.1
h.xmax   = 1.1
h.ylog   = True
h.xlabel = '\hat{I}_{PT}'
# h.draw()


h = QHist()
h.name      = 'PTFrac05C__PT'
h.params    = 'PTFrac05C : PT/1000'
h.xmin      = 5
h.xmax      = 50
h.xbin      = 30
h.xlog      = True
h.ymin      = 0.
h.ymax      = 1.
h.st_code   = 1
h.st_marker = True
h.options   = 'prof'
h.xlabel    = 'PT [GeV]'
h.ylabel    = '\hat{I}_{PT}'
# h.draw()

h = QHist()
h.params = 'PTCone05C'
h.xmin   = 5E1
h.xmax   = 5E4
h.xlog   = True
# h.cuts   = 'PTFrac05C > 0.8'
h.xlabel = 'I_{PT}'
# h.draw()

h = QHist()
h.name      = 'PTCone05C__PT'
h.params    = 'TMath::Log10(PTCone05C) : PT/1000'
h.cuts      = 'PTCone05C>100'
h.xmin      = 5
h.xmax      = 50
h.xbin      = 30
h.xlog      = True
h.ymin      = 2.5
h.ymax      = 4.5
h.st_code   = 1
h.st_marker = True
h.options   = 'prof'
h.xlabel    = 'PT [GeV]'
h.ylabel    = 'I_{PT}'
h.draw()

exit()


#---------#
# H1 only #
#---------#

h = QHist()
h.params = 'EFrac02PN05N'
h.xmin   = -0.1
h.xmax   = 1.1
h.ylog   = True
# h.draw()

# h = QHist()
# h.params = 'EFrac02PN05A'
# h.master_cuts += ['PTFrac05C>0.9', 'PTCone05C<2000']
# h.xmin   = -0.1
# h.xmax   = 1.1
# h.ylog   = True
# h.draw()

#---------#
# E only #
#---------#

h = QHist()
h.params = 'ECone02N'
# h.xmin   = 100
# h.xmax   = 1.1
h.ylog   = True
h.cuts   = 'PTFrac05C>0.9'
# h.draw()


raw_input()
# 