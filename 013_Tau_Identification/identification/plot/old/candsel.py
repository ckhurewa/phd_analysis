#!/usr/bin/env python

from MyPythonLib.PyROOT import QHist, import_tree

## Use loose version

# jid, title = 2636, 'Z0'
# jid, title = 2637, 'H2AA'

jid = 2727 # All, H2AA 13TeV

t = import_tree(jid, 'TauCandTupleWriter/LooseTauCand_h3' )

t.SetAlias('MT', '(PT**2 + MM**2)**0.5')
t.SetAlias('PT_CMID', 'PT_CMEAN*3 - PT_CMIN-PT_CMAX')

QHist.trees     = t
QHist.cuts      = ('MATCH', '!MATCH')
QHist.legends   = ('Real', 'Fake')
# QHist.prefix    = title+'_tau_h3'
# QHist.auto_save = True

N0 = t.Draw("MATCH", "MATCH", "goff")
QHist.comment = 'Initial after precut: %d / 5088'%N0

QHist.master_cuts = (
  'ETA      > 2',
  'ETA      < 4.5',
  'MM       > 600',
  'MM       < 1500',
  'MT       > 6000',
  'BPVCORRM < 4000',  
  # 'abs(BPVDIRA) > 0.9999',
  'BPVDLS     > 1',
  'BPVDLS     < 100',
  'BPVIP      < 1',
  'BPVIPCHI2  < 200',
  'BPVVDZ     > 1',
  'ECone05_PFCHARGED  < 200000',
  'ECone05_PFALL      < 300000',
  'PTCone05_PFCHARGED < 5000',
  'PTFrac05C  > 0.8',
  'EFrac05C   > 0.8',
  'VCHI2PDOF  < 20',
  'DRTRIOMAX  < 0.4',
  'DRTRIOMID  < 0.3',
  'DRTRIOMIN  < 0.15',
  'TRGHOSTPROB_CMAX < 0.2',
  'PT_CMIN    > 500',
  'PT_CMID    > 1500',
  'PT_CMAX    > 4000',
  'BPVIP_CMAX < 4',
)

# 
# 

h = QHist()
h.params = 'PTFrac05A'
# h.xmin   = 0.9996
# h.xmax   = 10000
# h.xlog   = True
# h.ylog   = True
h.draw()

#--------------------------------

h = QHist()
h.params = 'MM'
# h.draw()

h = QHist()
h.params = 'PT'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'BPVCORRM'
h.xlog   = True
h.ylog   = True
# h.draw()

h = QHist()
h.params = 'abs(BPVDIRA)'
h.xmin   = 0.99
h.ylog   = True
# h.draw()

h = QHist()
h.params = 'BPVDLS'
h.xmin   = -40
h.xmax   = 160
h.ylog   = True
# h.draw()

h = QHist()
h.params = 'BPVIP'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'BPVIPCHI2'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'BPVLTIME'
h.xmin   = 1E-7
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'BPVVDCHI2'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'BPVVDRHO'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'BPVVDZ'
h.ylog   = True
h.xmin   = -50
h.xmax   = 250
# h.draw()

h = QHist()
h.params = 'AALLSAMEBPV'
h.xmin   = 0
h.xmax   = 1.001
h.xbin   = 2
h.ymin   = 0
# h.draw()

h = QHist()
h.params = 'ADOCACHI2MAX'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'ADOCAMAX'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'ADOCAMIN'
h.xlog   = True
# h.draw()

h = QHist()
h.params = 'VCHI2PDOF'
h.xlog   = True
h.xmax   = 100
# h.draw()

h = QHist()
h.params = 'VPCHI2'
h.xlog   = True
h.ylog   = True
h.xmin   = 1E-18
# h.draw()

#------ TODO: EFrac: Show 2 outer cone: Charge & ALL (need PE)

h = QHist()
# h.params = 'PE/(PE+ECone05_PFALL)'
h.params = 'EFrac05'
# h.xlog   = True
# h.ylog   = True
# h.xmin   = 1E-18
# h.draw()



raw_input()
