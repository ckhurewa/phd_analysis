#!/usr/bin/env python

"""
Aim: To see the ETA/PT-dependence on the reconstructible types
"""

from PyrootCK import import_tree, QHist

tree_names = (
  'TauRecoStats/TauChildReco__e_e',
  'TauRecoStats/TauChildReco__mu_mu',
  'TauRecoStats/TauChildReco__h1_pi',
  'TauRecoStats/TauChildReco__h3_pi',
)

for jid,title in [(2591,'H2AA'), (2592,'Z0')]:
  for tname in tree_names:
    t = import_tree(jid, tname, title=title)

    QHist.prefix  = title
    QHist.suffix  = 'tau'+tname.split('__')[-1]
    QHist.trees   = t 
    QHist.cuts    = (
      'IMCReconstructible_CODE==2',
      'IMCReconstructible_CODE==3',
      'IMCReconstructible_CODE==4',
      'IMCReconstructible_CODE>4',
      'IMCReconstructible_CODE==0',
      'IMCReconstructible_CODE==1',
    )
    QHist.legends = (
      'LONG track',
      'Upstream',
      'Downstream',
      'Other (TT/Velo)',
      'NotReconstructible',
      'OutsideAcceptance',
    )

    h = QHist()
    h.name    = 'RecoType_MCETA'
    h.params  = 'MCETA'
    h.xmin    = 0
    h.xmax    = 6
    h.xbin    = 30
    # h.ylog    = True
    h.ymin    = 0
    h.xlabel  = 'MCETA'
    h.st_line = False
    h.st_grid = False
    h.st_marker = True

    h.normalize = False

    h.draw()

    # h = QHist()
    # h.name   = 'RecoType_MCPT'
    # h.params = 'MCPT'
    # h.xmin    = 99
    # h.xmax    = 1e5
    # h.xlog    = True
    # h.ylog    = True
    # h.xlabel  = 'MCPT'
    # h.draw()
  #   break
  # break

# raw_input()