#!/usr/bin/env python

from PyrootCK import import_tree, QHist, ROOT 
ROOT.gROOT.SetBatch(True)

#--------------------#
# RECOA contribution #
#--------------------#

# n = t.Draw('IMCReconstructible_CODE', '')
# l = (t.GetV1()[i] for i in xrange(n))
# print array('d', l)
# print array('d', itertools.islice(t.GetV1(), n))

# se = pd.DataFrame(tree2rec(t, ['IMCReconstructible_CODE'])).IMCReconstructible_CODE
# from collections import Counter
# c = Counter(se)
# arr = array('d', (c[key] for key in sorted(c)))

# arr = array('d', [1,2,3,4])

# c = TCanvas()
# h = TPie("name", 'title', len(arr), arr)
# h.Draw()
# c.Update()


#---------------------


def raw_recoeff_singleRecoType(prefix, tree, list_tes):
  h = QHist()
  h.prefix  = prefix
  h.name    = 'LongTrack_RecoEff'
  # h.name    = 'AnyTrack_RecoEff'
  h.trees   = tree
  h.params  = ['%s_ID!=0: MCPT'%tes for tes in list_tes]
  h.legends = list_tes
  h.options = 'prof'
  h.xlog    = True
  h.xmin    = 999
  h.xmax    = 1E5
  h.xbin    = 24
  h.ymin    = 0
  h.ymax    = 1
  h.xlabel  = 'MCPT'
  h.ylabel  = 'Fraction'
  # h.master_cuts = 'IMCReconstructible_CODE>=2'
  h.master_cuts = 'IMCReconstructible_CODE==2'
  h.st_line     = False
  h.st_marker   = True
  h.draw()


def draw_recoeff_singleTES(prefix, tree, tes):
  h = QHist()
  h.prefix  = prefix
  h.name    = 'RecoEff'
  h.suffix  = tes
  h.trees   = tree
  h.params  = '%s_ID!=0: MCPT'%tes
  h.cuts    = (
    'IMCReconstructible_CODE==2',
    'IMCReconstructible_CODE==3',
    'IMCReconstructible_CODE==4',
    'IMCReconstructible_CODE==6',
  )
  h.legends = (
    'LONG',
    'Downstream',
    'Upstream',
    'VELO',
  )
  h.options = 'prof'
  h.xlog    = True
  h.xmin    = 999
  h.xmax    = 1E5
  h.xbin    = 10
  h.xlabel  = 'MCPT (child)'
  h.ylabel  = 'Fraction'
  h.hasLineStyle=False
  h.hasMarkerStyle=True
  h.comment = 'Given prediction from IMCReconstructible, see the efficiency yield from single TES container.'
  h.draw()


from config import CHILDTYPE_LISTTES

for jid,title in [(2591, 'H2AA'), (2592, 'Z0')]:
  for nickname, list_tes in CHILDTYPE_LISTTES.iteritems():
    tree = import_tree(jid, 'TauRecoStats/TauChildReco__%s'%nickname)
    #
    prefix = title+'_tau_'+nickname.split('_')[0]
    raw_recoeff_singleRecoType(prefix, tree, list_tes)
    # for tes in list_tes:
    #   draw_recoeff_singleTES(prefix, tree, tes)


# raw_input()