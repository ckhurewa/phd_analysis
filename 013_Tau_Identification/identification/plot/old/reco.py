#!/usr/bin/env python

from MyPythonLib.PyROOT import import_tree, QHist
from ROOT import gROOT, gPad

gROOT.SetBatch(True)


#-----------#
# RECOSTATS #
#-----------#

QHist.reset()

# for jid,title in [(2570,'Z0'), (2571,'H2AA')]:
for jid,title in [(2591, 'H2AA'), (2592, 'Z0')]:
  for ttype in ('h1',):
  # for ttype in ('mu', 'e', 'h1', 'h3'):
    t = import_tree(jid, 'TauRecoStats/TauReco_%s'%ttype, title=title)

    ## Reco-efficiency as a func of MCPT (charged track only)
    h = QHist()
    h.trees   = t
    h.params  = ( 
      'count_child_INACC==nChildren_charged:MCPT_charged', 
      'count_child_RECOA==nChildren_charged:MCPT_charged', 
      'count_child_RECOD==nChildren_charged:MCPT_charged', 
      'count_child_PFParticles==nChildren_charged:MCPT_charged',
      'count_child_StdAllNoPIDsPions==nChildren_charged:MCPT_charged',
      'count_child_StdAllLoosePions==nChildren_charged:MCPT_charged',
    )
    h.prefix  = title+'_'
    h.name    = 'RecoEffMCPT_tau_'+ttype
    h.options = 'prof'
    h.hasLineStyle = False
    h.hasMarkerStyle = True
    h.xlog    = True
    h.xmin    = 990
    h.xmax    = 1E5
    h.xbin    = 30
    # h.ymin    = 1
    # h.ylog    = True
    # h.xlabel  = 'MCPT charged track(s)'
    h.ylabel  = 'Fraction'
    h.legends = [
      'All children in acceptance', 
      'All children IMCReconstructible', 
      'All children IMCReconstructed', 
      'All children reco in ParticleFlow',
      'All children reco in StdAllNoPIDsPions',
      'All children reco in StdAllLoosePions',
    ]
    h.draw()

    # ## Reco Resolution & Quality
    t   = import_tree(jid, 'TauRecoStats/TauChildReco__%s_pi'%ttype, title=title)
    exp = (
      (t, 'PFParticles_PT      / MCPT : MCPT', 'PFParticles_PT>0'),
      (t, 'StdAllLoosePions_PT / MCPT : MCPT', 'StdAllLoosePions_PT>0'),
    )
    h = QHist()
    h.expansion = exp
    h.prefix  = title+'_'
    h.name    = 'RecoQualityPT_tau_'+ttype
    # h.options = 'CONT1Z'
    h.options = 'prof '
    h.xlog    = True
    h.xmin    = 990
    h.xmax    = 1E5
    h.xbin    = 30
    # h.ymin    = 0.98
    # h.ymax    = 1.02
    h.draw()
    # h.params = 'PT/MCPT'



## (H3 only): MC Truth on MCDR:MCPT (charged)

t_Z0_h3_truth   = import_tree(2578, 'TauH3ConeAnalysis/tau_h3', title='Z0')
t_Z0_h3_reco    = import_tree(2574, 'SelTauCand/tau_h3_cand'  , title='Z0')
t_H2AA_h3_truth = import_tree(2576, 'TauH3ConeAnalysis/tau_h3', title='H2AA')
t_H2AA_h3_reco  = import_tree(2573, 'SelTauCand/tau_h3_cand'  , title='H2AA')

exp = (
  ( t_Z0_h3_truth,  'MCDR_mean:MCPT_charged', ''       ),
  ( t_Z0_h3_reco ,  'DR_mean:recotau_PT'    , 'MATCH'  ),
  ( t_Z0_h3_reco ,  'DR_mean:recotau_PT'    , '!MATCH' ),
)

QHist.reset()
h = QHist()
h.name      = 'Z0_evo_DR_mean'
h.expansion = exp
h.options   = 'CONT3'
h.xmin      = 0
h.xmax      = 50000
h.xbin      = 10
h.ymax      = 0.3
h.ybin      = 10
h.zbin      = 8
h.legends   = ( '3prongs MC', '3prongs reco(PF)', '3prongs reco(fake combi)' )
h.xlabel    = 'PT (3prongs)'
h.ylabel    = 'DR'
h.hasLineColor = True
h.hasLineStyle = False
# h.draw()



# raw_input()