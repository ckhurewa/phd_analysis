#!/usr/bin/env python

import sys
from PyrootCK import *

## TauCand identification
# JID = 3016 
# JID = 3033
# JID = 3215
JID = '/Users/khurewat/Documents/lphe_offline/gangadir_large/3215.root'

## Ditau generator-level
t_e   = import_tree( 'LooseTauCandTupleWriter/e' , JID ).SetTitle('e')
t_mu  = import_tree( 'LooseTauCandTupleWriter/mu', JID ).SetTitle('mu')
t_h1  = import_tree( 'LooseTauCandTupleWriter/h1_PT4', JID ).SetTitle('h1')
t_h3  = import_tree( 'LooseTauCandTupleWriter/h3_DRTRIOCUTS', JID ).SetTitle('h3')

t0_h1 = import_tree( 'LooseTauCandTupleWriter/h1_nobias_prescale1Em2', JID ).SetTitle('h1_p1Em2')
t0_h3 = import_tree( 'LooseTauCandTupleWriter/h3_nobias_prescale1Em2', JID ).SetTitle('h3_p1Em2')

QHist.master_cuts = [ 'hasRECO' ]
QHist.cuts   = 'MATCH', '!MATCH'

#-------------------------------------
# PT
#-------------------------------------

for t in (t_e, t_mu, t0_h1, t0_h3):
  h = QHist()
  h.name    = 'PT'
  h.suffix  = t.GetTitle()
  h.trees   = t
  h.params  = 'PT'
  h.xlog    = True
  h.xmin    = 1E2
  h.xmax    = 1E5
  h.xlabel  = '$p_T$ [MeV/c]'
  # h.draw()

t_e.SetAlias  ('PTCUT', 'PT>5000')
t_mu.SetAlias ('PTCUT', 'PT>5000')
t_h1.SetAlias ('PTCUT', 'PT>5000')
t0_h1.SetAlias('PTCUT', 'PT>5000')
t_h3.SetAlias ('PTCUT', 'PT>10000')
t0_h3.SetAlias('PTCUT', 'PT>10000')


QHist.master_cuts += [ 'PTCUT' ]

#-------------------------------------

## tau_h3 DRTRIO cuts
## Put this first since I have ntuple made on top of this with no prescale.
for p in ['DRTRIOMIN', 'DRTRIOMID', 'DRTRIOMAX']:
  h = QHist()
  h.name    = 'h3_'+p
  h.trees   = t0_h3
  h.params  = p
  h.xlog    = True
  h.xmin    = 1E-2
  h.xmax    = 4
  # h.draw()

## Use t_h3 instead of t0_h3 from here

CUTS_TAUH3 = [
  'DRTRIOMIN < 0.10',
  'DRTRIOMID < 0.15',
  'DRTRIOMAX < 0.20',
]

#-------------------------------------

## tau_h3 PTTRIO cuts
for p in ['PTTRIOMIN', 'PTTRIOMID', 'PTTRIOMAX']:
  h = QHist()
  h.name    = 'h3_'+p
  h.trees   = t_h3
  h.master_cuts += CUTS_TAUH3
  h.params  = p
  h.xlog    = True
  h.xmin    = 1E2
  h.xmax    = 1E5
  # h.draw()

CUTS_TAUH3 += [
  'PTTRIOMIN > 1000',
  'PTTRIOMID > 4000',
  'PTTRIOMAX > 8000',
]
  
#-------------------------------------


## tau_h3 selection
h = QHist()
h.name    = 'h3_M'
h.trees   = t_h3 
h.params  = 'M'
h.master_cuts += CUTS_TAUH3
h.xmin    = 0
h.xmax    = 3E3
h.xlabel  = 'M [MeV/$c^2$]'
# h.draw()


h = QHist()
h.name    = 'h3_VCHI2PDOF'
h.trees   = t_h3 
h.params  = 'VCHI2PDOF'
h.master_cuts += CUTS_TAUH3
h.xlog    = True
# h.draw()

CUTS_TAUH3 += [
  'M > 900',
  'M < 1400',
  'VCHI2PDOF < 10',
]

#-------------------------------------

h = QHist()
h.name   = 'h3_ADOCAMAX'
h.trees  = t0_h3
h.params = 'ADOCAMAX'
h.xlog   = True
h.xlabel = 'Max distance of closest approach [mm]'
# h.draw()

h = QHist()
h.name    = 'h3_BPVCORRM'
h.trees   = t0_h3 
h.params  = 'BPVCORRM'
h.xmin    = 0
h.xmax    = 6E3
h.xlabel  = 'Corrected Mass [MeV/$c^2$]'
# h.draw()

#-------------------------------------

for t in (t_e, t_mu, t_h1, t_h3):

  h = QHist()
  h.name    = 'Isolation'
  h.suffix  = t.GetTitle()
  h.trees   = t
  h.params  = 'PTFrac05C: PTCone05C'
  # h.xlog    = True
  h.ymin    = 0
  h.ymax    = 1.05
  h.xmin    = 1E2
  h.xmax    = 1E5
  h.xlog    = True
  # h.zlog    = True
  h.xbin  = 24
  h.ybin  = 24
  h.zbin  = 15
  h.options = 'cont3'
  # h.draw()

  h = QHist()
  # h.name   = 'PTFrac05C'
  h.suffix = t.GetTitle()
  h.trees  = t
  h.params = 'PTFrac05C'
  h.xmin   = -0.1
  h.xmax   = 1.1
  h.ylog   = True
  if 'h3' in t.GetTitle():
    h.master_cuts += CUTS_TAUH3
  # h.draw()

  h = QHist()
  h.name   = 'PTCone05C'
  h.suffix = t.GetTitle()
  h.trees  = t
  h.params = 'PTCone05C'
  h.master_cuts += [ 'PTFrac05C>0.9' ]
  h.xmin   = 1E1
  h.xmax   = 1E5 
  h.xlog   = True
  if 'h3' in t.GetTitle():
    h.master_cuts += CUTS_TAUH3
  h.draw()
  

## tau_h1 
h = QHist()
h.trees   = t_h1 
h.name    = 'h1_EFrac02PN05N'
h.params  = 'EFrac02PN05N'
h.master_cuts += [ 'PT>5000', 'PTFrac05C>0.8', 'PTCone05C<2000' ]
# h.draw()
#
h = QHist()
h.trees   = t_h1 
h.name    = 'h1_EFrac02PN05A'
h.params  = 'EFrac02PN05A'
h.master_cuts += [ 'PT>5000', 'PTFrac05C>0.8', 'PTCone05C<2000', 'EFrac02PN05N>0.8' ]
# h.draw()
#
h = QHist()
h.trees   = t_h1 
h.name    = 'h1_ECone02C'
h.params  = 'ECone02C'
h.xmin    = 1E2
h.xlog    = True
h.master_cuts += [ 'PT>5000', 'PTFrac05C>0.8', 'PTCone05C<2000', 'EFrac02PN05N>0.8', 'EFrac02PN05A>0.8' ]
# h.draw()

## tau_e
h = QHist()
h.trees   = t_e
h.name    = 'e_EFrac05A'
h.params  = 'EFrac05A'
h.master_cuts += [ 'PT>5000', 'PTFrac05C>0.8', 'PTCone05C<2000' ]
# h.draw()

#----

#ECone05C, EFrac05A

# h = QHist()
# h.trees = t_h3 
# h.name   = 'h3_ECone05C'
# h.params = 'ECone05C'
# h.master_cuts += [ 'PT>10000', 'PTFrac05C>0.9', 'PTFrac05C<2000', 'M>700', 'M<1500', 'BPVCORRM>1000', 'BPVCORRM<2700', 'ADOCAMAX<0.1', 'PTTRIOMIN>1000' ]
# h.xmax    = 70E3
# # h.ylog    = True
# h.draw()


raw_input()
