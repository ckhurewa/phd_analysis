#!/usr/bin/env python

from PyrootCK import import_tree, QHist, ROOT
ROOT.gROOT.SetBatch(True)

## (H3 only): MC Truth on MCDR:MCPT (charged)

t_H2AA_h3_truth = import_tree(2576, 'TauH3ConeAnalysis/tau_h3', title='H2AA')
t_H2AA_h3_reco  = import_tree(2573, 'SelTauCand/tau_h3_cand'  , title='H2AA')

t_Z0_h3_truth   = import_tree(2578, 'TauH3ConeAnalysis/tau_h3', title='Z0')
t_Z0_h3_reco    = import_tree(2574, 'SelTauCand/tau_h3_cand'  , title='Z0')


exp_H2AA = (
  ( t_H2AA_h3_truth,  'MCDR_mean:MCPT_charged', ''       ),
  ( t_H2AA_h3_reco ,  'DR_mean:recotau_PT'    , 'MATCH'  ),
  ( t_H2AA_h3_reco ,  'DR_mean:recotau_PT'    , '!MATCH' ),
)

exp_Z0 = (
  ( t_Z0_h3_truth,  'MCDR_mean:MCPT_charged', ''       ),
  ( t_Z0_h3_reco ,  'DR_mean:recotau_PT'    , 'MATCH'  ),
  ( t_Z0_h3_reco ,  'DR_mean:recotau_PT'    , '!MATCH' ),
)

h = QHist()
h.prefix    = 'Z0'
h.name      = 'evo_DR_mean'
h.expansion = exp_Z0
h.options   = 'CONT3'
h.xmin      = 1E3
h.xmax      = 1E5
h.xbin      = 10
h.xlog      = True
h.ymax      = 0.3
h.ybin      = 10
h.zbin      = 8
h.legends   = ( '3prongs MC', '3prongs reco(PF)', '3prongs reco(fake combi)' )
h.xlabel    = 'PT (3prongs)'
h.ylabel    = 'DR'
h.st_color  = True
h.st_line   = False
h.draw()


raw_input()