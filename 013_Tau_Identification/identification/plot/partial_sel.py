#!/usr/bin/env python

"""

Study the uncertainty of some selection variable (notable the tauh3-exclusives),
by temporary lifted that variable and observe the distribution using normalized
results.

Submitting to slurm
    sbatch -N1 -n1 --mem=2000 partial_sel.py eh3

"""

import subprocess
from PyrootCK import *
from id_utils import pd, get_current_dtype_dt, DT_TO_DTYPE, memorized

## Filter by mode
FILTERS = {
  'tau_VCHI2PDOF': 'Preselection & (tau_DRoPT<5e-3)   & ({ISO}) & _Displ     & _APT & _DPHI',
  'tau_DRoPT'    : 'Preselection & (tau_VCHI2PDOF<20) & ({ISO}) & _Displ     & _APT & _DPHI',
  'tau_BPVLTIME' : 'Preselection & _Prongs & ({ISO}) & (tau_BPVCORRM<3e3)    & _APT & _DPHI',
  'tau_BPVCORRM' : 'Preselection & _Prongs & ({ISO}) & (tau_BPVLTIME*1e6>60) & _APT & _DPHI',
}

#===============================================================================

def main_export_single(dt, mode):
  assert 'h3' in dt
  filt0 = FILTERS[mode]

  ## Import all trees, slow
  from ditau_prep import ALLTREES, T_data_nobias
  tnames = [
    # 't_ztautau',
    't_dymu',
    't_wmu17jet',
    't_zmu17jet',
    't_wmu17jet_noiso',
    't_zmu17jet_noiso',
    # 't_ttbar_41900010',
    # 't_ztaujet',
    # 't_we_noiso',
    #
    ## Not for BPVCORRM, VCHI2PDOF, DPHI
    't_real',
    'tdd_qcd',
  ]
  trees = {}
  for tname in tnames:
    iso  = '1==1' if tname.endswith('_noiso') else '_AntiIso' if tname.endswith('qcd') else '_Iso'
    filt = filt0.format(ISO=iso)
    tree = ALLTREES[tname]
    tree.SetAlias('EXPORT', filt)
    trees[tname] = tree

  ## real, noiso, experimental
  filt = filt0.format(ISO='1==1')
  tree = ALLTREES['t_real']
  tree.SetAlias('EXPORT', filt)
  trees['t_real_noiso'] = tree

  # ## Extra care for data,dd_qcd
  # t_real = T_data_nobias(DT_TO_DTYPE[dt], 't_real', 'Real (OS)')
  # t_real.SetAlias('EXPORT', filt0.format(ISO='_Iso'))
  # trees['t_real'] = t_real
  #
  # tdd_qcd = T_data_nobias(DT_TO_DTYPE[dt], 'tdd_qcd', 'DD ~ QCD (OS)')
  # tdd_qcd.SetAlias('EXPORT', filt0.format(ISO='_AntiIso'))
  # trees['tdd_qcd'] = tdd_qcd

  ## Ready, fire!
  ROOT.TFile.export_trees(
    trees  = trees,
    params = ['M', 'runNumber', 'eventNumber', 'ISO1', 'ISO2', 'cc_vPT1', 'cc_vPT2', mode],
    filt   = 'EXPORT',
    # target = 'partial_sel/%s_%s.root'%(dt, mode),
    target = 'partial_sel/temp.root',
  )

#===============================================================================

# ordered
TNAMES = {
  'h3mu': ['t_real', 't_ztautau', 'tdd_qcd', 't_wmu17jet_noiso', 't_ttbar_41900010'],
  # 'h3mu': ['t_real', 't_ztautau', 'tdd_qcd', 't_wmu17jet', 't_ttbar_41900010'],
  # 'eh3' : ['t_real']
}
RENAME = {
  't_real'          : 'OS',
  't_ztautau'       : 'Ztau',
  'tdd_qcd'         : 'QCD',
  't_wmu17jet_noiso': 'EWK',
  't_wmu17jet'      : 'EWK',
  't_ttbar_41900010': 'Other',
}
THRESHOLDS = {
  'tau_VCHI2PDOF': '<= 20',
  'tau_DRoPT'    : '<= 0.005',
  'tau_BPVLTIME' : '> 60e-6',
  'tau_BPVCORRM' : '< 3e3',
}


def get_scaling(dt, mode):
  """
  Compute the scaling factor, which is the number to multiply with next_selected
  and to be used in the QHist normalization.
  """
  threshold = THRESHOLDS[mode]
  fin = ROOT.TFile('partial_sel/%s_%s.root'%(dt,mode))
  # fin2 = ROOT.TFile('partial_sel/temp.root')

  acc = pd.Series()
  for tname in TNAMES[dt]:
    tree = fin.Get(tname)
    # if tname == 't_wmu17jet':
    #   tree = fin2.Get(tname)

    n0   = tree.count_ent_evt('({0} > 0)'.format(mode))[1]
    n1   = tree.count_ent_evt('({0} > 0) & ({0} {1})'.format(mode, threshold))[1]
    acc[RENAME[tree.name]] = 1.*n0/n1
  fin.Close()
  # fin2.Close()
  return acc


def get_trees(dt, mode):
  """
  Return the ordered trees at given dt,mode.
  """
  fin = ROOT.TFile('partial_sel/%s_%s.root'%(dt, mode))
  # fin2 = ROOT.TFile('partial_sel/temp.root')
  fin.ownership = False
  # fin2.ownership = False
  trees = []
  for tname in TNAMES[dt]:
    tree = fin.Get(tname)
    # if tname == 't_wmu17jet':
    #   tree = fin2.Get(tname)
    tree.ownership = False
    trees.append(tree)


  return trees

@memorized
def get_nexp_selected(dt):
  """
  Return the number of expected candidates, with all cuts inplace.
  At the order appropriate to h3mu/eh3.
  """
  import candidates
  cols = [RENAME[tname] for tname in TNAMES[dt]]
  return candidates.results_comb_withmasswindow()[dt][cols]

def prep_qhist(dt, mode):
  """
  Return the qhist prep with data, generic to all dt/mode, ready for tuning.
  """
  ## Prepare the ingredients
  trees = get_trees(dt, mode)
  nexp  = get_nexp_selected(dt)
  scale = get_scaling(dt, mode)
  nmz   = [x.n for x in nexp * scale] # flatten Series->list, ufloat->float

  print nexp
  print scale
  print nmz
  print sum(nmz[1:])

  # print nexp
  # nmz[2] *= 2
  # nmz[3] = nexp[3].n*20.

  ## Attach
  h = QHist()
  h.prefix    = dt
  h.name      = mode
  h.trees     = trees
  h.params    = mode
  h.normalize = nmz
  h.st_code   = 2
  h.xbin      = 20
  return h

def draw(dt):
  pass
  # prep_qhist(dt, 'tau_VCHI2PDOF').draw(xmin=0   , xmax=40  , xlog=False)
  # prep_qhist(dt, 'tau_VCHI2PDOF').draw(xmin=1e-2  , xmax=1e4, xlog=True)
  # prep_qhist(dt, 'tau_DRoPT'    ).draw(xmin=1e-4, xmax=1e-1, xlog=True )
  prep_qhist(dt, 'tau_BPVLTIME' ).draw(xmin=1e0, xmax=1e4, xbin=20, xlog=True, params='tau_BPVLTIME*1e6') # NOT OK
  # prep_qhist(dt, 'tau_BPVCORRM' ).draw(xmin=0, xmax=5, params='tau_BPVCORRM/1e3')

#===============================================================================

if __name__ == '__main__':
  pass

  ## Extract raw ntuple to small ntuple. Very slow
  # dtype, dt = get_current_dtype_dt()
  # main_export_single(dt, 'tau_VCHI2PDOF')
  # main_export_single(dt, 'tau_DRoPT')
  # main_export_single(dt, 'tau_BPVLTIME')
  # main_export_single(dt, 'tau_BPVCORRM')

  ## Read & Draw
  ROOT.gROOT.batch = True
  # print get_scaling_VCHI2PDOF('h3mu')
  # print get_nexp_selected('h3mu')
  # print prep_qhist('h3mu', 'tau_BPVLTIME')
  print draw('h3mu')
  # print [t.count_ent_evt() for t in get_trees('h3mu', 'tau_BPVLTIME')]
  # print [t.count_ent_evt('tau_BPVLTIME>0') for t in get_trees('h3mu', 'tau_BPVLTIME')]
  # print [t.count_ent_evt('tau_BPVLTIME>60e-6') for t in get_trees('h3mu', 'tau_BPVLTIME')]

  # print get_trees('h3mu', 'tau_BPVLTIME')[0].count_ent_evt()
