#!/usr/bin/env python

from PyrootCK import *

t_h1 = import_tree( 'CheckPVRefit/h1', '3334.root' )
t_h3 = import_tree( 'CheckPVRefit/h3', '3334.root' )

p_all     = 'norefit_best', 'norefit_min', 'autofit_best', 'autofit_min', 'manualfit_default_best', 'manualfit_default_min', 'manualfit_pvo_best', 'manualfit_pvo_min'
p_diff    = 'norefit_best/norefit_min', 'autofit_best/autofit_min', 'manualfit_default_best/manualfit_default_min', 'manualfit_pvo_best/manualfit_pvo_min', 

## Verdict:

## 1. Comparison between min/best
## 1.1 IP @ h1
# norefit: SAME      ??  ( there's no reason they can be the same ) Hypothesis: bestPV method pick best PV from minimizedChi2
# autofit: SAME
# default: different
# withpvo: SAME      ??

h = QHist()
h.name   = 'compare_bestmin_h1IP'
h.trees  = t_h1
h.params = p_diff
h.ylog   = True 
h.xlog   = True 
h.xmin   = 1E-1
# h.draw()

## 1.2 FD @ h3
# norefit: different
# autofit: SAME
# default: different 
# withpvo: different

h = QHist()
h.name   = 'compare_bestmin_h3FD'
h.trees  = t_h3
h.params = p_diff
h.ylog   = True 
h.xlog   = True 
h.xmin   = 1E-1
# h.draw()


## 2. Comparison against MC (shape)
## 2.1 IP @ h1 (avoid the bump!)
# norefit_best: X   --  Left  bump
# norefit_min : X   --  Left  bump
# autofit     : X   --  Right bump
# default_best: X   --  Right bump
# default_min : O
# withpvo_best: O
# withpvo_min : O

h = QHist()
h.name    = 'compare_shape_h1IP'
h.trees   = t_h1
h.params  = 'norefit_best', 'norefit_min', 'autofit_best', 'manualfit_default_best', 'manualfit_default_min', 'manualfit_pvo_best', 'manualfit_pvo_min'
# h.params  = 'mc', 'manualfit_default_min', 'manualfit_pvo_best', 'manualfit_pvo_min'
h.xlog    = True
h.xmin    = 1E-4
h.xmax    = 1E2
# h.draw()

h = QHist()
h.name    = 'compare_shape_h3FD'
h.trees   = t_h3
h.params  = 'mc', 'manualfit_pvo_best'
h.xlog    = True
# h.xmin    = 1E-1
# h.xmax    = 1E2
# h.draw()

## 3. Comparison deltaMC
## 3.1 IP @ h1, compare deltaMC
# norefit_best: NA
# norefit_min : NA
# autofit     : NA
# default_best: NA
# default_min : X -- just marginally worse
# withpvo_best: O
# withpvo_min : O

h = QHist()
h.name   = 'compare_deltamc_h1IP'
h.trees  = t_h1
h.params = '(manualfit_default_min-mc)/mc', '(manualfit_pvo_best-mc)/mc', '(manualfit_pvo_min-mc)/mc'
h.xmin   = -2
h.xmax   = +2
h.ylog   = True
# h.draw()

## 3.2 FD @ h3, compare deltaMC
# norefit_best: NA
# norefit_min : NA
# autofit     : NA
# default_best: NA
# default_min : X  -- Long left tail
# withpvo_best: O
# withpvo_min : X  -- Long left tail

h = QHist()
h.name   = 'compare_deltamc_h3FD'
h.trees  = t_h3
h.params = '(manualfit_default_min-mc)/mc', '(manualfit_pvo_best-mc)/mc', '(manualfit_pvo_min-mc)/mc'
h.xmin   = -2
h.xmax   = +2
h.ylog   = True
# h.draw()


h = QHist()
h.name   = 'compare_deltamcval_h3FD'
h.trees  = t_h3
h.params = '(manualfit_pvo_best-mc)/mc'
h.xmin   = -1
h.xmax   = +1
# h.ylog   = True
h.draw()

raw_input()