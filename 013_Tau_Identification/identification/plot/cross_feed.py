#!/usr/bin/env python

"""

Collect the channel-dependent generator efficiency from the summary.xml

This will be needed for the correct cross-feed contribution.

"""

## List to Gauss files
# JIDS = 4067, 4068, 4069, 4070, 4142, 4143, 4144, 4145, 4257, 4260

import os
from id_utils import *
from PyrootCK.mathutils import EffU
from collections import Counter

#===============================================================================

def load_xml(jid):
  import xml.etree.ElementTree as ET
  path = '/Users/khurewat/Documents/lphe_offline/gangadir/workspace/khurewat/LocalXML/%i/output/summary.xml'%jid
  if not os.path.exists(path):
    return None
  return ET.parse(path).getroot()

def load_counter(jid):
  """
  Load the summary.xml file into python counter
  """
  tree = load_xml(jid)
  ## skip missing, losing some stats doesn't matter
  if tree is None:
    return Counter()
  gen = tree.find('counters').iter('counter')
  return Counter({x.get('name'): long(x.text) for x in gen})

def load_statEntity(jid):
  tree = load_xml(jid)
  gen  = tree.find('counters').iter('statEntity')
  df   = pd.DataFrame()
  for node in gen:
    l = node.text.split()
    df[node.get('name')] = int(float(l[0])), int(float(l[1]))
  df.index = ['nume', 'deno']
  return df.T


#===============================================================================

@memorized
def egen():
  ## Load all data into counters
  counter = Counter()
  for jid in JIDS:
    counter += load_counter(jid)

  eff = {}
  for dt in CHANNELS:
    n = counter['ditau-moni/'+dt]
    d = counter['Generation/'+dt]
    eff[dt] = EffU(d,n)
  return pd.Series(eff)

@memorized
def eacc_post_egen():
  """
  Acceptance cut on the generated sample having lite generator level
  acceptance cut already to compress disk usage.
  """
  acc = pd.Series()
  df  = load_statEntity(5150)
  for dt in CHANNELS:
    key = 'FilterFiducialAccentance/valid_acceptance/'+DT_TO_DTYPE[dt]
    se  = df.loc[key]
    acc[dt] = EffU(se.deno, se.nume)
  return acc


def eacc_nlo():
  """
  Alternative method: Use simple NLO eacc since the starting point of my tuple
  is already required cand inside acceptance.
  """
  ## 160709, J5035: Large stat, fix ll small masscut mistake.
  # add 1.61% syst borrow previous paper
  return pd.Series({
    'mumu': ufloat( 0.389621, 0.006273, "acc" ),
    'h1mu': ufloat( 0.116294, 0.001872, "acc" ),
    'h3mu': ufloat( 0.181774, 0.002927, "acc" ),
    'ee'  : ufloat( 0.378022, 0.006086, "acc" ),
    'eh1' : ufloat( 0.114486, 0.001843, "acc" ),
    'eh3' : ufloat( 0.178769, 0.002878, "acc" ),
    'emu' : ufloat( 0.384642, 0.006193, "acc" ),
  })


#===============================================================================

def normalize_with_syst(se):
  """
  Return the normalized array (sum==1).
  - Handle nan properly
  - Keep pd.Series index
  - During the division, also add syst from Clopper-Pearson div
  """
  tot = 1.*sum(x for x in se if not np.isnan(nominal_value(x)))
  nom = nominal_value(tot)
  return se.apply(lambda x: EffU(nom, x))


_DEFAULT_PATH = '$DIR13/identification/plot/bkg_context'

@memorized
def results_raw(path=_DEFAULT_PATH):
  """
  Compute the cross-feed from Ztautau0-nofiducial sample,
  for both with/without mass window regime.
  """
  acc0   = {}
  count0 = df_from_path('check_ztau0_selected', path)['evt']

  ## separate 2 regimes: nomw, wmw
  for regime in ['nomw', 'wmw']:
    acc = pd.DataFrame()
    df0 = count0.xs(regime, level=1)
    for dt, df1 in df0.groupby(level=0):
      df1    = df1.xs(dt).unstack()
      se     = pd.Series()
      se[dt] = df1.loc['with_fidacc', 'truechan']
      # pick all other fake, including outside fiducial
      for bkg, val in df1.loc['without_fidacc'].filter(regex='from.*').iteritems():
        bkg_dt = bkg.split('_')[-1]
        if bkg_dt != dt:
          se[bkg_dt] = val
      acc[dt] = normalize_with_syst(se)
    acc0[regime] = acc
  return pd.concat(acc0)

@memorized
def results(path=_DEFAULT_PATH):
  """
  with mass window
  """
  df = results_raw(path).loc['wmw']
  se = pd.Series()
  for dt in df.columns: # Loop over existing columns
    se[dt] = 1.-df.loc[dt,dt]
  return se

@memorized
def results_nomasswindow(path=_DEFAULT_PATH):
  df = results_raw(path).loc['nomw']
  se = pd.Series()
  for dt in df.columns: # Loop over existing columns
    se[dt] = 1.-df.loc[dt,dt]
  return se

#===============================================================================

def latex_table():
  ## Detailed part
  df = results_raw().loc['wmw'].loc[CHANNELS_ALL, CHANNELS]
  df = df.rename(DT_TO_LATEX, columns=DT_TO_LATEX)
  df.index.name = 'Origin'
  df = df.applymap(lambda x: '-' if x.n<0.001 else x).fmt1lp

  ## Total part
  df2 = pd.DataFrame({'Total':results()}).T[CHANNELS]
  df2 = df2.fmt2lp.rename(columns=DT_TO_LATEX)

  ## Finally
  df = pd.concat([df, df2])
  inject_midrule(df, -1)
  return df.to_markdown(stralign='right')


def latex_table_lite():
  """
  Only the total impurity, for summary.
  """
  df = pd.DataFrame({'eff':results()}).T[CHANNELS]
  return df.fmt2lp.to_markdown(drop_index=True, rename_columns=DT_TO_LATEX)


def latex_table_detailed():
  """
  Table of sources vs final state, for appendix
  """
  df = results_raw().loc['wmw'].loc[CHANNELS_ALL, CHANNELS]
  df = df.rename(DT_TO_LATEX, columns=DT_TO_LATEX)
  df.index.name = 'Origin \\\\ Channel'
  df = df.applymap(lambda x: '-' if x.n<0.001 else x).fmt1lp
  return '\n'+df.to_markdown()


#===============================================================================

if __name__ == '__main__':
  pass
  # print eacc()
  # print egen() * eacc()

  # print results_raw()
  # print results_nomasswindow().fmt2p
  # print results().fmt2p

  ## LATEX
  print latex_table()
  print latex_table_lite()
  print latex_table_detailed()
