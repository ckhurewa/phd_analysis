#!/usr/bin/env python

"""

Handle the computation of same-sign background: QCD+EWK

Note to self: Use cache with care, as there's a lot of correlation going on here.

"""

from PyrootCK import *
from id_utils import *
from uncertainties import nominal_value
from PyrootCK.QHist import postprocessors

import samesign_utils 
import selected

## path to cache
PATH_SAMESIGN = os.path.splitext(os.path.abspath(__file__))[0]
PATH_CONTEXT  = '$DIR13/identification/plot/bkg_context'

#===============================================================================
# SAME-SIGN = QCD + EWK
#===============================================================================

## 160711: Removed TRGHP (dilepton mode C)
## 160813: DOCACHI2>15 for mumu,ee, APT<0.5 for emu, DRoPT<0.012&DRtPT<5 for h3
## 160816: TFractionFitter restricted rangeX
## 160818: APT<0.5 for emu, DRoPT<0.007&DRtPT<4
## 160830: Revamped SS to include Ztau, lots of cuts loosened
## 160902: DOCACHI2>0.1 in non-ll channel
## 161011: No soft DOCACHI2>0.1, tight >20, tauh3 BPVLTIME, const lumi, Zee norm corr, revise rZ
## 161210: New data batch, before change hadron eta
## 161212: WEH
## 161219: WEH mc-patched
## 170204 AUTO, unless overridden by below
SPEC_SSFIT = {
  # mainly muon need tf series
  # 'emu': 'emu_tf_40_false',
  'emu': 'emu_tf_120_false',
}

#===============================================================================
# EWK BACKGROUND
#===============================================================================

## Pick source to calculate factor r_{EWK}
## Specify which stage r should be taken from
## True : Use after isolation
## False: Use before isolation ( not recommended unless stats is too low after iso )
## None : Use r=1
SPEC_EWK = {
  'mumu': {
    'wmumujet': True,
    'dymu'    : None,
  },
  'h1mu': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
  'h3mu': {
    'wmu17jet': False,
    'dymu'    : False,
  },
  'ee': {
    'we'    : False,
    'dye10' : None,
  },
  'eh1': {
    'we'    : False,
    'dye10' : False,
  },
  'eh3': {
    'we'    : False,
    'dye10' : False,
  },
  'emu': {
    'we'   : False,
    'dye10': False,
  },
  'mue': {
    'wmu17jet': True,
    'dymu'    : True,
  },
}
# note: noiso --> more stats --> rEWK has less syst.

#===============================================================================
# BREAKING SAME-SIGN
#===============================================================================

def get_trees_ssfit(dt):
  """
  Prepare list of trees to be used in ssfit_engine
  """
  ## Prepare the deck
  queues = {
    'mumu': ['tss_real', 'tssdd_qcd', 'tss_wmumujet_noiso', 'tss_ztaujet_noiso'],
    
    'h1mu': ['tss_real', 'tssdd_qcd', 'tss_wmu17jet', 'tss_ztaujet_noiso'],
    # 'h1mu': ['tss_real', 'tssdd_qcd', 'tss_wmu17jet_noiso', 'tss_dymu_noiso', 'tss_ztaujet_noiso'],
    # 'h1mu': ['tss_real', 'tssdd_qcd', 'tss_wmu17jet_noiso', 'tss_zmu17jet_noiso', 'tss_ztaujet'],
    # 'h1mu': ['tss_real', 'tssdd_qcd', 'tss_wmu17jet', 'tss_dymu', 'tss_ztaujet'],

    'h3mu': ['tss_real', 'tssdd_qcd', 'tss_wmu17jet_noiso', 'tss_ztaujet_noiso'],
    'ee'  : ['tss_real', 'tssdd_qcd', 'tss_we_noiso', 'tss_ztaujet_noiso'],
    'eh1' : ['tss_real', 'tssdd_qcd', 'tss_we_noiso', 'tss_ztaujet'      ], # has high stat, no need for noiso
    'eh3' : ['tss_real', 'tssdd_qcd', 'tss_we_noiso', 'tss_ztaujet_noiso'],
    'emu' : ['tss_real', 'tssdd_qcd', 'tss_we_noiso', 'tss_wmu17jet', 'tss_ztaujet_noiso'],
  }[dt]

  ## rename titles
  titles4 = 'Data', 'QCD', 'V+Jet', 'Z#rightarrow#tau#tau'
  titles5 = 'Data', 'QCD', 'V_{e}+Jet', 'V_{#mu}+Jet', 'Z#rightarrow#tau#tau'
  titles  = titles5 if dt=='emu' else titles4

  ## Fetch trees into the engine
  trees0  = selected.load_selected_trees_dict(dt)
  trees   = []
  for tname, title in zip(queues, titles):
    t = trees0[tname]
    t.title = title
    trees.append(t)
  # return trees # all trees
  return trees #[:-1] # exclude Ztau

#-------------------------------------------------------------------------------

## disable me is useful to replot figure but don't disturbing the results
@pickle_dataframe_nondynamic 
def break_samesign(dt):
  ROOT.gROOT.SetBatch(True)
  ROOT.RooFit # trigger patch

  ## List of input trees
  trees = get_trees_ssfit(dt)

  ## helper to yield preprocessor
  def lim_ztau(frac):
    def preprocessor(fitter):
      # restrict Ztau (last item) to 10%
      logger.info('Apply constrain: %.3f'%frac)
      fitter.Constrain(len(trees)-2, 0.0, frac)
      return fitter
    return preprocessor

  # Put upper limit on Ztautau on all channel as a function of MC estimate
  fracs = samesign_utils.ztautau_frac_upperlim(PATH_CONTEXT)
  pp    = lim_ztau(fracs[dt] * 1.5)

  ## Start the engine, return collected result
  xlabel = 'p_{T}(#tau_{%s}) - p_{T}(#tau_{%s})'%LABELS[dt]
  return samesign_utils.ssfit_engine(dt, trees, xlabel=xlabel, preprocessor=pp)


def break_samesign_all():
  """
  Loop over all ttypes & mass, break samesign into QCD+EWK
  """
  return pd.concat({dt:break_samesign(dt) for dt in CHANNELS})

#===============================================================================
# PROCESSING SAME-SIGN
#===============================================================================

@memorized
def parse_spec_ssfit():
  """
  Return a dict of ssfit, with auto-pick if needed.
  """
  return samesign_utils.guess_ssfit_spec(PATH_SAMESIGN, SPEC_SSFIT)

#-------------------------------------------------------------------------------

@memorized
def result_ssfit_raw():
  """
  Raw result where it's only parsed against the spec.
  """
  path1 = PATH_SAMESIGN
  path2 = PATH_CONTEXT
  return samesign_utils.result_ssfit_raw(parse_spec_ssfit(), path1, path2)


#===============================================================================
# QCD
#===============================================================================

@memorized
def result_qcd_detailed():
  """
  Raw dataframe to have everything, ready to be printed or calculated.
  """
  ## Load the DD-QCD ratio
  df0 = df_from_path('check_sel_masswindow', PATH_CONTEXT) # with mass win
  # df0 = df_from_path('check_mc_nos_nss', PATH_CONTEXT) # no mass win
  df  = pd.DataFrame()
  for dt in CHANNELS:
    df[dt] = df0.loc[(dt,'qcd'), 'evt']
  df.index = ['OS', 'SS']
  dfQ = df.T

  ## Multiply it to SS-fit result
  dfSS  = result_ssfit_raw()[['QCD']].drop('mue')
  df    = pd.concat([ dfQ, dfSS ], axis=1).reindex(CHANNELS)
  df['rQCD']  = df.OS.apply(var)/df.SS.apply(var)
  df['final'] = df.QCD * df.rQCD
  return df


def result_qcd_nomasswin():
  return result_qcd_detailed()['final']


def result_qcd_withmasswin():
  fracs = selected.fraction_mass_window().loc['tdd_qcd']
  return result_qcd_nomasswin() * fracs


#===============================================================================
# EWK
#===============================================================================

@memorized
def load_rewk():
  """
  Wrapper to load the rEWK multiplication factor, from the given spec & context.
  This is nothing to do with the fitting result.
  """
  return samesign_utils.load_rewk(path_context=PATH_CONTEXT, spec=SPEC_EWK)


## NOTE: Preferably make cache on another function that will only be useful 
# for latex, but NOT useful for candidates.py computation, so that I am certain
# that the correlation is correct from freshly calculated value, not from cache.
@memorized
def result_ewk_prefinal():
  ssfit_raw   = result_ssfit_raw()
  rewk_parsed = load_rewk()
  return samesign_utils.result_ewk_prefinal(ssfit_raw, rewk_parsed)


@memorized
def compare_ewk_mc_ssfit():
  """
  Compare this result before proceeding any further
  """
  rewk = samesign_utils.load_rewk(path_context=PATH_CONTEXT) # before collapse
  df   = samesign_utils.compare_ewk_mc_ssfit(rewk, result_ssfit_raw())
  return df.loc[list(CHANNELS)+['mue']]


@memorized
def compare_ewk_mc_os():
  ss  = compare_ewk_mc_ssfit()
  res = result_ewk_prefinal()
  df  = samesign_utils.compare_ewk_mc_os(ss, res)
  return df.loc[list(CHANNELS)+['mue']]

#-------------------------------------------------------------------------------

@memorized
def result_ewk_nomasswin():
  se = result_ewk_prefinal()['final'].copy()
  se['emu'] = se['emu_raw']+se['mue_raw']
  del se['emu_raw']
  del se['mue_raw']
  return se[list(CHANNELS)]


@memorized
def frac_masswin_ewk():
  """
  This is tricky: The fraction is slightly different, so I need weighted average
  of those heterogeneous names.
  """
  fracs  = selected.fraction_mass_window()
  resraw = result_ewk_prefinal()
  acc    = pd.Series()
  for dt, spec in SPEC_EWK.iteritems():
    if dt == 'mumu': # fraction of Z is unusable, use only W
      acc[dt] = fracs.loc['t_wmumujet', 'mumu']
    elif dt == 'ee':
      acc[dt] = fracs.loc['t_dye10', 'ee']
    else:
      queues = [] # for weighed average
      rawkey  = dt+'_raw' if dt in ('emu','mue') else dt
      for vname in spec:
        vtype   = 'W' if vname.startswith('w') else 'Z'
        frackey = 'emu' if dt=='mue' else dt
        frac    = fracs.loc['t_'+vname, frackey]
        weight  = resraw.loc[rawkey, vtype+'os']
        queues.append([frac, weight])
      logger.debug('frac wavg: %s %r'%(dt, str(queues)))
      acc[rawkey] = weighted_average(queues).n
  return acc


@memorized
def result_ewk_withmasswin(collapse=True):
  se = result_ewk_prefinal()['final'] * frac_masswin_ewk()
  if collapse:
    se['emu'] = se['emu_raw']+se['mue_raw']
    del se['emu_raw']
    del se['mue_raw']
    return se[list(CHANNELS)]
  return se


#===============================================================================

@memorized
def compare_ztau_constraints():
  """
  Report results at different the Ztautau constraints.
  Need manually-collected cache
  """
  ## Load the results from all regimes
  cols = ['NSS', 'QCD', 'EWK', 'Ztau', 'NOS']
  queues = {
    'x0'  : '$DIR13/identification/plot/bkg_samesign/170424_ztaunmz*0',
    'x1'  : '$DIR13/identification/plot/bkg_samesign/170424_ztaunmz*1',
    'x1.5': '$DIR13/identification/plot/bkg_samesign/170424_ztaunmz*1_5',
    'x2'  : '$DIR13/identification/plot/bkg_samesign/170424_ztaunmz*2',
  }
  acc = {}
  for regime, path in queues.iteritems():
    ## Load constraint-dependent results
    spec = samesign_utils.guess_ssfit_spec(path)
    df   = samesign_utils.result_ssfit_raw(spec, path, PATH_CONTEXT)
    ## force zero in special case
    if regime == 'x0':
      df['Ztau'] = '-' 
    ## independent of constraints
    rQCD = result_qcd_detailed()['rQCD']
    rEWK = load_rewk().xs('V', level=1)['r']
    rEWK.name = 'rEWK'
    ## concat together
    df   = pd.concat([df, rQCD, rEWK], axis=1)
    df.loc['mue', 'rQCD'] = df.loc['emu', 'rQCD'] 
    ## calculate expected QCD+EWK OS
    df['NOS'] = df.QCD*df.rQCD + df.EWK*df.rEWK
    acc[regime] = df[cols]
  df = pd.concat(acc)

  ## Group by channel instead, pick canonical order
  df = df.reorder_levels([1,0]).sortlevel()
  df = df.unstack().loc[list(CHANNELS)+['mue']].stack()
  df.index.names = 'Channel', 'Constraint'
  return df

#===============================================================================
# UNBIASED
#===============================================================================

def _unbiased_normalization(dt):
  """
  Return list of normalization (area under curve = number of candidates)
  for given channel.
  """
  res = result_ssfit_raw()
  ## Need custom pick
  if dt == 'emu':
    return res.loc['emu','NSS'], res.loc['emu','QCD'], res.loc['emu','EWK'], res.loc['mue','EWK'], res.loc['emu','Ztau']
  else:
    cols = ['NSS', 'QCD', 'EWK', 'Ztau']
    return list(res.loc[dt][cols])

def draw_ssfit_unbiased(dt):
  ## Fetch channel-dependent data
  dat    = result_ssfit_raw().loc[dt]
  trees  = get_trees_ssfit(dt)
  nmz    = _unbiased_normalization(dt)
  xbin   = dat['xbin']
  chi2df = dat['chi2pdof']
  xlab12 = LABELS_DITAU_SEP2[dt]

  ## Color = _, QCD, EWK, Ztau. 
  # Make sure of the consistent in emu channel
  colors = 0, 28, 6, ROOT.kRed
  if dt=='emu':
    colors = 0, 28, 6, ROOT.kBlue, ROOT.kRed

  ## Ready, fire
  samesign_utils.draw_ssfit_unbiased(dt, trees, nmz, xbin, chi2df, colors, *xlab12)

def draw_ssfit_unbiased_all():
  for dt in CHANNELS:
    draw_ssfit_unbiased(dt)

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    break_samesign_all()
    sys.exit()

  ## Apply LHCb style
  ROOT.gROOT.batch = True
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## same-sign = QCD+EWK
  # print samesign_utils.ztautau_frac_upperlim(PATH_CONTEXT)
  # print break_samesign('ee').sort_values('chi2pdof')

  ## Choose spec
  # samesign_utils.print_ssfit_stats(PATH_SAMESIGN, only_dt='emu') # , only_dt='emu', show_all=True) #, 
  # print parse_spec_ssfit()
  # print compare_ewk_mc_ssfit().fmt2f # SS-level is direct, OS-level use cache `result_ewk`
  # print compare_ewk_mc_os().fmt0f

  ## Results
  # print result_ssfit_raw().fmt2f
  # print result_qcd_detailed().fmt3f
  # print result_qcd_nomasswin().fmt2f
  # print result_qcd_withmasswin().fmt2f
  # print result_ewk_prefinal().fmt2f
  # print result_ewk_nomasswin().fmt2f
  # print frac_masswin_ewk()
  # print result_ewk_withmasswin().fmt2f

  ## AUX
  # print load_rewk()
  # print compare_ztau_constraints()
  draw_ssfit_unbiased('mumu')
  # draw_ssfit_unbiased_all()
