#!/usr/bin/env python

"""

IDEA: Assert that syst of rEWK is not too high.
IDEA: Assert that for ztautau signal MC there's just one cand per event.
"""

import selected
from id_utils import pd, CHANNELS, df_from_path
PATH_CONTEXT = '$DIR13/identification/plot/bkg_context'

#===============================================================================

def assert_same_data_os_ss():
  """
  This number can come from 2 methods
  1. bkg_context.check_data_nos_nss
  2. selected.main_export

  Their numbers should be equal
  """
  df1 = df_from_path('check_data_nos_nss', PATH_CONTEXT).xs('ent', level=1)

  acc = {}
  for dt in CHANNELS:
    trees = selected.load_selected_trees_dict(dt)
    acc[dt] = pd.Series({'OS':trees['t_real'].entries, 'SS':trees['tss_real'].entries})
  df2 = pd.concat(acc).unstack()
  assert df1.equals(df2), "Contents from data not equal."

#===============================================================================

def assert_no_mult_cand():
  """
  Assert that there is no multiple canditates per event from data
  """
  df = df_from_path('check_data_nos_nss', PATH_CONTEXT)
  assert df.xs('ent', level=1).equals(df.xs('evt', level=1)), 'Found multiple cand in one event.'

#===============================================================================

def assert_comb_plot_stat():
  """
  Assert that the histogram for comb plot has enough statistics to show a good
  stack plot.
  """
  for dt in CHANNELS:
    for tree in selected.load_selected_trees_comb(dt):
      assert tree.entries > 20, 'Tree too small: [%s] %s = %i'%(dt, tree.name, tree.entries)

#===============================================================================
  
if __name__ == '__main__':
  pass 

  assert_same_data_os_ss()
  assert_no_mult_cand()
  # assert_comb_plot_stat()
