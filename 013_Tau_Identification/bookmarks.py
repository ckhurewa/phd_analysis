
"""

List of uri used for 8TeV Z02TauTau analysis.

This is primarily used by `ganga.py`

"""

#-------------#
# SIGNAL 8TeV #
#-------------#

### H2AA (local)
# glob('/panfs/khurewat/MC/H2AA_H125_A30/8TeV_nu2.5_md100/Stripping21/*/*.dst'), # 8TeV
# glob('/panfs/khurewat/MC/H2AA_H125_A30/13TeV_Nu1.6_25ns_100k/2361/*.dst'), # 13TeV

### Z02TauTau (S20, ~4Mevt, ~200files)
# NOTE: No GAMMA STAR INTEFERENCE
ID_42100000 = [
  'evt+std://MC/2012/42100000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42100000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

### Z02TauTau 8TeV (S21, restripped locally from S20 above)
# uri = 'evt://MC/2012/42100000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST'
# glob('/panfs/khurewat/ganga_storage/2811/*/*.dst'),  # S21 Z0 MagDown
# glob('/panfs/khurewat/ganga_storage/2812/*/*.dst'),  # S21 Z0 MagUp

## Custom Zg tautau (1Mevt)
Zg_tautau = [
  [ '$EOS_HOME/ganga/4083/%i/000000.AllStreams.dst'%i for i in xrange(100) ],
  [ '$EOS_HOME/ganga/4273/%i/000000.AllStreams.dst'%i for i in xrange(75)  ],
  [ '$EOS_HOME/ganga/4279/%i/000000.AllStreams.dst'%i for i in xrange(75)  ],
]

#------------------------------------#
# DrellYann (production summer 2015) #
#------------------------------------#

## 42102011 DrellYan_tautau=10GeV ( 2.5Mevt, 108 files )
# ID_42102011 = [
#   'evt+std://MC/2012/42102011/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
#   'evt+std://MC/2012/42102011/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# ]

## 42112011 DrellYan_mumu=10GeV ( 20+ Mevt, ~866 files )
ID_42112011 = [
  'evt+std://MC/2012/42112011/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42112011/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42122011 DrellYan_ee=10GeV ( 2.2Mevt, 104 files )
ID_42122011 = [
  'evt+std://MC/2012/42122011/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42122011/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42122001 Zg_ee40GeV ( 2Mevt, 128 files ) 1072472 + 1053800, 63 + 65
ID_42122001 = [
  'evt+std://MC/2012/42122001/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42122001/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  
  ## THESE ARE SOMEWHAT FAULTY... better email 
  # 'evt+std://MC/2012/42122001/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  # 'evt+std://MC/2012/42122001/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42112001 Zg_mumu40GeV ( 4Mevt, 200 files)
ID_42112001 = [
  'evt+std://MC/2012/42112001/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST', 
  'evt+std://MC/2012/42112001/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST', 
]

#---------#
# W + Jet #
#---------#

## 42300010: W-tau + Jet (New! production summer 2015) ( 2.5Mevt, 100 files )
ID_42300010 = [
  'evt+std://MC/2012/42300010/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42300010/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42311011: W_munujet=l17 (~10Mevt, 400 files)
ID_42311011 = [
  'evt+std://MC/2012/42311011/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42311011/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]
  
## 42311010: W_munujet ( 2Mevt, 91 files )
ID_42311010 = [
  'evt+std://MC/2012/42311010/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42311010/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# 42321010: W_enujet ( 2Mevt, 96 files )
ID_42321010 = [
  'evt+std://MC/2012/42321010/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42321010/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42901000: W_taunubbtau=lep ( ~3.5 Mevt , ~170 files)
ID_42901000 = [
  'evt+std://MC/2012/42901000/Beam4000GeV-2012-MagDown-Nu2.5-AlpGen/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42901000/Beam4000GeV-2012-MagUp-Nu2.5-AlpGen/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## Custom Wmu20+mu5 (70k)
Wmumujet = [
  [ '$EOS_HOME/ganga/3949/%i/SelZ02TauTau.Strip.dst'%i for i in xrange(50) ],
  [ '$EOS_HOME/ganga/3973/%i/SelZ02TauTau.Strip.dst'%i for i in xrange(20) ],
]

## Custom We20+jet (20k), stripped Z02TauTau
We20jet = [
  [ '$EOS_HOME/ganga/4431/%i/000000.AllStreams.dst'%i for i in xrange(100)],
  [ '$EOS_HOME/ganga/4447/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4481/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4489/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4495/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4500/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4504/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4518/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4524/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4528/%i/000000.AllStreams.dst'%i for i in xrange(20) ],
  [ '$EOS_HOME/ganga/4532/%i/000000.AllStreams.dst'%i for i in xrange(10) ],
]


#-------------------------#
# Ilten's hard-parton BKG #
#-------------------------#

# 49001000 minbias=HardQCD,pt18GeV ( 40 files )
ID_49001000 = [
  'evt+std://MC/2012/49001000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/49001000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# 49011004 ccbar=HardQCD,pt18GeV,mu ( 51 files, 1Mevt )
ID_49011004 = [
  'evt+std://MC/2012/49011004/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/49011004/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# 49011005 bbbar=HardQCD,pt18GeV,mu ( 45 files )
ID_49011005 = [
  'evt+std://MC/2012/49011005/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/49011005/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# 49021004 ccbar=HardQCD,pt18GeV,e ( 57 files )
ID_49021004 = [
  'evt+std://MC/2012/49021004/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/49021004/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# 49021005 bbbar=HardQCD,pt18GeV,e ( 46 files )
ID_49021005 = [
  'evt+std://MC/2012/49021005/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/49021005/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# #----------------------------------------------#
# # Old W-tau, may have tau-polarization problem #
# #----------------------------------------------#

# ## 42300000: W_taunutau (30kevt, too low to use)
# # evt+std://MC/2012/42300000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST

# ## 42300001: W_taunutau=Mu (~2Mevt)
# # 'evt+std://MC/2012/42300001/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/42300001/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',

# ## 42300002: W_taunutau=e (~1Mevt)
# ## INCOMPLETE!!
# # 'evt+std://MC/2012/42300002/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/42300002/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',


#---------#
# W->mu/e #
#---------#

# ## 42311002: W_munumu=PHOTOS (~5Mevt) 8TeV, without S21 (for on-the-fly)
# # 'sim+std://MC/2012/Beam4000GeV-JulSep2012-MagDown-Nu2.5-EmNoCuts/Sim06b/Trig0x40990042Flagged/Reco14/Stripping20NoPrescalingFlagged/42311002/ALLSTREAMS.DST',
# # 'sim+std://MC/2012/Beam4000GeV-JulSep2012-MagUp-Nu2.5-EmNoCuts/Sim06b/Trig0x40990042Flagged/Reco14/Stripping20NoPrescalingFlagged/42311002/ALLSTREAMS.DST',

# ## 42311003: W_munumu=10GeV (~5Mevt)
# ## INCOMPLETE!!
# # 'evt+std://MC/2012/42311003/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/42311003/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',

## 42321000: W_enue (~5Mevt, 270files)
ID_42321000 = [
  'evt+std://MC/2012/42321000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42321000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# ## 42321001: W_enue=PHOTOS
# ## Trigger too old to use.


#---------#
# Z + Jet #
#---------#

## 42100020: Z_tautaujet ( 70x2 files, 1.5x2 Mevt)
ID_42100020 = [
  'evt+std://MC/2012/42100020/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42100020/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42112022: Z_mumujet=l17 ( 128x2 files, 2.5x2 Mevt )
ID_42112022 = [
  'evt+std://MC/2012/42112022/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08b/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42112022/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08b/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# ### 42112050: Zbjet=mumu,InAcc ( 11x2 files, 100x2 kevt )
# ### 'evt+std://MC/2012/42112050/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# ### 'evt+std://MC/2012/42112050/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',

# ### 42112051: Zcjet=mumu,InAcc ( 10x2 files, 100x2 kevt )
# ### 'evt+std://MC/2012/42112051/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# ### 'evt+std://MC/2012/42112051/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',

## 42112052: Zcjet=mu17,InAcc ( 57x2 files, 1x2 Mevt )
ID_42112052 = [
  'evt+std://MC/2012/42112052/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42112052/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42112053: Zbjet=mu17,InAcc ( 60x2 files, 1x2 Mevt )
ID_42112053 = [
  'evt+std://MC/2012/42112053/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42112053/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

#-----------------#
# Z -> bb (gamma) #
#-----------------#

## 42150000: Z_bb,2binAcc ( 541 files, 10Mevt ). PYTHIA6!
ID_42150000 = [
  'evt+std://MC/2012/42150000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42150000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

# ## 42100200: Zgamma_bb=PHOTOS ( ~30 files, 500 kevt )
# ## HAS ACCOMPANIED GAMMA
# # 'evt+std://MC/2012/42100200/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/42100200/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',

#-------#
# ttbar #
#-------#

## 41900006: ttbar_gg_1l17GeV ( 68x2 files, 1Mevt x 2 )
ID_41900006 = [
  'evt+std://MC/2012/41900006/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/41900006/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 41900007: ttbar_qqbar_1l17GeV ( 67x2 files, 1Mevt x 2 )
ID_41900007 = [
  'evt+std://MC/2012/41900007/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/41900007/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 41900010: tt_bb=1l,10GeV,2b,powheg ( 120x2 files, 4Mevt )
ID_41900010 = [
  'evt+std://MC/2012/41900010/Beam4000GeV-2012-MagDown-Nu2.5-Powheg/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/41900010/Beam4000GeV-2012-MagUp-Nu2.5-Powheg/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

#---------#
# WW , WZ #
#---------#
  
## 41922002: WW_lnul,lnul=1l15GeV ( 9x2 files, 210 kevt)
ID_41922002 = [
  'evt+std://MC/2012/41922002/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/41922002/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42021000: WW_lnul,X=1l15GeV ( 10x2 files, 222 kevt)
ID_42021000 = [
  'evt+std://MC/2012/42021000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42021000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

## 42021001: WZ_l,X=1l15GeV ( 10x2 files, 229 kevt)
ID_42021001 = [
  'evt+std://MC/2012/42021001/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
  'evt+std://MC/2012/42021001/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
]

#----------------#
# Custom Minbias #
#----------------#

minbias_mux = [ 
  ['$EOS_HOME/ganga/4151/%i/000000.AllStreams.dst'%i for i in xrange(10) ],
  ['$EOS_HOME/ganga/4289/%i/000000.AllStreams.dst'%i for i in xrange(110)],
]

#-------------------------#
# Custom invarmass di-tau #
#-------------------------#

# ### 13TeV Ditau ( various mass, no trigger/stripping )
# # 'evt+std://MC/Dev/49000150/Beam6500GeV-RunII-MagDown-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 5-10
# # 'evt+std://MC/Dev/49000151/Beam6500GeV-RunII-MagDown-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 10-20
# # 'evt+std://MC/Dev/49000152/Beam6500GeV-RunII-MagDown-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 20-50
# # 'evt+std://MC/Dev/49000153/Beam6500GeV-RunII-MagDown-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 50-90
# # 'evt+std://MC/Dev/49000154/Beam6500GeV-RunII-MagDown-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 90++
# # #
# # 'evt+std://MC/Dev/49000150/Beam6500GeV-RunII-MagUp-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 5-10
# # 'evt+std://MC/Dev/49000151/Beam6500GeV-RunII-MagUp-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 10-20
# # 'evt+std://MC/Dev/49000152/Beam6500GeV-RunII-MagUp-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 20-50
# # 'evt+std://MC/Dev/49000153/Beam6500GeV-RunII-MagUp-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 50-90
# # 'evt+std://MC/Dev/49000154/Beam6500GeV-RunII-MagUp-Nu1.6-25ns-Pythia8/Sim08f/Reco15DEV/LDST', # 90++

# #--------#
# # Others #
# #--------#

# ## 30000000: Minimum bias (~40Mevt, ~1300 files)
# # 'evt+std://MC/2012/30000000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/30000000/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/30000000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',
# # 'evt+std://MC/2012/30000000/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/ALLSTREAMS.DST',


#===============================================================================
# DATA
#===============================================================================

### Data 2012 S20 ( 6k + 7k ) = 12982 files
# DATA_S20_EW = [
#   '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/90000000/EW.DST',
#   '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20/90000000/EW.DST',
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20/EW.DST',
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/EW.DST',
# ]
## downloaded list
DATA_S20_EW = [
  '/share/lphe/home/khurewat/analysis/013_Tau_Identification/LHCb_Collision12_Beam4000GeVVeloClosedMagDown_Real Data_Reco14_Stripping20_90000000_EW.DST.py',
  '/share/lphe/home/khurewat/analysis/013_Tau_Identification/LHCb_Collision12_Beam4000GeVVeloClosedMagUp_Real Data_Reco14_Stripping20_90000000_EW.DST.py',
]

### Data S20.MINIBIAS.DST ( 3615 + 2562 )
DATA_S20_MINIBIAS = [
  'sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/90000000/MINIBIAS.DST',
  'sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20/90000000/MINIBIAS.DST',
]

DATA_S20_CALIB = [
  'sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/90000000/CALIBRATION.DST',
  'sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20/90000000/CALIBRATION.DST',
]

# down: 3595 files, 483450312 events
# up  : 
DATA_S20_PID = [
  'sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/90000000/PID.MDST',
  'sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20/90000000/PID.MDST',
]

#-----#
# S21 #
#-----#

### Data 2012 S21 ( 6k + 6k ) = 12995 files
# DATA_S21 = [
#   'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/EW.DST',
#   'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/EW.DST',
# ]

# ## + 4.5k + 4k
# DATA_S21r1 = [
#   'evt+std://LHCb/Collision11/90000000/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/EW.DST',
#   'evt+std://LHCb/Collision11/90000000/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/EW.DST',
# ]

#   ### Data S21.Calibration ( 4449 + 5066 )
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/CALIBRATION.DST',
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/CALIBRATION.DST',

#   ### Data S21.PID.MDST ( 3484 + 4184 )
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/PID.MDST',
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/PID.MDST',

#   ### Data S21.MINIBIAS.DST ( 3409 + 4102 )
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/MINIBIAS.DST',
#   # 'evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/MINIBIAS.DST',


# #---------#
# # S23 DEV #
# #---------#

# ## Run125113 (test provided from Stripping Coordinators) For S23 DEV
# # '/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v9r6/Phys/StrippingSelections/tests/data/pool_xml_catalog_Reco14_Run125113.xml',

# #-------#
# # TRASH #
# #-------#

# ### Data - probably not usable 
# # 'evt+std://LHCb/Collision10/90000000/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco08/Stripping14/EW.DST'
# # 'evt+std://LHCb/Collision10/90000000/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco08/Stripping14/EW.DST'
# # 'evt+std://LHCb/Collision12_25/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/EW.DST'
# # 'evt+std://LHCb/Collision12hl/90000000/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco13g/FULL.DST'

# ## Cleanup vars so that it's not pickedup by vars(bookmarks)
# del i

