#!/usr/bin/env python

"""
Based on 42102011, but now with harsh fiducial cut to save space
"""

from Gaudi.Configuration import *
# importOptions( "$DECFILESROOT/options/DrellYantautau10GeV.py" )

# Pythia options for Z/gamma->tautau 42102011
from Configurables import Generation
from Gaudi.Configuration import *
Generation().PileUpTool = "FixedLuminosityForRareProcess"
importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )
from Configurables import Special, Pythia8Production
Generation().addTool( Special )
Generation().Special.addTool( Pythia8Production )
Generation().Special.Pythia8Production.Commands += [
  "WeakSingleBoson:ffbar2gmZ = on", # Z0/gamma* production
  "PhaseSpace:mHatMin = 60.",       # constrain inv mass
  "PhaseSpace:mHatMax = 120.",
  "23:mMin = 60.",                  # min mass of Z0 in GeV
  "23:mMax = 120.",
  "23:onMode = off",                # turn it off
  "23:onIfMatch = 15 -15",          # turn it on for the decay to tau final state only
  #
  # 160104: To quickly generate MuX channel
  # "15:onMode = off",
  #"15:onIfAny = 13"
]

Generation().Special.CutTool = "PythiaHiggsType"
from Configurables import PythiaHiggsType
Generation().Special.addTool( PythiaHiggsType )
Generation().Special.PythiaHiggsType.TypeOfLepton = [ "tau+" ]
# Allow Pythia to handle tau decays (not EvtGen through TAUOLA).
Generation().Special.DecayTool = ""

#--------


from Configurables import Generation
Generation().EventType = 42102013
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "PythiaProduction"
# from Configurables import ToolSvc
# from Configurables import EvtGenDecay
# ToolSvc().addTool( EvtGenDecay )
# ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/DrellYan_tautau=10GeV.dec"
Generation().Special.CutTool = "PythiaHiggsType"
from Configurables import PythiaHiggsType
Generation().Special.addTool( PythiaHiggsType )
Generation().Special.PythiaHiggsType.NumberOfLepton = 2
Generation().Special.PythiaHiggsType.LeptonIsFromMother     = True
Generation().Special.PythiaHiggsType.MotherOfLeptonMinMass  = 60
Generation().Special.PythiaHiggsType.NumberOfbquarks = -1
from GaudiKernel import SystemOfUnits
Generation().Special.PythiaHiggsType.LeptonPtMin = 18.0*SystemOfUnits.GeV


## TODO
# - Properly separate 10 channels
# - Use SCALE(0.2) instead? --> not possible
# - It's a shame: LoKi can't do lambda here

preambulo_baseditau = """
# use descendants to allow tau recoiling. Use with care.
def WithOne(cut):
  from LoKiGen.decorators import GNINTREE, HepMC
  return (GNINTREE(cut, HepMC.descendants) >= 1)
def WithTwo(cut):
  from LoKiGen.decorators import GNINTREE, HepMC
  return (GNINTREE(cut, HepMC.descendants) >= 2)
#
# Channel counter: Forget about kinematic. Look on topology only.
IsTau       = (GABSID == 15)
IsMuon      = (GABSID == 13)
IsElec      = (GABSID == 11)
IsHad       = (GHADRON & GCHARGED)
IsTauMu     = IsTau & (GNINTREE(IsMuon)==1)
IsTauE      = IsTau & (GNINTREE(IsElec)==1)
IsTauH1     = IsTau & (GNINTREE(IsHad)==1)
IsTauH3     = IsTau & (GNINTREE(IsHad)==3)
IsDitau     = (GNINTREE(IsTau)==2) # don't search in descendants unlike other.
IsDitauEE   = WithTwo(IsTauE)
IsDitauEH1  = WithOne(IsTauE) & WithOne(IsTauH1)
IsDitauEH3  = WithOne(IsTauE) & WithOne(IsTauH3)
IsDitauEMU  = WithOne(IsTauE) & WithOne(IsTauMu)
IsDitauH1H1 = WithTwo(IsTauH1)
IsDitauH1H3 = WithOne(IsTauH1) & WithOne(IsTauH3)
IsDitauH1MU = WithOne(IsTauH1) & WithOne(IsTauMu)
IsDitauH3H3 = WithTwo(IsTauH3)
IsDitauH3MU = WithOne(IsTauH3) & WithOne(IsTauMu)
IsDitauMUMU = WithTwo(IsTauMu)
moni = (
  select(IsDitau)
  >> process(monitor(IsDitauEE   , 'ee'    , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauEH1  , 'eh1'   , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauEH3  , 'eh3'   , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauEMU  , 'emu'   , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauH1H1 , 'h1h1'  , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauH1H3 , 'h1h3'  , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauH1MU , 'h1mu'  , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauH3H3 , 'h3h3'  , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauH3MU , 'h3mu'  , LoKi.Monitoring.ContextSvc)) 
  >> process(monitor(IsDitauMUMU , 'mumu'  , LoKi.Monitoring.ContextSvc)) 
  >> (GSIZE>-1)
)
""".split('\n')

## More cut on tau product
from Configurables import LoKi__FullGenEventCut
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/DitauCondensed"
Generation().addTool( LoKi__FullGenEventCut, name='DitauCondensed' )
cut = Generation().DitauCondensed
cut.Code = "(moni) & (count(GoodDitau) >= 1)"
cut.Preambulo += preambulo_baseditau + """
from GaudiKernel.SystemOfUnits import GeV, mrad
VRAND         = (GONE>>XRAND)
InWideAcpt    = (GTHETA < 360.0*mrad)  # 1.7
GoodMuon      = IsMuon & InWideAcpt
GoodElec      = IsElec & InWideAcpt
GoodHadron    = IsHad  & InWideAcpt
GoodTauMu_3   = IsTau & (GNINTREE( GoodMuon & (GPT> 3*GeV) )==1)
GoodTauMu_13  = IsTau & (GNINTREE( GoodMuon & (GPT>13*GeV) )==1)
GoodTauMu_18  = IsTau & (GNINTREE( GoodMuon & (GPT>18*GeV) )==1)
GoodTauE_3    = IsTau & (GNINTREE( GoodElec & (GPT> 3*GeV) )==1)
GoodTauE_13   = IsTau & (GNINTREE( GoodElec & (GPT>13*GeV) )==1)
GoodTauE_18   = IsTau & (GNINTREE( GoodElec & (GPT>18*GeV) )==1)
GoodTauH1     = IsTau & (GNINTREE(GoodHadron)==1) & (GNINTREE(GoodHadron&(GPT>9*GeV))==1)
GoodTauH3     = IsTau & (GNINTREE(GoodHadron)==3) & (GNINTREE(GoodHadron&(GPT>5*GeV))>=1) & (GNINTREE(GoodHadron&(GPT>0.5*GeV))==3)
GoodDitauEE   = WithOne(GoodTauE_18)  & WithTwo(GoodTauE_3)     # 20,5  GeV
GoodDitauEH1  = WithOne(GoodTauE_13)  & WithOne(GoodTauH1)      # 15,10 GeV
GoodDitauEH3  = WithOne(GoodTauE_13)  & WithOne(GoodTauH3)      # 15,(1,,8) GeV
GoodDitauEMU  = (WithOne(GoodTauE_18) & WithOne(GoodTauMu_3)) | (WithOne(GoodTauE_3) & WithOne(GoodTauMu_18))  # 20,5
GoodDitauH1MU = WithOne(GoodTauMu_13) & WithOne(GoodTauH1)      # 15,10
GoodDitauH3MU = WithOne(GoodTauMu_13) & WithOne(GoodTauH3)      # 15,(1,,8) GeV
GoodDitauMUMU = WithOne(GoodTauMu_18) & WithTwo(GoodTauMu_3)    # 20,5
GoodDitauType = (GoodDitauEE)|(GoodDitauEH1)|(GoodDitauEH3)|(GoodDitauEMU)|(GoodDitauH1MU)|(GoodDitauH3MU)|(GoodDitauMUMU)
GoodDitau     = (IsDitau) & (GoodDitauType) & (GM>60*GeV) & (GM<120*GeV)
""".split('\n')


## Force P8 Pileup
## From https://svnweb.cern.ch/trac/lhcb/browser/Gauss/trunk/Gen/LbAlpGen/options/AlpGen.py
from Configurables import Generation, Pythia8Production
Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
Generation().PileUpTool = "FixedLuminosityForRareProcess"
Generation().Special.addTool( Pythia8Production, name = "Pythia8PileUp" )
Generation().Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
Generation().Special.ReinitializePileUpGenerator  = False

## Post-filter monitoring
from Configurables import GaudiSequencer
from Configurables import LoKi__VoidFilter
algo = LoKi__VoidFilter('ditau-moni')
algo.Code      = 'GSOURCE() >> moni'
algo.Preambulo = preambulo_baseditau
GaudiSequencer("GenMonitor").Members += [algo]
