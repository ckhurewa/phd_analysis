
## from my analysis/902_fullsim_resources
import Sim08h

#===============================================================================

regex = r'.*(Z02TauTau_|WMu|We).*'

app = DaVinci(version='v36r1p3', platform='x86_64-slc6-gcc48-opt')  # For S21
app.optsfile = [
  Sim08h.env_injector( TMP_STRIPPING_REGEX=regex ),
  '/home/khurewat/analysis/902_fullsim_resources/DV_stripping_regex.py',
  '$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py',
  '$APPCONFIGOPTS/DaVinci/DV-Stripping21-Stripping-MC-NoPrescaling.py',
  '$APPCONFIGOPTS/DaVinci/DataType-2012.py',
  '$APPCONFIGOPTS/DaVinci/InputType-DST.py',
  '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
  #
]
app.extraopts = """
from Configurables import LHCbApp
LHCbApp().DDDBtag    = 'dddb-20130929-1'
LHCbApp().CondDBtag  = 'sim-20141210-1-vc-md100'
"""

#===============================================================================

## pull lfns from grid into Boole
# jids    = (4145,4257,4260)
# lfns    = ( x for jid in jids for x in jobs(jid).lfn_list() if x.endswith('.sim') )
# ds      = LHCbDataset.new(lfns)

## note
# boole  : 98GB
# moore  : 68GB
# brunel : 84GB
# davinci: 80GB

# s = 'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/4082/%i/Brunel.dst'
# s  = 'PFN:/panfs/khurewat/ganga_storage/4277/%i/output/Brunel.dst'
# ds = LHCbDataset([ s%i for i in xrange(75) ])

ds = LHCbDataset.new('$EOS_HOME/ganga/4083/0/000000.AllStreams.dst')

#-------------------------------------------------------------------------------

j = Job()
j.name        = 'Zg_tautau'
# j.comment     = '42102013: Gauss+Dirac (200evt x 500sj)+900k'
# j.comment     = '42102013 Zgtautau (4kevt/j)[700k:1M]'
j.comment     = 'S21 RedoCalo'
# j.application = Sim08h.Gauss( '42102013.py', 200 )
# j.application = Sim08h.Brunel()
# j.application = Sim08h.DaVinci( r'.*(Z02TauTau_|WMu).*' )
j.application = app
# j.backend     = Dirac()
j.backend     = PBS(extraopts='--mem=3600 -t 12:0:0 --exclude=lphe01,lphe02,lphe03,lphe04,lphe05')
j.inputdata   = ds
# j.splitter    = GaussSplitter( eventsPerJob=200, numberOfJobs=500, firstEventNumber=900000 )
# j.splitter    = SplitByFiles( filesPerJob=1 )
j.outputfiles = [ LocalFile('*.dst'), '*.xml' ] # DiracFile

j.submit()
# queues.add( j.submit )

#==============================================================================
