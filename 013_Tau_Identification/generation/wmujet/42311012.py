# file /nightlies/jenkins/workspace/nightly-slot-checkout/tmp/checkout/DBASE/Gen/DecFiles/v27r59/options/42311011.py generated: Tue, 15 Dec 2015 12:06:00
#
# Event Type: 42311012
#
# ASCII decay Descriptor: pp -> [W+ -> mu+ nu_mu]cc + [jet -> mu+- ...] ...
#
from Gaudi.Configuration import *
importOptions( "$DECFILESROOT/options/Wmunujet.py" )
from Configurables import Generation
Generation().EventType = 42311012
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "PythiaProduction"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
#ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/W_munujet=l17.dec"

##
from Configurables import PythiaHiggsType
Generation().Special.CutTool = "PythiaHiggsType"
Generation().Special.addTool( PythiaHiggsType )
Generation().Special.PythiaHiggsType.NumberOfLepton = 1
from GaudiKernel import SystemOfUnits
Generation().Special.PythiaHiggsType.LeptonPtMin = 20*SystemOfUnits.GeV
Generation().Special.PythiaHiggsType.LeptonIsFromMother = True
Generation().Special.PythiaHiggsType.NumberOfbquarks = -1


## Cut for another muon, not from W
from Configurables import LoKi__FullGenEventCut
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/LoKi__FullGenEventCut"
Generation().addTool( LoKi__FullGenEventCut )
cut = Generation().LoKi__FullGenEventCut
cut.Code = "(count(OtherMu)>=1) & (dphi_mumu>2.6)"
cut.Preambulo += [
  "from GaudiKernel.SystemOfUnits import GeV, mrad",
  "from GaudiKernel.PhysicalConstants import pi",
  'IsW          = (GABSID==24)',
  'IsMuon       = (GABSID==13)',
  'InWideAcpt   = (GTHETA < 480.0*mrad )',
  'CutPT        = (GPT > 5*GeV)',
  'NotFromW     = (~GHAS( IsW, HepMC.ancestors ))',
  #
  'OtherMu    = IsMuon & InWideAcpt & CutPT & NotFromW',
  #
  'muons      = (select(IsMuon & InWideAcpt & CutPT))',
  'dphiraw    = ((muons >> max_value(GPHI)) - (muons >> min_value(GPHI)))',
  'dphi_mumu  = (pi - abs(dphiraw-pi))',
]


## Just Curious about P8PileUp
## From https://svnweb.cern.ch/trac/lhcb/browser/Gauss/trunk/Gen/LbAlpGen/options/AlpGen.py
from Configurables import Generation, Pythia8Production
Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
Generation().PileUpTool = "FixedLuminosityForRareProcess"
Generation().Special.addTool( Pythia8Production, name = "Pythia8PileUp" )
Generation().Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
Generation().Special.ReinitializePileUpGenerator  = False
