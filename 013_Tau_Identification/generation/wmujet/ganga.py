
# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LHCbGanga61
import re
from glob import glob

import sys
sys.path.append('/home/khurewat/analysis/902_fullsim_resources')
from Sim08h import boole, moore, brunel, extraopts_dddb

## NOTE: Need GangaCK library to allow smarter Application.optsfile behavior

#===============================================================================

def davinci():
  from Ganga import GPI
  app = GPI.DaVinci(version='v32r2p1', platform='x86_64-slc5-gcc46-opt')
  app.optsfile = [
    # '$APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py',
    '../shared/FlaggedS20Z02TauTau.py',
    # 'FilteredStripping20Z02TauTau.py',  # 
    # 'FilteredStripping24Ditau.py',
    #
    '$APPCONFIGOPTS/DaVinci/DataType-2012.py',
    '$APPCONFIGOPTS/DaVinci/InputType-DST.py',
    '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
  ]
  app.extraopts = extraopts_dddb
  return app

#===============================================================================

s='PFN:root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/3972/%i/Brunel.dst'
ds = LHCbDataset([ s%i for i in xrange(200) ])

#-------------------------------------------------------------------------------

j = Job()
j.name        = 'Wmu+mujet 151228'
# j.comment     = 'Gauss+Dirac (50evt x 200)+20k'
j.comment     = '20kevt = [0k:10k]+[60k:70k] (1kevt/j)'
j.application = davinci()
# j.backend     = Dirac()
j.backend     = PBS(extraopts='--mem=2400 -t 4:0:0')  # Kill it before it consumes space
j.inputdata   = ds
# j.splitter    = GaussSplitter( eventsPerJob=50, numberOfJobs=200, firstEventNumber=20000 )
j.splitter    = SplitByFiles( filesPerJob=10 ) # Non-Gauss
j.outputfiles = [ MassStorageFile('*.dst'), '*.xml' ]

j.submit()

#==============================================================================
