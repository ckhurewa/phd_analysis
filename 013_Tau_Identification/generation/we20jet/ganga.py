
from glob import glob

## from my analysis/902_fullsim_resources
import Sim08h

## Mem
# Boole  : ~1500
# Moore  : ~2100
# Brunel : ~1700
# DaVinci: ~5800

#===============================================================================

## Boole
# jids  = 4509,4510
# ds    = LHCbDataset.new(x for jid in jids for x in jobs(jid).lfn_list() if x.endswith('sim'))

# LHCbDataset.new(x for x in jobs(4510).lfn_list() if x.endswith('sim'))

root = '/home/khurewat/gangadir/workspace/khurewat/LocalXML'
# root = '/panfs/khurewat/ganga_storage'
ds   = LHCbDataset.new(glob(root+'/4531/*/output/*.dst'))

#-------------------------------------------------------------------------------

app = Sim08h.DaVinci( r'Stripping(Z02TauTau_).*' )
app.extraopts = """
## Patch the Stripping level to discard useless events
from Configurables import GaudiSequencer
GaudiSequencer('StrippingSequenceStreamAllStreams').IgnoreFilterPassed = False
"""

#-------------------------------------------------------------------------------

j = Job()
j.name        = 'we20jet'
# j.comment     = '42321011: Gauss+Dirac (100evt x 500sj)+1.95M'
j.comment     = '42321011 [1.9M:2.0M](10kevt/j) Filtered'
# j.application = Sim08h.Gauss( '42321011.py', 100 )
# j.application = Sim08h.Brunel()
j.application = app
# j.backend     = Dirac()
j.backend     = PBS(extraopts='--mem=5800 -t 1-0:0:0 --exclude=lphe01,lphe02')
j.inputdata   = ds
# j.splitter    = GaussSplitter( eventsPerJob=100, numberOfJobs=500, firstEventNumber=1950000 )
j.splitter    = SplitByFiles( filesPerJob=10 )
# j.outputfiles = [ DiracFile('*.sim'), LocalFile('*.xml') ]
j.outputfiles = [ LocalFile('*.dst'), LocalFile('*.xml') ]

queues.add( j.submit )
# j.submit()

#==============================================================================
