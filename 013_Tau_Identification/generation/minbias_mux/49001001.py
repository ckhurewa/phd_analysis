# Event Type: 49001001
#
# ASCII decay Descriptor: pp => ?
#
from Gaudi.Configuration import *
importOptions( "$DECFILESROOT/options/HardQCD_minbias.py" )
from Configurables import Generation
Generation().EventType = 49001000
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/minbias=HardQCD,pt18GeV.dec"
Generation().Special.CutTool = ""


Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/LoKi__FullGenEventCut"
from Configurables import LoKi__FullGenEventCut
Generation().addTool(LoKi__FullGenEventCut)
cutTool = Generation().LoKi__FullGenEventCut
cutTool.Code = 'HardMuPlusMuon | HardMuPlusHadron'
cutTool.Preambulo += """
from GaudiKernel.SystemOfUnits import GeV, mrad
IsMuon  = (GABSID==13)
IsHad   = (GHADRON & GCHARGED)
InAcc   = (GTHETA < 360.0*mrad)  # eta > 1.7
HardMu  = IsMuon & InAcc & (GPT > 18*GeV)
GoodMu  = IsMuon & InAcc & (GPT > 4*GeV)
GoodHad = IsHad  & InAcc & (GPT > 4*GeV)
#
HardMuPlusMuon   = (count(HardMu)>=1) & (count(GoodMu)>=2)
HardMuPlusHadron = (count(HardMu)>=1) & (count(GoodHad)>=1)
""".split('\n')


