
from glob import glob

## from my analysis/902_fullsim_resources
import Sim08h

#===============================================================================

# jids  = 4541, 4543, 4544, 4545, 4546, 4547, 4548, 4549, 4550, 4551, 4552, 4553
# ds    = LHCbDataset.new(x for jid in jids for x in jobs(jid).lfn_list() if x.endswith('sim'))

root = '/home/khurewat/gangadir/workspace/khurewat/LocalXML'
ds   = LHCbDataset.new(glob(root+'/4579/*/output/*.dst'))

#-------------------------------------------------------------------------------

j = Job()
j.name        = 'minbias_mux'
# j.comment     = 'Test timing & geneff'
# j.comment     = '49001001: Gauss+Dirac (20evt x 250sj)+195k'
j.comment     = '49001001 [120k:200k](1kevt/j)'
# j.application = Sim08h.Gauss( '49001001.py', 20 )
# j.application = Sim08h.Boole()
# j.application = Sim08h.Moore()
# j.application = Sim08h.Brunel()
j.application = Sim08h.DaVinci( r'.*(Z02TauTau_|WMu).*' )
# j.backend     = Dirac()
j.backend     = PBS(extraopts='--mem=3600 -t 1-0:0:0 --exclude=lphe01,lphe02,lphe03')  # Kill it before it consumes space
j.inputdata   = ds
# j.splitter    = GaussSplitter( eventsPerJob=20, numberOfJobs=250, firstEventNumber=195000 )
j.splitter    = SplitByFiles( filesPerJob=1 )
# j.outputfiles = [ DiracFile('*.sim'), LocalFile('*.xml') ]
j.outputfiles = [ LocalFile('*.dst'), '*.xml' ]

queues.add( j.submit )
# j.submit()

#==============================================================================
