from Gauss.Configuration import *
GenInit('GaussGen').RunNumber =  1
GenInit('GaussGen').FirstEventNumber = 1
# Gauss().OutputType  = 'NONE' # Disable GaussTape output
# Gauss().Histograms  = 'NONE' # Disable histo
LHCbApp().EvtMax    = 4  # Need anyway otherwise it'll crash (?)

