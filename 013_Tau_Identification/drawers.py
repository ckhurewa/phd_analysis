#!/usr/bin/env python

"""

Provide consistent drawing utilities.

"""

from PyrootCK import *
from qhist import utils
from qhist.styles import set_qhist_color_palette
import pandas as pd

#===============================================================================

def set_global_font_size(size=0.06): # default to LHCb
  ## like LHCbStyle, but dynamic
  ROOT.gStyle.textSize  = size
  ROOT.gStyle.labelSize = size, 'x'
  ROOT.gStyle.labelSize = size, 'y'
  ROOT.gStyle.titleSize = size*1.2, 'x'
  ROOT.gStyle.titleSize = size*1.2, 'y'

def set_global_line_width(val=2.00): # default to LHCb
  ROOT.gStyle.lineWidth      = val
  ROOT.gStyle.frameLineWidth = val
  ROOT.gStyle.histLineWidth  = val
  ROOT.gStyle.funcWidth      = val
  ROOT.gStyle.gridWidth      = val
  
def set_style_square():
  """
  Appropriate default for square plot, 
  to be used on top of LHCb Style.
  """
  ## Lower the font size, margin
  set_global_font_size(0.05)
  ROOT.gStyle.padRightMargin  = 0.02
  ROOT.gStyle.padTopMargin    = 0.02
  ROOT.gStyle.padBottomMargin = 0.13
  ROOT.gStyle.CanvasDefH      = 480
  ROOT.gStyle.CanvasDefW      = 480


#===============================================================================
# EFFICIENCIES
# Mainly used for reconstruction efficiencies.
#===============================================================================

def graphs_raw(df0):
  """
  Return list of TGraphs object, for better flexibility on custom styling.

  note: Don't cap by [0,1], be geenric
  """
  logger.debug('Input effs:\n%s'%df0)
  df = df0.copy() # don't disturb the original
  if hasattr(df0, 'name'): # hack
    df.name = df0.name

  ## save index name before lost
  index_name    = df.index.name
  df.index      = [ ufloat.from_interval(s) for s in df.index ]
  df.index.name = index_name

  ## Making TGraphErrors
  for name, col in df.iteritems():
    col = col.dropna() # drop na in each respective column.
    l = [(valx,valy) for valx,valy in col.iteritems()]
    if l:
      lx,ly = zip(*l)
      # g = ROOT.TGraphErrors.from_pair_ufloats(lx, ly)
      g = ROOT.TGraphAsymmErrors.from_pair_asymvar(lx, ly)
      g.title = str(name)
      g.ownership = False
      yield g


def graphs_eff(df, add_options=None, nudge_step=0., add_hook=None):
  """
  Specific drawer given a dataframe that fits these specs
  - A dataframe
  - can be in raw interval string (x, y], or in ufloat
  - Columns are each elements to be superimpose
  - Suitable for efficiencies plot
  """
  ## Start
  if os.environ['HOME'] == '/Users/khurewat': # Turn on for OSX only
    if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
      ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
    set_qhist_color_palette()

  ## Save info before they are lost
  xtitle = unicode(df.index.name)
  ytitle = unicode(getattr(df, 'name', df.columns.name))

  ## Nudging the graph if requested
  if nudge_step and isinstance(df, pd.DataFrame):
    cols = df.columns
    acc = {}
    for i, (tt, se) in enumerate(df.iteritems()):
      offset  = nudge_step * (i-(len(df.columns)-1.)/2)
      acc[tt] = se.rename(lambda m: m+offset)
    df = pd.concat(acc).unstack().T
    df = df[cols] # restore columns order

  ## Making TGraphErrors
  graphs = ROOT.TMultiGraph()
  graphs.ownership = False
  for i,g in enumerate(graphs_raw(df)):
    g.fillColor   = 0
    g.markerStyle = utils.MARKERS(i)
    g.markerColor = utils.COLORS(i)
    g.lineColor   = utils.COLORS(i)
    if add_hook: # optional pre-Add hook
      add_hook(i, g)
    ## todo: can it fetch the option from indivudial graph via add_hook?
    opt = 'lp' if not add_options else (add_options if isinstance(add_options, basestring) else add_options[i])
    graphs.Add(g, opt)

  ## Draw & config
  graphs.Draw('A')
  graphs.xaxis.title = xtitle
  graphs.yaxis.title = ytitle
  graphs.minimum     = 0.
  graphs.maximum     = 1.

  ## Legends, if n>1
  leg = None
  if len(graphs.graphs)>1:
    ROOT.gPad.BuildLegend()
    leg = ROOT.gPad.GetPrimitive('TPave')
    leg.ownership = False
    leg.fillColorAlpha = 0,0
    leg.header = 'LHCb preliminary'
    leg.x1 = 0.3
    leg.x2 = 0.7
    leg.y1 = 0.2
    leg.y2 = 0.4

  ## finally
  ROOT.gPad.Update()
  return graphs, leg


def graphs_dd_mc(df):
  """
  Customized for 2-entries comparison between DD, MC.
  """
  ## Start
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Making TGraphErrors
  graphs = ROOT.TMultiGraph()
  graphs.ownership = False

  gDD, gMC = list(graphs_raw(df)) # DD, MC
  gDD.fillColor      = 0
  gMC.markerStyle    = 2
  gMC.lineColorAlpha = 0,0
  # gMC.fillColorAlpha = ROOT.kRed, 0.7
  gMC.fillColor   = ROOT.kRed
  gMC.fillStyle   = 3245 # diagonal stripe

  ## Actual draw
  graphs.Add(gDD, 'AP')
  graphs.Add(gMC, 'AP2')
  graphs.Draw('A')

  ## post-config
  graphs.xaxis.title = unicode(df.index.name)
  graphs.yaxis.title = unicode(df.columns.name)
  graphs.minimum     = 0.
  graphs.maximum     = 1.

  ## legend
  ROOT.gPad.BuildLegend()
  leg = ROOT.gPad.GetPrimitive('TPave')
  leg.header = 'LHCb preliminary'
  leg.ownership = False
  leg.x1 = 0.3
  leg.x2 = 0.7
  leg.y1 = 0.2
  leg.y2 = 0.4

  ## finally
  ROOT.gPad.Update()
  return graphs, leg


#===============================================================================
# MISC
#===============================================================================

def note(msg, opt='NB'):
  """
  Fiducial msg, suitable for sliced efficiency (comparing data/MC of one var
  at another var fixed).
  """
  text = ROOT.TPaveText(0.65, 0.21, 0.92, 0.28, 'NDC')
  text.AddText(msg)
  text.ownership      = False
  text.fillColorAlpha = 1, 0
  text.textAlign      = 11 # left-bottom
  text.Draw(opt)
  return text

#-------------------------------------------------------------------------------

def Box(list_xy):
  """
  Quickly draw opaque box.
  """
  x,y = zip(*list_xy)
  pl  = ROOT.TPolyLine( len(x), x, y )
  pl.ownership      = False
  pl.fillColorAlpha = ROOT.kBlack, 0.4
  return pl

#-------------------------------------------------------------------------------

def Line(list_xy):
  """
  Quickly create a polyline.
  """
  x,y = zip(*list_xy)
  pl  = ROOT.TPolyLine( len(x), x, y )
  pl.ownership = False
  pl.lineColor = ROOT.kRed
  pl.lineWidth = 4
  return pl

#-------------------------------------------------------------------------------

def occupancy_grid(h, ETA, PT):
  """
  Given bins in ETA, PT, draw the grid that represents the binning regime.
  Assume the xaxis=PT, yaxis=ETA.

  Suitable for ePID probe occupancy.
  """
  def draw(line):
    ## With stylize ready
    line.ownership      = False
    line.lineColorAlpha = ROOT.kRed, 0.8
    line.lineStyle      = 2
    line.Draw()

  ## Parameterize & loop draw
  xarr, yarr = PT, ETA
  xmin = h.xaxis.xmin
  xmax = h.xaxis.xmax
  ymin = h.yaxis.xmin
  ymax = h.yaxis.xmax
  for xval in xarr:
    draw(ROOT.TLine(xval, ymin, xval, ymax))
  for yval in yarr:
    draw(ROOT.TLine(xmin, yval, xmax, yval))

