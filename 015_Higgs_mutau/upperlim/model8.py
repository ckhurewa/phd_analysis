#!/usr/bin/env python
"""

MODEL 8
- Combined channel
- Blob of background
- POI = cscbr

"""

import prep_workspace
import collectors
from lim_utils import (
  TTYPES, TTYPES_NOMU, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite
)

#===============================================================================

def prepare_model(w, mass, syst, exclude_mu=False):
  ## Early aware of exclude_mu
  ttypes = TTYPES_NOMU if exclude_mu else TTYPES

  ## Parameter of interest
  w.factory('cscbr [1, -50, 300]') # in picobarn

  ## Define nsig <-- cscbr * lumi * tt_mult
  q0_mult = 'brtau', 'eacc', 'erec', 'esel'
  
  # 1. non-channel-dependent quantity
  nuisance_gaussian.inject(w, 'lumi')

  # 2. channel-dependent correlated mult
  nuisance_gaussian.fullcorr4.inject(w, 'eacc', *TTYPES)
  nuisance_gaussian.corr4.inject    (w, 'erec', *TTYPES)

  ## Start looping over all channel
  for tt in TTYPES:

    ## 3. channel-dependent uncorrelated mult
    nuisance_gaussian.inject(w, tt+'_brtau', tt+'_esel')

    # Then, define the actual product: tt_nsig = cscbr * lumi * tt_mult
    q_nsig = ['cscbr','lumi'] + [tt+'_'+s for s in q0_mult]
    w.factoryArgs('prod: %s_nsig'%tt, q_nsig)

    ## Prepare queue of processes
    q_bkg = get_queues_bkg(tt)

    ## Single nuisance: nbkg, and other (const) fractional contribution
    nuisance_gaussian.inject(w, tt+'_nbkg')
    for bkg in q_bkg:
      w.factory('{0}_frac_{1} [0.]'.format(tt, bkg))

    ## Bind the const fraction of each background
    q_nexp = ['{0}_frac_{1} * {0}_pdf_{1}'.format(tt, bkg) for bkg in q_bkg]
    arg_epdf_sig = '{0}_nsig * {0}_pdf_higgs_{1}'.format(tt, mass)
    arg_epdf_bkg = '{0}_nbkg * {0}_pdf_BKG'.format(tt)
    w.factoryArgs('SUM: {0}_pdf_BKG'.format(tt) , q_nexp)
    w.factoryArgs('SUM: {0}_epdf_SIG'.format(tt), [arg_epdf_sig]) # for drawing
    w.factoryArgs('SUM: {0}_epdf_BKG'.format(tt), [arg_epdf_bkg]) # for drawing
    w.factoryArgs('SUM: {0}_epdf_SB'.format(tt) , [arg_epdf_sig, arg_epdf_bkg])

    ## Finally, the model with all gaussian constraints
    arg = 'PROD: {0}_model({0}_epdf_SB, syst_lumi, syst_eacc, syst_erec, syst_{0}_brtau, syst_{0}_esel, syst_{0}_nbkg)'
    w.factory(arg.format(tt))

  ## Make the simultaneous fit model
  if syst:
    if exclude_mu:
      w.factory('SIMUL::PDF(ttypes_nomu, e=e_model, h1=h1_model, h3=h3_model)')
    else:
      w.factory('SIMUL::PDF(ttypes, mu=mu_model, e=e_model, h1=h1_model, h3=h3_model)')
  else:
    if exclude_mu:
      w.factory('SIMUL::PDF(ttypes_nomu, e=e_epdf_SB, h1=h1_epdf_SB, h3=h3_epdf_SB)')
    else:
      w.factory('SIMUL::PDF(ttypes, mu=mu_epdf_SB, e=e_epdf_SB, h1=h1_epdf_SB, h3=h3_epdf_SB)')

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'cscbr')
  w.defineSet('OBS', 'mass' )

  ## Alias to observed data
  if exclude_mu:
    w.data('data_nomu_obs').SetName('DATA')
    w.var('mu_erec').constant = True
  else:
    w.data('data_obs').SetName('DATA')

#===============================================================================

def bind_values(w, regime, geo, mass, syst, exclude_mu):
  """
  Bind the initial values
  """
  ## Early aware of exclude_mu
  ttypes = TTYPES_NOMU if exclude_mu else TTYPES

  ## Helper binder
  bind = lambda key, val: nuisance_gaussian.bind(w, key, val, syst)

  ## 1. non-channel-dependent quantity
  bind('lumi', collectors.lumi)

  ## 2. channel-dependent correlated mult
  nuisance_gaussian.fullcorr4.bind(w, 'eacc', collectors.eacc().loc[(geo,mass)], syst)
  nuisance_gaussian.corr4.bind    (w, 'erec', collectors.erec().loc[(regime,mass)], collectors.corr_erec(regime,mass), syst)
  
  ## 3. channel-dependent uncorrelated
  for tt in ttypes:
    ## Bind backgrounds fractions
    nbkgs = collectors.nbkg().loc[regime][tt]
    nbkg  = nbkgs.sum()
    bind(tt+'_nbkg', nbkg)
    for bkg, val in nbkgs.iteritems():
      v = w.var('{0}_frac_{1}'.format(tt, bkg))
      if v:
        v.val = val.n/nbkg.n

    ## Bind other mult
    bind(tt+'_brtau', collectors.br[tt])
    bind(tt+'_esel' , collectors.esel().loc[(regime,mass), tt])

#===============================================================================

def main_single(args):
  ## Load the base workspace
  args.tt = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.mass, args.syst, args.exclude_mu)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.geo, args.mass, args.syst, args.exclude_mu)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
