#!/usr/bin/env python
"""

MODEL 1
- Single channel
- Simple counting experiment (Poisson)
- POI = nsig

"""

import prep_workspace
import collectors
from lim_utils import (
  ROOT, TTYPES, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite, tune_POI_range,
)

#===============================================================================

def prepare_model(w, syst):
  ## Parameter of interest & observable
  w.factory('nsig [0, -200, 1000]')
  w.factory('nobs [0]')

  ## Simple Poisson counting model with Gaussian nbkg
  nuisance_gaussian.inject(w, 'nbkg')
  w.factory('sum:nexp(nsig, nbkg)')
  w.factory('Poisson:nominal(nobs, nexp)')

  ## Finally, switch with/without systematic
  if syst:
    w.factory('PROD:PDF(nominal, syst_nbkg)')
  else:
    w.Import(w.pdf('nominal'), RenameVariable=('nominal', 'PDF'))

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'nsig')
  w.defineSet('OBS', 'nobs')

#===============================================================================

def bind_values(w, regime, tt, syst):
  ## Number of background, as Gaussian nuisance param
  nbkg = collectors.nbkg().loc[regime, tt].sum()
  nuisance_gaussian.bind(w, 'nbkg', nbkg, syst)

  ## Number of observed
  w.set('OBS').first().val = collectors.nobs().loc[regime, tt]  
  data = ROOT.RooDataSet('DATA', 'DATA', w.set('OBS'))
  data.add(w.set('OBS'))
  w.Import(data)

  ## Adjust POI lower bound
  tune_POI_range(w)

#===============================================================================

def main_single(args):
  ## Load the base workspace, manually simplify the name
  args.kernel = args.geo = args.mass = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.syst)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.tt, args.syst)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
