#!/usr/bin/env python

from PyrootCK import *
from lim_utils import (
  FINAL_KERNEL, FINAL_REGIME,
  pd, memorized, gpad_save, plot_bestfit, plot_brazillian, X_to_latex,
)

## local data
import load_upperlim
import load_bestfit
import prod_csc

#===============================================================================
# BRANCHING FRACTION (a.k.a SIGNAL STRENGTH)
#===============================================================================

@memorized
def get_upperlim_percent():
  return load_upperlim.load_br().loc['3FitBkgs', FINAL_KERNEL, 'minzexp', 'withsyst'].unstack()*100.

@memorized
def get_bestfit_percent():
  return load_bestfit.load_asym_brhmt()*100

@gpad_save
def draw_bestfit():
  # df = branching_fraction()[['best']] * 100.
  df = pd.DataFrame({'brhmt': load_bestfit.load_asym_brhmt()})*100.
  gr, leg = plot_bestfit(df)
  gr.yaxis.title = 'BR [%]'
  gr.minimum = -30
  gr.maximum = 50

@gpad_save
def draw_upperlim():
  ## Apply LHCb style
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ## Get data
  gr, leg = plot_brazillian(get_upperlim_percent())
  gr.minimum = 1.
  gr.maximum = 200

#===============================================================================

def latex():
  ulim = get_upperlim_percent()
  df   = pd.DataFrame({
    'best%': get_bestfit_percent().fmt1l,
    'obs%' : ulim['obs'].fmt1l,
    'exp%' : ulim['exp1'].fmt1l,
  })
  return X_to_latex(df).to_markdown(stralign='right')

#===============================================================================
# SM VERSION (mH = 125)
#===============================================================================

def get_SM_bestfit():
  scale = prod_csc.scale_125_BSM_to_SM() # csc scale factor
  return get_bestfit_percent().loc[125] * (1./scale)

@memorized
def get_SM_upperlim():
  """
  Approximate by scaling the csc from BSM to SM. 
  For uncertainty (8% -> 5%), it's more difficult to do... leave it
  """
  scale = prod_csc.scale_125_BSM_to_SM() # csc scale factor
  se    = get_upperlim_percent().loc[125]/scale/100. # back to non-percent
  ## repack it to explicit form
  return pd.Series({
    'obs'      : se['obs'],
    'exp'      : se['exp'],
    'exp_high1': se['exp1'].high,
    'exp_high2': se['exp2'].high,
    'exp_low1' : se['exp1'].low,
    'exp_low2' : se['exp2'].low,
  })

def latex_SM_bestfit():
  fmt = '$\\BR(\\HMT) = \\tol{{{0.val:.2f}}}{{{0.ehigh:.2f}}}{{{0.elow:.2f}}}\\%$'
  return fmt.format(get_SM_bestfit())

def latex_SM_obslim():
  fmt = '$\\BR(\\HMT) < {:.1f}\\%$'
  return fmt.format(get_SM_upperlim().obs*100.) 

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv:
    ROOT.gROOT.batch = True
    draw_bestfit()
    draw_upperlim()
    sys.exit()

  ## DEV
  # print load_bestfit.load_asym_brhmt()
  # print get_upperlim_percent()
  draw_bestfit()
  # draw_upperlim()
  # print latex()

  ## SM
  # print get_SM_bestfit()
  # print get_SM_upperlim()
  # print latex_SM_bestfit()
  # print latex_SM_obslim()
