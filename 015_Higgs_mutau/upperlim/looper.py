#!/usr/bin/env python
"""

Because RooFit's variable singleton model is tricky, I should (can't) do loop 
in a single python session. Instead, I have to spawn subprocess for each 
iteration instead.

"""

import re
import sys
import subprocess
import itertools
from glob import glob
from lim_utils import REGIMES, MASSES, TTYPES, KERNELS, GEOS, create_clean_dir

SYST0 = ['--with-syst']
SYSTS = ['--with-syst', '--no-syst']
GEO0  = ['4pi']

## argparse flag, to be prefixed with '--'.
# The order must match the array below
TYPE_LOOP = 'geo', 'kernel', 'regime', 'tt', 'mass', None # last one is dummy flag for syst
ALL_LOOP = {
  ## Poisson
  1: [ None, None, REGIMES, TTYPES, None  , SYST0], # nsig
  7: [ GEO0, None, REGIMES, TTYPES, MASSES, SYST0], # cscbr
  9: [ GEOS, None, REGIMES, None  , MASSES, SYST0], # cscbr-simult
  10:[ GEO0, None, REGIMES, None  , MASSES, SYST0], # brhmt-simult
  ## Blob-bkg
  2: [ None, KERNELS, REGIMES, TTYPES, MASSES, SYST0], # nsig
  3: [ GEO0, KERNELS, REGIMES, TTYPES, MASSES, SYST0], # cscbr
  8: [ GEOS, KERNELS, REGIMES, None  , MASSES, SYST0], # cscbr-simult
  11:[ GEO0, KERNELS, REGIMES, None  , MASSES, SYST0], # brhmt-simult
  ## Free-bkg
  4: [ None, KERNELS, REGIMES, TTYPES, MASSES, SYSTS], # nsig
  5: [ GEO0, KERNELS, REGIMES, TTYPES, MASSES, SYSTS], # cscbr
  6: [ GEOS, KERNELS, REGIMES, None  , MASSES, SYSTS], # cscbr-simult
  12:[ GEO0, KERNELS, REGIMES, None  , MASSES, SYSTS], # brhmt-simult
}

#===============================================================================

def init_clean_dir(func):
  """
  Helper decorator to delete & create clean model directory.
  """
  def wrap(n, *args, **kwargs):
    ## Clean existing
    dname = 'model%i'%n
    create_clean_dir(dname)
    ## actual run
    func(n, *args, **kwargs)
  return wrap

#===============================================================================

def get_mass(flags):
  """
  Helper method to reverse getting mass from list of flags
  """
  return int(flags[flags.index('--mass')+1]) if '--mass' in flags else None

def gen_loop(n):
  """
  Specifying the spec, yield the appropriate list of flags ready for exe.
  """
  ## For each kind of flag, inflate the list of args for that flag first.
  acc = []
  for tag, arr in zip(TYPE_LOOP, ALL_LOOP[n]):
    if arr:
      if tag is not None:
        flag = '--'+tag
        acc.append([(flag, str(x)) for x in arr])
      else:
        acc.append([(str(x),) for x in arr])
  ## loop over product of args
  for x in itertools.product(*acc):
    l = [z for y in x for z in y]
    ## skip lowmass in non-lowmass regime, mostly not well-defined.
    valid = True
    mass = get_mass(l)
    if mass is not None:
      if 'tight' in l: # skip 45, 55, 65
        if mass <= 65:
          valid = False
      if 'nominal' in l:
        if mass == 45: # skip lowmass 45
          valid = False
        if mass >= 165: # skip highmass 165, 175, 185, 195
          valid = False
      if 'lowmass' in l:
        if mass > 125: # skip highmass, keep [45,125]
          valid = False
    # finally
    if valid:
      yield l

@init_clean_dir
def loop_model_sync(n):
  """
  Do each loop sequantially, slow, but with automated post-processing.
  """
  ## Start looping
  stdout = ''
  for flags in gen_loop(n):
    args = ['./model%i.py'%n] + flags
    stdout += subprocess.check_output(args)+'\n'
  ## Also flush stdout to file
  with open('model%i/stdout'%n, 'w') as fout:
    fout.write(stdout)

#-------------------------------------------------------------------------------

@init_clean_dir
def loop_model_slurm(n, debug=False):
  """
  Use SLURM backend for the loop, ignore synchronous post-processing 
  """
  args0 = ['sbatch', '-N', '1', '-n', '1', '--ntasks-per-node', '1']
  if debug:
    args1 = ['--mem', '1450', '-p', 'debug', '-A', 'debug']
  else: #main
    args1 = ['--mem', '1450']
  for flags in gen_loop(n):
    args = args0 + args1 + ['model%i.py'%n] + flags
    subprocess.call(args)

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    ## loop over all model
    for mid in ALL_LOOP:
      loop_model_slurm(mid)
    sys.exit()

  # ## DEV
  # for flags in gen_loop(2):
  #   print flags

  ## EXE
  # (1,7,9,10), (2,3,8,11), (4,5,6,12)
  # loop_model_sync(12)
  loop_model_slurm(12)
