#!/bin/bash
# This will delete all existing draw, and call '--redraw' again on all applicable 
# script, making daily archive along the way.
# Some prefetch computation used for drawing will also be discarded this way
# The draws from fitting (CLs, duo, profileLL) which are defraged are not included,
# and thus not discarded.

## Vars
DATE=`date +%y%m%d`

## Discard them first
rm -f model*/*.pdf

## Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

loads(){
  ## Clear
  rm -f load*/*.df
  rm -f load*/*.pdf

  ## Execute
  ./load_upperlim.py --redraw
  ./load_bestfit.py --redraw
  ./load_pull.py

  ## Archive the pickled results
  for DNAME in load_bestfit load_upperlim; do
    rm -rf $DNAME/$DATE # delete existing
    mkdir -p $DNAME/$DATE
    cp $DNAME/*.df $DNAME/$DATE
  done  
}

validate(){
  ## Clear
  for dname in regime strategy tt syst kernel; do
    rm -rf validate_*/$dname
  done

  ## Execute
  ./validate_upperlim.py
  ./validate_bestfit.py

  ## Archive: validation has custom sub dirs
  for DNAME in validate_upperlim validate_bestfit; do
    rm -rf $DNAME/$DATE # delete existing
    mkdir -p $DNAME/$DATE
    for SUBDIRNAME in regime strategy tt syst kernel; do
      cp -R $DNAME/$SUBDIRNAME $DNAME/$DATE/
    done
  done
}

limits(){
  ## Clear
  rm -f cscbr/*.df 
  rm -f cscbr/*.pdf

  ## Execute
  ./cscbr.py --redraw

  ## Different plots of the upperlim/bestfit
  mkdir -p cscbr/$DATE
  cp cscbr/*.pdf cscbr/$DATE/

  ## Summary of limits as a func of mass
  for MID in 2 3 4 5 6 7 8 9; do
    # mkdir -p cscbr/$DATE/model$MID
    cp -R model$MID/limits cscbr/$DATE/model$MID
  done
}

mass(){
  ## Execute
  ./massfit.py --redraw
}

others(){
  ## Clear
  rm -f signal_yield/*.pdf
  rm -f branching_fraction/*.pdf

  ## Execute
  ./signal_yield.py --redraw
  ./branching_fraction.py --redraw  

  ## Different plots of the upperlim/bestfit
  for DNAME in signal_yield branching_fraction; do
    mkdir -p $DNAME/$DATE
    cp $DNAME/*.pdf $DNAME/$DATE/
  done
}

## Execute modules
# loads
# validate
limits
# mass
# others
