#!/usr/bin/env python
"""

MODEL 9
- Combined channel
- Simple counting experiment (Poisson)
- POI = cscbr

"""

import prep_workspace
import collectors
from lim_utils import (
  ROOT, TTYPES, TTYPES_NOMU, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite,
)

#===============================================================================

def prepare_model(w, syst, exclude_mu=False):
  ## Early aware of exclude_mu
  ttypes = TTYPES_NOMU if exclude_mu else TTYPES

  ## Parameter of interest
  w.factory('cscbr [1, -100, 300]') # in picobarn

  ## Define nsig <-- cscbr * lumi * tt_mult
  q0_mult = 'brtau', 'eacc', 'erec', 'esel'

  # 1. non-channel-dependent quantity
  nuisance_gaussian.inject(w, 'lumi')

  # 2. channel-dependent correlated mult
  nuisance_gaussian.fullcorr4.inject(w, 'eacc', *TTYPES)
  nuisance_gaussian.corr4.inject    (w, 'erec', *TTYPES)

  ## Start looping over all channel
  for tt in TTYPES:

    ## Observable
    w.factory('%s_nobs [0]'%tt)

    ## 3. channel-dependent uncorrelated mult
    nuisance_gaussian.inject(w, tt+'_brtau', tt+'_esel')

    ## Then, define the actual product: tt_nsig = cscbr * lumi * tt_mult
    q_nsig = ['cscbr','lumi'] + [tt+'_'+s for s in q0_mult]
    w.factoryArgs('prod: %s_nsig'%tt, q_nsig)

    ## Simple Poisson counting model with Gaussian nbkg
    nuisance_gaussian.inject(w, '%s_nbkg'%tt)
    w.factory('sum    : {0}_nexp   ({0}_nsig, {0}_nbkg)'.format(tt))
    w.factory('Poisson: {0}_nominal({0}_nobs, {0}_nexp)'.format(tt))

  ## Product of poisson-likelihood PDF
  q_cons  = ['syst_lumi', 'syst_eacc', 'syst_erec']
  q_model = ['%s_nominal']
  if syst:
    q_model += ['syst_%s_nbkg', 'syst_%s_brtau', 'syst_%s_esel']
  w.factoryArgs('PROD: PDF', [s%tt for s in q_model for tt in ttypes]+q_cons)

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'cscbr')
  if exclude_mu:
    w.defineSet('OBS', 'h1_nobs,h3_nobs,e_nobs')
    w.var('mu_erec').constant = True
  else:
    w.defineSet('OBS', 'mu_nobs,h1_nobs,h3_nobs,e_nobs')

#===============================================================================

def bind_values(w, regime, geo, mass, syst, exclude_mu=False):
  ## Early aware of exclude_mu
  ttypes = TTYPES_NOMU if exclude_mu else TTYPES

  ## Helper binder
  bind = lambda key, val: nuisance_gaussian.bind(w, key, val, syst)

  ## Observed dataset
  data = ROOT.RooDataSet('DATA', 'DATA', w.set('OBS'))

  ## 1. non-channel-dependent quantity
  bind('lumi', collectors.lumi)

  ## 2. channel-dependent correlated mult
  nuisance_gaussian.fullcorr4.bind(w, 'eacc', collectors.eacc().loc[(geo,mass)], syst)
  nuisance_gaussian.corr4.bind    (w, 'erec', collectors.erec().loc[(regime,mass)], collectors.corr_erec(regime,mass), syst)

  ## 3. channel-dependent uncorrelated
  for tt in ttypes:
    ## Number of observed, bind into the RooRealVar first
    w.var(tt+'_nobs').val = collectors.nobs().loc[regime, tt]  

    ## Number of background, as Gaussian nuisance param
    nbkg = collectors.nbkg().loc[regime, tt].sum()
    bind(tt+'_nbkg', nbkg)
    
    ## Bind other mult
    bind(tt+'_brtau', collectors.br[tt])
    bind(tt+'_esel' , collectors.esel().loc[(regime,mass), tt])

  ## finally, append into new dataset
  data.add(w.set('OBS'))
  w.Import(data)

#===============================================================================

def main_single(args):
  ## Load the base workspace
  args.kernel = args.tt = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.syst, args.exclude_mu)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.geo, args.mass, args.syst, args.exclude_mu)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
