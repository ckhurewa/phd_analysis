#!/usr/bin/env python
"""

MODEL 3
- Single channel
- Blob of background
- POI = cscbr

"""

import prep_workspace
import collectors
from lim_utils import (
  TTYPES, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite, tune_POI_range,
)

#===============================================================================

def prepare_model(w, tt, mass, syst):
  ## Parameter of interest
  w.factory('cscbr [1, -200, 1000]') # in picobarn

  ## Define nsig <-- cscbr
  nuisance_gaussian.inject(w, 'lumi', 'mult')

  # Then, define the actual product Nsig = ...
  w.factory('prod: nsig (cscbr, lumi, mult)')

  ## Prepare queue of processes
  q_bkg = get_queues_bkg(tt)

  ## Single nuisance: nbkg, and other (const) fractional contribution
  nuisance_gaussian.inject(w, 'nbkg')
  for bkg in q_bkg:
    w.factory('frac_{0} [0.]'.format(bkg))

  ## Bind the const fraction of each background
  q_bkgpdf = ['frac_{0} * {1}_pdf_{0}'.format(bkg, tt) for bkg in q_bkg]
  arg_epdf_bkg = 'nbkg * pdf_BKG'
  arg_epdf_sig = 'nsig * {0}_pdf_higgs_{1}'.format(tt, mass)
  w.factoryArgs('SUM: pdf_BKG' , q_bkgpdf)
  w.factoryArgs('SUM: epdf_SIG', [arg_epdf_sig])
  w.factoryArgs('SUM: epdf_BKG', [arg_epdf_bkg])
  w.factoryArgs('SUM: epdf_SB' , [arg_epdf_sig, arg_epdf_bkg])

  ## Finally, switch with/without systematic
  if syst:
    w.factory('PROD: PDF(epdf_SB, syst_nbkg, syst_mult, syst_lumi)')
  else:
    w.Import(w.pdf('epdf_SB'), RenameVariable=('epdf_SB', 'PDF'))

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'cscbr')
  w.defineSet('OBS', 'mass')

  ## Alias to observed data
  w.data('%s_data_OS'%tt).SetName('DATA')

#===============================================================================

def bind_values(w, regime, tt, geo, mass, syst):
  """
  Bind the initial values
  """
  ## For the background fraction, borrow model2
  import model2
  model2.bind_values(w, regime, tt, syst)

  ## Bind multiplicative terms
  bind = lambda key, val: nuisance_gaussian.bind(w, key, val, syst)
  bind('lumi', collectors.lumi)
  bind('mult', collectors.inverse_xs_nolumi(regime=regime, tt=tt, geo=geo, mass=mass))

  ## Adjust POI lower bound
  tune_POI_range(w)

#===============================================================================

def main_single(args):
  ## Load the base workspace
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.tt, args.mass, args.syst)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.tt, args.geo, args.mass, args.syst)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
