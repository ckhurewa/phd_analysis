#!/usr/bin/env python

from PyrootCK import *
from lim_utils import KERNELS_ALL, REGIMES, TTYPES, memorized, packages, gpad_save

OUTPUT = 'study_keys/output.root'
WNAME  = 'study_keyspdf'

MIRROR_OPTIONS = [
  'NoMirror',
  'MirrorLeft',
  'MirrorRight',
  'MirrorBoth',
  'MirrorAsymLeft',
  'MirrorAsymLeftRight',
  'MirrorAsymRight ',
  'MirrorLeftAsymRight',
  'MirrorAsymBoth',
]

RHO_OPTIONS = {
  '0.1': 0.1,
  '0.2': 0.2,
  '0.5': 0.5,
  '1.0': 1.0,
  '1.2': 1.2,
  '1.5': 1.5,
  '2.0': 2.0,
  '3.0': 3.0,
}

RHO_OPTIONS_ZMUMU = {
  '0.3': 0.3,
  '0.4': 0.4,
  '0.5': 0.5,
  '0.6': 0.6,
  '0.7': 0.7,
  '0.8': 0.8,
  '0.9': 0.9,
  '1.0': 1.0,
  '1.1': 1.1,
  '1.2': 1.2,
  '1.3': 1.3,
  '1.4': 1.4,
  '1.5': 1.5,
}

#===============================================================================

def save():
  """
  Save all variation of KeysPdf. SLOW!
  """
  ## prep workspace
  w = ROOT.RooWorkspace(WNAME)
  massmin, massmax = 0, 200
  w.factory('mass[30, %i, %i]'%(massmin, massmax))
  m = w.var('mass')
  m.bins = (massmax-massmin)/5  # Bin of 5GeV

  ## tree, dataset, import
  trees = packages.rect_selected().load_tree_dict_workspace('h1', 'lowmass')
  tree  = trees['QCD']
  ds    = ROOT.RooDataSet('ds', 'ds', tree, {m})
  w.Import(ds)
  print 'Load dataset: ', ds.numEntries()

  ## Create different keyspdf
  for opt in MIRROR_OPTIONS:
    print 'Using opt:', opt
    w.factory('KeysPdf: QCD_{0}(mass, ds, {0})'.format(opt))

  ## Different rho
  for tag, rho in RHO_OPTIONS.iteritems():
    print 'Using opt:', rho 
    w.factory('KeysPdf: QCD_{}(mass, ds, NoMirror, {})'.format(tag, rho))

  ## Save for draw later
  w.Print()
  w.writeToFile(OUTPUT)

def save_zmumu():
  ## prep workspace
  w = ROOT.RooWorkspace(WNAME)
  massmin, massmax = 0, 200
  w.factory('mass[30, %i, %i]'%(massmin, massmax))
  m = w.var('mass')
  m.bins = (massmax-massmin)/5  # Bin of 5GeV

  ## Load zmumu
  for regime in ['lowmass', 'nominal']:
    trees = packages.rect_selected().load_tree_dict_workspace('mu', regime)
    tree  = trees['Zll']
    name  = 'ds_'+regime
    ds    = ROOT.RooDataSet(name, name, tree, {m})
    w.Import(ds)
    print 'Load dataset: ', ds.numEntries()

    ## Different rho
    for tag, rho in RHO_OPTIONS_ZMUMU.iteritems():
      print 'Using opt:', rho 
      w.factory('KeysPdf: Zll_{0}_{1}(mass, ds_{0}, NoMirror, {2})'.format(regime, tag, rho))

  ## Save for draw later
  w.Print()
  w.writeToFile('study_keys/zmumu.root')

#===============================================================================

def draw_mirror():
  ## Init
  fin = ROOT.TFile(OUTPUT)
  w   = fin.Get(WNAME)

  ## Loop draw
  c = ROOT.TCanvas()
  frame = w.var('mass').frame()
  w.data('ds').plotOn(frame)
  for i, opt in enumerate(MIRROR_OPTIONS):
    w.pdf('QCD_'+opt).plotOn(frame, lineColor=utils.COLORS(i), lineWidth=1)
  frame.Draw()
  c.BuildLegend()
  c.SaveAs('study_keys/mirror.pdf')


def draw_rho():
  ## Init
  fin = ROOT.TFile(OUTPUT)
  w   = fin.Get(WNAME)

  ## Loop draw
  c = ROOT.TCanvas()
  frame = w.var('mass').frame()
  w.data('ds').plotOn(frame)
  for i, opt in enumerate(RHO_OPTIONS):
    w.pdf('QCD_'+opt).plotOn(frame, lineColor=utils.COLORS(i), lineWidth=1)
  frame.Draw()
  c.logy = True
  c.BuildLegend()
  c.SaveAs('study_keys/rho.pdf')

def draw_zmumu():
  """
  Draw Zmumu shape at different rho
  """
  fin = ROOT.TFile('study_keys/zmumu.root')
  w   = fin.Get(WNAME)
  m   = w.var('mass')
  m.bins = 100

  c   = ROOT.TCanvas('zmumu', 'zmumu', 960, 640)
  c.Divide(1,2)
  for i, regime in enumerate(['lowmass', 'nominal']):
    c.cd(i+1)
    frame = m.frame(title=regime)
    w.data('ds_'+regime).plotOn(frame)
    for j, opt in enumerate(sorted(RHO_OPTIONS_ZMUMU)[:-5]):
      w.pdf('Zll_%s_%s'%(regime, opt)).plotOn(frame, lineColor=utils.COLORS(j), lineWidth=1)
    frame.Draw()
    ROOT.gPad.BuildLegend()
  c.SaveAs('study_keys/zmumu.pdf')

def fit_landau():
  ## Init
  fin = ROOT.TFile(OUTPUT)
  w   = fin.Get(WNAME)
  w.var('mass').setRange('fit', 30, 200)

  ## new dist
  # w.factory('Landau: pdf(mass, mean[20,40], sigma[0,20])')

  w.factory('Landau   : f1(mass, mean[30,40], sigma[0,10])')
  w.factory('Gaussian : f2(mass, mean2[0,5], sigma2[0,20])')
  w.factory('FCONV    : pdf(mass, f1, f2)')


  data  = w.data('ds')
  model = w.pdf('pdf')
  model.fitTo(data, Range='fit')

  frame = w.var('mass').frame()
  data.plotOn(frame)
  model.plotOn(frame)
  frame.Draw()
  ROOT.gPad.SaveAs('study_keys/landau.pdf')

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  # save()
  # save_zmumu()
  # draw_mirror()
  # draw_rho()
  draw_zmumu()
  # fit_landau()
