#!/usr/bin/env python
"""
Handle the CSCBR results
- best fit
- upperlim
- draw figures
- latex tables

"""

from PyrootCK import *
from PyrootCK.mathutils import asymvar
import load_upperlim
import load_bestfit

from lim_utils import (
  pd, memorized, gpad_save, pickle_dataframe_nondynamic, 
  POI_TO_ROOTLABELS, REGIME_TO_LATEX, FINAL_REGIME, FINAL_KERNEL, TT_TO_ROOTLABEL,
  X_TO_LATEX, X_to_latex,
  create_clean_dir, plot_brazillian, plot_bestfit, 
)

## dynamic yaxis range depends on the POI
YRANGE = {
  'cscbr'     : (0.9, 300),
  'cscbr_lhcb': (1e-2, 10),
  'nsig'      : (1, 200),
}

#===============================================================================
# INTERFACE TO FINAL RESULTS
#===============================================================================

## convert particular columns of exp,exp1 to asymvar
_exp_asym = lambda se: asymvar(se.exp, se.exp1.high-se.exp, se.exp-se.exp1.low)

@memorized
def upperlim_4pi():
  """
  Final value to be used as cscbr upperlim
  """
  df = load_upperlim.load_model(6).loc[FINAL_KERNEL, FINAL_REGIME, '4pi', 'withsyst'].unstack()
  ## Add the asym-var ready for latex
  df['exp_asym'] = df.apply(_exp_asym, axis=1)
  return df

@memorized
def upperlim_lhcb():
  """
  Scaled value in fiducial (geometrical acceptance).
  """
  df = load_upperlim.load_model(6).loc[FINAL_KERNEL, FINAL_REGIME, 'lhcb', 'withsyst'].unstack()
  df.columns.name = 'cscbr_lhcb'
  ## Add the asym-var ready for latex
  df['exp_asym'] = df.apply(_exp_asym, axis=1)  
  return df

@memorized
def bestfit_asym():
  """
  Choose regime/kernel here.
  """
  return load_bestfit.load_asym_cscbr().loc[FINAL_KERNEL, 'minerr']

#===============================================================================
# COMPARE CLs LIMITS
# The most basic information via "Brazillian" plots.
#===============================================================================

def draw_model_CLs(n):
  """
  Generic draw over upper limit over given dataframes.
  This will draw the results into the respective model.
  """
  ## determine free levels
  data = load_upperlim.load_model(n)
  level = range(len(set(data.index.names)-{'mass' , 'nsig', 'cscbr'}))
  
  ## Make sure target subdir exists
  dpath = 'model%i/limits'%n
  create_clean_dir(dpath)

  ## Then loop over them
  for keys, df in data.groupby(level=level):
    name   = keys if isinstance(keys, basestring) else '_'.join(keys)
    target = os.path.join(dpath, 'limit_%s.pdf'%name)
    
    ## settle the name for ytitle
    if 'lhcb' in target:
      df.index.names = list(df.index.names)[:-1]+['cscbr_lhcb']
    poi = 'cscbr' if '4pi' in target else 'cscbr_lhcb' if 'lhcb' in target else 'nsig'

    ## start drawing
    gr, leg = plot_brazillian(df.loc[keys].unstack())
    gr.minimum, gr.maximum = YRANGE[poi]
    ROOT.gPad.SaveAs(target)

#-------------------------------------------------------------------------------

@gpad_save
def draw_ulim_4pi():
  """
  Final 4pi (inclusive) limit
  """
  gr, leg = plot_brazillian(upperlim_4pi())
  gr.minimum = 1
  gr.maximum = 100
  leg.header = 'LHCb #sqrt{#it{s}} = 8 TeV'
  leg.x1NDC  = 0.70
  leg.x2NDC  = 1.00
  leg.y1NDC  = 0.60
  leg.y2NDC  = 0.92


@gpad_save
def draw_ulim_lhcb():
  """
  Final reduced (LHCb geometrical acceptance) limit
  """
  gr, leg = plot_brazillian(upperlim_lhcb())
  gr.minimum = 1.5e-2
  gr.maximum = 4.
  leg.header = 'LHCb #sqrt{#it{s}} = 8 TeV'
  leg.x1NDC  = 0.65
  leg.x2NDC  = 1.00
  leg.y1NDC  = 0.60
  leg.y2NDC  = 0.92

@gpad_save
def draw_ulim_4pi_extra():
  """
  Superimpose with individual channels, per EPJC request.
  """
  gr, leg = plot_brazillian(upperlim_4pi())
  gr.minimum = 1
  gr.maximum = 100
  leg.header = 'LHCb #sqrt{#it{s}} = 8 TeV'
  leg.x1NDC  = 0.16
  leg.x2NDC  = 0.45
  leg.y1NDC  = 0.18
  leg.y2NDC  = 0.50
  leg.textFont = 132
  leg.textSize  = 0.05

  ## new legend
  leg2 = ROOT.TLegend()
  leg2.ConvertNDCtoPad()
  leg2.ownership = False
  leg2.fillStyle = 0
  leg2.textSize  = 0.06
  leg2.textFont  = 132
  # leg2.header = 'Observed limit'
  leg2.x1NDC  = 0.38
  leg2.x2NDC  = 0.65
  leg2.y1NDC  = 0.18
  leg2.y2NDC  = 0.40

  ## Add individual channel.
  df = load_upperlim.load_model(5)
  df = df.xs([FINAL_KERNEL,FINAL_REGIME,'4pi','obs','withsyst'], level=['kernel','regime','geo','cscbr','syst']).unstack('tt')
  
  g = ROOT.TGraphAsymmErrors.from_pair_asymvar(df['e'].index, df['e'].values)
  g.ownership   = False
  g.lineWidth   = 1
  g.lineColor   = ROOT.kBlue
  g.markerColor = g.lineColor
  g.markerStyle = 24
  gr.Add(g, 'LP')
  leg2.AddEntry(g, TT_TO_ROOTLABEL['e'], 'lp')

  g = ROOT.TGraphAsymmErrors.from_pair_asymvar(df['h1'].index, df['h1'].values)
  g.ownership   = False
  g.lineWidth   = 1
  g.lineColor   = ROOT.kRed
  g.markerColor = g.lineColor
  g.markerStyle = 25
  gr.Add(g, 'LP')  
  leg2.AddEntry(g, TT_TO_ROOTLABEL['h1'], 'lp')

  g = ROOT.TGraphAsymmErrors.from_pair_asymvar(df['h3'].index, df['h3'].values)
  g.ownership   = False
  g.lineWidth   = 1
  g.lineColor   = ROOT.kMagenta
  g.markerColor = g.lineColor
  g.markerStyle = 26
  gr.Add(g, 'LP')
  leg2.AddEntry(g, TT_TO_ROOTLABEL['h3'], 'lp')

  g = ROOT.TGraphAsymmErrors.from_pair_asymvar(df['mu'].index, df['mu'].values)
  g.ownership   = False
  g.lineWidth   = 1
  g.lineColor   = ROOT.kGreen+3
  g.markerColor = g.lineColor
  g.markerStyle = 27
  gr.Add(g, 'LP')
  leg2.AddEntry(g, TT_TO_ROOTLABEL['mu'], 'lp')
  
  leg2.Draw()

@gpad_save
def draw_ulim_4pi_extra2():
  
  def fmt_obs(se0, title, color):
    se = se0.xs('obs', level='cscbr')
    g = ROOT.TGraphAsymmErrors.from_pair_asymvar(se.index, se.values)
    g.ownership   = False
    g.title       = title
    g.markerStyle = 8
    g.markerSize  = 0.5
    g.markerColor = color
    g.lineWidth   = 2
    g.lineColor   = color
    g.fillColor   = 0
    return g

  def fmt_exp(se0, title, color):
    se = se0.xs('exp', level='cscbr')
    g = ROOT.TGraphAsymmErrors.from_pair_asymvar(se.index, se.values)
    g.ownership   = False
    g.title       = title
    g.markerStyle = 8
    g.markerSize  = 0.5
    g.markerColor = color
    g.lineWidth   = 2
    g.lineStyle   = 2
    g.lineColor   = color
    g.fillColor   = 0
    return g

  ## Use the helper constructor immediately
  c = ROOT.TCanvas()
  c.ownership = False

  ## Add individual channel.
  df = load_upperlim.load_model(5)
  df = df.xs([FINAL_KERNEL,FINAL_REGIME,'4pi','withsyst'], level=['kernel','regime','geo','syst']).unstack('tt')
  df2 = load_upperlim.load_model(6).xs([FINAL_KERNEL,FINAL_REGIME,'4pi','withsyst'], level=['kernel','regime','geo','syst'])

  gr = ROOT.TMultiGraph()
  gr.ownership = False

  gr.Add(fmt_obs(df2     , 'Simultaneous'       , color=ROOT.kBlack)  , 'LP')
  gr.Add(fmt_obs(df['e'] , TT_TO_ROOTLABEL['e'] , color=ROOT.kRed)    , 'LP')
  gr.Add(fmt_obs(df['h1'], TT_TO_ROOTLABEL['h1'], color=ROOT.kBlue)   , 'LP')
  gr.Add(fmt_obs(df['h3'], TT_TO_ROOTLABEL['h3'], color=ROOT.kMagenta), 'LP')
  gr.Add(fmt_obs(df['mu'], TT_TO_ROOTLABEL['mu'], color=ROOT.kGreen+3), 'LP')

  gr.Add(fmt_exp(df2     , 'Simultaneous'       , color=ROOT.kBlack)  , 'LP')
  gr.Add(fmt_exp(df['e'] , TT_TO_ROOTLABEL['e'] , color=ROOT.kRed)    , 'LP')
  gr.Add(fmt_exp(df['h1'], TT_TO_ROOTLABEL['h1'], color=ROOT.kBlue)   , 'LP')
  gr.Add(fmt_exp(df['h3'], TT_TO_ROOTLABEL['h3'], color=ROOT.kMagenta), 'LP')
  gr.Add(fmt_exp(df['mu'], TT_TO_ROOTLABEL['mu'], color=ROOT.kGreen+3), 'LP')

  gr.Draw('AP')
  gr.minimum = 1
  gr.maximum = 100
  gr.xaxis.title      = '#it{m_{H}} [GeV/#it{c}^{2}]'
  gr.yaxis.title      = POI_TO_ROOTLABELS['cscbr']
  gr.xaxis.limits     = 25, 205
  gr.xaxis.ndivisions = 209, False

  ## Legend
  leg = ROOT.TLegend()
  leg.ConvertNDCtoPad()
  leg.AddEntry(gr.graphs[0], '', 'LP')
  leg.AddEntry(gr.graphs[1], '', 'LP')
  leg.AddEntry(gr.graphs[2], '', 'LP')
  leg.AddEntry(gr.graphs[3], '', 'LP')
  leg.AddEntry(gr.graphs[4], '', 'LP')
  
  leg.ownership = False
  leg.fillStyle = 0
  leg.textFont  = 132
  leg.textSize  = 0.05
  leg.x1NDC  = 0.15
  leg.x2NDC  = 0.53
  leg.y1NDC  = 0.20
  leg.y2NDC  = 0.44
  leg.Draw()  

  ## Legend
  leg = ROOT.TLegend()
  leg.ConvertNDCtoPad()
  leg.AddEntry(gr.graphs[5], '', 'LP')
  leg.AddEntry(gr.graphs[6], '', 'LP')
  leg.AddEntry(gr.graphs[7], '', 'LP')
  leg.AddEntry(gr.graphs[8], '', 'LP')
  leg.AddEntry(gr.graphs[9], '', 'LP')
  
  leg.ownership = False
  # leg.fillStyle = 0
  leg.textFont  = 132
  leg.textSize  = 0.05
  # leg.header    = 'LHCb #sqrt{#it{s}} = 8 TeV \n#n'
  leg.x1NDC  = 0.24
  leg.x2NDC  = 0.62
  leg.y1NDC  = 0.20
  leg.y2NDC  = 0.44
  leg.Draw()

  ## 
  tt = ROOT.TText(30, 6., "observed   expected")
  tt.ownership = False
  tt.textSize  = 0.035
  tt.Draw()

  tt = ROOT.TLatex(30, 8., 'LHCb #sqrt{#it{s}} = 8 TeV')
  tt.ownership = False
  tt.textSize  = 0.048
  tt.Draw()

  ## tune and finally return
  c.logy = True
  c.RedrawAxis()

#===============================================================================
# BESTFIT CSCBR
#===============================================================================

@gpad_save
def draw_bestfit_4pi():
  """
  Draw bestfit of cscbr, simple version of only one regime.
  Note: Use the asymmetric vaule directly from cache.
  """
  df = bestfit_asym()[['4pi']]
  gr, _ = plot_bestfit(df)
  gr.yaxis.title = POI_TO_ROOTLABELS['cscbr']
  gr.minimum = -30
  gr.maximum = +30

@gpad_save
def draw_bestfit_lhcb():
  df = bestfit_asym()[['lhcb']]
  gr, _ = plot_bestfit(df)
  gr.yaxis.title = POI_TO_ROOTLABELS['cscbr_lhcb']
  gr.minimum = -1
  gr.maximum = +1


@gpad_save
def draw_bestfit_detailed():
  """
  Detailed version where all regimes are shown, for appendix
  """
  ## Grab data
  df = load_bestfit.load_asym_cscbr()['4pi'].xs(FINAL_KERNEL, level='kernel')
  df = df.unstack('regime')[['lowmass', 'nominal', 'tight']] # keep only canonical regimes
  df = df.rename(columns=REGIME_TO_LATEX)

  ## Draw
  gr, leg = plot_bestfit(df)
  gr.yaxis.title = POI_TO_ROOTLABELS['cscbr']
  gr.minimum = -30
  gr.maximum = +30
  leg.y1NDC = 0.25
  leg.y2NDC = 0.60

#===============================================================================
# LATEX
#===============================================================================

def latex_4pi():
  """
  Nicely print final CLs limit to table.
  """
  df0 = upperlim_4pi()
  df  = pd.DataFrame({
    'best_pb': bestfit_asym()['4pi'],
    'exp_pb' : df0['exp_asym'],
    'obs_pb' : df0['obs'],
  })
  return X_to_latex(df).fmt2l.to_markdown(stralign='right')

def latex_lhcb():
  df0 = upperlim_lhcb()
  df  = pd.DataFrame({
    'best_fb': bestfit_asym()['lhcb'],
    'exp_fb' : df0['exp_asym'],
    'obs_fb' : df0['obs'],
  })
  return X_to_latex(df*100.).fmt2l.to_markdown(stralign='right')

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
  ROOT.gROOT.batch = True
  if '--redraw' in sys.argv:
    for n in xrange(2, 9+1):
      draw_model_CLs(n)
    draw_ulim_4pi()
    draw_ulim_lhcb()
    draw_bestfit_4pi()
    draw_bestfit_lhcb()
    draw_bestfit_detailed()
    sys.exit()

  ## DEV
  # print upperlim_4pi()
  # print upperlim_lhcb()
  # print bestfit_asym()

  ## Drawer
  # draw_model_CLs(2)
  # draw_ulim_4pi()
  # draw_ulim_4pi_extra()
  draw_ulim_4pi_extra2()
  # draw_ulim_lhcb()
  # draw_bestfit_4pi()
  # draw_bestfit_lhcb()
  # draw_bestfit_detailed()

  ## Latex
  # print latex_4pi()
  # print latex_lhcb()
