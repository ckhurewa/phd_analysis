#!/usr/bin/env python

import load_bestfit
from lim_utils import (
  sys, ROOT, REGIMES, TT_TO_ROOTLABEL, gpad_save, plot_bestfit, X_to_latex
)

#-------------------------------------------------------------------------------

NSIG_RANGE = {
  'lowmass': (-40, 100),
  'nominal': (-10, 50),
  'tight'  : (-5 , 15),
}

@gpad_save
def draw_nsig(regime):
  """
  Draw the nsig of 3 regimes. 
  Draw each regime in separate canvas, compare channels in same canvas.
  Tuned for LHCb report.
  """  
  df = load_bestfit.load_asym_nsig().loc[regime].rename(columns=TT_TO_ROOTLABEL)
  gr, leg = plot_bestfit(df)
  gr.minimum, gr.maximum = NSIG_RANGE[regime]

@gpad_save
def draw_nsig_fommax():
  df = load_bestfit.load_asym_nsig_fommax().rename(columns=TT_TO_ROOTLABEL)
  gr, leg = plot_bestfit(df)
  gr.minimum, gr.maximum = -20, 40

#===============================================================================

def latex_bestfit():
  """
  Return the table of expected nsig from the extended MLL fitting.
  """
  df = load_bestfit.load_asym_nsig()
  return X_to_latex(df).fmt2l.to_markdown(stralign='right')

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv:
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
    ROOT.gROOT.batch = True
    for rg in REGIMES:
      draw_nsig(rg)
    draw_nsig_fommax()
    sys.exit()

  ## DEV
  # print load_bestfit.load_asym_nsig()
  # print latex_bestfit()
