#!/usr/bin/env python

from PyrootCK import *
from lim_utils import open_model_files, is_Simultaneous, create_clean_dir

## Fine tune number of bins by regime
XBINS = {
  'lowmass': 90,
  'nominal': 36,
  'tight'  : 18,
}

#===============================================================================
# Mass Plot
#===============================================================================

def _draw_mass_fit(target, obs, data, epdf_BKG, epdf_SIG, epdf_SB):
  ## tune number of bins
  for k,v in XBINS.iteritems():
    if k in target:
      obs.bins = v

  ## Frame
  c = ROOT.TCanvas()
  frame = obs.frame()

  ## Data. Do this first to get nice ylabel
  data.plotOn(frame)

  ## BKG & SIG
  nmz = (1.0, ROOT.RooAbsReal.RelativeExpected)
  epdf_BKG.plotOn(frame, Normalization=nmz, LineColor=ROOT.kRed)
  epdf_SIG.plotOn(frame, Normalization=nmz, LineColor=ROOT.kBlue , LineStyle=2)
  epdf_SB.plotOn (frame, Normalization=nmz, LineColor=ROOT.kBlack, LineStyle=2)

  ## 1st-pass Draw
  frame.minimum = frame.maximum * -0.05 # relative
  frame.XTitle = 'Mass [GeV/c^{2}]'
  frame.Draw()

  ## Hack to suppress empty data bin, do after actual draw call
  gr = ROOT.gPad.FindObject('h_'+data.name)
  for i in xrange(gr.n):
    if gr.Y[i] == 0:
      gr.SetPointEYhigh(i, 0)

  ## Legends. the true name is deduced from RooFit routine.
  leg = ROOT.TLegend(0.65, 0.65, 0.95, 0.95)
  leg.AddEntry(data                       , 'Data'           , 'PE')
  leg.AddEntry(epdf_SIG.name+'_Norm[mass]', 'Fit: Signal'    , 'L')
  leg.AddEntry(epdf_BKG.name+'_Norm[mass]', 'Fit: Background', 'L')
  leg.AddEntry(epdf_SB.name +'_Norm[mass]', 'Fit: Total'     , 'L')
  leg.Draw()

  ## Save normal + log
  ROOT.gPad.RedrawAxis()
  ROOT.gPad.SaveAs(target)

  ## 2nd-pass draw, log scale
  frame.SetMinimum(7e-2)
  frame.Draw()
  leg.Draw()
  ROOT.gPad.SetLogy(True)
  ROOT.gPad.RedrawAxis()
  ROOT.gPad.SaveAs(target.replace('mass', 'logmass'))

#-------------------------------------------------------------------------------

def draw_mass_fit(n):
  """
  Draw all mass fit plot from given model, 
  into the subdirectory of respective model.
  """
  ## list of core items
  core_pdfs = 'epdf_SB', 'epdf_SIG', 'epdf_BKG'

  ## Make sure of the clean subdir
  for dname in ['mass', 'logmass']:
    create_clean_dir(os.path.join('model%i'%n, dname))

  ## Open the file & loop
  for fin in open_model_files(n):
    for key in fin.keys: 
      ## skip because 4pi=lhcb
      if 'lhcb' in key.name:
        continue
      ## pickup
      if key.name.startswith('mll_'):
        w = key.ReadObj()
        obs = w.var('mass')
        dat0 = w.data('DATA')
        if is_Simultaneous(n):
          subdats = {ds.name:ds for ds in dat0.split(w.cat('ttypes'))}
        ## Loop for each cat
        for prefix in ['', 'e_', 'h1_', 'h3_', 'mu_']:
          pdfs = {pdf:w.pdf(prefix+pdf) for pdf in core_pdfs}
          ## Make sure that epdf_(SIG,BKG,SB) trio is available
          if all(pdf for pdf in pdfs.values()):
            ## depends on prefix & model ID, find appropriate data
            data = dat0 if not prefix else subdats[prefix.replace('_','')]
            if not data:
              w.Print()
              raise KeyError(dname)
            ## Ready, draw
            target = 'model%i/mass/mass_%s.pdf'%(n, prefix+w.name)
            _draw_mass_fit(target, obs, data, **pdfs)

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  if '--redraw' in sys.argv:
    for n in [2, 3, 4, 5, 6, 8, 11, 12]:
      draw_mass_fit(n)
    sys.exit()

  ## Dev
  draw_mass_fit(6)
