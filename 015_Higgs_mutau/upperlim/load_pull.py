#!/usr/bin/env python

from lim_utils import (
  get_auto_indices, prep_index, open_model_files, 
  memorized, gpad_save, TT_TO_ROOTLABEL,
)
import collectors

import pandas as pd
from PyrootCK import *

#===============================================================================

## INCORRECT
# def pull(var):
#   """
#   Actual calculation of the pull given RooRealVar
#   """ 
#   mean = (var.max+var.min)/2
#   return (var.val-mean)/var.error

def decode(key, mass=None, regime=None, **kwargs):
  """
  Reverse-retrieve the collector's ufloat from the binding key
  """
  if key == 'std_eacc':
    raise ValueError('No need to decode.')
  if key == 'lumi':
    return collectors.lumi
  tt,key = key.split('_')
  if key == 'brtau':
    return collectors.br[tt]
  if key == 'erec':
    return collectors.erec()[tt][(regime, mass)]
  if key == 'esel':
    return collectors.esel()[tt][(regime, mass)]
  return collectors.nbkg()[tt][(regime, key)]

def stddev(v0, v, err):
  return (v-v0)/err

def pull(var, **kwargs):
  name = var.name
  if name == 'std_eacc': # standard normal distribution
    return stddev(0, var.val, var.error)
  ## retrive the input value, calc stddev(pull) based on fit error.
  v0 = decode(name, **kwargs)
  return stddev(v0.n, var.val, var.error)

@memorized
def load_model(n):
  """
  Load the raw (value,error) pair.
  """
  ## Prepare loader
  indices = get_auto_indices(n)
  poi = indices[-1]

  ## Loop over inputs
  acc = {}
  for fin in open_model_files(n):
    for key in fin.keys:
      if key.name.startswith('mllres_'):
        res = key.ReadObj()
        idx = prep_index(key)
        kwargs = dict(zip(indices[:-1], idx))
        ## Collect the pull results given the RooFitResult, skip POI.
        acc2 = {var.name:pull(var, **kwargs) for var in res.floatParsFinal() if var.name!=poi}
        acc[idx] = pd.Series(acc2)
    fin.Close()

  ## finally
  se = pd.concat(acc)
  se.index.names = indices
  return se

#===============================================================================

LABEL_BASE = {
  # Number of background
  'Ztau' : 'N_{Z#lower[0.4]{#tau#tau}}',
  'Zll'  : 'N_{Z#lower[0.4]{ll}}',
  'QCD'  : 'N_{QCD}',
  'EWK'  : 'N_{Vj}',
  'Other': 'N_{Other}',
  #
  'brtau': 'BR',
  'eacc' : 'A',
  'erec' : '#varepsilon_{rec}',
  'esel' : '#varepsilon_{sel}',
}

def label(key):
  if key == 'lumi':
    return 'L'
  s1, s2 = key.split('_')
  base = LABEL_BASE.get(s2, s2)
  sup  = '^{%s}'%s1.replace('mu', '#mu')
  return base + sup

@gpad_save
def draw_pull_125():
  ## Prep data
  se = load_model(6).xs([125, 'nominal', 'keyspdf', 'withsyst', '4pi'], level=['mass', 'regime', 'kernel', 'syst', 'geo'])
  n  = len(se.index)+2 # empty bin at margin

  ## Prep canvas
  c = ROOT.TCanvas('pull', 'pull', 960, 240)
  h = ROOT.TH1F('h', 'h', n, 0, n);
  c.ownership = False
  h.ownership = False
  YMIN, YMAX = -3, 3

  ## Fill histo
  for i, key in enumerate(se.index):
    h.xaxis.SetBinLabel(i+2, label(key))
  h.Draw('AXIS')
  h.minimum = YMIN
  h.maximum = YMAX
  h.yaxis.title = "(v' - v) / #delta_{v}"
  h.yaxis.titleOffset = 0.5
  h.xaxis.labelSize   *= 2.2
  h.yaxis.labelSize   *= 1.5
  h.xaxis.LabelsOption('v')

  ## Plot the points
  l  = [(i+1.5, val) for i,val in enumerate(se)]
  gr = ROOT.TGraphErrors.from_pair_ufloats(*zip(*l))
  gr.Draw('P')
  gr.ownership = False

  ## Some linebreak & label
  marks = [
    (9 , TT_TO_ROOTLABEL['e']),
    (17, TT_TO_ROOTLABEL['h1']),
    (24, TT_TO_ROOTLABEL['h3']),
    (25, None),
    (33, TT_TO_ROOTLABEL['mu']),
  ]
  for i,(x, text) in enumerate(marks):
    ## draw vertical linebreak
    l = ROOT.TPolyLine(2, [x,x], [YMIN,YMAX])
    l.ownership = False
    l.lineColorAlpha = 1, 0.3
    l.Draw() 
    ## draw label
    if text:
      l = ROOT.TPaveText(x-6.5, 1.5, x-1, 2.5)
      l.ownership = False
      l.AddText(text)
      # l.textFont  = 133
      l.textSize  = 0.08
      l.textAlign = 21
      l.fillColor = 0
      l.Draw('NB')

  ## finally
  c.leftMargin  = 0.08
  c.rightMargin = 0.01
  c.bottomMargin *= 1.5
  c.Update()

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  draw_pull_125()

  ## DEV
  # print decode('e_EWK')
  # print load_model(6).to_string()
  # print collectors.nbkg()
  # print collectors.erec()
  # print collectors.esel()
