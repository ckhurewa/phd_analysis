#!/usr/bin/env python
"""

Yukawa coupling derived from branching fraction

"""
# from lim_utils import (
#   pd, memorized, pickle_dataframe_nondynamic, gpad_save,
#   ufloat, ROOT,
# )

## inputs
# import prod_csc
# import upperlim
# import bestfit

from math import pi, sqrt
import branching_fraction

#===============================================================================

def coupling_mutau():
  """
  Do only 125GeV for now
  """
  gamma_sm = 4.1e-3 # 4.1 MeV
  mH = 125.

  def calc(b):
    """
    Calculate the term: sqrt(Y_mutau^2 + Y_taumu^2) = 8pi/mH * Gamma(Higgs->ll)
    """
    cp__2 = 8*pi / mH * gamma_sm / (1./b - 1)
    return cp__2**0.5 # approximately true, but not exact for exp1, exp2

  return branching_fraction.get_SM_upperlim().apply(calc)

def latex():
  fmt = '\\num{{{:.2f}e-2}}'
  return fmt.format(coupling_mutau().obs*100.)

#===============================================================================

if __name__ == '__main__':
  # print coupling_mutau()
  print latex()