#!/usr/bin/env python
"""

Helper handle to collect all normalization factors ready for fit.

"""

from PyrootCK import *
from PyrootCK.mathutils import combine_uncorrelated, combine_fully_correlated
from uncertainties import nominal_value
from uncertainties.unumpy import matrix

## first priority
sys.path.insert(0, os.path.expandvars('$DIR15/efficiencies'))
## next
sys.path.append(os.path.expandvars('$DIR15/selection/bkg_rectangular'))

## misc tools
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import (
  pd, memorized, df_clean_unan, Br_tau, Lumi, Idx, TTYPES, REGIMES, MASSES,
  X_to_latex, printer_minmax,
)

#===============================================================================
# LUMINOSITY & BRANCHING RATIO
#===============================================================================

lumi = Lumi(with_beam=False)
br   = Br_tau()

#===============================================================================
# ACCEPTANCE
#===============================================================================

@memorized
def eacc():
  import acceptance
  return acceptance.efficiencies()

#===============================================================================
# RECONSTRUCTION EFFICIENCY
#===============================================================================

## Simple version, all tt,mass
# import rec_direct
# erec = rec_direct.efficiencies()

## outside cache to shared to corr_erec
import rec_detailed

## Full calc version
@memorized
def erec():
  eff = rec_detailed.efficiencies()
  idx = eff.index.get_level_values('mass') > 40
  eff = eff[idx] # kill useless low-mass
  return eff

def corr_erec(regime, mass):
  """
  Return the correlation matrix ready for nuisance_gaussian.corr4.bind
  """
  return matrix(rec_detailed.correlation_raw(regime).loc['higgs_%i'%mass])

#===============================================================================
# SELECTION EFFICIENCY
#===============================================================================

@memorized
def esel():
  # 161130: Prelim version with all mass, tt. Still with cuts older than Ztau
  import selection
  eff = selection.efficiencies()
  idx = eff.index.get_level_values('mass') > 40
  eff = eff[idx] # kill useless low-mass
  return eff

#===============================================================================
# EXPECTED BACKGROUNDS
#===============================================================================

def nobs():
  import summary
  return summary.results().xs('OS', level='process').applymap(nominal_value).applymap(int)

def nbkg():
  import summary
  return summary.results_roofit()

def corr_nss():
  """
  Return correlation coefficient of nQCD+nVj.
  """
  import summary
  return summary.corr_nss().fillna(0.)

#===============================================================================

@memorized
@df_clean_unan
def inverse_nolumi():
  """
  Assume the uncertainty is added in quadrature: SEL, REC, ACC, BRTAU
  """
  eff = esel() * erec() # regime-dependent
  eff = eff.groupby(level=0).apply(lambda df: (df.xs(df.name)*eacc()*br))
  return eff.stack()

@memorized
@df_clean_unan
def inverse():
  """
  Provide factor X such that: N = sigma * X
  Discard low-mass here manually, be careful

  Work backward: sel -> rec -> acc -> br -> lumi
  """
  return inverse_nolumi()*lumi

def inverse_xs_nolumi(regime, tt, geo, mass):
  """
  Helper slicer for model binding
  """
  return inverse_nolumi().xs([regime, tt, geo, mass], level=['regime', 'tt', 'geo', 'mass']).values[0]

DEFAULT_ORDER = ['regime', 'geo', 'tt', 'syst', 'mass']

@memorized
@df_clean_unan
def baseline_bestfit():
  ## Get the components
  import summary
  n = summary.results_bestfit()
  x = inverse().unstack()
  ## calculate the simult-channel, based on BLUE of cscbr first
  # the channels are independent (fully uncorrelated) by nature.
  df = x.groupby(level=0).apply(lambda df: n.loc[df.name]/df.xs(df.name))
  df['simult'] = df.apply(combine_uncorrelated, axis=1)
  ## rotate to have only regime in the columns, ready for r2
  df = df.stack().unstack('regime').applymap(df_clean_unan())
  ## the regimes, however, are assumed fully correlated
  df['fullcorr'] = df.apply(combine_fully_correlated, axis=1)
  ## inject the dummy syst=withsyst column
  df = pd.concat({'withsyst': df.stack()}, names=['syst'])
  ## Finally, stack all and order in canonical order,
  ## preferably close to what used in the load_bestfit
  df = df.reorder_levels(DEFAULT_ORDER).sort_index()
  return df

def best_to_ulim(u):
  ## double syst & stat separately
  err2      = u.s**2
  err2_stat = u.get_error('stat')**2
  err2_syst = err2-err2_stat
  return max(0, u.n) + 2*(err2_stat**0.5) + 2*(err2_syst**0.5)
  ## merge syst+stat in quadrature, then double
  # return max(0, u.n) + 2*u.s

@memorized
def baseline_upperlim():
  """
  This is the "model0", where upper limit is simply delta + 2*sigma.
  Contain both statistical & systematic uncertainties.
  Calculate the cscbr.
  """
  df = baseline_bestfit().apply(best_to_ulim).unstack('regime')
  ## add also regime=minzexp
  df['minzexp'] = df.min(axis=1)
  ## add also regime=punzi
  det  = lambda m: 'lowmass' if m < 70 else 'nominal' if m < 90 else 'tight' # Best
  keys = [(det(m), m) for m in MASSES]
  col  = df.stack().unstack(['regime', 'mass']).T.loc[keys]
  df['punzi'] = col.reset_index('regime', drop=True).unstack().T
  ## Finally
  return df.stack().reorder_levels(DEFAULT_ORDER).sort_index()

@memorized
def baseline_upperlim_nsig():
  return summary.results_bestfit().applymap(best_to_ulim)

#===============================================================================
# LATEX
#===============================================================================

def prerr(u):
  """
  wrapper to extract relative error, in percent. Fallback to nan.
  """
  return u.rerr*100. if u is not None else pd.np.nan

def latex_syst_lumi():
  return pd.Series({tt:lumi.get_rerr('lumi') for tt in TTYPES}).fmt2p

def latex_syst_br():
  return br.apply(prerr).fmt2f

def latex_syst_eacc():
  """
  For simplicity, show only that of the 4pi acceptance.
  """
  return eacc().xs('4pi', level='geo').applymap(prerr).apply(printer_minmax)

def latex_syst_erec(final_regime=False):
  df = erec().copy()
  if final_regime:
    df = pd.concat([
      df.loc['lowmass'].loc[[45,55]],
      df.loc['nominal'].loc[[65,75,85]],
      df.loc['tight'].iloc[5:],
    ])
  return df.applymap(prerr).apply(printer_minmax)

def calc_syst_esel():
  """
  Return the un-collapse version here for appendix
  """
  rerr = esel().applymap(prerr)
  return pd.concat([
    rerr.loc['lowmass'].loc[[45,55,65]],
    rerr.loc['nominal'].loc[[75,85]],
    rerr.loc['tight'].iloc[5:],
  ])
  
def latex_syst_esel():
  return X_to_latex(calc_syst_esel()).fmt2f.to_markdown()

def latex_stat_uncer():
  df = nobs().loc[list(REGIMES)].applymap(lambda x: x**-0.5)
  return X_to_latex(df).fmt2p.to_markdown()

def latex_relative_uncertainties():
  """
  The final regime is already chosen here.
  """
  ## Collect
  df = pd.concat({
    'lumi'      : latex_syst_lumi(),
    'br'        : latex_syst_br(),
    'eacc'      : latex_syst_eacc(),
    'eacc_pdf'  : eacc().xs('4pi', level='geo').applymap(lambda u: u.get_rerr('pdf')*100.).apply(printer_minmax),
    'eacc_scale': eacc().xs('4pi', level='geo').applymap(lambda u: u.get_rerr('scale')*100.).apply(printer_minmax),
    'erec'      : latex_syst_erec(final_regime=True),
    'esel'      : calc_syst_esel().apply(printer_minmax).T,
  }).unstack()
  ## Reordering
  df = df.loc[['lumi', 'br', 'eacc', 'eacc_pdf', 'eacc_scale', 'erec', 'esel']]
  ## finally
  df.index.name = '[\%]'
  df = df.rename({
    'br'        : '\\BRtauX',
    'eacc_pdf'  : '$\\hookrightarrow$ PDF', 
    'eacc_scale': '$\\hookrightarrow$ Scales',
  })
  return X_to_latex(df).to_markdown()

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print br
    print eacc().fmt2p
    print erec().fmt2p
    print esel().fmt2p
    print nbkg().fmt2f
    print nobs()
    print corr_nss()
    sys.exit()

  ## cscbr
  # print inverse_nolumi()
  # print inverse()
  # print baseline_bestfit().to_string()
  # print baseline_upperlim().to_string()

  ## nsig
  # print summary.results().fmt2f
  # print summary.results_bestfit().fmt2f
  # print baseline_upperlim_nsig().fmt2f

  ## Correlations
  # print corr_nss()
  # print corr_erec('lowmass', 45)

  ## LATEX
  # print latex_syst_lumi()
  # print latex_syst_beam()
  # print latex_syst_br()
  # print latex_syst_eacc()
  # print latex_syst_erec()
  # print latex_syst_esel()
  # print latex_stat_uncer()
  # print latex_relative_uncertainties()

  # print erec()
  print latex_syst_erec(final_regime=True)
