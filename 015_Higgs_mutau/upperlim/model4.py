#!/usr/bin/env python
"""

MODEL 4
- Single channel
- Individual background
- POI = nsig

"""

import prep_workspace
import collectors
from lim_utils import (
  TTYPES, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite, tune_POI_range,
)

#===============================================================================

def prepare_model(w, tt, mass, syst):
  ## Parameter of interest  
  w.factory('nsig [1, -50, 200]')

  ## Prepare queue of processes
  q_ss, q_bkg2 = get_queues_bkg(tt, split_ss=True)

  ## Inject the collection of bkg process
  systs = nuisance_gaussian.inject(w, *q_bkg2)
  w.factoryArgs('PROD: syst_nbkg2', systs)
  nuisance_gaussian.corr2.inject(w, 'nss', *q_ss)

  ## Prepare nominal value
  q_nexp = ['{0} * {1}_pdf_{0}'.format(s, tt) for s in q_ss+q_bkg2]
  arg_epdf_sig = 'nsig * {0}_pdf_higgs_{1}'.format(tt, mass)
  w.factoryArgs('SUM: epdf_BKG', q_nexp)         # for drawing
  w.factoryArgs('SUM: epdf_SIG', [arg_epdf_sig]) # for drawing
  w.factoryArgs('SUM: epdf_SB' , q_nexp+[arg_epdf_sig])

  ## Finally, switch with/without systematic
  if syst:
    w.factory('PROD: PDF(epdf_SB, syst_nbkg2, syst_nss)')
  else:
    w.Import(w.pdf('epdf_SB'), RenameVariable=('epdf_SB', 'PDF'))

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'nsig')
  w.defineSet('OBS', 'mass')

  ## Alias to observed data
  w.data('%s_data_OS'%tt).SetName('DATA')

#===============================================================================

def bind_values(w, regime, tt, syst):
  """
  Bind the initial values
  """
  nbkgs = collectors.nbkg().loc[regime,tt]
  for key, val in nbkgs.iteritems():
    if key not in ('QCD', 'EWK'):
      nuisance_gaussian.bind(w, key, val, syst)

  ## Anti-correlated SS=QCD+EWK
  vals = {'EWK': nbkgs['EWK'], 'QCD': nbkgs['QCD']}
  corr = collectors.corr_nss().loc[regime,tt]
  nuisance_gaussian.corr2.bind(w, 'nss', vals, corr, syst)

  ## Adjust POI lower bound
  tune_POI_range(w)

#===============================================================================

def main_single(args):
  ## Load the base workspace
  args.geo = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.tt, args.mass, args.syst)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.tt, args.syst)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
