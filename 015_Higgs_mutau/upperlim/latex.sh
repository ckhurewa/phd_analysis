#!/usr/bin/env bash
# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

## Vars
PROJ=${HOME_EPFL}/Papers/007_HiggsLFV/figs/upperlim

## Discard the affected latex page
DPATH=$PROJ/../../ANA
rm -f $DPATH/08_stats.tex $DPATH/09_results.tex $DPATH/63_app_validate.tex $DPATH/64_app_results.tex 

## validation: kernel
DPATH=$PROJ/validate_kernel
mkdir -p $DPATH
for TT in e h1 h3 mu simult; do
  cp -v validate_upperlim/kernel/punzi_${TT}.pdf $DPATH/$TT.pdf 
done

## validation: regimes
DPATH=$PROJ/validate_regime
mkdir -p $DPATH
for TT in e h1 h3 mu simult; do
  cp -v validate_upperlim/regime/3FitBkgs_${TT}0.pdf $DPATH/${TT}.pdf 
  cp -v validate_upperlim/regime/2Poisson_${TT}0.pdf $DPATH/2Poisson_${TT}.pdf
done

## validation: strategy -- main
DPATH=$PROJ/validate_stg
mkdir -p $DPATH
for RG in lowmass nominal tight punzi minzexp; do
  cp -v validate_upperlim/strategy/${RG}_simult0.pdf $DPATH/${RG}.pdf
  for TT in e h1 h3 mu; do
    cp -v validate_upperlim/strategy/${RG}_${TT}0.pdf $DPATH/${RG}_${TT}.pdf
  done
done

## validation: simult
DPATH=$PROJ/validate_simult
mkdir -p $DPATH
for RG in lowmass nominal tight punzi minzexp; do
  cp -v validate_upperlim/tt/3FitBkgs_${RG}.pdf $DPATH/${RG}.pdf
  cp -v validate_upperlim/tt/2Poisson_${RG}.pdf $DPATH/2Poisson_${RG}.pdf
done

## validation: syst
DPATH=$PROJ/validate_syst
mkdir -p $DPATH
for RG in lowmass nominal tight punzi minzexp; do
  for TT in e h1 h3 mu simult; do
    cp -v validate_upperlim/syst/${RG}_${TT}.pdf $DPATH/${RG}_${TT}.pdf
  done
done
cp -v validate_upperlim/syst/punzi_simult.pdf $DPATH/simult.pdf
#
cp -v load_pull/pull_125.pdf $PROJ/pull/

## TODO: validation: kernel

## signal_yield
DPATH=$PROJ/signal_yield
mkdir -p $DPATH
cp -v signal_yield/nsig_lowmass.pdf $DPATH
cp -v signal_yield/nsig_nominal.pdf $DPATH
cp -v signal_yield/nsig_tight.pdf   $DPATH
cp -v signal_yield/nsig_fommax.pdf  $DPATH

## cscbr
DPATH=$PROJ/cscbr
mkdir -p $DPATH
cp -v cscbr/bestfit_4pi.pdf      $DPATH
cp -v cscbr/bestfit_detailed.pdf $DPATH
cp -v cscbr/bestfit_lhcb.pdf     $DPATH
cp -v cscbr/ulim_4pi.pdf         $DPATH
cp -v cscbr/ulim_lhcb.pdf        $DPATH

## CLs, model5
DPATH=$PROJ/model5
mkdir -p $DPATH
for TT in e h1 h3 mu; do
  for RG in lowmass nominal tight punzi; do
    cp -v model5/limits/limit_keyspdf_${RG}_4pi_${TT}_withsyst.pdf $DPATH/${RG}_${TT}.pdf 
  done
done

## CLs, model6
DPATH=$PROJ/model6
mkdir -p $DPATH
for RG in lowmass nominal tight punzi; do
  cp -v model6/limits/limit_keyspdf_${RG}_4pi_withsyst.pdf $DPATH/${RG}.pdf
done

## BR
DPATH=$PROJ/br
mkdir -p $DPATH
cp -v branching_fraction/bestfit.pdf  $DPATH
cp -v branching_fraction/upperlim.pdf $DPATH

## Appendix: GraphViz
DPATH=$PROJ/graphVizTree
mkdir -p $DPATH
cp -v graphVizTree/model6.pdf $DPATH

## Appendix: Compare kernel
DPATH=$PROJ/validate_kernel
mkdir -p $DPATH
cp -v prep_workspace/compare_kernel_lowmass.pdf $DPATH/lowmass.pdf
cp -v prep_workspace/compare_kernel_nominal.pdf $DPATH/nominal.pdf
cp -v prep_workspace/compare_kernel_tight.pdf   $DPATH/tight.pdf

## MLL mass fit, use only one from model12 (last one)
## Move the debug plots to subdir
DPATH=$PROJ/massfit
mkdir -p $DPATH
for KN in keyspdf histstat histpdf; do
  rm -rf $DPATH/$KN
  mkdir $DPATH/$KN
  for TT in e h1 h3 mu; do
    cp -v model12/mass/mass_${TT}_${KN}_lowmass_withsyst_45.pdf  $DPATH/${KN}/lowmass_${TT}.pdf
    cp -v model12/mass/mass_${TT}_${KN}_nominal_withsyst_85.pdf  $DPATH/${KN}/nominal_${TT}.pdf
    cp -v model12/mass/mass_${TT}_${KN}_tight_withsyst_125.pdf   $DPATH/${KN}/tight_${TT}.pdf
  done 
done

echo 'Finished successfully!'
