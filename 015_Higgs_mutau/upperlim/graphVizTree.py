#!/usr/bin/env python

from PyrootCK import *
import subprocess
import lim_utils

def export(model, name):
  """
  Take a RooAbsPdf and export to pdf file via GraphViz.
  """
  model.graphVizTree('temp.dot')
  target  = 'graphVizTree/%s.pdf'%name

  # args    = 'fdp', '-Tpdf', 'temp.dot', '-o', target
  # args    = 'fdp', '-Tpdf', 'temp.dot', '-Gsize=8,12', '-Gdpi=300', '-Gratio=fill', '-Nfontsize=48', '-o', target
  # args    = 'fdp', '-Tpdf', 'temp.dot', '-Gratio=1.4', '-Nfontsize=48', '-o', target
  args    = 'fdp', '-Tpdf', 'temp.dot', '-Gratio=1.4', '-o', target

  subprocess.call(args)
  print 'Exported:', target
  os.remove('temp.dot')

def main():
  queues = {
    # 2: 'mll_keyspdf_nominal_h1_125',
    # 3: 'mll_keyspdf_nominal_h1_125',
    # 4: 'mll_keyspdf_nominal_h1_125',
    5: 'mll_keyspdf_nominal_4pi_h1_125',
    6: 'mll_keyspdf_nominal_4pi_125',
    7: 'mll_nominal_4pi_h1_125',
    # 8: 'mll_keyspdf_nominal_125',
  }
  for n, wname in queues.iteritems():
    # for fin in lim_utils.open_model_files(n):

    ## temp workaround for model7
    if n==7:
      fin = ROOT.TFile('model7/nominal_4pi_h1_125.root')

      w = fin.Get(wname)
      if w: 
        export(w.pdf('PDF'), 'model%i'%n)

if __name__ == '__main__':
  main()