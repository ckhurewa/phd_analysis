#!/usr/bin/env python

"""

Report the case where observed limit is found larger than 2sigma expected.

"""

import re
import subprocess
from glob import glob

#===============================================================================

## Check error
try:
  print subprocess.check_output('grep Traceback slurm*', shell=True)
except subprocess.CalledProcessError as e:
  print e

try:
  print subprocess.check_output('grep slurmstepd slurm*', shell=True)
except subprocess.CalledProcessError as e:
  print e

## check stange
for fpath in glob('slurm*.out'):
  is_strange = False
  ## prelim info
  args = 'tail', '-n', '6', fpath
  dat  = subprocess.check_output(args)
  try:
    obs  = float(re.findall(r'Observed:\s+(\S+)', dat)[0])
    sig2 = float(re.findall(r'\+2sigma :\s+(\S+)', dat)[0])
  except IndexError, e:
    print fpath
    print dat
    raise e  

  ## starnge if obs is quite larger than 2sigma
  if obs > sig2*1.2: # allow margin of above
    is_strange = True

  ## Dig deeper
  if not is_strange and sig2.is_integer():
    head   = subprocess.check_output(['head', '-n', '200', fpath])
    poi    = re.findall(r'model_POI:\((\S+)\)', head)[0]
    poimax = float(re.findall(r'RooRealVar::%s = \w+\s*L\(\S+ - (\S+)\)'%poi, head)[0])
    if sig2 == poimax:
      is_strange = True

  ## Report strange
  if is_strange:
    info = subprocess.check_output(['grep', 'Namespace', fpath]).strip()

    # ## ignore some case: syst=False
    # if 'syst=False' in info:
    #   continue

    print fpath
    print info
    # print dat.strip()
