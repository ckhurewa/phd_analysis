#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Draw the PDF from workspace before continuing any calculation.

"""
import math
from PyrootCK import *
from lim_utils import (
  REGIMES, TTYPES, REGIME_TO_LATEX, KERNELS, gpad_save,
)
from prep_workspace import _load, LIST_BKGS


#===============================================================================
# DRAWERS
#===============================================================================

@gpad_save
def draw_compare_group(kernel):
  """
  Visualize the pdf as prepared in the Workspace.
  """
  ## Prepare source, loop to allow dynamic skip
  sig  = 45, 65, 85, 105, 125, 145, 165

  ## Prepare large pad
  ROOT.gStyle.titleFontSize = 0.1
  c = ROOT.TCanvas(kernel, kernel, 1440, 900)
  c.Divide(6, 4) 
  c.ownership = False

  ## iterrate each row
  for i, tt in enumerate(TTYPES):
    for j, regime in enumerate(REGIMES):
      ## Load workspace
      w = _load(kernel, regime)
      # m = w.var('mass_'+tt) # multimass
      m = w.var('mass') # single mass
      mass_s = ROOT.RooArgSet(m)

      ## Backgrounds
      c.cd(i*6 + j*2 + 1)
      title = '_'.join([regime, tt, 'bkg'])
      frame = m.frame(title=title)
      # leg   = ROOT.TLegend(0.5, 0.6, 0.7, 0.8)
      for k, tag in enumerate(LIST_BKGS):
        pdf = w.pdf(tt+'_pdf_'+tag)
        if pdf: # skip null
          pdf.title = tag
          pdf.plotOn(frame, lineColor=utils.COLORS(k), lineWidth=1, fillColor=ROOT.kWhite)
          # leg.AddEntry(pdf, tag, 'l')
      ## draw & save
      # frame.addObject(leg)
      frame.Draw()
      ROOT.gPad.BuildLegend()
      ROOT.gPad.topMargin = 0.15

      ## Signal
      c.cd(i*6 + j*2 + 2)
      title = '_'.join([regime, tt, 'sig'])
      frame = m.frame(title=title)
      for k, tag in enumerate(sig):
        pdf = w.pdf(tt+'_pdf_higgs_%i'%tag)
        if pdf and not math.isnan(pdf.getNorm(mass_s)): # skip null & bad pdf.
          pdf.plotOn(frame, lineColor=utils.COLORS(k), lineWidth=1)
      ## draw & save
      frame.Draw()
      ROOT.gPad.BuildLegend()
      ROOT.gPad.topMargin = 0.15
  ## finally
  c.cd()

def draw_compare_group_all():
  for kernel in KERNELS:
    draw_compare_group(kernel)

#===============================================================================

@gpad_save
def draw_compare_kernel(regime):
  """
  For each process, compare the kernels. This allows the validation of keyspdf.
  """
  ## prep data
  w1 = _load('keyspdf', regime)
  w2 = _load('histpdf', regime)

  ## Prep canvas
  c = ROOT.TCanvas(regime, regime, 1440, 900)
  c.ownership = False
  c.Divide(5, 4) # bkg x ttype
  ROOT.gStyle.titleFontSize = 0.1
  for i, tt in enumerate(TTYPES):
    for j, bkg in enumerate(LIST_BKGS):
      if tt=='h3' and bkg=='Zll': # skip
        continue
      c.cd(i*5 + j + 1)
      title = '_'.join([REGIME_TO_LATEX[regime], tt, bkg])
      pdf   = '_'.join([tt, 'pdf', bkg])
      # poi   = w1.var('mass_'+tt) # multimass
      poi   = w1.var('mass') # single mass
      frame = poi.frame(title=title)
      w1.pdf(pdf).plotOn(frame, lineWidth=1, lineColor=ROOT.kRed)
      w2.pdf(pdf).plotOn(frame, lineWidth=1, lineColor=ROOT.kBlue)
      frame.Draw()
      ROOT.gPad.topMargin = 0.15
  # finally
  c.cd()

def draw_compare_kernel_all():
  for regime in REGIMES:
    draw_compare_kernel(regime)

#-------------------------------------------------------------------------------

@gpad_save
def draw_compare_data():
  """
  compare just data points
  """
  ## Prep canvas
  c = ROOT.TCanvas('data', 'data', 1440, 900)
  c.ownership = False
  c.Divide(3, 4) # regimes x ttype
  ROOT.gStyle.titleFontSize = 0.1

  ## Loop over regime first (data dependency)
  for j, regime in enumerate(REGIMES):
    w = _load('keyspdf', regime)
    for i, tt in enumerate(TTYPES):
      c.cd(i*3 + j + 1)
      title = '_'.join([regime, tt])
      # frame = w.var('mass_'+tt).frame(title=title) # multimass
      frame = w.var('mass').frame(title=title) # singlemass
      w.data(tt+'_ds_OS').plotOn(frame)
      frame.Draw()
      ROOT.gPad.topMargin = 0.15
  # finally
  c.cd()

#===============================================================================

def draw_all():  
  draw_compare_group_all()
  draw_compare_kernel_all()
  draw_compare_data()

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  if '--redraw' in sys.argv:
    draw_all()
    sys.exit()

  ## Draw
  # draw_compare_group_all()
  draw_compare_kernel_all()
  # draw_compare_data()
