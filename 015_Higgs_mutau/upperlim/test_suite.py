#!/usr/bin/env python
"""

Handle the modularized statistical tests collected into suite.

"""

from PyrootCK import *
from PythonCK.decorators import report_info
from ROOT import RooStats
import math

#===============================================================================
# UTILS
#===============================================================================

def poi_rounder(x):
  """
  Helper function for nicely-rounding the POI axis.
  The step tends to be large for small number.
  """
  ## pick the smallest step that x can fit in comfortably
  steps = [0.01, 0.05, 0.1, 0.2, 0.5, 1., 2., 3., 4., 5., 10., 20., 40., 50., 100., 200., 500., 1000.]
  for step in steps:
    if x < 3*step:
      break
  res = (round(x/step)+1)*step
  logger.info('rounding: %.2f, step=%s --> %s'%(x, step, res))
  return res

#===============================================================================

@report_info
def exe_draw_duo(name, w, data, model_sb):
  """
  Simple draw of duo: data & model
  Just to cross-check the current shape.
  Note: Only valid for one observable.

  > TODO: Split compat with singlemass-multimass config
  > TODO: Support for simult singlemass plot (4 pads in model6, 12 in model10)
  """
  assert len(model_sb.observables)==1, 'Not implemented'
  # obs = model_sb.observables.find('mass_'+label)

  ## Check if is simultaneous fit
  is_simult = isinstance(model_sb.pdf, ROOT.RooSimultaneous)
  if is_simult:
    ## Divide the canvas appropriately
    c = ROOT.TCanvas()
    cat = model_sb.pdf.indexCat()
    if len(cat)==4: # model 6, 8, 9
      c.Divide(2, 2)
    elif len(cat)==12: # model 10, 11, 12
      c.Divide(4, 3)
    elif len(cat)==8: # like above, without tight regime
      c.Divide(4, 2)
    else:
      logger.warning('NotImplemented: ncat=%i'%len(cat))
      return

    ## Loop draw
    obs = model_sb.observables.first()
    for i,label in enumerate(cat):
      c.cd(i+1)
      frame = obs.frame()
      data.plotOn(frame, Cut='{0}=={0}::{1}'.format(cat.name, label))
      model_sb.pdf.plotOn(frame, Slice=(cat, label), ProjWData=(ROOT.RooArgSet(cat), data))
      frame.Draw()
    c.SaveAs('duo_%s.pdf'%name)

  else:
    frame = model_sb.observables.first().frame()
    nmz1  = data.sumEntries(), ROOT.RooAbsReal.NumEvent
    nmz2  = data.sumEntries()**0.5, ROOT.RooAbsReal.NumEvent
    w.pdf('epdf_BKG').plotOn(frame, lineColor=ROOT.kRed , Normalization=nmz1)
    w.pdf('epdf_SIG').plotOn(frame, lineColor=ROOT.kBlue, Normalization=nmz2)
    data.plotOn(frame, DataError=ROOT.RooAbsData.SumW2)
    frame.minimum = 1e-3 # remove zero hairline
    frame.Draw()
    ROOT.gPad.SaveAs('duo_%s.pdf'%name)

#===============================================================================

@report_info
def exe_profile_likelihood(name, data, model_sb, poi):
  ## example use profile likelihood calculator
  calc = RooStats.ProfileLikelihoodCalculator(data, model_sb)
  calc.SetConfidenceLevel(0.95)

  ## Calculate the interval, return the result
  intv = calc.GetInterval()
  ilo  = intv.LowerLimit(poi)
  ihi  = intv.UpperLimit(poi)
  best = intv.bestFitParameters.find(poi.name)
  print 'Interval is [%.3f, %.3f]'%(ilo, ihi)

  ## Plot
  c = ROOT.TCanvas('LLP_'+name)
  plot = ROOT.RooStats.LikelihoodIntervalPlot(intv)
  plot.SetRange(best.val-best.error*3, best.val+best.error*3) # manual range
  plot.SetMaximum(10)
  plot.SetNPoints(20) # // do not use too many points, it could become very slow for some models
  plot.Draw('tf1')  # // use option TF1 if too slow (plot.Draw("tf1")
  # plot.Draw()  # // use option TF1 if too slow (plot.Draw("tf1")

  ## check for success
  success = not math.isnan(plot.GetPlottedObject().Integral())
  if success: # skip bad one
    c.SaveAs('profileLL_%s.pdf'%name)
  else:
    logger.warning('Bad profileLL histo, skip saving.')

  ## Finally, return bool if good
  # intv.Write('intv_'+name, ROOT.TObject.kOverwrite) # got strange hadd problem...
  return intv

#===============================================================================

@report_info
def exe_mll_fitTo(name, w, data, model_sb):

  ## DUMMY: TRY EXCLUDE RANGE
  # mass = w.var('mass')
  # mass.setRange('sig1', 20, 80)
  # mass.setRange('sig2', 100, 160)

  ## MLL mass fit, then save snapshot of this workspace vars
  res = model_sb.pdf.fitTo(data, Save=True, Minos=w.set('POI'), Constrain=w.set('CONS'), Hesse=True)
  # res = model_sb.pdf.fitTo(data, Save=True, Minos=w.set('POI'), Constrain=w.set('CONS'), Hesse=True, range='fit1')
  # res = model_sb.pdf.fitTo(data, Save=True, Minos=w.set('POI'), Constrain=w.set('CONS'), Hesse=True, range='fit1,fit2')
  
  res.Print()
  if res.status()==0:
    w.Write('mll_'+w.name, ROOT.TObject.kOverwrite)
    res.Write('mllres_'+w.name, ROOT.TObject.kOverwrite)
  else:
    logger.warning("Fit failed")

#===============================================================================

def calc_frequentist(data, model_sb, model_b):
  """
  Suitable for counting experiment (Poisson). Use frequentistCalculor.
  """
  ## Setup suitable calc for toys
  # Then we create the Hypo Test calculator class. For example the frequentist
  # calculator and we can set the number of toys to use. *NB. The class takes
  # first alternate model then null model. In this case null is S+B
  calc = RooStats.FrequentistCalculator(data, model_b, model_sb)
  calc.SetToys(2000, 2000) # 1000 for null, 1000 for alt
  return calc

@report_info
def calc_asymptotic(data, model_sb, model_b):
  ## This is for the Asymptotic Calculator
  calc = RooStats.AsymptoticCalculator(data, model_b, model_sb)
  calc.SetOneSided(True) # for one-side tests (limits)
  calc.SetPrintLevel(0) # 0 for less verbose
  return calc

@report_info
def exe_asymp_hypotest(name, calc):
  ## Validate result here first. Don't continue further
  ## Note: When GetHypoTest is call, the scan will start and it will discard
  # the previous MLL values
  res = calc.GetHypoTest()
  res.Print()
  # assert res.Significance()>0, 'Hypo test failed.' # not necessary failed
  res.Write('hypotest_'+name, ROOT.TObject.kOverwrite)

@report_info
def exe_inverse_hypo_test(name, calc, model_sb, poimin=0, poimax=None, npoints=41):
  """
  Perform the invert test for upper limit from CLs
  """
  ## Setup the hypo test
  test = RooStats.HypoTestInverter(calc)
  test.SetConfidenceLevel(0.95)
  test.UseCLs(True)
  # test.SetVerbose(True)
  ## We need also to configure the ToyMCSampler and set the test statistics
  toymcs = test.GetHypoTestCalculator().GetTestStatSampler()
  ## profile likelihood test statistics
  RooStats.ProfileLikelihoodTestStat.SetAlwaysReuseNLL(True)
  profll = RooStats.ProfileLikelihoodTestStat(model_sb.pdf)
  profll.SetOneSided(True)
  # ## ratio of profile likelihood - need to pass snapshot for the alt
  # ## RatioOfProfiledLikelihoodsTestStat ropl(*sbModel.GetPdf(), *bModel.GetPdf(), bModel.GetSnapshot())
  # ## set the test statistic to use
  toymcs.SetTestStatistic(profll)

  ## for number counting (extended pdf do not need this)
  is_frequentist = (not model_sb.pdf.canBeExtended())
  if is_frequentist:
    print 'Counting pdf (non-extended).'
    toymcs.SetNEventsPerToy(1)

  ## We can run now the inverter, 2-passes is needed
  # If the range is too large, the poi can be buried under first bin
  # If the range is too small, the upper lim +sigma is trimmed
  if poimax is None:
    ## 1st-pass scan with help from recent bestfit poi
    # Try to be resonably large to cover 2*sigma
    v = calc.GetBestFitPoi().first()
    poimax = poi_rounder(max(0, v.val) + v.error*4) # disregard negative nominal
    logger.info('1st-pass: [%s, %s]'%(poimin, poimax))
    test.SetFixedScan(21, poimin, poimax)
    r = test.GetInterval()

    ## 2nd-pass: finer range
    max1 = r.GetExpectedUpperLimit(2)
    max2 = r.UpperLimit()
    if max1>0 and max2/max1 > 4: # in particular case, max2 is way too large, discard it
      poimax = poi_rounder(max1)
    else:
      poimax = poi_rounder(max(max1, max2))
    logger.info('2nd-pass: [%i, %i] via (%.1f, %.1f)'%(poimin, poimax, max1, max2))
    test.Clear()

  ## Actual run, func scan
  test.SetFixedScan(npoints, poimin, poimax)
  r = test.GetInterval()

  ## And finally we can query the result and make some plots:
  fmt = '{:>5.2f}' if poimax > 10 else '{:>5.4f}'
  print ('Observed: %s'%fmt).format(r.UpperLimit())
  print ('Expect  : %s'%fmt).format(r.GetExpectedUpperLimit(0))
  print ('-1sigma : %s'%fmt).format(r.GetExpectedUpperLimit(-1))
  print ('-2sigma : %s'%fmt).format(r.GetExpectedUpperLimit(-2))
  print ('+1sigma : %s'%fmt).format(r.GetExpectedUpperLimit(1))
  print ('+2sigma : %s'%fmt).format(r.GetExpectedUpperLimit(2))
  # calc.GetBestFitPoi().first().Print()

  ## plot now the result of the scan & save to file
  plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot", "Feldman-Cousins Interval", r)
  c = ROOT.TCanvas("HypoTestInverter Scan")
  plot.Draw("CLb 2CL") #  // plot also CLb and CLs+b
  c.SaveAs('CLs_%s.pdf'%name)
  r.Write('fit_result_'+name, ROOT.TObject.kOverwrite)

  # ## Draw the plot (valid only for toy-like calc)
  ## Note: This is only for discovery check, not exclusion
  # if is_frequentist:
  #   c2 = ROOT.TCanvas('HypoTestStats')
  #   plot = ROOT.RooStats.HypoTestPlot(r, 100)
  #   plot.Draw()
  #   c2.SaveAs('HTStats_%s.pdf'%name)

#===============================================================================

def execute(w, args):
  """
  - w: Prepared workspace with 'DATA', 'MODEL_S', 'MODEL_SB'
    It should also has a name as unique identifier

  Will run these:
  - MLL best fit
  - Upper limit by inverse Hypo test

  All test results & plots will be saved to 'output.root' of current pwd.
  This can be chdir beforehand.

  Each submodule may save the figures to the working directory.
  I delegate the role to each submodule because there can be multiple plot
  for each one.
  """
  ## In this, force batch mode for speed
  ROOT.gROOT.batch = True

  ## Print report before start
  w.Print()
  w.PrintVars()

  ## chdir to subdirectory
  caller = None
  if 'SLURM_JOB_NAME' in os.environ:
    caller = os.environ['SLURM_JOB_NAME']
  else:
    caller = os.path.abspath(sys.argv[0])
  dpath = os.path.splitext(caller)[0]
  if not os.path.exists(dpath):
    os.makedirs(dpath)
  os.chdir(dpath)
  logger.info('chdir: %s'%dpath)
  logger.info('Args : %s'%str(args))

  ## Prepare inputs
  name     = w.name
  data     = w.data('DATA')
  model_sb = w.obj('MODEL_SB')
  model_b  = w.obj('MODEL_B')
  poi      = w.set('POI').first()
  model_sb.Print()
  data.Print()

  ## additional flags
  is_counting = ('nobs' in w.set('OBS').first().name)
  is_histpdf  = any('histpdf' in s for s in sys.argv)

  ## Prepare output
  # The caller should already chdir to appropriate location.
  fout = ROOT.TFile(name+'.root', 'recreate')

  #-------------------------#
  # Execute the test suites #
  #-------------------------#

  ## Profile likelihood
  if is_counting: # only works for Poisson counting
   if args.tt is not None: # doesn't work in correlated simulted fit
      exe_profile_likelihood(name, data, model_sb, poi) 
  
  ## Prelim draw of signal+bkg PDFs.
  if not is_counting and not args.skip_drawduo:
    exe_draw_duo(name, w, data, model_sb)
  
  ## Maximum likelihood fit
  # if not is_histpdf: # not working in Poisson, histpdf
  # same as exe_profile, but more intensive. Also has the full model for graphViz
  exe_mll_fitTo(name, w, data, model_sb) 

  ## Hypotest inverter
  if not args.skip_hypoinv:
    ## Precheck with asymp hypotest
    calc = calc_asymptotic(data, model_sb, model_b)
    exe_asymp_hypotest(name, calc)
    ## Run actual hypo test
    exe_inverse_hypo_test(name, calc, model_sb, 
      poimin=args.poimin, poimax=args.poimax, npoints=args.npoints)

  ## Finally
  fout.Close()

#===============================================================================
