#!/usr/bin/env python
"""

Provide common utilities for upper limit determination.

"""

import os
import sys
import ROOT

## Load tool from parent dir
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15'))
import HMT_utils
from HMT_utils import *
import shutil

## Suppress RooFit banner, for LaTeX-pandoc compat
ROOT.gEnv.SetValue("RooFit.Banner", "OFF")
from ROOT import RooFit
# trigger_RooFit_patch() # apply patch by default

## Misc
import argparse
from glob import glob

## Handles
import nuisance_gaussian
import test_suite

## List of available kernels
KERNELS_ALL = 'keyspdf', 'histpdf', 'histstat', 'equiprob'
KERNELS     = 'keyspdf', 'histpdf', 'histstat', # production-only

## List of available geometrical acceptance
GEOS = '4pi', 'lhcb'

## Choice of regime. Dynamic, not pickled
# FINAL_REGIME = 'punzi'
FINAL_REGIME   = 'manual' # based on Punzi
FINAL_KERNEL   = 'keyspdf'
FINAL_STRATEGY = '3FitBkgs'

## Use for the simult-fit without muon
TTYPES_NOMU = 'e', 'h1', 'h3'

## Misc labels
POI_TO_ROOTLABELS = {
  'nsig'      : 'Number of signal',
  # 'cscbr'     : '#it{#sigma_{gg#rightarrow H#rightarrow#mu^{#pm}#tau^{#mp}}} [pb]',
  # 'cscbr_lhcb': '#it{#sigma_{gg#rightarrow H#rightarrow#mu^{#pm}#tau^{#mp}}(2 < #eta_{#mu,#tau} < 4.5)} [pb]',
  'cscbr'     : '#it{#sigma(gg#rightarrow H#rightarrow#mu^{#pm}#tau^{#mp})} [pb]',
  'cscbr_lhcb': '#it{#sigma(gg#rightarrow H#rightarrow#mu^{#pm}#tau^{#mp})(2<#eta_{#mu,#tau}<4.5)} [pb]',
  'brhmt'     : 'BR(#it{H#rightarrow#mu#tau}) [%]',
}

#===============================================================================

def get_POI(n):
  """
  Helper method to return correct POI as a function of model.
  """
  if n in (1,2,4):
    return 'nsig'
  if n in (10,11,12):
    return 'brhmt'
  return 'cscbr'

def is_Poisson(n):
  return n in (1,7,9,10)

def is_Simultaneous(n):
  return n in (6,8,9, 10,11,12)

def get_auto_indices(n):
  """
  Helper method to return the default indices as a function of model.
  The order is the same as prep_workspace._load
  """
  idx = ['kernel', 'regime', 'geo', 'tt', 'syst', 'mass', get_POI(n)]
  if is_Simultaneous(n): # simult-fit over channels, no tt
    idx.remove('tt')
  if is_Poisson(n): # Poisson (counting), no kernel
    idx.remove('kernel')
  if n in (1,2,4): # POI=nsig, so geo is not needed.
    idx.remove('geo')
  if n in (10,11,12): # POI=br, fixed acc to 4pi
    idx.remove('geo')
  if n == 1: # The only one which has no mass dependency
    idx.remove('mass')
  return idx

def open_model_files(n):
  """
  Helper method to yield TFile from model number.
  - making sure of absolute path.
  - handle the choice of merged file or staged files during development
  """
  ## settle the directory with absolute path first
  dpath  = os.path.expandvars('$DIR15/upperlim/model%i'%n)
  logger.info('Base path: %s'%dpath)

  ## Search queues
  queues = [
    os.path.join(dpath, 'output.root'),      # prefered merged file for speed
    os.path.join(dpath, 'output_*.root'),    # semi-merged
    os.path.join(dpath, '*.root'),           # sharded, new
    os.path.join(dpath, 'roots', '*.root'),  # sharded, collective
  ]
  for spath in queues:
    fpaths = glob(spath)
    if fpaths: # abort search queue if found
      break

  ## Loop over queue files
  for fpath in fpaths:
    logger.info(fpath.replace(dpath, '.'))
    yield ROOT.TFile(fpath)

def create_clean_dir(dname):
  """
  Create this dir if not existed.
  If existed, delete it first.
  """
  if os.path.exists(dname):
    shutil.rmtree(dname)
  os.makedirs(dname)

#-------------------------------------------------------------------------------

def prep_index(tkey):
  """
  Convert the given ROOT.TKey for index ready for pd.DataFrame
  """
  key = tkey.name
  for kw in ('fit_result', 'mll', 'mllres', 'intv', 'hypotest'):
    key = key.replace(kw+'_', '')
  key = tuple(key.split('_'))
  return tuple(int(s) if s.isdigit() else s for s in key) # cast mass to int

def prepend_level(se, **kwargs):
  """
  Helper to prepend a multiindex level to a pd.Series.
  """
  res = se
  for key,val in sorted(kwargs.iteritems()):
    res = pd.concat([res], names=[key], keys=[val])
  return res

#===============================================================================

def get_parser():
  """
  Return new common parser, base for the fitting script
  """
  parser = argparse.ArgumentParser()
  parser.add_argument('--kernel', type=str  , default='keyspdf', choices=KERNELS_ALL+(None,))
  parser.add_argument('--regime', type=str  , default='nominal', choices=REGIMES+(None,))
  parser.add_argument('--tt'    , type=str  , default='e'      , choices=TTYPES +(None,))
  parser.add_argument('--mass'  , type=int  , default=125      , choices=MASSES )
  parser.add_argument('--geo'   , type=str  , default='4pi'    , choices=GEOS)
  #
  parser.set_defaults(syst=True)
  parser.add_argument('--with-syst', dest='syst', action='store_true')
  parser.add_argument('--no-syst'  , dest='syst', action='store_false')
  #
  parser.set_defaults(exclude_mu=False)
  parser.add_argument('--with-mu', dest='exclude_mu', action='store_false')
  parser.add_argument('--no-mu'  , dest='exclude_mu', action='store_true')
  #
  parser.add_argument('--skip-hypoinv', action='store_true')
  parser.add_argument('--skip-drawduo', action='store_true')
  #
  parser.add_argument('--poimin', type=float, default=0.)   # to override HypoTest
  parser.add_argument('--poimax', type=float, default=None) # to override HypoTest
  parser.add_argument('--cutoff', type=float, default=None, help='max POI cutoff')
  parser.add_argument('--npoints',type=int  , default=41  , help="Number of points to scan.")
  return parser

#===============================================================================

def get_queues_bkg(tt, split_ss=False):
  """
  Return the list of expected process, wired from selection module.

  Extra caveat for h3, which needs to remove Zll entry (it was needed for histo,
  but not for fitting).

  If split_ss is True, return 2 list: [qcd, vj], [the rest]
  for handling correlated
  """
  queues_expected = dict(packages.rect_selected().deck_trees_comb(tt))
  del queues_expected['OS']
  if tt == 'h3':
    del queues_expected['Zll']
  queues = queues_expected.keys()
  ## split or not
  if not split_ss:
    return queues
  queues.remove('QCD')
  queues.remove('EWK')
  # prefer EWK first, as QCD can be null
  return ['EWK', 'QCD'], queues

#===============================================================================

def create_ModelConfigSB(w, syst):
  """
  Given the workspace, create the new ModelConfig 'model_sb', 'model_b'
  based on the meta-variables defined in the workspace

  PDF : SetPdf
  POI : SetParametersOfInterest
  OBS : SetObservables
  NUI : SetNuisanceParameters
  GOBS: SetGlobalObservables
  CONS: SetConstraintParameters
  """

  ## Alias to meta vars
  POI = w.set('POI')
  poi = POI.first()

  ## Actual ModelConfig
  conf = ROOT.RooStats.ModelConfig('model', w)
  conf.SetPdf(w.pdf('PDF'))    # i.e., the likelihood function
  conf.SetParametersOfInterest(POI)
  conf.SetObservables(w.set('OBS'))
  if syst:
    conf.SetNuisanceParameters(w.set('NUI'))

  ## Signal+Background model
  model_sb = conf
  model_sb.name = 'MODEL_SB'

  ## // these are needed for the hypothesis tests
  ## PS. I'm not sure about the order here too, just following the example
  # https://twiki.cern.ch/twiki/bin/view/RooStats/RooStatsExercisesMarch2015
  model_sb.SetSnapshot(POI)
  if syst:
    conf.SetGlobalObservables(w.set('GOBS'))
    conf.SetConstraintParameters(w.set('CONS'))
  else:
    conf.SetGlobalObservables(ROOT.RooArgSet(ROOT.RooArgSet(w.set('NUI'), w.set('GOBS')), w.set('CONS')))

  # Background model, by setting signal=0
  model_b = conf.Clone()
  model_b.name = 'MODEL_B'
  oldval = poi.val
  poi.val = 0
  model_b.SetSnapshot(POI)
  poi.val = oldval # it affect the cache initialization

  ## Import model in the workspace
  w.Import(model_sb)
  w.Import(model_b)

  ## Experimental: discard unused data to have smaller save file
  for x in w.allData():
    if (x.name != 'DATA') and ('OS' not in x.name) and ('_ds_' in x.name):
    # if (x.name != 'DATA') and ('OS' not in x.name):
      # if x.name in blacklist:
        # print x.name
        x.reset()

#===============================================================================

def tune_POI_range(w):
  """
  In the extended likelihood, the minimum value of POI cannot be too small,
  otherwise the final PDF will be negative.

  The workspace must have appropriate extended PDF of signal, background.

  Note: Don't tune down the upper lim, as the CLs scan tends to occupy
  those extra region. Allow tune-up though.
  """
  obs = w.set('OBS').first()
  poi = w.set('POI').first()

  ## model1 only
  if obs.name == 'nobs':
    vmin = int(-obs.val)
    vmax = int(+obs.val)

  ## other simult counting model
  elif 'obs' in obs.name:
    logger.info('Not implemented, skip for now')
    return

  ## simult: 
  elif isinstance(w.pdf('PDF'), ROOT.RooSimultaneous):
    return # not needed

  ## For the ePDF constraints.
  else:
    x1   = w.pdf('epdf_BKG').expectedEvents(ROOT.RooArgSet())
    x2   = w.pdf('epdf_SIG').expectedEvents(ROOT.RooArgSet())
    vmin = -x1/x2 # minimum: such that sig + bkg > 0
    vmax = x1/x2  # maximum: such that sig < bkg

  ## upperlim can only increase
  vmax = max(poi.max, vmax)

  ## report & adjust
  fmt = '[{0.min:.1f}, {0.max:.1f}] --> [{1:.1f}, {2:.1f}]'
  logger.info(fmt.format(poi, vmin, vmax))
  poi.min = vmin
  poi.max = vmax

#===============================================================================

def slice_validation(poi, data):
  """
  Common filter for the validation scripts.
  """
  ## In case of comparison by KERNEL
  if poi == 'kernel':
    data = data.xs(['withsyst', '4pi', '3FitBkgs'], level=['syst', 'geo', 'strategy'])
  
  ## In case of comparison by REGIME
  if poi == 'regime':
    data = data.filter(regex='null|keyspdf').reset_index('kernel', drop=True)
    data = data.xs(['withsyst', '4pi'], level=['syst', 'geo'])

  ## In case of comparison by STRATEGY
  # Collapse kernel away, choose only 'null', 'keyspdf'. 
  if poi == 'strategy':
    data = data.filter(regex='null|keyspdf').reset_index('kernel', drop=True)
    data = data.xs(['withsyst', '4pi'], level=['syst', 'geo'])
    data = data.loc[data.index.get_level_values('regime') != 'fullcorr']

  ## In case of comparison by SYST
  if poi == 'syst':
    data = data.xs(['4pi', '3FitBkgs', 'keyspdf'], level=['geo', 'strategy', 'kernel'])

  ## In case of comparison by SYST
  if poi == 'tt':
    data = data.filter(regex='null|keyspdf').reset_index('kernel', drop=True)
    data = data.xs(['withsyst', '4pi'], level=['syst', 'geo'])

  ## Finally
  return data

#===============================================================================
# DRAWERS
#===============================================================================

def plot_brazillian(data):
  """
  Perform the "Brazillian" (upperlim with 1,2 confidence interval), 
  as a func of mass.

  data: DataFrame index by mass. 
  The columns are: exp (float), exp1 (u), exp2 (u), obs (float)
  """
  ## Use the helper constructor immediately
  c = ROOT.TCanvas()
  c.ownership = False
  g = ROOT.TMultiGraph.brazillian(data)
  g.ownership = False
  g.Draw('AP')
  g.minimum = 1e-2 # some sensible default, for log scale
  g.xaxis.title      = '#it{m_{H}} [GeV/#it{c}^{2}]'
  g.yaxis.title      = POI_TO_ROOTLABELS[data.columns.name]
  g.xaxis.limits     = 25, 205
  g.xaxis.ndivisions = 209, False

  ## Legend
  leg = c.BuildLegend()
  leg.ConvertNDCtoPad()
  leg.ownership = False
  leg.primitives.Reverse()
  leg.fillStyle = 0
  leg.textFont = 132
  leg.header = 'LHCb preliminary'
  leg.x1NDC  = 0.15 # just enough to be before 75GeV (tight)
  leg.x2NDC  = 0.50
  leg.y1NDC  = 0.20
  leg.y2NDC  = 0.44
  leg.Draw()

  ## tune and finally return
  c.logy = True
  c.RedrawAxis()
  c.Update()
  return g, leg

#-------------------------------------------------------------------------------

def plot_bestfit(df):
  """
  Wrapper around drawers.graphs_eff,
  Suitable for bestfit plot (value near zero) as a function of mH.
  """
  c = ROOT.TCanvas()
  c.ownership = False
  gr, leg = drawers.graphs_eff(df, add_options='p', nudge_step=1.)
  gr.Draw('A')
  gr.xaxis.title = 'm_{H} [GeV/c^{2}]'
  gr.yaxis.title = 'Candidates'
  gr.xaxis.labelSize = 0.12
  gr.xaxis.titleSize = 0.12
  gr.yaxis.labelSize = 0.12
  gr.yaxis.titleSize = 0.12

  ## axis
  gr.xaxis.limits     = 35, 205
  gr.xaxis.ndivisions = 17, False
  gr.minimum = -20 # sensible default
  gr.maximum = 20
  
  ## tune legend
  if leg:
    leg.Draw()
    leg.textSize = 0.1
    leg.x1NDC = 0.75
    leg.x2NDC = 0.92
    leg.y1NDC = 0.50
    leg.y2NDC = 0.92

  ## tune marker if single
  if len(df.columns)==1:
    gr[0].markerStyle = 2

  ## Some linebreak
  line = ROOT.TPolyLine( 2, [35, 205], [0, 0])
  line.ownership = False
  line.lineColorAlpha = 1, 0.3
  line.Draw()

  ## Tune canvas
  gr.yaxis.titleOffset = 0.26
  c.canvasSize   = 640, 120
  c.leftMargin   = 0.06
  c.rightMargin  = 0.02
  c.bottomMargin = 0.26
  c.topMargin    = 0.05

  ## finally
  return gr, leg
