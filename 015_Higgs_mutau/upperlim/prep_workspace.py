#!/usr/bin/env python

"""

This will prepare the trees, variables, RooDataset in advance, 
ready to be picked up from other model, as well as facilitating the loop
over large parameter space.

- Load all (2) selection regimes
- Load all 4 tau channels
- Load all Higgs masses

"""

import math
from PyrootCK import *
from lim_utils import (
  KERNELS_ALL, REGIMES, TTYPES, REGIME_TO_LATEX,
  memorized, packages, gpad_save
)

## Homogeneous list of background process
LIST_BKGS = 'Ztau', 'Zll', 'QCD', 'EWK', 'Other'

## Minimum mass axis for KeysPdf stability.
MINMASS = {
  'lowmass': {'e':20, 'h1':30, 'h3':30, 'mu':20 },
  'nominal': {'e':25, 'h1':35, 'h3':40, 'mu':25 },
  'tight'  : {'e':55, 'h1':55, 'h3':60, 'mu':30 },
}

#===============================================================================

def target(kernel, regime):
  """
  Use in both save & load stage to control which ROOT file to pickup.
  """
  assert kernel in KERNELS_ALL, "Unknown kernel: %s"%kernel
  assert regime in REGIMES    , "Unknown regime: %s"%regime
  ## use same source
  if kernel == 'histstat':
    kernel = 'histpdf'
  ## ready, load
  dpath = os.path.abspath(os.path.splitext(__file__)[0])
  fname = '{kernel}_{regime}.root'.format(**locals())
  return os.path.join(dpath, fname)

#===============================================================================

def save(kernel, regime, single_mass=True):
  """
  Fetch the selected tree and process them into Workspace, saving lots of time.
  """
  ## Define the Workspace & main observable
  w = ROOT.RooWorkspace('HMT_workspace')
  w.ownership = False

  ## Load the trees, and make into RooDataset & PDF
  # note: tree is sorted in PT, don't do naive prescale
  get_trees = packages.rect_selected().load_tree_dict_workspace
  for tt in TTYPES:

    ## ttype & regime dependent mass axis
    if single_mass:
      mmin = 20
      mmax = 200
      w.factory('mass[%i, %i, %i]'%(mmin, mmin, mmax))
      m = m0 = w.var('mass')
    else:
      mmin = MINMASS[regime][tt]
      mmax = 200
      w.factory('mass_%s[%i, %i, %i]'%(tt, mmin, mmin, mmax))
      w.factory('mass[0, %i]'%mmax) # dummy, for TTree branch
      m  = w.var('mass_'+tt)
      m0 = w.var('mass')
      m.bins = (mmax-mmin)/5  # Bin of 5GeV

    ## Loop over all trees
    for proc, tree in get_trees(tt, regime).iteritems():

      ## Import dataset from tree first
      name = tt+'_ds_'+proc
      ds   = ROOT.RooDataSet(name, name, tree, {m0})
      ds.changeObservableName(m0.name, m.name)
      w.Import(ds)

      # same PDF name across kernel
      pdfname = '{0}_pdf_{1}'.format(tt, proc)

      ## process different kernel
      # histpdf needs roodatahist first
      if kernel in ('histpdf', 'histstat'):
        dname = name.replace('_ds_','_data_')
        dh    = ROOT.RooDataHist(dname, dname, {m}, ds)
        w.Import(dh)
        w.factory('HistPdf: {0}({1},{2})'.format(pdfname, m.name, dname))

      ## keyspdf is smooth, but slow
      if kernel == 'keyspdf':
        # tune some setting
        rho = 1.2
        if proc == 'Zll':
          rho = 0.6
        if 'higgs' in proc: # extra smooth for signal
          rho = 2.0
        if ds.numEntries() > 10e3: # warn if large
          # tree->Draw("something", "Entry % 5 == 0"); // draw every 5th entry
          print 'Large incoming before KeysPdf: %s [%i]'%(proc, ds.numEntries())
        dname = '{0}_ds_{1}'.format(tt, proc)
        w.factory('KeysPdf: {0}({1},{2},MirrorLeft,{3})'.format(pdfname, m.name, dname, rho))

  ## Make combine dataset
  w.factory('ttypes[mu,e,h1,h3]') # category

  ## Depends on kernel
  if kernel == 'keyspdf':
    w.Import(ROOT.RooDataSet('ds_obs', 'combined_data', 
      ({m} if single_mass else {w.var('mass_'+tt) for tt in TTYPES}),
      ROOT.RooFit.Index(w.cat('ttypes')),
      ROOT.RooFit.Import({tt:w.data(tt+'_ds_OS') for tt in TTYPES}), # map (dict) category:dataset
    ))

  ## Finally, save to file
  w.Print()
  w.writeToFile(target(kernel, regime), True) # recreate

#-------------------------------------------------------------------------------

def save_all():
  """
  Save all trees, all channels, all mass, over all regimes, at all kernels.
  Long, feel free to take a coffee in the mean time.
  """
  for kernel in KERNELS:
    for regime in REGIMES:
      # skip if existed
      fpath = target(kernel, regime)
      if os.path.exists(fpath):
        logger.info('Skip existed: %s'%fpath)
      else:
        logger.info('Prep: %s %s'%(kernel, regime))
        save(kernel, regime)
        logger.info('Saved: %s %s'%(kernel, regime))

#===============================================================================

def load(args):
  """
  Load the workspace and rename it properly, with signature for argpase
  The rationale is that this is load to compute under new regime.
  The `args` follow central argparse namespace.
  """
  kernel = args.kernel or 'keyspdf' # can be None in model1
  syst   = 'withsyst' if args.syst else 'nosyst'
  name   = '{0.kernel}_{0.regime}_{0.geo}_{0.tt}_{1}_{0.mass}'.format(args, syst)
  name   = name.replace('_None','').replace('None_','')
  w = _load(kernel, args.regime)
  w.name = name
  return w

def _load(kernel, regime):
  """
  Load the respective Workspace from the given regime & kernel.
  """
  fin = ROOT.TFile(target(kernel, regime))
  w   = fin.Get('HMT_workspace')
  fin.ownership = False
  w.ownership   = False
  return w

#-------------------------------------------------------------------------------

REGIME_BINS = {
  'lowmass': 16,
  'nominal': 12,
  'tight'  : 8,
}

def _import_datahist(dh, w, m):
  """
  With rebinning & reduce to really trimming datahist, not condensing.
  """
  name = dh.name.replace('_dh_', '_data_')
  w.Import(ROOT.RooDataHist(name, name, {m}, dh.reduce(cutRange='all')))
  return name # to be used by RooHistPdf

def _import_histpdf(pdf, w, m):
  """
  Preproecssor for the transfer of histpdf. Mainly rebinning.
  """
  name = _import_datahist(pdf.dataHist(), w, m) # name of new DataHist
  w.factory('HistPdf: {0}({1},{2})'.format(pdf.name, m.name, name))

def load_lite(args):
  """
  Return a new Workspace derived from the base one with only necessary elements,
  to reduce size as well as being less clustered.

  Also provide seamless abstraction between kernels here.
  """
  ## Init
  w0 = load(args)
  w  = ROOT.RooWorkspace(w0.name)
  is_counting = args.mass is None
  is_simult   = args.tt is None

  ## Transfer -- all
  for attr in ('allVars', 'allCats'):
    for obj in getattr(w0, attr)():
      w.Import(obj, Silence=True)
  mass = w.var('mass') # handle

  ## Counting model doesn't need PDF/Dataset, return immediately
  if is_counting:
    return w

  # unless high mH, cap mass.max to prevent binning such that there's bin
  # with non-zero data but PDF from both signal+bkg is zero.
  # apply to all regime to remove bias
  # This is quite problematic, as it depends on channel, but also need the same
  # cut for simult fit. h3 needs narrow range, but h1 can't be too narrow...
  # Incorrect criteria
  # - skip cutoff for simult fit
  if args.cutoff:
    mass.max = args.cutoff
    logger.info('Apply manual upper mass cutoff=%i for mH=%i'%(args.cutoff,args.mass))
  elif not args.syst and args.kernel=='keyspdf' and not args.tt:
    pass # guess: skip cutoff for no-syst simult
  elif not args.syst and args.kernel=='keyspdf' and args.tt == 'h1': # h1 hates cutoff...
    pass
  else:
    for cutoff in [160, 170, 180, 190, 200]:
      if args.mass+5 <= cutoff:
        mass.max = cutoff
        logger.info('Apply upper mass cutoff=%i for mH=%i'%(cutoff,args.mass))
        break
  mass.setRange('all', 20, mass.max)

  ## Rebinning var  
  ## static bin of 10GeV
  if args.kernel == 'histpdf':
    nbin = mass.bins = int((mass.max-mass.min)/10)
    width = 1.*(mass.max-mass.min)/nbin
    logger.info('Adjust bin = %i, width = %.2f'%(nbin,width))

  ## bins varies by statistical size (~regime)
  if args.kernel == 'histstat': 
    nbin = mass.bins = REGIME_BINS[args.regime]
    # mass.bins = 9 # [20,200], 20GeV per bin
    # mass.bins = 18 # 10GeV
    # mass.bins = 36 # 5GeV
    width = 1.*(mass.max-mass.min)/nbin
    logger.info('Adjust bin = %i, width = %.2f'%(nbin,width))

  ## PDF: Only relevant channel & higgs mass
  for obj in w0.allPdfs():
    # discard irrelevant Higgs first
    if ('higgs' in obj.name) and ('higgs_%i'%args.mass not in obj.name):
      continue
    # discard irrelevant channels
    if args.tt is not None and not obj.name.startswith(args.tt):
      continue
    # apply preprocessing if histpdf
    if args.kernel in ('histpdf', 'histstat'):
      _import_histpdf(obj, w, mass)
    elif args.kernel in ('equiprob', 'keyspdf'): # ready
      w.Import(obj)

  ## Data: Keep only observed
  if args.kernel == 'keyspdf': # histpdf already done above, no more need
    for obj in w0.allData():
      if is_simult:
        # if obj.name not in ('ds_obs', 'data_obs'): # compat layer
        if not any(obj.name.endswith(s) for s in ('_obs','_OS')):
          continue
      else: # channelwise: kill other channels
        if not obj.name.startswith(args.tt):
          continue
      ## rename and import
      obj.name  = obj.name.replace ('ds_', 'data_')
      obj.title = obj.title.replace('ds_', 'data_')
      w.Import(obj)

  ## Make new combined datahist to bind with transfered vals.
  if is_simult and args.kernel in ('histpdf', 'histstat', 'equiprob'):
    w.Import(ROOT.RooDataHist('data_obs', 'combined_data', [mass],
      ROOT.RooFit.Index(w.cat('ttypes')),
      ROOT.RooFit.Import('e' , w.data('e_data_OS')),
      ROOT.RooFit.Import('h1', w.data('h1_data_OS')),
      ROOT.RooFit.Import('h3', w.data('h3_data_OS')),
      ROOT.RooFit.Import('mu', w.data('mu_data_OS')),
    ))

  # ## New category without muon
  # w.factory('ttypes_nomu[e,h1,h3]') # category
  # ## New nomu dataset, all kernel
  # cls = ROOT.RooDataSet if args.kernel=='keyspdf' else ROOT.RooDataHist
  # w.Import(cls('data_nomu_obs', 'combined_data', {mass},
  #   ROOT.RooFit.Index(w.cat('ttypes_nomu')),
  #   ROOT.RooFit.Import('e' , w.data('e_data_OS')),
  #   ROOT.RooFit.Import('h1', w.data('h1_data_OS')),
  #   ROOT.RooFit.Import('h3', w.data('h3_data_OS')),
  # ))

  ## Finally
  return w

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  if '--recalc' in sys.argv:
    save_all()
    draw_all()
    sys.exit()

  ## DEV
  # save('histpdf', 'lowmass')
  # save('histpdf', 'nominal')
  # save('histpdf', 'tight')
  # load('nominal', 'keyspdf').Print()
