#!/usr/bin/env python
"""

MODEL 7
- Single channel
- Simple counting experiment (Poisson)
- POI = cscbr

"""

import prep_workspace
import collectors
from lim_utils import (
  ROOT, TTYPES, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite
)

#===============================================================================

def prepare_model(w, syst):
  ## Parameter of interest & observable
  w.factory('cscbr [1, -100, 1500]') # in picobarn
  w.factory('nobs  [0]')

  ## Define nsig <-- cscbr
  nuisance_gaussian.inject(w, 'lumi', 'mult')

  # Then, define the actual product Nsig = ...
  w.factory('prod: nsig (cscbr, lumi, mult)')

  ## Simple Poisson counting model with Gaussian nbkg
  nuisance_gaussian.inject(w, 'nbkg')
  w.factory('sum    : nexp    (nsig, nbkg)')
  w.factory('Poisson: nominal (nobs, nexp)')

  ## Finally, switch with/without systematic
  if syst:
    w.factory('PROD:PDF(nominal, syst_lumi, syst_mult, syst_nbkg)')
  else:
    w.Import(w.pdf('nominal'), RenameVariable=('nominal', 'PDF'))

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'cscbr')
  w.defineSet('OBS', 'nobs')

#===============================================================================

def bind_values(w, regime, tt, geo, mass, syst):
  ## Number of background, as Gaussian nuisance param
  import model1
  model1.bind_values(w, regime, tt, syst) # has tune_POI_range

  ## Bind multiplicative terms
  bind = lambda key, val: nuisance_gaussian.bind(w, key, val, syst)
  bind('lumi', collectors.lumi)
  bind('mult', collectors.inverse_xs_nolumi(regime=regime, tt=tt, geo=geo, mass=mass))

#===============================================================================

def main_single(args):
  ## Load the base workspace
  args.kernel = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.syst)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.tt, args.geo, args.mass, args.syst)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
