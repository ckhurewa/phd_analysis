#!/usr/bin/env bash

# ## STRONG Recalculation protocol, only when the selection changes.

# ## clear cache
# rm prep_workspace/*.pdf
# rm prep_workspace/*.root

# ## init preparation, then full loop run
# ./collectors.py --recalc        # make sure the items are ready
# ./prep_workspace.py --recalc
# ./looper.py --recalc
