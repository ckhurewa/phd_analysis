#!/usr/bin/env python
"""

MODEL 12
- Combined channel
- Individual background
- POI = brhmt

"""

import prep_workspace
import collectors
import prod_csc
from lim_utils import (
  TTYPES, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite,
)

#===============================================================================

def prepare_model(w, mass, syst):
  ## Parameter of interest
  w.factory('brhmt [0.01, -2., 10]') # branching fraction Higgs -> mu tau

  ## Define nsig <-- brhmt * lumi * csc * tt_mult
  q0_mult = 'brtau', 'eacc', 'erec', 'esel'

  # 1. non-channel-dependent quantity
  nuisance_gaussian.inject(w, 'lumi', 'csc')

  # 2. channel-dependent correlated mult
  nuisance_gaussian.fullcorr4.inject(w, 'eacc', *TTYPES)
  nuisance_gaussian.corr4.inject    (w, 'erec', *TTYPES)

  ## Start looping over all channel
  for tt in TTYPES:

    ## 3. channel-dependent uncorrelated mult
    nuisance_gaussian.inject(w, tt+'_brtau', tt+'_esel')

    # Then, define the actual product: tt_nsig = brhmt * lumi * csc * tt_mult
    q_nsig = ['brhmt','lumi','csc'] + [tt+'_'+s for s in q0_mult]
    w.factoryArgs('prod: %s_nsig'%tt, q_nsig)

    ## Inject expected candidates
    q_ss, q_bkg2 = get_queues_bkg(tt, split_ss=True)
    q_nbkg = [ tt+'_'+x for x in q_bkg2 ]
    systs  = nuisance_gaussian.inject(w, *q_nbkg)
    w.factoryArgs('PROD: syst_%s_bkg2'%tt, systs)
    q_nss  = [ tt+'_'+x for x in q_ss]
    nuisance_gaussian.corr2.inject(w, tt+'_nss', *q_nss)

    ## Once I have all pdf, repare the number of expected candidate term. 
    ## SB = SIG + BKG. Enforce the consistent naming for automatic drawing
    q_nexp = ['{0}_{1} * {0}_pdf_{1}'.format(tt,bkg) for bkg in q_ss+q_bkg2]
    arg_epdf_sig = '{0}_nsig * {0}_pdf_higgs_{1}'.format(tt, mass)
    w.factoryArgs('SUM: %s_epdf_BKG'%tt, q_nexp)         # for drawing
    w.factoryArgs('SUM: %s_epdf_SIG'%tt, [arg_epdf_sig]) # for drawing
    w.factoryArgs('SUM: %s_epdf_SB'%tt , q_nexp+[arg_epdf_sig])

    ## Finally, the cscbr model with all gaussian constraints
    arg = 'PROD: {0}_model ({0}_epdf_SB, syst_lumi, syst_csc, syst_eacc, syst_erec, syst_{0}_brtau, syst_{0}_esel, syst_{0}_bkg2, syst_{0}_nss)'
    w.factory(arg.format(tt))

  ## Make the simultaneous fit model
  if syst:
    w.factory('SIMUL::PDF(ttypes, mu=mu_model, e=e_model, h1=h1_model, h3=h3_model)')
  else:
    w.factory('SIMUL::PDF(ttypes, mu=mu_epdf_SB, e=e_epdf_SB, h1=h1_epdf_SB, h3=h3_epdf_SB)')

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'brhmt')
  w.defineSet('OBS', 'mass' )

  ## Alias to observed data
  w.data('data_obs').SetName('DATA')

#===============================================================================

def bind_values(w, regime, geo, mass, syst):
  bind    = lambda key, val: nuisance_gaussian.bind(w, key, val, syst)
  list_ss = 'EWK', 'QCD'

  ## 1. non-channel-dependent quantity
  bind('lumi', collectors.lumi)
  bind('csc' , prod_csc.values().loc[mass, 'csc'])

  ## 2. channel-dependent correlated mult
  nuisance_gaussian.fullcorr4.bind(w, 'eacc', collectors.eacc().loc[(geo,mass)], syst)
  nuisance_gaussian.corr4.bind    (w, 'erec', collectors.erec().loc[(regime,mass)], collectors.corr_erec(regime,mass), syst)

  ## 3. channel-dependent uncorrelated
  for tt in TTYPES:

    ## Bind backgrounds
    nbkgs = collectors.nbkg().loc[regime,tt]
    for key, val in nbkgs.iteritems():
      if key not in list_ss:
        bind(tt+'_'+key, val)

    ## Bind other mult
    bind(tt+'_brtau', collectors.br[tt])
    bind(tt+'_esel' , collectors.esel().loc[(regime,mass), tt])

    ## Anti-correlated SS=QCD+EWK
    vals = {tt+'_'+pc:nbkgs[pc] for pc in list_ss}
    corr = collectors.corr_nss().loc[regime,tt]
    nuisance_gaussian.corr2.bind(w, tt+'_nss', vals, corr, syst)

#===============================================================================

def main_single(args):
  ## Load the base workspace
  args.geo = args.tt = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.mass, args.syst)

  ## bind the rest of scalar values
  bind_values(w, args.regime, '4pi', args.mass, args.syst)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
