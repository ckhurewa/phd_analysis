#!/usr/bin/env python
"""
Focus on loading the upper limit results and cache it.
"""

from lim_utils import *
from uncertainties import nominal_value
from collections import defaultdict

#===============================================================================
# HYPO TEST RESULT (BETA)
#===============================================================================

def load_HypoTestResult():
  """
  Still under debugging
  """
  acc = {}
  for fin in open_model_files(2):
    for key in fin.keys:
      if key.name.startswith('hypotest_'):
        res = fin.Get(key.name)
        se  = pd.Series({'CLs': res.CLs(), 'Sig': res.Significance()})
        acc[prep_index(key)] = se
    fin.Close()
  df = pd.concat(acc).unstack()
  print df.loc['keyspdf', 'nominal', '4pi']
  # print df.apply(lambda se: '-' if se.Sig>0 else False, axis=1).unstack().T


#===============================================================================
# UPPER LIMIT RESULTS - AUX
#===============================================================================

def flatten_result(r):
  """
  Flatten the CLs result to appropriate Series
  """
  name = r.title
  if not r:
    logger.error('Missing test result: '+name)
    return
  if r.ArraySize() == 0:
    logger.error('Missing result entries: '+name)
    return
  if not r.lastResult:
    logger.error('Missing test lastResult: '+name)
    return
  if r.lastResult.Significance() == 0:
    logger.warning('Failed test, skip: '+name)
    return
  e1h = r.GetExpectedUpperLimit(1)
  e2h = r.GetExpectedUpperLimit(2)
  e1l = r.GetExpectedUpperLimit(-1)
  e2l = r.GetExpectedUpperLimit(-2)
  try:
    return pd.Series({
      'obs' : r.UpperLimit(),
      'exp' : r.GetExpectedUpperLimit(0),
      'exp1': ufloat((e1h+e1l)/2, (e1h-e1l)/2),
      'exp2': ufloat((e2h+e2l)/2, (e2h-e2l)/2),
    })
  except Exception, e:
    print r.UpperLimit(), name, e1h, e2h, e1l, e2l
    print (e1h+e1l)/2, (e1h-e1l)/2
    print (e2h+e2l)/2, (e2h-e2l)/2
    raise e

def add_secondary_regimes(df0):
  """
  Centralize the strategy to merge several regimes into one,
  keeping other repr intact.
  """
  ## Find the level for regime, mass
  im = df0.index.names.index('mass')
  ir = df0.index.names.index('regime')

  ## determine free levels
  levels = [i for i,key in enumerate(df0.index.names) if key not in ('regime', 'cscbr')]

  ## Collect result from different merging strategies
  acc = defaultdict(list)
  for keys, df in df0.groupby(level=levels):
    mass = [s for s in keys if isinstance(s, int)][0] # lost track of where mass is, assume integer.

    ## Manual: [4,5][6,7,8][9,10,...]
    rg = 'lowmass' if mass < 60 else 'nominal' if mass < 90 else 'tight'
    acc['manual'].append(df.xs(rg, level=ir, drop_level=False))

    ## Punzi-minimum: ~ [45, 55, 65] + [75, 85] + [95, ...]
    rg = 'lowmass' if mass < 70 else 'nominal' if mass < 90 else 'tight'
    acc['punzi'].append(df.xs(rg, level=ir, drop_level=False))

    ## Minimum of expected limits
    if 'cscbr' in df.index.names: # not in baseline
      rg = df.xs('exp', level='cscbr').sort_values().index[0][ir]
      rg = 'nominal' if (rg=='tight' and mass<90) else rg # kill low-mass tight from minz
      acc['minzexp'].append(df.xs(rg, level=ir, drop_level=False))
    else: # baseline doesn't have exp.vs.obs, so it's equivalent here.
      rg = df.sort_values().index[0][ir]
      acc['minzexp'].append(df.xs(rg, level=ir, drop_level=False))

  ## Merge into new secondary regime, append & return
  for tag, l in sorted(acc.iteritems()):
    df  = pd.concat(l).rename(lambda s: tag if s in REGIMES else s)
    df0 = df0.append(df)
  return df0.sort_index()

#===============================================================================
# UPPER LIMIT RESULTS
#===============================================================================

@memorized
def load_model(n):
  """
  Load the fit results (HypoTestInverterResult), dynamic schema.
  """
  ## Loop over queue files
  acc = {}
  for fin in open_model_files(n):
    for key in fin.keys:
      if key.name.startswith('fit_result'):
        res = key.ReadObj()
        res.title = key.name
        l = flatten_result(res)
        if l is not None:
          acc[prep_index(key)] = l
    fin.Close()

  ## post processing
  df = pd.concat(acc)
  try:
    df.index.names = get_auto_indices(n)
  except ValueError, e:
    print df
    print get_auto_indices(n)
    raise e
  ## Inject the secondary regimes, only if primary is available
  if 'regime' in df.index.names and 'mass' in df.index.names:
    df = add_secondary_regimes(df) 
  return df

#-------------------------------------------------------------------------------
# CSCBR (default)
#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def _load_model_all(mode):
  """
  Collect several related models together for a comparison across models.
  1. Naive addition
  2. Poisson counting
  3. Blob-background mass fit
  4. Free-background mass fit

  mode = expected/observed

  Collect all ttypes including simult fit.
  """
  ## Load baseline result, already has secondary-regime
  import collectors # slow
  bsl = collectors.baseline_upperlim()

  ## Collect 4 channels
  se1 = pd.concat({
    '1Simple' : prepend_level(bsl[bsl.index.get_level_values('tt')!='simult'], kernel='null'),
    '2Poisson': prepend_level(load_model(7).xs(mode, level=-1), kernel='null'),
    '3FitBlob': load_model(3).xs(mode, level=-1),
    '3FitBkgs': load_model(5).xs(mode, level=-1),
  }, names=['strategy'])

  ## Collect the simult fit, usually in the separate model
  se2 = pd.concat({
    '1Simple' : prepend_level(bsl.xs('simult', level='tt'), kernel='null'),
    '2Poisson': prepend_level(load_model(9).xs(mode, level=-1), kernel='null'),
    '3FitBlob': load_model(8).xs(mode, level=-1),
    '3FitBkgs': load_model(6).xs(mode, level=-1),
  }, names=['strategy'])
  se2 = prepend_level(se2, tt='simult')

  ## Arrange indices levels in l2 to match l1
  l1 = se1.index.names
  l2 = se2.index.names
  lv = [l2.index(key) for key in l1]
  se2 = se2.reorder_levels(lv).sort_index()

  ## Finally
  se = pd.concat([se1, se2]).sort_index()
  se.name = 'cscbr'
  return se

def load_all_exp():
  return _load_model_all('exp')

def load_all_obs():
  return _load_model_all('obs')

#-------------------------------------------------------------------------------
# BRANCHING FRACTION
#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def load_br():
  se = pd.concat({
    '2Poisson':prepend_level(load_model(10), kernel='null'),
    '3FitBlob':load_model(11),
    '3FitBkgs':load_model(12),
  }, names=['strategy'])
  se.name = 'brhmt'
  return se

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv: # test run & make cache
    for i in xrange(1, 9+1):
      load_model(i)
    load_all_obs()
    load_all_exp()
    load_br()
    sys.exit()

  ## DEV
  # for i in xrange(1, 9+1):
  #   print i, load_model(i)
  # print load_model(12)
  # print load_HypoTestResult()
  # print load_baseline()
  # print load_all_exp().to_string()
  # print load_all_obs().to_string()
  # print load_br()

  ## Comparing regimes punzi-minzexp
  # print load_all_exp().loc[Idx['3FitBkgs', 'keyspdf', :, 'lhcb', 'simult', 'withsyst']].unstack('regime').fmt2f.to_string()
  # print load_br().loc[Idx['3FitBkgs', 'histpdf', :, 'withsyst', :, 'exp']].unstack('regime').fmt1p

  ## Compare kernel
  # print load_all_exp().loc[Idx['3FitBkgs', :, 'minzexp', '4pi', 'simult', 'withsyst']].unstack('kernel').fmt2f.to_string()
  # print load_br().loc[Idx['3FitBkgs', :, 'minzexp', 'withsyst', :, 'exp']].unstack('kernel').fmt1p.to_string()
