#!/usr/bin/env python

from PyrootCK import *
from PyrootCK.mathutils import covariance_matrix

#===============================================================================
# INDEPENDENT VARS
#===============================================================================

def inject(w, *names):
  """
  Add the new variable with Gaussian uncertainty.
  Return the list of names of injected Gaussian model.
  """
  for name in names:
    w.factory('{}    [0,1]'.format(name))  # nuisance, free (non-const). In Extended.
    w.factory('{}0   [0.5]'.format(name))  # const global observable.
    w.factory('{}err [0.5]'.format(name))  # const constraint.

    ## 1st-formulation: True mean is used, but unknown
    w.factory('Gaussian: syst_{0}({0}0, {0}, {0}err)'.format(name))

    ## 2nd-formulation: Probability 
    # w.factory('Gaussian: syst_{0}({0}, {0}0, {0}err)'.format(name))

    ## Inject
    _insert_NGC(w, [name], [name+'0'], [name+'err'])

  return ['syst_'+s for s in names]

#-------------------------------------------------------------------------------

def bind(w, name, val, syst=True):
  """
  Given a workspace, and a ufloat instace with associated name, binds the value
  of ufloat to the corresponding RooRealVar in the Workspace.
  """
  ## Check first if the variable existed in the workspace
  var = w.var(name)
  if not var:
    logger.info('Skip non-existing var: %r'%name)
    return
  vnuis = w.var(name)
  vgobs = w.var(name+'0')
  vcons = w.var(name+'err')

  ## Continue
  logger.info('[{:15s}] {:8.3f}'.format(name, val))
  n,s = uncertainties.nominal_value(val), uncertainties.std_dev(val)

  ## Always adjust range, careful of zero
  if (n==0 and s==0): 
    ## cannot put sigma=0 into gauss, make this const
    vnuis.setVal(0)
    vgobs.setVal(0)
    vcons.setVal(1)
    vnuis.constant = True
    vgobs.constant = True
    vcons.constant = True  
  else:
    sigma = 4
    newmax = n+sigma*s
    # newmin = n-sigma*s
    newmin = max(n-sigma*s, 0) # stable
    vnuis.setMax(newmax)
    vgobs.setMax(newmax)
    vnuis.setMin(newmin)
    vgobs.setMin(newmin)
    #
    ## Set values
    vnuis.setVal(n)
    vgobs.setVal(n)
    vcons.setVal(s)

  ## In case of no-systematic, force the floating nuisance to const
  if not syst:
    vnuis.constant = True
    vgobs.constant = True
    vcons.constant = True

#===============================================================================
# 2-CORRELATED VARIABLES
#===============================================================================

class corr2:
  """
  Handles for injecting 2 correlated variables `n1`, `n2`,
  given the correlation factor, `corr`.
  """
  ## collect the used tags, along with ordered variables
  TAGS = {}

  @classmethod
  def inject(cls, w, tag, n1, n2):
    ## Base triplet
    lnuis = []
    lgobs = []
    lcons = []
    for name in n1, n2:
      w.factory('{}    [0,1]'.format(name))
      w.factory('{}0   [0.5]'.format(name))
      w.factory('{}err [0.5]'.format(name)) # expected shifted error at binding.
      lnuis.append(name)
      lgobs.append(name+'0')
      lcons.append(name+'err')
    
    ## Regression coefficients
    w.factory('{}_reg [0]'.format(tag))
    lgobs.append('{}_reg'.format(tag))

    ## shifted mean
    w.factory('expr: {2}0_("@0+@1*(@2-@3)", {{ {2}0, {0}_reg, {1}, {1}0 }})'.format(tag,n1,n2))

    ## PDFs product
    w.factory('Gaussian: syst_{0}({0}0 , {0}, {0}err)'.format(n1))
    w.factory('Gaussian: syst_{0}({0}0_, {0}, {0}err)'.format(n2))
    w.factory('PROD    : syst_{0}(syst_{1}, syst_{2}|{1})'.format(tag,n1,n2))

    ## Finally
    cls.TAGS[tag] = n1,n2
    _insert_NGC(w, lnuis, lgobs, lcons)

  @classmethod
  def bind(cls, w, tag, vals, corr, syst=True):
    logger.info('corr2 factor: %.2f'%corr)
    n1,n2 = cls.TAGS[tag]
    val1  = vals[n1]
    val2  = vals[n2]

    ## Regression coefficients: corr * err2/err1
    reg = (corr * val2.s/val1.s) if val1.s>0 else 0.
    vreg = w.var(tag+'_reg')
    vreg.val = reg
    logger.info('corr2 regr  : %.2f'%reg)

    ## Bind, with shifted error. Old bound
    bind(w, n1, val1, syst)
    bind(w, n2, val2, syst)
    err0 = val2.s
    # skip the binding again if error is zero (default=1 is better)
    if err0 > 0:
      vname = '{}err'.format(n2)
      err   = err0 * (1-corr**2)**0.5
      w.var(vname).val = err
      logger.info('corr2 shift err: [%s] %.2f --> %.2f'%(vname, err0, err))

    ## In case of no-systematic, force the floating nuisance to const
    if not syst:
      vreg.constant = True


#===============================================================================
# 4-CORRELATED VARIABLES
#===============================================================================

class corr4:
  """
  For 4 correlated variables given the correlation matrix.

  Relies on this expansion:
    PDF(x1,x2,x3,x4) = PDF(x1) * PDF(x2|x1) * PDF(x3|x1,x2) * PDF(x4|x1,x2,x3)

  Precalculate most variable inside the "bind" method, so that inside "inject"
  it's not complicate.

  REF:
  https://en.wikipedia.org/w/index.php?title=Multivariate_normal_distribution
  """
  TAGS = {}

  @classmethod
  def inject(cls, w, tag, n1, n2, n3, n4):
    ## Tag is a suffix
    cls.TAGS[tag] = n1,n2,n3,n4
    n1 = n1+'_'+tag
    n2 = n2+'_'+tag
    n3 = n3+'_'+tag
    n4 = n4+'_'+tag
    
    ## Base triplet
    lnuis = []
    lgobs = []
    lcons = []
    for name in n1, n2, n3, n4:
      w.factory('{0}    [0,1]'.format(name))
      w.factory('{0}0   [0.5]'.format(name))
      w.factory('{0}err [0.5]'.format(name)) # expected shifted error at binding.
      lnuis.append(name)
      lgobs.append(name+'0')
      lcons.append(name+'err')

    ## Matrix (vector) of regression coefficients
    w.factory('{}_reg41 [0]'.format(tag))
    w.factory('{}_reg42 [0]'.format(tag))
    w.factory('{}_reg43 [0]'.format(tag))
    w.factory('{}_reg31 [0]'.format(tag))
    w.factory('{}_reg32 [0]'.format(tag))
    w.factory('{}_reg21 [0]'.format(tag))
    
    ## Shifted means
    w.factory('expr: {2}0_("@0+@1*(@2-@3)"                      , {{ {2}0, {0}_reg21, {1}, {1}0  }})'.format(tag,n1,n2))
    w.factory('expr: {3}0_("@0+@1*(@2-@3)+@4*(@5-@6)"           , {{ {3}0, {0}_reg31, {1}, {1}0, {0}_reg32, {2}, {2}0 }})'.format(tag,n1,n2,n3))
    w.factory('expr: {4}0_("@0+@1*(@2-@3)+@4*(@5-@6)+@7*(@8-@9)", {{ {4}0, {0}_reg41, {1}, {1}0, {0}_reg42, {2}, {2}0, {0}_reg43, {3}, {3}0 }})'.format(tag,n1,n2,n3,n4))

    ## Conditional PDFs
    w.factory('Gaussian: syst_{0}({0}0 , {0}, {0}err)'.format(n1))
    w.factory('Gaussian: syst_{0}({0}0_, {0}, {0}err)'.format(n2))
    w.factory('Gaussian: syst_{0}({0}0_, {0}, {0}err)'.format(n3))
    w.factory('Gaussian: syst_{0}({0}0_, {0}, {0}err)'.format(n4))
    w.factory('PROD    : syst_{0}(syst_{1}, syst_{2}|~{2}, syst_{3}|~{3}, syst_{4}|~{4})'.format(tag,n1,n2,n3,n4))

    ## Finally
    _insert_NGC(w, lnuis, lgobs, lcons)


  @classmethod
  def bind(cls, w, tag, vals, corr, syst=True):
    ## Report
    logger.info('corr4 matrix')
    for row in corr:
      logger.info('  '+str(row))
    names = cls.TAGS[tag]

    ## Revert correlation matrix into covariance matrix
    ln = [vals[n].n for n in names]
    ls = [vals[n].s for n in names]
    covar = covariance_matrix(ln, ls, corr.tolist())

    ## Precalculate the matrices of regression coefficient, then repack
    reg2 = covar[1,:1] * covar[:-3, :-3].I
    reg3 = covar[2,:2] * covar[:-2, :-2].I
    reg4 = covar[3,:3] * covar[:-1, :-1].I
    coeffs = {
      'reg21': reg2[0,0],
      'reg31': reg3[0,0],
      'reg32': reg3[0,1],
      'reg41': reg4[0,0],
      'reg42': reg4[0,1],
      'reg43': reg4[0,2],
    }

    ## Precalculate the shifted error due to ordered conditional PDF
    var4shift = (reg4*covar[:3,3])[0,0]
    var3shift = (reg3*covar[:2,2])[0,0]
    var2shift = (reg2*covar[:1,1])[0,0]
    errs = [
      ls[0], # noshift
      (ls[1]**2 - var2shift)**0.5,
      (ls[2]**2 - var3shift)**0.5,
      (ls[3]**2 - var4shift)**0.5,
    ]

    ## Report
    fmt = 'Shifting err [{:10s}]: {:.2f} --> {:.2f}'
    for name, err0, err in zip(names, ls, errs):
      logger.info(fmt.format(name+'_'+tag, err0, err))

    ## Ready: Bind the PDFs, conservative bound
    for name, err in zip(names, errs):
      val = vals[name]
      bind(w, name+'_'+tag, val, syst)
      w.var('{0}_{1}err'.format(name,tag)).val = err

    ## bind regression coeffs
    for key,val in coeffs.iteritems():
      w.var(tag+'_'+key).val = val

    ## In case of no-systematic, force the floating nuisance to const
    if not syst:
      for key in coeffs:
        w.var(tag+'_'+key).constant = True

#===============================================================================

class fullcorr4:
  """
  For the fully-correlated variables. Only one Normal distribution is needed.
  Then 4 "nuisance" variables are derived.
  """
  TAGS = {}

  @classmethod
  def inject(cls, w, tag, n1, n2, n3, n4):
    ## Define the standard normal distribution first
    w.factory('Gaussian: syst_{0}(std_{0}0[0], std_{0}[0,-20,20], std_{0}err[1])'.format(tag))

    ## Then, inject & define expressions
    lgobs = ['std_{}0'.format(tag)]
    lcons = ['std_{}err'.format(tag)]
    for name in n1, n2, n3, n4:
      w.factory('expr: {1}_{0}("@0*@1 + @2", {{ {1}_{0}err[0], std_{0}, {1}_{0}0[0] }})'.format(tag, name))
      lgobs.append('{1}_{0}0'.format(tag, name))
      lcons.append('{1}_{0}err'.format(tag, name))

    ## Finally
    cls.TAGS[tag] = n1,n2,n3,n4
    _insert_NGC(w, ['std_'+tag], lgobs, lcons)

  @classmethod
  def bind(cls, w, tag, vals, syst=True):
    """
    Similar to normal bind, but simpler.
    """
    for name in cls.TAGS[tag]:
      val = vals[name]
      fullname = name+'_'+tag
      w.var(fullname+'0').val = val.n
      w.var(fullname+'err').val = val.s
      logger.info('[{:15s}] {:8.3f}'.format(fullname, val))

    ## In case of no-systematic, force the floating nuisance to const
    if not syst:
      w.var('std_'+tag).constant = True
      for name in cls.TAGS[tag]:
        fullname = name+'_'+tag
        w.var(fullname+'0').constant = True
        w.var(fullname+'err').constant = True


#===============================================================================

def _insert_NGC(w, lnuis, lgobs, lcons):
  """
  Helper method to collect NuisanceParam, GlobalObservables, and
  ConstraintParameters during the injection phase.

  This implicitly defineSet 'NUI', 'GOBS', 'CONS' on the workspace,
  Ready for the `create_ModelConfigSB` aliasing.
  
  """
  for key,l in zip(['NUI', 'GOBS', 'CONS'], [lnuis, lgobs, lcons]):
    # check exists
    if not w.set(key):
      w.defineSet(key, '')
    # insert
    wset = w.set(key)
    for x in l:
      wset.add(w.var(x))


#===============================================================================
