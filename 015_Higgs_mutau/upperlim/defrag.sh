#!/usr/bin/env bash

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
# set -e

## Move the debug plots to subdirl 
for MID in 1 2 3 4 5 6 7 8 9 10 11 12; do
  MODEL=model$MID
  for TAG in CLs duo profileLL; do
    mkdir -p $MODEL/$TAG
    mv -v $MODEL/$TAG*.pdf $MODEL/$TAG
  done
  # Then the staging root files, except the main "output.root" one
  # careful not to move the output*.root files.
  mkdir -p $MODEL/roots
  mv $MODEL/*.root $MODEL/roots
  mv $MODEL/roots/output*.root $MODEL/

  ## hadd if missing
  ## model3,5,7 cannot have full output.root due to large amount
  # need a sharded version
  if [ $MODEL == "model1" ] || [ $MODEL == "model6" ] || [ $MODEL == "model8" ] || [ $MODEL == "model9" ] || [ $MODEL == "model10" ] || [ $MODEL == "model11" ] || [ $MODEL == "model12" ]; then
    if [ ! -f $MODEL/output.root ]; then
      hadd -f9 $MODEL/output.root $MODEL/roots/*.root
    fi    
  else
    for REGIME in lowmass nominal tight; do
      # call hadd only when it's missing
      if [ ! -f $MODEL/output_${REGIME}.root ]; then
        hadd $MODEL/output_${REGIME}.root $MODEL/roots/*${REGIME}*.root
      fi
    done
  fi

done
echo "Finish successfully!"
