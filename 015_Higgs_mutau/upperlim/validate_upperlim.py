#!/usr/bin/env python
"""
For plotting the upper limit validation.
"""
import os
from lim_utils import (
  ROOT, REGIMES, X_TO_ROOTLABELS, POI_TO_ROOTLABELS, 
  create_clean_dir, slice_validation
)
import HMT_utils
import load_upperlim

#===============================================================================
# DRAWER: COMPARISON
#
# Strategy: Strategy starts from simple (naive limit) to complex, providing good 
#           ground for validation. In general, the limit should be smaller as
#           the strategy utilize more informative observables.
#
# Regime: Different selection regime is good at different mass range.
#
# ttype : Each channel (e, mu, h1, h3) looks on separate dataset, until 
#         simultaneous fit utilize information from all channels for constraining.
#===============================================================================

## dynamic yaxis range depends on the POI
YRANGE = {
  'cscbr'     : (1   , 1e3),
  'cscbr_lhcb': (1e-2, 50 ),
}

def draw_comparison(df, target):
  ## common prep
  df = df.rename(columns=X_TO_ROOTLABELS)
  poi = 'cscbr_lhcb' if 'lhcb' in target else 'cscbr'
  df.name = POI_TO_ROOTLABELS[poi]

  ## Start drawing
  c = ROOT.TCanvas()
  gr, leg = HMT_utils.drawers.graphs_eff(df)
  gr.minimum, gr.maximum = YRANGE[poi]
  gr.xaxis.limits     = 25, 205
  gr.xaxis.ndivisions = 209, False
  gr.xaxis.title      = 'm_{H} [GeV/c^{2}]'
  gr.Draw('A')
  if leg:
    leg.x1NDC = 0.15
    leg.x2NDC = 0.55
    leg.y2NDC = 0.20 + len(leg.primitives)*0.05 # dynamic height
    leg.Draw()
  ROOT.gPad.logy = True
  ROOT.gPad.SaveAs(target)

#-------------------------------------------------------------------------------

ORDERED_TTYPES = 'simult', 'e', 'h1', 'h3', 'mu'

def draw_comparison_all():
  ## fetch the data
  data0 = load_upperlim.load_all_exp()

  ## Parameters to compare
  for poi in ('kernel', 'regime', 'strategy', 'syst', 'tt'):
    
    ## delete subdir if existed
    dpath = os.path.join('validate_upperlim', poi)
    create_clean_dir(dpath)

    ## Slice
    data = slice_validation(poi, data0)

    ## Loop over other levels
    lv = [i for i,key in enumerate(data.index.names) if key not in (poi, 'mass')]
    for keys, df in data.groupby(level=lv):

      ## Determine target output
      target = os.path.join(dpath, '%s.pdf'%('_'.join(keys)))

      ## Grab data
      df = df.xs(keys, level=lv).unstack().T      

      ## fine-tune reordering
      if poi == 'tt':
        df = df[[c for c in ORDERED_TTYPES if c in df.columns]]
      ## finally
      draw_comparison(df, target)

      ## additional pass for non-primary regime
      if poi == 'regime':
        target = target.replace('.pdf', '0.pdf') # for debug, keep extra copy
        df2 = df[list(REGIMES)]
        draw_comparison(df2, target)

      ## additional pass without blobbkg
      if poi == 'strategy':
        target = target.replace('.pdf', '0.pdf') # for debug, keep extra copy
        df2 = df.drop('3FitBlob', axis=1)
        draw_comparison(df2, target)

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gROOT.batch = True
  draw_comparison_all()
