#!/usr/bin/env python
"""

Handle the expected signal mass plot & tables

"""
import os
from lim_utils import (
  ROOT, HMT_utils, X_TO_ROOTLABELS, POI_TO_ROOTLABELS, 
  create_clean_dir, slice_validation,
)
import load_bestfit

#===============================================================================
# DRAWER
#===============================================================================

def draw_comparison(df, target):
  ## Prep
  df = df.rename(columns=X_TO_ROOTLABELS)
  poi = 'cscbr_lhcb' if 'lhcb' in target else 'cscbr'
  df.name = POI_TO_ROOTLABELS[poi]

  ## Start drawing
  c = ROOT.TCanvas()
  gr, leg = HMT_utils.drawers.graphs_eff(df, add_options='p', nudge_step=1.)
  gr.minimum      = -100
  gr.maximum      = 100
  gr.xaxis.limits = 40, 200
  gr.xaxis.title  = 'm_{H} [GeV/c^{2}]'
  gr.Draw('A')
  if leg:
    leg.Draw()
  c.SaveAs(target)

#-------------------------------------------------------------------------------

def draw_comparison_all():
  data0 = load_bestfit.load_model_all()

  ## Parameters to compare
  for poi in ('kernel', 'regime', 'strategy', 'syst', 'tt'):

    ## delete subdir if existed
    dpath = os.path.join('validate_bestfit', poi)
    create_clean_dir(dpath)

    ## Slice
    data = slice_validation(poi, data0)

    ## Loop over other levels
    lv = [i for i,key in enumerate(data.index.names) if key not in (poi, 'mass')]
    for keys, df in data.groupby(level=lv):

      ## Process
      target = os.path.join(dpath, '%s.pdf'%('_'.join(keys)))
      df = df.xs(keys, level=lv).unstack().T
      draw_comparison(df, target)

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gROOT.batch = True
  draw_comparison_all()
