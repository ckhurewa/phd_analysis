#!/usr/bin/env python
"""
Focus on loading the bestfit results and cache it.
"""

from lim_utils import *
from PyrootCK.mathutils import combine_fully_correlated, asymvar
from collections import defaultdict

#===============================================================================
# BEST FIT OF POI
#===============================================================================

def add_secondary_regimes(df0):
  """
  Add additional secondary regimes derived from the existing one.
  """
  ## Find the level for regime, mass
  im = df0.index.names.index('mass')
  ir = df0.index.names.index('regime')

  ## determine free levels
  levels = [i for i,key in enumerate(df0.index.names) if key not in ('regime',)]

  ## Collect result from different merging strategies
  acc = defaultdict(list)
  for keys, df in df0.groupby(level=levels):
    # # combine all measurement, assuming errors are fully correlated
    # tag = 'fullcorr'
    # val = combine_fully_correlated(df.values)
    # idx = list(df.index[0])
    # idx[ir] = tag
    # idx = pd.MultiIndex.from_tuples([idx], names=df.index.names)
    # acc[tag].append(pd.Series([val], index=idx))

    # keep only one with smallest error
    tag = 'minerr'
    val = sorted(df.values, key=lambda u: u.s)[0]
    idx = list(df.index[0])
    idx[ir] = tag
    idx = pd.MultiIndex.from_tuples([idx], names=df.index.names)
    acc[tag].append(pd.Series([val], index=idx))

    ## Punzi-minimum: ~ [45, 55, 65] + [75, 85] + [95, ...]
    tag  = 'punzi'
    mass = keys[-1]
    rg   = 'lowmass' if mass < 70 else 'nominal' if mass < 90 else 'tight'
    acc[tag].append(df.xs(rg, level=ir, drop_level=False).rename({rg:tag}))

  ## Merge into new secondary regime, append & return
  df = df0.copy()
  for _, l in sorted(acc.iteritems()):
    df = df.append(pd.concat(l))
  return df.sort_index()

#-------------------------------------------------------------------------------

@memorized
def load_model(n):
  """
  Load result of single model, for single parameter-of-interest.
  """
  ## Prepare loader
  idx = get_auto_indices(n)
  poi = idx[-1]
  loader_poisson = lambda obj: obj.bestFitParameters.find(poi).ufloat()
  loader_massfit = lambda obj: obj.var(poi).ufloat()
  tag, loader = 'mll_', loader_massfit

  ## Loop over items in the TFile
  acc = {}
  for fin in open_model_files(n):
    for key in fin.keys:
      if key.name.startswith(tag):
        res = key.ReadObj()
        key = tuple(key.name.split('_')[1:]) # discard "mll_"
        key = tuple(int(s) if s.isdigit() else s for s in key) # cast mass to int
        acc[key] = loader(res)
    fin.Close()

  ## Post processing
  se = pd.Series(acc)
  se.name = poi
  try:
    se.index.names = idx[:-1]
  except ValueError, e:
    print se.index.names
    print idx
    raise e

  ## Inject the secondary regimes, only if primary is available
  if 'regime' in se.index.names and 'mass' in se.index.names:
    se = add_secondary_regimes(se)
  return se

#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def load_model_all():
  """
  Load results from multiple models into strategies.
  """
  ## Load baseline result, already has secondary-regime
  import collectors # heavy
  bsl = collectors.baseline_bestfit()

  ## Collect 4 channels
  se1 = pd.concat({
    '1Simple' : prepend_level(bsl[bsl.index.get_level_values('tt') != 'simult'], kernel='null'),
    '2Poisson': prepend_level(load_model(7), kernel='null'),
    '3FitBlob': load_model(3),
    '3FitBkgs': load_model(5),
  }, names=['strategy'])
  l1 = se1.index.names

  ## Collect the simult fit, usually in the separate model
  se2 = pd.concat({
    '1Simple' : prepend_level(bsl.xs('simult', level='tt'), kernel='null'),
    '2Poisson': prepend_level(load_model(9), kernel='null'),
    '3FitBlob': load_model(8),
    '3FitBkgs': load_model(6),
  }, names=['strategy'])
  se2 = prepend_level(se2, tt='simult')
  l2  = se2.index.names
  lv  = [l2.index(key) for key in l1]
  se2 = se2.reorder_levels(lv).sort_index()

  ## Finally
  se = pd.concat([se1, se2]).sort_index().dropna()
  se.name = 'cscbr'
  return se

#-------------------------------------------------------------------------------
# RESULT OF ASYMMETRIC ERROR
#-------------------------------------------------------------------------------

@pickle_dataframe_nondynamic
def cache_model6_asym():
  """
  Cache the asymmetric result form model6 only.
  Save 2 POIs: cscbr (simult), nsig (from each channel)
  """
  acc = {}
  for fin in open_model_files(6):
    for key in fin.keys:
      if key.name.startswith('mllres'):
        ## grab the object
        res = key.ReadObj()
        w   = fin.Get(key.name.replace('mllres', 'mll'))

        ## grab the POI asym variable
        poi   = w.set('POI').first()
        cscbr = (poi.val, poi.errorHi, poi.errorLo)
        
        ## grab the sym-->asym correction factor
        fhi   = poi.errorHi/poi.error
        flo   = abs(poi.errorLo/poi.error)

        ## Loop over all nsig
        for k,v in w.allFunctions().iteritems():
          if not k.endswith('_nsig'):
            continue
          err  = v.getPropagatedError(res)
          nsig = (v.val, err*fhi, err*flo)
          tt   = k.split('_')[0]
          acc[prep_index(key)+(tt,)] = nsig
          acc[prep_index(key)+('cscbr',)] = cscbr
    fin.Close()

  ## finally
  df = pd.Series(acc).unstack() # unstack last column: cscbr-nsig
  df.index.names = get_auto_indices(6)[:-1]
  df = df.xs('withsyst', level='syst')
  return df


@pickle_dataframe_nondynamic
def cache_model12_asym():
  acc = {}
  for fin in open_model_files(12):
    for key in fin.keys:
      if key.name.startswith('mllres'):
        ## grab the object
        res = key.ReadObj()
        w   = fin.Get(key.name.replace('mllres', 'mll'))
        ## grab the POI asym variable, save
        poi = w.set('POI').first()
        acc[prep_index(key)] = poi.val, poi.errorHi, poi.errorLo
    fin.Close()
  ## finally
  se = pd.Series(acc)
  se.index.names = get_auto_indices(12)[:-1]
  se = se.xs('withsyst', level='syst')
  return se

#-------------------------------------------------------------------------------
# CSCBR
#-------------------------------------------------------------------------------

@memorized
def load_asym_cscbr():
  ## pick up the raw result, cast into asymvar
  get = lambda x: asymvar(*x)
  df  = cache_model6_asym()['cscbr'].apply(get).unstack(-2)
  ## Create secondary regime: minerr
  def make_minerr(df):
    return df.apply(lambda df2: sorted(df2.values, key=lambda u: u.ehigh)[0])
  res = df.groupby(level=['mass','kernel']).apply(make_minerr)
  res = prepend_level(res, regime='minerr').reorder_levels(df.index.names)
  ## finally, inject into original
  return pd.concat([df, res]).sort_index()

#-------------------------------------------------------------------------------
# NSIG
#-------------------------------------------------------------------------------

@memorized
def load_asym_nsig():
  """
  Pick up the raw result, cast into asymvar.
  Collapse kernel here.
  """
  get = lambda x: asymvar(*x)
  df  = cache_model6_asym().xs(['lhcb','keyspdf'], level=['geo','kernel'])
  return df[list(TTYPES)].applymap(get)

@memorized
def load_asym_nsig_fommax():
  """
  Collapse in regime to only one; FoM max.
  """
  df0  = load_asym_nsig() # prepare to have only (rg,mass) index
  det  = lambda m: 'lowmass' if m < 70 else 'nominal' if m < 90 else 'tight' # Best
  keys = [(det(m), m) for m in MASSES]
  return df0.loc[keys].reset_index('regime', drop=True)

#-------------------------------------------------------------------------------
# BRHMT
#-------------------------------------------------------------------------------

def load_asym_brhmt():
  ## pick up the raw result, cast into asymvar
  get  = lambda x: asymvar(*x)
  det  = lambda m: 'lowmass' if m < 70 else 'nominal' if m < 90 else 'tight' # Best
  keys = [(det(m), m) for m in MASSES]
  df   = cache_model12_asym().xs('keyspdf', level='kernel')
  return df.loc[keys].reset_index('regime', drop=True).apply(get)

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv: # test run & make cache
    for i in xrange(1, 9+1):
      load_model(i)
    load_model_all()
    cache_model6_asym()
    cache_model12_asym()
    sys.exit()
  
  ## DEV
  # for i in xrange(1, 9+1):
  #   print i, load_model(i)
  # print load_model(1).to_string()
  # print load_model(6).loc['keyspdf']
  print load_model_all().to_string()
  # print load_model_all().unstack('regime').loc[('3FitBkgs','4pi','simult')]

  ## Asymmetric result
  # print cache_model6_asym().to_string()
  # print cache_model12_asym()
  # print load_asym_cscbr().to_string()
  # print load_asym_nsig()
  # print load_asym_nsig_fommax()
  # print load_asym_brhmt()
