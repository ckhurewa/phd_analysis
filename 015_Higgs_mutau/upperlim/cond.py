#!/usr/bin/env python
"""

Testing conditional PDF for correlated uncertainties

"""

from PyrootCK import *
import nuisance_gaussian
from uncertainties import ufloat
from uncertainties.unumpy import matrix

def main1():
  """
  First attempt, just to get it works.
  """
  w = ROOT.RooWorkspace()

  w.factory('rho  [-0.5]')
  w.factory('nqcd [100, 80, 120]')
  w.factory('nvj  [20 , 10, 30 ]')
  w.factory('Gaussian: Gx   (nqcd, 100, 20)')
  w.factory("expr    : ymean('20 + 10/20*rho*(nqcd-100)', rho, nqcd)")
  w.factory("expr    : yerr ('(1 - rho**2)**0.5 * 10', rho)")
  w.factory('Gaussian: Gy   (nvj , ymean, yerr)')

  # ## direct
  # pdf1 = ROOT.RooArgSet(w.pdf('Gx'))
  # pdf2 = ROOT.RooFit.Conditional(ROOT.RooArgSet(w.pdf('Gy')), ROOT.RooArgSet(w.var('nvj')))
  # model = ROOT.RooProdPdf('model', 'model', pdf1, pdf2)
  
  ## factory
  # w.factory('PROD:model(Gx, Gy|nqcd)')
  w.factory('PROD:model(Gx, Gy|~nvj)')
  model = w.pdf('model')

  model.Print()
  h = model.createHistogram('hist', w.var('nqcd'), YVar=w.var('nvj'))
  h.Draw('colz')
  ROOT.gPad.Update()
  raw_input()

#===============================================================================

def main2():
  """
  Second attempt with standard naming in nuisance_gaussian module.
  """
  w = ROOT.RooWorkspace()
  w.factory('rho     [-0.5]')
  w.factory('nqcd    [100, 40, 160]')
  w.factory('nqcd0   [100]')
  w.factory('nqcderr [20]')
  w.factory('nvj     [20, 0, 50]')
  w.factory('nvj0    [20]')
  w.factory('nvjerr  [10]')

  w.factory('expr: nvj0__nqcd  ("@0 + @1*@2/@3*(@4-@5)", {nvj0  , rho, nvjerr, nqcderr, nqcd, nqcd0})')
  w.factory('expr: nvjerr__nqcd("@0 * ((1-@1**2)**0.5)", {nvjerr, rho})')

  w.factory('Gaussian: syst_nqcd(nqcd0     , nqcd, nqcderr)')
  w.factory('Gaussian: syst_nvj (nvj0__nqcd, nvj , nvjerr__nqcd)')
  w.factory('PROD    : model    (syst_nqcd, syst_nvj|nqcd)')

  model = w.pdf('model')
  h = model.createHistogram('hist', w.var('nqcd'), YVar=w.var('nvj'))
  h.Draw('colz')
  ROOT.gPad.Update()
  raw_input()

#===============================================================================

def main3():
  """
  Third attempt using function.
  """
  w = ROOT.RooWorkspace()

  tag   = 'nss'
  names = 'nqcd', 'nvj'
  vals  = {
    'nqcd': ufloat(67.536, 10.625), 
    'nvj' : ufloat(14.490, 10.302),
  }
  corr  = 0.9
  # names = list(reversed(names))

  ## with corr
  nuisance_gaussian.corr2.inject(w, tag, *names)
  nuisance_gaussian.corr2.bind(w, tag, vals, corr)

  # ## default (no corr)
  # nuisance_gaussian.inject(w, *names)
  # w.factory('PROD: syst_nss(syst_nqcd, syst_nvj)')
  # for name,val in zip(names, vals):
  #   nuisance_gaussian.bind(w, name, val)

  w.Print()
  w.PrintVars()

  model = w.pdf('syst_nss')
  x = w.var('nqcd')
  y = w.var('nvj')
  h = model.createHistogram('hist', x, YVar=y)
  h.Draw('colz')
  h.xaxis.title = 'N_{QCD}'
  h.yaxis.title = 'N_{Vj}'
  ROOT.gPad.rightMargin = 0.057
  ROOT.gPad.topMargin = 0.012
  ROOT.gPad.Update()

  ROOT.gPad.SaveAs('cond/fullcorr_nss.pdf')
  # raw_input()

#===============================================================================

def main4():
  """
  Going for 4 dimensional.
  """
  w = ROOT.RooWorkspace()
  ttypes = 'e', 'h1', 'h3', 'mu'
  nuisance_gaussian.corr4.inject(w, 'erec', *ttypes)
  w.Print()
  
  vals = {
    'e' : ufloat(0.6 , 0.1),
    'h1': ufloat(0.6 , 0.075),
    'h3': ufloat(0.4 , 0.09),
    'mu': ufloat(0.65, 0.05),
  }
  # dummy
  rho = -0.5
  corr_mat = matrix([
    [1, 0., 0., 0.],
    [0., 1, 0., 0.],
    [0., 0., 1, rho],
    [0., 0., rho, 1],
  ])

  # corr_mat = matrix([
  #   [1, 0.75, 0.62, 0.66],
  #   [0.75, 1, 0.73, 0.74],
  #   [0.62, 0.73, 1, 0.54],
  #   [0.66, 0.74, 0.54, 1],
  # ])

  nuisance_gaussian.corr4.bind(w, 'erec', vals, corr_mat)

  model = w.pdf('syst_erec')
  h = model.createHistogram('hist', w.var('h3_erec'), YVar=w.var('mu_erec'))
  h.Draw('colz')
  ROOT.gPad.Update()
  raw_input()


#===============================================================================

def main5():
  """
  For the fully correlated eacc.
  """
  w = ROOT.RooWorkspace()
  tag = 'eacc'
  ttypes = 'e', 'h1', 'h3', 'mu'
  nuisance_gaussian.fullcorr4.inject(w, tag, *ttypes)
  vals = {
    'e' : ufloat(0.03, 0.0015),
    'h1': ufloat(0.04, 0.002),
    'h3': ufloat(0.05, 0.0025),
    'mu': ufloat(0.04, 0.002),
  }
  nuisance_gaussian.fullcorr4.bind(w, tag, vals)
  w.Print()

  w.var('std_eacc').val = 5
  w.Print()

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  ROOT.gStyle.SetPalette(ROOT.kBird) ## Colors
  # main1()
  # main2()
  main3()
  # main4()
  # main5()
