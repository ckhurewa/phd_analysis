#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Attempt to get the range-exclusion working for HypoTest & RooStats

"""
from lim_utils import ROOT, get_parser, get_queues_bkg
from prep_workspace import load_lite

## Init
args = get_parser().parse_args()
args.tt = 'mu'
w = load_lite(args)
w.Print()
w.PrintVars()

## Prepare range
mass = w.var('mass')
mass.setRange('exz1', mass.min, 80)
mass.setRange('exz2', 100, mass.max)
mass.setRange('fit', 40, 80)
mass.Print()


w.factory('newk   [100, -200, 500]')
w.factory('nzll   [100, -200, 500]')
w.factory('nother [100, -200, 500]')
w.factory('nqcd   [100, -200, 500]')
w.factory('nztau  [100, -200, 500]')
epdf1 = ROOT.RooExtendPdf('test3', 'test3', w.pdf('mu_pdf_EWK'  ), w.var('newk'))
epdf2 = ROOT.RooExtendPdf('test2', 'test2', w.pdf('mu_pdf_Zll'  ), w.var('nzll'))
epdf3 = ROOT.RooExtendPdf('test4', 'test4', w.pdf('mu_pdf_Other'), w.var('nother'))
epdf4 = ROOT.RooExtendPdf('test5', 'test5', w.pdf('mu_pdf_QCD'  ), w.var('nqcd'))
epdf5 = ROOT.RooExtendPdf('test1', 'test1', w.pdf('mu_pdf_Ztau' ), w.var('nztau'))

epdf = ROOT.RooAddPdf('test', 'test', [epdf1, epdf2, epdf3, epdf4, epdf5])

# epdf.Print()
# print epdf.extendMode()
# print epdf.selfNormalized()
# print epdf.evaluate()

data = w.data('mu_data_OS')
# data.Print()
# print data.numEntries()
# print data.reduce(CutRange='exz1').numEntries()
# print data.reduce(CutRange='exz2').numEntries()

# epdf.fitTo(data)
epdf.fitTo(data, range='exz1,exz2')
# w.var('nsig').Print()

frame = mass.frame()
data.plotOn(frame)
epdf.plotOn(frame)
epdf.plotOn(frame, components='mu_pdf_EWK'  , lineStyle=2, lineColor=ROOT.kMagenta)
epdf.plotOn(frame, components='mu_pdf_Zll'  , lineStyle=2, lineColor=ROOT.kBlue)
epdf.plotOn(frame, components='mu_pdf_Other', lineStyle=2, lineColor=ROOT.kGreen)
epdf.plotOn(frame, components='mu_pdf_QCD'  , lineStyle=2, lineColor=ROOT.kYellow+3)
epdf.plotOn(frame, components='mu_pdf_Ztau' , lineStyle=2, lineColor=ROOT.kRed)
frame.Draw()
frame.minimum = 1e-3
ROOT.gPad.Update()
raw_input()
