#!/usr/bin/env python
"""

MODEL 2
- Single channel
- Blob of background
- POI = nsig

"""

import prep_workspace
import collectors
from lim_utils import (
  TTYPES, get_parser, get_queues_bkg, create_ModelConfigSB, 
  nuisance_gaussian, test_suite, tune_POI_range,
)

#===============================================================================

def prepare_model(w, tt, mass, syst):
  ## Parameter of interest
  w.factory('nsig [1, -200, 500]')

  ## Prepare queue of processes
  q_bkg = get_queues_bkg(tt)

  ## Single nuisance: nbkg, and other (const) fractional contribution
  nuisance_gaussian.inject(w, 'nbkg')
  for bkg in q_bkg:
    w.factory('frac_{0} [0.]'.format(bkg))

  ## Bind the const fraction of each background
  q_bkgpdf = ['frac_{0} * {1}_pdf_{0}'.format(bkg, tt) for bkg in q_bkg]
  w.factoryArgs('SUM: pdf_BKG', q_bkgpdf)
  q_nominal = 'nbkg * pdf_BKG', 'nsig * {0}_pdf_higgs_{1}'.format(tt, mass)
  w.factoryArgs('SUM: epdf_SB', q_nominal)

  ## Finally, switch with/without systematic
  if syst:
    w.factory('PROD: PDF(epdf_SB, syst_nbkg)')
  else:
    w.Import(w.pdf('epdf_SB'), RenameVariable=('epdf_SB', 'PDF'))

  ## define for drawing
  w.factory('SUM: epdf_SIG(nsig * {0}_pdf_higgs_{1})'.format(tt, mass))
  w.factory('SUM: epdf_BKG(nbkg * pdf_BKG)')

  ## Metavars --> ModelConfig
  w.defineSet('POI', 'nsig')
  w.defineSet('OBS', 'mass')

  ## Alias to observed data
  w.data('%s_data_OS'%tt).SetName('DATA')

#===============================================================================

def bind_values(w, regime, tt, syst):
  """
  Bind the const fractional contribution of each background
  """
  nbkgs = collectors.nbkg().loc[regime][tt]
  nbkg  = nbkgs.sum()
  nuisance_gaussian.bind(w, 'nbkg', nbkg, syst)
  for key, val in nbkgs.iteritems():
    v = w.var('frac_%s'%key)
    if v:
      v.val = val.n/nbkg.n
      w.set('GOBS').add(v)
    else:
      print 'Skip:', key

  ## Adjust POI lower bound
  tune_POI_range(w)

#===============================================================================

def main_single(args):
  ## Load the base workspace
  args.geo = None
  w = prep_workspace.load_lite(args)

  ## prepare the model based on above pre-loaded pdf
  prepare_model(w, args.tt, args.mass, args.syst)

  ## bind the rest of scalar values
  bind_values(w, args.regime, args.tt, args.syst)

  ## Wrap prepare metavars into ModelConfig, prepare snapshot.
  create_ModelConfigSB(w, args.syst)

  ## finally, run the fitting
  test_suite.execute(w, args)

#===============================================================================

if __name__ == '__main__':
  main_single(get_parser().parse_args())
