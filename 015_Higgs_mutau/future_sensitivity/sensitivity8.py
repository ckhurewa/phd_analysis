#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
# Get base config
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import pd, packages, Br_tau, memorized
from uncertainties import nominal_value

def Lumi():
  return 2000.

def Csc():
  se = packages.prod_csc().values()['csc'] # 8TeV
  se.index.name = 'mass'
  return se[[25, 45, 85, 125, 195]]

def BrHMT():
  # return 1. # if assume 100%
  return 0.25/100. # < 0.25%, CMS 13TeV best limit

def Eacc():
  sys.path.append(os.path.expandvars('$DIR15/efficiencies/acceptance'))
  import read_full
  return read_full.efficiencies().loc['4pi'].stack()

def Erec():
  """
  Approximate from the existing
  """
  se = pd.Series({
    'e' : 0.60,
    'h1': 0.65,
    'h3': 0.20,
    'mu': 0.75,
  }, name='erec')
  se.index.name = 'tt'
  return se

def Esel():
  """
  Assume ~20% same as current status.
  With help from MVA, the background rejection should be better.
  """
  se = pd.Series({
    'e' : 0.25,
    'h1': 0.20,
    'h3': 0.30,
    'mu': 0.10,
  }, name='esel')
  se.index.name = 'tt'
  return se

def collect():
  df = pd.concat({'eacc': Eacc()}, axis=1)
  df = df.join(Csc()).join(Br_tau()).join(Erec()).join(Esel())
  df['lumi']  = Lumi() # scalar
  df['brhmt'] = BrHMT()
  ## Ready, make the product now.
  df['nsig'] = df.prod(axis=1).apply(nominal_value)
  return df.dropna()

if __name__ == '__main__':
  df = collect()['nsig'].unstack('tt')
  df['total'] = df.sum(axis=1)
  print df.fmt2f
