#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Calculation of expected signal yield using the observed limits,
according to Will's request.

Nsig = lumi * cscbr_acc * brtau * eff_pt * erec * esel

Question: Does the observed limit of cscbr at 8TeV remain applicable 
for the estimation of yield at 13 TeV?
- weak solution: the cross-section increase with energy, so the limit at 8 TeV
  is usable...

Note: There is no obseved limit for mH = 25GeV.

"""

import sys, os
# Get base config
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import pd, packages, Br_tau, memorized
from uncertainties import nominal_value
sys.path.append(os.path.expandvars('$DIR15/upperlim'))
from lim_utils import FINAL_STRATEGY, FINAL_KERNEL, FINAL_REGIME
import load_upperlim

## Local result reused 
from sensitivity13 import Lumi, Ekine, Erec, Esel

def CscBR_acc():
  """
  Observed upper limit of csc(pp -> Higgs -> mutau), with mu,tau inside
  LHCb geometrical acceptance.

  Result in pb.
  """
  se = load_upperlim.load_all_obs().loc[(FINAL_STRATEGY,FINAL_KERNEL,FINAL_REGIME,'lhcb','simult','withsyst')]
  se[200] = se[195] # shift compat
  return se

@memorized
def collect_raw():
  df = pd.DataFrame({'kine':Ekine(old=False)})
  df = df.join(CscBR_acc()).join(Br_tau()).join(Erec()).join(Esel())
  df['lumi']  = Lumi() # scalar
  ## Ready, make the product now.
  df['nsig'] = df.prod(axis=1).apply(nominal_value)
  return df.dropna()

#===============================================================================

if __name__ == '__main__':
  print Ekine()

  # print CscBR_acc()
  print collect_raw()

  ## Summary of nsig
  df = collect_raw()['nsig'].unstack()
  df['total'] = df.sum(axis=1)
  print df.fmt0f
