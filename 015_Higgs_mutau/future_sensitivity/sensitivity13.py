#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Give an estimation for future sensivity of HMT search, via

N = lumi csc BR_higgs BR_tau egeo ept erec esel

"""

import sys, os
# Get base config
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import pd, packages, Br_tau, memorized
from uncertainties import nominal_value

def Lumi():
  """Assume 5 fb-1 = 5000 pb-1 in the Run-II only."""
  return 5000.

def Csc():
  """13TeV (Run-II) condition."""
  # se = packages.prod_csc().values()['csc'] # 8TeV, wrong
  import prod_csc_13tev
  se =  prod_csc_13tev.values()['csc']
  se.index.name = 'mass'
  return se

def BrHMT():
  # return 1. # if assume 100%
  return 0.25/100. # < 0.25%, CMS 13TeV best limit

def Eacc():
  """
  Note: Only geometrical only (ETA, no PT), and also only some masses.
  Reminder: 13TeV used POWHEG+PYTHIA
  """
  sys.path.append(os.path.expandvars('$DIR15/efficiencies/acceptance'))
  import read_fast
  return read_fast.eff_13tev().swaplevel().sort_index()

def Ekine(old=True):
  """
  Cut on PT to be compat with trigger choice.

  OLD: use exactly the existing cut used in 8TeV study
  NEW: theoretically better PT cuts.
  """
  sys.path.append(os.path.expandvars('$DIR15/efficiencies/acceptance'))
  import read_full13
  eff = read_full13.efficiencies().unstack('cut')
  se  = eff['kine1'] if old else eff['kine2']
  se.name = 'kine'
  return se

def Erec():
  """
  Approximate from the existing
  """
  se = pd.Series({
    'e' : 0.60,
    'h1': 0.65,
    'h3': 0.20,
    'mu': 0.75,
  }, name='erec')
  se.index.name = 'tt'
  return se

def Erec_full():
  """
  Borrow from existing
  """
  ## actual computation
  regimes = {
    25 : 'lowmass',
    45 : 'lowmass',
    85 : 'nominal',
    125: 'tight',
    195: 'tight',
  }
  sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
  import rec_detailed
  df = rec_detailed.efficiencies_raw().xs('full', level='mode')
  df = df.unstack('mass').unstack('regime').T
  df = df.loc[list(regimes.items())].stack('tt')
  se = df.apply(lambda se: se.erec/se.etrig, axis=1)
  se = se.reset_index('regime', drop=True).sort_index().rename({195:200})
  se.name = 'erec'
  return se

# def Etrig():
#   """
#   Assume ~70%, given that the PT threshold is already satisfied.
#   """
#   return 0.70

# def Estrip(old=True):
#   """
#   Assume ~80% by putting more kinematic cut in data
#   """
#   return 1.0 if old else 0.80

def Esel():
  """
  Assume approximately current status.
  With help from MVA, the background rejection should be better.
  """
  se = pd.Series({
    'e' : 0.25,
    'h1': 0.20,
    'h3': 0.30,
    'mu': 0.10,
  }, name='esel')
  se.index.name = 'tt'
  return se

@memorized
def collect_raw(old=True):
  df = pd.concat([Ekine(old), Eacc()], axis=1)
  df = df.join(Csc()).join(Br_tau()).join(Erec()).join(Esel())
  df['lumi']  = Lumi() # scalar
  df['brhmt'] = BrHMT()
  ## Ready, make the product now.
  df['nsig'] = df.prod(axis=1).apply(nominal_value)
  return df.dropna()

#===============================================================================

if __name__ == '__main__':
  pass
  # print Lumi()
  # print Br_tau()
  # print Csc()
  # print Eacc()
  # print Ekine()
  # print Erec()
  old = False
  print collect_raw(old)

  ## Summary of nsig
  df = collect_raw(old)['nsig'].unstack()
  df['total'] = df.sum(axis=1)
  print df.fmt1f
