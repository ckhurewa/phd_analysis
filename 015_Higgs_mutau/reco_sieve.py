#!/usr/bin/env python

from PyrootCK.mathutils import EffU
from PyrootCK.mathutils import EffU_unguard as div
from PythonCK.decorators import report_info
from qhist.utils import join
import pandas as pd

## Generator for list of primitive objects used in reco cut
reco_objects = {
  'e' : ('hardmu', 'e'),
  'mu': ('hardmu', 'mu'),
  'h1': ('hardmu', 'had' ),
  'h3': ('hardmu', 'pr1' , 'pr2' , 'pr3'),
}

#------------#
# ACCEPTANCE #
#------------#

cuts_acc_mumu = [
  {
    ( 'hardmu_MCPT > 20E3', 'mu_MCPT >  5E3' ),
    ( 'hardmu_MCPT >  5E3', 'mu_MCPT > 20E3' ),
  },
  'hardmu_MCETA > 2.0',
  'hardmu_MCETA < 4.5',
  'mu_MCETA     > 2.0',
  'mu_MCETA     < 4.5',
  'HMTch_MCM    > 20E3',
  # 'HMTch_MCM    < 120E3',
]

cuts_acc_mue = [
  {
    ( 'hardmu_MCPT > 20E3', 'e_MCPT >  5E3' ),
    ( 'hardmu_MCPT >  5E3', 'e_MCPT > 20E3' ),
  },
  'hardmu_MCETA > 2.0',
  'hardmu_MCETA < 4.5',
  'e_MCETA      > 2.0',
  'e_MCETA      < 4.5',
  'HMTch_MCM    > 20E3',
  # 'HMTch_MCM    < 120E3',
]

cuts_acc_lh1 = [
  'hardmu_MCPT  > 20E3',
  'hardmu_MCETA > 2.0',
  'hardmu_MCETA < 4.5',
  #
  'had_MCPT    > 10E3',
  'had_MCETA   > 2.00',
  'had_MCETA   < 4.50',
  'HMTch_MCM   > 30E3',
  # 'HMTch_MCM    < 120E3',
]

cuts_acc_lh3 = [
  'hardmu_MCPT   > 20E3',
  'hardmu_MCETA  > 2.0',
  'hardmu_MCETA  < 4.5',
  #
  'pr1_MCPT   >  6E3',
  'pr3_MCPT   >  1E3',
  #
  'pr1_MCETA  > 2.00',
  'pr1_MCETA  < 4.50',
  'pr2_MCETA  > 2.00',
  'pr2_MCETA  < 4.50',
  'pr3_MCETA  > 2.00',
  'pr3_MCETA  < 4.50',
  #
  'tauh3ch_MCPT > 12E3',
  'tauh3ch_MCM  > 700',
  'tauh3ch_MCM  < 1500',
  'HMTch_MCM    > 30E3',
  # 'HMTch_MCM   < 120E3',
]

## Acceptance cut
cuts_acc = {
  'e' : cuts_acc_mue,
  'mu': cuts_acc_mumu,
  'h1': cuts_acc_lh1,
  'h3': cuts_acc_lh3,
}

#------------#
# MC-Predict #
#------------#

## Ask the particle to be reconstructed, predicted by IMCReconstructed.
cut_mcpredict = join(
  '{0}_recob >= 2',
  '{0}_recod >= 1',
).format

cut_recob      = '{0}_recob>=2'.format
cuts_recob     = { tt:['{0}_recob>=2'.format(obj) for obj in objs ] for tt,objs in reco_objects.iteritems()}
cuts_recod     = { tt:['{0}_recod>=1'.format(obj) for obj in objs ] for tt,objs in reco_objects.iteritems()}
cuts_mcpredict = { tt:[cut_mcpredict(obj) for obj in objs ] for tt,objs in reco_objects.iteritems()}

cuts_mcpredict1 = {tt:cut_mcpredict('hardmu') for tt in reco_objects}
cuts_mcpredict2 = {tt:[cut_mcpredict(obj) for obj in objs[1:]] for tt,objs in reco_objects.iteritems()}

#-------#
# TRACK #
#-------#

## Tracking quality cut
cut_track = join(
  ## Modern, but eventually incompat with what sanitized in Ztautau.
  # '{0}_TRPCHI2>0.01', ## removed from HMT
  # '{0}_TRTYPE == 3',  # Long
  # '{0}_TRCHI2DOF < 3', # this is probably on by default.
  # '({0}_PERR2/{0}_P/{0}_P)**0.5  < 0.1', # relative momentum err < 10%
  # '{0}_TRGHOSTPROB < 0.4',  # Added 170312
  # '{0}_PROBNNghost < 0.4',  # Added 170312

  ## Strict Ztautau version.
  '{0}_ID != 0',
  '{0}_TRPCHI2>0.01', # Infamous, keep for compat since Zmumu
  '{0}_TRTYPE == 3',  # Long
).format
cuts_track = {tt:[cut_track(obj) for obj in objs ] for tt,objs in reco_objects.iteritems()}

cuts_track1 = {tt:cut_track('hardmu') for tt in reco_objects}
cuts_track2 = {tt:[cut_track(obj) for obj in objs[1:]] for tt,objs in reco_objects.iteritems()}

#--------#
# VERTEX #
#--------#

## In addition to h3: Vertex formed.
cuts_vertex = {
  'e' : '1==1',
  'h1': '1==1',
  'mu': '1==1',
  'h3': 'tauh3ch_P>0', # successful combine
}

#-----------#
# KINEMATIC #
#-----------#

## Kinematic cut
## Find min/max from basic formula
min_pr_PT = '(pr1_PT<=pr2_PT)*((pr1_PT<=pr3_PT)*pr1_PT + (pr1_PT>pr3_PT)*pr3_PT)  + (pr1_PT>pr2_PT)*((pr2_PT<=pr3_PT)*pr2_PT + (pr2_PT>pr3_PT)*pr3_PT)'
max_pr_PT = '(pr1_PT>=pr2_PT)*((pr1_PT>=pr3_PT)*pr1_PT + (pr1_PT<pr3_PT)*pr3_PT)  + (pr1_PT<pr2_PT)*((pr2_PT>=pr3_PT)*pr2_PT + (pr2_PT<pr3_PT)*pr3_PT)'

## The prong's PT can be permuted
cuts_kine_lh3 = [
  'hardmu_PT   > 20E3',
  'hardmu_ETA  > 2.0',
  'hardmu_ETA  < 4.5',
  #
  max_pr_PT+' > 6E3',
  min_pr_PT+' > 1E3',
  #
  'pr1_ETA  > 2.00',
  'pr1_ETA  < 4.50',
  'pr2_ETA  > 2.00',
  'pr2_ETA  < 4.50',
  'pr3_ETA  > 2.00',
  'pr3_ETA  < 4.50',
  #
  'tauh3ch_PT > 12E3',
  'tauh3ch_M  > 700',
  'tauh3ch_M  < 1500',
  'HMTch_M    > 30E3',
]

## Like cuts_mckine, but simply replace the MC functors to regular functors.
cuts_kine = { key:join(val).replace('_MC','_') for key,val in cuts_acc.iteritems() }
cuts_kine['h3'] = cuts_kine_lh3

#-----#
# PID #
#-----#

## PID cuts
cut_PID_mu = '{0}_ISMUON'.format
cut_PID_h  = join(
  '!{0}_ISLOOSEMUON',
  '{0}_PP_InAccHcal',
  '{0}_HCALFrac >= 0.05',
).format
cut_PID_e   = join(
  '!{0}_ISLOOSEMUON',
  '{0}_PP_InAccHcal',
  '{0}_PP_InAccEcal',
  '{0}_HCALFrac    >= 0',
  '{0}_HCALFrac    <= 0.05',
  '{0}_ECALFrac    >= 0.1',
  '{0}_PP_CaloPrsE >= 50',
).format

cuts_pid1 = {tt:cut_PID_mu('hardmu') for tt in reco_objects}
cuts_pid2 = {
  'e': [
    cut_PID_e('e'),
  ],
  'mu': [
    cut_PID_mu('mu'),
  ],
  'h1': [
    cut_PID_h('had'),
  ],
  'h3': [
    cut_PID_h('pr1'),
    cut_PID_h('pr2'),
    cut_PID_h('pr3'),
  ],
}
cuts_pid = {tt:cuts+[cuts_pid1[tt]] for tt,cuts in cuts_pid2.iteritems()}

#-----#
# GEC #
#-----#

cuts_gec = {tt:'nSPDhits<=600' for tt in reco_objects}

#---------#
# TRIGGER #
#---------#

cuts_trigger = {
  'e' : {
    'hardmu_TOS_MUON & hardmu_PT>20e3',
    'e_TOS_ELECTRON  & e_PT>20e3',
  },
  'mu': {
    'hardmu_TOS_MUON & hardmu_PT>20e3',
    'mu_TOS_MUON     & mu_PT>20e3',
  },
  'h1': { 'hardmu_TOS_MUON' },
  'h3': { 'hardmu_TOS_MUON' },
}

#===============================================================================

@report_info
def apply(tree, tt):
  """
  Give the tree at channel tt, return the number of GetEntries at different 
  stage of reconstruction for further calculation.
  The tree will also implicitly recieve the aliases.
  """
  ## Aliasing
  tree.SetAlias('Acc'    , join(cuts_acc[tt]))
  tree.SetAlias('Predict', join(cuts_mcpredict[tt]))
  tree.SetAlias('Track'  , join(cuts_track[tt]))
  tree.SetAlias('Vertex' , join(cuts_vertex[tt]))
  tree.SetAlias('Kine'   , join(cuts_kine[tt]))
  tree.SetAlias('PID'    , join(cuts_pid[tt]))
  tree.SetAlias('GEC'    , join(cuts_gec[tt]))
  tree.SetAlias('Trigger', join(cuts_trigger[tt]))
  #
  tree.SetAlias('Predict1', join(cuts_mcpredict1[tt]))
  tree.SetAlias('Predict2', join(cuts_mcpredict2[tt]))
  tree.SetAlias('Track1'  , join(cuts_track1[tt]))
  tree.SetAlias('Track2'  , join(cuts_track2[tt]))
  tree.SetAlias('PID1'    , join(cuts_pid1[tt]))
  tree.SetAlias('PID2'    , join(cuts_pid2[tt]))

  ## Start counting
  get = tree.GetEntries
  se  = pd.Series({
    ## default
    'raw'   : get(),
    'A'     : get('Acc'),
    'AT'    : get('Acc & Track'),
    'ATV'   : get('Acc & Track & Vertex'),
    'ATVK'  : get('Acc & Track & Vertex & Kine'),
    'ATVKP' : get('Acc & Track & Vertex & Kine & PID'),
    'ATVKPG': get('Acc & Track & Vertex & Kine & PID & GEC'),
    'full'  : get('Acc & Track & Vertex & Kine & PID & GEC & Trigger'),
    
    # 'full2': get('Acc & Track & Vertex & Kine & PID & GEC & Trigger & (tau_MCPT>20e3) & (tau_MCETA>2) & (tau_MCETA<4.5) & (hardmu_PT>20e3) & (mu_PT>5e3) & (hardmu_MCPT > 20E3)'),
    # 'full2': get('Acc & Track & Vertex & Kine & PID & GEC & Trigger & (tau_MCPT>20e3) & (tau_MCETA>2) & (tau_MCETA<4.5)'),

    ## without acceptance prerequisite
    'T'     : get('Track'),
    'TV'    : get('Track & Vertex'),
    'TVK'   : get('Track & Vertex & Kine'),
    'TVKP'  : get('Track & Vertex & Kine & PID'),
    'TVKPG' : get('Track & Vertex & Kine & PID & GEC'),
    'TVKPGH': get('Track & Vertex & Kine & PID & GEC & Trigger'),

    ## with IMCreconstructible, IMCreconstucted
    'APT' : get('Acc & Predict & Track'),
    'APTV': get('Acc & Predict & Track & Vertex'),
    'PTVK': get('Predict & Track & Vertex & Kine'),

    ## Components
    'APTV1': get('Acc & Predict1 & Track1 & Vertex'),
    'APTV2': get('Acc & Predict2 & Track2 & Vertex'),
    'TVKP1': get('Track & Vertex & Kine & PID1'),
    'TVKP2': get('Track & Vertex & Kine & PID2'),
  })

  ## Providing efficiencies
  se['etrack'] = div(se['A']    , se['APTV']  ) # Fulldiv, also require vertex existence
  se['ekine']  = div(se['APTV'] , se['PTVK']  ) # with leak-in
  se['epid']   = div(se['TVK']  , se['TVKP']  )
  se['egec']   = div(se['TVKP'] , se['TVKPG'] )
  se['etrig']  = div(se['TVKPG'], se['TVKPGH'])
  #
  se['etrack1'] = div(se['A']  , se['APTV1'])
  se['etrack2'] = div(se['A']  , se['APTV2'])
  se['epid1']   = div(se['TVK'], se['TVKP1'])
  se['epid2']   = div(se['TVK'], se['TVKP2'])

  ## default erec, uncorrected
  se['erec'] = se.etrack * se.ekine * se.epid * se.egec * se.etrig
  return se

#===============================================================================
