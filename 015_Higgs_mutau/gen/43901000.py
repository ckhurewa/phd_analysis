# Event Type: 43901000
#
# ASCII decay Descriptor: pp -> {H_10 -> mu- tau+}

# // redefine the properties of H_20 and H_30 particles:
# ParticlePropertySvc.Particles = {
#     "H_20 88 35 0.0 120.0 9.4e-26 Higgs'0 35 0.0e+00" ,
#     "H_30 89 36 0.0  40.0 1.0e-12      A0 36 0.0e+00"
#   } ;

HIGGS_MASS = 195.0

from Gaudi.Configuration import *
from Configurables import Generation
Generation().EventType = 43901000
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
Generation().Special.CutTool = ""
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/LoKi__FullGenEventCut"
from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = [ 
  "H_10 87 25 0.0 %f 9.400e-026 Higgs0 25 0.000e+000"%HIGGS_MASS
  # "H_10 87 25 0.0 25.000 9.400e-026 Higgs0 25 0.000e+000"
]

importOptions("$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py")
Generation().Special.Pythia8Production.Commands += [
  ## Open the SM Higgs decay
  'HiggsSM:gg2H = on',
  ## Control decay to mu+tau
  '25:isResonance = on',
  '25:onmode = off',
  '25:addChannel = 1 1 100 13 -15',
  '25:addChannel = 1 1 100 -13 15',
  '25:mMin = 5.0',
  'PhaseSpace:mHatMin = %f'%HIGGS_MASS,     # Minimum mass.
  'PhaseSpace:mHatMax = %f'%HIGGS_MASS,     # Maximum mass.
]
Generation().PileUpTool = "FixedLuminosityForRareProcess"

## Debug
#Generation().Special.Pythia8Production.ListAllParticles = True
#Generation().OutputLevel = 2

## Allow Pythia to handle tau decays (not EvtGen through TAUOLA).
## See P.Ilten's comment
Generation().DecayTool = ''
Generation().Special.DecayTool = ''

from Configurables import GenerationToSimulation
GenerationToSimulation("GenToSim").KeepCode = "GABSID == 'H_10'"

## Fullgen fiducial + acceptance cut. Be smart here.
from Configurables import LoKi__FullGenEventCut
Generation().addTool( LoKi__FullGenEventCut )
cut = Generation().LoKi__FullGenEventCut
cut.Code = " count ( GoodHiggs ) > 0 "
cut.Preambulo += [
  "from GaudiKernel.SystemOfUnits import ns, GeV, mrad",
  "InWideAcpt   = (GTHETA < 480.0*mrad)",
  'SoftTauX     = (GABSID==15) & (GNINTREE((GABSID!=15) & GCHARGED     & (InWideAcpt) & (GPT> 4*GeV))>0)',
  'HardTauL     = (GABSID==15) & (GNINTREE(((GABSID==11)|(GABSID==13)) & (InWideAcpt) & (GPT>14*GeV))>0)',
  'SoftMu       = (GABSID==13) & (InWideAcpt) & (GPT >  4*GeV)',
  'HardMu       = (GABSID==13) & (InWideAcpt) & (GPT > 14*GeV)',
  'GoodPair1    = (GNINTREE(SoftTauX)>0) & (GNINTREE(HardMu)>0)',
  'GoodPair2    = (GNINTREE(HardTauL)>0) & (GNINTREE(SoftMu)>0)',
  "GoodHiggs    = (GID==25) & ((GoodPair1)|(GoodPair2))",
]


## P8PileUp
## From https://svnweb.cern.ch/trac/lhcb/browser/Gauss/trunk/Gen/LbAlpGen/options/AlpGen.py
from Configurables import Generation, Pythia8Production
Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
Generation().PileUpTool = "FixedLuminosityForRareProcess"
Generation().Special.addTool( Pythia8Production, name = "Pythia8PileUp" )
Generation().Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
Generation().Special.ReinitializePileUpGenerator  = False
