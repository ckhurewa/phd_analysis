
# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LHCbGanga61
import re

#===============================================================================

def expandvars( s, env ):
  """Helper method to replace envvar in string s from manually-given env dict."""
  for var in re.findall(r'(\$\w+)', s):
    varname = var.replace('$', '')
    s = s.replace(var, env.get(varname, var))
  return s

def attach_optsfile( app, *opts ):
  """
  Helper method to allow setting `optsfile` attribute to app enabling envvar.
  """
  env = app.getenv()
  app.optsfile = [ expandvars(opt, env) for opt in opts ]
  return app  # No need to return actually.

#-------#
# GAUSS #
#-------#

gauss = Gauss(version='v45r10p1', platform='x86_64-slc5-gcc43-opt')
gauss.extraopts = \
"""
from Gauss.Configuration import *
GenInit('GaussGen').RunNumber =  1
GenInit('GaussGen').FirstEventNumber = 1
LHCbApp().EvtMax = 3   # Need anyway otherwise it'll crash (?)
"""
attach_optsfile( gauss,
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py',
  '45001002.py',
  '$LBPYTHIA8ROOT/options/Pythia8.py',
  '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py',
  '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
)

#-------#
# BOOLE #
#-------#

boole = Boole(version='v26r3', platform='x86_64-slc5-gcc43-opt' )
attach_optsfile( boole, 
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/Boole/Default.py',
  '$APPCONFIGOPTS/Boole/DataType-2012.py',
  '$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py',
  '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
)

#-------#
# MOORE #
#-------#

moore = Moore(version='v14r8p1', platform='x86_64-slc5-gcc43-opt')
moore.extraopts = \
"""
from Configurables import Moore
Moore().outputFile = 'Moore.dst'  # Force output
"""
attach_optsfile( moore, 
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/Moore/MooreSimProductionWithL0Emulation.py',
  '$APPCONFIGOPTS/Conditions/TCK-0x409f0045.py',
  '$APPCONFIGOPTS/Moore/DataType-2012.py',
  '$APPCONFIGOPTS/L0/L0TCK-0x0045.py',
) 

#--------#
# BRUNEL #
#--------#

brunel = Brunel(version='v43r2p11', platform='x86_64-slc5-gcc46-opt')
brunel.extraopts = \
"""
from Gaudi.Configuration import *
OutputStream("DstWriter").Output = "DATAFILE='PFN:Brunel.dst' TYP='POOL_ROOTTREE' OPT='REC'"
"""
attach_optsfile( brunel, 
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/Brunel/DataType-2012.py',
  '$APPCONFIGOPTS/Brunel/MC-WithTruth.py',
#  '$APPCONFIGOPTS/Persistency/DST-multipleTCK-2012.py',
  '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
)

#---------#
# DAVINCI #
#---------#

davinci = DaVinci(version='v32r2p1', platform='x86_64-slc5-gcc46-opt')
attach_optsfile( davinci, 
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py',
  '$APPCONFIGOPTS/DaVinci/DataType-2012.py',
  '$APPCONFIGOPTS/DaVinci/InputType-DST.py',
  '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
)

#==============================================================================

#backend = LSF(extraopts='--mem=3000 -t 4:0:0')  # Kill it before it consumes space
backend = Dirac()

tGauss = LHCbTransform(
  name         = 'Gauss',
  backend      = backend,
  application  = gauss,
  mc_num_units = 5, # Increase later
  outputfiles  = [ '*.xml', DiracFile('*.sim'), LocalFile('summary.xml') ],
  splitter     = GaussSplitter( numberOfJobs=200, eventsPerJob=100, firstEventNumber=0 ),
)

tBoole = LHCbTransform(
  name        = 'Boole',
  backend     = backend,
  application = boole,
  outputfiles = [ '*.xml', DiracFile('*.digi'), LocalFile('summary.xml') ],
  splitter    = SplitByFiles( filesPerJob=2, ignoremissing=False ),
  delete_chain_input = True,
)

tMoore = LHCbTransform(
  name        = 'Moore',
  backend     = backend,
  application = moore,
  outputfiles = [ '*.xml', DiracFile('*.dst'), LocalFile('summary.xml') ],
  splitter    = SplitByFiles( filesPerJob=2, ignoremissing=False ),
  delete_chain_input = True,
)

tBrunel = LHCbTransform(
  name        = 'Brunel',
  backend     = backend,
  application = brunel,
  outputfiles = [ '*.xml', DiracFile('*.dst'), LocalFile('summary.xml') ],
  splitter    = SplitByFiles( filesPerJob=2, ignoremissing=False ),
  delete_chain_input = True,
)

tDaVinci = LHCbTransform(
  name        = 'DaVinci',
  backend     = backend,
  application = davinci,
  outputfiles = [ '*.xml', DiracFile('*.dst'), LocalFile('summary.xml') ],
  splitter    = SplitByFiles( filesPerJob=2, ignoremissing=False ),
  delete_chain_input = True,
)

task = LHCbTask( float=5, name='Higgs_mutau_fullsim' )
task.appendTransform( tGauss   )
task.appendTransform( tBoole   )
task.appendTransform( tMoore   )
task.appendTransform( tBrunel  )
task.appendTransform( tDaVinci )
#
tBoole.addInputData  ( TaskChainInput(include_file_mask=['\.sim$'] , input_trf_id = tGauss.getID()))
tMoore.addInputData  ( TaskChainInput(include_file_mask=['\.digi$'], input_trf_id = tBoole.getID()))
tBrunel.addInputData ( TaskChainInput(include_file_mask=['\.dst$'] , input_trf_id = tMoore.getID()))
tDaVinci.addInputData( TaskChainInput(include_file_mask=['\.dst$'] , input_trf_id = tBrunel.getID()))
#
task.run()
