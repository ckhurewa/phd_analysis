from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    # MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *

"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
# decay = "[B0 => ^(Lambda_c~- ==> ^p~- ^K+ ^pi-) ^p+ ^pi- ^pi+]CC"
decay = "[H_10 => ^tau+ ^mu-]CC"
# decay_heads = ["p-", 'p+', 'W-', "W+", 'mu-', 'mu+']
# decay_heads = ['W-', 'W+', 'mu-', 'mu+']
decay_heads = ['H_10']
year = 2012


# datafile = 'root://eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/c/ckhurewa/2016_09/140626/140626638/Gauss-43901000-100ev-20160919.sim'
# datafile = 'root://eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/c/ckhurewa/2016_09/141011/141011855/Gauss-43901000-100ev-20160923.sim'
# datafile = 'root://eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/c/ckhurewa/2016_10/142694/142694563/Gauss-43901000-100ev-20161013.sim'
# datafile = 'root://eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/c/ckhurewa/2016_10/142871/142871068/Gauss-43901000-100ev-20161015.sim'
# datafile = 'file:///storage/gpfs_lhcb/lhcb/user/c/ckhurewa/2016_10/142093/142093031/Gauss-43901000-100ev-20161005.sim'
# datafile = 'file:///storage/gpfs_lhcb/lhcb/user/c/ckhurewa/2016_10/142747/142747438/Gauss-43901000-100ev-20161014.sim'
# datafile = 'root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/user/c/ckhurewa/2016_09/140368/140368622/Gauss-43901000-100ev-20160916.sim'
# datafile = 'root://ccdcacli067.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/user/c/ckhurewa/2016_10/142590/142590333/Gauss-43901000-100ev-20161012.sim'
# datafile = 'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/user/c/ckhurewa/2016_09/140928/140928697/Gauss-43901000-100ev-20160923.sim'
# datafile = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/user/c/ckhurewa/2016_09/140833/140833552/Gauss-43901000-100ev-20160921.sim?svcClass=lhcbUser'
datafile = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/user/c/ckhurewa/2016_09/141420/141420986/Gauss-43901000-100ev-20160928.sim?svcClass=lhcbUser'
# datafile = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/user/c/ckhurewa/2016_10/142490/142490884/Gauss-43901000-100ev-20161010.sim?svcClass=lhcbUser'
# datafile = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/user/c/ckhurewa/2016_10/142554/142554152/Gauss-43901000-100ev-20161011.sim?svcClass=lhcbUser'
# datafile = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/user/c/ckhurewa/2016_10/142658/142658743/Gauss-43901000-100ev-20161013.sim?svcClass=lhcbUser'
# datafile = 'root://clhcbdlf.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/user/c/ckhurewa/2016_10/142783/142783340/Gauss-43901000-100ev-20161014.sim?svcClass=lhcbUser'


# datafile = "Gauss-43901000-4ev-20160916.xgen"


# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
mctuple.ToolList += [
    "MCTupleToolHierarchy",
    # "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
]
# # Add a 'number of photons' branch
# mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
# mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
#     "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
# }
tt = mctuple.addTupleTool( 'LoKi::Hybrid::MCTupleTool', name='mtt_higgs')
tt.Variables = {
  'MCM'   : 'MCM',
  'MCETA' : 'MCETA',
}


# Print the decay tree for any particle in decay_heads
printMC = PrintMCTree()
printMC.ParticleNames = decay_heads

# Name of the .xgen file produced by Gauss
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

# Configure DaVinci
DaVinci().TupleFile = "DVntuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
# DaVinci().UserAlgorithms = [printMC] #, mctuple]
DaVinci().UserAlgorithms = [mctuple]

