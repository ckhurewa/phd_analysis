
# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LHCbGanga61
import re

#===============================================================================

def expandvars( s, env ):
  """Helper method to replace envvar in string s from manually-given env dict."""
  for var in re.findall(r'(\$\w+)', s):
    varname = var.replace('$', '')
    s = s.replace(var, env.get(varname, var))
  return s

def attach_optsfile( app, *opts ):
  """
  Helper method to allow setting `optsfile` attribute to app enabling envvar.
  """
  env = app.getenv()
  app.optsfile = [ expandvars(opt, env) for opt in opts ]
  return app  # No need to return actually.

#-------#
# BOOLE #
#-------#

boole = Boole(version='v26r3', platform='x86_64-slc5-gcc43-opt' )
boole.extraopts = \
"""
from Configurables import LHCbApp
LHCbApp().EvtMax = 10 
"""
attach_optsfile( boole, 
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/Boole/Default.py',
  '$APPCONFIGOPTS/Boole/DataType-2012.py',
  '$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py',
  '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py',
)

#-------#
# MOORE #
#-------#

moore = Moore(version='v14r8p1', platform='x86_64-slc5-gcc43-opt')
moore.extraopts = \
"""
from Configurables import Moore
Moore().outputFile = 'Moore.dst'  # Force output
"""
attach_optsfile( moore, 
  '$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py',
  '$APPCONFIGOPTS/Moore/MooreSimProductionWithL0Emulation.py',
  '$APPCONFIGOPTS/Conditions/TCK-0x409f0045.py',
  '$APPCONFIGOPTS/Moore/DataType-2012.py',
  '$APPCONFIGOPTS/L0/L0TCK-0x0045.py',
) 


#==============================================================================

backend = LSF(extraopts='--mem=2400 -t 4:0:0')  # Kill it before it consumes space
# backend = Dirac()


tBoole = LHCbTransform(
  name        = 'Boole',
  backend     = backend,
  application = boole,
  outputfiles = [ '*.xml', LocalFile('*.digi', localDir='/panfs/khurewat'), LocalFile('summary.xml') ],
  splitter    = None, # SplitByFiles( filesPerJob=1, ignoremissing=True ),
  files_per_unit=1,
  # mc_num_units= 1,
  # delete_chain_input = True,
)

tMoore = LHCbTransform(
  name        = 'Moore',
  backend     = backend,
  application = moore,
  outputfiles = [ '*.xml', MassStorageFile('*.dst'), LocalFile('summary.xml') ],
  # splitter    = SplitByFiles( filesPerJob=2, ignoremissing=False ),
  # delete_chain_input = True,
)



task = LHCbTask( float=1, name='Higgs_mutau_fullsim', comment='TestLocalChainInput' )
task.appendTransform( tBoole   )
# task.appendTransform( tMoore   )
#
tBoole.addInputData( LHCbDataset(['PFN:/panfs/khurewat/test.sim']) )
# tMoore.addInputData( TaskChainInput(include_file_mask=['\.digi$'], input_trf_id=tBoole.getID()))
#
task.run()
