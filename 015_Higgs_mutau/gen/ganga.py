import os
from glob import glob

## from my analysis/902_fullsim_resources
import Sim08h

## HACK To get the "simple" namePattern functionality back. Use with care
## Required for using local file as input
if '__setattr__' in LocalFile._impl.__dict__:
  del LocalFile._impl.__setattr__

#===============================================================================

def job_gauss():
  """
  Focus on the generation.
  """
  j = Job()
  j.name        = 'HiggsGen'
  j.comment     = '43901000 @ 195: Gauss+Dirac (100evt x 1ksj) + 0k'
  j.application = Sim08h.Gauss( '43901000.py', 100 )
  j.backend     = Dirac()
  j.splitter    = GaussSplitter( eventsPerJob=100, numberOfJobs=1000, firstEventNumber=0 )
  j.outputfiles = [ DiracFile('*.sim'), LocalFile('*.xml') ]
  return j

#-------------------------------------------------------------------------------

## shared coment between Boole/Moore/Brunel
# comment_BMB = 'Prefetch'
comment_BMB = '43901000 @ 195GeV [:100k](1kevt/j)'

def job_boole():
  jids  = 5290,
  ds    = LHCbDataset.new(lfn for jid in jids for lfn in jobs(jid).lfn_list() if lfn.endswith('sim'))

  j = Job()
  j.name        = 'HiggsFullgen'
  j.comment     = comment_BMB
  j.application = Sim08h.Boole()
  j.backend     = PBS(extraopts='--mem=2900 -t 1-0:0:0 --exclude=lphe01,lphe02,lphe03')
  j.inputdata   = ds
  j.splitter    = SplitByFiles( filesPerJob=10 )
  j.outputfiles = [ LocalFile('*.digi') ]
  return j


def job_moore_brunel():
  root = '/home/khurewat/gangadir/workspace/khurewat/LocalXML'
  # root = '/panfs/khurewat/ganga_storage'
  ds   = LHCbDataset.new(glob(root+'/5298/*/*put/*.d*'))

  j = Job()
  j.name        = 'HiggsFullgen'
  j.comment     = comment_BMB
  # j.application = Sim08h.Moore()
  j.application = Sim08h.Brunel()
  j.backend     = PBS(extraopts='--mem=2636 -t 1-0:0:0 --exclude=lphe01,lphe02')
  j.inputdata   = ds
  j.splitter    = SplitByFiles(filesPerJob=1)
  j.outputfiles = [ LocalFile('*.dst') ]
  return j

#===============================================================================

# j = job_gauss()
# j = job_boole()
# j = job_moore_brunel()

# j.submit()
queues.add(j.submit)

