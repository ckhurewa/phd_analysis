#!/usr/bin/env python

"""

Helper handle to import the module from 013 Ztautau on on-demand basis
(limited scope to avoid messing with sys.path)

"""

import sys
import os
import imp
from PythonCK.decorators import memorized

#===============================================================================
# 013: ZTAUTAU
#===============================================================================

@memorized
def id_utils():
  path = os.path.expandvars('$DIR13/identification/plot/id_utils.py')
  return imp.load_source('id_utils', path)


@memorized
def rec():
  """
  Z->tautau reconstruction efficiency module
  """
  sys.path.append(os.path.expandvars('$DIR13/efficiencies/reconstruction'))
  import rec
  return rec

@memorized
def samesign_utils():
  """
  On-demand import of samesign_utils module, isolate it as it can be heavy.
  """
  # path = os.path.expandvars('$DIR13/identification/plot/samesign_utils.py')
  # return imp.load_source('samesign_utils', path)
  ## note: samesign_utils uses id_utils locally
  sys.path.append(os.path.expandvars('$DIR13/identification/plot/'))
  import samesign_utils
  return samesign_utils

@memorized
def qhist_postprocessors():
  sys.path.append(os.path.expandvars('$DIR13/identification/plot/'))
  import qhist_postprocessors
  return qhist_postprocessors

@memorized
def normalizer():
  sys.path.append(os.path.expandvars('$DIR13/results'))
  import normalizer
  return normalizer


@memorized
def ztau_selected():
  sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
  import selected
  return selected

#===============================================================================
# 015: HIGGS MU TAU
#===============================================================================

@memorized
def rect_selected():
  """
  Import the selected module, rectangular cut, on-demand.
  To be used only during the saving, to not interfere the global sys.path.
  """
  sys.path.append(os.path.expandvars('$DIR15/selection/bkg_rectangular'))
  import rect_selected
  return rect_selected

@memorized
def bkg_ztautau():
  sys.path.append(os.path.expandvars('$DIR15/selection'))
  import bkg_ztautau
  return bkg_ztautau

@memorized
def prod_csc():
  """
  For sensitivity study.
  """
  sys.path.append(os.path.expandvars('$DIR15/upperlim'))
  import prod_csc
  return prod_csc
