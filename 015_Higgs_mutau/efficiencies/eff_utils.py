#!/usr/bin/env python

## Load tool from parent dir
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import ufloat, TTYPES, X_TO_ROOTLABELS, drawers

#===============================================================================

def drawer_hmt_eff(df, ytitle, header=None):
  """
  Common drawer for efficiencies as a function of mH for 4 chanels.
  """
  ## Apply LHCb style
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Sanitize, reorder indices (mass, ascending), and columns (canonical order).
  # df = df.loc[sorted(df.index), list(TTYPES)]
  logger.debug(df)

  ## Begin draw
  c = ROOT.TCanvas()
  c.ownership = False
  xarr = [ufloat(x, 5.) for x in df.index]
  gr   = ROOT.TMultiGraph()
  gr.ownership = False
  for i, (tt, yarr) in enumerate(df.iteritems()):
    g = ROOT.TGraphErrors.from_pair_ufloats(xarr, yarr)
    g.name = g.title = X_TO_ROOTLABELS[tt]
    g.lineColorAlpha = utils.COLORS(i), 0.6
    g.fillColorAlpha = 0,0
    # g.markerSize     = 0
    g.markerColor    = utils.COLORS(i)
    g.markerStyle    = utils.MARKERS(i)
    gr.Add(g, 'PE')
  gr.Draw('A')
  gr.minimum = 0.0
  # gr.maximum = 1.2 # auto floating
  gr.xaxis.limits = 0, 200
  gr.xaxis.title  = 'm_{H} [GeV/c^{2}]'
  gr.yaxis.title  = ytitle

  ## Adjust legend position. Default to upper-left corner
  leg = ROOT.gPad.BuildLegend()
  nrow = len(df.columns)
  if header:
    leg.header = header
    nrow += 1
  leg.x1 = 0.15
  leg.x2 = 0.50
  leg.y1 = 0.93 - 0.06*nrow # dynamic height (tt,regime)
  leg.y2 = 0.93
  leg.fillStyle = 0

  ## Finally
  c.RedrawAxis()
  return gr

#-------------------------------------------------------------------------------

def plot_hmt_eff(df, ytitle, header=None, has_yerr=True, nudge_step=0.2):
  """
  Second version. Change from horizontal err to show the nudging instead.
  """
  ## Apply LHCb style
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  logger.debug(df)

  ## Sanitize some labels
  df = df.copy().rename(X_TO_ROOTLABELS, columns=X_TO_ROOTLABELS)

  ## Hook before Add
  def add_hook(i, g):
    g.lineStyle = utils.LINE(i)
    if has_yerr:
      g.markerStyle = 0 # disable, not to block yerr      

  ## Canvas
  c = ROOT.TCanvas()
  c.ownership = False
  gr, leg = drawers.graphs_eff(df, nudge_step=nudge_step, add_hook=add_hook)
  gr.Draw('A')
  gr.xaxis.title = 'm_{H} [GeV/c^{2}]'
  gr.yaxis.title = ytitle

  ## axis
  gr.xaxis.limits     = 25, 205
  gr.xaxis.ndivisions = 209, False

  ## tune legend
  if leg:
    leg.Draw()
    leg.textSize = 0.06  # for force slim marker
    nrow = len(df.columns)
    if leg.header or header: # existing
     nrow += 1
    if header: # override
      leg.header = header
    leg.x1NDC = 0.16
    leg.x2NDC = 0.30
    leg.y1NDC = 0.92 - 0.06*nrow # dynamic height (tt,regime)
    leg.y2NDC = 0.92
    leg.fillStyle = 0

  # c.frameLineWidth = 1
  gr.yaxis.tickLength = 0.02

  ## Tune canvas
  # gr.yaxis.titleOffset = 0.54
  # c.canvasSize   = 600, 480
  # c.leftMargin   = 0.08
  # c.rightMargin  = 0.03
  # c.bottomMargin = 0.16
  # c.topMargin    = 0.03

  ## finally
  c.RedrawAxis()
  return gr, leg
