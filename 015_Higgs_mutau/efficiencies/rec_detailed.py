#!/usr/bin/env python
"""

Get HMT reconstruction efficiency, detailed version with binning.

Remark:
The `rec` module should in theory be stateless, and independent of Ztautau,
so that it's fully reusable for HMT. However, in practice there are some caveats:
1. The nTracks distribution is hard-coded to selected Ztautau from data, so the
   tracking correct can be a little different.

(for fast mode)
2. The kinematic cut is also hard-coded in Ztautau MC selection `kinematics.py`,
   with precomputed value for each channel. As long as the acceptance cut in 
   those channels between Ztautau and HMT are identical, this is okay.

"""

## Default tools, tethered
from PyrootCK import *
from eff_utils import drawer_hmt_eff, plot_hmt_eff
from HMT_utils import (
  pd, packages, deco, TTYPES, REGIMES, X_TO_ROOTLABELS, TT_TO_DT, DT_TO_TT,
  X_to_latex, printer_minmax, inject_midrule, Idx,
  memorized, pickle_dataframe_nondynamic, gpad_save,
)
from uncertainties import nominal_value, std_dev, correlation_matrix

## Handle
Rec = packages.rec

#===============================================================================

def _runner(tt):
  func = getattr(Rec(), tt+'mu_single') # crux
  return lambda t: Rec().postprocess(func(Rec().preprocess(t)), fast=True)

## Turn on fast mode (discard syst), but add approx error at the end instead.
REC_RUNNERS_FAST = {
  'e' : lambda t: _runner('e' )(t) * ufloat(1, 0.035),
  'h1': lambda t: _runner('h1')(t) * ufloat(1, 0.035),
  'h3': lambda t: _runner('h3')(t) * ufloat(1, 0.060),
  'mu': lambda t: _runner('mu')(t) * ufloat(1, 0.030),
}

#-------------------------------------------------------------------------------
# FULL RUNNER
#-------------------------------------------------------------------------------

@memorized
def get_mc_ekine():
  """
  Because this is a process, channel & mass dependent quantity,
  it need to be rewired for HMT.
  """
  import rec_direct
  return rec_direct.calc_all_stages()['ekine'].unstack().T.rename(columns=TT_TO_DT)

@memorized
def get_mc_etrack2(dt, mass):
  """
  e/h1/h3 is always in the second etrack.
  """
  import rec_direct
  return rec_direct.calc_all_raw().loc[(DT_TO_TT[dt], mass, 'etrack2')]

def runner_full_e(dfin, mass):
  ## Attach
  dt = 'emu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = Rec().reweigh_ntracks.lookup  ( dfin, dt)
  df['corr_trk_e']       = Rec().trk_correction.lookup   ( dfin, dt, 'e'  )
  df['corr_trp_e']       = Rec().lookup_fix_TRPCHI2      ( dfin, dt, 'e'  )
  df['etrack_mu']        = Rec().tracking_muon_dd.lookup ( dfin, dt, 'mu' )
  df['etrack_e']         = get_mc_etrack2(dt, mass) * df.corr_trk_e * df.corr_trp_e
  df['epid_mu']          = Rec().pid_muon_dd.lookup      ( dfin, dt, 'mu' )
  df['epid_e']           = Rec().pid_electron_dd.lookup  ( dfin, dt, 'e'  ) * Rec().pid_electron_dd.syst()['emu_e']
  df['etrig_mu']         = Rec().trigger_muon.lookup     ( dfin, dt, 'mu' )
  df['etrig_e' ]         = Rec().trigger_electron.lookup ( dfin, dt, 'e'  )

  ## Finally
  df['etrack'] = df.etrack_e * df.etrack_mu
  df['ekine']  = get_mc_ekine().loc[mass, dt]
  df['epid']   = df.epid_e   * df.epid_mu
  df['egec']   = Rec().GEC.efficiencies()[dt]
  df['etrig']  = df.etrig_e  + df.etrig_mu - df.etrig_e*df.etrig_mu
  return df


def runner_full_mu(dfin, mass):
  ## Attach
  dt = 'mumu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['etrack_mu1']       = Rec().tracking_muon_dd.lookup( dfin, dt, 'mu1' )
  df['etrack_mu2']       = Rec().tracking_muon_dd.lookup( dfin, dt, 'mu2' )
  df['epid_mu1' ]        = Rec().pid_muon_dd.lookup     ( dfin, dt, 'mu1' )
  df['epid_mu2' ]        = Rec().pid_muon_dd.lookup     ( dfin, dt, 'mu2' )
  df['etrig_mu1']        = Rec().trigger_muon.lookup    ( dfin, dt, 'mu1' )
  df['etrig_mu2']        = Rec().trigger_muon.lookup    ( dfin, dt, 'mu2' )

  ## Finally
  df['etrack'] = df.etrack_mu1 * df.etrack_mu2
  df['ekine']  = get_mc_ekine().loc[mass, dt]
  df['epid']   = df.epid_mu1   * df.epid_mu2
  df['egec']   = Rec().GEC.efficiencies()[dt]
  df['etrig']  = df.etrig_mu1  + df.etrig_mu2 - df.etrig_mu1*df.etrig_mu2
  return df


def runner_full_h1(dfin, mass):
  ## Attach
  dt = 'h1mu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = Rec().reweigh_ntracks.lookup      ( dfin, dt )
  df['corr_trk_pi']      = Rec().trk_correction.lookup       ( dfin, dt, 'pi' )
  df['corr_trp_pi']      = Rec().lookup_fix_TRPCHI2          ( dfin, dt, 'pi' )
  df['etrack_mu']        = Rec().tracking_muon_dd.lookup     ( dfin, dt, 'mu' )
  df['etrack_pi']        = get_mc_etrack2(dt, mass) * df.corr_trk_pi * df.corr_trp_pi
  df['epid_mu']          = Rec().pid_muon_dd.lookup          ( dfin, dt, 'mu' )
  df['epid_pi']          = Rec().pid_hadron_dd.lookup        ( dfin, dt, 'pi' ) * Rec().pid_hadron_aux.syst()[dt]
  df['etrig']            = Rec().trigger_muon.lookup         ( dfin, dt, 'mu' )

  ## Finally
  df['etrack'] = df.etrack_pi * df.etrack_mu
  df['ekine']  = get_mc_ekine().loc[mass, dt]
  df['epid']   = df.epid_pi   * df.epid_mu
  df['egec']   = Rec().GEC.efficiencies()[dt]
  return df

def runner_full_h3(dfin, mass):
  ## Attach
  dt = 'h3mu'
  df = pd.DataFrame()
  df['weight_selection'] = dfin.weight # for misid_lepton tree
  df['weight_nTracks']   = Rec().reweigh_ntracks.lookup ( dfin, dt )
  df['etrack_mu']        = Rec().tracking_muon_dd.lookup( dfin, dt, 'mu'  )
  df['corr_trk_pr1']     = Rec().trk_correction.lookup  ( dfin, dt, 'pr1' )
  df['corr_trk_pr2']     = Rec().trk_correction.lookup  ( dfin, dt, 'pr2' )
  df['corr_trk_pr3']     = Rec().trk_correction.lookup  ( dfin, dt, 'pr3' )
  df['corr_trp_pr1']     = Rec().lookup_fix_TRPCHI2     ( dfin, dt, 'pr1' )
  df['corr_trp_pr2']     = Rec().lookup_fix_TRPCHI2     ( dfin, dt, 'pr2' )
  df['corr_trp_pr3']     = Rec().lookup_fix_TRPCHI2     ( dfin, dt, 'pr3' )
  df['epid_mu']          = Rec().pid_muon_dd.lookup     ( dfin, dt, 'mu'  )
  df['epid_pr1']         = Rec().pid_hadron_dd.lookup   ( dfin, dt, 'pr1' )
  df['epid_pr2']         = Rec().pid_hadron_dd.lookup   ( dfin, dt, 'pr2' )
  df['epid_pr3']         = Rec().pid_hadron_dd.lookup   ( dfin, dt, 'pr3' )
  df['etrig']            = Rec().trigger_muon.lookup    ( dfin, dt, 'mu'  )

  ## Finally
  corr_trk_tauh3     = df.corr_trk_pr1 * df.corr_trk_pr2 * df.corr_trk_pr3
  corr_trk_tauh3    *= df.corr_trp_pr1 * df.corr_trp_pr2 * df.corr_trp_pr3
  df['etrack_tauh3'] = get_mc_etrack2(dt, mass) * corr_trk_tauh3
  df['etrack']       = df.etrack_mu  * df.etrack_tauh3
  df['ekine']        = get_mc_ekine().loc[mass, dt]
  df['epid_tauh3']   = df.epid_pr1  * df.epid_pr2 * df.epid_pr3 * Rec().pid_hadron_aux.syst()[dt]
  df['epid']         = df.epid_mu   * df.epid_tauh3
  df['egec']         = Rec().GEC.efficiencies()[dt]
  return df

#-------------------------------------------------------------------------------

# @deco.concurrent # correlation between channels is lost
def _erec_single(tree, tt, fast):
  """
  Run computation of erec for single channel & mass, at given mode.
  """
  ## Fast mode
  if fast:
    return REC_RUNNERS_FAST[tt](tree)
  ## Full mode. Grab the mass
  mass = int(tree.name.split('_')[-1])
  func = globals()['runner_full_'+tt]
  dfin = Rec().preprocess(tree)
  return Rec().postprocess(func(dfin, mass))

# @deco.synchronized
def _erec_sync(regime, fast):
  """
  Run loop over all ttypes and masses, at given regime & mode.
  """
  acc = {}
  for tt in TTYPES:
    trees = packages.rect_selected().load_trees_higgs_dict(tt, regime)
    for tname, tree in trees.iteritems():
      key = tt, tname
      acc[key] = _erec_single(tree, tt, fast)
  return pd.concat(acc)

@pickle_dataframe_nondynamic
def erec_full(regime):
  return _erec_sync(regime, fast=False)

@pickle_dataframe_nondynamic
def erec_fast(regime):
  return _erec_sync(regime, fast=True)

@pickle_dataframe_nondynamic
def correlation_raw(regime):
  """
  Save the correlation matrix between channels, for each mH.
  SLOW TO THE MAX!

  Nominal only for now!
  """
  se = _erec_sync(regime, fast=False)
  se.index.names = 'tt', 'tname', 'field'
  df  = se.xs('erec', level='field').unstack('tt')
  acc = {}
  for s,df2 in df.groupby(level='tname'):
    arr = df2.xs(s).values
    if not any(x is None for x in arr):
      acc[s] = pd.DataFrame(correlation_matrix(arr))
  return pd.concat(acc)

def correlation_range():
  """
  return approximate correlation range at each element
  """
  print correlation_raw().unstack().mean().unstack().fmt2f
  # print correlation_raw().unstack().max().unstack().fmt1p

#===============================================================================

def collapse_regime_punzi(df, strict=False):
  """
  Collapse the regime index, keeping masses only those belong to that regime
  according to Punzi FoM.
  """
  Lv   = df.index.get_level_values
  idx1 = (Lv('regime')=='lowmass') & (Lv('mass') > 40)
  idx2 = (Lv('regime')=='nominal') & (Lv('mass') > 70)
  idx3 = (Lv('regime')=='tight')   & (Lv('mass') > 90)
  if strict: # collapse with no overlap
    idx1 &= (Lv('mass') < 70)
    idx2 &= (Lv('mass') < 90)
  return df[idx1|idx2|idx3]

@memorized
def efficiencies_raw():
  """
  Collect everything together into one dataframe first, and sanitize.
  """
  df = pd.concat({
    'full': pd.concat({rg:erec_full(rg) for rg in REGIMES}),
    'fast': pd.concat({rg:erec_fast(rg) for rg in REGIMES}),
  })
  df = df.rename(lambda s: s if 'higgs' not in s else int(s.split('_')[-1])).sort_index()
  df.index.names = ['mode', 'regime', 'tt', 'mass', 'quantity']
  return df

@pickle_dataframe_nondynamic
def efficiencies():
  """
  Select only the final erec for speed, prepare for collectors.
  """
  return efficiencies_raw().xs(['erec', 'full'], level=['quantity', 'mode']).unstack('tt')

#===============================================================================
# DRAWER
#===============================================================================

@gpad_save
def draw_erec(tag):
  """
  tag = regime, tt
  Rewire the function this way so that gpad_save is happy.
  """
  ## Fetch the right data
  df0 = collapse_regime_punzi(efficiencies_raw())
  df0 = df0.xs(['full', 'erec'], level=['mode', 'quantity'])
  if tag in REGIMES:
    lv = 'regime'
    header = 'Regime: %s'%X_TO_ROOTLABELS[tag]
  elif tag in TTYPES:
    lv = 'tt'
    header = 'Channel: %s'%X_TO_ROOTLABELS[tag]
  else:
    raise ValueError('Unknown channel/regime.')
  df = df0.xs(tag, level=lv).unstack(0)
  ## Actual draw
  ytitle  = 'Reconstruction efficiency'
  gr, leg = plot_hmt_eff(df, ytitle, header=header)
  gr.maximum = 1.1

#===============================================================================
# LATEX
#===============================================================================

COLS = ['etrack', 'ekine', 'epid', 'egec', 'etrig', 'erec']

def latex_eff():
  df = efficiencies()
  # kill some entries to reduce rows
  df = df[df.index.get_level_values('mass') > 40]
  # finally
  return X_to_latex(df*100.).to_bicol_latex()


def latex_eff_lite():
  """
  Show components of the erec, use printer_minmax to collapse in regime/mass.
  """
  df = collapse_regime_punzi(efficiencies_raw().xs('full', level='mode')).apply(nominal_value)
  df = (df.unstack(['regime', 'mass']).T*100).apply(printer_minmax).unstack('tt')
  df = df.loc[COLS]
  df.index.name = ''
  inject_midrule(df, 5)
  return X_to_latex(df).to_markdown(stralign='center')

## GROUP BY CHANNEL
def _latex_eff_channel(tt):
  fields = {
    'e' : ['etrack', 'etrack_mu' , 'etrack_e'    , 'ekine', 'epid', 'epid_mu' , 'epid_e'    , 'egec', 'etrig', 'etrig_mu' , 'etrig_e'  , 'erec'],
    'mu': ['etrack', 'etrack_mu1', 'etrack_mu2'  , 'ekine', 'epid', 'epid_mu1', 'epid_mu2'  , 'egec', 'etrig', 'etrig_mu1', 'etrig_mu2', 'erec'],
    'h1': ['etrack', 'etrack_mu' , 'etrack_pi'   , 'ekine', 'epid', 'epid_mu' , 'epid_pi'   , 'egec', 'etrig', 'erec'],
    'h3': ['etrack', 'etrack_mu' , 'etrack_tauh3', 'ekine', 'epid', 'epid_mu' , 'epid_tauh3', 'egec', 'etrig', 'erec'],
  }
  conv = {
    'etrack_mu'   : '\\etrk{\\muon}',
    'etrack_mu1'  : '\\etrk{\\muon}',
    'etrack_mu2'  : '\\etrk{\\taumu}',
    'etrack_e'    : '\\etrk{\\taue}',
    'etrack_pi'   : '\\etrk{\\tauh1}',
    'etrack_tauh3': '\\etrk{\\tauh3}',
    #
    'epid_mu'   : '\\epid{\\muon}',
    'epid_mu1'  : '\\epid{\\muon}',
    'epid_mu2'  : '\\epid{\\taumu}',
    'epid_e'    : '\\epid{\\taue}',
    'epid_pi'   : '\\epid{\\tauh1}',
    'epid_tauh3': '\\epid{\\tauh3}',
    #
    'etrig_mu' : '\\etrg{\\muon}',
    'etrig_mu1': '\\etrg{\\muon}',
    'etrig_mu2': '\\etrg{\\taumu}',
    'etrig_e'  : '\\etrg{\\taue}',
  }
  df = efficiencies_raw().xs(['full', tt], level=['mode', 'tt']).unstack('quantity')
  df = collapse_regime_punzi(df)[fields[tt]]
  return X_to_latex(df*100., **conv).to_bicol_latex()

latex_eff_channel_e  = lambda: _latex_eff_channel('e')
latex_eff_channel_mu = lambda: _latex_eff_channel('mu')
latex_eff_channel_h1 = lambda: _latex_eff_channel('h1')
latex_eff_channel_h3 = lambda: _latex_eff_channel('h3')

## GROUP BY FIELD
# def _latex_eff_complete(field):
#   df = collapse_regime_punzi(efficiencies_raw().xs('full', level='mode'), strict=True)
#   df = df.reset_index('regime', drop=True).unstack('tt').xs(field, level='quantity')
#   return X_to_latex(df*100.).to_bicol_latex()

# latex_eff_complete_etrack = lambda: _latex_eff_complete('etrack')
# latex_eff_complete_ekine  = lambda: _latex_eff_complete('ekine')
# latex_eff_complete_epid   = lambda: _latex_eff_complete('epid')
# latex_eff_complete_etrig  = lambda: _latex_eff_complete('etrig')

#===============================================================================

if __name__ == '__main__':
  if '--cache' in sys.argv:
    print efficiencies().fmt1p
    sys.exit()
  if '--latex' in sys.argv:
    for tag in TTYPES+REGIMES:
      draw_erec(tag)
    latex_eff()
    sys.exit()

  ## DEV
  # print get_mc_etrack2('emu', 125)
  # print get_mc_ekine().loc[115, 'emu']
  # print erec_fast('tight')
  print erec_full('nominal').loc[('h1', 'higgs_95')]
  # print erec_full('tight').xs(['h3','etrack_tauh3'], level=[0,2]).fmt1p
  # print efficiencies_raw().xs('erec', level='quantity').unstack('mode').applymap(lambda u: u.rerr*100.).to_string()
  # print efficiencies()
  # print correlation_raw('lowmass')
  # print correlation_raw('nominal')
  # print correlation_raw('tight')
  # print correlation_range()

  ## DRAWER
  # draw_erec('nominal')

  ## Latex
  # print latex_eff()
  # print latex_eff_lite()
  # print latex_eff_complete_etrack()
  # print latex_eff_complete_ekine()
  # print latex_eff_complete_epid()
  # print latex_eff_complete_etrig()
  # print _latex_eff_channel('h3')
  # print latex_eff_channel_e()
  # print latex_eff_channel_mu()
