#!/usr/bin/env python

"""

Gallery of the reconstruction-level variables

"""

from PyrootCK import *
from rec_direct import get_trees

#===============================================================================
# OBSERVATION WITH QHIST
#===============================================================================

def draw_event():
  """
  Draw event-level quantities.
  """
  h = QHist()
  h.params = 'nSPDhits'
  h.xmin   = 0
  h.xmax   = 800
  # h.draw()

def draw_mc():

  pnames0 = ['higgs', 'tau', 'hardmu', 'HMTch']
  pnames = {
    'e' : ['e'],
    'h1': ['pi'],
    'h3': ['pr1', 'pr2', 'pr3', 'tauh3ch'],
    'mu': ['mu'],
  }

  pnames0_reco = ['hardmu']

  for tt in pnames:
    QHist.suffix    = tt
    # QHist.filters   = None # reset filters
    QHist.trees     = get_trees()[tt].tolist()
    QHist.st_line   = False
    QHist.xbin      = 100
    QHist.auto_name = True
    QHist.batch     = True

    for pname in pnames0+pnames[tt]:
      h = QHist()
      h.params  = pname+'_MCPT'
      h.xmin    = 0
      h.xmax    = 120e3
      # h.draw()

      h = QHist()
      h.params  = pname+'_MCETA'
      h.xmin    = 1
      h.xmax    = 8
      # h.draw()

      h = QHist()
      h.params  = pname+'_MCM'
      h.xmin    = 0
      h.xmax    = 200e3
      # h.draw()

      h = QHist()
      h.params = pname+'_MCP'
      h.xmin   = 0
      h.xmax   = 2e6
      # h.draw()

    ## Need a reco object
    # QHist.filters = pname+'_ID!=0'
    for pname in pnames0_reco + pnames[tt]:
      h = QHist()
      h.params = pname+'_ETA'
      h.xmin   = 1
      h.xmax   = 6
      # h.draw()

      h = QHist()
      h.params = pname+'_PT'
      h.xmin   = 1e3
      h.xmax   = 120e3
      # h.draw()

      if pname == 'tauh3ch':
        pass
        
      else:
      ## even more exclusive for true reco particle
        h = QHist()
        h.params = pname+'_TRCHI2DOF'
        h.xmin   = 0.1
        h.xmax   = 3
        # h.draw()

        h = QHist()
        h.name   = pname+'_iso'
        h.params = pname+'_0.50_cc_IT'
        h.xmin   = 0.
        h.xmax   = 1.
        h.draw()

#===============================================================================

if __name__ == '__main__':
  # draw_mc()
