#!/usr/bin/env python
"""
Return the approximation of the Punzi figure-of-merit for current selections

pseudo-Punzi = effS / sqrt(1 + N)
"""

import selection # tethered
import eff_utils
from HMT_utils import (
  ROOT, sys, pd, Idx, REGIMES, TTYPES, X_TO_ROOTLABELS,
  df_from_path, memorized, gpad_save, drawers
)

#===============================================================================

@memorized
def results():
  ## Load data
  path = '$DIR15/selection/bkg_rectangular/rect_context'
  tag  = 'make_context_all'
  nobs = df_from_path(tag, path).loc[Idx[:,:,'selection','DATA','evt','OS']]
  nobs = nobs.loc[list(REGIMES)] # only active regime

  ## apply caculation
  def fom(df):
    df = df.unstack('mass')
    df['nobs'] = nobs
    # return df.apply(lambda se: se[0].n/(1+se[1])**0.5, axis=1)
    return df.apply(lambda se: se[0].n/(1+se[1]**0.5), axis=1)
  esel = selection.efficiencies().stack()
  return esel.groupby(level='mass').apply(fom)

#-------------------------------------------------------------------------------

@gpad_save
def draw_punzi(tag):
  ## Fetch the right data
  df0 = results()
  if tag in df0.index.levels[1]: # regime
    df = df0.xs(tag, level=1).unstack('tt')
    header = 'Regime: %s'%X_TO_ROOTLABELS[tag]
  elif tag in df0.index.levels[2]: # tt
    df = df0.xs(tag, level=2).unstack('regime')
    header = 'Channel: %s'%X_TO_ROOTLABELS[tag]
  else:
    raise ValueError('Unknown channel/regime.')
  ## Actual draw
  ylabel  = '#varepsilon^{Signal}_{sel} / 1+#sqrt{N}'
  gr, leg = eff_utils.plot_hmt_eff(df, ylabel, header=header, has_yerr=False)
  gr.maximum = df.max().max() * 1.4

#===============================================================================

if __name__ == '__main__':
  if '--latex' in sys.argv:
    for tag in TTYPES+REGIMES:
      draw_punzi(tag)
    sys.exit()

  ## DEV
  # print results().unstack('regime').max(axis=1).unstack('tt')
