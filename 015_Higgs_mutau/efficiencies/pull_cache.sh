#!/bin/bash

rsync -rav \
  --include=*/ \
  --include='*/*.df' \
  --exclude=* \
  lpheb:~/analysis/015_Higgs_mutau/efficiencies/ .
