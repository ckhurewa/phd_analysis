#!/usr/bin/env python

"""

Calculating the acceptance of Higgs -> mu tau signal,
mainly as a function of Higgs mass.

"""

## Default tools
from PyrootCK import *
from PyrootCK.mathutils import EffU_unguard
sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
import eff_utils # tethered
from HMT_utils import TTYPES, pd, gpad_save, intv_to_mid, memorized, X_to_latex

## 8TeV, fast (calculated acceptance online)
# JID = 5272  # 2e5 x 500, forgot tauh3 mass cut
# JID = 5304  # 1e5 x 100, fixed tauh3 mass cut, flexible lower mass bound
# JID = '5471-5472' # individualized channel-mass TProf2D-fid-acc

## 3 fid def x 3 acc def. 10M, all masses
# JID = 5473 # MSTW2008nlo90cl (21141)
JID = 5477 # CT10nnlo (11200)
# JID = 5478 # CT10(10800)

## 13TeV Sensitivity study
JID13 = 5505

#===============================================================================

# def efficiencies():
#   """
#   For 5304
#   """
#   ## Load the TProfile2D
#   fin  = import_file(JID)
#   prof = fin.Get('acc_no_min_mass')
#   fin.ownership = False
#   prof.ownership = False

#   ## collapse 2 bins together to match the m_H masses
#   prof.RebinX(2)

#   ## Convert to dataframe, sanitize
#   df = prof.dataframe().T.rename(intv_to_mid)
#   ## approximate uncertainty of 1.61% like Ztautau
#   # TODO for more accurate syst
#   df = df * ufloat(1, 0.0161)
#   df.name = 'Acceptance'
#   return df[list(TTYPES)]

def schema1(get):
  ## Calculate 4 eff: (4pi, lhcb) x (nominmass, withminmass)
  inacc_withminmass = get(1,3)+get(2,3)+get(3,3)
  inacc_nominmass   = get(1,2)+get(2,2)+get(3,2) + inacc_withminmass
  infid_lhcbpt      = get(3,1)+get(3,2)+get(3,3)
  infid_lhcb        = get(2,1)+get(2,2)+get(2,3) + infid_lhcbpt
  infid_4pi         = get(1,1)+get(1,2)+get(1,3)
  return pd.Series({
    ('4pi'    ,'nominmass'  ): EffU_unguard(infid_4pi   , inacc_nominmass),
    ('4pi'    ,'withminmass'): EffU_unguard(infid_4pi   , inacc_withminmass),
    ('lhcb'   ,'nominmass'  ): EffU_unguard(infid_lhcb  , inacc_nominmass),
    ('lhcb'   ,'withminmass'): EffU_unguard(infid_lhcb  , inacc_withminmass),
    ('lhcb_pt','nominmass'  ): EffU_unguard(infid_lhcbpt, inacc_nominmass),
    ('lhcb_pt','withminmass'): EffU_unguard(infid_lhcbpt, inacc_withminmass),
  })

def schema2(get):
  """
  J5505, with geo-acceptance.
  Suitable for 13TeV check
  """
  inacc_withminmass = get(1,4)+get(2,4)+get(3,4)
  inacc_nominmass   = get(1,3)+get(2,3)+get(3,3) + inacc_withminmass
  ingeoacc          = get(1,2)+get(2,2)+get(3,2) + inacc_nominmass
  infid_lhcbpt      = get(3,1)+get(3,2)+get(3,3)+get(3,4)
  infid_lhcb        = get(2,1)+get(2,2)+get(2,3)+get(2,4) + infid_lhcbpt
  infid_4pi         = get(1,1)+get(1,2)+get(1,3)+get(1,4) + infid_lhcb
  return pd.Series({
    ('4pi', 'ingeoacc'): EffU_unguard(infid_4pi, ingeoacc),
  })

@memorized
def efficiencies_raw(jid, schema=schema1):
  """
  For 5471-5472.root
  """
  acc = {}
  fin = import_file(jid)
  for key in fin.keys:
    th2 = key.ReadObj()
    get = th2.GetBinContent
    tt, mass = th2.name.split('_')[0:2] # some new version has PDFid
    acc[(tt, int(mass))] = schema(get)
  ## finally
  se = pd.concat(acc)
  se.index.names = 'tt', 'mass', 'geo', 'acc'
  return se

def efficiencies():
  """
  Final eacc to be exported for upperlim.
  Include the PDF uncertainty here
  """
  eff = efficiencies_raw(JID).xs('withminmass', level='acc')
  ## Syst from Aurelio, using MSTW2008.
  syst_ll  = ufloat(1., 0.013)
  syst_lh1 = ufloat(1., 0.019)
  syst_lh3 = ufloat(1., 0.015)
  syst = pd.Series({
    'e': syst_ll,
    'h1': syst_lh1,
    'h3': syst_lh3,
    'mu': syst_ll,
  })
  eff = eff.groupby(level='tt').apply(lambda df: df * syst[df.name])
  return eff.unstack('tt').swaplevel('mass', 'geo').sort_index()

def eff_13tev():
  """
  For 13TeV sensitivity check, only geometrical acceptance.
  """
  eff = efficiencies_raw(JID13, schema2).xs(['4pi', 'ingeoacc'], level=['geo', 'acc'])
  eff.name = 'eacc'
  ## assume 5% syst, good enough
  return eff * ufloat(1, 0.05)

#===============================================================================
# DRAWERS
#===============================================================================

@gpad_save
def draw_eacc_lhcb():
  eff = efficiencies().loc['lhcb'] * 100.
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance [%]')
  gr.maximum = 120

@gpad_save
def draw_eacc_4pi():
  eff = efficiencies().loc['4pi'] * 100.  # in percent
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance [%]')
  gr.maximum = 4.0
  # mimic right align
  leg.x1NDC  = 0.80
  leg.x2NDC  = 0.90
  leg.primitives[0].textAlign = 31

@gpad_save
def draw_eacc_13tev():
  eff = eff_13tev().unstack('tt') * 100.
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Geometrical acceptance [%]', nudge_step=0.)
  gr.maximum = 12.0
  leg.header = 'LHCb 13TeV'
  leg.x1NDC  = 0.70
  leg.x2NDC  = 0.90

  pass

#===============================================================================
# LATEX
#===============================================================================

def latex_eff_4pi():
  df  = efficiencies().loc['4pi']*100.
  idx = df.index.get_level_values('mass') > 40
  return X_to_latex(df[idx]).to_bicol_latex()

def latex_eff_lhcb():
  df  = efficiencies().loc['lhcb']*100.
  idx = df.index.get_level_values('mass') > 40
  return X_to_latex(df[idx]).to_bicol_latex()

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv:
    draw_eacc_lhcb()
    draw_eacc_4pi()
    sys.exit()

  ## DEV
  # print efficiencies_raw(5499).xs(['4pi', 'withminmass'], level=['geo','acc']).unstack('tt').fmt2p
  # print efficiencies_raw(5505, schema2).unstack('tt').fmt1p
  # print efficiencies_raw(JID)
  # print efficiencies().fmt3p
  # print eff_13tev().unstack('tt').fmt1p

  ## Drawer
  # draw_eacc()
  # draw_eacc_13tev()

  ## Latex
  # print latex_eff_4pi()
  # print latex_eff_lhcb()
