#!/usr/bin/env python

"""

Calculating the acceptance of Higgs -> mu tau signal,
mainly as a function of Higgs mass.

"""

## Default tools
import os, sys
sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
import eff_utils # tethered
from HMT_utils import (
  TTYPES, pd, re, intv_to_mid, EffU, EffU_unguard, X_to_latex, join,
  pickle_dataframe_nondynamic, memorized, gpad_save, 
)

#===============================================================================

# no mass, PT cut
CUT_FIDUCIAL = join(
  'mu_ETA  > 2.0',
  'mu_ETA  < 4.5',
  'tau_ETA > 2.0',
  'tau_ETA < 4.5',
)

CUTS_GEOACC = {
  'e' : ['mu_ETA > 2.', 'mu_ETA < 4.5', 'tauvis_ETA > 2.', 'tauvis_ETA < 4.5'],
  'mu': ['mu_ETA > 2.', 'mu_ETA < 4.5', 'tauvis_ETA > 2.', 'tauvis_ETA < 4.5'],
  'h1': ['mu_ETA > 2.', 'mu_ETA < 4.5', 'tauvis_ETA > 2.', 'tauvis_ETA < 4.5'],
  'h3': ['mu_ETA > 2.', 'mu_ETA < 4.5', 'pr1_ETA>2.','pr1_ETA<4.5','pr2_ETA>2.','pr2_ETA<4.5','pr3_ETA>2.','pr3_ETA<4.5'],
}

# no lower mass cut
CUTS_ACCEPTANCE = {
  'e' : [ CUTS_GEOACC['e'] , 'mu_PT > 20', 'tauvis_PT > 5'],
  'h1': [ CUTS_GEOACC['h1'], 'mu_PT > 20', 'tauvis_PT > 10'],
  'mu': [ CUTS_GEOACC['mu'],
    { # dimuon
      ('mu_PT > 20', 'tauvis_PT > 5'),
      ('mu_PT > 5' , 'tauvis_PT > 20'),
    }
  ],  
  'h3': [ CUTS_GEOACC['h3'],
    'mu_PT      > 20',
    'pr1_PT     > 6',
    'pr3_PT     > 1',
    'tauvis_PT  > 12',
  ]
}

#===============================================================================
# TREES
#===============================================================================

## 8TeV
# jid = 4568
# JID = 4570
# JID = 5222  # extend range to [30, 200]
# JID = 5315 # POWHEG-BOX hard event, then Pythia shower
JID = 5324 # Like above, but smaller 
# the veto may be biased...

## 13TeV
# JID = 4657

@memorized
def get_tree(tt):
  t = import_tree('tau%s'%tt, JID)
  t.SetAlias('Fid', CUT_FIDUCIAL)
  t.SetAlias('Acc', join(CUTS_ACCEPTANCE[tt]))
  t.SetAlias('Geo', join(CUTS_GEOACC[tt]))
  t.SetAlias('X'  , 'Fid*1 + Acc*2') # Helper column
  t.estimate = t.entries+1
  return t

def get_trees():
  return [get_tree(tt) for tt in TTYPES]

#===============================================================================
# DRAW
#===============================================================================

def draw_eacc_mass():
  """
  This provide the transformation from unbinned ntuple to binned histo in mass.
  """
  h = QHist()
  h.name    = 'eacc_mass'
  h.trees   = get_trees()
  h.params  = 'Acc: higgs_M'
  h.options = 'prof'
  h.xmin    = 20
  h.xmax    = 200
  h.xbin    = (h.xmax-h.xmin)/10
  h.ymin    = 0
  h.ymax    = 0.05
  h.draw()

#===============================================================================

def extract_single(tree):
  """
  Given a single tree, apply the selection to collect number of entries at
  different stages. Also bin the result in mass of Higgs.

  Discard the uncertainty for now, it's jsut a number.
  """
  h = ROOT.TH2F('htemp', 'htemp', 20, 0, 200, 4, 0, 4)
  h.Reset()
  tree.Draw('X: higgs_M >> htemp', '', 'goff')
  df = h.dataframe().T
  df.columns = ['NotFid_NotAcc', 'Fid_NotAcc', 'NotFid_Acc', 'Fid_Acc']
  return df.applymap(uncertainties.nominal_value)

def fast_eff(ttype, mass, cut):
  """
  Fast method to calculate efficiency from tree of Higgs at given ttype,mH.
  """
  mcut = '({0} > higgs_M-4) & ({0} < higgs_M+4)'.format(mass)
  tree = get_tree(ttype)
  n0   = tree.GetEntries(join(mcut))
  n1   = tree.GetEntries(join(mcut, cut))
  return 1.*n1/n0

@pickle_dataframe_nondynamic
def extract_all():
  """
  Loop over all channels. This is shared result between two eacc.
  """
  return pd.concat({tt:extract_single(get_tree(tt)) for tt in TTYPES})

@memorized
def efficiencies():
  """
  Calculate 2 acceptances
  - 4pi : Inclusive production
  - lhcb: Forward fiducial (2 < eta < 4.5)
  """
  df0 = extract_all().rename(intv_to_mid)
  func_4pi  = lambda se: EffU(se.sum(), se.NotFid_Acc+se.Fid_Acc)
  func_lhcb = lambda se: EffU_unguard(se.Fid_Acc+se.Fid_NotAcc, se.NotFid_Acc+se.Fid_Acc)
  se = pd.concat({
    '4pi' : df0.apply(func_4pi, axis=1),
    'lhcb': df0.apply(func_lhcb, axis=1),
  })
  se.index.names = 'geo', 'tt', 'mass'
  ## veto bad value, then return
  se = se.apply(lambda x: x if x.s<x.n else pd.np.nan).dropna()
  return se.unstack('tt')

@pickle_dataframe_nondynamic
def eff_egeo():
  """
  For set of mass to be compared with 13TeV
  """
  acc = {}
  for tt in TTYPES:
    for mass in 25, 45, 85, 125, 195:
      acc[(tt, mass)] = fast_eff(tt, mass, 'Geo')
  se = pd.Series(acc)
  se.index.names = 'tt', 'mass'
  return se

#===============================================================================
# DRAWERS
#===============================================================================

@gpad_save
def draw_eacc_lhcb():
  eff = efficiencies().loc['lhcb'] * 100.
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance [%]')
  gr.maximum = 120

@gpad_save
def draw_eacc_4pi():
  eff = efficiencies().loc['4pi'] * 100.  # in percent
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance [%]')
  gr.maximum = 4.0
  # mimic right align
  leg.x1NDC  = 0.80
  leg.x2NDC  = 0.90
  leg.primitives[0].textAlign = 31

#===============================================================================
# LATEX
#===============================================================================

def latex_eff_4pi():
  df  = efficiencies().loc['4pi']*100.
  idx = df.index.get_level_values('mass') > 40
  return X_to_latex(df[idx]).to_bicol_latex()

def latex_eff_lhcb():
  df  = efficiencies().loc['lhcb']*100.
  idx = df.index.get_level_values('mass') > 40
  return X_to_latex(df[idx]).to_bicol_latex()

#===============================================================================

if __name__ == '__main__':
  if '--latex' in sys.argv:
    draw_eacc_lhcb()
    draw_eacc_4pi()
    sys.exit()

  ## DEV
  # print get_tree('mu')
  # print trees()
  # print extract_single(get_tree('mu'))
  # print extract_all()
  # print efficiencies().fmt3p
  # print eff_egeo().unstack('tt').fmt1p

  ## DRAWER
  # draw_eacc_mass()
  # draw_eacc_from_4pi()
  # draw_eacc_from_fid()

  ## LATEX
  # print latex_eff_4pi()
  # print latex_eff_lhcb()
