#!/usr/bin/env python

"""

Study the effect of PDF systematics by scanning the efficiencies over the range 
of PDF members.

"""

import re
from glob import glob
from PyrootCK import *
from PyrootCK.mathutils import EffU

sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
import eff_utils # tethered
from HMT_utils import (
  TTYPES, pd, pickle_dataframe_nondynamic, 
  gpad_save, memorized, X_to_latex, deco
)

## List of input jobs
INPUTS = {
  5482: 'CT14nnlo',              # 13000+57
  5486: 'NNPDF31_nnlo_as_0118',  # 303600+101
  5488: 'MMHT2014nnlo68cl',      # 25300+51
  5489: 'MSTW2008nnlo68cl',      # 21200+41
  5500: 'MMHT2014nlo68cl',
  5501: 'NNPDF31_nlo_as_0118',
}

## normalize the sigma level to 1.
SIGMA = {
  'CT14nnlo'            : 1.645,
  'MMHT2014nlo68cl'     : 1.,
  'MMHT2014nnlo68cl'    : 1.,
  'MSTW2008nnlo68cl'    : 1.,
  'NNPDF31_nlo_as_0118' : 1.645,
  'NNPDF31_nnlo_as_0118': 1.645,
}

## Which PDF to use for the final result?
FINAL_PDF = 'MMHT2014nlo68cl'

#===============================================================================

@deco.concurrent
def read_merged(jid):
  fin = import_file(jid)
  acc = {}
  for key in fin.keys:
    h    = key.ReadObj()
    df   = h.dataframe()
    nume = df.loc['inacc-withminmass'].sum().n
    deno_4pi  = df.sum().sum().n
    deno_lhcb = df[['infid_eta', 'infid_etapt']].sum().sum().n
    tt, mass, pdfid = key.name.split('_')
    mass  = int(mass)
    pdfid = int(pdfid)
    acc[(tt, mass, '4pi' , pdfid)] = EffU(deno_4pi , nume)
    acc[(tt, mass, 'lhcb', pdfid)] = EffU(deno_lhcb, nume)
  ## Package & return
  se = pd.Series(acc)
  se.index.names = 'tt', 'mass', 'geo', 'pdfid'
  return se

@deco.synchronized
def read_sync():
  acc = {}
  for jid, pdfname in INPUTS.iteritems():
    logger.info('Loading: [%i] %s'%(jid, pdfname))
    acc[pdfname] = read_merged(jid)
  return acc

@pickle_dataframe_nondynamic
def read_all():
  ## Package & return
  se = pd.concat(read_sync())
  se.index = se.index.set_names('pdf', level=0)
  return se

#===============================================================================

def process_single(se0):
  ## Grabbing input
  se = se0.loc[se0.name].apply(lambda u: u.n)
  n  = len(se)
  f0 = se.iloc[0]

  ## guess: first half is fplus, second half is minus
  # fplus  = se.iloc[1:n/2+1].reset_index(drop=True)
  # fminus = se.iloc[n/2+1:n].reset_index(drop=True)
  ## guess: alternating plus/minus
  fplus  = se.iloc[1::2].reset_index(drop=True)
  fminus = se.iloc[2::2].reset_index(drop=True)
  
  ## Calculate the sum of uncertainties
  div  = SIGMA[se0.name[0]]
  eacc = pd.DataFrame({'f+': fplus, 'f-': fminus})
  res  = pd.DataFrame()
  res['sig+'] = eacc.apply(lambda se: max([se['f+']-f0, se['f-']-f0, 0])/div, axis=1)
  res['sig-'] = eacc.apply(lambda se: max([f0-se['f+'], f0-se['f-'], 0])/div, axis=1)
  res['sig']  = eacc.apply(lambda se: 0.5*(se['f+']-se['f-'])/div, axis=1)

  ## Collect the result, return in percent!
  res = (res**2).sum()**0.5
  res['f0'] = f0
  return res*100.

@pickle_dataframe_nondynamic
def result_raws():
  ## load, group & study uncer
  groups = read_all().groupby(level=['pdf','tt','mass','geo'])
  res    = groups.apply(process_single).unstack()
  res['eff']  = res.apply(lambda se: ufloat(se.f0, se.sig), axis=1)
  res['rerr'] = res.apply(lambda se: se.sig/se.f0*100., axis=1)
  return res

def relative_uncertainties():
  """
  Reduce to choice of PDF, in absolute for compat with other script
  """
  return result_raws()['rerr'].unstack('pdf')[FINAL_PDF].unstack('tt')/100.

@memorized
def efficiencies():
  """
  Final interface for the collectors

  Note: Even though this sample is only 1Mevt at each mass point (compare to 
  the `read_fast` which has 10Mevt), there is enough systematic uncertainty
  hre such that it completely eclipse the stat uncer.
  """
  eff = result_raws().loc[FINAL_PDF]['eff'].unstack('tt')/100.
  eff = eff.swaplevel().sort_index()
  return eff

#===============================================================================
# DRAWERS
#===============================================================================

@gpad_save
def draw_eacc_pdf(tt):
  eff = result_raws()['eff'].xs(['4pi',tt], level=['geo','tt']).unstack('pdf')
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance [%]')
  gr.maximum = 4.
  leg.x1NDC = 0.60
  leg.x2NDC = 0.90

@gpad_save
def draw_rerr_4pi_pdf(tt):
  rerr = result_raws()['rerr'].xs(['4pi',tt], level=['geo','tt']).unstack('pdf')
  gr, leg = eff_utils.plot_hmt_eff(rerr, 'Acc Systematics [%]')
  gr.maximum = 15.

@gpad_save
def draw_eacc_lhcb():
  eff = efficiencies().loc['lhcb']
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance')
  gr.maximum = 1.2

@gpad_save
def draw_eacc_4pi():
  eff = efficiencies().loc['4pi']
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Acceptance')
  gr.maximum = 0.04
  # mimic right align
  leg.x1NDC  = 0.80
  leg.x2NDC  = 0.90
  leg.primitives[0].textAlign = 31

def draw_all():
  for tt in TTYPES:
    draw_eacc_pdf(tt)
    draw_rerr_4pi_pdf(tt)
  draw_eacc_lhcb()
  draw_eacc_4pi()

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv:
    draw_all()
    sys.exit()

  # print read_merged(5499)
  # print read_all()
  # print result_raws()
  # print result_raws()['eff'].xs(['4pi'], level=['geo']).unstack('pdf').fmt2f.to_string()
  # print raw_rerr().fmt2f.to_string()
  print efficiencies().fmt3p

  ## Drawer
  # draw_eacc_pdf('h3')
  # draw_rerr_4pi_pdf('h3')
  # draw_eacc_lhcb()
  # draw_eacc_4pi()
  # draw_all()

  ## Latex
  # print latex_eff_4pi()
  # print latex_eff_lhcb()
