#!/usr/bin/env python

"""

Gallery for the acceptance-level Higgs.

Compare the different production:
- LO from Pythia
- NLO from POWHEG-BOX

"""

## default tools
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import TTYPES

#===============================================================================

def trees(tt):
  trees = [
    import_tree('tau'+tt, 5324).SetTitle('POWHEG-BOX (gg_H)'),
    import_tree('tau'+tt, 5329).SetTitle('Pythia8 (gg2H)'),
    import_tree('tau'+tt, 5330).SetTitle('Pythia8 (gg2Hg)'),
  ]
  ## alias for mH=125 only
  for tree in trees:
    tree.SetAlias('mH125', '(higgs_M > 125-2) & (higgs_M < 125+2)')
  return trees

#===============================================================================
# COMPARE PRODUCTION MODE
#===============================================================================

def compare_mH125_indpt():
  """
  Compare different production mode at fixed mH = 125GeV.
  """
  QHist.reset()
  QHist.auto_name = True
  QHist.prefix    = 'prod'
  QHist.filters   = 'mH125'
  QHist.trees     = trees('mu')
  QHist.xbin      = 100

  h = QHist()
  h.params = 'higgs_ETA'
  h.xmin   = -60
  h.xmax   = 60
  h.draw()

  h = QHist()
  h.params = 'higgs_PT'
  h.xmin   = 0
  h.xmax   = 100
  h.draw()

  h = QHist()
  h.params = 'higgs_PZ'
  h.xmin   = -1e3
  h.xmax   = +1e3
  h.draw()

  h = QHist()
  h.params = 'mu_ETA'
  h.xmin   = -6
  h.xmax   = +6
  h.draw()

  h = QHist()
  h.params = 'mu_PT'
  h.xmin   = 0
  h.xmax   = 200
  h.draw()


def compare_mH125_dpt():
  """
  Compare different production mode at fixed mH = 125GeV.
  """
  QHist.reset()
  QHist.auto_name = True
  QHist.prefix    = 'prod'
  QHist.filters   = 'mH125'
  QHist.xbin      = 100

  for tt in TTYPES:
    QHist.trees  = trees(tt)
    QHist.suffix = tt

    h = QHist()
    h.params = 'higgsvis_ETA'
    h.xmin   = -6
    h.xmax   = +6
    # h.draw()

    h = QHist()
    h.params = 'higgsvis_M'
    h.xmin   = 0
    h.xmax   = 130
    # h.draw()

    h = QHist()
    h.params = 'higgsvis_PT'
    h.xmin   = 1
    h.xmax   = 1e3
    h.xlog   = True
    # h.draw()

    ## tauvis (e, h1, h3), skip mu 
    if tt == 'mu':
      continue
    QHist.suffix = ''

    h = QHist()
    h.params = 'tauvis_ETA'
    h.name   = h.params.replace('tauvis', tt)
    h.xmin   = -6
    h.xmax   = +6
    # h.draw()

    h = QHist()
    h.params = 'tauvis_PT'
    h.name   = h.params.replace('tauvis', tt)
    h.xmin   = 1e-1
    h.xmax   = 300
    h.xlog   = True
    h.draw()

    if tt == 'h3':
      h = QHist()
      h.params = 'tauvis_M'
      h.name   = h.params.replace('tauvis', tt)
      h.xmin   = 0
      h.xmax   = 2
      # h.draw()

#===============================================================================

def prelim():
  QHist.reset()
  QHist.auto_name = True
  QHist.trees     = trees('h1')

  h = QHist()
  h.params = 'higgs_ETA'
  h.draw()

#===============================================================================

if __name__ == '__main__':
  pass

  # print trees('mu')

  ## Production mode
  compare_mH125_indpt()
  # compare_mH125_dpt()
