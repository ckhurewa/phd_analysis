#!/bin/bash

## Init
set -e
PROJ=${HOME_EPFL}/Papers/007_HiggsLFV/figs

## Generator-level gallery
MODULE=gallery_gen
rm -rf $MODULE/*.root
rm -rf $MODULE/*.pdf
rm -rf $MODULE/minipdf
./$MODULE.py
cd $MODULE
QHistPdfExporter.py *.root \
  --width=360 --height=240 \
  --legend-header='LHCb preliminary' \
  --pad-margin-bottom=0.16 \
  --pad-margin-left=0.12 \
  --legend-top=0.97 \
  --legend-left=0.7 \
  --legend-bottom=0.67 \
  --legend-text-size=11
rm -rf $PROJ/$MODULE
mv -v minipdf $PROJ/$MODULE
