#!/usr/bin/env python


#===============================================================================
# COMPARE MULTIPLE MASS
#===============================================================================

def _draw_multimass(tree, name, param, ymin, ymax):
  """
  Drawer in violin plot
  """
  ## actual draw
  h = ROOT.TH2F(name, param, 20, 10, 210, 200, ymin, ymax)
  tree.Draw('%s: higgs_M >> %s'%(param, name))
  h.fillColor      = ROOT.kGray
  h.markerStyle    = 20
  h.markerSize     = 0.5
  h.lineColorAlpha = ROOT.kRed, 0
  h.xaxis.title    = 'm_{H} [GeV]'
  h.Draw('VIOLIN')
  ROOT.gPad.SaveAs('gallery/multimass/%s.pdf'%name)

#-------------------------------------------------------------------------------

def compare_multmass1():
  """
  Compare fixed mode at different mass
  1. Channel-independent quantities
  """
  tree = trees('mu')[0] # POWHEG

  queues = {
    'higgs_ETA': ('higgs_ETA'             , -10 , 10 ),
    'higgs_PZ' : ('higgs_PZ'              , -1e3, 1e3),
    'higgs_PT' : ('TMath::Log10(higgs_PT)', 0   , 3  ),
    'mu_ETA'   : ('mu_ETA'                , -6  , 6  ),
    'mu_PZ'    : ('mu_PZ'                 , -200, 200),
    'mu_PT'    : ('TMath::Log10(mu_PT)'   , 0   , 3  ),
  }

  ## Loop over queues
  for name, (param, ymin, ymax) in queues.iteritems():
    _draw_multimass(tree, name, param, ymin, ymax)

#-------------------------------------------------------------------------------

def compare_multmass2():
  """
  Compare fixed mode at different mass
  2. Channel-dependent sub quantities
  """
  queues = {
    'e': {
      'e_ETA': 'tauvis_ETA',
      'e_PZ' : 'tauvis_PZ',
      'e_PT' : 'TMath::Log10(tauvis_PT)',
    },
    'h1': {
      'h1_ETA': 'tauvis_ETA',
      'h1_PZ' : 'tauvis_PZ',
      'h1_PT' : 'TMath::Log10(tauvis_PT)',
    },
    'h3': {
      'pr1_ETA': 'pr1_ETA',
      'pr1_PZ' : 'pr1_PZ',
      'pr1_PT' : 'TMath::Log10(pr1_PT)',
      'pr2_ETA': 'pr2_ETA',
      'pr2_PZ' : 'pr2_PZ',
      'pr2_PT' : 'TMath::Log10(pr2_PT)',
      'pr3_ETA': 'pr3_ETA',
      'pr3_PZ' : 'pr3_PZ',
      'pr3_PT' : 'TMath::Log10(pr3_PT)',
      'h3_ETA' : 'tauvis_ETA',
      'h3_PZ'  : 'tauvis_PZ',
      'h3_PT'  : 'TMath::Log10(tauvis_PT)',
      'h3_M'   : 'tauvis_M',
    }
  }

  ## rather common phasespace
  yaxis = {
    'ETA': (-6 , 6 ),
    'PZ' : (-50, 50),
    'PT' : (-1 , 2.5),
    'M'  : (0  , 2),
  }

  ## Loop over queues
  for tt, spec in queues.iteritems():
    tree = trees(tt)[0] # POWHEG
    for name, param in spec.iteritems():
      functor = name.split('_')[-1]
      ymin, ymax = _multimass_yaxis[functor]
      _draw_multimass(tree, name, param, ymin, ymax)

#-------------------------------------------------------------------------------

def compare_multmass3():
  """
  Compare fixed mode at different mass
  2. Channel-dependent visible (sum of charged children) Higgs 
  """
  queues = {
    'higgsvis_%s_ETA': ('higgsvis_ETA'              , -6  , 6  ),
    'higgsvis_%s_M'  : ('higgsvis_M'                , 0   , 200),
    'higgsvis_%s_PZ' : ('higgsvis_PZ'               , -500, 500),
    'higgsvis_%s_PT' : ('TMath::Log10(higgsvis_PT)' , 0   , 3  ),
  }
  for tt in TTYPES:
    tree = trees(tt)[0]
    for name, args in queues.iteritems():
      _draw_multimass(tree, name%tt, *args)


#===============================================================================

if __name__ == '__main__':
  ## Multiple masses
  # compare_multmass1()
  # compare_multmass2()
  # compare_multmass3()
