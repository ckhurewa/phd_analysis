#!/bin/tcsh
# USAGE: mass, nevt, seed
# >> ./powheg_pythia8.csh 125 1000 0 25300 4000d0

## For the merge Ganga output stream
date
pwd
ls -l
source ~/bin/setup_powheg-box.csh

## Prep environ, get the ready-grid.
cp ~/analysis/015_Higgs_mutau/efficiencies/acceptance/generators/powheg/powheg0.input .
cp ~/analysis/015_Higgs_mutau/efficiencies/acceptance/generators/pythia_utils.py .
cp ~/analysis/015_Higgs_mutau/efficiencies/acceptance/generators/shower_pythia.py .
cp ~/analysis/015_Higgs_mutau/efficiencies/acceptance/generators/shower_powheg.py .

## Reuse grid <-- NO: I have different mass points
# mv usegrid-powheg.input powheg.input
cp powheg0.input powheg.input

## Add the unique gen seed
echo "\nhmass $1"   >> powheg.input
echo "\nnumevts $2" >> powheg.input
echo "\niseed $3"   >> powheg.input
echo "\nlhans1 $4"  >> powheg.input
echo "\nlhans2 $4"  >> powheg.input
echo "\nebeam1 $5"  >> powheg.input
echo "\nebeam2 $5"  >> powheg.input

## Run 
gg_H powheg.input
echo `date`
ls -l

## Use Pythia to filter, shower, and write to ROOT.
## pass the ttype to control tau decay channel
## pass mass for the sake of renaming output file only.
./shower_powheg.py --full --ttype=e  --tag=e_$1_$4
./shower_powheg.py --full --ttype=mu --tag=mu_$1_$4
./shower_powheg.py --full --ttype=h1 --tag=h1_$1_$4
./shower_powheg.py --full --ttype=h3 --tag=h3_$1_$4

## Finally
echo `date`
ls -l
