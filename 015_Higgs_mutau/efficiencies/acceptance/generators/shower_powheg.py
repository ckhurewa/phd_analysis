#!/usr/bin/env python
"""

Shower the hard event from POWHEG-BOX

"""

from PyPythia8 import Pythia
import pythia_utils # local

#===============================================================================

def main(args):
  ## Prepare Pythia
  pythia = Pythia()
  pythia.readString('Next:numberShowEvent = 0')
  pythia.readString('Beams:frameType = 4')
  pythia.readString('Beams:LHEF = pwgevents.lhe')

  ## Adjust decay
  pythia.readString('25:isResonance = on')
  pythia.readString('25:onmode = off')
  pythia.readString('25:addChannel = 1 1 100 13 -15') # onMode bRatio meMode product1 product2
  pythia.readString('25:addChannel = 1 1 100 -13 15')
  pythia_utils.allow_tau_channel(pythia, args.ttype)

  ## Throw the prepared engine to execution
  # set large number, unused, as it relies on LHEF instead
  engine = pythia_utils.execute_tuple if args.full else pythia_utils.execute_online
  output = 'tuple_%s.root'%args.tag
  status = pythia.init()
  if not status:
    raise ValueError('Failed init, abort.')
  engine(pythia, nevt=int(1e12), output=output)

#===============================================================================

if __name__ == '__main__':
  args = pythia_utils.get_parser().parse_args()
  print args
  main(args)

  # ## DEV: Read immediately
  # from PyrootCK import *
  # fin = ROOT.TFile('tuple_h1_125.root')
  # h   = fin.Get('h1_125')
  # print h.dataframe().applymap(lambda u: int(u.n))
