#!/usr/bin/env python

from PyPythia8 import Pythia
import pythia_utils # local

#===============================================================================

def main(args):
  """
  Run the gg -> Higgs production with Pythia8
  """

  ## Prepare main engine
  pythia = Pythia()
  pythia.readString('Beams:eCM = %i'%args.ecm)
  pythia.readString('Next:numberShowEvent = 0')
  pythia.readString('Random:setSeed = on')
  pythia.readString('Random:seed = %d'%args.seed)

  ## Controling decay
  pythia.readString('HiggsSM:%s = on'%args.process)
  pythia.readString('25:isResonance = on')
  pythia.readString('25:onmode = off')
  pythia.readString('25:addChannel = 1 1 100 13 -15') # onMode bRatio meMode product1 product2
  pythia.readString('25:addChannel = 1 1 100 -13 15')

  ## Control tau decay
  allow_tau_channel(pythia, args.ttype)

  ## Controling mass.
  pythia.readString('25:m0 = %f'%args.mass)
  if args.mass < 50: # See Higgs:clipWings
    pythia.readString('25:mMin = 0.')
    pythia.readString('25:mMax = 50.')

  # ## Controling mass. Breit-Wigner shape distorted to be flat enough
  # pythia.readString('25:m0      = 300.')
  # pythia.readString('25:mMin    =  30.')
  # pythia.readString('25:mMax    = 300.')
  # pythia.readString('25:mWidth  = 200 ')
  # # pythia.readString('25:doForceWidth = True')

  # ## Veto
  # veto = Veto_fiducial()  # get ownership
  # pythia.setUserHooksPtr(veto)

  ## Throw the prepared engine to execution
  output = 'tuple_%s_%i.root'%(args.ttype, args.mass)
  output = output.replace('_None', '')
  engine = pythia_utils.execute_tuple if args.full else pythia_utils.execute_online
  engine(pythia, nevt=args.nevt, output=output)

  ## Finally
  # veto.tree.write()

#===============================================================================

if __name__ == '__main__':
  args = pythia_utils.get_parser().parse_args()
  print 'ARGS:', args
  main(args)
