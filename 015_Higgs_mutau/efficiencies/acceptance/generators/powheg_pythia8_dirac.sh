#!/bin/bash
# Shell script to run POWHEG+PYPYTHIA8 job on Dirac
#
# USAGE: ./powheg_pythia8_dirac.sh mass nevt seed pdfid ebeam
# >>> ./powheg_pythia8_dirac.sh 125 1000 0 25300 4000d0

## Metadata
date
pwd
ls -l 
env

## Prep env
ROOT=/cvmfs/sft.cern.ch/lcg/releases
POWHEGSYS=$ROOT/MCGenerators/powheg-box/r2092-ba9e1/x86_64-slc6-gcc49-opt
LHAPDFSYS=$ROOT/MCGenerators/lhapdf/6.1.6-b6602/x86_64-slc6-gcc49-opt
FASTJETSYS=$ROOT/fastjet/3.2.0-bb0d1/x86_64-slc6-gcc49-opt
export PATH=$POWHEGSYS/bin:${PATH}
export PATH=$LHAPDFSYS/bin:${PATH}
export LD_LIBRARY_PATH=$LHAPDFSYS/lib:$FASTJETSYS/lib:${LD_LIBRARY_PATH}
export LHAPDF_DATA_PATH=${ROOT}/../external/lhapdfsets/current:${LHAPDFSYS}/share/LHAPDF
export PYTHONPATH=${LHAPDFSYS}/lib/python2.7/site-packages:${PYTHONPATH}

# ## This part will be prep by ganga
# rm -rf temp; mkdir temp
# cp powheg/powheg0.input temp
# cp pythia_utils.py      temp
# cp shower_powheg.py     temp
# cd temp

## Add the unique gen seed
cp powheg0.input powheg.input
echo "hmass $1"   >> powheg.input
echo "numevts $2" >> powheg.input
echo "iseed $3"   >> powheg.input
echo "lhans1 $4"  >> powheg.input
echo "lhans2 $4"  >> powheg.input
echo "ebeam1 $5"  >> powheg.input
echo "ebeam2 $5"  >> powheg.input

## Run
gg_H powheg.input
echo `date`
ls -l

## Pythia
# TODO

# ## Finalize
# echo `date`
# ls -l
