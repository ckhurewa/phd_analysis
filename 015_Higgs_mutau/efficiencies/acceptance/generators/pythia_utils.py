#!/usr/bin/env python

from random import random
from collections import OrderedDict
import argparse
import ROOT

from rootpy.io import root_open
from rootpy.tree import Tree
from PyPythia8 import UserHooks

#===============================================================================
# PARSER
#===============================================================================

def get_parser():
  parser = argparse.ArgumentParser()
  parser.add_argument('--seed'   , type=int  , default=1  , help='Manual from [1,9E8]')
  parser.add_argument('--nevt'   , type=int  , default=100 , help='Number of events.')
  parser.add_argument('--ecm'    , type=int  , default=8000, help='Energy at center-of-mass')
  parser.add_argument('--ttype'  , type=str  , default='h1', choices=['mu', 'e', 'h1', 'h3'])
  parser.add_argument('--mass'   , type=float, default=125., help='Higgs mass [GeV]')
  parser.add_argument('--process', type=str  , default='gg2H', help='HiggsSM process.')
  parser.add_argument('--full'   , action='store_true', help='Run fast mode (count only) or full mode (get distribution).')
  # parser.add_argument('--pdf'    , type=int)
  #
  parser.add_argument('--tag' , type=str, help="Unique tag for output file & histogram name.")
  return parser  

#===============================================================================
# UTILS
#===============================================================================

def tauitype(tau):
  """
  return one of type (e==0, h1==1, h3==2, mu==3, other==4) of given Particle.
  """
  nQ = 0
  for c in tau.children():
    idabs = c.idAbs
    if idabs == 11:
      return 0
    if idabs == 13:
      return 3
    nQ += int(c.isCharged)
  if nQ==1:
    return 1 # tauh1
  if nQ==3:
    return 2 # tauh3
  return 4 # Other

def tautype( tau ):
  """
  Return the text string of the tau type
  """
  return ('e','h1','h3','mu','other')[tauitype(tau)]

#-------------------------------------------------------------------------------

def allow_tau_channel(pythia, tt):
  """
  Given the Pythia engine, turn off all tau decay, and open only the specified:
  e, mu, h1, h3
  """
  if tt == 'mu':
    pythia.readString('15:onmode  = off')
    pythia.readString('15:onIfAny = 13') # taumu
  elif tt == 'e':
    pythia.readString('15:onmode  = off')
    pythia.readString('15:onIfAny = 11') # taumu
  elif tt == 'h3':
    pythia.readString('15:onmode    = off')
    pythia.readString('15:onIfAll = -211 -211  211') # pi pi pi
    pythia.readString('15:onIfAll = -211  211  321') # pi pi K
    pythia.readString('15:onIfAll = -211 -321  321') # pi K  K
  elif tt == 'h1':
    pythia.readString('15:offIfAll = 11')
    pythia.readString('15:offIfAll = 13')
    pythia.readString('15:offIfAll = -211 -211  211') # pi pi pi
    pythia.readString('15:offIfAll = -211  211  321') # pi pi K
    pythia.readString('15:offIfAll = -211 -321  321') # pi K  K
  else:
    raise NotImplementedError('Unknown ttype: %s'%tt)
  pythia.readString('15:offIfAll = 16 -211 -211 -211 211 211') # turnoff 5-prongs

  return pythia

#-------------------------------------------------------------------------------

def new_TProfile2D_acc(name):
  """
  Return Pre-formatted TH2D for acceptance counting.
  Use TProfile2D to collect acc.
  - xaxis: mass (max at 300 GeV)
  - yaxis: channels
  """
  hist = ROOT.TProfile2D(name, name, 300/5, 0, 300, 5, 0, 5)
  hist.GetYaxis().SetBinLabel(1, 'e')
  hist.GetYaxis().SetBinLabel(2, 'h1')
  hist.GetYaxis().SetBinLabel(3, 'h3')
  hist.GetYaxis().SetBinLabel(4, 'mu')
  hist.GetYaxis().SetBinLabel(5, 'other')
  return hist

def new_TH2D_states(name):
  """
  Return Pre-formatted TH2D for acceptance counting.
  """
  hist = ROOT.TH2D(name, name, 3, 0, 3, 4, 0, 4)
  hist.GetXaxis().SetBinLabel(1, 'outfid')
  hist.GetXaxis().SetBinLabel(2, 'infid_eta')
  hist.GetXaxis().SetBinLabel(3, 'infid_etapt')
  #
  hist.GetYaxis().SetBinLabel(1, 'outacc')
  hist.GetYaxis().SetBinLabel(2, 'ingeoacc')
  hist.GetYaxis().SetBinLabel(3, 'inacc-nominmass')
  hist.GetYaxis().SetBinLabel(4, 'inacc-withminmass')
  return hist

#-------------------------------------------------------------------------------

def in_fiducial(mu0, tau0, eta_only):
  """
  Return True if the given HMT is inside LHCb Zll fiducial volume.
  """
  ## Need before FSR
  mu  = mu0.topCopyId()
  tau = tau0.topCopyId()
  ##
  in_geo = lambda l: (l.eta >= 2.) and (l.eta <= 4.5)
  if not (in_geo(mu) and in_geo(tau)):
    return False
  if eta_only:
    return True
  return (mu.pT > 20) and (tau.pT > 20)

def in_geo_acceptance(mu0, tau):
  """
  Check whether the prompt muon and the decay product of tau lepton are in 
  LHCb geometrical acceptance or not (ETA only, no PT check)
  """
  in_eta = lambda p: (p.eta>2.0) and (p.eta<4.5)

  ## Need after FSR
  mu = mu0.botCopyId()
  if not in_eta(mu):
    return False

  ## Tau channel
  tt = tautype(tau)
  if tt == 'h3':
    prongs   = tau.children(isCharged=True)
    p1,p2,p3 = sorted(prongs, key=lambda p: -p.pT)
    if not (in_eta(p1) and in_eta(p2) and in_eta(p3)):
      return False
  else:
    child = tau.children(isCharged=True)[0]
    if not in_eta(child):
      return False

  ## All clear
  return True

def in_acceptance(mu0, tau, min_mass=False):
  """
  Return True if the given HMT is inside the analysis acceptance.
  """
  ## The geometrical acceptance check is factorized.
  if not in_geo_acceptance(mu0, tau):
    return False

  ## Need after FSR
  mu = mu0.botCopyId()

  ## On hard muon, channel-independent
  if mu.pT<20:
    return False

  ## Channel
  tt = tautype(tau)
  if tt == 'h3':
    prongs   = tau.children(isCharged=True)
    p1,p2,p3 = sorted(prongs, key=lambda p: -p.pT)
    p123     = p1.p + p2.p + p3.p  # 3-prongs 4-momentum
    mass     = (p123 + mu.p).mCalc
    if not ((p1.pT > 6) and (p3.pT>1)):
      return False
    if p123.pT < 12:
      return False
    if not ((p123.m > 0.7) and (p123.m < 1.5)):
      return False
    if min_mass and mass < 30:
      return False

  ## single-prongs
  else:
    child = tau.children(isCharged=True)[0]
    mass  = (child.p + mu.p).mCalc
    if tt == 'h1':
      if child.pT < 10:
        return False
      if min_mass and mass < 30:
        return False
    else: # e, mu
      if child.pT< 5:
        return False
      if min_mass and mass < 20:
        return False

  ## All clear
  return True


#-------------------------------------------------------------------------------
# TREE SPEC
#-------------------------------------------------------------------------------

def set_vec4( tree, prefix, p ):
  setattr( tree, prefix+'_M'  , p.m   )
  setattr( tree, prefix+'_PT' , p.pT  )
  setattr( tree, prefix+'_PZ' , p.pz  )
  setattr( tree, prefix+'_ETA', p.eta )
  setattr( tree, prefix+'_PHI', p.phi )
  return tree

def set_higgs_mu_tau( tree, H, mu, tau ):
  set_vec4( tree, 'higgs', H   )
  set_vec4( tree, 'mu'   , mu  )
  set_vec4( tree, 'tau'  , tau )
  setattr(tree, 'higgs_M0', H.m0 ) # nominal mass
  return tree

def update_spec( spec, prefix ):
  for func in ['M', 'PT', 'PZ', 'ETA', 'PHI']:
    spec[prefix+'_'+func] = 'F'

def create_trees(full=True):
  """
  Return the dict of trees, to be used in exe_tuple. 
  The branches are customized for different channels.

  If not full, create only branch before tau decay.
  """
  trees = {}

  ## Basic tree spec
  SPEC = {}
  update_spec( SPEC, 'higgs' )
  update_spec( SPEC, 'mu'    )
  update_spec( SPEC, 'tau'   )
  SPEC['higgs_M0'] = 'F'

  ## Return single process-level non-channel tree.
  if not full:
    tree = Tree('process')
    tree.create_branches(OrderedDict(sorted(SPEC.items())))
    return tree

  ## taumu
  spec = dict(SPEC) # copy
  update_spec( spec, 'tauvis'   )
  update_spec( spec, 'higgsvis' )
  tree = Tree('taumu')
  tree.create_branches(OrderedDict(sorted(spec.items())))
  trees['taumu'] = tree
  ## taue
  spec = dict(SPEC)
  update_spec( spec, 'tauvis'   )
  update_spec( spec, 'higgsvis' )
  tree = Tree('taue')
  tree.create_branches(OrderedDict(sorted(spec.items())))
  trees['taue'] = tree
  ## tauh1
  spec = dict(SPEC)
  update_spec( spec, 'tauvis'   )
  update_spec( spec, 'higgsvis' )
  tree = Tree('tauh1')
  tree.create_branches(OrderedDict(sorted(spec.items())))
  trees['tauh1'] = tree
  ## tauh3
  spec = dict(SPEC)
  update_spec( spec, 'pr1'      )
  update_spec( spec, 'pr2'      )
  update_spec( spec, 'pr3'      )
  update_spec( spec, 'tauvis'   )
  update_spec( spec, 'higgsvis' )
  tree = Tree('tauh3')
  tree.create_branches(OrderedDict(sorted(spec.items())))
  trees['tauh3'] = tree
  ## Finally
  return trees


#===============================================================================
# VETO HOOKS
#===============================================================================

class Veto_acceptance(UserHooks):
  """
  Early eliminate HMT that for sure will not be inside the acceptance.

  Those failed here is regarded as not-from-LHCb-fiducial (2.0<eta<4.5), 
  and impossible-to-be-inside-HMT-acceptance (eta, pt).
  """

  def __init__(self):
    super(Veto_acceptance, self).__init__()
    self.count_veto = 0
    # self.tree = create_trees(full=False)

  def doVetoProcessLevel(self, process):
    """
    Early veto, for speed, less precise.
    Let's do loosen version of actual cut.
    """
    H   = process.find(id=25)
    mu  = H.children(idAbs=13)[0]
    tau = H.children(idAbs=15)[0]

  #   # ## Collect stats at process level, very early.
  #   # ## Prescaled to be disk-friendly.
  #   # if random() < 0.001:
  #   #   set_higgs_mu_tau( self.tree, H, mu, tau ).fill()

    ## Actual veto. Eta only, no PT, 
    # so that I don't disturb the infid=LHCb denominator.
    veto = False
    if mu.eta < 1.6: # lower that this shouldn't be able to climb up to 2.0
      veto = True
    if tau.eta < 1.6: # same, shouldb be impossible to climb up, itself and children.
      veto = True

    # if not veto:
    #   # process.list()
    #   print '%.2f'%mu.eta, '%.2f'%tau.eta
    # self.temp_tau_eta = tau.eta

    ## finally
    self.count_veto += int(veto)
    return veto

  def doVetoPartonLevel(self, event):
    """
    More precise that process-level, but slower.
    Note: Don't be so tight as to interfering the outfid-inacc population.
    """
    H   = event.find(id=25)
    mu  = H.children(idAbs=13)[0]
    tau = H.children(idAbs=15)[0]

    ## Actual veto
    veto = False
    if mu.eta < 2.0:
      veto = True
    if tau.eta < 1.6: # still loose, it's up to the children
      veto = True

    # if not veto:
      # event.list()
    # print '%.2f'%mu.eta, '%.2f'%tau.eta, 'veto=',veto

    ## finally
    self.count_veto += int(veto)
    return veto
    # return (mu.eta<2.0) and (not veto) # debug: see failing tau
    # return False

#===============================================================================
# ENGINES
#===============================================================================

def execute_tuple(pythia, nevt=100, output='tuple.root'):
  """
  Actual processing outside the config, so that this part to modularized

  This execution will fetch the HMT particles and write to ntuple,
  including the decay of taus. There will be 4 trees for each tau type.

  In case of no nevt given, keep running (for engine from LHEF inputs).
  """

  ## Prepare IO
  fout  = root_open(output, 'recreate')
  trees = create_trees()

  ## Start the loop
  for i in xrange(nevt):
    if not pythia.next():
      if pythia.info.atEndOfFile:
        break
      continue

    ## Get particle
    H   = pythia.event.find(id=25)
    mu  = H.children(idAbs=13)[0]
    tau = H.children(idAbs=15)[0]

    ## Get tau type & respective tree
    ttype = tautype(tau)
    ## Also kill tau type other
    if ttype == 'other':
      continue
    tname = 'tau'+ttype
    tree  = trees[tname]

    ## Fill the tree diagram in tree, depen
    set_higgs_mu_tau( tree, H, mu, tau )

    ## Fill the children, depends
    if ttype == 'h3':
      prongs    = tau.children(isCharged=True)
      p1,p2,p3  = sorted( prongs, key=lambda p: p.pT, reverse=True )
      tauvis    = p1.p + p2.p + p3.p
      higgsvis  = mu.p + tauvis
      set_vec4( tree, 'pr1'     , p1 )
      set_vec4( tree, 'pr2'     , p2 )
      set_vec4( tree, 'pr3'     , p3 )
      set_vec4( tree, 'tauvis'  , tauvis  )
      set_vec4( tree, 'higgsvis', higgsvis)
    else:
      child = tau.children(isCharged=True)[0]
      set_vec4( tree, 'tauvis'  , child )
      set_vec4( tree, 'higgsvis', child.p + mu.p )

    ## Tree is ready, fill once.
    tree.fill()

  ## Finalize
  pythia.stat()
  for tree in trees.values():
    tree.write()
  fout.close()

#===============================================================================

def execute_online(pythia, nevt=100, output='tuple.root'):
  """
  calculate online and persist the result into histogram as a function of mass
  """

  ## Prepare IO. Make hist name from output file name.
  fout = root_open(output, 'recreate')
  name = output.replace('tuple_','').replace('.root', '')
  hist = new_TH2D_states(name)

  # ## Use fiducial veto, still keep the counter
  # # note: not working, need to put before init
  # veto = Veto_acceptance()  # get ownership
  # pythia.setUserHooksPtr(veto)

  ## Start the loop
  pythia.init()
  for i in xrange(nevt):
    if not pythia.next():
      if pythia.info.atEndOfFile:
        break
      continue

    ## Get particle
    H   = pythia.event.find(id=25)
    mu  = H.children(idAbs=13)[0]
    tau = H.children(idAbs=15)[0]

    # # ## debug
    # # pythia.event.list()
    # print veto.temp_tau_eta, min([x.eta for x in tau.children(isCharged=True)]), ((veto.temp_tau_eta < 2.0) and min([x.eta for x in tau.children(isCharged=True)])>2.0)
    # if (veto.temp_tau_eta < 2.0) and min([x.eta for x in tau.children(isCharged=True)])>2.0:
    #   print '%.2f'%mu.eta, '%.2f'%tau.eta, ['%.2f'%x.eta for x in tau.children(isCharged=True)]
    #   raw_input()

    ## Get tau type, should be single here
    ttype = tautype(tau)

    ## Simply fill histo the acceptance decision
    infid0 = in_fiducial(mu, tau, eta_only=True)
    infid1 = in_fiducial(mu, tau, eta_only=False)
    inacc0 = in_acceptance(mu, tau, min_mass=False)
    inacc1 = in_acceptance(mu, tau, min_mass=True)
    ingeo  = in_geo_acceptance(mu, tau)
    fid    = 2 if infid1 else 1 if infid0 else 0
    acc    = 3 if inacc1 else 2 if inacc0 else 1 if ingeo else 0
    hist.Fill(fid, acc)

  ## Add the last count & write
  hist.Fill(0, 0, veto.count_veto)
  hist.Write()

  ## Finalize
  pythia.stat()
  fout.close()
