#!/usr/bin/env python 

"""

Check some property of generator-level tau-lepton, for thesis theory section.

"""

import numpy as np
from pyroot_zen import ROOT
from PyPythia8 import Pythia, REtaPhi
from pythia_utils import get_parser, tauitype, allow_tau_channel
# from rootpy.io import root_open

#===============================================================================

def main(args):

  ## Prepare main engine
  pythia = Pythia()
  pythia.readString('Beams:eCM = %i'%args.ecm)
  pythia.readString('Next:numberShowProcess = 0')
  pythia.readString('Next:numberShowEvent = 0')
  pythia.readString('Random:setSeed = on')
  pythia.readString('Random:seed = %d'%args.seed)

  ## Controling decay: Z -> tau tau
  pythia.readString('WeakSingleBoson:ffbar2gmZ = on')
  pythia.readString('23:onmode = off')
  pythia.readString('23:onIfAny = 15 -15') # tau tau

  ## Control tau decay
  allow_tau_channel(pythia, args.ttype)

  ## Output file & histos
  output   = 'tuple_%s.root'%args.ttype
  fout     = ROOT.TFile(output, 'recreate')
  title    = 'ptfrac_'+args.ttype
  xarr     = np.geomspace(1, 100, 100+1) # for PT
  h_ptfrac = ROOT.TProfile(title, title, xarr)
  title    = 'dr_'+args.ttype
  h_dr     = ROOT.TProfile(title, title, xarr)

  ## Start the loop
  pythia.init()
  for i in xrange(args.nevt):
    if not pythia.next():
      if pythia.info.atEndOfFile:
        break
      continue

    # pythia.process.list()
    Z0 = pythia.event.find(id=23)
    for tau in Z0.children(idAbs=15):
      ## Get the sum of 4-momentum of charged children
      pchild = sum(c.p for c in tau.children(isCharged=True))
      ## Calculate fractional PT
      ptfrac = pchild.pT / tau.pT
      ## Calculate the distance
      dr = REtaPhi(pchild, tau.p)
      ## Fill the histogra
      h_ptfrac.Fill(tau.pT, ptfrac)
      h_dr.Fill(tau.pT, dr)

  ## Finalize
  pythia.stat()
  h_ptfrac.Write()
  h_dr.Write()
  fout.Close()

#===============================================================================

if __name__ == '__main__':
  args = get_parser().parse_args()
  print 'ARGS:', args
  main(args)
