#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Study the variation of eacc from different scales

"""

from PyrootCK import *
from PyrootCK.mathutils import EffU
sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
import eff_utils # tethered
from HMT_utils import TTYPES, pd, pickle_dataframe_nondynamic, gpad_save, memorized, X_to_latex
from uncertainties import ufloat, nominal_value

JID = 5504

#===============================================================================

@pickle_dataframe_nondynamic
def read():
  acc = {}
  fin = import_file(JID)
  for key in fin.keys:
    h  = key.ReadObj()
    df = h.dataframe()
    nume      = df.loc['inacc-withminmass'].sum().n 
    deno_4pi  = df.sum().sum().n
    deno_lhcb = df[['infid_eta', 'infid_etapt']].sum().sum().n
    tt, mass, pdfid, rscale, fscale = key.name.split('_')
    mass   = int(mass)
    pdfid  = int(pdfid)
    rscale = float(rscale)
    fscale = float(fscale)
    acc[(tt, mass, '4pi' , pdfid, rscale, fscale)] = EffU(deno_4pi, nume)
    acc[(tt, mass, 'lhcb', pdfid, rscale, fscale)] = EffU(deno_lhcb, nume)
  ## Package & return
  se = pd.Series(acc)
  se.index.names = 'tt', 'mass', 'geo', 'pdfid', 'rscale', 'fscale'
  return se

# 1/2 < muF/muR < 2
WHITELIST = [
  (0.5, 0.5),
  (0.5, 0.8),
  (0.5, 1.0),
  (0.8, 0.5),
  (0.8, 0.8),
  (0.8, 1.0),
  (0.8, 1.25),
  (1.0, 0.5),
  (1.0, 0.8),
  (1.0, 1.0),
  (1.0, 1.25),
  (1.0, 2.0),
  (1.25, 0.8),
  (1.25, 1.0),
  (1.25, 1.25),
  (1.25, 2.0),
  (2.0, 1.0),
  (2.0, 1.25),
  (2.0, 2.0),
]
def wrap_scale(se0):
  """
  Return the ufloat representing variation from scale.
  Note: Apply condition 1/2 < μF/μR < 2
  """
  se = se0.apply(nominal_value).loc[se0.name].loc[WHITELIST]
  return ufloat(se.mean(), se.std())


def relative_uncertainties():
  """
  Return rerr, in absolute.
  """
  df = read().xs(25100, level='pdfid').groupby(level=['tt', 'mass', 'geo']).apply(wrap_scale).unstack('tt')
  df = df.swaplevel().sort_index()
  return df.applymap(lambda u: u.rerr)


def main():
  """
  Print relative uncertainties in percent.
  """
  df = read().xs(25100, level='pdfid').groupby(level=['tt', 'mass', 'geo']).apply(wrap_scale).unstack('tt')
  df = df.swaplevel().sort_index()

  ## 1. Raw variation
  print df.applymap(lambda u: u.rerr).fmt2p

#===============================================================================

if __name__ == '__main__':
  # print read()
  print relative_uncertainties()
  # main()
