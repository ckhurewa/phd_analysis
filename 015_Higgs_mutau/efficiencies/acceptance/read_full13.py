#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
import eff_utils # tethered
from HMT_utils import (
  TTYPES, pd, pickle_dataframe_nondynamic, memorized, gpad_save
)

#===============================================================================

CUT_INACC = '({0}_ETA > 2.) & ({0}_ETA < 4.5)'.format

CUTS_GEOACC = {
  'e' : [CUT_INACC('mu'), CUT_INACC('tauvis')],
  'mu': [CUT_INACC('mu'), CUT_INACC('tauvis')],
  'h1': [CUT_INACC('mu'), CUT_INACC('tauvis')],
  'h3': [CUT_INACC('mu'), CUT_INACC('pr1'), CUT_INACC('pr2'), CUT_INACC('pr3')],
}

## Current kinematic cuts
CUTS_KINE1 = {
  'e' : ('mu_PT > 20', 'tauvis_PT > 5'),
  'h1': ('mu_PT > 20', 'tauvis_PT > 10'),
  'mu': { # dimuon
    ('mu_PT > 20', 'tauvis_PT > 5'),
    ('mu_PT > 5' , 'tauvis_PT > 20'),
  },
  'h3': (
    'mu_PT     > 20',
    'pr1_PT    > 6',
    'pr3_PT    > 1',
    'tauvis_PT > 12',
  ),
}

## Best use of existing Run-II trigger: Mostly VHighPT (no prescale)
# +mininal PT on second member
CUTS_KINE2 = {
  'e' : ('mu_PT > 12.5', 'tauvis_PT > 1'),
  'h1': ('mu_PT > 12.5', 'tauvis_PT > 1'),
  'h3': ('mu_PT > 12.5', 'pr1_PT > 1', 'pr2_PT > 1', 'pr3_PT > 1'),
  'mu': {
    ('mu_PT > 12.5', 'tauvis_PT > 1'   ),
    ('mu_PT > 1'   , 'tauvis_PT > 12.5'), # any muon
    # 'higgsvis_M > 10', # DiMuonDY3  
  }
}

## New dedicate trigger
CUTS_KINE3 = {
  
}

#===============================================================================

JID = 5506

def load_trees():
  """
  Load all trees into memory.
  """
  acc = {}
  for mass in [25, 45, 85, 125, 200]:
    src = '$LPHE_OFFLINE/gangadir_large/%i/tuple_%i.root'%(JID, mass)
    fin = ROOT.TFile(os.path.expandvars(src))
    fin.ownership = False
    for key in fin.keys:
      ## Collect them
      tt = key.name.replace('tau', '')
      tree = acc[(mass, tt)] = key.ReadObj()
      tree.title = '%s_%i'%(tt, mass)
      tree.estimate = tree.entries
      ## Set alias
      tree.SetAlias('ingeo', utils.join(CUTS_GEOACC[tt]))
      tree.SetAlias('kine1', utils.join(CUTS_KINE1[tt]))
      tree.SetAlias('kine2', utils.join(CUTS_KINE2[tt]))
  return pd.Series(acc)

def eff(cut):
  """
  Helper to calculate eff
  """
  def func(tree):
    tt   = tree.title.split('_')
    deno = tree.GetEntries('ingeo')
    nume = tree.GetEntries(utils.join('ingeo', cut))
    return 1.*nume/deno
  return func

@pickle_dataframe_nondynamic
def efficiencies():
  acc = {}
  trees = load_trees()
  for cut in ('kine1', 'kine2'):
    acc[cut] = trees.apply(eff(cut))
  se = pd.concat(acc)
  se.index.names = ['cut', 'mass', 'tt']
  return se

#===============================================================================

@gpad_save
def draw_eff(cut):
  eff = efficiencies().xs(cut, level='cut').unstack('tt') * 100.
  gr, leg = eff_utils.plot_hmt_eff(eff, 'Kinematic cut efficiency [%]')
  gr.maximum = 105
  leg.header = 'LHCb 13TeV'
  leg.x1NDC  = 0.7
  leg.x2NDC  = 0.9
  leg.y1NDC  = 0.2
  leg.y2NDC  = 0.5

#===============================================================================

if __name__ == '__main__':
  print efficiencies()
  # print efficiencies().loc['kine2'].unstack()
  # draw_eff('kine1')
  # draw_eff('kine2')
