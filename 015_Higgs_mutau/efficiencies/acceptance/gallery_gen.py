#!/usr/bin/env python
"""

Comparison of generator-level Higgs

"""

## default tools
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import TTYPES, ROOTLABELS, TT_TO_ROOTLABEL

#===============================================================================

def get_tree(tt):
  """
  This is a merged tree of many mH. It's separable by slicing selection cuts.
  """
  tree = import_tree('tau'+tt, 5324).SetTitle('POWHEG-BOX (gg_H)')
  for mass in xrange(25, 200, 10):
    tree.SetAlias('mH%i'%mass, '(higgs_M > %i-4) & (higgs_M < %i+4)'%(mass, mass))
    tree.SetAlias('hardmu_ETA', 'mu_ETA')
    tree.SetAlias('hardmu_PT' , 'mu_PT' )
    tree.SetAlias('InGeo1'    , '(2.0 < mu_ETA)  & (mu_ETA  < 4.5)')
    tree.SetAlias('InGeo2'    , '(2.0 < tau_ETA) & (tau_ETA < 4.5)')
    tree.SetAlias('InGeo'     , 'InGeo1 & InGeo2')
  return tree

#===============================================================================

def main():
  """
  Note: 1e3 for auto-log
  """
  trees  = {tt:get_tree(tt) for tt in TTYPES}
  masses = 45, 85, 125, 195

  QHist.reset()
  QHist.auto_name = True
  QHist.batch     = True
  QHist.trees     = trees['mu']
  QHist.cuts      = ['mH%i'%m for m in masses]
  QHist.xbin      = 50
  QHist.legends   = ['m_{H} %i GeV'%m for m in masses]
  QHist.st_marker = False

  ## Channel-independent
  h = QHist().draw(params='higgs_ETA' , xmin=-8, xmax=8  , xlabel='#eta(H)')
  h = QHist().draw(params='higgs_PT'  , xmin=1 , xmax=1e3, xlabel='p_{T}(H)')
  h = QHist().draw(params='hardmu_ETA', xmin=-6, xmax=6  , xlabel='#eta(#mu)')
  h = QHist().draw(params='hardmu_PT' , xmin=1 , xmax=1e3, xlabel='p_{T}(#mu)')

  ## Channel-dependent
  for tt, tree in trees.iteritems():
    s0 = TT_TO_ROOTLABEL[tt]
    s1 = ROOTLABELS[tt][1]
    QHist.trees  = tree
    QHist.prefix = tt
    h = QHist().draw(params='higgsvis_M'  , xmin=0 , xmax=250, xlabel='m(%s)'    %s0)
    h = QHist().draw(params='higgsvis_ETA', xmin=-8, xmax=8  , xlabel='#eta(%s)' %s0)
    h = QHist().draw(params='higgsvis_PT' , xmin=1 , xmax=1e3, xlabel='p_{T}(%s)'%s0)
    h = QHist().draw(params='tauvis_ETA'  , xmin=-8, xmax=8  , xlabel='#eta(%s)' %s1)
    h = QHist().draw(params='tauvis_PT'   , xmin=.1, xmax=1e3, xlabel='p_{T}(%s)'%s1)

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  main()
