#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Plot relative uncertainties of acceptance factor (PDF+Scale)

"""

from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15/efficiencies'))
import eff_utils # tethered
from HMT_utils import TTYPES, pd, gpad_save, drawers, X_TO_ROOTLABELS
from uncertainties import ufloat, nominal_value

## handles
import read_scan
import read_scale

@gpad_save
def draw_eacc_rerr(geo):
  ## Get data
  df1 = read_scan.relative_uncertainties().xs(geo, level='geo')*100.
  df2 = read_scale.relative_uncertainties().xs(geo, level='geo')*100.
  df1 = df1.rename(X_TO_ROOTLABELS, columns=X_TO_ROOTLABELS)
  df2 = df2.rename(X_TO_ROOTLABELS, columns=X_TO_ROOTLABELS)
  gr1, leg1 = drawers.graphs_eff(df1)
  gr2, leg2 = drawers.graphs_eff(df2)

  ## Start drawing
  c = ROOT.TCanvas()
  c.ownership = False
  gr = ROOT.TMultiGraph()
  gr.ownership = False
  for g in gr1.graphs:
    gr.Add(g, 'lp')
  for g in gr2.graphs:
    gr.Add(g, 'lp')
  gr.Draw('A')
  gr.minimum     = 0.
  gr.maximum     = 8.
  gr.xaxis.title = 'm_{H} [GeV/c^{2}]'
  gr.yaxis.title = 'Relative uncertainty [%]'

  ## Legend
  leg1.header = 'LHCb 8 TeV'
  leg1.x1NDC  = 0.16
  leg1.x2NDC  = 0.50
  leg1.y1NDC  = 0.63
  leg1.y2NDC  = 0.92
  leg1.textSize = 0.06
  leg1.Draw()

  ## Labels syst
  tt1 = ROOT.TText(170, 7 if geo=='4pi' else 2, 'PDF')
  tt1.ownership = False
  tt1.Draw()
  tt2 = ROOT.TText(170, 0.5, 'Scales')
  tt2.ownership = False
  tt2.Draw()


if __name__ == '__main__':
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  draw_eacc_rerr('4pi')
  draw_eacc_rerr('lhcb')
