#!/usr/bin/env python

import read_fast
import read_full
import read_scan
import read_scale

import os, sys
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import memorized, pd, ufloat, X_to_latex

#===============================================================================

@memorized
def efficiencies():
  """
  Decide on the best result with all relevant uncertainties.
  """
  ## Base value
  # eff = read_full.efficiencies() # full, flexible, lower stat
  # eff = read_fast.efficiencies() # fast, larger stat, count only
  eff = read_scan.efficiencies() # scanning of PDF members for additional syst.
  
  ## kill useless low-mass
  idx = eff.index.get_level_values('mass') > 40
  eff = eff[idx] 

  ## Organize the uncertainties
  df = pd.DataFrame({
    'eff0' : eff.stack(),
    'scale': read_scale.relative_uncertainties().stack(),
  })
  def wrap(se):
    return ufloat(se.eff0.n, se.eff0.s, 'pdf') * ufloat(1., se.scale, 'scale')
  return df.apply(wrap, axis=1).unstack('tt')

#===============================================================================
# LATEX
#===============================================================================

def latex_eff_4pi():
  return X_to_latex(efficiencies().loc['4pi']*100.).to_bicol_latex(decimals=[2,2,2,2])

def latex_eff_lhcb():
  return X_to_latex(efficiencies().loc['lhcb']*100.).to_bicol_latex(decimals=[2,2,2,2])

#===============================================================================

if __name__ == '__main__':
  print efficiencies().loc[('4pi', 125), 'h1']
  print efficiencies().loc[('4pi', 125), 'h1'].tags
  print efficiencies().loc[('4pi', 125), 'h1'].get_rerr('pdf')
  print efficiencies().loc[('4pi', 125), 'h1'].get_rerr('scale')

