#!/usr/bin/env python
"""

Get HMT offline selection eff

"""

from PyrootCK import *
from eff_utils import plot_hmt_eff # tethered
from HMT_utils import (
  pd, REGIMES, TTYPES, X_TO_ROOTLABELS, X_to_latex,
  gpad_save, df_from_path, EffU, 
)

#===============================================================================

def efficiencies():
  """
  Calculate for all regimes
  """
  ## Load data
  path = '$DIR15/selection/bkg_rectangular/rect_context'
  tag  = 'make_context_all'
  df   = df_from_path(tag, path).xs('evt', level='amount').xs('OS', level='sign')
  df   = df.filter(regex='higgs.*', axis=0).unstack('cuts')

  ## Apply division with Clopper-Pearson uncertainty
  div  = lambda se: EffU(se.presel_truechan, se.sel_anychan)  # 
  df   = df.apply(div, axis=1)
  
  ## rename higgs process to mass
  func = lambda s: int(s.split('_')[1]) if 'higgs' in s else s
  df   = df.rename(func).sort_index() 
  
  ## finally
  df   = df.unstack('tt').loc[list(REGIMES)]
  df.index.names = 'regime', 'mass'
  return df

#===============================================================================

@gpad_save
def draw_esel(tag):
  """
  tag = regime, tt
  """
  ## Fetch the right data
  df0 = efficiencies()
  if tag in df0.columns: # channel
    df = df0[tag].unstack(0)
    header = 'Channel: %s'%X_TO_ROOTLABELS[tag]
  elif tag in df0.index.levels[0]: # regime
    df = df0.loc[tag]
    header = 'Regime: %s'%X_TO_ROOTLABELS[tag]
  else:
    raise ValueError('Unknown channel/regime.')
  ## Actual draw
  ytitle  = 'Selection efficiency'
  gr, leg = plot_hmt_eff(df, ytitle, header=header)
  gr.maximum = 0.6

#===============================================================================

def latex_eff():
  df = efficiencies()
  ## Trimmed in mass to have less rows
  Lv   = df.index.get_level_values
  idx1 = (Lv('regime')=='lowmass') & (Lv('mass') > 40)
  idx2 = (Lv('regime')=='nominal') & (Lv('mass') > 60)
  idx3 = (Lv('regime')=='tight')   & (Lv('mass') > 70)
  df   = df[idx1|idx2|idx3]
  # finally
  return X_to_latex(df*100.).to_bicol_latex()

#===============================================================================

if __name__ == '__main__':
  if '--latex' in sys.argv:
    for tag in TTYPES+REGIMES:
      draw_esel(tag)
    latex_eff()
    sys.exit()

  ## DEV
  # print efficiencies().fmt2p.to_string()
  # print latex_eff()

  print efficiencies().applymap(lambda u: u.rerr*100.)
