#!/bin/bash

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

## Vars
PROJ=${HOME_EPFL}/Papers/007_HiggsLFV/figs
DATE=`date +%y%m%d`

## Apply on the modules, already interfaced the ame way
# for MODULE in selection rec_detailed acceptance/read_full punzi
for MODULE in punzi
do
  rm -f $MODULE/*.pdf               # remove existing
  ./$MODULE.py --latex              # rerun
  mkdir -p $MODULE/$DATE            # make clean dir
  mv $MODULE/*.pdf $MODULE/$DATE    # move into archive
  cp -v $MODULE/$DATE/* $PROJ/eff/  # copy to latex
done

## Discarded affected latex pages
# rm 06_signal_eff.tex 07_systematics.tex 61_app_eff.tex 62_app_stats.tex
