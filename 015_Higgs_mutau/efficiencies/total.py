#!/usr/bin/env python
"""

Draw total eff: eacc * erec * esel

"""

# Tools
from PyrootCK import *
from eff_utils import drawer_hmt_eff
from HMT_utils import pd, gpad_save

## Data handles
import acceptance
import rec_direct
import selection

#===============================================================================

def efficiencies():
  """
  Product of acceptance & reconstruction & selection
  """
  ## Retrieve
  eacc = acceptance.efficiencies()
  erec = rec_direct.calculate_erec_all()
  esel = selection.efficiencies()
  etot = eacc * erec * esel

  ## Clean NaN
  return etot.applymap(lambda x: pd.np.NaN if pd.np.isnan(x.n) else x).dropna()

#-------------------------------------------------------------------------------

def latex_syst():
  rerr = lambda u: None if not u.n else u.s*100/u.n

  ## Retrieve
  eacc = acceptance.efficiencies()
  erec = rec_direct.calculate_erec_all()
  esel = selection.efficiencies()
  etot = eacc * erec * esel

  print eacc.applymap(rerr)
  print erec.applymap(rerr)
  print esel.applymap(rerr)
  print etot.applymap(rerr)

#===============================================================================

@gpad_save
def draw_etot():
  drawer_hmt_eff(efficiencies()*100., 'Total efficiency [%]')

#===============================================================================

if __name__ == '__main__':
  # print efficiencies()
  # latex_syst()
  draw_etot()
