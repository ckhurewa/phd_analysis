#!/usr/bin/env python
"""

Get HMT reconstruction efficiency, direct version from MC.

"""

## Default tools
from PyrootCK import *
from eff_utils import drawer_hmt_eff
from HMT_utils import (
  pd, memorized, TTYPES, MASSES, COMPUTE_LABELS, deco,
  pickle_dataframe_nondynamic, gpad_save, reco_sieve, Idx,
)

## Source
# JID = 5218
# JID = 5301 # full range
JID = 5430 # fix non-unique prongs in tauh3

#===============================================================================

@memorized
def get_trees():
  """
  Return all trees, index=mass, columns=ttype
  """
  acc = pd.DataFrame(index=MASSES, columns=TTYPES)
  fin = import_file(JID)
  fin.ownership = False
  f2  = fin.Get('HMT_RecoStats')
  for key in f2.keys:
    tt, mass = key.name.split('_')
    mass = int(mass)
    tree = f2.Get(key.name)
    acc.set_value(mass, tt, tree)
  logger.info('Load trees completed')
  return acc

#===============================================================================

@pickle_dataframe_nondynamic
def calc_all_raw():
  """
  Cache the results from reco_sieve for further computation
  """
  acc = {}
  for tt, trees in get_trees().iteritems():
    for mass, tree in trees.iteritems():
      acc[(tt, mass)] = reco_sieve.apply(tree, tt)
  se = pd.concat(acc)
  se.index.names = ['tt', 'mass', 'field']
  return se

def calc_all_stages():
  """
  Return the eff at each stages.
  """
  cols = 'etrack', 'ekine', 'epid', 'egec', 'etrig', 'erec'
  return calc_all_raw().unstack('field')[list(cols)]

def efficiencies():
  """
  Common interface, return final erec
  """
  return calc_all_raw().xs('erec', level='field').unstack('tt')

@gpad_save
def draw_erec():
  """
  Draw final erec as a function of masses
  """
  gr = drawer_hmt_eff(efficiencies(), 'Reconstruction efficiency')
  gr.maximum = 1.1

#===============================================================================
# LATEX
#===============================================================================

def latex_eff_stages_generic(tt):
  """
  Provide the table of eff at different reco stages for latex.
  """
  df = calc_all_stages().xs(tt).loc[MASSES]
  df = df.rename(columns=COMPUTE_LABELS)
  df.index.name = '\\mH [\\gevcc{}]'
  return df.fmt2lp.to_markdown(stralign='right')

latex_eff_stages_mu = lambda: latex_eff_stages_generic('mu')
latex_eff_stages_e  = lambda: latex_eff_stages_generic('e')
latex_eff_stages_h1 = lambda: latex_eff_stages_generic('h1')
latex_eff_stages_h3 = lambda: latex_eff_stages_generic('h3')

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print efficiencies().fmt2p
    sys.exit()

  # print get_trees()
  # print calc_all_raw()
  print calc_all_raw().loc[Idx[:, :, 'full2']].unstack('tt')
  # print calc_all_stages().fmt1p.to_string()
  # print efficiencies().fmt1p
  # draw_erec()

  ## LATEX
  # print latex_eff_stages_generic('h1')
