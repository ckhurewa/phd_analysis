import os

## Wrappers
prefix = 'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga'
def eos(jid, amount=100):
  return ['%s/%i/%i/Brunel.dst'%(prefix,jid,i) for i in xrange(amount)]

## HACK To get the "simple" namePattern functionality back. Use with care
if '__setattr__' in LocalFile._impl.__dict__:
  del LocalFile._impl.__setattr__

#===============================================================================
# 8 TeV
#===============================================================================

## Load all HMT masses
ds = LHCbDataset.new(
  eos(5075), eos(5111, 199), eos(5114, 199), # 25 GeV
  eos(5117, 199), eos(5246, 200), eos(5250, 200), eos(5258, 199), eos(5261, 199), # 35 GeV
  eos(5121), # 45 GeV
  eos(5123), # 55 GeV
  eos(5127), # 65 GeV
  eos(5268, 99), # 75 GeV
  eos(5129), # 85 GeV
  eos(5274), # 95 GeV
  eos(5137), # 105 GeV
  eos(5282), # 115 GeV
  eos(5140), # 125 GeV
  eos(5284), # 135 GeV
  eos(5142), # 145 GeV
  eos(5294), # 155 GeV
  eos(5144), # 165 GeV
  eos(5297), # 175 GeV
  eos(5146), # 185 GeV
  eos(5299), # 195 GeV
  # eos(5286), # 200 GeV
)

#===============================================================================
# 13 TeV
#===============================================================================

# ## Data S24 (25k+20k files, )
# DATA_S24 = [
#   open('LHCb_Collision15_Beam6500GeVVeloClosedMagDown_Real Data_Reco15a_Stripping24_90000000_EW.DST.txt'),
#   open('LHCb_Collision15_Beam6500GeVVeloClosedMagUp_Real Data_Reco15a_Stripping24_90000000_EW.DST.txt'),
# ]
# ds = LHCbDataset.new(DATA_S24)

#===============================================================================

j = Job()
# j.name        = 'HMT-DCTW'
j.name        = 'HMT-RecoStats'
# j.comment     = 'batch1609. DitauCandTupleWriter.'
# j.comment     = 'batch1609. RecoStat (AllMasses/10)'
# j.comment     = 'batch1609, with WEH applied'
j.comment     = 'Fix tauh3 find_reco_best'
j.application = Bender(version='v29r7', module=File('bender.py'))
j.backend     = PBS(extraopts='--mem=4830 -t 1-0:0:0 --exclude=lphe01,lphe02,lphe03,lphe04')

# ## DCTW only
# j.inputfiles += [
#   os.path.expandvars('$DIR13/identification/DV-bender/DV_tau_cand_maker.py'),
# ]

## Finally
j.inputdata   = ds
j.splitter    = SplitByFiles( filesPerJob=10 )
j.outputfiles = [ '*.root', 'summary.xml' ]
queues.add( j.submit )
