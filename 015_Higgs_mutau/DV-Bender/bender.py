#!/usr/bin/env python

from BenderCK import *
from BenderCK.Tau import *

## Borrow Z->tautau library
import imp
path = '/home/khurewat/analysis/013_Tau_Identification/identification/DV-bender'
sys.path.insert(0, path) # for inner dependency
ZtautauBenderModule = imp.load_source('bender', os.path.join(path, 'bender.py'))

## Get BaseDitauRecoStats class
path = '/home/khurewat/analysis/013_Tau_Identification/efficiencies/DV-bender'
sys.path.insert(0, path)
from RecoStats import BaseDitauRecoStats, Particle_dummy # has base class

#==============================================================================

class FilterFiducialAccentance(BaseTauAlgoMC):
  """
  Difference from Ztautau analysis:
  - No Higgs mass cut at fid level
  - No mass cut of visible candidates.
  """

  cut_geo_normal = (MCETA>2.0)  & (MCETA<4.5)
  # cut_geo_hcal   = (MCETA>2.25) & (MCETA<3.75)
  cut_fid_lepton = (MCPT>20E3) & cut_geo_normal

  cut_taul  = (MCPT>5*GeV)  & cut_geo_normal
  cut_tauh1 = (MCPT>10*GeV) & cut_geo_normal
  cut_prong = (MCPT>1*GeV)  & cut_geo_normal

  @count
  def initial_tau_type(self, tau):
    return tau.type

  @count
  def final_tau_type(self, tau):
    return tau.type

  @count
  def valid_fiducial(self, mu, tau):
    """
    Require the ditau inside standard LHCb Z-fiducial.
    """
    if not self.cut_fid_lepton(mu):
      return False
    if not self.cut_fid_lepton(tau):
      return False
    return True

  @count
  def valid_acceptance_taumu(self, tauchild):
    if not self.cut_taul(tauchild): # or (invarmass<20*GeV):
      return False
    # if (invarmass>80*GeV) and (invarmass<100*GeV): # kill zpeak area
    #   return False
    return True

  @count
  def valid_acceptance_taue(self, tauchild):
    if not self.cut_taul(tauchild): # or (invarmass<20*GeV):
      return False
    return True

  @count
  def valid_acceptance_tauh1(self, tauchild):
    if not self.cut_tauh1(tauchild): # or (invarmass<30*GeV):
      return False
    return True

  @count
  def valid_acceptance_tauh3(self, tau, mu):
    pr1,pr2,pr3 = sorted(tau.children(CHARGED), key=MCPT, reverse=True)
    prongs      = pr1.momentum() + pr2.momentum() + pr3.momentum()
    tauh3mass   = prongs.M()
    invarmass   = (mu.momentum() + prongs).M()
    if not self.cut_prong(pr1):
      return False
    if not self.cut_prong(pr2):
      return False
    if not self.cut_prong(pr3):
      return False
    if MCPT(pr1) < 6*GeV:
      return False
    if MCPT(pr3) < 1*GeV:
      return False
    if prongs.pt() < 12*GeV:
      return False
    if (tauh3mass < 700*MeV) or (tauh3mass > 1500*MeV):
      return False
    # if invarmass < 30*GeV:
    #   return False
    return True

  @count
  def valid_acceptance(self, mu, tau ):
    ttype = tau.type

    ## cut on hard muon
    if not self.cut_fid_lepton(mu):
      return False

    ## cut on tauh3
    if ttype == 'h3':
      if not self.valid_acceptance_tauh3( tau, mu ):
        return False

    ## taue, taumu, tauh1
    else:
      tauchild = tau.children(CHARGED)[0]
      if ttype == 'e':
        if not self.valid_acceptance_taue( tauchild ):
          return False
      elif ttype == 'mu':
        if not self.valid_acceptance_taumu( tauchild ):
          return False
      elif ttype == 'h1':
        if not self.valid_acceptance_tauh1( tauchild ):
          return False
      else:
        raise ValueError

    return True


  def analyse(self):
    ## Prep
    higgs   = MCSOURCE('MC/Particles', '[X0 -> tau+ mu-]CC')()[0]
    tau,mu  = higgs.children()
    tau     = MyTau(tau)

    ## Report the count
    self.initial_tau_type(tau)

    ## Check at fiducal level (ignore dtype)
    if not self.valid_fiducial( mu, tau ):
      return SUCCESS

    ## Check acceptance cut (depends on which dtype)
    try:
      if not self.valid_acceptance( mu, tau ):
        return SUCCESS
    except ValueError, e:
      print self.event_break
      for tau in self.list_tau:
        print tau
        print tau.type
        print tau.decay()
      return SUCCESS

    ## Record final count
    self.final_tau_type(tau)

    ## Finally passing all
    self.setFilterPassed(True)
    return SUCCESS


#===============================================================================

def HMT_mass_binning(mcp):
  """
  Given the Higgs MCParticle, return the discreticized value of mass
  binning that it should be. This is an unfortunate ugly workaround.

  Basically, it retrieve the rest mass and return the nearest rounded mass.
  """
  BINS = xrange(25, 500, 10) # 25, 35, ...
  mass = MCM(mcp)/1e3
  l = [(abs(mark-mass),mark) for mark in BINS]
  return sorted(l, key=lambda x:x[0])[0][-1]

def tautype(higgs):
  """
  Return single string of tau type
  """
  mu, tau = sorted(higgs.daughters(), key=MCABSID)
  return MyTau(tau).type

def tuplename(higgs):
  """
  Return appropriate string for the tuple dir/name.
  This should be a tau channel & Higgs mass.
  """
  mass = HMT_mass_binning(higgs)
  return tautype(higgs) + '_' + str(mass)

#-------------------------------------------------------------------------------

class HMT_RecoStats(BaseDitauRecoStats):

  ## Configure before appMgr
  from Configurables import TupleToolConeIsolation
  conf = TupleToolConeIsolation('HMT_RecoStats.TupleToolConeIsolation')
  conf.MinConeSize = 0.5
  conf.MaxConeSize = 0.5
  conf.FillMaxPt   = False

  def queues_tau1(self, tau, hardmumcp, hardmureco):
    tauch      = tau.children(CHARGED)[0]
    tes        = self._containers[tau.type]
    reco       = self.find_reco_best(tes, tauch)
    prefix     = {'e':'e', 'h1':'had', 'mu':'mu'}[tau.type]
    HMTch_MCv4 = tauch.momentum() + hardmumcp.momentum()
    HMTch_Rv4  = reco.momentum()  + hardmureco.momentum()
    yield prefix , tauch     , reco
    yield 'HMTch', HMTch_MCv4, HMTch_Rv4

  def queues_tauh3(self, tauh3, hardmumcp, hardmureco):
    ## MCP & their 4vec
    pr1, pr2, pr3 = tauh3.children(CHARGED)
    tauh3ch = LHCb.MCParticle()
    tauh3ch.setMomentum(tauh3.vec4_children_charged) # Get a MCP version, with just charged children
    HMTch_MCv4 = tauh3ch.momentum() + hardmumcp.momentum()

    ## Helper fetch method for tauh3 (complicate)
    recotauh3ch, recopr1, recopr2, recopr3 = self.fetch_reco_best_tauh3(tauh3)
    
    ## Finally
    HMTch_Rv4 = recotauh3ch.momentum() + hardmureco.momentum()
    yield 'pr1'    , pr1       , recopr1
    yield 'pr2'    , pr2       , recopr2
    yield 'pr3'    , pr3       , recopr3
    yield 'tauh3ch', tauh3ch   , recotauh3ch
    yield 'HMTch'  , HMTch_MCv4, HMTch_Rv4

  def queues(self, higgs):
    """
    Yield (prefix, mcp, reco)
    """
    ## yield generator-level, object
    mu, tau = sorted(higgs.daughters(), key=MCABSID)
    yield 'higgs', higgs, None
    yield 'tau'  , tau  , None

    ## Handling hardmu
    tes   = self._containers['mu']
    reco  = self.find_reco_best(tes, mu)
    yield 'hardmu', mu, reco

    ## Handling tau
    tau = MyTau(tau) # convert to more enhanced Tau
    gen = self.queues_tauh3 if tau.type=='h3' else self.queues_tau1
    for x in gen(tau, mu, reco):
      yield x


  @pass_success
  def analyse(self):
    ## Do writing, based on the BaseDitauRecoStats
    for higgs in self.mcselect('higgs', '[ H_10 -> mu+ tau-]CC'):
      if tautype(higgs) in ('e', 'mu', 'h1', 'h3'):
        tname  = tuplename(higgs)
        queues = self.queues(higgs)
        self.write(tname, queues)
      ## Debug
      else:
        print 'Tau of "other" channels'
        print higgs


#===============================================================================

def configure(inputdata, catalogs=[], castor=True):
  print inputdata
  print catalogs

  ## Set manually
  IS_MC = True

  ## Attach input, clean due to incompat with Ganga 6.3.1 + EOS files
  inputdata = [x.replace('file:///root://', 'root://') for x in inputdata]
  setData( inputdata, catalogs, castor )

  seq = simple_configure( IS_MC,
    ## STILL BORROW WD13
    # '$DIR13/identification/DV-bender/DV_tau_cand_maker.py',
  )

  seq.Members += [
    ## DCTW sequence
    # FilterFiducialAccentance().name(), # FOR SIGNAL ONLY
    # ZtautauBenderModule.DitauCandTupleWriter().name(),

    ## RecoStat
    HMT_RecoStats().name(),
  ]

  return SUCCESS

#-------------------------------------------------------------------------------

if __name__ == '__main__':

  ## Higgs -> mu tau
  ## 125 GeV
  # uri = ['root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5140/%i/Brunel.dst'%i for i in xrange(100)]

  ## HMT, no Stripping
  uri = [
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5146/1/Brunel.dst', # 185
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5144/0/Brunel.dst', # 165
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5117/0/Brunel.dst', # 35
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/5127/0/Brunel.dst', # 65
  ]

  ## Finally
  configure(uri)
  run(1000)
