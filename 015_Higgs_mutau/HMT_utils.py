#!/usr/bin/env python

"""

Provide global settings & base imports for the rest of modules

"""

## Base tools
import os
import sys
import imp
from PyrootCK import *
from PyrootCK.mathutils import EffU, EffU_unguard
from qhist.utils import join

## Built on top of 013-Ztautau.
# Then I'll override some with HMT later
sys.path.append(os.path.expandvars('$DIR13'))
import ditau_utils
from ditau_utils import *

# ## Preloaded tool without *
# id_utils = imp.load_source('id_utils', os.path.expandvars('$DIR13/identification/plot/id_utils.py'))

## Another tool
import packages
import reco_sieve

## EXPERIMENTAL for concurrency: concurrent/synchronized
# https://github.com/alex-sherman/deco
import deco

## less channels than Ztautau
CHANNELS = 'mumu', 'emu', 'h1mu', 'h3mu'

TTYPES = 'e', 'h1', 'h3', 'mu'

## rectangular cut selection regimes
REGIMES_ALL = 'nominal', 'lowmass', 'oldnominal', 'oldlowmass', 'tight', 'oldtight'
REGIMES     = 'lowmass', 'nominal', 'tight' # effective one

## Fixed list of mass to be used canonically
MASSES = range(45, 200, 10)

## Best estimate of fiducial cross-section, used to get estimate of ztautau
# ZTAUTAU_CSC = 94.9 # LHCb's Zll
# ZTAUTAU_CSC = (95.0 + 93.8)/2 # native Zmumu+Zee
# ZTAUTAU_CSC = 95.56 # from my Ztautau analysis
ZTAUTAU_CSC = ufloat(92.72, 5.36) # from my Ztautau analysis

## DT is the old nomenclature in Ztautau. In HMT, I can jsut use tau type.
DT_TO_TT = {
  'emu' : 'e',
  'h1mu': 'h1',
  'h3mu': 'h3',
  'mumu': 'mu',
  'mue' : 'e',
}

TT_TO_DT = {
  'mu': 'mumu',
  'h3': 'h3mu',
  'h1': 'h1mu',
  'e' : 'emu',
}

TT_TO_DTYPE = {
  'mu': 'mu_mu',
  'h3': 'h3_mu',
  'h1': 'h1_mu',
  'e' : 'e_mu',
}

TT_TO_LATEX = {
  'mu': '\\hmumu',
  'h1': '\\hmuh1',
  'h3': '\\hmuh3',
  'e' : '\\hmue',
}

TT_TO_ROOTLABEL = {
  'mu'    : '#it{#mu#tau}#scale[0.6]{#lower[0.4]{#mu}}',
  'h1'    : '#it{#mu#tau}#scale[0.6]{#lower[0.4]{h1}}',
  'h3'    : '#it{#mu#tau}#scale[0.6]{#lower[0.4]{h3}}',
  'e'     : '#it{#mu#tau}#scale[0.6]{#lower[0.4]{e}}',
  'simult': 'Simultaneous',
}

# Hijack, for compat & ntuple branch
# affecting `013.selected.export_tree`, `spec125`.
PREFIXES.update({
  'mu': ('mu1', 'mu2'),
  'e' : ('mu' , 'e'  ),
  'h1': ('mu' , 'pi' ),
  'h3': ('mu' , 'tau'),
})
PREFIXES_ALL.update({
  'mu': ('mu1', 'mu2'),
  'e' : ('mu' , 'e'  ),
  'h1': ('mu' , 'pi' ),
  'h3': ('mu' , 'tau', 'pr1', 'pr2', 'pr3' ),
})

## fine-tune the subscript
ROOTLABELS = {
  'e' : ('#it{#mu}', LABELS_TAU['e']),
  'h1': ('#it{#mu}', LABELS_TAU['h1']),
  'h3': ('#it{#mu}', LABELS_TAU['h3']),
  'mu': ('#it{#mu}', LABELS_TAU['mu']),
}

REGIME_TO_LATEX = {
  'nominal': 'Central',
  'lowmass': 'Low-mass',
  'tight'  : 'High-mass',
}
REGIME_TO_ROOTLABELS = REGIME_TO_LATEX

STRATEGY_TO_LATEX = {
  '1Simple' : 'Baseline',
  '2Poisson': 'Poisson counting',
  '3FitBkgs': 'Extended likelihood',
  '3FitBlob': 'Extended likelihood (blob)',
}
STRATEGY_TO_ROOTLABELS = STRATEGY_TO_LATEX

## Misc flags
FLAGS_TO_LATEX = {
  'process': 'Process',
  'regime' : 'Regime',
  'mass'   : '\\mH [\\!\\gevcc{}]',
  #
  'keyspdf' : 'RooKeysPdf',
  'histpdf' : 'RooHistPdf (const bin width)',
  'histstat': 'RooHistPdf (width per regime)',
  #
  'geo'    : 'Geometrical acceptance',
  '4pi'    : 'Inclusive',
  '4pi_pb' : 'Inclusive [pb]',
  'lhcb'   : 'LHCb geometrical acceptance',
  'lhcb_pb': 'LHCb geometrical acceptance [pb]',
  'lhcb_fb': 'LHCb geometrical acceptance [fb]',
  #
  'nosyst'  : 'Without uncertainties',
  'withsyst': 'With uncertainties',
  #
  'best%'  : 'Best fit [%]',
  'exp%'   : 'Expected limits [%]',
  'obs%'   : 'Observed limits [%]',
  'exp_pb' : 'Expected limits [pb]',
  'obs_pb' : 'Observed limits [pb]',
  'best_pb': 'Best fit [pb]',
  'exp_fb' : 'Expected limits [fb]',
  'obs_fb' : 'Observed limits [fb]',
  'best_fb': 'Best fit [fb]',
}

## Universal column rename dict
X_TO_LATEX = {}
X_TO_LATEX.update(COMPUTE_LABELS)
X_TO_LATEX.update(TT_TO_LATEX)
X_TO_LATEX.update(REGIME_TO_LATEX)
X_TO_LATEX.update(FLAGS_TO_LATEX)
X_TO_LATEX.update(PROCESS_LATEX)

X_TO_ROOTLABELS = {}
X_TO_ROOTLABELS.update(TT_TO_ROOTLABEL)
X_TO_ROOTLABELS.update(REGIME_TO_ROOTLABELS)
X_TO_ROOTLABELS.update(STRATEGY_TO_ROOTLABELS)
X_TO_ROOTLABELS.update(FLAGS_TO_LATEX)

#===============================================================================

def X_to_latex(df0, **kwargs):
  """
  Allow on-the-fly additional converters
  """
  df = df0.copy()
  df = df.rename(X_TO_LATEX, columns=X_TO_LATEX)
  df = df.rename(kwargs, columns=kwargs)
  df.index.names = [X_TO_LATEX.get(s, s) for s in df.index.names]
  df.index.names = [kwargs.get(s, s) for s in df.index.names]
  return df

def printer_minmax(se):
  """
  Helper printer to be applied on the pd.Series to collapse the mass axis 
  appropriately for latex representation.
  """
  ## If similar magnitude, show single number
  if se.max()-se.min() < 0.05:
    return '{:.2f}'.format((se.min()+se.max())/2)
  ## Otherwise, show the range
  return '{:.1f}--{:.1f}'.format(se.min(), se.max())

#===============================================================================
# CONTEXT
#===============================================================================

def get_current_dtype_dt_tt():
  ## shorthands to get current sys.argv context
  dtype, dt = packages.id_utils().get_current_dtype_dt()
  return dtype, dt, DT_TO_TT[dt]

#===============================================================================
# STRING
#===============================================================================

def intv_to_mid(s):
  """
  Force int result
  >>> intv_to_mid('(30, 40]')
  35
  """
  res = re.findall(r'\((\d+), (\d+)\]', s)
  if not res: # do nothing if fails to convert
    return s
  s1,s2 = res[0]
  return (int(s1)+int(s2))/2

#===============================================================================
# TREES
#===============================================================================

## For Higgs -> mu tau signal
TRUECHAN_HMT = {
  'mu': ( 'tau_type_mu>=1', 'tau_type_h1==0', 'tau_type_h3==0', 'tau_type_e==0' ),
  'h1': ( 'tau_type_mu==0', 'tau_type_h1>=1', 'tau_type_h3==0', 'tau_type_e==0' ),
  'h3': ( 'tau_type_mu==0', 'tau_type_h1==0', 'tau_type_h3>=1', 'tau_type_e==0' ),
  'e' : ( 'tau_type_mu==0', 'tau_type_h1==0', 'tau_type_h3==0', 'tau_type_e>=1' ),
}

def apply_alias(tt, tname, tree):
  ## Override the di-tau true channel alias. HACK WITH CARE
  # Note that this is the base alias used by many derived aliases.
  if 'higgs' in tname:
    tree.SetAlias('_TrueChan', utils.join(TRUECHAN_HMT[tt]))

#===============================================================================
