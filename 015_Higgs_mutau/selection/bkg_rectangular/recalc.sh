#!/usr/bin/env bash
# 
# Helper script to recalculate expected candidates when selection changed.
#

start=`date +%s`

# ## Clean all, use with extreme care!
# rm -rf rect_selected/staging/*.root
# rm -rf rect_selected/*.root
# rm -rf rect_samesign
# rm -rf */*.df

## clean only one regime & one ttype
## careful of regime of similar name
regime=tight
ttype=h1
echo 'Recalculating with:' ${regime} ${ttype}
rm -vrf rect_selected/${regime}.root
rm -vrf rect_selected/staging/${ttype}_${regime}.root
rm -vrf rect_context/make_context_all.df
rm -vrf rect_context/*_${regime}_${ttype}.df
rm -vrf rect_ztautau/erec.df
rm -vrf rect_ztautau/erec_raw_${regime}_${ttype}.df
rm -vrf rect_samesign/ss/${regime}-${ttype}-*.pdf
rm -vrf rect_samesign/*_${regime}_${ttype}.df
rm -vrf summary/results.df
rm -vrf $DIR15/efficiencies/rec_detailed/*_${regime}*.df 

## 
./rect_selected.py --recalc
./rect_context.py --recalc
./rect_ztautau.py --recalc
./rect_samesign.py --recalc
./summary.py --recalc

## time elapsed
end=`date +%s`
echo Time elapsed $((end-start)) seconds
