#!/usr/bin/env python
"""

Provide common utils useful for rectangular selection

"""
import os
import sys
import imp
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15')) # for tethered import
import HMT_utils
from HMT_utils import DTYPES, CHANNELS, TT_TO_DTYPE, packages, memorized, get_current_dtype_dt_tt

#===============================================================================
# LOCALIZED PRESELECTED TREES
# i.e., with specific alias under rectangular cuts
#===============================================================================

@memorized
def _load_cutmodule(regime):
  """
  Helper to load cutmodule as a function of mass,
  also provide the explicit null cuts & TrueChan to HMT
  """
  path = os.path.expandvars('$DIR15/selection/bkg_rectangular/spec_%s.py')%regime
  cutmodule = imp.load_source('spec_'+regime, path)

  ## apply null cuts
  nulldict_dtype = {dtype:'1==1' for dtype in DTYPES}
  nulldict_dt    = {dt:'1==1'    for dt    in CHANNELS}
  cutmodule.CUTS_PRESEL_NOBADRUN = ''
  cutmodule.CUTS_PRESELECTION    = nulldict_dtype
  cutmodule.CUTS_MIXISOLATION    = nulldict_dtype
  cutmodule.CUTS_IMPPAR_PROMPT   = ''
  cutmodule.CUTS_PRESELECTION_MUMU_WITHPEAK = ''
  cutmodule.CUTS_PRESELECTION_EE_WITHPEAK   = ''
  return cutmodule


def _get_aliaser(tt, regime):
  """
  Return the aliser function(tname, tree), at the fixed (tt,mass)
  """
  dtype     = TT_TO_DTYPE[tt]
  cutmodule = _load_cutmodule(regime)
  def aliaser(tname, tree):
    packages.id_utils().apply_alias(cutmodule, dtype, tname, tree)
    HMT_utils.apply_alias(tt, tname, tree)
  return aliaser  


@memorized
def load_presel_trees_with_aliases(tt, regime):
  """
  Wrapper of preselection.load_trees_mc, attached with the 
  mass-dependent alias.
  """
  ## put inside here, as `preselection` also import `ditau_cuts` early.
  sys.path.append(os.path.expandvars('$DIR15/selection')) # for cutmodule
  import preselection

  ## Get localized aliaser
  aliaser = _get_aliaser(tt, regime)

  ## Load the tree, populate with extra ones (e.g., noiso)
  trees = preselection.load_trees_dict(tt)
  ROOT.gROOT.cd() # need this to temporary carry the noiso-cloned trees.
  # for tag in ['wmu17jet', 'zmu17jet']:

  ## only what's really needed for ssfit histo.
  tags = ['wmu17jet', 'zmu17jet', 'ztautau', 'wtaujet']
  if tt == 'e':
    tags.append('we')
  for tag in tags: 
    for prefix in ['t_', 'tss_']:
      tname = prefix+tag
      t2 = trees[tname].CopyTree('') # clone somewhat doesn't work
      tname += '_noiso'
      t2.name = tname
      trees[tname] = t2

  ## Apply alias
  for tname, tree in trees.iteritems():
    aliaser(tname, tree)

  ## Finally
  logger.info('Finish prep aliases: [%s] %s'%(regime, tt))
  return trees


def load_presel_trees_with_aliases_groupped(tt, regime):
  """
  Helper to load alltrees, groupped
  """
  d = load_presel_trees_with_aliases(tt, regime)
  return packages.id_utils().indexing_trees_os_ss(d)

#===============================================================================

if __name__ == '__main__':
  pass

  # print load_presel_trees_with_aliases('mu')
  # print load_presel_trees_with_aliased_groupped('h3')

  tree = load_presel_trees_with_aliases('h3', 'lowmass')['t_real']
  print tree.entries
  print 'presl', tree.count_ent_evt('Preselection')
  print 'prong', tree.count_ent_evt('Preselection & _Prongs')
  print 'iso  ', tree.count_ent_evt('Preselection & _Prongs & _Iso')
  print 'displ', tree.count_ent_evt('Preselection & _Prongs & _Iso & _Displ')
  print 'apt  ', tree.count_ent_evt('Preselection & _Prongs & _Iso & _Displ & _APT')
  print 'dphi ', tree.count_ent_evt('Preselection & _Prongs & _Iso & _Displ & _APT & _DPHI')
  print tree.count_ent_evt('Selection')

