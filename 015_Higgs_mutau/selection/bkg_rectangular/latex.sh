#!/usr/bin/env bash
set -e

## Vars
PROJ=${HOME_EPFL}/Papers/007_HiggsLFV
DATE=`date +%y%m%d`

## plot_signal
plot_signal(){
  rm -f plot_signal/*.pdf
  rm -f plot_signal/*.root
  ./plot_signal.py --redraw
  cd plot_signal
  QHistPdfExporter.py plot_signal.root \
    --width=360 --height=240 \
    --legend-header='LHCb preliminary' \
    --pad-margin-bottom=0.16 \
    --pad-margin-left=0.12 \
    --xaxis-morelog \
    --xaxis-noexponent \
    --xaxis-offset=1.00 \
    --yaxis-offset=0.64 \
    --legend-top=0.95 \
    --legend-left=0.7 \
    --legend-bottom=0.58 \
    --legend-text-size=13
  cd ..
  rm -rf plot_signal/$DATE                         # remove existing, otherwise rename fails
  mv plot_signal/minipdf plot_signal/$DATE         # rename subdir
  cp plot_signal/trio*.pdf plot_signal/$DATE/      # copy the trio plot (not QHist) into archive
  rm -rf $PROJ/figs/plot_signal                    # remove remote
  cp -vR plot_signal/$DATE $PROJ/figs/plot_signal  # copy to latex
}

## Varbin
varbin(){
  rm varbin/*.df                        # remove pickle cache
  ./varbin.py --redraw                  # rerun drawing & cache results
  mkdir -p varbin/$DATE                 # make clean dir
  mv varbin/*.pdf varbin/$DATE          # move into archive
  cp varbin/$DATE/* $PROJ/figs/varbin   # copy to latex  
}

## Samesign
# note: this doesn't re-run the drawing.
samesign(){
  ./ss_export.py  # this makes a subdir "selected" in "rect_samesign"
  rm -rf rect_samesign/$DATE                     # remove existing archive
  mv rect_samesign/selected rect_samesign/$DATE  # rename into archive
  cp -R rect_samesign/$DATE/*.pdf $PROJ/figs/ss  # copy to latex  
}

## Normalizer table
normalizer(){
  rm -rf normalizer/*.txt               # clear orphans
  ./rect_context.py --latex             # populate tables
  mkdir -p normalizer/$DATE             # make clean subdir
  mv normalizer/*.txt normalizer/$DATE  # move into archive
  cp normalizer/$DATE/* $PROJ/tables    # copy to latex  
}

## Modules
# plot_signal
# varbin
# samesign
# normalizer

## Finally, update IPS
# rm -f $PROJ/ANA/05_background.tex

echo "Finished!"
