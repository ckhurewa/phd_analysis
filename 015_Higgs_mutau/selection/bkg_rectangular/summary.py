#!/usr/bin/env python
"""

For the study of bkg using rectangular cut method.

"""

from PyrootCK import *

## Base tools, careful of bing overridden
import rect_utils # tethering
from HMT_utils import (
  DT_TO_TT, REGIMES, MASSES, REGIMES_ALL, X_to_latex, Idx,
  pd, memorized, pickle_dataframe_nondynamic, fmt2cols, inject_midrule,
)
from uncertainties import nominal_value, std_dev, correlation_matrix

#===============================================================================
# COLLECT ALL SOURCES
#===============================================================================

# @memorized
@pickle_dataframe_nondynamic
def results():
  """
  Collect all processes together.
  Including the dev-regime here for completeness. 
  The derivative functions below can filter them out upon context.
  """
  import rect_ztautau
  import rect_zll
  import rect_samesign
  import rect_context

  acc = {}
  for rg in REGIMES_ALL:
    get = lambda proc: rect_context.get_norms(proc, rg)
    ## specific order
    df = pd.DataFrame()
    df['Ztau']  = rect_ztautau.results()[rg]
    df['Zll']   = rect_zll.results()[rg]
    df['QCD']   = rect_samesign.result_qcd()[rg]
    df['EWK']   = rect_samesign.result_ewk()[rg]
    df['VV']    = get('WW_lx') + get('WZ_lx')
    df['ttbar'] = get('ttbar_41900006') + get('ttbar_41900007') + get('ttbar_41900010')
    df['Zbb']   = get('zbb')

    ## Repackage
    df['BKG'] = df.sum(axis=1)
    df['OS']  = rect_context.results_data_OS_SS().loc[rg,'OS'].apply(lambda x: var(x, 'stat'))
    acc[rg]   = df.T

  ## finally
  df = pd.concat(acc)
  df.index.names = 'regime', 'process'
  return df

#-------------------------------------------------------------------------------

@memorized
def results_comb():
  """
  Suitable for combination plot, where ttbar+VV+Zbb are combined into other
  Note: Keep all regimes.
  """
  LIST_OTHERS = ['ttbar', 'VV', 'Zbb']
  def collapse(df):
    df   = df.xs(df.name).T
    col  = df[LIST_OTHERS].sum(axis=1)
    icol = len(df.columns)-2
    df.insert(icol, 'Other', col)
    return df.T.drop(LIST_OTHERS)
  return results().groupby(level=0).apply(collapse)

def results_detailed():
  """
  Detailed version of results function above with Zll splitted into Zmumu, Zee
  """
  df    = results()
  misid = results_zll_misid()['expected']
  df.loc['Zmu', 'e'] = misid['mue']
  df.loc['Ze' , 'e'] = misid['emu']
  return df

@memorized
def results_roofit():
  """
  Simplifed version suitable for RooFit
  - discard BKG, OS. Keep only components.
  - Use others
  - Throwaway unused regime
  """
  idx = [rg for rg in REGIMES if 'old' not in rg]
  df  = results_comb().copy().drop(['BKG', 'OS'], level=1)
  df  = df.loc[idx]
  df.index.names = 'regime', 'process'
  df.columns.name = 'tt'
  return df

@memorized
def results_upperlim():
  """
  Basic upperlim computation: delta + sigma*2
  """
  def func(df):
    df = df.xs(df.name)
    return (df.loc['OS'] - df.loc['BKG']).apply(lambda u: u.upperlim())
  return results().groupby(level=0).apply(func)

@memorized
def results_bestfit():
  """
  Like above, but simplify for bestfit = OBS-EXP
  Preserve the uncertainty for BLUE
  """
  def func(df):
    df = df.xs(df.name)
    return df.loc['OS']-df.loc['BKG']
  return results().groupby(level=0).apply(func)

#===============================================================================

def check_excess():
  raise NotImplementedError

  import rect_selected

  acc0  = {}
  res   = results()
  fracs = rect_selected.fraction_mass_window_all()
  for rg in REGIMES:
    if rg not in ('lowmass', 'lowmass2'):
      continue
    acc1 = {}
    n = res.loc[rg].drop('BKG')
    for mass in MASSES:
      f   = fracs.loc[rg,mass].unstack().T
      df  = n*f
      obs = df.loc['OS']
      bkg = df.drop('OS').sum()
      acc1[mass] = obs-bkg
    acc0[rg] = pd.concat(acc1).unstack()
  df = pd.concat(acc0)
  print df.applymap(lambda u: '---' if abs(u.s)>abs(u.n) else '{:.1f}'.format(u)).to_string()

@memorized
def corr_nss():
  """
  Return correlation coefficient of nQCD+nVj.
  """
  df = results_roofit().stack().unstack('process')[['QCD', 'EWK']]
  return df.apply(lambda se: correlation_matrix(se)[0,1], axis=1)

#===============================================================================
# LATEX
#===============================================================================

def latex_results():
  """
  Print final result for selected regimes.
  """
  df = results().loc[list(REGIMES)]
  df.index.names = 'Regime', 'Process'
  ## Explicitly mark empty amount as null
  df = df.replace(0, '---')
  ## Ready, convert to latex
  msg = X_to_latex(df).to_bicol_latex()
  # add linebreak between BKG, OS manually
  msg = inject_midrule(msg, 23,24, 33,34, 43,44, replace='\\cmidrule{2-10}')
  msg = msg.replace('Backgrounds', 'Total background')
  # finally
  return msg

def latex_syst():
  """
  Relative systematics for each BKG.

  Note: Grouped QCD+Vj together as they are correlated,
  as requested by the review referee.
  """
  ## Load + add the same-sign (QCD+Vj) together
  df0 = results_comb().unstack('regime').T
  df0['SS'] = df0['QCD']+df0['EWK']
  df0 = df0.T.stack('regime').swaplevel().sort_index()
  ## Load, drop OS, re-order to have BKG last
  # procs = 'Ztau', 'Zll', 'QCD', 'EWK', 'Other', 'BKG'
  procs = 'Ztau', 'Zll', 'SS', 'QCD', 'EWK', 'Other', 'BKG'
  df0 = df0.reindex(list(procs), level=1)
  df = df0.loc[Idx[list(REGIMES), list(procs)], Idx[:]]
  
  ## In each regime, get relative uncertainty
  def rerr(df):
    nbkg = df.xs('BKG', level='process').applymap(nominal_value)
    return (df/nbkg).applymap(std_dev)
  df = df.groupby(level='regime').apply(rerr)  
  ## fine-tune 0.00 as null (in some tight channels)
  df = df.replace(0, '---')
  ## Print for latex, in percentage
  df  = df.rename({'BKG': 'Total'})
  msg = X_to_latex(df).fmt2p.to_bicol_latex()
  ## add indent mark for QCD,Vj components
  msg = msg.replace('& QCD &', '& $\\hookrightarrow$ QCD &')
  msg = msg.replace('& \\Vj &', '& $\\hookrightarrow$ \\Vj &')
  ## add linebreak between BKG, OS manually
  msg = inject_midrule(msg, 11,19,27, replace='\\cmidrule{2-6}')
  ## Use manual tabu env to have smaller-font row
  msg = msg.replace('\\begin{tabular}{llllll}', '\\begin{tabu}{llrrrr}')
  msg = msg.replace('\\end{tabular}', '\\end{tabu}')
  msg = inject_midrule(msg, 8,9,17,18,26,27, replace='\\rowfont{\\footnotesize}')
  msg = inject_midrule(msg, 12,23,34, replace='\\rowfont{\\normalsize}')

  return msg

def latex_nss_correlation():
  """
  Return correlation factor between QCd and Vj backgorund.
  One factor is need as it's just 2x2 sym matrix.
  """
  fmt = lambda x: '-' if pd.np.isnan(x) else '$%.2f$'%x
  return X_to_latex(corr_nss().unstack('tt')).applymap(fmt).to_markdown(stralign='right')

#===============================================================================

if __name__ == '__main__':
  if '--recalc' in sys.argv:
    print results().fmt2f
    sys.exit()

  ## Final
  # print results().fmt1f
  # print results().loc[['tight', 'oldtight']].fmt2f
  # print results_comb().fmt2f
  # print results_detailed()
  # print results_roofit()
  # print results_upperlim().fmt1f
  # print results_bestfit()
  # print corr_nss()

  ## Latex
  # print latex_results()
  print latex_syst()
  # print latex_nss_correlation()
