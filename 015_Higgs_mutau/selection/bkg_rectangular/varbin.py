#!/usr/bin/env python

import sys
import rect_selected # tethered
from qhist import QHist, ROOT
from HMT_utils import (
  TTYPES, REGIMES, TT_TO_LATEX, PROCESS_LATEX, REGIME_TO_LATEX, ROOTLABELS,
  pd, df_from_path, memorized, pickle_dataframe_nondynamic, packages,
)
ROOT.gROOT.batch = True

## data
import summary

## borrow varbin function
postprocessors = packages.qhist_postprocessors()

#===============================================================================
# COMB WITH VARIABLE BIN WIDTH
#===============================================================================

@pickle_dataframe_nondynamic
def draw_varbin(regime, tt):
  
  ## Prepare inputs
  trees    = rect_selected.load_trees_comb(tt, regime)
  res_raw  = summary.results_comb().loc[regime, tt]
  entries  = [ res_raw[t.name].n for t in trees] # get expected value for each tree

  ## Prepare struct
  h = QHist()
  h.name      = regime+'_'+tt
  h.trees     = trees
  h.params    = 'mass:weight'
  h.normalize = entries
  h.xmin      = 20
  h.xmax      = 200
  h.xlog      = True
  h.xlabel    = 'm(%s%s) [GeV/c^{2}]'%ROOTLABELS[tt]
  h.ylabel    = 'Candidates'

  ## Finally, run the engine, add more result, persist
  res = postprocessors.variable_binning(h)
  res['nobs'] = entries[0]
  res['nbkg'] = sum(entries[1:])
  return pd.Series(res)

#===============================================================================

def latex_stats():
  """
  Show fit statistics, with chi2/ndf
  Note: FOM not available because S is not defined
  """ 
  ## Load & add fields
  cols = ['ndf', 'chi2pdof']
  df   = df_from_path('draw_varbin', __file__).unstack()[cols]
  tups = [s.split('_') for s in df.index]
  df.index        = pd.MultiIndex.from_tuples(tups, names=['Regime', 'Channel'])
  df['ndf']       = df['ndf'].apply('${:.0f}$'.format) # force int
  df['chi2pdof']  = df['chi2pdof'].apply('${:.2f}$'.format)
  df.columns.name = 'Variable'
  # finally
  df = df.stack().unstack('Channel').sort_index()
  df = df.rename(REGIME_TO_LATEX).rename(columns=TT_TO_LATEX)
  df = df.rename({'chi2pdof': '\\chisq/ndf'})
  return df.to_markdown(stralign='right')

#===============================================================================

if __name__ == '__main__':
  if '--redraw' in sys.argv:
    ## For consistent style
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
    for rg in REGIMES:
      for tt in TTYPES:
        draw_varbin(rg, tt)
    sys.exit()

  ## LATEX
  print latex_stats()
