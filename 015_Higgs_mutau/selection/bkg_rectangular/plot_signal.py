#!/usr/bin/env python

import sys
import ROOT
from qhist import QHist
from qhist.styles import set_qhist_color_palette
import rect_selected # tethered
import summary 
from HMT_utils import TTYPES, PREFIXES, ROOTLABELS, REGIMES_ALL
ROOT.gROOT.batch = True

#===============================================================================
# PUTTING ALL BACKGROUNDS TOGETHER
#===============================================================================

## TMVA blue
COLOR_BLUE = ROOT.TColor(0.50, 0.61, 0.80)

def _higgs_overlay(h, tt, regime):
  """
  Given the qhist instance of obs + bkg histos, draw the obs-sig-bkg plot,
  where all bkgs are collapsed into one.
  """
  ## Load the trees of higgs, cached
  mass = {
    'nominal'   : 85, #125,
    'lowmass'   : 55,
    'tight'     : 125, #155,
    #
    'oldnominal': 125,
    'oldlowmass': 55,
    'oldtight'  : 155,
  }[regime]
  tname   = 't_higgs_%i'%mass
  t_higgs = rect_selected.load_trees_dict(tt, regime)[tname]

  ## Prepare elements
  h_bkg = h.anchors.stack.stack.Last() # this loses axis label
  h_bkg.XTitle = h.xlabel
  h_bkg.YTitle = h.ylabel
  h_obs = h[0]
  h_sig = h_obs.Clone('h_sig')
  h_sig.Reset()
  t_higgs.Draw('mass >> h_sig', 'weight', 'goff')
  h_sig.Scale(h_obs.Integral()**0.5 / h_sig.Integral()) # scale to stats
  h_sig.Sumw2(False) # delete structure

  ## Additional figure for PAPER: draw Higgs signal directly on bkg composition.
  ## This is a little difficult to see...
  h.anchors.mainpad.cd()
  h_sig.lineWidth = 4
  h_sig.markerSize = 0
  h_sig.lineColor = ROOT.kCyan
  h_sig.Draw('h same')
  h.anchors.legend.AddEntry(h_sig, '#it{H} #scale[0.8]{(%i GeV/#it{c}^{2})}'%mass, "l")
  for ext in ('pdf', 'png', 'C'): # all publication extensions
    h.anchors.save('plot_signal/all_%s_%s.%s'%(regime, tt, ext))
  
  ## note: return here, if I continue down below, the second iteration of the
  ## redraw loop will fails. Investigate me later, I'm hungry now.
  return

  ## Decoration, a la TMVA
  h_bkg.lineColor      = ROOT.kRed
  h_bkg.fillColor      = ROOT.kRed
  h_bkg.fillStyle      = 3004
  h_sig.fillStyle      = 1001 # solid
  h_sig.fillColorAlpha = COLOR_BLUE.number, 0.5
  h_sig.lineColor      = ROOT.kBlue
  h_sig.lineWidth      = 1
  h_sig.markerSize     = 0
  h_obs.markerSize     = 1

  ## Title for legend
  h_sig.title = 'H #rightarrow #mu#tau (%i GeV/#it{c}^{2})'%mass
  h_bkg.title = 'Backgrounds'
  h_obs.title = 'Data'

  ## Adjust ymax then Draw, from back to front
  ymax = max(h_obs.maximum, h_bkg.maximum)*1.1
  ymax += ymax**0.5 # scale up by its Poisson error
  h_bkg.maximum = ymax
  h_bkg.minimum = 1e-3  # suppress zero hairline
  c = ROOT.TCanvas('trio', 'trio')
  h_bkg.Draw('h')
  h_sig.Draw('h same')
  h_obs.Draw('PE same')

  ## Legend
  leg = c.BuildLegend()
  leg.ConvertNDCtoPad()
  leg.primitives.Reverse()
  leg.x1NDC = 0.55
  leg.x2NDC = 0.95
  leg.y1NDC = 0.70
  leg.y2NDC = 0.92
  leg.Draw()

  ## finally, save
  c.RedrawAxis()
  c.SaveAs('./plot_signal/trio_%s_%s.pdf'%(regime, tt))
  c.Delete()

#===============================================================================

def postpc(h):
  ## adjust the legend
  leg = h.anchors.legend
  leg.header = 'LHCb #sqrt{#it{s}} = 8 TeV'
  leg.X1NDC = 0.58
  leg.X2NDC = 1.07
  leg.Y1NDC = 0.46
  leg.Y2NDC = 0.95
  leg.textSize = 0.07
  ## axis label
  h.anchors.stack.xaxis.labelSize   *= 1.1
  h.anchors.stack.xaxis.titleOffset *= 0.90
  h.anchors.stack.xaxis.titleSize   *= 1.2
  h.anchors.stack.yaxis.titleOffset *= 0.80
  h.anchors.stack.yaxis.titleSize   *= 1.25
  ## margin
  h.anchors.mainpad.topMargin *= 0.10
  h.anchors.mainpad.leftMargin *= 1.0
  h.anchors.mainpad.rightMargin *= 0.56
  h.anchors.mainpad.bottomMargin *= 1.05
  h.anchors.update()

def draw_combine(regime, tt):
  ## Prep data
  trees    = rect_selected.load_trees_comb(tt, regime)
  res_raw  = summary.results_comb().loc[regime, tt]
  entries  = [res_raw[t.name].n for t in trees] # get expected value for each tree
  prefixes = PREFIXES[tt]
  labels   = ROOTLABELS[tt]

  ## custom PT label
  fmtpt = 'p#scale[0.6]{{#lower[0.4]{{T}}}}({})'.format
  mutau = ''.join(ROOTLABELS[tt])

  ## conf
  H = QHist()
  H.filters   = 'weight'
  H.suffix    = regime+'_'+tt # for latex macro "twelvefigures"
  H.trees     = trees
  H.normalize = entries
  H.st_code   = 2
  H.batch     = True
  H.auto_name = True
  H.ymin      = 1e-3 # To remove zero hairline
  H.postproc = postpc

  ## mass only
  h = H()
  h.params = 'mass'
  h.xmin   = 0
  h.xmax   = 200
  h.xbin   = 40
  h.xlabel = '#it{m}(%s) [GeV/#it{c}^{2}]'%mutau
  h.ylabel = 'Candidates / (%.0f GeV/#it{c}^{2})'%(h.xmax/h.xbin)
  h.draw()
  
  ## hijack
  _higgs_overlay(h, tt, regime)
  return

  ## Others
  H.ylabel = 'Candidates'
  H(params='PT/1e3', xmin=1 , xmax=1e3, xbin=40, xlabel=fmtpt(mutau), xlog=True).draw()
  H(params='ETA' , xmin=2.0 , xmax=8.0, xbin=24, xlabel='#eta(%s)'%mutau).draw()
  H(params='Y'   , xmin=1.8 , xmax=4.6, xbin=28, xlabel='Y(%s)'%mutau).draw()
  H(params='PT1' , xmin=20  , xmax=200, xbin=40, xlabel=fmtpt(labels[0]), xlog=True).draw()
  H(params='PT2' , xmin=5   , xmax=100, xbin=40, xlabel=fmtpt(labels[1]), xlog=True).draw()
  H(params='ETA1', xmin=2.0 , xmax=4.5, xbin=20, xlabel='#eta(%s)'%labels[0]).draw()
  H(params='ETA2', xmin=2.0 , xmax=4.5, xbin=20, xlabel='#eta(%s)'%labels[1]).draw()
  H(params='IP1' , xmin=1e-4, xmax=10 , xbin=40, xlog=True).draw()
  H(params='IP2' , xmin=1e-4, xmax=10 , xbin=40, xlog=True).draw()

#-------------------------------------------------------------------------------

def draw_combine_all():
  for rg in REGIMES_ALL:
    for tt in TTYPES:
      draw_combine(rg, tt)  

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  set_qhist_color_palette()
  if '--redraw' in sys.argv:
    draw_combine_all()
    sys.exit()

  draw_combine('lowmass', 'h1')
