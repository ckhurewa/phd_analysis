#!/usr/bin/env python

from PyrootCK import *
import rect_selected
import rect_context
from HMT_utils import (
  pd, packages, pickle_dataframe_nondynamic, memorized,
  ROOTLABELS, TTYPES, TT_TO_DT, REGIMES, REGIMES_ALL, X_to_latex,
)

## path to cache, used in ss_export
PATH_SAMESIGN = os.path.splitext(os.path.abspath(__file__))[0]

## Optional override
SPEC_SSFIT = {
  'oldnominal_h1': 'oldnominal-h1_tf_72_false',
  #
  'lowmass_mu'   : 'lowmass-mu_tf_36_false',
  'lowmass_e'    : 'lowmass-e_tf_24_false',
  'nominal_e'    : 'nominal-e_tf_40_false',
  #
  'tight_h3'     : 'tight-h3_tf_24_false', # limited stat, don't use too-many bins
  'tight_h1'     : 'tight-h1_tf_20_false', # limited stat, don't use too-many bins
}

## For rEWK calculation
SPEC_EWK = {
  'mu': {
    'wmumujet': True,
    'zmu17jet': None,
  },
  'h1': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
  'h3': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
  'e': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
}

#===============================================================================
# HELPERS
#===============================================================================

def get_trees_ssfit(regime, tt):
  """
  Load the set of trees ready for ssfit.
  """
  trees = rect_selected.load_trees_dict(tt, regime)

  ## allow t1 to be null
  t1 = trees.get('tss_real', None)
  if t1:
    t1.title = 'Data'

  ## data-driven QCD
  t2 = trees['tssdd_qcd']
  t2.title = 'QCD'

  ## More detail on EWK tree
  t3 = trees[{
    'oldnominal': {
      'mu': 'tss_wmumujet',
      'h1': 'tss_wmu17jet',
      'h3': 'tss_dymu',
      # 'h3': 'tss_wmu17jet_noiso',
      # 'e' : 'tss_wmu17jet_noiso',
      'e' : 'tss_dymu',
    },
    'nominal': {
      'mu': 'tss_wmumujet',
      #
      # 'h1': 'tss_dymu',
      'h1': 'tss_wzjet',
      #
      'h3': 'tss_dymu',
      # 'h3': 'tss_wmu17jet_noiso',
      #
      # 'e' : 'tss_wmu17jet_noiso',
      'e' : 'tss_dymu',
    },
    'oldlowmass': {
      'e' : 'tss_wzjet',
      'h1': 'tss_wzjet',
      'h3': 'tss_wzjet',
      'mu': 'tss_wmumujet',
    },
    'lowmass': {
      'e' : 'tss_wzjet',
      'h1': 'tss_wzjet',
      'h3': 'tss_wzjet',
      'mu': 'tss_wmumujet',
    },
    'tight': {
      'e' : 'tss_wzjet_noiso',
      'h1': 'tss_wzjet',
      'h3': 'tss_wzjet_noiso',
      'mu': 'tss_wzjet_noiso',
    },
    'oldtight': {
      'e' : 'tss_dymu',
      'h1': 'tss_wzjet_noiso',
      'h3': 'tss_dymu',
      'mu': 'tss_wmu17jet_noiso',
    },
  }[regime][tt]]
  t3.title = 'V+jet'

  ## Hard electron (already remove by DPT cut in emu)
  # if tt == 'e':
  #   t3.title = 'V_{#mu}+jet'
  #   t4 = trees['tss_we']
  #   return t1, t2, t3, t4
  # else:
  #   return t1, t2, t3

  # # # ##
  # if regime=='lowmass' and tt=='h1':
  #   sys.path.append('..')
  #   import preselection
  #   tree0 = preselection.load_trees_dict('h1')['tssdd_qcd']
  #   # filt = utils.join([
  #   #   'mu_0.50_cc_IT   < 0.4',
  #   #   'pi_0.50_cc_IT   < 0.4',
  #   #   # 'mu_0.50_cc_vPT  > 10E3',
  #   #   # 'pi_0.50_cc_vPT  > 10E3',
  #   #   'mu_BPVIP > 0',
  #   #   'mu_BPVIP < 0.05',
  #   #   'pi_BPVIP > 0.01',
  #   #   'DPHI > 2.7',
  #   #   'APT < 0.4',
  #   # ])
  #   # # t2 = tree0.drop(filt, by=['mu_PT', 'pi_PT'], ascending=[False, False])
  #   # t2 = tree0.drop(filt)
  #   # t2.SetAlias('DPT', '(mu_PT-pi_PT)/1e3')
  #   return t1, t2, t3 #, trees['tss_wtaujet_noiso']

  return t1, t2, t3

#===============================================================================
# BREAK SAMESIGN
#===============================================================================

def debug_draw(regime, tt):
  trees = get_trees_ssfit(regime, tt)

  ## debug
  trees0 = rect_selected.load_trees_dict(tt, regime)
  trees = [
    # trees0['tss_wmu17jet'],
    # trees0['tss_zmu17jet'],
    trees0['tss_wzjet'],
    trees0['tss_wzjet_noiso'],
    # trees0['tss_dymu'],
    # trees0['t_zmu17jet'],
    # trees0['t_dymu'],
    # trees0['tss_wtaujet_noiso'],
    # trees0['t_wtaujet_noiso'],
  ]

  h = QHist()
  h.trees   = trees
  h.params  = 'DPT'
  h.xmin    = -80
  h.xmax    = 120
  h.xbin    = 50
  # h.params  = 'TMath::Abs(DPT)/(PT1+PT2)'
  # h.xmin    = 0
  # h.xmax    = 1
  # h.xbin    = 20
  # h.params  = 'M/1e3'
  # h.xmin    = 0
  # h.xmax    = 200
  # h.xbin    = 40
  # h.normalize = False
  h.draw()
  exit()

@pickle_dataframe_nondynamic
def break_samesign(regime, tt):
  """
  Note: need to name the suffix as dt because `id_utils.result_ssfit_raw`
  use complicate rewiring between emu+mue
  - Able to drop Ztautau, emu (hard electron) contribution.
  """
  ## Apply LHCb style
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Fetch trees into the engine
  ROOT.gROOT.SetBatch(True)
  trigger_RooFit_patch()
  trees = get_trees_ssfit(regime, tt)
  tag   = regime+'-'+tt

  ## helper to yield preprocessor
  def lim_ewk(frac):
    def preprocessor(fitter):
      logger.info('Apply constrain: %.3f'%frac)
      fitter.Constrain(1, frac, 1.0)
      return fitter
    return preprocessor

  ## min limit is expected number of MC.
  pp = None
  if regime in ('lowmass', 'oldlowmass') and tt=='h1':
    pp = lim_ewk(0.20)
  if regime in ('nominal',) and tt=='h1':
    pp = lim_ewk(0.38)
  if regime in ('tight',) and tt=='h1':
    pp = lim_ewk(0.84)

  exe = packages.samesign_utils().ssfit_engine
  return exe(tag, trees, preprocessor=pp)


def break_samesign_all():
  """
  Loop over all ttypes & mass, break samesign into QCD+EWK
  """
  for regime in REGIMES_ALL:
    for tt in TTYPES:
      break_samesign(regime, tt)


#===============================================================================
# COMPUTE
#===============================================================================

@memorized
def parse_spec_ssfit():
  """
  Return a dict of ssfit, with auto-pick if needed.
  """
  return packages.samesign_utils().guess_ssfit_spec(__file__, SPEC_SSFIT)


@memorized
def load_rewk(regime):
  """
  Wrapper to load the rEWK multiplication factor, from the given spec & context.
  This is nothing to do with the fitting result.
  """
  context = rect_context.get_default_context(regime)
  return packages.samesign_utils().load_rewk(context, SPEC_EWK, 'selection', 'selnoiso')


@memorized
def result_ssfit_raw(regime):
  """
  Raw result where it's only parsed against the spec.
  """
  spec0   = parse_spec_ssfit()
  spec    = spec0.filter(regex=r'^'+regime+'_', axis=0).rename(index=lambda s: s.split('_')[1])
  data_ss = rect_context.results_data_OS_SS()['SS'].loc[regime]
  tag     = 'break_samesign_'+regime
  return packages.samesign_utils().result_ssfit_raw_HMT(spec, __file__, data_ss, tag)


def compare_ewk_mc_ssfit(regime):
  """
  Compare this result before proceeding any further
  """
  context  = rect_context.get_default_context(regime)
  raw_rewk = packages.samesign_utils().load_rewk(context, None, 'selection', 'selnoiso')
  df = packages.samesign_utils().compare_ewk_mc_ssfit(raw_rewk, result_ssfit_raw(regime))
  df = df[['NSS', 'spec', 'W-SS', 'Z-SS', 'W+Z (MC)', 'Data', 'compat']] # concise
  return df

def compare_ewk_mc_ssfit_all():
  for rg in REGIMES_ALL:
    print compare_ewk_mc_ssfit(rg).fmt1f

@memorized
def get_rQCD():
  """
  Provide names to join in latex.
  """
  df = rect_context.results_QCD()['r'].unstack().T
  df.columns.name = 'Regime'
  df.index.name = 'Channel'
  return df

@memorized
def get_rEWK():
  df = pd.concat({rg: load_rewk(rg)['r'].xs('V', level=1) for rg in REGIMES_ALL})
  df.index.names = 'Regime', 'Channel'
  return df

@memorized
def result_qcd():
  r = get_rQCD()
  n = pd.DataFrame({rg:result_ssfit_raw(rg)['QCD'] for rg in REGIMES_ALL})
  return n * r

@memorized
def result_ewk():
  r = get_rEWK()
  n = pd.concat({rg:result_ssfit_raw(rg)['EWK'] for rg in REGIMES_ALL})
  n.index.names = 'Regime', 'Channel'
  return (r*n).unstack().T

#===============================================================================
# DRAWER
#===============================================================================

def draw_ssfit_unbiased(regime, tt):
  ## Skip failed fit
  tag = regime+'_'+tt
  dat = result_ssfit_raw(regime).loc[tt]
  if not dat['xbin']:
    logger.info('Skip failed fit: %s'%tag)
    return

  ## Fetch channel-dependent data
  trees  = get_trees_ssfit(regime, tt)
  nmz    = dat[['NSS', 'QCD', 'EWK']].tolist()
  xbin   = dat['xbin']
  chi2df = dat['chi2pdof']
  colors = 0, 28, 6 # _, QCD, V+Jet
  xlab12 = ROOTLABELS[tt]

  ## Ready, fire
  packages.samesign_utils().draw_ssfit_unbiased(tag, trees, nmz, xbin, chi2df, colors, *xlab12)

def draw_ssfit_unbiased_all():
  """
  Loop draw the unbiased plot of the SS-fit. It's slightly different than the
  histo return after the end of TFractionFitter. Issued by Ztau referees.
  """
  for rg in REGIMES:
    for tt in TTYPES:
      draw_ssfit_unbiased(rg, tt)

#===============================================================================
# LATEX
#===============================================================================

SSFIT_FIELDS = {
    'NSS'     : '\\nss{}',
    'chi2pdof': 'Fit \\chisqndf',
    'QCD'     : '\\nss{QCD}',
    'EWK'     : '\\nss{\\Vj}',
    'Ztau'    : '\\nss{\\ztautau}',
    'rQCD'    : '\\rQCD',
    'rEWK'    : '\\rVj',
}

def latex():
  ## Collect inputs
  cols = ['NSS', 'chi2pdof', 'QCD', 'EWK']
  df   = pd.concat({rg:result_ssfit_raw(rg) for rg in REGIMES})[cols]
  df.index.names = 'Regime', 'Channel'
  df['rQCD'] = get_rQCD()[list(REGIMES)].T.stack()
  df['rEWK'] = get_rEWK()[list(REGIMES)]

  ## sanitize output. Kill QCD in low-stat approximation
  df['QCD'] = df.apply(lambda se: '---' if pd.np.isnan(se.chi2pdof) else se.QCD, axis=1)
  df['chi2pdof'] = df['chi2pdof'].fmt3f.fillna('---')

  ## finally
  df = df.rename(columns=SSFIT_FIELDS)
  return X_to_latex(df).to_bicol_latex()

#===============================================================================

if __name__ == '__main__':
  ## Defaulted recalculation
  if '--recalc' in sys.argv:
    break_samesign_all()
    print parse_spec_ssfit()
    compare_ewk_mc_ssfit_all()
    sys.exit()

  ## Apply LHCb style
  ROOT.gROOT.batch = True
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## DEV
  # print get_trees_ssfit('e')

  ## Required preparation
  # print break_samesign_all()
  # print parse_spec_ssfit()
  # compare_ewk_mc_ssfit_all()

  ## Re-run & check single break
  # rg, tt = 'lowmass', 'e'
  # # debug_draw(rg, tt)
  # print break_samesign(rg, tt)
  # packages.samesign_utils().print_ssfit_stats(__file__, only_dt=rg+'_'+tt) #, show_all=True) #,
  # print load_rewk(rg)
  # print compare_ewk_mc_ssfit(rg).fmt1f

  ## Draw ss-unbiased
  # draw_ssfit_unbiased('tight', 'e')
  draw_ssfit_unbiased_all()

  ## Results
  # print result_ssfit_raw('nominal')
  # print result_qcd().fmt2f
  # print result_ewk().fmt2f
  # print latex()
