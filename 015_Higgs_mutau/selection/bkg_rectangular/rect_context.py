#!/usr/bin/env python
"""

For those relying on the context & normalizer

"""

from PyrootCK import *

## Get local & HMT tools
import rect_utils
from HMT_utils import (
  pd, deco, Idx, TTYPES, REGIMES, REGIMES_ALL, TT_TO_LATEX, packages,
  memorized, pickle_dataframe_nondynamic, 
)

#===============================================================================
# MAKE CONTEXT
# As a function of trees, selection context, ttypes, mH
#===============================================================================

@deco.concurrent
def _make_context(tt, selection, regime):
  """
  Loop over different imported specs & aliases here.

  ?: Do I need to manually reset alias, or it's overwritten by default?
  """
  ## Context from MC trees
  _, trees_os, trees_ss = rect_utils.load_presel_trees_with_aliases_groupped(tt, regime)
  acc = {}
  acc['OS'] = pd.DataFrame({s:t.count_ent_evt(selection) for s,t in trees_os.iteritems()}, index=['ent', 'evt'])
  acc['SS'] = pd.DataFrame({s:t.count_ent_evt(selection) for s,t in trees_ss.iteritems()}, index=['ent', 'evt'])
  df = pd.concat(acc).T.rename({'real': 'DATA'}).fillna(0).applymap(int)
  logger.info('Finished [%s] %s'%(tt, selection))
  return df

@deco.synchronized
def _make_context_sync(regime, tt):
  """
  Simple synchronous intermediate stage.
  """
  ## Queue of context to be made
  queues = {
    # name              selection alias
    'selection'      : 'Selection',  # implicit == Sel_truechan
    'selnoiso'       : 'Sel_noiso',
    'ddqcd'          : 'Sel_antiiso',
    'presel_truechan': 'Presel_truechan', # for Ztautau esel
    'sel_anychan'    : 'Sel_anychan',     # for Ztautau ZXC
  }
  ## Collect the calls
  acc = {}
  for name, selection in queues.iteritems():
    acc[name] = _make_context(tt, selection, regime)
  return pd.concat(acc)

@pickle_dataframe_nondynamic
def make_context(regime, tt):
  """
  Prepare context for single-mass point.
  Actual pickle one, not compat with syncronized directly
  Fixed the legacy structure.
  """
  return _make_context_sync(regime, tt) 

@pickle_dataframe_nondynamic # to reduce overhead
def make_context_all():
  """
  Loop over regimes & channels here.
  Make it simple here, leave the compat to the client.
  """
  acc = {}
  for regime in REGIMES_ALL:
    for tt in TTYPES:
      acc[(regime, tt)] = make_context(regime, tt)
  ## finalize
  df = pd.concat(acc)
  df.index.names = 'regime', 'tt', 'cuts', 'process'
  df.columns.names = 'sign', 'amount'
  return df.stack().stack()

#===============================================================================
# LOCALIZED NORMALIZER
#===============================================================================

@memorized
def get_default_context(regime):
  """
  To be used with normalizer. This needs to be sanitized for compatibility.
  """
  ## Santize the structure for compatibility
  df = make_context_all().unstack('tt')
  df = df.rename({'DATA': 'real'})
  df = df.xs('evt', level='amount').xs(regime, level='regime')

  ## group by cuts, sign
  level = ['cuts', 'sign']
  acc   = {}
  for keys, df2 in df.groupby(level=level):
    key = keys[0]+'_'+keys[1].lower()
    df2 = df2.xs(keys, level=level)
    df2 = df2.fillna(0).applymap(int)
    acc[key] = df2
  return acc

@memorized
def cache_table(tablename, regime):
  """
  Underlying method for consulting same table inside fixed local context.
  """
  return packages.normalizer().calc_single_array(tablename, get_default_context(regime))

@memorized
def get_norms(process, regime, tablename='selection_os'):
  """
  Return the expected number of candidates for 4 channel of requested process.
  It uses the current context.
  """
  arr = cache_table(tablename, regime).loc[process].apply(lambda args:args[2])
  return pd.Series({tt:arr[tt].to_ufloat() for tt in TTYPES})

#===============================================================================
# PUBLIC METHODS
#===============================================================================

@memorized
def results_data_OS_SS():
  """
  Return number of OS/SS candidates from data under default selection.
  """
  return make_context_all()[Idx[:,:,'selection','DATA','evt']].unstack()

@memorized
def results_QCD():
  """
  For rect_samesign, also calculate rQCD.
  """
  df = make_context_all()[Idx[:,:,'selection','qcd','evt']].unstack()
  df['r'] = df['OS'].apply(var)/df['SS'].apply(var)
  return df

#===============================================================================

def foo():
  # print context
  context = get_default_context('lowmass')
  print packages.normalizer().calc_single_array_expected('selection', 'os', context)
  print packages.normalizer().calc_single_array_expected('selection', 'ss', context)
  # print normalizer.calc_single_array_expected('selection', 'ss', context)
  # # print normalizer.calc_single_array_expected('mc_selnoiso' , 'os', context)
  # # print normalizer.calc_single_array_expected('mc_selnoiso' , 'ss', context)
  # print context['check_data_nos_nss']

#===============================================================================
# EXPORT
#===============================================================================

def preproc(df):
  """
  preprocessor to make normalizer table localized to HMT
  """
  ## Remove unnecessary rows
  for idx in 'hardqcd', 'ztaujet':
    df = df.drop(idx)
  ## Rename the header
  df = df.rename(columns=TT_TO_LATEX)
  ## finally
  return df

def export_normalizer_tables():
  ## Flatten the regimes
  context = {}
  for rg in REGIMES:
    for key, val in get_default_context(rg).iteritems():
      context[rg+'_'+key] = val
  ## Grab all tags (without os/ss), then export all
  tags = set(s.replace('_os','').replace('_ss','') for s in context)
  func = packages.normalizer().latex_table_osss
  for tag in tags:
    func(tag, context, preproc)

#===============================================================================

if __name__ == '__main__':
  ## Defaulted recalculation
  if '--recalc' in sys.argv:
    make_context_all()
    sys.exit()
  if '--latex' in sys.argv:
    export_normalizer_tables()
    sys.exit()

  ## Make
  # print _make_context_sync('nominal', 'h1')
  # print _make_context('e', 'Selection', 'nominal').get()
  # print make_context('nominal')
  # print make_context_all()
  # print make_context_all().loc[('lowmass','h1')].xs('SS', level='sign').unstack().to_string()
  # print make_context_all().loc[('nominal','h1')].xs('selection', level='cuts').unstack().to_string() # check ent!=evt

  ## With normalizer
  # print get_default_context('nominal')  # compat structure with normalizer
  # print get_norms('WW_lx')
  # foo()
  # export_normalizer_tables()

  ## Load
  # print results_data_OS_SS()
  # print results_QCD()
