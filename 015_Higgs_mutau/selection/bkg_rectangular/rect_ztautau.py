#!/usr/bin/env python
"""

Provide the estimation of Ztautau background for the HMT signal,
(i.e., with HMT cuts) used in the computation in bkg_rectangular.py

Be very careful on the dependencies & caches.

"""

## Base tools, careful of being overridden
from PyrootCK import *
from PyrootCK.mathutils import EffU

import rect_selected
import rect_context
from HMT_utils import (
  pd, deco, packages, TTYPES, TT_TO_LATEX, REGIMES, REGIMES_ALL, X_to_latex,
  memorized, pickle_dataframe_nondynamic, inject_midrule, Idx,
)

#===============================================================================
# RECONSTRUCTION EFF
#===============================================================================

def _runner(tt):
  rec = packages.rec()
  func = getattr(rec, tt+'mu_single') # crux
  return lambda t: rec.postprocess(func(rec.preprocess(t)), fast=True)

## Turn on fast mode (discard syst), but add approx error at the end instead.
REC_RUNNERS = {
  'e' : lambda t: _runner('e' )(t) * ufloat(1, 0.035),
  'h1': lambda t: _runner('h1')(t) * ufloat(1, 0.035),
  'h3': lambda t: _runner('h3')(t) * ufloat(1, 0.060),
  'mu': lambda t: _runner('mu')(t) * ufloat(1, 0.030),
}

@pickle_dataframe_nondynamic
def erec_raw(regime, tt):
  """
  Compute the full erec for given ttype, regime
  """
  tree = rect_selected.load_trees_dict(tt, regime)['t_ztautau']
  return REC_RUNNERS[tt](tree)

@pickle_dataframe_nondynamic
def erec():
  """
  Reparse to compat format with others.
  """
  acc = {}
  for rg in REGIMES_ALL:
    for tt in TTYPES:
      acc[(rg,tt)] = erec_raw(rg, tt)
  df = pd.concat(acc)
  return df.xs('erec', level=2).unstack().T

#===============================================================================

@memorized
def fetch(regime):
  df = rect_context.make_context_all().xs(regime, level='regime')
  return df.xs('OS', level='sign').xs('evt', level='amount').unstack('tt')

@memorized
def esel(regime):
  """
  Use the uncorrected version, not so much changes.
  """
  div = lambda se: EffU(se.presel_truechan, se.selection)
  # print fetch(regime).xs('ztautau0', level=1).apply(div)
  # print fetch(regime).xs('ztautau', level=1).apply(div)
  return fetch(regime).xs('ztautau', level='process').apply(div)

@memorized
def purity(regime):
  """
  Purity, defined as number of di-tau candidates from true channel over those 
  from all channels
  """
  div = lambda se: EffU(se.sel_anychan, se.selection)
  return fetch(regime).xs('ztautau0_nofidacc', level='process').apply(div)

@memorized
def results_raw(regime):
  """
  nsig = lumi * csc * br * eacc * erec * esel
  """
  df = packages.bkg_ztautau().components()
  df['esel']       = esel(regime)
  df['purity_inv'] = 1./purity(regime)
  df['erec']       = erec()[regime]  # replace the approx version
  expected         = df.prod(axis=1) # before other aux columns
  df['purity']     = purity(regime)
  df['expected']   = expected # to be last column
  return df

@memorized
def results_raw_all():
  df = pd.concat({regime:results_raw(regime) for regime in REGIMES_ALL})
  df.index.names = 'Regime', 'Channel'
  return df

def results():
  """
  Return only final expected number
  """
  return results_raw_all()['expected'].unstack().T

#===============================================================================

## Slightly more explicit to describe Ztautau in HMT
LABELS = {
  'br'    : '\\BRzt',
  'eacc'  : '\\eacczt',
  'erec'  : '\\ereczt',
  'esel'  : '\\eselzt',
  'purity': '\\purityzt',
}

def latex():
  """
  Report all components of Ztautau background, in percent.
  """
  cols = ['br','eacc','erec','esel','purity']
  df   = results_raw_all().loc[list(REGIMES), cols]
  df   = df.rename(columns=LABELS)
  return X_to_latex(df*100.).to_bicol_latex(decimals=[2,2,2,2,2])

#===============================================================================

if __name__ == '__main__':
  ## Defaulted recalculation
  if '--recalc' in sys.argv:
    print erec()
    sys.exit()

  ## DEV
  # print fetch('nominal')
  # print fetch_presel_sel('h3')
  # print erec().fmt2p
  # print esel('nominal').fmt2p
  # print purity().fmt2p
  # print results_raw('lowmass').fmt3f
  # print results().fmt2f
  print latex()
