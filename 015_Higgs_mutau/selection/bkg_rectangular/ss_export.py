#!/usr/bin/env python

import rect_utils # tethering
from HMT_utils import packages, REGIMES, TTYPES
from rect_samesign import PATH_SAMESIGN, parse_spec_ssfit

#===============================================================================

if __name__ == '__main__':
  ## Handle method to copy the figures to Desktop.
  spec = parse_spec_ssfit()[[rg+'_'+tt for rg in REGIMES for tt in TTYPES]]
  packages.samesign_utils().export_pdf(spec, PATH_SAMESIGN)
