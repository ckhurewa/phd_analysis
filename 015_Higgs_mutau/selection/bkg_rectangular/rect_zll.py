#!/usr/bin/env python
"""

Handle the estimation of Z->ll backgrounds.

"""

from PyrootCK import *
import rect_selected
from HMT_utils import (
  pd, Idx, REGIMES_ALL, REGIMES, DT_TO_TT, 
  X_to_latex, memorized, pickle_dataframe_nondynamic,
)
from uncertainties import correlated_values_norm

#===============================================================================
# TRUE ZMUMU
#===============================================================================

@memorized
def calc_zmumu(regime):
  ## Grab the tree, already with default selection applied.
  trees  = rect_selected.load_trees_dict('mu', regime)
  t_real = trees['t_real']
  t_dymu = trees['t_dymu']

  ## Cand in peak region region [80,100] in MC
  # The scale factor is simply DD/MC
  cut = utils.join('mass > 80', 'mass < 100')
  n1  = var(t_real.GetEntries(cut))
  n2  = var(t_dymu.GetEntries(cut))
  scale = n1 / n2

  ## Apply the scale factor to MC sample of Zmumu, at Selection of entire range
  return pd.Series({
    'peak'     : '[80, 100]',
    'data_peak': n1,
    'data_full': t_real.entries, # cross-check
    'mc_peak'  : n2,
    'mc_full'  : t_dymu.entries,
    'exp_full' : t_dymu.entries * scale,
  })

@memorized
def calc_zmumu_all():
  se = pd.concat({rg:calc_zmumu(rg) for rg in REGIMES_ALL})
  se.index.names = 'regime', 'field'
  return se

#-------------------------------------------------------------------------------

MWINS = {
  # 'mwin6'  : '(M>88e3) & (M<94e3)',
  'mwin10' : '(M>86e3) & (M<96e3)',
  'mwin20' : '(M>80e3) & (M<100e3)',
  # 'mwin60' : '(M>60e3) & (M<120e3)',
  'default': '(M>20e3) & (M<200e3)',
}
IP = {
  # 'default'     : '((IP1>0) & (IP1<0.05) & (IP2>0.05))|((IP2>0) & (IP2<0.05) & (IP1>0.05))',
  'default'      : '(IP1>0) & (IP1<0.05) & (IP2>0.05)',
  'prompts'      : '(IP1>0) & (IP1<0.05) & (IP2>0) & (IP2<0.05)',
  'tightprompts2': '(IP1>0) & (IP1<0.02) & (IP2>0) & (IP2<0.02)',
  # 'tightprompts1': '(IP1>0) & (IP1<0.01) & (IP2>0) & (IP2<0.01)',
}
APTS = {
  'default': '(_APT)',
  # 'flip'   : {
  #   'lowmass': '(mu1_PT>20e3) & (APT < 0.3)',
  #   'nominal': '(mu1_PT>30e3) & (APT < 0.3)',
  #   'tight'  : '(mu1_PT>50e3) & (APT < 0.4)',
  # }
}

@pickle_dataframe_nondynamic
def raw_zmumu_via_prompts():
  """
  Alternative method to normalize true Zmumu using double-prompts sideband.
  More workaround, but theoretically gain 2 advantages:
  - No need for assumption of zero-signal, more fit friendly
  - Less systematics.
  """
  import rect_utils
  cut0 = 'Preselection & _Iso & _DPHI'
  acc  = {}
  for rg in REGIMES_ALL:
    trees = rect_utils.load_presel_trees_with_aliases('mu', rg)
    for key in ['t_real', 't_dymu', 't_higgs_125']:
      tree = trees[key]
      get  = lambda *cuts: tree.count_ent_evt(utils.join(cut0, cuts))[1]
      for name1, cut1 in MWINS.iteritems():
        for name2, cut2 in IP.iteritems():
          for name3, cut3 in APTS.iteritems():
            if name3=='flip':
              cut3 = cut3.get(rg, cut3[rg.replace('old','')])
            acc[(rg, key, name1, name2, name3)] = get(cut1, cut2, cut3)
  ## Finally
  se = pd.Series(acc)
  se.index.names = 'regime', 'tree', 'win', 'ip', 'apt'
  return se


@memorized
def calc_zmumu_via_prompts():
  """
  Assumption: the choice of normalization window & IP cut depends on the regime.
  Especially the tight regime, where a narrow window & tighter IP cut is needed
  """
  specs = {
    'lowmass'   : ('mwin20', 'prompts'      , 'default'),
    'oldlowmass': ('mwin20', 'prompts'      , 'default'),
    'nominal'   : ('mwin20', 'prompts'      , 'default'),
    'oldnominal': ('mwin20', 'prompts'      , 'default'),
    'tight'     : ('mwin10', 'tightprompts2', 'default'),
    'oldtight'  : ('mwin10', 'tightprompts2', 'default'),
  }

  ## Raw results
  df = raw_zmumu_via_prompts()
  # print df

  ## Direct method [80,100]
  # print df.loc[Idx[:,'t_real','mwin20','default']]
  
  ## Indirect via prompts, with uncertainty
  def calc(se):
    nmc_full = nmc0[se.name[0]]
    nmc_nmz  = se.t_dymu
    nos_nmz  = var(se.t_real)
    # apply fullcorr between MC numbers
    nmc_full, nmc_nmz = correlated_values_norm([
      [nmc_full,nmc_full**0.5],
      [nmc_nmz ,nmc_nmz**0.5 ],
    ], [[1, 1], [1, 1]])
    # print nmc_full, mnc_nmz, nos_nmz
    return nos_nmz * nmc_full / nmc_nmz

  ## Calculate result from all normalization choices.
  df2  = df.unstack('tree')
  nmc0 = df.loc[Idx[:,'t_dymu','default','default','default']]
  idx1 = df2.index.get_level_values('win').difference(['default'])
  idx2 = df2.index.get_level_values('ip').difference(['default'])
  df2  = df2.loc[Idx[:,idx1,idx2],:]
  res  = df2.apply(calc, axis=1).unstack('regime')
  # print res 

  ## Filter chosen spec
  return pd.Series({key:res.loc[val,key] for key, val in specs.iteritems()})


#===============================================================================
# MISID LEPTON
#===============================================================================

SPEC_ZLL = [
  ( 'h1mu', 'h1', 't_dimuon_misid'    , '\\zmumu'),
  ( 'mue' , 'e' , 't_dimuon_misid'    , '\\zmumu'),
  ( 'emu' , 'e' , 't_dielectron_misid', '\\zee'  ),
]

@memorized
def results_misid(regime):
  ## Prefer this specific order
  ## With homogenized tree regime
  df = pd.DataFrame()
  for key, tt, tname_os, bkg in SPEC_ZLL:
    tname_ss = tname_os.replace('t_', 'tss_')
    trees    = rect_selected.load_trees_dict(tt, regime)
    df_OS    = trees[tname_os].dataframe()
    df_SS    = trees[tname_ss].dataframe()
    misid_OS = ufloat(df_OS['weight'].sum(), df_OS['weight_err'].sum())
    misid_SS = ufloat(df_SS['weight'].sum(), df_SS['weight_err'].sum())
    size     = df_OS.shape[0]
    rate     = misid_OS / size  # has only misid error, no stat error
    exp_OS   = misid_OS * ufloat(1, var(df_OS.shape[0]).rerr)
    exp_SS   = misid_SS * ufloat(1, var(df_SS.shape[0]).rerr)
    exp      = exp_OS - exp_SS
    df[key]  = [tt, bkg, rate, size, exp, exp_OS, exp_SS]
  ## Package it
  df.index = ['tt', 'bkg', 'rate', 'size', 'expected', 'nos', 'nss']
  df.columns.name = 'channel'
  return df.T

@pickle_dataframe_nondynamic
def results_misid_all():
  """Pickle for latex"""
  return pd.concat({rg:results_misid(rg) for rg in REGIMES_ALL})

#===============================================================================

@memorized
def results():
  """
  Summarized result ready for integration.
  """
  acc = pd.DataFrame()
  res_mu = calc_zmumu_via_prompts()
  # res_mu = calc_zmumu_all().xs('exp_full', level='field')
  for regime in REGIMES_ALL:
    res_misid = results_misid_all().loc[regime]['expected']
    acc[regime] = pd.Series({
      'mu': res_mu[regime],
      'h1': res_misid['h1mu'],
      'h3': ufloat(0., 0.),
      'e' : res_misid['emu'] + res_misid['mue'],
    })
  return acc

#===============================================================================
# LATEX
#===============================================================================

COLUMNS = {
  'bkg'      : 'Background',
  'rate'     : 'Mean mis-id $\\times 10^4$',
  'size'     : 'Selected fake',
  'impure'   : 'SS/OS [\%]',
  'expected' : 'Expected \\zll',
}

def latex_misid():
  ## fetch data
  df = results_misid_all().loc[list(REGIMES)].rename(DT_TO_TT)
  df.index.names = 'Regime', 'Channel'
  # df.insert(4, 'impure', df['nss']/df['nos'])
  df = df.drop(['tt', 'nos', 'nss'], axis=1)
  ## Format
  df['rate']     = df['rate'] * 1e4
  # df['impure']   = df['impure'] * 100. # not enough width
  
  ## To markdown
  df['rate']     = df['rate'].fmt2l
  df['expected'] = df['expected'].fmt2l
  msg = X_to_latex(df.rename(columns=COLUMNS)).to_markdown(stralign='right')

  ## Bicol latex; doesn't look good when column header is too wide
  # msg = X_to_latex(df.rename(columns=COLUMNS)).to_bicol_latex()
  # msg = msg.replace('S[table-format=1.2]@', '>{\\hspace{20pt}}S[table-format=1.2]@') # force align
  return msg

def latex_zmumu():
  """
  Compare the results from 2 methods.
  """
  df = pd.DataFrame({
    '1indirect': calc_zmumu_via_prompts(),
    '2direct'  : calc_zmumu_all().xs('exp_full', level='field'),
  }).loc[list(REGIMES)]
  df = df.rename(columns={
    '1indirect': 'Via double-prompt control region',
    '2direct'  : 'Direct in signal selection',
  })
  df = X_to_latex(df).T
  df.columns.name = 'Methods'
  print df
  return df.to_bicol_latex().replace(' &   ', ' & \\quad')

#===============================================================================

if __name__ == '__main__':
  ## True Zmumu
  # print calc_zmumu('lowmass')
  # print calc_zmumu_all()
  # print raw_zmumu_via_prompts()
  # print calc_zmumu_via_prompts().fmt1f
  print latex_zmumu()

  ## Misid Zmumu
  # print results_misid('lowmass')
  # print results_misid('nominal')
  # print results_misid('tight')
  # print results_misid_all()
  # print results().fmt2f
  # print latex_misid()
