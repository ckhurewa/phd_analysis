"""
Offline selection (after preselection) for HMT
"""
from PyrootCK import logger, utils
logger.info('Loading cuts: %s'%__file__)
from HMT_utils import CHANNELS, PREFIXES, TT_TO_DTYPE

#===============================================================================

## Another layer of selection after kinematic, but before di-tau level.
CUTS_TAUH3_SELECTION = [
  'tau_VCHI2PDOF < 9',
  # 'tau_DRoPT     < 0.01',  # nominal = 0.005 # removed
]

#===============================================================================

CUTS_ISOLATION = {
  'e_mu' : [ 'ISO1 > 0.9', 'ISO2 > 0.8' ],
  'mu_mu': [ 'ISO1 > 0.9', 'ISO2 > 0.8' ],
  'h1_mu': [ 'ISO1 > 0.9', 'ISO2 > 0.8', 'cc_vPT1<2e3', 'cc_vPT2<2e3' ],
  'h3_mu': [ 'ISO1 > 0.9', 'ISO2 > 0.8' ],
}

CUT_ANTIISOLATION = utils.join(
  '{0}_0.50_cc_IT   < 0.6',
  '{1}_0.50_cc_IT   < 0.6',
  '{0}_0.50_cc_vPT  > 10E3',
  '{1}_0.50_cc_vPT  > 10E3',
).format

CUTS_ANTIISOLATION = { TT_TO_DTYPE[tt]:CUT_ANTIISOLATION(*p) for tt,p in PREFIXES.iteritems() if tt in TT_TO_DTYPE }

#===============================================================================

CUTS_IMPPAR_NORMAL = {
  'emu': [
    'IP1 > 0',
    'IP1 < 0.05', # ~ 10 ** -1.3
    'IP2 > 0.01',
  ],
  'h1mu': [
    'IP1 > 0',
    'IP1 < 0.05',
    'IP2 > 0.01',
  ],
  'h3mu': [
    'IP1 > 0',
    'IP1 < 0.05',
    'tau_BPVLTIME*1e6 > 30',
    'tau_BPVCORRM     < 3000',
  ],
  'mumu': [
    'IP1 > 0',
    'IP1 < 0.05',
    'IP2 > 0.05',
  ],
}

#===============================================================================

CUTS_DITAU_DPHI = {dt:'DPHI > 2.7' for dt in CHANNELS} # obligatory for this data source.
CUTS_UPPER_MASS = {dt:'1==1' for dt in CHANNELS}
CUTS_DITAU_APT  = {
  'emu' : '(PT1 > 20) & (APT < 0.6) & (DPT > 0)', # kill Ztautau, maxAPT for EWK asym
  'h1mu': '(PT1 > 20) & (APT < 0.4)',
  'h3mu': '(PT1 > 20)',
  'mumu': '(PT1 > 20) & (APT > 0.3)', # kill Zmumu
}
