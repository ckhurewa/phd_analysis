"""
Offline selection (after preselection) for HMT
"""
from PyrootCK import logger, utils
logger.info('Loading cuts: %s'%__file__)
from HMT_utils import CHANNELS, PREFIXES, TT_TO_DTYPE

#===============================================================================

## Another layer of selection after kinematic, but before di-tau level.
CUTS_TAUH3_SELECTION = [
  'tau_VCHI2PDOF < 9',
  'tau_DRoPT     < 0.01', # GeV-1
]

#===============================================================================

CUTS_ISOLATION = {
  'e_mu' : [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
  'mu_mu': [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
  'h1_mu': [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
  'h3_mu': [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
}

CUT_ANTIISOLATION = utils.join(
  '{0}_0.50_cc_IT   < 0.6',
  '{1}_0.50_cc_IT   < 0.6',
  '{0}_0.50_cc_vPT  > 10E3',
  '{1}_0.50_cc_vPT  > 10E3',
).format

CUTS_ANTIISOLATION = { TT_TO_DTYPE[tt]:CUT_ANTIISOLATION(*p) for tt,p in PREFIXES.iteritems() if tt in TT_TO_DTYPE }

#===============================================================================

CUTS_IMPPAR_NORMAL = {
  'emu': [
    'IP1 > 0',
    'IP1 < 0.03',
    'IP2 > 0.01',
  ],
  'h1mu': [
    'IP1 > 0',
    'IP1 < 0.03',
    'IP2 > 0.03',
  ],
  'h3mu': [
    'IP1 > 0',
    'IP1 < 0.03',
    'tau_BPVLTIME*1e6 > 30',
    'tau_BPVCORRM     < 3000',
  ],
  'mumu': [
    'IP1 > 0',
    'IP1 < 0.03',
    'IP2 > 0.05',
  ],
}

#===============================================================================

CUTS_DITAU_DPHI = {dt:'DPHI > 2.7' for dt in CHANNELS} # obligatory for this data source.
CUTS_UPPER_MASS = {dt:'1==1' for dt in CHANNELS}
CUTS_DITAU_APT  = {
  'emu' : '(PT1 > 40) & (PT2 > 20) & (DPT > 0)', # kill hard-e
  'h1mu': '(PT1 > 40) & (PT2 > 20)',
  'h3mu': '(PT1 > 40) & (PT2 > 20)',
  'mumu': '(PT1 > 50) & (APT > 0.4)', # kill Zmumu
}
