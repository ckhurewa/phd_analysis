#!/bin/bash

rsync -avz -e "ssh -o StrictHostKeyChecking=no" --progress \
  --include=*/ \
  --include='*/*.df' \
  --include='rect_selected/*.root' \
  --exclude=* \
  lpheb:~/analysis/015_Higgs_mutau/selection/bkg_rectangular/ .
  
  # --include='rect_samesign/ss/*.pdf' \ # don't. I'll draw it manually with LHCb style.
