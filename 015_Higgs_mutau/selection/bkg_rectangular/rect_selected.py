#!/usr/bin/env python
"""

Need different name to avoid the conflict with 013's selected, which is also 
has a data-dependency.

"""

import sys
import rect_utils
import subprocess
from glob import glob
from HMT_utils import (
  pd, imp, os, ROOT, deco, packages,
  TTYPES, REGIMES, REGIMES_ALL, MASSES, PROCESS_ROOTLABELS, 
  EffU, logger, memorized, pickle_dataframe_nondynamic
)

#===============================================================================

# @memorized
# def pkg_ztau_selected():
#   ## CAREFUL, it also add path not visible from sys.path
#   return imp.load_source('selected', os.path.expandvars('$DIR13/identification/plot/selected.py'))

#===============================================================================
# EXPORT SELECTED CANDIDATES
#===============================================================================

@deco.concurrent
def main_export_single(tt, regime):
  """
  Use the same exporter (+pre/post-processor) as Ztautau.
  Export all presel trees (with additional deriaives, like noiso).
  """
  trees  = rect_utils.load_presel_trees_with_aliases(tt, regime)
  target = 'rect_selected/staging/%s_%s.root'%(tt, regime)
  if os.path.exists(target):
    logger.info('Target existed, skip. %s'%target)
  else:
    packages.ztau_selected().export_selected_trees(tt, trees, target)


@deco.synchronized
def _main_export_all():
  for rg in REGIMES_ALL:
    target = 'rect_selected/%s.root'%rg
    ## skip if already existed
    if os.path.exists(target):
      logger.info('Target existed, skip. %s'%target)
      continue
    ## Do again otherwise 
    for tt in TTYPES:
      main_export_single(tt, rg)


def main_export_all():
  # To do hadd after deco
  _main_export_all()
  for rg in REGIMES_ALL:
    target = 'rect_selected/%s.root'%rg
    if not os.path.exists(target):
      args = ['hadd', '-f', target] + glob('rect_selected/staging/*_%s.root'%rg)
      subprocess.call(args)


#===============================================================================

def prep_stats():
  """
  Simply print the cached tree entries
  """
  acc = pd.DataFrame()
  fin = ROOT.TFile(_selected_default)
  for key in fin.keys:
    tt   = key.name
    tdir = fin.Get(tt)
    for key2 in tdir.keys:
      tname = key2.name
      tree  = tdir.Get(tname)
      acc.loc[tname, tt] = tree.entries
  return acc.sort_index() 


#===============================================================================
# LOADER
#===============================================================================

@memorized
def load_trees_dict(tt, regime):
  """
  Shortcut method to load selected trees.
  Load all trees in a dict form.
  """
  src   = '$DIR15/selection/bkg_rectangular/rect_selected/%s.root'%regime
  trees = packages.ztau_selected().load_selected_trees_dict(tt, src)
  
  ## additional t_wzjet tree for use, derived form selected tree
  # in tight regime, some tree are no longer available
  tw0 = 'wmu17jet'
  tz0 = 'zmu17jet'
  for prefix in ['t_', 'tss_']:
    for suffix in ['', '_noiso']:
      ## line up the tree names
      twz = prefix + 'wzjet' + suffix
      tw  = prefix + tw0     + suffix
      tz  = prefix + tz0     + suffix
      ## replace the opposite-sign Zjet
      if prefix=='t_' and tt=='mu': 
        tz = 'tss_'+tz0+suffix

      ## skip if not ready
      if tw not in trees:
        logger.warning('Skip missing tree [%s, %s]; %s'%(regime, tt, tw))
        continue
      if tz not in trees:
        logger.warning('Skip missing tree [%s, %s]; %s'%(regime, tt, tz))
        continue

      ## make if
      tree = trees[twz] = ROOT.TChain(twz, twz)
      tree.Add(trees[tw])
      tree.Add(trees[tz])

  ## finally
  return trees


def deck_trees_all(tt):
  """
  Return the deck for the combination, i.e., mapping of process nickname & treename
  """
  ## common (channel-independent mapping)
  common = {
    'OS'   : 't_real',
    'Ztau' : 't_ztautau',
    'QCD'  : 'tdd_qcd',
    'VV'   : 't_WW_lx',
    'Other': 't_ttbar_41900010',
    'ttbar': 't_ttbar_41900010',
    'ZXC'  : 'tss_ztautau0',
  }

  ## Full fledge deck of sorted trees
  queues = {
    'mu': [
      ('OS'   , common['OS']   ),
      ('Ztau' , common['Ztau'] ),
      ('Zll'  , 't_dymu'       ),
      ('QCD'  , common['QCD']  ),
      ('EWK'  , 't_wmumujet'   ),
      ('VV'   , common['VV']   ),
      ('ttbar', common['ttbar']),
    ],
    'h1': [
      ('OS'   , common['OS']    ),
      ('Ztau' , common['Ztau']  ),
      ('Zll'  , 't_dimuon_misid'),
      ('QCD'  , common['QCD']   ),
      # ('EWK'  , 't_wzjet'       ),
      ('EWK'  , 't_wzjet_noiso' ), # smoother shape
      ('VV'   , common['VV']    ),
      ('ttbar', common['ttbar'] ),
    ],
    'h3': [
      ('OS'   , common['OS']   ),
      ('Ztau' , common['Ztau'] ),
      ('Zll'  , 't_dymu'       ),
      ('QCD'  , common['QCD']  ),
      ('EWK'  , 't_wzjet'      ),
      ('VV'   , common['VV']   ),
      ('ttbar', common['ttbar']),
    ],
    'e': [
      ('OS'   , common['OS']        ),
      ('Ztau' , common['Ztau']      ),
      # ('Zmu'  , 't_dimuon_misid'    ),
      # ('Ze'   , 't_dielectron_misid'),
      ('Zll'  , 't_dielectron_misid'), # single repr
      ('QCD'  , common['QCD']       ),
      ('EWK'  , 't_wzjet'           ), # need correct shape for RooFit
      # ('EWK'  , 't_wmu17jet'  ), # need correct shape for RooFit
      # ('EWK'  , 't_wmu17jet_noiso'  ), # need good shape for RooFit
      ('VV'   , common['VV']   ),
      ('ttbar', common['ttbar']),
    ],
  }[tt]
  return queues


def deck_trees_comb(tt):
  """
  Use above method, but collapse tt+VV together as "other", more suitable for 
  the plotting (but not fitting).
  """
  deck = deck_trees_all(tt)[:-2]  # remove VV, ttbar
  deck.append(('Other', 't_ttbar_41900010'))
  return deck


@memorized
def load_trees_comb(tt, regime):
  """
  For plot_signal. Return ordered-list. 
  The results will be re-ordered to match this order on this list.
  The tree title is also changed to root-compatible string.
  """
  trees  = load_trees_dict(tt, regime)
  queues = deck_trees_comb(tt)
  acc    = []
  for processname, tname in queues:
    t = trees[tname].Clone(processname)
    t.ownership = False
    t.title = PROCESS_ROOTLABELS[processname]
    # attach alias again, after clone 
    packages.id_utils().apply_alias_simple(tt, t)
    acc.append(t)
  return acc


def load_trees_higgs_dict(tt, regime):
  """
  Semi-automatic loading Higgs trees,
  The opposite-sign only.

  Users
  - load_tree_dict_workspace
  - efficiencies/rec_detail
  """
  acc   = {}
  trees = load_trees_dict(tt, regime)
  for tname, tree in trees.iteritems():
    if tname.startswith('t_higgs_'):
      proc = '_'.join(tname.split('_')[1:])
      acc[proc] = tree 
  return acc    

#-------------------------------------------------------------------------------

def load_tree_dict_workspace(tt, regime):
  """
  Return the dict of trees ready to be iterated and fed into upperlim-fit Workspace.
  - Take only relevant ones: opposite-sign, higgs
  - Process alias is already in place (e.g., t_real --> OS)
  - The dict-key is unique, and ready to be used to name RooDataset, etc.
  """
  ## opposite-sign higgs
  acc = {}
  for tname, tree in load_trees_higgs_dict(tt, regime).iteritems():
    if any(s in tname for s in ['_25', '_35']): #kill super-low mass
      continue
    if tree.entries <= 1: # kill bad tree
      continue
    acc[tname] = tree

  ## othrs in the deck
  trees = load_trees_dict(tt, regime)
  for proc, tname in deck_trees_comb(tt):
    acc[proc] = trees[tname]

  # Override in case of tight-mu-QCD. The number is usually zero, so I need 
  # arbitary PDF with enough stats and similar shape
  if regime == 'tight':
    if tt == 'mu':
      acc['QCD'] = trees['tssdd_qcd'] # slightly more stats
    # if tt in ('h1', 'h3'):
    #   acc['EWK'] = trees['t_wmu17jet_noiso']

  ## Disable, even though theoretically valid, strange bug appeared...
  ## Postscale the Zll tree which slows down the KeysPdf ctr.
  if tt in ('e', 'h1') and regime!='tight':
    tree = acc['Zll']
    tree.Draw('mass', 'Entry$ % 10 == 0', 'goff') # 10% postscale
    tree2 = tree.sliced_tree()
    logger.info('Prescaled Zll %s %s: %i --> %i'%(regime, tt, tree.entries, tree2.entries))
    acc['Zll'] = tree2

  ## finally
  return acc


#===============================================================================
# DRAWER
#===============================================================================

def draw_debug_mass_single(tt, regime):
  pass


#===============================================================================
# FRACTION
#===============================================================================

def fraction_mass_window_single(tt, regime):
  """
  Approximate version. 
  """
  tnames = {
    'Ztau'  : 't_ztautau',
    'Zll'   : 't_dymu',
    'QCD'   : 'tdd_qcd',
    'EWK'   : 't_wmu17jet',
    'VV'    : 't_WZ_lx',
    'ttbar' : 't_ttbar_49000010',
    'Zbb'   : 't_zbb',
    'OS'    : 't_real',
  }
  logger.info('Calc: [%s] %s'%(regime, tt))
  trees = load_trees_dict(tt, regime)
  acc0  = {}
  for proc, tname in tnames.iteritems():
    if tname not in trees: # null tree
      acc0[proc] = pd.Series({m:1. for m in MASSES})
      continue
    ## start calc
    acc  = pd.Series()
    tree = trees[tname]
    tree.Draw('mass: weight', '', 'para goff')
    df   = tree.sliced_dataframe()
    deno = int(df.weight.sum())
    ## Loop over different upper mass
    for mass in MASSES:
      nume = df[df.mass <= mass].weight.sum()
      acc.loc[int(mass)] = EffU(deno, int(nume))
    ## add also misc
    acc['size']    = len(df.index)
    acc['maxmass'] = max(df.mass)
    acc0[proc]     = acc 
  ## finally
  return pd.concat(acc0)


# @pickle_dataframe_nondynamic
def fraction_mass_window_all():
  """
  Over all regimes and ttype
  """
  acc = {}
  for regime in REGIMES:
    for tt in TTYPES:
      acc[(regime, tt)] = fraction_mass_window_single(tt, regime).unstack()
  return pd.concat(acc)


#===============================================================================

if __name__ == '__main__':
  ## Defaulted recalculation
  if '--recalc' in sys.argv:
    main_export_all()
    sys.exit()

  ## Cache the tree, once the selection cut is stable
  # mainly for draw_combined, computing reconstruction eff.
  # main_export_single('e')
  # main_export_all()

  ## Fraction
  # print fraction_mass_window_all('h1', 'nominal').unstack().T.to_string()
  # print fraction_mass_window_all()

  ## Dev
  for tt in TTYPES:
    print load_trees_dict(tt, 'tight')
  # for tname, tree in load_tree_dict_workspace('h1', 'nominal').iteritems():
  #   print tname, tree.entries
