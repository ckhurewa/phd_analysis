#!/usr/bin/env python

from PyrootCK import *
from rect_utils import load_presel_trees_with_aliases

tt = 'h1'

t_real, _, trees_os, _ = load_presel_trees_with_aliases(tt)

# t4 = trees_os['wmu17jet']
t4 = trees_os['dymu']
t4.SetAlias('Selection', 'Sel_noiso')

trees = t_real, trees_os['ztautau'], trees_os['tdd_qcd'], t4 #, trees_os['higgs_125']
# trees = t_real, trees_os['dymu'], trees_os['ztautau0'], trees_os['tdd_qcd']

# print trees_os['dymu'].count_ent_evt('Selection')

h = QHist()
h.filters   = 'Selection'
h.trees     = trees
h.params    = 'M/1e3'
h.xmin      = 0
h.xmax      = 200
h.xbin      = 40
# h.normalize = 296., 156., 113.61, 13.72 # h3
# h.normalize = 395, 199, 87.15, 88.
# h.normalize = 471, 269-40, 213-20, 41+10
h.normalize = 436, 69, 19, 219
h.st_code   = 2
h.draw()

exit()
