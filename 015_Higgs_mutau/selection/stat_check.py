#!/usr/bin/env python

"""

Give the overview stats of HMT samples after DCTW

"""

from PyrootCK import *
import pandas as pd
pd.options.display.width = 320 

#===============================================================================

def report_DCTW():
  """
  Number of entry (before Preselection) inside DCTW
  """
  fpath = '/Users/khurewat/Documents/lphe_offline/gangadir_large/5152/tuple.root'
  fin = ROOT.TFile(fpath)

  df = pd.DataFrame()
  root = 'DitauCandTupleWriter'
  for key in fin.Get(root).keys:
    for key2 in fin.Get(root+'/'+key.name).keys:
      t = fin.Get('/'.join([root, key.name, key2.name]))
      # print key.name, key2.name, t.entries
      df.loc[key.name, key2.name] = int(t.entries)
  print df
  fin.Close()

#===============================================================================

def report_Selected():
  sys.path.append('..')
  from HMT_utils import TTYPES, load_trees_higgs

  df = pd.DataFrame()
  for tt in TTYPES:
    for tree in load_trees_higgs(tt):
      mass = int(tree.name.split('_')[-1])
      df.loc[mass,tt] = int(tree.entries)
  print df.sort_index()

#===============================================================================

def report_preselected():
  """
  Report number of preselected entries. Apply also truechan & collapse-multcand
  """
  pass

#===============================================================================

if __name__ == '__main__':
  pass

  # report_DCTW()
  report_Selected()
