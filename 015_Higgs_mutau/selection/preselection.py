#!/usr/bin/env python

## FIRST Load the module containing cuts.
# This should pick up the HMT local one
import ditau_cuts

## Base tools
from collections import defaultdict, OrderedDict
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import packages, memorized, get_current_dtype_dt_tt
from glob import glob

## Default destination
_target = os.path.expandvars('$DIR15/selection/preselection/{}.root').format

#===============================================================================
# UTILS
#===============================================================================

BLACKLIST = [
  'BCID',
  'BCType',
  '*TCK',
  'GpsTime',
  'Polarity',
  'Stripping*',
  #
  'MATCH',
  '*_M',
  '*_PERR2',
  '*_PHI',
  '*_PROBNNghost',
  '*_TR*',
  '*_Y',
  '*_maxPt_*',
  '*0.50_IT',
  # '*0.50_nc*', # request for lowmass qcd study
  '*0.50_cc_mult',
  '*0.50_cc_sPT',
  '*Frac*',
  '*MC*',
  '*PP*',
  # '*Q', # need for dalitzplot, tauh3
  '*Recon*',
  '*TOS*',
  '*Y0',
]

def postpc_branch_mute(tname, tree):
  """
  Mute branch after passing the filter. Aim for better performance.
  The post-processor is to be used in side TFile.export_trees
  """
  ## Close all branches listed in the blacklist
  for s in BLACKLIST:
    tree.SetBranchStatus(s, 0)
  ## skip this if it's signal
  if not any(s in tname for s in ('higgs', 'ztautau')):
    tree.SetBranchStatus('*type*', 0)
  ## Finally
  return tree

#===============================================================================

def export_trees_higgs(tt):
  """
  Export queue of signal Higgs trees (all masses, OS+SS)
  """
  import HMT_prep
  ROOT.TFile.export_trees(
    trees  = HMT_prep.ALLTREES,
    filt   = 'Presel_anychan & _Ntrim',
    target = _target(tt+'_higgs'),
    postpc = postpc_branch_mute,
  )

#-------------------------------------------------------------------------------

def export_trees_leptonmisid(dt, tt):
  """
  Return queue of misidentified di-lepton trees.
  Need manipulation to get the camoflaged trees
  """
  ## Skip if not needed
  if tt not in ('h1', 'e'):
    logger.info('No need for lepton-misid trees in tt=%s, skip.'%tt)
    return

  ## Modify path on temporary basis
  sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
  import ditau_prep
  import lepton_misid
  logger.info('Imported ditau_prep, lepton_misid')

  ## Prepare reweighter under HMT preselection.
  misid_rates = lepton_misid.rates()
  func_rwgh   = packages.id_utils().reweighting_misid_zll
  reweighter  = lambda t, src: func_rwgh(t, dt, misid_rates[src], 'Preselection')

  ## Fetch the dilepton tree, return the camoflaged preselection
  trees = {}
  if dt == 'h1mu':
    trees = {
      't_dimuon_misid'  : reweighter(ditau_prep.t_dimuon,   'muon_as_hadron'),
      'tss_dimuon_misid': reweighter(ditau_prep.tss_dimuon, 'muon_as_hadron'),
    }
  if dt == 'emu':
    trees = {
      't_dimuon_misid'      : reweighter(ditau_prep.t_dimuon      , 'muon_as_elec'),
      'tss_dimuon_misid'    : reweighter(ditau_prep.tss_dimuon    , 'muon_as_elec'),
      't_dielectron_misid'  : reweighter(ditau_prep.t_dielectron  , 'elec_as_muon'),
      'tss_dielectron_misid': reweighter(ditau_prep.tss_dielectron, 'elec_as_muon'),
    }

  ## Finally, export
  ROOT.TFile.export_trees(
    trees  = trees,
    target = _target(tt+'_leptonmisid'),
    postpc = postpc_branch_mute,
  )

#-------------------------------------------------------------------------------

def export_trees_data_mc013(dt, tt):
  """
  Export all trees found in 013/ditau_prep modules, 
  containing both MC and data (batch 5204-5217).

  NOTE: BATCH 5312-5313 is broken.
  Data this batch is huge...
  Large batch of ntuple from data, 50GB.
  Operate on single file at a time, no need to rush

  Revert to use batch 5204-5217. 
  Smaller, biased on VCHI2/DPHI, not immediately suitable for HMT.MVA.
  """

  ## Modify path on temporary basis
  sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
  import selected

  ## remove the noiso trees, no difference here yet.
  trees = selected._export_queues(dt)
  for tname in list(trees.keys()):
    if tname.endswith('noiso'):
      del trees[tname]

  ## Ready, export
  ROOT.TFile.export_trees(
    trees  = trees,
    filt   = 'Presel_anychan & _Ntrim', # for Ztautau esel & ZXC
    target = _target(tt+'_data_mc'),
    postpc = postpc_branch_mute,
  )

#===============================================================================
# LOADER
#===============================================================================

@memorized
def load_trees_dict(tt):
  """
  Collect all trees, suitable for exporting selection.
  Not all alias will be attached yet (compat for mass-dependent cuts),
  only the simple (cut-independent) aliases are available
  Can have memory-leakage, as all files are left open. Use with care!.

  Use `ROOT.gROOT.CloseFiles()` when applicable.
  """
  aliaser = lambda t: packages.id_utils().apply_alias_simple(tt, t)
  path    = os.path.expandvars('$DIR15/selection/preselection/{}.root').format
  tags    = 'data_mc', 'higgs', 'leptonmisid'
  acc     = {}
  for tag in tags:
    fpath = path(tt+'_'+tag)
    if os.path.exists(fpath): # skip some non-existent misid
      fin = ROOT.TFile(fpath)
      fin.ownership = False
      for key in fin.keys:
        kname = key.name
        if kname.split('_')[0] in ('t', 'tdd', 'tss', 'tssdd'): # exclude temp
          ## Tree
          # tree = fin.Get(kname)
          # tree = key.ReadObj()
          # tree.name = kname
          ## Chain
          obj  = key.ReadObj()
          tree = ROOT.TChain(obj.name, obj.title)
          tree.Add(fpath+'/'+kname)
          ## finalize
          tree.ownership = False
          tree.estimate = tree.entries+1
          acc[kname] = aliaser(tree)
  ## Return the ordereddict
  trees = OrderedDict()
  for key, val in sorted(acc.iteritems()):
    trees[key] = val
  return trees

@memorized
def load_trees_src_keys(tt):
  """
  Like above, but return only the (path, list of tnames) to be used for 
  multiprocessing, avoid opening any file leading to memleak.
  """
  path = os.path.expandvars('$DIR15/selection/preselection/{}.root').format
  tags = 'data_mc', 'higgs', 'leptonmisid'
  acc  = defaultdict(list)
  for tag in tags:
    fpath = path(tt+'_'+tag)
    if os.path.exists(fpath): # skip some non-existent misid
      fin = ROOT.TFile(fpath)
      for key in fin.keys:
        acc[fpath].append(key.name)
      fin.Close()
  return acc

#===============================================================================

if __name__ == '__main__':
  pass
  
  ## Current dtype under study
  _, dt, tt = get_current_dtype_dt_tt()

  ## Exporters: preselected tree for both rectangular & MVA methods.
  # export_trees_higgs(tt)
  # export_trees_leptonmisid(dt, tt)
  export_trees_data_mc013(dt, tt)  # slowest

  ## Loaders
  # print load_trees_dict('h1')
  # print load_trees_src_keys('h1')
