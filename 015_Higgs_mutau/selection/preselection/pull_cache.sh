#!/bin/bash

## Pull roots files from remote to local
rsync -aiuvz -e "ssh -o StrictHostKeyChecking=no" \
  --progress \
  --include="*.root" \
  lpheb:~/analysis/015_Higgs_mutau/selection/preselection/ .
