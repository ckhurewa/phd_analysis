#!/usr/bin/env python

## Handle to preselected trees
import preselection


#===============================================================================
# PRELIM CHECK
#===============================================================================

def prelim3():
  tree0 = preselection.load_trees_dict('h1')['tssdd_qcd']
  print tree0

  # ## lowmass
  # filt = utils.join([
  #   'mu_0.50_cc_IT   < 0.6',
  #   'pi_0.50_cc_IT   < 0.6',
  #   'mu_0.50_cc_vPT  > 10E3',
  #   'pi_0.50_cc_vPT  > 10E3',
  #   'mu_BPVIP > 0',
  #   'mu_BPVIP < 0.05',
  #   'pi_BPVIP > 0.01',
  #   'DPHI > 2.7',
  #   'APT < 0.4',
  # ])

  ## nominal
  filt = utils.join([
    'mu_0.50_cc_IT   < 0.6',
    'pi_0.50_cc_IT   < 0.6',
    'mu_0.50_cc_vPT  > 10E3',
    'pi_0.50_cc_vPT  > 10E3',
    'mu_BPVIP > 0',
    'mu_BPVIP < 0.05',
    'pi_BPVIP > 0.01',
    'DPHI > 2.7',
    'APT < 0.5',
    'mu_PT > 30e3',
  ])
  t1 = tree0.drop(filt)
  print t1.entries
  t2 = tree0.drop(filt, by=['mu_PT', 'pi_PT'], ascending=[False, False])
  print t2.entries

  h = QHist()
  h.trees = t1, t2
  h.params = '(mu_PT-pi_PT)/1e3'
  h.xmin = -80
  h.xmax = +120
  h.xbin = 100
  h.draw()
  exit()

def prelim2():
  """
  Study the shape of data-driven QCD at different anti-iso cuts
  """
  trees0 = preselection.load_trees_dict('h1')

  tree = trees0['tssdd_qcd']
  tree.SetAlias('IT1' , 'mu_0.50_cc_IT' )
  tree.SetAlias('IT2' , 'pi_0.50_cc_IT' )
  tree.SetAlias('vPT1', 'mu_0.50_cc_vPT')
  tree.SetAlias('vPT2', 'pi_0.50_cc_vPT')

  h = QHist()
  h.filters = [ # current lowmass
    'mu_BPVIP > 0',
    'mu_BPVIP < 0.05',
    'pi_BPVIP > 0.01',
    'mu_PT > 20e3',
    'APT < 0.4',
  ]
  h.trees  = tree
  h.params = '(mu_PT-pi_PT)/1e3'
  h.cuts   = [
    '(IT1<0.7) & (IT2<0.7) & (vPT1>10e3) & (vPT2>10e3)', # current antiiso spec
    '(IT1<0.6) & (IT2<0.6) & (vPT1>10e3) & (vPT2>10e3)', # ztautau
    '(IT1<0.4) & (IT2<0.4) & (vPT1>20e3) & (vPT2>20e3)',
  ]
  h.xmin = -80
  h.xmax = +120
  h.xbin = 100
  h.draw()

  exit()

def prelim():
  ## Load background trees
  # sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
  # from ditau_prep import t_real, tss_real, t_ztautau, t_wmu17jet

  raise NotImplementedError('I can use preselected tree directly, instead of raw trees.')

  # QHist.filters = 'Preselection'
  # QHist.filters = 'Preselection'
  # QHist.filters = 'Preselection', '_Prongs', '_Iso', 'DPHI > 2.7'
  # QHist.filters = 'Selection'
  # QHist.trees = t_real, t_higgs_125, tss_real, t_ztautau
  # QHist.trees = t_real, t_higgs_125, t_wmu17jet

  h = QHist()
  h.params = '(mu_PT - pi_PT)/1e3'
  h.xmin   = -200
  h.xmax   = 200
  h.xbin   = 50
  # h.draw()

  h = QHist()
  h.params  = 'mu_BPVIP'
  h.xlog    = True
  h.xmin    = 1e-4
  h.xmax    = 1e2
  h.xbin    = 50
  # h.draw()

  h = QHist()
  h.params  = 'pi_BPVIP'
  h.xlog    = True
  h.xmin    = 1e-2
  h.xmax    = 1e0
  h.xbin    = 50
  # h.draw()

  h = QHist()
  h.params  = 'M/1e3'
  h.xmin    = 0
  h.xmax    = 300
  h.xbin    = 60
  # h.draw()

  h = QHist()
  h.params = 'APT'
  h.xmin   = 0.
  h.xmax   = 1.
  h.xbin   = 20
  # h.draw()

  h = QHist()
  h.params = 'DOCACHI2'
  h.xlog   = True
  # h.draw() 

  h = QHist()
  h.params = 'DPHI'
  # h.draw()

  QHist.reset()
  exit()

#===============================================================================

if __name__ == '__main__':
  # prelim()
  # prelim2()
  prelim3()
