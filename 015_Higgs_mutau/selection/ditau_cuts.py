#!/usr/bin/env python

"""

Responsible for applying the preselection cut to prepare the intermediate Trees,
used by both rectangular & MVA study.

Note: Do not change the name, in order to be able to use `ditau_prep` with 
this sections.

Note: These cuts are better (modern), but I cannot use it in order to use
consistent eff with Ztautau.

# 'PRER1           < 0.1', # relative momentum error
# 'PRER2           < 0.1',
# 'mu1_TRGHP       < 0.4', # Track ghost prob
# 'mu2_TRGHP       < 0.4',
# 'mu1_PROBNNghost < 0.4', # PID-NN ghost-hypo prob
# 'mu2_PROBNNghost < 0.4',

Note: No stripping as later Higgs tree doesn't go through stripping.
{ 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' },

"""

from PyrootCK import logger, utils
logger.info('Loading cuts: %s'%__file__)

## Don't forget the cut in DitauCandTupleWriter.filt_ditau on REAL DATA
# tauh3: PT>12, 0.7<M<1.5, BPVM>3000, VCHI2PDOF<20
# DPHI > 2.7
# PT > 15,5/10/12

#===============================================================================

cuts_nobadrun1 = {'runNumber < 111802', '111890 < runNumber'} # excluded in Zee 8TeV
cuts_nobadrun2 = {'runNumber < 126124', '126160 < runNumber'} # excluded in Zee 8TeV, Zmumu 8TeV
cuts_nobadrun3 = {'runNumber < 129530', '129539 < runNumber'} # excluded in ?

## All the same in all channels
CUTS_PRESEL_NOBADRUN = (
  'nSPDhits<=600', 
  cuts_nobadrun1,
  cuts_nobadrun2, 
  cuts_nobadrun3,
)

#===============================================================================
#
# Note: Use utils.join for joining cuts
# TUPLE for AND
# SET   for OR
#
CUTS_PRESELECTION = {
  'mu_mu': [
    {
      ( 'mu1_PT > 20E3', 'mu2_PT >  5E3' ),
      ( 'mu1_PT >  5E3', 'mu2_PT > 20E3' ),
    },
    { 
      ( 'mu1_TOS_MUON', 'mu1_PT > 20e3' ),
      ( 'mu2_TOS_MUON', 'mu2_PT > 20e3' ),
    },
    #
    'mu1_PT      > mu2_PT', # to ensure there's no double-counting
    'mu1_ETA     > 2.0',
    'mu2_ETA     > 2.0',
    'mu1_ETA     < 4.5',
    'mu2_ETA     < 4.5',
    'mu1_TRPCHI2 > 0.01', # not good with MC
    'mu2_TRPCHI2 > 0.01',
    #
    'M > 20e3', # kill combinatorics
  ],
  'e_mu': [
    {
      ( 'e_PT  > 20E3', 'mu_PT >  5E3' ),
      ( 'e_PT  >  5E3', 'mu_PT > 20E3' ),
    },
    { 
      ( 'e_TOS_ELECTRON', 'e_PT  > 20E3' ),
      ( 'mu_TOS_MUON'   , 'mu_PT > 20E3' ),
    },
    'e_ETA      > 2.0',
    'e_ETA      < 4.5',
    'e_TRPCHI2  > 0.01',
    'e_PP_InAccEcal',
    'e_PP_InAccHcal',
    #
    'mu_ETA     > 2.0',
    'mu_ETA     < 4.5',
    'mu_TRPCHI2 > 0.01',
    #
    'M > 20e3', # kill combinatorics
  ],
  'h1_mu': [
    'mu_TOS_MUON',
    'mu_PT      > 20E3',
    'mu_ETA     > 2.0',
    'mu_ETA     < 4.5',
    'mu_TRPCHI2 > 0.01',
    #
    'pi_PT      > 10E3',
    'pi_ETA     > 2.00',
    'pi_ETA     < 4.50',
    'pi_TRPCHI2 > 0.01',
    'pi_PP_InAccHcal',
    #
    'M > 30e3', # kill combinatorics
  ],
  'h3_mu': [
    'mu_TOS_MUON',
    'mu_PT      > 20E3',
    'mu_ETA     > 2.0',
    'mu_ETA     < 4.5',
    'mu_TRPCHI2 > 0.01',
    #
    'pr1_ETA    > 2.00',
    'pr2_ETA    > 2.00',
    'pr3_ETA    > 2.00',
    'pr1_ETA    < 4.50',
    'pr2_ETA    < 4.50',
    'pr3_ETA    < 4.50',
    'pr1_TRPCHI2  > 0.01',
    'pr2_TRPCHI2  > 0.01',
    'pr3_TRPCHI2  > 0.01',
    #
    'tau_PTTRIOMIN  > 1E3',
    'tau_PTTRIOMAX  > 6E3',
    'tau_PT         > 12E3',
    'tau_M          > 700',
    'tau_M          < 1500',
    'tau_VCHI2PDOF  < 20',
    'pr1_PP_InAccHcal',
    'pr2_PP_InAccHcal',
    'pr3_PP_InAccHcal',
    #
    #
    'M > 30e3', # kill combinatorics
  ],
}

#===============================================================================

## Nulls, use in separate stages
# Provide here for compat with id_utils.apply_alias
from collections import defaultdict
CUTS_TAUH3_SELECTION = []
CUTS_ISOLATION       = defaultdict(str)
CUTS_ANTIISOLATION   = defaultdict(str)
CUTS_MIXISOLATION    = defaultdict(str)
CUTS_IMPPAR_NORMAL   = defaultdict(str)
CUTS_IMPPAR_PROMPT   = []
CUTS_DITAU_APT       = defaultdict(str)
CUTS_DITAU_DPHI      = defaultdict(str)
CUTS_UPPER_MASS      = defaultdict(str)

#===============================================================================
