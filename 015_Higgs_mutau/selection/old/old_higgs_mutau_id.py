#!/usr/bin/env python

import sys
from PyrootCK import *

ROOT.gROOT.ProcessLine('.L lhcbStyle.C')

dtype = 'h1mu'


# >>> Need tauh3 cuts!! I forgot to put them in NTUPLE!

#
# Note: Use QHistUtils.join for joining cuts
# TUPLE for AND
# SET   for OR
#
CUTS_PRESELECTION = {
  'e_e': [
    'StrippingZ02TauTau_EXLineDecision',
    'e1_TOS_ELECTRON',
    'e1_PT  > 20000',
    'e2_PT  >  5000',
    'M      > 20000',
    #
    'e1_BPVIP   > 0.01',
    'e1_BPVIP   < 0.2',
    'e2_BPVIP   > 0.01',
    'e2_BPVIP   < 0.2',
  ],
  'e_h1': [
    'StrippingZ02TauTau_EXLineDecision',
    'e_TOS_ELECTRON',
    'e_PT   > 17000',
    'pi_PT  > 10000',
    'M      > 26000',
    #
    'e_BPVIP    > 0.01',
    'e_BPVIP    < 0.2',
    'pi_BPVIP   > 0.01',
    'pi_BPVIP   < 0.2',
  ],
  'e_h3': [ 
    'StrippingZ02TauTau_EXLineDecision',
    'e_TOS_ELECTRON',
    'e_PT   > 15000',
    'tau_PT > 20000',
    'M      > 34640',
    #
    'e_BPVIP    > 0.01',
    'e_BPVIP    < 0.2',
    'tau_BPVVD  > 8',
    'tau_BPVVD  < 100',
  ],
  'e_mu': [
    {
      (
        'StrippingZ02TauTau_EXLineDecision',
        'e_TOS_ELECTRON',
        'e_PT  > 20000',
        'mu_PT >  5000',
        'M     > 20000',
      ),
      (
        'StrippingZ02TauTau_MuXLineDecision',
        'mu_TOS_MUON',
        'e_PT  >  5000',
        'mu_PT > 20000',
        'M     > 20000',
      )
    },
    'e_BPVIP  > 0.01',
    'e_BPVIP  < 0.2',
    'mu_BPVIP > 0.01',
    'mu_BPVIP < 0.2',    
  ],
  'h1mu': [
    'StrippingZ02TauTau_MuXLineDecision',
    'mu_TOS_MUON',
    'mu_PT > 30000',
    'pi_PT > 10000',
    'M     > 34000',
    # 'M     > 17320', # (15,5)
    # 'M     > 20000', # (20,5)
    # 'M     > 24500', # (15,10)
    # 'M     > 28280', # (20,10)
    # 'M     > 26000', # (17,10)
    #
    'mu_BPVIP   < 0.02',
    'pi_BPVIP   > 0.01',
    'pi_BPVIP   < 0.3',
  ],
  'h3_mu': [
    'StrippingZ02TauTau_MuXLineDecision',
    'mu_TOS_MUON',
    # 'mu_PT  > 15000',
    # 'tau_PT > 15000',
    # 'M      > 34640', # (15,20)
    # 'M      > 37000', # (17,20)
    # 'M      > 32000', # (17,15)
    # 'M      > 30000',
    #
    'mu_BPVIP   < 0.01',
    'tau_BPVVD  > 1',  
    'tau_BPVVD  < 100',
  ],
  'mu_mu': [
    'StrippingZ02TauTau_MuXLineDecision',
    'mu1_TOS_MUON'   ,
    'mu1_PT  > 20000',
    'mu2_PT  >  5000',
    # 'M       > 17320', # (15,5)
    # 'M       > 18440',
    'M       > 20000',
    # 'M<80000 || M>100000',
    #
    'mu1_BPVIP   > 0.01', 
    'mu1_BPVIP   < 0.2',
    'mu2_BPVIP   > 0.01',
    'mu2_BPVIP   < 0.2',
  ]
}[dtype]

## Not used in QCD sample
# CUTS_ISOLATION = [ QHistUtils.join([
#   '{0}_PTFrac05C  > 0.9',
#   '{0}_PTCone05C  < 3000',
#   '{1}_PTFrac05C  > 0.9',
#   '{1}_PTCone05C  < 3000',
# ]).format( *prefixes[dtype] ) ]


CUTS_ISOLATION = {
  'e_e': [
    'e1_PTFrac05C   > 0.9',
    'e1_PTCone05C   < 2000',
    'e2_PTFrac05C   > 0.9',
    'e2_PTCone05C   < 2000'
  ],
  'e_h1': [
    'e_PTFrac05C    > 0.9',
    'e_PTCone05C    < 1000',
    'pi_PTFrac05C   > 0.9',
    'pi_PTCone05C   < 1000'
  ],
  'e_mu': [
    'mu_PTFrac05C  > 0.9',
    'mu_PTCone05C  < 2000',
    'e_PTFrac05C   > 0.9',
    'e_PTCone05C   < 2000'
  ],
  'h1mu': [
    'mu_PTFrac05C  > 0.9',
    'mu_PTCone05C  < 2000',
    'pi_PTFrac05C  > 0.8',
    'pi_PTCone05C  < 3000'
  ],
  'h3_mu': [
    'mu_PTFrac05C   > 0.9',
    'mu_PTCone05C   < 2000',
    'tau_PTFrac05C  > 0.8',
    'tau_PTCone05C  < 3000'
  ],  
  'mu_mu': [
    'mu1_PTFrac05C  > 0.9',
    'mu1_PTCone05C  < 2000',
    'mu2_PTFrac05C  > 0.9',
    'mu2_PTCone05C  < 2000'
  ],
}[dtype]

prefixes = {
  'e_e'   : ( 'e1' , 'e2'  ),
  'e_h1'  : ( 'e'  , 'pi'  ),
  'e_h3'  : ( 'e'  , 'tau' ),
  'e_mu'  : ( 'e'  , 'mu'  ),
  'h1mu'  : ( 'pi' , 'mu'  ),
  'h3_mu' : ( 'tau', 'mu'  ),
  'mu_mu' : ( 'mu1', 'mu2' ),  
}

CUTS_ANTIISOLATION = {
  'h1mu': [
    # 'pi_PTFrac05C < 0.4',
    'pi_PTCone05C > 10000',
    'mu_PTCone05C > 10000',
  ]
}[dtype]

# CUTS_ANTIISOLATION = [ QHistUtils.join([
#   # '{0}_PTFrac05C  < 0.5',
#   '{0}_PTCone05C  > 10000',
#   # '{1}_PTFrac05C  < 0.5',
#   '{1}_PTCone05C  > 10000',
# ]).format( *prefixes[dtype] ) ]


CUTS_SELECTION = {
  'e_e': [
    'M      < 60000',
    'APT      > 0.1',
    'APT      < 0.5',
    #
    'DOCA     > 0.03',
    'DOCA     < 0.2',
    'DOCACHI2 > 4', 
    'DPHI     > 2.7',
    'MINIPS   > 3',
    'MINIPS   < 20',
  ],
  'e_h1': [
    'APT      < 0.4',  #!
    'DOCA     > 0.01',
    'DOCA     < 0.2',
    'DOCACHI2 > 4',
    'DPHI     > 2.7',
    'MINIPS   > 3',
    'MINIPS   < 20',
  ],
  'e_h3': [
    'APT      < 0.5',
    'DOCA     > 0.001',
    # 'DOCA     < 0.2',
    'DOCACHI2 > 0.1',
    'DPHI     > 2.7',
    # 'MINIPS   > 2',  # May not be too useful for h3
    # 'MINIPS   < 20',
  ],
  'e_mu': [
    'APT      < 0.5',
    'DOCA     > 0.01',
    'DOCA     < 0.2',
    'DOCACHI2 > 1',
    'DPHI     > 2.7',
    'MINIPS   > 1',
    'MINIPS   < 20',
  ],
  'h1mu': [
    'APT      < 0.6',  
    # 'DOCA     > 0.01',
    # 'DOCA     < 0.5',
    # 'DOCACHI2 > 0.5',
    'DPHI     > 2.7',
    # 'MINIPS   > 1',
    # 'MINIPS   < 20',
  ],
  'h3_mu': [  
    # 'APT      < 0.7',
    # 'DOCA     > 0.001',
    # 'DOCA     < 0.2',
    # 'DOCACHI2 > 0.1',
    # 'DPHI     > 2.7',
    # 'MINIPS > 0.5',  # May not be too useful for h3
    # 'MINIPS < 20',
  ],
  'mu_mu': [
    'APT      > 0.2', # !!
    'APT      < 0.6',
    'DOCA     > 0.01',
    'DOCA     < 0.2',
    'DOCACHI2 > 4',  # !!
    'DPHI     > 2.7',
    'MINIPS   > 4',   # !
    'MINIPS   < 20',
  ]
}[dtype]

#----------------------------------------------------------

## Batch151008
real = 3557, 3558, 3559, 3560, 3561, 3562, 3563, 3564, 3565, 3567, 3568, 3569, 3570, 3571, 3572, 3573, 3574, 3575, 3576, 3577, 3580, 3581, 3582, 3583

## transitioning to shorter dtype: h1_mu --> h1mu
dtype2 = {
  'h1mu': 'h1_mu'
}[dtype]

tname_os = 'DitauCandTupleWriter/%s'%dtype2
tname_ss = 'DitauCandTupleWriter/%s_ss'%dtype2
# tname_oa = 'DitauCandTupleWriter/%s_antiiso'%dtype
# tname_sa = 'DitauCandTupleWriter/%s_ss_antiiso'%dtype


t_real          = import_tree( tname_os, *real ).SetTitle('Real (OS)')
tss_real        = import_tree( tname_ss, *real ).SetTitle('Real (SS)')
tss_noiso_real  = import_tree( tname_ss, *real ).SetTitle('Real (SS)')

# t_real_oa   = import_tree( tname_oa, *real ).SetTitle('Real (OpSign,antiiso)')
# t_real_sa   = import_tree( tname_sa, *real ).SetTitle('Real (SameSign,antiiso)')
t_aiso_real     = import_tree( tname_os, *real ).SetTitle('QCD~RealAntiIso (OS)')
tss_aiso_real   = import_tree( tname_ss, *real ).SetTitle('QCD~RealAntiIso (SS)')


t_ztautau     = import_tree( tname_os, 3584  ).SetTitle('ztautau')
tss_ztautau   = import_tree( tname_ss, 3584  ).SetTitle('ztautau (SS)')

t_higgsmutau  = import_tree( tname_os, 3815 ).SetTitle('Higgs -> mu tau')

t_dytau       = import_tree( tname_os, 3637  ).SetTitle('DY tautau')
t_dymu        = import_tree( tname_os, 3636  ).SetTitle('DY mumu')
t_dye         = import_tree( tname_os, 3591  ).SetTitle('DY ee')

t_cc_hardmu   = import_tree( tname_os, 3417  ).SetTitle('ccbar2mux')
t_bb_hardmu   = import_tree( tname_os, 3418  ).SetTitle('bbbar2mux')
t_cc_harde    = import_tree( tname_os, 3419  ).SetTitle('ccbar2ex')
t_bb_harde    = import_tree( tname_os, 3420  ).SetTitle('bbbar2ex')

tss_cc_hardmu = import_tree( tname_ss, 3417 ).SetTitle('ccbar-mu (SS)')
tss_bb_hardmu = import_tree( tname_ss, 3418 ).SetTitle('bbbar-mu (SS)')


t_wtaujet     = import_tree( tname_os, 3585  ).SetTitle('Wtau+Jet')
t_wmu17jet    = import_tree( tname_os, 3589  ).SetTitle('Wmu17+jet')
t_wejet       = import_tree( tname_os, 3588  ).SetTitle('We+jet')
t_wmujet      = import_tree( tname_os, 3590  ).SetTitle('Wmu+jet')

tss_wtaujet   = import_tree( tname_ss, 3585 ).SetTitle('Wtau+Jet (SS)')
tss_wmu17jet  = import_tree( tname_ss, 3589 ).SetTitle('Wmu17+jet (SS)')
tss_wmujet    = import_tree( tname_ss, 3590 ).SetTitle('Wmu+jet (SS)')

t_ztaujet           = import_tree( tname_os, 3597 ).SetTitle('Ztautau+Jet')
t_zmu17jet          = import_tree( tname_os, 3598 ).SetTitle('Zmumu17+Jet')
t_noiso_zmu17jet    = import_tree( tname_os, 3598 ).SetTitle('Zmumu17+Jet (NoIso)')
tss_zmu17jet        = import_tree( tname_ss, 3598 ).SetTitle('Zmumu17+Jet (SS)')
tss_noiso_zmu17jet  = import_tree( tname_ss, 3598 ).SetTitle('Zmumu17+Jet (SS-NoIso)')

t_noiso_wmu17jet    = import_tree( tname_ss, 3589 ).SetTitle('Wmu17+jet (NoIso)')
tss_noiso_wmu17jet  = import_tree( tname_ss, 3589 ).SetTitle('Wmu17+jet (SS-NoIso)')
tss_noiso_wmujet    = import_tree( tname_os, 3590 ).SetTitle('Wmu+jet (SS-NoIso)')


## Apply alias to all trees
for tname,t in dict(locals()).iteritems():
  if isinstance( t, ROOT.TTree ):
    # 
    # print t.GetTitle(), t.GetEntries()

    ## Taylored Preselection cuts
    # cuts = list(CUTS_PRESELECTION)
    cuts = [ 'MATCH' ] + CUTS_PRESELECTION # Need MATCH for Z02TauTau signal

    ## AntiIso, NoIso, Iso
    if 'aiso' in tname: # Anti-iso
      cuts += CUTS_ANTIISOLATION
    elif 'noiso' in tname:
      pass
    else:
      cuts += CUTS_ISOLATION

    ## Stringify
    cuts = QHistUtils.join(cuts)

    ## SS/OS  -- Diference in Stripping
    ## Replace the OS-Line with SS-Line
    if tname.startswith('tss_'):
      t.SetAlias('Preselection', cuts.replace('LineDecision', 'LineSSDecision') )
    else:
      t.SetAlias('Preselection', cuts )
    #
    t.SetAlias('Selection', QHistUtils.join( CUTS_SELECTION + ['Preselection == 1'] ))

    # print tname, t.GetAlias('Preselection')

## Put all background for catalogin for now
background_def  = [ t_ztautau, tss_aiso_real , t_wtaujet  ] 
background_e    = [ t_wejet , t_dye   , t_cc_harde  , t_bb_harde ]
background_mu   = [ t_wmujet, t_dymu  , t_cc_hardmu , t_bb_hardmu, t_zmu17jet  ]

T_BACKGROUND_ALL = {
  'e_e'  : background_def + background_e,
  'e_h1' : background_def + background_e,
  'e_h3' : background_def + background_e,
  'e_mu' : background_def + background_e + background_mu,
  'h1mu': background_def + background_mu,
  'h3_mu': background_def + background_mu,
  'mu_mu': background_def + background_mu,
}[dtype]


#===============================================================================


# QHist.auto_name   = True
# QHist.trees       = [ t_higgsmutau ] + T_BACKGROUND_ALL
QHist.comment     = 'Higgs mutau prestudy. StrippingZ02TauTauMuX, muon TOS'
QHist.trees       = t_higgsmutau, t_aiso_real, t_wmu17jet, t_zmu17jet, t_ztautau
QHist.prefix      = dtype
# QHist.master_cuts = 'Preselection'
QHist.master_cuts = 'Selection'
QHist.xbin    = 30
QHist.title   = dtype
QHist.st_line = False

## Cross-check
h = QHist()
h.params = 'mu_PT/1000'
h.xlog   = True
h.xmin   = 1E1
h.xmax   = 2E2
h.xlabel = 'PT(mu) [GeV]'
h.draw()

h = QHist()
h.params = 'mu_BPVIP'
h.xlog   = True
h.xmin   = 1E-3
h.xmax   = 1E0
h.xlabel = 'IP(mu) [mm]'
# h.draw()

h = QHist()
h.params = 'mu_PTCone05C'
h.xlog   = True
h.xmin   = 1E1
h.xmax   = 2E5
# h.draw()

h = QHist()
h.params = 'mu_PTFrac05C'
h.xmax   = 0.999
h.ylog   = True
# h.draw()


h = QHist()
h.params = 'pi_PT/1000'
h.xlog   = True
h.xmin   = 4E0
h.xmax   = 2E2
h.xlabel = 'PT(tauh1) [GeV]'
# h.draw()

h = QHist()
h.params = 'pi_BPVIP'
h.xlog   = True
h.xmin   = 1E-3
h.xmax   = 1E0
h.xlabel = 'IP(tauh1) [mm]'
# h.draw()

h = QHist()
h.params = 'pi_PTFrac05C'
h.xmax   = 0.99
# h.draw()

h = QHist()
h.params = 'pi_PTCone05C'
h.xlog   = True
h.xmin   = 1E2
# h.xmax   = 2E4
# h.draw()

h = QHist()
h.params = 'tau_BPVVD'
h.xlog   = True
# h.xmin   = 1E-3
# h.xmax   = 1E0
# h.draw()

raw_input(); sys.exit()


# -----------------------------

h = QHist()
h.params = 'M/1000'
h.xmin   = 0
h.xmax   = 160
# h.xbin   = 30
h.xlabel = 'M [\gevcc]'
# h.draw()


h = QHist()
h.params = 'APT'
h.xlabel = 'A_{PT}'
# h.draw()
# QHistUtils.pad_divider( h, 2 )


h = QHist()
h.params = 'DOCA'
h.xlog   = True
# h.xmin   = 1E-4
# h.xmax   = 1E1
h.xlabel = 'DOCA [mm]'
# h.draw()


h = QHist()
h.params = 'DOCACHI2'
h.xlog   = True
# h.xmin   = 1E-1
# h.xmax   = 1E3
# h.draw()
# QHistUtils.pad_divider( h, 2 )


h = QHist()
h.params = 'DPHI'
h.xlabel = '\Delta\phi [rad]'
# h.draw()
# QHistUtils.pad_divider( h, 2 )


h = QHist()
h.params = 'MINIPS'
# h.xmin   = 1E-1
# h.xmax   = 40
h.xlog   = True
h.xlabel = 'min(\Sigma\tilde{r})'
# h.draw()

# raw_input(); sys.exit()

#==============================================================

QHist.reset()
QHist.master_cuts = [ 'Selection' ]

## Another post-selection check

# h = QHist()
# h.trees  = t_ztautau
# h.params = 'MATCH'
# h.xmin   = -1
# h.xmax   = 2
# h.xbin   = 40
# h.xlog   = True
# h.draw()

# raw_input(); sys.exit()

# --------


## Dissolving SS
h = QHist()
h.trees   = tss_real, tss_aiso_real, tss_wmu17jet, tss_zmu17jet
h.params  = 'mu_PT - pi_PT'
# h.ylog    = True
h.xmin    = -20E3
h.xmax    = 80E3
h.xbin    = 20
h.draw()

# raw_input()

res = QHistUtils.fraction_fit( h )
frac_qcd  = res[0]
frac_wmuj = res[1]
frac_zmuj = res[2]

# raw_input(); sys.exit()


## Pre-normalize

h = QHist()
h.trees = tss_aiso_real, t_aiso_real, tss_real, t_real, tss_wmu17jet, t_wmu17jet, tss_zmu17jet, t_zmu17jet, t_ztautau
h.params = 'M/1000'
h.xmin   = 30
h.xmax   = 150
h.draw()

n_samesign  = h[2].GetEntries()
ratio_qcd   = h[1].GetEntries() / h[0].GetEntries()
ratio_wmuj  = h[5].GetEntries() / h[4].GetEntries()
ratio_zmuj  = h[7].GetEntries() / h[6].GetEntries()
n_qcd       = n_samesign * frac_qcd   * ratio_qcd
n_wmuj      = n_samesign * frac_wmuj  * ratio_wmuj
n_zmuj      = n_samesign * frac_zmuj  * ratio_zmuj
n_real      = h[3].GetEntries()
nmc_ztautau = h[8].GetEntries()

print locals()

#---------------


lumi = 1.989E12  # divide due to XS below unit in [mb]

## h1_mu
nmz = [
  [ t_real    ,  n_real ],
  # [ t_ztautau ,    0  ],  # TO-BE-REPLACED
  [ t_higgsmutau,    0  ],  # TO-BE-REPLACED
  #
  [ t_aiso_real ,  n_qcd  ],  # real-SS-iso
  [ t_wmu17jet  ,  n_wmuj ],
  [ t_zmu17jet  ,  n_zmuj ],
  #
  [ t_ztautau ,  nmc_ztautau /  4046990 * 1.22E-06 * 3.67E-01 * lumi ],
  # [ t_wtaujet ,    0. /  2576506 * 3.67E-05 * 2.65E-01 * lumi ],
  # [ t_wmujet ,    . /  2039268 * 3.67E-05 * 2.65E-01 * lumi ],
  # [ t_wmu17jet,  274. / 10015768 * 3.67E-05 * 2.65E-01 * lumi ],
  # [ t_dymu    ,    7. / 20571464 * 4.57E-06 * 4.00E-01 * lumi ],
  # [ t_zmu17jet,  115. /  5073236 * 1.29E-06 * 3.29E-01 * lumi ],
  # [ t_wejet   ,    0. /  2080560 * 3.67E-05 * 2.64E-01 * lumi ],
  # [ t_dye     ,   21. /  2369246 * 4.57E-06 * 4.05E-01 * lumi ],
  # [ t_ztaujet,  506. /  3030718 * 1.32E-06 * 3.79E-01 * lumi],
]
## For signal renorm
n_signal  = nmz[0][1] - sum([ v for _,v in nmz ][2:])
nmz[1][1] = n_signal


h = QHist()
h.name      = 'massfit'
h.suffix    = dtype
h.trees     = [ t for t,_ in nmz ]
h.params    = 'M/1000'
h.xmin      = 30
h.xmax      = 150
h.xbin      = 30
h.normalize = [ v for _,v in nmz ]
h.st_code   = 2
# h.ylog      = True
h.xlabel    = 'M [\gevcc]'
h.ylabel    = 'Candidate'
h.draw()

# raw_input(); sys.exit()


#-------------
## Same-sign plot (for... fun...)

h = QHist()
h.name      = 'massfit_samesign'
h.title     = dtype
h.trees     = tss_real, tss_aiso_real, tss_wmu17jet, tss_zmu17jet
h.params    = 'M/1000'
h.xmin      = 30
h.xmax      = 150
h.xbin      = 30
h.normalize = [ n_samesign, n_samesign * frac_qcd, n_samesign * frac_wmuj, n_samesign * frac_zmuj  ]
h.st_code   = 2
# h.ylog      = True
h.xlabel    = 'M [\gevcc]'
h.ylabel    = 'Candidate'
h.draw()


# NSS = 490
# N_QCD = NSS * 0.7
# N_EWK = NSS * 0.3 * 1945./1166

# print N_EWK, N_QCD


# h = QHist()
# h.trees  = t_real, t_ztautau, t_aiso_real, t_wmu17jet
# h.params = 'M'
# h.normalize = [ 1333, 1333-N_QCD-N_EWK, N_QCD, N_EWK ]
# h.st_code   = 2
# h.xmin      = 20E3
# h.xmax      = 120E3
# h.xbin      = 30
# h.draw()

raw_input()