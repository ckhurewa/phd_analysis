# #!/usr/bin/env python

# """

# Preparing trees for plotting.

# Borrowing from Z tautau analysis.
# Changes:

# - mu is now prompt, not displaced
# - remove upper mass limit (we're scanning higgs mass)
# - 

# """

# from PyrootCK import * 

# dtype = 'mu_mu'

# ## Optionally take from sys.argv, overriding above
# if len(sys.argv) == 2:
#   dtype = sys.argv[1]

# log.info('Study dtype: %s'%dtype)

# dt = dtype.replace('_','')

# #===============================================================================

# ## Prefixes used as name of the branches
# # Choose the higher-PT one up front, muon then electron.
# prefixes = {
#   'e_e'   : ( 'e1' , 'e2'  ),
#   'e_h1'  : ( 'e'  , 'pi'  ),
#   'e_h3'  : ( 'e'  , 'tau' ),
#   'e_mu'  : ( 'mu' , 'e'   ),
#   'h1_mu' : ( 'mu' , 'pi'  ),
#   'h3_mu' : ( 'mu' , 'tau' ),
#   'mu_mu' : ( 'mu1', 'mu2' ),  
# }

# ## Labels suitable for x-axis, follow the order above
# labels = {
#   'e_e'   : ( 'e1'  , 'e2'  ),
#   'e_h1'  : ( 'e'   , 'h1'  ),
#   'e_h3'  : ( 'e'   , 'h3'  ),
#   'e_mu'  : ( '#mu' , 'e'   ),
#   'h1_mu' : ( '#mu' , 'h1'  ),
#   'h3_mu' : ( '#mu' , 'h3'  ),
#   'mu_mu' : ( '#mu1', '#mu2'),  
# }

# cuts_nobadrun1 = {'runNumber < 111802', '111890 < runNumber'} # elec too low
# cuts_nobadrun2 = {'runNumber < 126124', '126160 < runNumber'} # elec too high
# cuts_nobadrun3 = {'runNumber < 129530', '129539 < runNumber'}

# CUTS_PRESEL_NOBADRUN = {
#   'e_e'  : [ cuts_nobadrun1, cuts_nobadrun2, cuts_nobadrun3 ],
#   'e_h1' : [ cuts_nobadrun1, cuts_nobadrun2, cuts_nobadrun3 ],
#   'e_h3' : [ cuts_nobadrun1, cuts_nobadrun2, cuts_nobadrun3 ],
#   'e_mu' : [ cuts_nobadrun1, cuts_nobadrun2, cuts_nobadrun3 ],
#   'h1_mu': [ cuts_nobadrun2 ],
#   'h3_mu': [ cuts_nobadrun2 ],
#   'mu_mu': [ cuts_nobadrun2 ],
# }

# CUTS_PRESELECTION = {
#   'e_mu': [
#     { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' }, # 'StrippingWeLineDecision' },
#     { 'mu_TOS_MUON', 'e_TOS_ELECTRON' },
#     'e_PT       > 20E3',
#     'e_ETA      > 2.0',
#     'e_ETA      < 4.5',
#     'e_TRGHP    < 0.2',
#     #
#     'mu_PT      > 20E3',
#     'mu_ETA     > 2.0',
#     'mu_ETA     < 4.5',
#     'mu_TRGHP   < 0.2',
#     #
#     # 'mu_PERR2/mu_P**2 < 0.01',
#     # 'e_PERR2/e_P**2 < 0.01',
#     #
#     'M  > 40E3',
#   ],
#   'h1_mu': [
#     { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' },
#     'mu_TOS_MUON',
#     #
#     'pi_PT      > 20E3',
#     'pi_ETA     > 2.25',
#     'pi_ETA     < 3.75',
#     'pi_TRGHP   < 0.2',
#     #
#     'mu_PT      > 20E3',
#     'mu_ETA     > 2.0',
#     'mu_ETA     < 4.5',
#     'mu_TRGHP   < 0.2',
#     #
#     #
#     # 'mu_PERR2/mu_P**2 < 0.01',
#     # 'pi_PERR2/pi_P**2 < 0.01',
#     #
#     'M  >  40E3',
#     # 'M  < 120E3',
#   ],
#   'h3_mu': [
#     { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' },
#     'mu_TOS_MUON',
#     #
#     'mu_PT      > 20E3',
#     'mu_ETA     > 2.0',
#     'mu_ETA     < 4.5',
#     'mu_TRGHP   < 0.2',
#     #
#     'pr1_ETA    > 2.25',
#     'pr2_ETA    > 2.25',
#     'pr3_ETA    > 2.25',
#     'pr1_ETA    < 3.75',
#     'pr2_ETA    < 3.75',
#     'pr3_ETA    < 3.75',
#     'pr1_TRGHP  < 0.2',
#     'pr2_TRGHP  < 0.2',
#     'pr3_TRGHP  < 0.2',
#     #
#     'tau_PTTRIOMIN  > 1E3',
#     'tau_PTTRIOMAX  > 6E3',
#     'tau_PT         > 12E3',
#     'tau_M          > 700',
#     'tau_M          < 1500',
#     #
#     'M  > 30E3', 
#   ],
#   ## DV guaranteed already that mu1_PT > mu2_PT
#   'mu_mu': [
#     { 'mu1_TOS_MUON', 'mu2_TOS_MUON' },
#     { 'StrippingZ02TauTau_MuXLineDecision', 'StrippingWMuLineDecision' },
#     'mu1_PT     > 20E3',
#     'mu2_PT     > 5E3',
#     'mu1_ETA    > 2.0',
#     'mu2_ETA    > 2.0',
#     'mu1_ETA    < 4.5',
#     'mu2_ETA    < 4.5',
#     'mu1_TRGHP  < 0.2',
#     'mu2_TRGHP  < 0.2',
#     #
#     'M > 20E3',
#     # 'M < 100E3', # kill 120
#   ]
# }

# ## Another layer of selection after kinematic, but before di-tau level.
# CUTS_TAUH3_SELECTION = [
#   'tau_VCHI2PDOF < 20',
#   'tau_BPVCORRM  < 3000',
#   'tau_DRTRIOMAX < 0.2',
# ]

# CUTS_IMPPAR_NORMAL = {
#   'e_mu': [
#     ## hardmu
#     'mu_BPVIP   < 0.1',
#     ## taue
#     'e_BPVIP    > 0.01',
#     'e_BPVIP    < 1',
#   ],
#   'h1_mu': [
#     ## hardmu
#     'mu_BPVIP   < 0.1',
#     ## tauh1
#     'pi_BPVIP   > 0.02',
#     'pi_BPVIP   < 1',
#   ],
#   'h3_mu': [
#     ## hardmu
#     'mu_BPVIP   < 0.1',
#     ## tauh3
#     'tau_BPVVD  > 8',
#     'tau_BPVVD  < 100',
#   ],
#   'mu_mu': [
#     ## hardmu
#     'mu1_BPVIP  < 0.1',
#     ## taumu
#     'mu2_BPVIP  > 0.01',
#     'mu2_BPVIP  < 1',
#   ],
# }


# CUTS_ISOLATION = {
#   'mu_mu': [
#     'mu1_0.50_cc_IT   > 0.9',
#     'mu2_0.50_cc_IT   > 0.9',
#     # 'mu2_0.50_cc_IT   > 0.8',
#   ],
#   'h1_mu': [
#     # 'mu_0.50_cc_vPT   < 2000',
#     # 'pi_0.50_cc_vPT   < 2000'
#     'mu_0.50_cc_IT    > 0.9',
#     'pi_0.50_cc_IT    > 0.9',
#   ],
#   'h3_mu': [
#     # 'mu_0.50_cc_vPT   < 2000',
#     # 'tau_0.50_cc_vPT  < 3000', # says, the shower
#     'mu_0.50_cc_IT    > 0.9',
#     'tau_0.50_cc_IT   > 0.9',
#     # 'tau_0.50_cc_IT   > 0.8', # not too lose, otherwise I'll have many cand in evt
#   ],  
#   'e_mu': [
#     'e_0.50_cc_IT     > 0.9',
#     'mu_0.50_cc_IT    > 0.9',
#   ],
# }

# CUTS_MIXISOLATION = {
#   'mu_mu': [
#     'mu1_0.50_cc_IT    > 0.9',
#     'mu2_0.50_cc_IT    < 0.6',
#     'mu2_0.50_cc_vPT   > 10E3',
#   ],
#   'h1_mu': [
#     'pi_0.50_cc_IT    < 0.6',
#     'pi_0.50_cc_vPT   > 10E3',
#     'mu_0.50_cc_IT    > 0.9',
#   ],
#   'h3_mu': [
#     'mu_0.50_cc_IT    > 0.9',
#     'tau_0.50_cc_IT   < 0.6',
#     'tau_0.50_cc_vPT  > 10E3',
#   ],
#   'e_mu': [
#     'mu_0.50_cc_IT  > 0.9',
#     'e_0.50_cc_IT   < 0.6',
#     'e_0.50_cc_vPT  > 10E3',
#   ]
# }

# CUT_ANTIISOLATION = utils.join(
#   '{0}_0.50_cc_IT   < 0.6',
#   '{1}_0.50_cc_IT   < 0.6',
#   '{0}_0.50_cc_vPT  > 10E3',
#   '{1}_0.50_cc_vPT  > 10E3',
# ).format

# CUTS_ANTIISOLATION = { dtype:CUT_ANTIISOLATION(*p) for dtype,p in prefixes.iteritems() }


# CUTS_SELECTION = {
#   'e_mu': [
#     # 'APT      > 0.2',
#     # 'nPVs==1',
#     'DPHI     > 2.7',
#     'DOCACHI2 < 200', # from bender
#   ],
#   'h1_mu': [
#     # 'APT    > 0.1',
#     'DPHI     > 2.7',
#     'DOCACHI2 < 200', # from bender
#   ],
#   'h3_mu': [
#     'DPHI     > 2.7',
#     'DOCACHI2 < 200', # from bender
#   ],
#   'mu_mu': [
#     'APT      > 0.2',
#     'DPHI     > 2.7',
#     'DOCACHI2 > 10',
#     'DOCACHI2 < 200', # from bender
#   ],
# }

# ## Alias for the discriminating variable to use at same-sign dissolution.
# ALIASES_DIFF_SAMESIGN = {
#   'ee'  : 'e1_PT/1000  - e2_PT/1000',
#   'eh1' : 'e_PT/1000   - pi_PT/1000',
#   'eh3' : 'e_PT/1000   - tau_PT/1000',
#   'emu' : 'mu_PT/1000  - e_PT/1000',
#   'h1mu': 'mu_PT/1000  - pi_PT/1000',
#   'h3mu': 'mu_PT/1000  - tau_PT/1000',
#   'mumu': 'mu1_PT/1000 - mu2_PT/1000',
# }

# #===============================================================================

# ## For those from Ztautau analysis
# tname_os = 'DitauCandTupleWriter/%s'%dtype
# tname_ss = 'DitauCandTupleWriter/%s_ss'%dtype

# ## Real data, based from Ztautau
# ## Batch160314: Properly loosen tauh3, large
# real = 4294, 4295, 4296, 4298, 4299, 4300, 4301, 4302, 4303, 4304, 4305, 4306
# ## Batch160417: Filtered on high-PT to have slim size. Added rapidity
# # cuts: tauh3: pt12,m700,m1500,bpvcorrm3,dr0.2,v20
# # cuts: ditau: docachi2<200,dphi>2.7
# # cuts: ll   : pt20,5
# # cuts: lh1  : pt20,10
# # cuts: lh3  : pt20,12
# # real = 4562,

# t_real      = import_tree( tname_os, *real ).SetTitle('Real (OS)')
# tss_real    = import_tree( tname_ss, *real ).SetTitle('Real (SS)')

# ## data-driven QCD shape 
# ## TODO: Defind data-driven QCD cut in Higgs->mutau regime
# tdd_qcd     = import_tree( tname_os, *real ).SetTitle('DD ~ QCD (OS)')
# tssdd_qcd   = import_tree( tname_ss, *real ).SetTitle('DD ~ QCD (SS)')

# ## Signal: Higgs -> mu tau
# t_higgs     = import_tree( 'HiggsMuTauTuple/%s'%dtype   , 4652 ).SetTitle('Higgs 125')
# tss_higgs   = import_tree( 'HiggsMuTauTuple/%s_ss'%dtype, 4652 ).SetTitle('Higgs 125 (SS)')

# ## Z -> tautau
# ## 4333 With fiducial & acceptance cut in place.
# ## 4674 Like before, but now with truth di-tau event-info 
# t_ztautau   = import_tree( tname_os, 4674 ).SetTitle('Zg tautau') 
# tss_ztautau = import_tree( tname_ss, 4674 ).SetTitle('Zg tautau(SS)')

# ## Another version from central, may/maynot have gamma*
# # But have consistent egen suitable for estimation table.
# # TODO: check the gen in J4415 to be sure
# t_ztautau0    = import_tree( tname_os, 4417 ).SetTitle('Z tautau') 
# tss_ztautau0  = import_tree( tname_ss, 4417 ).SetTitle('Z tautau') 

# ## ttbar
# t_ttbar_41900006    = import_tree( tname_os, 4307 ).SetTitle('ttbar 41900006')
# tss_ttbar_41900006  = import_tree( tname_ss, 4307 ).SetTitle('ttbar 41900006 (SS)')
# t_ttbar_41900007    = import_tree( tname_os, 4308 ).SetTitle('ttbar 41900007')
# tss_ttbar_41900007  = import_tree( tname_ss, 4308 ).SetTitle('ttbar 41900007 (SS)')
# t_ttbar_41900010    = import_tree( tname_os, 4309 ).SetTitle('ttbar 41900010')
# tss_ttbar_41900010  = import_tree( tname_ss, 4309 ).SetTitle('ttbar 41900010 (SS)')

# ## DiBosons
# t_WW_ll       = import_tree( tname_os, 4310 ).SetTitle('WW_ll 41922002')
# tss_WW_ll     = import_tree( tname_ss, 4310 ).SetTitle('WW_ll 41922002 (SS)')
# t_WW_lx       = import_tree( tname_os, 4311 ).SetTitle('WW_lx 42021000')
# tss_WW_lx     = import_tree( tname_ss, 4311 ).SetTitle('WW_lx 42021000 (SS)')
# t_WZ_lx       = import_tree( tname_os, 4312 ).SetTitle('WZ_lx 42021001')
# tss_WZ_lx     = import_tree( tname_ss, 4312 ).SetTitle('WZ_lx 42021001 (SS)')

# ## Z
# t_ztaujet     = import_tree( tname_os, 4313 ).SetTitle('Ztautau+Jet')
# tss_ztaujet   = import_tree( tname_ss, 4313 ).SetTitle('Ztautau+Jet (SS)')
# t_dymu        = import_tree( tname_os, 4314 ).SetTitle('DY mumu (MC)')
# tss_dymu      = import_tree( tname_ss, 4314 ).SetTitle('DY mumu (SS)')
# t_zmu17jet    = import_tree( tname_os, 4315 ).SetTitle('Zmumu17+Jet')
# tss_zmu17jet  = import_tree( tname_ss, 4315 ).SetTitle('Zmumu17+Jet (SS)')
# t_zmu17cjet   = import_tree( tname_os, 4316 ).SetTitle('Zmumu17+cjet')
# tss_zmu17cjet = import_tree( tname_ss, 4316 ).SetTitle('Zmumu17+cjet (SS)')
# t_zmu17bjet   = import_tree( tname_os, 4317 ).SetTitle('Zmumu17+bjet')
# tss_zmu17bjet = import_tree( tname_ss, 4317 ).SetTitle('Zmumu17+bjet (SS)')
# t_dye40       = import_tree( tname_os, 4334 ).SetTitle('DY ee (m40)')
# tss_dye40     = import_tree( tname_ss, 4334 ).SetTitle('DY ee (m40) (SS)')
# t_dye10       = import_tree( tname_os, 4319 ).SetTitle('DY ee (m10)')
# tss_dye10     = import_tree( tname_ss, 4319 ).SetTitle('DY ee (m10) (SS)')
# t_zbb         = import_tree( tname_os, 4320 ).SetTitle('Zbb')
# tss_zbb       = import_tree( tname_ss, 4320 ).SetTitle('Zbb (SS)')


# ## W+Jet
# t_wtaujet     = import_tree( tname_os, 4321 ).SetTitle('Wtau+Jet')
# tss_wtaujet   = import_tree( tname_ss, 4321 ).SetTitle('Wtau+Jet (SS)')
# t_wmujet      = import_tree( tname_os, 4322 ).SetTitle('Wmu+jet')
# tss_wmujet    = import_tree( tname_ss, 4322 ).SetTitle('Wmu+jet (SS)')
# t_wmu17jet    = import_tree( tname_os, 4323 ).SetTitle('Wmu17+jet')
# tss_wmu17jet  = import_tree( tname_ss, 4323 ).SetTitle('Wmu17+jet (SS)')
# t_wejet       = import_tree( tname_os, 4324 ).SetTitle('We+jet')
# tss_wejet     = import_tree( tname_ss, 4324 ).SetTitle('We+jet (SS)')
# t_wtaubb      = import_tree( tname_os, 4325 ).SetTitle('Wtau+bbbar')
# tss_wtaubb    = import_tree( tname_ss, 4325 ).SetTitle('Wtau+bbbar (SS)')
# t_we          = import_tree( tname_os, 4555 ).SetTitle('We (incl)')
# tss_we        = import_tree( tname_ss, 4555 ).SetTitle('We (incl) (SS)')

# ## QCD
# t_hardqcd     = import_tree( tname_os, 4326 ).SetTitle('HardQCD')
# tss_hardqcd   = import_tree( tname_os, 4326 ).SetTitle('HardQCD (SS)')
# t_cc_hardmu   = import_tree( tname_os, 4327 ).SetTitle('ccbar-mu')
# tss_cc_hardmu = import_tree( tname_ss, 4327 ).SetTitle('ccbar-mu (SS)')
# t_bb_hardmu   = import_tree( tname_os, 4328 ).SetTitle('bbbar-mu')
# tss_bb_hardmu = import_tree( tname_ss, 4328 ).SetTitle('bbbar-mu (SS)')
# t_cc_harde    = import_tree( tname_os, 4329 ).SetTitle('ccbar-e')
# tss_cc_harde  = import_tree( tname_ss, 4329 ).SetTitle('ccbar-e (SS)')
# t_bb_harde    = import_tree( tname_os, 4330 ).SetTitle('bbbar-e')
# tss_bb_harde  = import_tree( tname_ss, 4330 ).SetTitle('bbbar-e (SS)')

# ## Custom
# t_wmumujet    = import_tree( tname_os, 4331 ).SetTitle('Wmu20+mujet')
# tss_wmumujet  = import_tree( tname_ss, 4331 ).SetTitle('Wmu20+mujet (SS)')
# t_mcmb_mux    = import_tree( tname_os, 4332 ).SetTitle('MCMB MuX')
# tss_mcmb_mux  = import_tree( tname_ss, 4332 ).SetTitle('MCMB MuX (SS)')
# t_we20jet     = import_tree( tname_os, 4533 ).SetTitle('We20+jet')
# tss_we20jet   = import_tree( tname_ss, 4533 ).SetTitle('We20+jet (SS)')


# #===============================================================================

# def apply_alias( tname, t, dtype ):
#   ## No aliases made for these channel. Do it locally
#   if dtype in ('e_e', 'e_h1', 'e_h3'):
#     return

#   ## Kinematic alias
#   t.SetAlias('E' , '(M**2 + P**2)**0.5')
#   t.SetAlias('PZ', '(P**2 - PT**2)**0.5')
#   t.SetAlias('Y' , '1/2*TMath::Log((E+PZ)/(E-PZ))')

#   ## Resolve to single dtype
#   GOODRUN   = [] # Ignore
#   # GOODRUN   = CUTS_PRESEL_NOBADRUN[dtype]
#   PRESEL    = CUTS_PRESELECTION[dtype]
#   ISO       = CUTS_ISOLATION[dtype]
#   ANTIISO   = CUTS_ANTIISOLATION[dtype]
#   MIXISO    = CUTS_MIXISOLATION[dtype]
#   DISPLACED = CUTS_IMPPAR_NORMAL[dtype]
#   # IPPROMPT  = CUTS_IMPPAR_PROMPT
#   SELECTION = CUTS_SELECTION[dtype]
#   if 'h3' in dtype:
#     SELECTION += CUTS_TAUH3_SELECTION

#   ## Early stringify
#   cuts_presel = utils.join(GOODRUN, PRESEL)

#   ## SS/OS  -- Difference in Stripping
#   ## Replace the OS-Line with SS-Line
#   if tname.startswith('tss'):
#     cuts_presel = cuts_presel.replace('XLineDecision', 'XLineSSDecision')

#   ## Establish presel alias, required MC matching by default
#   t.SetAlias('Presel_match'  , utils.join( 'MATCH', cuts_presel )) # used in ztautau
#   t.SetAlias('Presel_nomatch', cuts_presel        )
#   t.SetAlias('Preselection'  , 'Presel_nomatch'   )

#   ## Selection cuts, with different Data-Driven mixtures
#   t.SetAlias('Sel_default'      , utils.join( 'Preselection'  , ISO    , DISPLACED, SELECTION ))
#   t.SetAlias('Sel_match'        , utils.join( 'Presel_match'  , ISO    , DISPLACED, SELECTION ))
#   t.SetAlias('Sel_nomatch'      , utils.join( 'Presel_nomatch', ISO    , DISPLACED, SELECTION ))
#   t.SetAlias('Sel_dd_qcd'       , utils.join( 'Preselection'  , ANTIISO, DISPLACED, SELECTION ))
#   t.SetAlias('Sel_noiso'        , utils.join( 'Preselection'  ,          DISPLACED, SELECTION ))
#   t.SetAlias('Sel_mixiso'       , utils.join( 'Preselection'  , MIXISO , DISPLACED, SELECTION ))

#   ## THE default 'Selection' cut now differs by name
#   ## very tricky
#   if tname in ( 'tdd_qcd', 'tssdd_qcd' ):
#     t.SetAlias('Selection', 'Sel_dd_qcd' )
#   else:
#     t.SetAlias('Selection', 'Sel_default')

#   ## Alias for SS
#   t.SetAlias( 'diff_samesign', ALIASES_DIFF_SAMESIGN[dt] )


# ## Apply alias to all trees
# for tname,t in dict(locals()).iteritems():
#   if isinstance( t, ROOT.TTree ):
#     apply_alias( tname, t, dtype )

# #===============================================================================

# ## This order should be compat with latex's.

# ALLTREES_OS = [
#   t_ztautau, t_ztautau0,
#   t_dymu, t_dye10, t_dye40, t_zbb,
#   t_wtaujet, t_wmujet, t_wmu17jet, t_we, t_wejet, t_we20jet, t_wmumujet, t_wtaubb,
#   t_ztaujet, t_zmu17jet, t_zmu17cjet, t_zmu17bjet,
#   t_hardqcd, t_cc_harde, t_cc_hardmu, t_bb_harde, t_bb_hardmu, t_mcmb_mux,
#   t_ttbar_41900006, t_ttbar_41900007, t_ttbar_41900010,
#   t_WW_ll, t_WW_lx, t_WZ_lx,
# ]

# ALLTREES_SS = [
#   tss_ztautau, tss_ztautau0,
#   tss_dymu, tss_dye10, tss_dye40, tss_zbb,
#   tss_wtaujet, tss_wmujet, tss_wmu17jet, tss_we, tss_wejet, tss_we20jet, tss_wmumujet, tss_wtaubb,
#   tss_ztaujet, tss_zmu17jet, tss_zmu17cjet, tss_zmu17bjet,
#   tss_hardqcd, tss_cc_harde, tss_cc_hardmu, tss_bb_harde, tss_bb_hardmu, tss_mcmb_mux,
#   tss_ttbar_41900006, tss_ttbar_41900007, tss_ttbar_41900010,
#   tss_WW_ll, tss_WW_lx, tss_WZ_lx,
# ]

# #===============================================================================

# ## Irregular selection

# ## Dimuon under selected channel
# # always load the mu_mu tree, but the selection cut varies by channel.
# t_dimuon = import_tree( 'DitauCandTupleWriter/mu_mu', 4314 ).SetTitle('DiMuon (MC)')

# if dt in ( 'h1mu', 'h3mu', 'emu' ):
#   filt = utils.join(
#     CUTS_PRESELECTION [dtype],
#     CUTS_ISOLATION    [dtype],
#     CUTS_IMPPAR_NORMAL[dtype],
#     CUTS_SELECTION    [dtype],
#   )

#   if dtype=='h1_mu':
#     filt1 = filt.replace('mu_','mu1_').replace('pi_','mu2_')
#     filt2 = filt.replace('mu_','mu2_').replace('pi_','mu1_')
#   if dtype == 'e_mu':
#     filt1 = filt.replace('mu_','mu1_').replace('e_','mu2_')
#     filt2 = filt.replace('mu_','mu2_').replace('e_','mu1_')
#   if dtype == 'h3_mu':
#     filt1 = filt.replace('mu_','mu1_').replace('tau_','mu2_')
#     filt2 = filt.replace('mu_','mu2_').replace('tau_','mu1_')

#   filt = utils.join({ filt1, filt2 }) # do the OR
#   t_dimuon.SetAlias('Selection', filt )


# #===============================================================================

# if __name__ == '__main__':
#   from IPython import embed; 
#   embed()
#   pass


