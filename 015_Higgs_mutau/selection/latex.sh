#!/bin/bash

## Vars
set -e
PROJ=${HOME_EPFL}/Papers/007_HiggsLFV/figs
DATE=`date +%y%m%d`

## Bind the preselection gallery
rm -f gallery/*.pdf
rm -f gallery/*.root
./gallery.py --redraw
cd gallery
QHistPdfExporter.py gallery.root \
  --width=360 --height=240 \
  --legend-header='LHCb preliminary' \
  --pad-margin-bottom=0.16 \
  --pad-margin-left=0.12 \
  --xaxis-morelog \
  --xaxis-offset=1.00 \
  --yaxis-offset=0.64 \
  --legend-top=0.95 \
  --legend-left=0.65 \
  --legend-bottom=0.65 \
  --legend-text-size=12
cd ..
rm -rf gallery/$DATE               # remove existing, otherwise rename fails
mv gallery/minipdf gallery/$DATE   # rename subdir
rm -rf $PROJ/gallery               # remove remote
cp -R gallery/$DATE $PROJ/gallery  # copy to latex

echo "Finished!"
