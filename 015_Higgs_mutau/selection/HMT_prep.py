#!/usr/bin/env python
"""

Prepare the Higgs tree on top of ditau_prep.
To be used as a base of function that really need to use the Higgs tree.

"""
## FIRST Load the module containing cuts. This should pick up the HMT local one 
# Need to retain this name in order to let 013/ditau_prep pick up this same module.
import ditau_cuts

## Base tools
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15'))
import HMT_utils
id_utils = HMT_utils.packages.id_utils()

## Current dtype under study
dtype, dt, tt = HMT_utils.get_current_dtype_dt_tt()

#===============================================================================
# LOADING TREES
#===============================================================================

## Signal: Higgs -> mu tau
# 4652: Original, 'HiggsMuTauTuple/%s'
# 5065: Reused DitauCandTupleWriter, revised fid+acc, with TupleToolMCTruth
# NOTE: This batch have funny mass values. Will be supercede soon.

## Batch form same TFile
# 5152: First version
# 5236: With WEH patched
# 5300: Full mass range
JID = 5300
def load_tree(mass):
  tos = import_tree('DitauCandTupleWriter/%i/%s'%(mass, dtype)   , JID).Clone('Higgs_%i'%mass)
  tss = import_tree('DitauCandTupleWriter/%i/%s_ss'%(mass, dtype), JID).Clone('Higgs_%i_SS'%mass)
  tos.title = 'Higgs %i'%mass
  tss.title = 'Higgs %i (SS)'%mass
  return tos, tss

t_higgs_25 , tss_higgs_25  = load_tree(25)
t_higgs_35 , tss_higgs_35  = load_tree(35)
t_higgs_45 , tss_higgs_45  = load_tree(45)
t_higgs_55 , tss_higgs_55  = load_tree(55)
t_higgs_65 , tss_higgs_65  = load_tree(65)
t_higgs_75 , tss_higgs_75  = load_tree(75)
t_higgs_85 , tss_higgs_85  = load_tree(85)
t_higgs_95 , tss_higgs_95  = load_tree(95)
t_higgs_105, tss_higgs_105 = load_tree(105)
t_higgs_115, tss_higgs_115 = load_tree(115)
t_higgs_125, tss_higgs_125 = load_tree(125)
t_higgs_135, tss_higgs_135 = load_tree(135)
t_higgs_145, tss_higgs_145 = load_tree(145)
t_higgs_155, tss_higgs_155 = load_tree(155)
t_higgs_165, tss_higgs_165 = load_tree(165)
t_higgs_175, tss_higgs_175 = load_tree(175)
t_higgs_185, tss_higgs_185 = load_tree(185)
t_higgs_195, tss_higgs_195 = load_tree(195)


#===============================================================================
# GLOBAL VARS, USE WITH CARE
#===============================================================================

## Fetch all trees, apply the alias, and index them
ALLTREES, ALLTREES_OS, ALLTREES_SS = id_utils.indexing_trees_os_ss(locals())

for tname, tree in ALLTREES.iteritems():
  id_utils.apply_alias(ditau_cuts, dtype, tname, tree)
  HMT_utils.apply_alias(tt, tname, tree) # Overwriting alias, e.g., TrueChan

#===============================================================================
