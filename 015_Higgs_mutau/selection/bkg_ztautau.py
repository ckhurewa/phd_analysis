#!/usr/bin/env python

"""

Base & common values to compute expected Ztautau background.

"""

from PyrootCK import *
from PyrootCK.mathutils import EffU
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import (
  TT_TO_DTYPE, TTYPES, ZTAUTAU_CSC, Lumi, Br_ditau,
  pd, pickle_dataframe_nondynamic, reco_sieve,
)

#===============================================================================
# ACCEPTANCE
#===============================================================================

def eacc():
  """
  Use the one from Ztautau analysis, it's compatible.
  """
  ## Get 2D histogram
  JID = 5223 # different regime (mass window, normal/narrow eta )
  fin = import_file(JID)

  ## Loop over ttypes
  acc = pd.Series()
  for tt in TTYPES:
    h = fin.Get('nomasswindow_normaleta_hist_'+TT_TO_DTYPE[tt]) # hadron in normal eta
    fid_acc   = int(h.GetBinContent(2,2))
    fid_noacc = int(h.GetBinContent(2,1))
    nofid_acc = int(h.GetBinContent(1,2))
    acc[tt]   = EffU( fid_acc+fid_noacc, fid_acc+nofid_acc )
  return acc

#===============================================================================
# RECONSTRUCTION
#===============================================================================

_sieve_conf = {  
  'e' : {'lep2':'hardmu', 'lep1':'e' },
  'mu': {'lep1':'hardmu', 'lep2':'mu'},
  'h1': {'lep' :'hardmu'},
  'h3': {'lep' :'hardmu'},
}

def erec_single(tt):
  ## Get tree
  # 4677: original 
  # 5433: fix non-unique prongs in tauh3
  t = import_tree('DitauRecoStats/'+TT_TO_DTYPE[tt], 5433)
  t.make_aliases_from_prefix(**_sieve_conf[tt])
  return reco_sieve.apply(t, tt)


@pickle_dataframe_nondynamic
def erec_approximate():
  """
  The one from MC is approximately correct.
  """
  ## Obtain the globla erec & its part
  df = pd.DataFrame({tt:erec_single(tt) for tt in TTYPES}).T

  ## Replace these with data-driven correction.
  # Taken from Ztautau analysis. Valid only for Z->tautau kinematic profile
  df['egec_new'] = pd.Series({
    'mu': ufloat(0.93 , 0.0032),
    'e' : ufloat(0.923, 0.007 ), 
    'h1': ufloat(0.93 , 0.006160349010292532), 
    'h3': ufloat(0.93 , 0.00612689441351113),
  })
  df['etrig_new'] = pd.Series({
    'mu': ufloat(0.882, 0.015),
    'e' : ufloat(0.860, 0.014),
    'h1': ufloat(0.801, 0.017),
    'h3': ufloat(0.804, 0.017),
  })

  ## finaly, compute the corrected version
  f_erec = lambda se: se.erec * se.egec_new/se.egec * se.etrig_new/se.etrig
  return df.apply(f_erec, axis=1)

#===============================================================================

def components():
  """
  Return a dataframe containing these: CSC, LUMI, BR, EACC, EREC
  ready for attaching esel, purity, depends on the method using.
  """
  ## Get ditau branching ratio, under channel nomenclature of HMT
  br_ditau = Br_ditau()
  br = pd.Series({
    'mu': br_ditau['mumu'],
    'h1': br_ditau['h1mu'],
    'h3': br_ditau['h3mu'],
    'e' : br_ditau['emu'],
  })

  return pd.DataFrame({
    'csc' : ZTAUTAU_CSC,
    'lumi': Lumi(),
    'br'  : br,
    'eacc': eacc(),
    'erec': erec_approximate(),
  })

#===============================================================================

if __name__ == '__main__':
  print eacc().fmt2p
  # print erec_single('h1')
  # print erec()
  # print components()
