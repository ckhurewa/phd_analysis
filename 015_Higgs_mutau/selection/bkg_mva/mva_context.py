#!/usr/bin/env python
"""

Using the selected candidate, run through the normalizer to get the expected
number of candidates from MC.

"""

from PyrootCK import ROOT, logger, var, sys
import mva_selected # tethered
from HMT_utils import (
  pd, deco, Idx, MASSES, TTYPES, packages, EffU,
  memorized, pickle_dataframe_nondynamic, nominal_value,
)

#===============================================================================
# MAKE CONTEXT
#===============================================================================

CONTEXT_SELECTIONS = [
  'selection', 
  'ddqcd', 
  'selnoiso', 
  'presel_truechan', 
  'sel_anychan', 
  'Presel_TrueChanZtau', 
  'Sel_TrueChanZtau'
]

@deco.concurrent
def _make_context(tt, mass, cut):
  ## Load MVA trees
  trees_os, trees_ss = mva_selected.load_trees_groupped(tt, mass)
  acc = {}
  acc['OS'] = pd.DataFrame({s:t.count_ent_evt(cut) for s,t in trees_os.iteritems()}, index=['ent', 'evt'])
  acc['SS'] = pd.DataFrame({s:t.count_ent_evt(cut) for s,t in trees_ss.iteritems()}, index=['ent', 'evt'])
  df = pd.concat(acc).T.rename({'real': 'DATA'}).fillna(0).applymap(int)
  logger.info('Finished [%s] %s'%(tt, cut))
  return df


@deco.synchronized
def _make_context_sync(tt, mass):
  acc = {}
  for cut in CONTEXT_SELECTIONS:
    acc[cut] = _make_context(tt, mass, cut)
  return pd.concat(acc)


@pickle_dataframe_nondynamic
def make_context(tt, mass):
  """
  Sharded by mass here instead of part of the loop to avoid the memory-leakage
  in mva_selected.load_trees.
  """
  res = _make_context_sync(tt, mass)
  res.index.names = 'cut', 'process'
  ROOT.gROOT.CloseFiles()
  return res


# @pickle_dataframe_nondynamic # to reduce overhead
# @memorized
def make_context_all():
  ## Loop collect
  acc = {}
  for tt in TTYPES:
  # for tt in ['h1']:
    # for mass in MASSES:
    for mass in [125]:
      acc[(tt, mass)] = make_context(tt, mass)
  ## Finalize
  df = pd.concat(acc).stack().stack()
  df.index.names = 'tt', 'mass', 'cut', 'process', 'count', 'sign'
  return df

#===============================================================================
# LOCALIZED NORMALIZER
#===============================================================================

@memorized
def get_default_context(mass):
  """
  To be used with normalizer. This needs to be sanitized for compatibility.
  """
  ## Santize the structure for compatibility
  df = make_context_all().unstack('tt')
  df = df.rename({'DATA': 'real'})
  df = df.xs('evt', level='count').xs(mass, level='mass')

  ## group by cuts, sign
  level = ['cut', 'sign']
  acc   = {}
  for keys, df2 in df.groupby(level=level):
    key = keys[0]+'_'+keys[1].lower()
    df2 = df2.xs(keys, level=level)
    df2 = df2.fillna(0).applymap(int)
    acc[key] = df2
  return acc


@memorized
def cache_table(tablename, mass):
  return packages.normalizer().calc_single_array(tablename, get_default_context(mass))


@memorized
def get_norms(process, mass, tablename='selection_os'):
  """
  Return the expected number of candidates for 4 channel of requested process.
  It uses the current context.
  """
  arr = cache_table(tablename, mass).loc[process].apply(lambda args:args[2])
  print arr
  return pd.Series({tt:arr[tt].to_ufloat() for tt in TTYPES if tt in arr})


#===============================================================================
# PUBLIC METHODS
#===============================================================================

@memorized
def results_data_OS_SS():
  """
  Return number of OS/SS candidates from data under default selection.
  """
  return make_context_all()[Idx[:,:,'selection','DATA','evt']].unstack()

@memorized
def results_nobs():
  """
  Subset of above, only OS, provided the statistical uncertainty
  """
  return results_data_OS_SS()['OS'].apply(lambda x: var(x, 'stat'))

@memorized
def results_QCD():
  """
  For rect_samesign, also calculate rQCD.
  """
  df = make_context_all()[Idx[:,:,'selection','qcd','evt']].unstack()
  df['r'] = df['OS'].apply(var)/df['SS'].apply(var)
  return df

#===============================================================================
# PRELIM CHECK
#===============================================================================

def check_higgs_esel():
  """
  Preliminary check of Higgs esel with purity eff included (sel_anychan/presel_anychan)
  """
  df  = make_context_all().xs(['OS', 'evt'], level=['sign', 'count'])
  df  = df.filter(regex='higgs.*', axis=0).unstack('cut').reset_index('process')
  ## Apply division with Clopper-Pearson uncertainty
  div = lambda se: EffU(se.presel_truechan, se.sel_anychan)
  df  = df.apply(div, axis=1)
  ## rename higgs process to mass
  # func = lambda s: int(s.split('_')[1]) if 'higgs' in s else s
  # df   = df.rename(func).sort_index() 
  return df  

def check_punzi():
  """
  Preliminary check of selection FoM, assuming no excess found, such that B=N
  """
  nobs = results_nobs()
  esel = check_higgs_esel()
  return (esel / (1+nobs)**0.5).apply(nominal_value)

#===============================================================================

if __name__ == '__main__':
  ## Defaulted recalculation
  if '--recalc' in sys.argv:
    print make_context_all()
    sys.exit()


  ## DEV
  # print _make_context('h1', 125, 'selection')
  # print make_context('e', 125).to_string()
  # print make_context_all()
  # print make_context_all().loc[Idx[:, 125, 'selection',: , 'evt']].unstack('sign')
  # print make_context_all().loc[Idx['h1', 125, :, :, 'evt']].unstack('sign')
  # print get_default_context(125)
  # print cache_table('selection_os', 125)
  # print get_norms('wmu17jet', 125)

  # print results_data_OS_SS()
  # print results_QCD()
  # print results_nobs()
  print check_higgs_esel().fmt1p
  print check_punzi()