#!/usr/bin/env python
"""

Scan for best Punzi FoM at the "lite" training mode, required cut on isolation.

"""

from PyrootCK import *
from uncertainties import nominal_value
import argparse
import subprocess
import mva_selected
from mva_utils import TRAINED_BKGS
from HMT_utils import pd, TTYPES, MASSES, pickle_dataframe_nondynamic, memorized, Idx

#===============================================================================
# METHOD 2: Scan the response files
#===============================================================================

def scan_single(t_real, t_sig, bkg, mode):
  logger.info('Scanning: %s.%s'%(mode, bkg))
  ## Prepare the draw string
  param = 'mva.%s.%s_BDT >> h(200, -1, 1)'%(mode,bkg)

  ## Scan the number of data left after MVA cut. Assume the mult-cand is negligible.
  n = t_real.Draw(param, 'iso_dphi', 'goff')
  h_cum = ROOT.gROOT.FindObject('h').cumulative
  n_obs = n - (h_cum.series().apply(nominal_value))
  # print n_obs

  ## Get the signal esel due to iso_dphi.
  n0 = t_sig.count_ent_evt()[1]
  n1 = t_sig.count_ent_evt('iso_dphi')[1]
  esel1 = 1.*n1/n0
  # print esel1

  ## Scan the signal esel
  n = t_sig.Draw(param, 'iso_dphi', 'goff')
  h_cum = ROOT.gROOT.FindObject('h').cumulative
  esel2 = 1. - h_cum.series().apply(nominal_value)/n
  # print esel2

  ## Finally, calculate the Punzi FoM. Package & return
  esel = esel1*esel2
  fom  = esel/(1. + n_obs**0.5)
  df   = pd.concat({
    'nobs': n_obs,
    'esel': esel,
    'fom' : fom,
  }, axis=1)
  ## save only largest FoM, required min nobs
  df = df.loc[df.nobs > 25] # 20% stat
  se = df.loc[df['fom'].idxmax()]
  se['cut'] = float(se.name.split(',')[0].replace('(',''))
  return se


@pickle_dataframe_nondynamic
def scan_fixed_mass(mass):
  ## Loop over choice of trees
  acc = {}
  for tt in TTYPES:
    trees = mva_selected.load_trees(tt, mass)
    t_real = trees['t_real']
    t_sig  = trees['t_higgs_%i'%mass]
    ## Loop over bkgs
    for bkg in TRAINED_BKGS[tt]:
      for mode in ['lite', 'noiso']:
        se = acc[(tt, mass, bkg, mode)] = scan_single(t_real, t_sig, bkg, mode)
  ## Finally
  ROOT.gROOT.CloseFiles()
  df = pd.concat(acc)
  return df

def scan_all():
  """
  Seperate the instances due to large memory usage, loop over mass on different
  python process.
  """
  for mass in MASSES:
    args = './fom_response.py', '--mass', str(mass)
    print subprocess.check_output(args)

@memorized
def load_all():
  se = pd.concat([scan_fixed_mass(m) for m in MASSES])
  se.index.names = 'tt', 'mass', 'bkg', 'mode', 'field'
  return se.unstack('field')

## list single bkg to be used in MVA selection
BKG = {
  'e' : 'data_ss',
  'h1': 'data_ss',
  'h3': 'data_ss',
  'mu': 'dymu',
}

def load():
  """
  Slice only the important information.
  """
  df = load_all()
  def slice_bkg(df):
    return df.loc[df.name].xs(BKG[df.name], level='bkg')
  return df.groupby(level='tt').apply(slice_bkg)

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.SetBatch(True)

  ## Prepare parser
  parser = argparse.ArgumentParser()
  parser.add_argument('--mass', type=int, default=None, choices=MASSES)
  args = parser.parse_args()

  ## Run single mass if specified
  if args.mass is not None:
    scan_fixed_mass(args.mass)
    sys.exit()

  ## Run normal module
  # print scan_fixed_mass(45)
  # print scan_all()
  # print load_all()
  # print load()['fom'].unstack('mode').to_string()
  # print load().xs('noiso', level='mode').to_string()
  print load().xs('noiso', level='mode')['fom'].unstack('tt').to_string()
