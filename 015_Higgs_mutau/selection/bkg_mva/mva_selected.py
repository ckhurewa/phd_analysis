#!/usr/bin/env python
"""
Handle the load of preselected tree, attach the response friend,
and the MVA selection cut is made here.

Reminder: The isolation is factored-out in order to allow anti-iso QCD selection.

"""

from PyrootCK import *
from mva_utils import preselection, MODES, DIR_TRAIN_EVAL
from HMT_utils import TTYPES, PREFIXES, TT_TO_DTYPE, packages, EffU

#===============================================================================

CUTS_ISOLATION = {
  'e' : [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
  'mu': [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
  'h1': [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
  'h3': [ 'ISO1 > 0.9', 'ISO2 > 0.9' ],
}

CUT_ANTIISOLATION = utils.join(
  '{0}_0.50_cc_IT  < 0.6',
  '{1}_0.50_cc_IT  < 0.6',
  '{0}_0.50_cc_vPT > 10E3',
  '{1}_0.50_cc_vPT > 10E3',
).format

CUTS_ANTIISOLATION = {tt:CUT_ANTIISOLATION(*p) for tt,p in PREFIXES.iteritems() if tt in TTYPES}

CUT_DPHI = 'DPHI > 2.7'

# Temporary choice of MVA selection
# users: mva_context.py, mva_ztau.py
CUTS_MVA = {
  ## Double BDT
  # 'e' : '(mva.lite.ztautau_BDT > 0.) & (mva.lite.data_ss_BDT > 0.)',
  # 'h1': '(mva.lite.ztautau_BDT > 0.) & (mva.lite.data_ss_BDT > 0.2)',
  # 'h3': '(mva.lite.ztautau_BDT > 0.) & (mva.lite.data_ss_BDT > 0.)',

  # ## Try single-BDT: Ztautau only
  # 'e' : '(mva.noiso.ztautau_BDT > -0.1 ) & (ISO1>0.9) & (ISO2>0.9)',
  # 'h1': '(mva.lite.ztautau_BDT > 0 )',
  # 'h3': '(mva.noiso.ztautau_BDT > -0.1 ) & (ISO1>0.9) & (ISO2>0.9)',

  # ## Try single-BDT: data_ss only
  # 'e' : '(mva.lite.data_ss_BDT > 0.)',
  # 'h1': '(mva.lite.data_ss_BDT > 0.)',
  'h3': '(mva.lite.data_ss_BDT > 0.)',

  ## Focus on zmumu
  'mu': '(mva.lite.dymu_BDT > 0.)',

  ## mH45
  'e' : '(mva.noiso.data_ss_BDT > -0.10)',
  'h1': '(mva.noiso.data_ss_BDT > -0.13)',
}

#===============================================================================

def get_aliser(tt):
  """
  Return the aliaser function prepared at the given channel.
  """
  ## Localized to tt
  ISO   = CUTS_ISOLATION[tt]
  AISO  = CUTS_ANTIISOLATION[tt]
  MVA   = CUTS_MVA[tt]
  ZTRUE = '(ditau_type_%s >= 1)'%TT_TO_DTYPE[tt] # for Ztautau
  ## Make the composite cut ready
  aliases = {
    'selection'      : utils.join(ISO , CUT_DPHI, MVA),
    'ddqcd'          : utils.join(AISO, CUT_DPHI, MVA),
    'selnoiso'       : utils.join(      CUT_DPHI, MVA),
    'presel_truechan': '1==1',
    'sel_anychan'    : 'selection',
  }
  def apply_alias(tname, tree):
    ## only for QCD
    if 'qcd' in tname:
      tree.SetAlias('selection'      , aliases['ddqcd'])
      tree.SetAlias('ddqcd'          , aliases['ddqcd'])
      tree.SetAlias('selnoiso'       , '1==0')
      tree.SetAlias('presel_truechan', '1==1')
      tree.SetAlias('sel_anychan'    , 'selection')
    elif 'higgs' in tname:
      tree.SetAlias('_truechan', 'tau_type_%s==1'%tt)
      tree.SetAlias('selection'      , utils.join('_truechan', aliases['selection']))
      tree.SetAlias('ddqcd'          , utils.join('_truechan', aliases['ddqcd']    ))
      tree.SetAlias('selnoiso'       , utils.join('_truechan', aliases['selnoiso'] ))
      tree.SetAlias('presel_truechan', '_truechan')
      tree.SetAlias('sel_anychan'    , aliases['selection'])
    else:
      ## Main composite
      for key, val in aliases.iteritems():
        tree.SetAlias(key, val)

    ## For Ztautau esel, ZXC
    tree.SetAlias('Presel_TrueChanZtau', ZTRUE if 'ztautau' in tname else '1==1')
    tree.SetAlias('Sel_TrueChanZtau'   , utils.join('selection', 'Presel_TrueChanZtau'))

    ## For FoM-lite study
    tree.SetAlias('iso_dphi', utils.join(ISO, CUT_DPHI))

  ## finally, return a func
  return apply_alias

# def load_tree(tt, mass, tname):
#   """
#   Load the single tree from the prepared configuration..
#   As this is an explicitly manual approach, return (tfile, ttree) to be used 
#   with careful memory handling.
#   """
#   pass

def load_trees(tt, mass):
  """
  Load the preselected tree, friended with the MVA response above.

  Use `ROOT.gROOT.CloseFiles()` when applicable.
  """
  ## 1. Load the presel tree & HMT.MVA aliaser
  trees0  = preselection.load_trees_dict(tt)
  aliaser = get_aliser(tt)

  ## discard all other higgs tree
  for tname in list(trees0.keys()):
    if 'higgs' in tname and str(mass) not in tname:
      del trees0[tname]

  ## 2. Attach friend & aliases of the existed response
  trees = {}
  for dltype in MODES:
    ## load appropriated response file
    fname = 'response_%s_%i_%s.root'%(tt, mass, dltype)
    fin   = ROOT.TFile(os.path.join(DIR_TRAIN_EVAL, fname))
    fin.ownership = False
    for tname, t_src in trees0.iteritems():
      ## attach the friend
      t_res = fin.Get(tname)
      if not t_res:
        logger.debug('Skip missing response tree: %s'%tname)
        continue
      t_res.ownership = False
      t_src.AddFriend(t_res, 'mva.%s'%dltype)
      ## local aliases
      aliaser(tname, t_src)
      ## push to export queue
      trees[tname] = t_src

  # ## 3. additional t_wzjet tree for use, derived form selected tree
  # # in tight regime, some tree are no longer available
  # tw0 = 'wmu17jet'
  # tz0 = 'zmu17jet'
  # for prefix in ['t_', 'tss_']:
  #   # for suffix in ['', '_noiso']:
  #   for suffix in ['']:
  #     ## line up the tree names
  #     twz = prefix + 'wzjet' + suffix
  #     tw  = prefix + tw0     + suffix
  #     tz  = prefix + tz0     + suffix
  #     ## replace the opposite-sign Zjet
  #     if prefix=='t_' and tt=='mu': 
  #       tz = 'tss_'+tz0+suffix
  #     ## skip if not ready
  #     if tw not in trees:
  #       logger.warning('Skip missing tree [%s, %s]; %s'%(regime, tt, tw))
  #       continue
  #     if tz not in trees:
  #       logger.warning('Skip missing tree [%s, %s]; %s'%(regime, tt, tz))
  #       continue
  #     ## make it
  #     tree = trees[twz] = ROOT.TChain(twz, twz)
  #     tree.Add(trees[tw])
  #     tree.Add(trees[tz])
  #     aliaser(twz, tree)

  ## Ready, return
  return trees 

def load_trees_groupped(tt, mass):
  """  
  Return grouppd trees (trees_os, trees_ss)
  """
  trees = load_trees(tt, mass)
  return packages.id_utils().indexing_trees_os_ss(trees)[1:]

#===============================================================================

if __name__ == '__main__':
  trees = load_trees('h1', 45)
  # print trees['t_real'].count_ent_evt('selection')
  # print trees['tss_real'].count_ent_evt('selection')
  # print trees['t_higgs_125'].count_ent_evt('selection')
  # print load_trees_groupped('h1', 125)

  ## Recheck higgs esel
  # print trees['t_higgs_125'].count_ent_evt('(mva.lite.data_ss_BDT > 0.)')
  # print trees['t_higgs_125'].count_ent_evt('(ISO1 > 0.9) & (ISO2 > 0.9) & (mva.lite.data_ss_BDT > 0.)')

  ## fast punzi
  n0 = trees['t_higgs_45'].count_ent_evt('presel_truechan')[1]
  n1 = trees['t_higgs_45'].count_ent_evt('sel_anychan')[1]
  n  = trees['t_real'].count_ent_evt('selection')[1]
  print n, EffU(n0, n1), (EffU(n0, n1) / (1+n)**0.5).n
