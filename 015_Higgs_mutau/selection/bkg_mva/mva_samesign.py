#!/usr/bin/env python

from PyrootCK import *
from PyrootCK.QHist import postprocessors
import mva_selected # tethered
import mva_context
from HMT_utils import (
  pd, TTYPES, MASSES, packages,
  pickle_dataframe_nondynamic, memorized
)

## DEV override
MASSES = [125,]

## Optional override
SPEC_SSFIT = {
  # 'e' : 'e_tf_30_true',
  # 'h1': 'h1_tf_30_true', 
  # 'h3': 'h3_tf_30_true',
  # 'mu': 'mu_tf_30_true',
  '125_h1': 'h1-125_rf_20',
}

## For rEWK calculation
SPEC_EWK = {
  'e': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
  'h1': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
  'h3': {
    'wmu17jet': True,
    'zmu17jet': True,
  },
  'mu': {
    'wmu17jet': True, # temp instead of wmumujet
    'zmu17jet': None,
  },
}

#===============================================================================
# TREES
#===============================================================================

def get_trees_ssfit(tt, mass):
  trees = mva_selected.load_trees_groupped(tt, mass)[1]

  ## same-sign data
  t1 = trees['real']
  t1.SetAlias('SSFIT', 'selection')
  t1.title = 'Data'

  ## data-driven QCD
  t2 = trees['qcd']
  t2.SetAlias('SSFIT', 'ddqcd')
  t2.title= 'QCD'

  ## EWK
  t3 = trees[{
    'e' : 'wmu17jet',
    'h1': 'wmu17jet',
    'h3': 'wmu17jet',
    'mu': 'wmumujet',
    # 'h1': 'wzjet',
  }[tt]]
  t3.title = 'V+jet'
  # t3.SetAlias('SSFIT', 'selnoiso')
  t3.SetAlias('SSFIT', 'selection')

  # t4 = trees['zmu17jet']
  # t4.SetAlias('SSFIT', 'selection')

  ## Finally
  return t1, t2, t3 #, t4

#===============================================================================

@pickle_dataframe_nondynamic
def break_samesign(mass, tt):
  """
  Required tt to be the last arg.
  """
  ## Apply LHCb style
  if ROOT.gStyle.name != 'lhcbStyle': # don't repeat
    ROOT.gROOT.ProcessLine(".L lhcbstyle.C")

  ## Fetch trees into the engine
  ROOT.gROOT.SetBatch(True)
  trigger_RooFit_patch()
  trees = get_trees_ssfit(tt, mass)
  tag   = tt+'-'+str(mass)

  ## Execute
  exe = packages.samesign_utils().ssfit_engine
  return exe(tag, trees, param='DPT', filters='SSFIT', ranges=(-100, 200))
  # return id_utils.ssfit_engine(tt, trees, param='DPT', filters='Selection', ranges=(-100, 200), maxbin=60)


def break_samesign_all():
  """
  Loop over all ttypes & mass, break samesign into QCD+EWK
  """
  for tt in TTYPES:
    for mass in MASSES:
      break_samesign(mass, tt)

#===============================================================================
# COMPUTE
#===============================================================================

@memorized
def parse_spec_ssfit():
  return packages.samesign_utils().guess_ssfit_spec(__file__, SPEC_SSFIT)

@memorized
def load_rewk(mass):
  context = mva_context.get_default_context(mass)
  return packages.samesign_utils().load_rewk(context, SPEC_EWK, 'selection', 'selnoiso')

@memorized
def result_ssfit_raw(mass):
  spec0   = parse_spec_ssfit()
  spec    = spec0.filter(regex=str(mass)+'_', axis=0).rename(index=lambda s: s.split('_')[1])
  data_ss = mva_context.results_data_OS_SS()['SS'].xs(mass, level='mass')
  tag     = 'break_samesign_%i'%mass
  result  = packages.samesign_utils().result_ssfit_raw_HMT(spec, __file__, data_ss, tag)
  del result['Ztau']
  result.index.name = 'tt'
  return result

#===============================================================================
# RATIO FACTOR + RESULTS
#===============================================================================

@memorized
def get_rQCD():
  return mva_context.results_QCD()['r'].swaplevel()

@memorized
def get_rEWK():
  return pd.concat({m:load_rewk(m)['r'].xs('V', level=1) for m in MASSES}, names=['mass', 'tt'])

@memorized
def result_qcd():
  r = get_rQCD()
  n = pd.concat({m:result_ssfit_raw(m)['QCD'] for m in MASSES}, names=['mass'])
  return n*r

@memorized
def result_ewk():
  r = get_rEWK()
  n = pd.concat({m:result_ssfit_raw(m)['EWK'] for m in MASSES}, names=['mass'])
  return n*r

#===============================================================================

def debug_draw():
  h = QHist()
  h.filters = 'SSFIT'
  h.trees   = get_trees_ssfit('e', 125)
  h.params  = 'DPT'
  h.xmin    = -80
  h.xmax    = 120
  h.xbin    = 30
  # h.params  = 'TMath::Abs(DPT)/(PT1+PT2)'
  # h.xmin    = 0
  # h.xmax    = 1
  # h.xbin    = 20
  # h.params  = 'M/1e3'
  # h.xmin    = 0
  # h.xmax    = 200
  # h.xbin    = 40
  h.normalize = False
  h.draw()

  # ## helper to yield preprocessor
  # def lim_ewk(frac):
  #   def preprocessor(fitter):
  #     logger.info('Apply constrain: %.3f'%frac)
  #     fitter.Constrain(1, frac, 1.0)
  #     return fitter
  #   return preprocessor

  fitter, res, uarr = postprocessors.fraction_fit(h, False)
  print uarr
  print '{:.2f}'.format(sum(uarr))

  exit()

#===============================================================================

if __name__ == '__main__':
  ## Defaulted recalculation
  if '--recalc' in sys.argv:
    break_samesign_all()
    print parse_spec_ssfit()
    sys.exit()

  ## DEV
  # print get_trees_ssfit('e', 125)
  # debug_draw()
  print break_samesign(125, 'h1')
  # packages.samesign_utils().print_ssfit_stats(__file__, show_all=True) #,#, only_dt='h1') #, 
  # packages.samesign_utils().print_ssfit_stats(__file__)
  
  # print parse_spec_ssfit()
  # print load_rewk(125)
  # print result_ssfit_raw(125)
  # print get_rQCD()
  # print get_rEWK()
  # print result_qcd()
  # print result_ewk()
