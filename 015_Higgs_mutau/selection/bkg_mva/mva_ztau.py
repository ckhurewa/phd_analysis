#!/usr/bin/env python

"""

Estimate the number of Ztautau background

"""

from PyrootCK import *
from PyrootCK.mathutils import EffU

import mva_context
from HMT_utils import pd, TTYPES, TT_TO_DTYPE, memorized, packages, MASSES

## DEV override mass
MASSES = [125,]

#===============================================================================
# RECONSTRUCTION EFF
#===============================================================================

def erec_dummy():
  return pd.Series({
    'e' : 0.523,
    'h1': 0.492,
    'h3': 0.154,
    'mu': 0.663,
  })


#===============================================================================

@memorized
def fetch(mass):
  df = mva_context.make_context_all().xs(mass, level='mass')
  return df.xs(['OS', 'evt'], level=['sign', 'count']).unstack('tt')

@memorized
def esel(mass):
  div = lambda se: EffU(se.Presel_TrueChanZtau, se.selection)
  return fetch(mass).xs('ztautau', level='process').apply(div)

def purity():
  """
  Purity, defined as number of di-tau candidates from true channel over those 
  from all channels
  """
  raise NotImplementedError

@memorized
def results_raw(mass):
  """
  Compute expected Ztautau = csc * Lumi * BR * ACC * REC * SEL
  """
  df = packages.bkg_ztautau().components()
  df['esel']       = esel(mass)
  # df['purity_inv'] = 1./purity(regime)
  df['erec']       = erec_dummy()
  expected         = df.prod(axis=1) # before other aux columns
  # df['purity']     = 1./df['purity_inv']
  df['expected']   = expected # to be last column
  return df

@memorized
def results_raw_all():
  df = pd.concat({m:results_raw(m) for m in MASSES})
  df.index.names = 'mass', 'tt'
  return df

def results():
  return results_raw_all()['expected']

#===============================================================================

if __name__ == '__main__':
  pass

  ## DEV
  # print fetch(125)
  # print esel(125)
  # print purity()

  ## Finally
  # print results_raw()
  # print results().apply('{:5.2f}'.format)
  # print results_raw_all()
  print results()
