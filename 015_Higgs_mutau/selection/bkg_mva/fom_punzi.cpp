// Helper macro to run Punzi optimization in batch mode
int fom_punzi(TString tag, TString src){
  gROOT->SetBatch(false);
  TMVA::mvaeffs(tag, src, true, "S/(1+sqrt(B))");
  return 0;
}
