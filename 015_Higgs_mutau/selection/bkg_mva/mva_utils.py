#!/usr/bin/env python

import os, sys
sys.path.append(os.path.expandvars('$DIR15'))  # HMT_utils
from HMT_utils import TTYPES, PREFIXES, ROOTLABELS, memorized

from PyrootCK import ROOT
from PyrootCK.ioutils import TMVA_Adapter

## handle
sys.path.append(os.path.expandvars('$DIR15/selection')) # For running on cluster
import preselection

#===============================================================================
# SOURCES
# users: response.py, fom.py
#===============================================================================

## Path to output dir of latest def
# jid_train, jid_eval = 5356, 5362  # fix wrong tag name
# jid_train, jid_eval = 5367, 5368  # true chan, no M>20 cut, fast BDT only
# jid_train, jid_eval = 5369, 5370  # more trees, PRER & VCHI2PDOF cut
# jid_train, jid_eval = 5371, 5373  # add mu_data_ss
# jid_train, jid_eval = 5374, 5377  # noiso data
# jid_train, jid_eval = 5381, 5384  # noiso data, M>20e3
# jid_train, jid_eval = 5393, 5394  # lite
# jid_train, jid_eval = 5395, 5396  # full, all 4 modes
# jid_train, jid_eval = 5397, 5400  # fast, presel ghost < 0.4, data_ss train-iso>0.8'
JID = 5476

## Local
DIR_TRAIN_EVAL = '.'
# DIR_EVALUATE = '.'

## Ganga
DIR_TRAIN_EVAL = '/home/khurewat/gangadir/workspace/khurewat/LocalXML/%i/output/response'%JID
# DIR_EVALUATE = '/Users/khurewat/Documents/lphe_offline/gangadir_large/%i'%jid_eval

#===============================================================================

## list of trained backgrounds
TRAINED_BKGS = {
  'mu': ['ztautau', 'data_ss', 'dymu'],
  'e' : ['ztautau', 'data_ss'],
  'h1': ['ztautau', 'data_ss'],
  'h3': ['ztautau', 'data_ss'],
}

#===============================================================================

def load_vars_all(tt):
  """
  Return semi-static list of all vars, approproate expression & title
  can be reduced to smaller set
  """
  p1,p2 = PREFIXES[tt]
  l1,l2 = ROOTLABELS[tt]
  return {
    'P1'    : ['TMath::Log10(%s_P)'%p1        , 'p(#tau_{%s})'%l1     ],
    'P2'    : ['TMath::Log10(%s_P)'%p2        , 'p(#tau_{%s})'%l2     ],
    'PT1'   : ['TMath::Log10(%s_PT)'%p1       , 'p_{T}(#tau_{%s})'%l1 ],
    'PT2'   : ['TMath::Log10(%s_PT)'%p2       , 'p_{T}(#tau_{%s})'%l2 ],
    'IP1'   : ['%s_BPVIP'%p1                  , 'IP(#tau_{%s})'%l1    ],
    'IP2'   : ['%s_BPVIP'%p2                  , 'IP(#tau_{%s})'%l2    ],
    # 'IPXSQ1': ['TMath::Log10(%s_BPVIPCHI2)'%p1, 'IPchi2(#tau_{%s})'%l1],
    # 'IPXSQ2': ['TMath::Log10(%s_BPVIPCHI2)'%p2, 'IPchi2(#tau_{%s})'%l2],
    'IPXSQ1': ['%s_BPVIPCHI2'%p1              , 'IPchi2(#tau_{%s})'%l1],
    'IPXSQ2': ['%s_BPVIPCHI2'%p2              , 'IPchi2(#tau_{%s})'%l2],
    'ISO1'  : ['%s_0.50_cc_IT'%p1             , 'Iso(#tau_{%s})'%l1   ],
    'ISO2'  : ['%s_0.50_cc_IT'%p2             , 'Iso(#tau_{%s})'%l2   ],
    'ISOn1' : ['%s_0.50_nc_IT'%p1             , 'IsoN(#tau_{%s})'%l1  ],
    'ISOn2' : ['%s_0.50_nc_IT'%p2             , 'IsoN(#tau_{%s})'%l2  ],
    'ConeC1': ['%s_0.50_cc_vPT'%p1            , 'ConeC(#tau_{%s})'%l1 ],
    'ConeC2': ['%s_0.50_cc_vPT'%p2            , 'ConeC(#tau_{%s})'%l2 ],
    'ConeN1': ['%s_0.50_nc_vPT'%p1            , 'ConeN(#tau_{%s})'%l1 ],
    'ConeN2': ['%s_0.50_nc_vPT'%p2            , 'ConeN(#tau_{%s})'%l2 ],
    #
    # 'IP1oPT': ['TMath::Max(%s_BPVIP, %s_BPVIP)'%(p1,p2), 'IPmax'],
    #
    'BPVCORRM' : ['tau_BPVCORRM'                            , 'm_{corr}(#tau_{h3})'             ],
    'VCHI2PDOF': ['TMath::Log10(tau_VCHI2PDOF)'             , 'Vertex #chi^{2}/dof(#tau_{h3})'  ],
    'BPVVD'    : ['TMath::Log10(tau_BPVVD)'                 , 'd_{flight}(#tau_{h3})'           ],
    'BPVLTIME' : ['TMath::Log10(tau_BPVLTIME)'              , 'Decay time(#tau_{h3})'           ],
    'DRTRIOMAX': ['tau_DRTRIOMAX'                           , '#Delta R_{max}(#tau_{h3})'       ],
    'DRTRIOMID': ['tau_DRTRIOMID'                           , '#Delta R_{mid}(#tau_{h3})'       ],
    'DRTRIOMIN': ['tau_DRTRIOMIN'                           , '#Delta R_{min}(#tau_{h3})'       ],
    'DRoPT'    : ['TMath::Log10(tau_DRTRIOMAX/(tau_PT/1e3))', '#Delta R_{max}/p_{T}(#tau_{h3})' ],
    'DRtPT'    : ['TMath::Log10(tau_DRTRIOMAX*(tau_PT/1e3))', '#Delta R_{max}*p_{T}(#tau_{h3})' ],
    #
    'DPHI'    : ['DPHI'                  , '#Delta#phi'    ],
    'DR'      : ['DR'                    , '#Delta R'      ],
    'APT'     : ['APT'                   , 'A_{PT}'        ],
    'DPT'     : ['%s_PT-%s_PT'%(p1,p2)   , '#Delta_{PT}'   ],
    'DOCACHI2': ['TMath::Log10(DOCACHI2)', 'DOCA #chi^{2}' ],
    'M'       : ['M/1e3'                 , 'Invariant mass'],
  }

#===============================================================================
# VAR MODES
# Return a collection of (expr-title) pair, ready for adapter.
#===============================================================================

def load_vars_train_all(tt):
  """
  All variables possible.
  """
  ## Prepare the customized spec
  SPEC_1P = [
    'P1', 'P2', 'PT1', 'PT2',
    'IP1', 'IP2', 'IPXSQ1', 'IPXSQ2',
    'ISO1', 'ISO2', 'ISOn1', 'ISOn2', 'ConeC1', 'ConeC2', 'ConeN1', 'ConeN2',
    'DPHI', 'DR', 'APT', 'DOCACHI2',
  ]
  SPEC_3P = [
    'P1', 'P2', 'PT1', 'PT2',
    'IP1', 'IPXSQ1',
    'ISO1', 'ISO2', 'ISOn1', 'ISOn2', 'ConeC1', 'ConeC2', 'ConeN1', 'ConeN2',
    'BPVCORRM', 'VCHI2PDOF', 'BPVLTIME',
    'DRTRIOMAX', 'DRTRIOMID', 'DRTRIOMIN', 'DRoPT',
    'DPHI', 'DR', 'APT', 'DOCACHI2',
  ]
  spec    = SPEC_3P if 'h3' in tt else SPEC_1P
  params0 = load_vars_all(tt)
  return [v for k,v in params0.iteritems() if k in spec]


def load_vars_train_noiso(tt):
  """
  Like "ALL", but skip the isolation variable.
  """
  SPEC_1P = [
    'P1', 'P2', 'PT1', 'PT2',
    'IP1', 'IP2', 'IPXSQ1', 'IPXSQ2',
    'DPHI', 'DR', 'APT', 'DOCACHI2',
  ]
  SPEC_3P = [
    'P1', 'P2', 'PT1', 'PT2',
    'IP1', 'IPXSQ1',
    'BPVCORRM', 'VCHI2PDOF', 'BPVLTIME',
    'DRTRIOMAX', 'DRTRIOMID', 'DRTRIOMIN', 'DRoPT',
    'DPHI', 'DR', 'APT', 'DOCACHI2',
  ]
  spec    = SPEC_3P if 'h3' in tt else SPEC_1P
  params0 = load_vars_all(tt)
  return [v for k,v in params0.iteritems() if k in spec]


def load_vars_train_lite_withpt(tt):
  """
  Minimum choice of variable, similar to Ztautau.
  Exclude isolation to allow late-cut for QCD anti-iso.
  """
  SPEC_1P = 'PT1', 'PT2', 'IPXSQ1', 'IPXSQ2', 'DPHI', 'APT'
  SPEC_3P = 'PT1', 'PT2', 'IPXSQ1', 'BPVCORRM', 'BPVLTIME', 'DRoPT', 'DPHI', 'APT'
  spec    = SPEC_3P if 'h3' in tt else SPEC_1P
  params0 = load_vars_all(tt)
  return [v for k,v in params0.iteritems() if k in spec]


# def load_vars_train_lite_nopt(tt):
#   """
#   Minimum choice of variable, excluding also the PT1, PT2
#   """
#   SPEC_1P = 'IPXSQ1', 'IPXSQ2', 'DPHI', 'APT'
#   SPEC_3P = 'IPXSQ1', 'BPVCORRM', 'BPVLTIME', 'DRoPT', 'DPHI', 'APT'
#   spec    = SPEC_3P if 'h3' in tt else SPEC_1P
#   params0 = load_vars_all(tt)
#   return [v for k,v in params0.iteritems() if k in spec]

## 
MODE_LOADERS = {
  'all'  : load_vars_train_all,
  'noiso': load_vars_train_noiso,
  'lite' : load_vars_train_lite_withpt,
}

## available modes in the current training
MODES = MODE_LOADERS.keys()

#===============================================================================

def new_data_loader(name, tt, mode='all'):
  ## Create new loader, add vars
  params = MODE_LOADERS[mode](tt)
  loader = ROOT.TMVA.DataLoader(name+'_'+mode) # this name is VERY important
  for expr, title in params:
    loader.AddVariable(expr, title, '', 'F')
  return loader

def new_reader_adapter(tt, mode='all'):
  params = MODE_LOADERS[mode](tt)
  adapter = TMVA_Adapter(params)
  adapter.reader = ROOT.TMVA.Reader()
  adapter.reader.ownership = False
  return adapter

#===============================================================================
