"""

Run the training & evaluation of MVA jobs

"""

#===============================================================================

def job_train_and_eval():
  ttypes = 'e', 'h1', 'h3', 'mu'
  masses = xrange(45, 200, 10)
  # masses = 125,
  args = [[tt, str(m)] for tt in ttypes for m in masses]

  j = Job()
  j.name        = 'HMT.MVA.train_eval'
  j.comment     = ''
  j.application = Executable(exe=File('train_and_eval.sh'))
  j.backend     = PBS(extraopts='--mem=2900 -t 3-0:0:0 --exclude=lphe16,lphe17,lphe18,lphe19,lphe20')
  j.splitter    = ArgSplitter(args=args)
  j.outputfiles = ['*.root', '*/weights/*']
  return j

#===============================================================================

j = job_train_and_eval()

queues.add(j.submit)
