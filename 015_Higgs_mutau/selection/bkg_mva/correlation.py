#!/usr/bin/env python

#===============================================================================

def main_calculate_corr():

  tree = trimmed_trees( 'Preselection', first_only=True )

  ## load the input params into adapter
  adapter = TMVA_Adapter(load_params(dt))

  ## Loads the library
  ROOT.TMVA.Tools.Instance()
  fout    = ROOT.TFile("corr_%s.root"%dt, "RECREATE")
  factory = ROOT.TMVA.Factory("TMVAClassification", fout )

  ## Add variables
  adapter.attach_factory(factory)

  ## Add sources
  factory.AddSignalTree(tree)   
  factory.PrepareTrainingAndTestTree(ROOT.TCut(''), ROOT.TCut(''), '')

  ## RUN
  factory.TrainAllMethods()     # Train MVAs using the set of training events
  factory.TestAllMethods()      # Evaluate all MVAs using the set of test events
  factory.EvaluateAllMethods()  # Evaluate and compare performance of all configured MVAs

  ## Finally
  fout.Close()

#===============================================================================

def extract_single(dt):
  fin = ROOT.TFile('corr_%s.root'%dt)
  h   = fin.Get('CorrelationMatrixS')
  h.title       = 'Correlation Matrix: #tau_{%s}#tau_{%s}'%labels[dtype]
  h.bit         = ROOT.TH1.kNoTitle
  
  ## Canvas adjustment
  if 'h3' in dt:
    h.markerSize  = 2.4
    h.Draw('text col')
    #
    gPad.SetCanvasSize( 720, 360 )
    gPad.SetLeftMargin  (0.16)
    gPad.SetRightMargin (0.04)
    gPad.SetTopMargin   (0.02)
    gPad.SetBottomMargin(0.12)
  else:
    h.markerSize      = 2.6
    h.xaxis.labelSize = 0.08
    h.yaxis.labelSize = 0.08
    h.Draw('text col')
    #
    gPad.SetCanvasSize( 720, 280 )
    gPad.SetLeftMargin  (0.10)
    gPad.SetRightMargin (0.04)
    gPad.SetTopMargin   (0.02)
    gPad.SetBottomMargin(0.12)
  gPad.Update()
  gPad.SaveAs('classifier/corr_%s.pdf'%dt)
  fin.Close()

def main_extract():
  """
  Getting corr matrix as PDF in style.
  """
  gROOT.SetBatch(True)
  gStyle.SetPalette(ROOT.kTemperatureMap)
  for dt in channels:
    extract_single(dt)

#===============================================================================
