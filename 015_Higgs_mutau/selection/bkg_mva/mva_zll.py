#!/usr/bin/env python

from PyrootCK import *
from PyrootCK.mathutils import EffU

from mva_utils import CUT_SELECTION, DIR_EVALUATE
from HMT_utils import pd, TTYPES, TT_TO_DTYPE, memorized

#===============================================================================

tt = 'h1'

path = os.path.join(DIR_EVALUATE, 'evaluated_%s_125.root')%tt

fin = ROOT.TFile(path)

tree = fin['t_dimuon_misid']

print tree
print tree.Draw('weight', CUT_SELECTION[tt], 'goff')
df = tree.sliced_dataframe()
print df
