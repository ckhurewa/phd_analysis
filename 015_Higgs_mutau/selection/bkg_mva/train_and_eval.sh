#!/usr/bin/env bash

lb-run ROOT $DIR15/selection/bkg_mva/training.py --ttype=$1 --mass=$2
lb-run ROOT $DIR15/selection/bkg_mva/response.py --ttype=$1 --mass=$2
