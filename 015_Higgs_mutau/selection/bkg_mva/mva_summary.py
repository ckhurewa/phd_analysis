#!/usr/bin/env python

from PyrootCK import *
import mva_utils
from HMT_utils import (
  DT_TO_TT, MASSES, X_to_latex, Idx,
  pd, memorized, pickle_dataframe_nondynamic, fmt2cols, inject_midrule,
)
from uncertainties import nominal_value, std_dev

## DEV override mass
MASSES = [125,]

#===============================================================================

@memorized
def results():
  import mva_context
  import mva_ztau
  import mva_samesign

  acc = {}
  for mass in MASSES:
    get = lambda proc: mva_context.get_norms(proc, mass)
    ## specific order
    df = pd.DataFrame()
    df['Ztau']  = mva_ztau.results().xs(mass, level='mass')
    # df['Zll']   = mva_zll.results()[rg]
    df['QCD']   = mva_samesign.result_qcd().xs(mass, level='mass')
    df['EWK']   = mva_samesign.result_ewk().xs(mass, level='mass')
    # df['VV']    = get('WW_lx') + get('WZ_lx')
    # df['ttbar'] = get('ttbar_41900006') + get('ttbar_41900007') + get('ttbar_41900010')
    # df['Zbb']   = get('zbb')

    ## Repackage
    df['BKG'] = df.sum(axis=1)
    df['OS']  = mva_context.results_nobs().xs(mass, level='mass')
    acc[mass] = df.T

  ## finally
  return pd.concat(acc, names=['mass', 'process'])

#===============================================================================

if __name__ == '__main__':
  pass

  ## Final
  print results().fmt1f
