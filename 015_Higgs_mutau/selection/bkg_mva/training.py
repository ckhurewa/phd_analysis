#!/usr/bin/env python

import argparse
from PyrootCK import *
sys.path.append(os.path.expandvars('$DIR15/selection/bkg_mva')) # For running on cluster
from mva_utils import new_data_loader, TRAINED_BKGS, MODES, preselection, load_vars_all
from HMT_utils import TT_TO_DTYPE, TTYPES

#===============================================================================

def train_single(tt, tag, t_sig, t_bkg, cut_sig='', cut_bkg='', fast=True):
  """
  Attempt to use BDT to separate Higgs -> mu tau from given background tree.
  Note: The alias doesn't work here...
  """
  assert t_sig
  assert t_bkg
  print locals()

  ## Loads the library
  ROOT.TMVA.Tools.Instance()
  name    = '%s_%s'%(tt, tag)
  logger.info('Running: %s'%name)
  # fac_opt = "!V:!Silent:Transformations=I;D;P;G,D:AnalysisType=Classification"
  fac_opt = "!V:!Silent:Transformations=I:AnalysisType=Classification"
  fout    = ROOT.TFile("train_%s.root"%name, "RECREATE")
  factory = ROOT.TMVA.Factory('factory', fout, fac_opt)

  ## Customized loader to tt
  # Prepare all kind of loader (i.e., different set of variables )
  loaders = {}
  for mode in MODES:
    loaders[mode] = loader = new_data_loader(name, tt, mode=mode)
    loader.AddSignalTree(t_sig)
    loader.AddBackgroundTree(t_bkg)
    loader.PrepareTrainingAndTestTree(ROOT.TCut(cut_sig), ROOT.TCut(cut_bkg),
      'SplitMode=Random:NormMode=NumEvents')

  ## Add the methods: BDT
  # https://nbviewer.jupyter.org/github/oprojects/tmva-notebook-dev/blob/master/ParallelExecution.ipynb
  # https://root.cern.ch/doc/v608/TMVAClassification_8C_source.html
  ctype   = ROOT.TMVA.Types.kBDT
  method  = 'BDT'
  opts    = '!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20'
  for loader in loaders.values():
    factory.BookMethod(loader, ctype, method, opts)

  ## Dev version, more methods, slower training time.
  if not fast:

    ## TMVA ANN: MLP (recommended ANN) -- all ANNs in TMVA are Multilayer Perceptrons
    ctype   = ROOT.TMVA.Types.kMLP
    method  = "MLP"
    opts    = "H:!V:NeuronType=tanh:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5:!UseRegulator"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

    ## Add the methods: CUT
    # ~ 5sec
    ctype   = ROOT.TMVA.Types.kCuts
    method  = 'Cuts'
    opts    = "!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

    #
    ctype   = ROOT.TMVA.Types.kCuts
    method  = "CutsPCA"
    opts    = "!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart:VarTransform=PCA"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

    # ## Multi-dimensional probability density estimator
    # ## SLOW
    # ctype   = ROOT.TMVA.Types.kPDERS
    # method  = "PDERS"
    # opts    = "!H:!V:NormTree=T:VolumeRangeMode=Adaptive:KernelEstimator=Gauss:GaussSigma=0.3:NEventsMin=400:NEventsMax=600"
    # factory.BookMethod(loader_all  , ctype, method, opts)
    # factory.BookMethod(loader_noiso, ctype, method, opts)

    ## Add the methods: BDT
    # https://nbviewer.jupyter.org/github/oprojects/tmva-notebook-dev/blob/master/ParallelExecution.ipynb
    ctype   = ROOT.TMVA.Types.kBDT
    method  = "BDTG"
    opts    = "!H:!V:NTrees=1000:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=20:MaxDepth=2"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

    ctype   = ROOT.TMVA.Types.kBDT
    method  = 'BDTB'
    opts    = "!H:!V:NTrees=400:BoostType=Bagging:SeparationType=GiniIndex:nCuts=20"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

    ctype   = ROOT.TMVA.Types.kBDT
    method  = 'BDTD'
    opts    = "!H:!V:NTrees=400:MinNodeSize=5%:MaxDepth=3:BoostType=AdaBoost:SeparationType=GiniIndex:nCuts=20:VarTransform=Decorrelate"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

    ctype   = ROOT.TMVA.Types.kBDT
    method  = 'BDTF'
    opts    = "!H:!V:NTrees=50:MinNodeSize=2.5%:UseFisherCuts:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20"
    for loader in loaders.values():
      factory.BookMethod(loader, ctype, method, opts)

  ## RUN
  factory.TrainAllMethods()     # Train MVAs using the set of training events
  factory.TestAllMethods()      # Evaluate all MVAs using the set of test events
  factory.EvaluateAllMethods()  # Evaluate and compare performance of all configured MVAs

  ## Finally
  fout.Close()

#===============================================================================

def main_training(args):
  ## Load preselected tree
  tt    = args.ttype
  trees = preselection.load_trees_dict(tt)
  t_sig = trees['t_higgs_%i'%args.mass]

  ## Additional preselection cut for training stability & unbias.
  ## Remark: Alias not valid here
  # precut = '(M > 20e3) & (_TrueChan)'
  allvars = {key:vals[0] for key,vals in load_vars_all(tt).iteritems()}
  cuts0 = [
    'DPHI > 2.7', # due to the hard-coded cut in DCTW.
    ## True ISO
    '%s > 0.9'%allvars['ISO1'], # isolation as common first stage
    '%s > 0.9'%allvars['ISO2'],
    ## Weak ISO: Just outside the anti-iso cut
    # '!((%s<0.6) & (%s<0.6) & (%s>10e3) & (%s>10e3))'%(allvars['ISO1'], allvars['ISO2'], allvars['ConeC1'], allvars['ConeC2']),
  ]
  if tt == 'e': # kill for easier ssfit shape here
    cuts0 += ['%s > 0'%allvars['DPT']]

  ## Signal + global cut
  cut_sig = utils.join(
    cuts0,
    ['tau_type_%s == %i'%(s,int(s==tt)) for s in TTYPES],
  )

  ## Start run, loop over all queue backgrounds
  for bkgname in TRAINED_BKGS[tt]:
    cut_bkg = ''
    if bkgname == 'data_ss':
      t_bkg = trees['tss_real']
    else:
      t_bkg = trees['t_'+bkgname]
      if bkgname == 'ztautau':
        cut_bkg = 'ditau_type_%s == 1'%TT_TO_DTYPE[tt]
      # old schema, need to patch for missing branch even if the cut is not applied
      if bkgname == 'dymu':
        for _tt in TTYPES:
          t_bkg.SetAlias('tau_type_%s'%_tt, '0==1')
    ## Add global cut to bkg
    cut_bkg = utils.join(cuts0, cut_bkg)
    ## Unique identifier
    tag = '%i_%s'%(args.mass, bkgname)
    train_single(tt, tag, t_sig, t_bkg, cut_sig, cut_bkg, fast=(not args.full))

  ## finally
  ROOT.gROOT.CloseFiles()

#===============================================================================

if __name__ == '__main__':
  ## Run with args
  parser = argparse.ArgumentParser()
  parser.add_argument('--ttype', type=str  , default='h1', choices=TTYPES)
  parser.add_argument('--mass' , type=int  , default=125  )
  parser.add_argument('--full' , default=False, action='store_true', help="True to run all MVA methods.")
  args = parser.parse_args()
  print args
  main_training(args)
