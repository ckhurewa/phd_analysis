#!/usr/bin/env python

"""

Peek last line of stderr at given job, useful for MVA training job

"""

import os
import subprocess

JID = 5393

foo = lambda i: subprocess.check_output(['tail', '-n', '1', '/home/khurewat/gangadir/workspace/khurewat/LocalXML/%i/%i/output/stderr'%(JID,i)]).split('\r')[-2]

for i in xrange(68):
  print '%03i'%i, foo(i)
