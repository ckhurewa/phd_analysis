#!/usr/bin/env python

import argparse
from PyrootCK import *

sys.path.append(os.path.expandvars('$DIR15/selection/bkg_mva')) # For running on cluster
from mva_utils import (
  MODES, TRAINED_BKGS, TTYPES, DIR_TRAIN_EVAL
  preselection, new_reader_adapter,
)

FAST_LIST = [
  't_ztautau' , 
  't_ztautau0_nofidacc',
  't_real'    , 'tss_real', 
  'tdd_qcd'   , 'tssdd_qcd', 
  't_wmu17jet', 'tss_wmu17jet', 
  't_zmu17jet', 'tss_zmu17jet',
]

#===============================================================================

def main(tt, mass):
  """
  Write the response only. Sharded version
  """
  ## 1. Prepare the adapter & reader
  # They have to be on the same adapter so that the output tree can be ref
  # simultaneously across different classifier
  adapters = {}
  for dltype in MODES:
    adapters[dltype] = adapter = new_reader_adapter(tt, mode=dltype)
    for btype in TRAINED_BKGS[tt]:
      tag  = '%s_%i_%s_%s'%(tt, mass, btype, dltype)
      wdir = os.path.join('.', tag, 'weights')
      adapter.book_reader_dir(wdir, prefix=btype)

  ## 2. Calculate the weights and attach to trees
  # try to squeeze the responses from different training into same file
  trees = preselection.load_trees_dict(tt)
  for dltype, adapter in adapters.iteritems():
    logger.info('Load the adapter: %s'%dltype)
    fout = ROOT.TFile('response_%s_%i_%s.root'%(tt,mass,dltype), 'RECREATE')
    # methods = [s+'_BDT' for s in TRAINED_BKGS[tt]]
    # methods = 'ztautau_Cuts', # debug
    for i, (tname, tree) in enumerate(trees.iteritems()):
      logger.info('Prep tree: %s [%i/%i]'%(tname, i+1, len(trees)))
      tree.title = tname
      # ## Skip unnecessary higgs
      # don't skip, it's useful for cross-checking that neighbor higgs should
      # be similar enough...
      # if 'higgs' in tname and '_'+str(mass) not in tname:
      #   continue
      # ## Fast mode, only the whitelisted
      # if tname not in ['t_higgs_%i'%mass]+FAST_LIST:
      #   continue
      tf = adapter.make_weights(tree) #, methods=methods)
      tf.name = tname
      tf.Write()

    ## Close the output file
    ROOT.gDirectory.Delete("weights;*")
    fout.Close()

  # ## Close the source file
  # ROOT.gROOT.CloseFiles()
  logger.info('Make response completed')

#===============================================================================

if __name__ == '__main__':
  ## Run with args
  parser = argparse.ArgumentParser()
  parser.add_argument('--ttype', type=str, default='h1', choices=TTYPES)
  parser.add_argument('--mass' , type=int, default=125 )
  args = parser.parse_args()
  main(args.ttype, args.mass)
