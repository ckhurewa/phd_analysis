# #!/usr/bin/env python

# from PyrootCK import *
# from PyrootCK import TMVA_Adapter

# sys.path.append(os.path.expandvars('$DIR13/identification/plot'))
# from ditau_prep import *

# import pandas as pd
# pd.options.display.width = 160

# #===============================================================================
# # CONFIG
# #===============================================================================

# # list of trained adapters
# LIST_BDT = [
#   'samesign', 
#   'ztautau', 
#   # 'wjet', 
#   # 'zjet', 
#   # 'dymu', 
#   # 'ccbar', 
#   # 'bbbar',
# ]

# export_blacklist_subitem = [
#   '_M',
#   '_PROBNNghost',
#   '_TRGHP',
#   '_TOS_ELECTRON',
#   '_TOS_MUON',
#   '_BPVIPCHI2',
#   '_0.50_cc_mult',
#   '_0.50_cc_sPT',
#   '_0.50_cc_vPT',
#   '_0.50_cc_maxPt_Q',
#   '_0.50_cc_maxPt_PT',
#   '_0.50_nc_mult',
#   '_0.50_nc_sPT',
#   '_0.50_nc_vPT',
#   '_0.50_nc_IT',
#   '_0.50_nc_maxPt_PT',
#   '_0.50_IT',
#   '_Y',
#   '_Y0',
#   '_PERR2',
# ]

# EXPORT_BLACKLIST = [
#   'nPVs', 
#   'nTracks',
#   'nSPDhits', 
#   'runNumber',
#   'eventNumber',
#   'BCID',
#   'BCType',
#   'OdinTCK',
#   'L0DUTCK',
#   'HLT1TCK',
#   'HLT2TCK',
#   'GpsTime',
#   'Polarity',
#   'StrippingDitau_EXLineDecision',
#   'StrippingDitau_EXssLineDecision',
#   'StrippingDitau_MuXLineDecision',
#   'StrippingDitau_MuXssLineDecision',
#   'StrippingWMuControl10LineDecision',
#   'StrippingWMuLowLineDecision',
#   'StrippingWeLowLineDecision',
#   'StrippingZ02MuMuLineDecision',
#   'StrippingZ02eeLineDecision',
#   'StrippingZ02eeSSLineDecision',
#   #
#   'DOCA',
#   'DR',
#   'Q'
# ] + [ pf+s for pf in prefixes[dtype] for s in export_blacklist_subitem ]

# ## fields for SS trees
# EXPORT_WHITELIST_SS = [ 'runNumber', 'eventNumber', 'diff_samesign', '_Iso', 'M' ]

# ## fields for OS trees
# EXPORT_WHITELIST_OS = [ 'runNumber', 'eventNumber', 'M', 'P', 'PT', 'PZ', 'ETA', 'Y', 'PHI' ]

# #===============================================================================

# def load_params(dt):
#   """
#   Return a list of (param, label) of cuts which should be used for TMVA study.
#   """

#   p1,p2 = prefixes[DT_TO_DTYPE[dt]]
#   l1,l2 = labels[DT_TO_DTYPE[dt]]

#   params_h3 = [
#     [ 'TMath::Log10(tau_P)'         , 'p(#tau_{h3})'                  ],
#     [ 'TMath::Log10(tau_PT)'        , 'p_{T}(#tau_{h3})'              ],
#     [ 'TMath::Log10(tau_VCHI2PDOF)' , 'Vertex #chi^{2}/dof(#tau_{h3})'],
#     [ 'tau_DRTRIOMAX'               , '#Delta R_{max}(#tau_{h3})'     ],
#     [ 'tau_0.50_cc_IT'              , 'Iso(#tau_{h3})'                ],
#     [ 'TMath::Log10(tau_BPVVD)'     , 'd_{flight}(#tau_{h3})'         ],
#     [ 'tau_BPVCORRM'                , 'm_{corr}(#tau_{h3})'           ],
#   ]

#   params_ditau = [
#     [ 'DPHI'                  , '#Delta#phi'    ],
#     [ 'APT'                   , 'A_{PT}'        ],
#     [ 'TMath::Log10(DOCACHI2)', 'DOCA #chi^{2}' ],
#   ]

#   if 'h3' in dtype:
#     params0 = [
#       [ 'TMath::Log10(%s_P)'%p1    , 'p(#tau_{%s})'%l1     ],
#       [ 'TMath::Log10(%s_PT)'%p1   , 'p_{T}(#tau_{%s})'%l1 ],
#       [ 'TMath::Log10(%s_BPVIP)'%p1, 'IP(#tau_{%s})'%l1    ],
#       [ '%s_0.50_cc_IT'%p1         , 'Iso(#tau_{%s})'%l1   ],
#     ]
#     params = params0 + params_h3 + params_ditau

#   else:
#     params0 = [
#       [ 'TMath::Log10(%s_P)'%p1    , 'p(#tau_{%s})'%l1     ],
#       [ 'TMath::Log10(%s_PT)'%p1   , 'p_{T}(#tau_{%s})'%l1 ],
#       [ 'TMath::Log10(%s_BPVIP)'%p1, 'IP(#tau_{%s})'%l1    ],
#       [ '%s_0.50_cc_IT'%p1         , 'Iso(#tau_{%s})'%l1   ],
#       #
#       [ 'TMath::Log10(%s_P)'%p2    , 'p(#tau_{%s})'%l2     ],
#       [ 'TMath::Log10(%s_PT)'%p2   , 'p_{T}(#tau_{%s})'%l2 ],
#       [ 'TMath::Log10(%s_BPVIP)'%p2, 'IP(#tau_{%s})'%l2    ],
#       [ '%s_0.50_cc_IT'%p2         , 'Iso(#tau_{%s})'%l2   ],
#     ]
#     params = params0 + params_ditau

#   return params

# #===============================================================================

# @PythonCK.decorators.memorized
# def load_adapter():
#   param   = load_params(dt)
#   adapter = TMVA_Adapter( param )
#   adapter.reader = ROOT.TMVA.Reader()
#   for name in LIST_BDT:
#     adapter.reader.BookMVA( name, 'weights/%s_h1mu_BDT.weights.xml'%name )
#   log.info('Adapters loaded.')
#   return adapter


# #===============================================================================

# def handle_prepare_normal( tname, t0, param, cut ):
#   """
#   Extract the tree, with selection cut, and save only certain branches.
#   """
#   ## Precheck, skip if this tree is empty
#   n = t0.Draw(param, cut, 'para goff')
#   if n<=0:
#     return

#   ## Extract whitelisted columns
#   df_raw = t0.sliced_dataframe()

#   ## Load the trees to be read into adapter
#   adapter = load_adapter()
#   adapter.tree = t0.CopyTree(cut)
#   log.info('Evaluating BDT: %s'%tname)
#   df_bdt  = adapter.evaluate_mva(*LIST_BDT)
#   return pd.concat([df_raw, df_bdt], axis=1)


# def handle_prepare_faking_dilepton( dtype, tname, tree, param_os ):
#   """
#   Perform the computation for tree of dilepton misid calculation.
#   - Separate into 2 steps: Preselection using rectangular cuts, and MVA BDT
#   - Work on 2 faking separately (l1,l2; l2,l1), then AND-stages, OR-faking.
#   """

#   ## Prepare the camoflage spec.  
#   if tname == 't_dimuon':
#     origin = 'mu_mu'
#   elif tname == 't_dielectron':
#     origin = 'e_e'
#   spec1 = dict(zip(prefixes[origin], prefixes[dtype]))
#   spec2 = dict(zip(prefixes[origin], reversed(prefixes[dtype])))
#   param = 'Preselection & _Ntrim'

#   acc = []
#   for i,spec in enumerate([spec1, spec2]):
#     j=i+1

#     ## 1st: Presel  
#     ditau_utils.camoflage( tree, **spec )
#     tree.Draw( param, '', 'goff')
#     df_presel = tree.sliced_dataframe()
#     df_presel.columns = ['Presel_Ntrim']
#     # collapse to valid entry, whislt temprary store the valid index
#     df_presel = df_presel[df_presel['Presel_Ntrim']==1].reset_index()

#     ## 2nd: BDT, loop over adapters.
#     t2 = tree.CopyTree(param)
#     ditau_utils.camoflage( t2, **spec )
#     adapter = load_adapter()
#     adapter.tree = t2
#     log.info('Evaluating BDT: %s'%(tname))
#     df_bdt = adapter.evaluate_mva(*LIST_BDT)
    
#     ## some validation on size
#     assert df_presel.shape[0] == df_bdt.shape[0]

#     ## Wrap & collect
#     df = pd.concat([df_presel, df_bdt], axis=1)
#     df.index = df['index']
#     del df['index']
#     del df['Presel_Ntrim']
#     df.columns = [ 'fake{}.BDT.{}'.format(j,name) for name in LIST_BDT]
#     acc.append(df)

#   ## last element, the event itself.
#   tree.Draw(param_os, '', 'para goff')
#   df_raw = tree.sliced_dataframe()

#   ## finally
#   df_bdt  = pd.concat(acc, axis=1).fillna(-1.)
#   df      = pd.concat([df_raw, df_bdt], axis=1, join='inner') # note the inner join
#   df.to_pickle('temp_%s.root'%tname)
#   return df


# #-------------------------------------------------------------------------------

# def main_HMT_prepare():
#   """
#   Isolate only the necessary trees & branch, attached the MVA data.
#   """
#   from root_numpy import array2tree

#   ## Cuts for the extraction of trees
#   cut_default   = 'Preselection & _Ntrim'
#   cut_override  = {
#     't_higgs'     : 'Preselection & _Ntrim & _TrueChanHMT',
#     'tss_higgs'   : 'Preselection & _Ntrim & _TrueChanHMT',
#     'tdd_qcd'     : 'Preselection & _Ntrim & _AntiIso',
#     'tssdd_qcd'   : 'Preselection & _Ntrim & _AntiIso',
#     't_ztautau'   : 'Presel_truechan & _Ntrim',
#     'tss_ztautau' : 'Presel_truechan & _Ntrim',
#     't_ztautau0'  : 'Presel_truechan & _Ntrim',
#     'tss_ztautau0': 'Presel_truechan & _Ntrim',
#   }

#   ## skip processing of these trees
#   list_trees_skipping = [
#     't_combine_ww_wz', 
#     't_combine_ttbar',
#     't_wtaubb', 
#     'tss_wtaubb',
#   ]

#   ## Choose between all trees, or debugging queue
#   # queues = ALLTREES
#   queues = {k:v for k,v in ALLTREES.iteritems() if k in ['t_higgs']}

#   ## extraction param
#   param_ss  = ':'.join(EXPORT_WHITELIST_SS)
#   param_os  = ':'.join(EXPORT_WHITELIST_OS)
#   fout      = ROOT.TFile('BDT_%s.root'%dt, 'recreate')
#   ftemp     = ROOT.TFile('temp.root', 'recreate')
#   ftemp.cd()

#   ## Start loop
#   for tname, t0 in sorted(queues.iteritems()):

#     ## handle special case
#     if tname in ('t_dimuon', 't_dielectron'):
#       df = handle_prepare_faking_dilepton( dtype, tname, t0, param_os )

#     ## generic case.
#     else:
#       cut   = cut_override.get( tname, cut_default )
#       param = param_ss if tname.startswith('tss') else param_os
#       df    = handle_prepare_normal( tname, t0, param, cut )
#       if df is None:
#         print 'Skip:', tname
#         continue

#     ## finally, write
#     fout.cd()
#     t = array2tree(df.to_records(index=False), name=tname)
#     t.Write() # Write clean new tree
#     ftemp.cd()
#     log.info('Written: '+tname)

#   ## Finally
#   ftemp.Close()
#   fout.Close()  


# #===============================================================================

# if __name__ == '__main__':
#   pass 

#   # main_calculate_corr()
#   # main_extract()
#   # study_mumu()

#   ## Preparation
  
#   handle_prepare_faking_dilepton( dtype, 't_dimuon', t_dimuon, 'M' )

#   # main_HMT_prepare()

