#!/usr/bin/env python

"""

"""

from PyrootCK import *
from mva_utils import DIR_EVALUATE

#===============================================================================

def _calculate(tt, spec, t_sig, t_bkg, filt=''):
  # Loads the library
  ROOT.TMVA.Tools.Instance()
  name    = 'temp'
  fout    = ROOT.TFile("assist/%s.root"%name, "RECREATE")
  fac_opt = "!V:!Silent:Transformations=I:AnalysisType=Classification"
  factory = ROOT.TMVA.Factory('factory', fout, fac_opt)

  ## Prepare loader
  loader = ROOT.TMVA.DataLoader('default')
  for param in spec:
    loader.AddVariable(param, param, '', 'F')
  loader.AddSignalTree(t_sig)
  loader.AddBackgroundTree(t_bkg)
  loader.PrepareTrainingAndTestTree(ROOT.TCut(filt), ROOT.TCut(filt),
    'SplitMode=Random:NormMode=NumEvents')

  ## Add the methods
  ctype   = ROOT.TMVA.Types.kCuts
  method  = 'Cuts'
  opts    = "!H:!V:FitMethod=MC:EffSel:VarProp=FSmart:CreateMVAPdfs=True"
  factory.BookMethod(loader, ctype, method, opts)

  ## RUN
  factory.TrainAllMethods()     # Train MVAs using the set of training events
  factory.TestAllMethods()      # Evaluate all MVAs using the set of test events
  factory.EvaluateAllMethods()  # Evaluate and compare performance of all configured MVAs

  ## Finally
  fout.Close()

#===============================================================================

def main():
  tt = 'mu'

  # spec = 'PT1', 'PT2'
  # spec = 'TMath::Log10(DOCACHI2)',
  spec = 'TMath::Log10(IP1)', 'TMath::Log10(IP2)', 'TMath::Log10(DOCACHI2)'

  filt = utils.join(
    # 'ISO1 > 0.9',
    # 'ISO2 > 0.9',
  )

  fin = ROOT.TFile(os.path.join(DIR_EVALUATE, 'evaluated_%s_125.root'%tt))
  t_sig = fin.Get('t_higgs_125')
  t_bkg = fin.Get('t_dymu')

  _calculate(tt, spec, t_sig, t_bkg, filt)

#===============================================================================

if __name__ == '__main__':
  main()