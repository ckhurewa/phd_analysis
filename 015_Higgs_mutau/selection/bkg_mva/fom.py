#!/usr/bin/env python

"""

Observed the figure of merit from the trained samples.

"""
import re
import subprocess

from mva_utils import TRAINED_BKGS, TTYPES, DIR_TRAIN_EVAL, MODES
from HMT_utils import ROOT, os, pd, deco, pickle_dataframe_nondynamic, MASSES

#===============================================================================
# Extract data from all trained root files
#===============================================================================

def parse_output(stdout):
  """
  Parse the stdout of the `mvaeffs` results into to dataframe.
  """
  pat = \
  r'---\s+'\
  r'(?P<classifier>\S+):  \(\s+\d+,\s+\d+\)\s+'\
  r'(?P<cut>\S+)\s+'\
  r'(?P<fom>\S+)\s+'\
  r'(?P<nsig>\S+)\s+'\
  r'(?P<nbkg>\S+)\s+'\
  r'(?P<esig>\S+)\s+'\
  r'(?P<ebkg>\S+)'
  df = pd.DataFrame(m.groupdict() for m in re.finditer(pat, stdout))
  df = df.set_index('classifier').applymap(float)
  return df


@deco.concurrent
def fetch_single(tag, fpath):
  """
  Run over one trained file, return the dataframe of results
  """
  ## PyROOT version cannot handle batch
  # arg    = 'ROOT.TMVA.mvaeffs("%s", "%s", True, "S/(1+sqrt(B))")'%(tag, fpath)
  # args   = 'python', '-c', 'import ROOT; ROOT.gROOT.SetBatch(True); %s'%arg
  # output = subprocess.check_output(args)
  ## Assert existence first
  print 'Loading: fpath=%-30s, tag=%20s'%(tag, fpath)
  assert os.path.exists(fpath), "Not existed: %s"%fpath
  ## Load it
  args   = '$ROOTSYS/bin/root', '-q', '-b', "$DIR15/selection/bkg_mva/fom_punzi.cpp(\"%s\",\"%s\")"%(tag, fpath)
  args   = [os.path.expandvars(arg) for arg in args]
  output = subprocess.check_output(args)
  return parse_output(output)


@deco.synchronized
def fetch_sync():
  ## chdir because it produces the histo files
  print 'Move to dir:', DIR_TRAIN_EVAL
  os.chdir(DIR_TRAIN_EVAL)
  acc = {}
  # for tt in TTYPES:
  for tt in ['e']:
    # for mass in MASSES:
    for mass in [45,]:
      for bkg in TRAINED_BKGS[tt]:
        for mode in MODES:
          key   = tt, mass, bkg, mode
          tag   = '%s_%i_%s_%s'%key
          fpath = 'train_%s_%i_%s.root'%(tt, mass, bkg)
          acc[key] = fetch_single(tag, fpath)
  return pd.concat(acc)

# @pickle_dataframe_nondynamic
def fetch_all():
  ## Loop over all & return
  df = fetch_sync()
  df.index.names = 'tt', 'mass', 'bkg', 'mode', 'classifier'
  return df

#===============================================================================
# Representation
#===============================================================================

def BDT():
  """
  Show the locations of punzi-optimized threshold, for each channels and bkgs
  """
  print fetch_all().xs('BDT', level=2)


#===============================================================================

if __name__ == '__main__':
  pass
  # print fetch_single('e_45_data_ss_lite', 'train_e_45_data_ss.root')
  # print fetch_all()
  # BDT()
