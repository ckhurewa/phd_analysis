#!/usr/bin/env python
"""

Suitable for plotting the comparison gallery based on un-selected tree.

"""

from PyrootCK import *
from PyrootCK.QHist.CoreV3 import QHist as QHistV3
from copy import deepcopy

## Base tools
sys.path.append(os.path.expandvars('$DIR15'))
from HMT_utils import (
  PREFIXES, LABELS, DT_TO_TT, TTYPES, ROOTLABELS, 
  memorized, TREENAME_TO_ROOTLABELS, Br_tau,
)

## Handle to preselected trees
import preselection

#===============================================================================

def skip_except_h3(func):
  """
  Helper decorator to skip tauh3 channel
  """
  def wrap(tt):
    if tt != 'h3':
      print 'Only for h3, skip'
      return
    return func(tt)
  return wrap

@memorized
def hmt_trees(tt):
  """
  Shortcut to (isolate) import of deck of trees
  """
  trees  = preselection.load_trees_dict(tt)
  masses = 45, 85, 125, 195
  # masses = 125,
  acc    = []
  for m in masses:
    tree = trees['t_higgs_%i'%m]
    tree.title = 'm_{H} %i GeV/c^{2}'%m
    acc.append(tree)
  return acc

@memorized
def bkg_trees(tt):
  trees0 = preselection.load_trees_dict(tt)
  trees  = [
    trees0['t_ztautau'],
    trees0['t_cc_hardmu'],
    trees0['t_dymu' if tt=='mu' else 't_wmu17jet'],
  ]
  for tree in trees:
    tree.title = TREENAME_TO_ROOTLABELS[tree.title]
  return trees

def trees_queues(tt):
  yield 'sig', hmt_trees(tt)
  yield 'bkg', bkg_trees(tt)

#-------------------------------------------------------------------------------

def new_postproc(x1=0.70, y2=0.96):
  """
  Return new postprocessor, flexible in the legend x-position.
  """
  def postproc(h):
    c = h.anchors.canvas
    c.canvasSize = c.ww, int(c.wh*0.9)
    c.Update()
    h.anchors.stack.xaxis.noExponent = True
    h.anchors.stack.yaxis.noExponent = True
    # h.anchors.stack.xaxis.labelSize *= 0.9
    # h.anchors.stack.yaxis.labelSize *= 0.8
    leg = h.anchors.legend
    leg.header = 'LHCb 8 TeV'
    leg.x1NDC  = x1
    leg.x2NDC  = x1+0.24
    leg.y1NDC  = y2 - 0.06*leg.nRows
    leg.y2NDC  = y2
    ROOT.gPad.topMargin   *= 0.3
    ROOT.gPad.leftMargin  *= 0.7
    ROOT.gPad.rightMargin *= 0.6
    ROOT.gPad.Update()
  return postproc

def new_normalizers(tt):
  br = Br_tau()[tt].n
  egen2 = {
    'e' : 14256./33377,
    'h1': 23520./94796,
    'h3': 12044./28434,
    'mu':  7114./16376,
  }[tt]
  nmz_wjet  = lambda n: n/10015768. * 1976. * 1.32e4 * 0.236
  nmz_zmumu = lambda n: n/20571464. * 1976. * 4466 * 0.391
  return [
    # via JID 4069
    #         esel          lumi    csc    egen1      brtau  egen2
    lambda n: n/  120068. * 1976. * 865. * 1113./8051 * br * egen2,
    lambda n: n/ 1167321. * 1976. * 1.64e6 * 1.78e-4,
    (nmz_zmumu if tt=='mu' else nmz_wjet),
  ]

#===============================================================================

def gallery0_pt(tt):
  ## main template
  H0 = QHistV3(prefix=tt, auto_name=True, xbin=30, xlog=True, xmin=5, xmax=300)
  H0.postproc  = new_postproc(0.71)
  for suffix, trees in trees_queues(tt):
    if suffix=='bkg':
      H0.normalize = new_normalizers(tt)
      if tt=='mu':
        H0.ylog = True
    H = H0(trees=trees, suffix=suffix)
    H(params='PT1', xlabel='p_{T}(%s) [GeV/c]'%ROOTLABELS[tt][0]).draw()
    H(params='PT2', xlabel='p_{T}(%s) [GeV/c]'%ROOTLABELS[tt][1]).draw()

#===============================================================================

def gallery2_iso(tt):
  """
  Unlike Ztautau, apply ISO first as it's super discriminant.
  """
  left = new_postproc(0.13)
  bottom = new_postproc(x1=0.4, y2=0.5)

  H0 = QHistV3(prefix=tt, auto_name=True, xbin=25)
  for suffix, trees in trees_queues(tt):
    if suffix=='bkg':
      H0.normalize = new_normalizers(tt)

    H = H0(trees=trees, suffix=suffix)
    for i in xrange(2):
      j = i+1
      h = H(params='ISO%i'%j, xmin=0, xmax=1.1, postproc=left)
      h.xlabel = '#hat{I}_{pT}(%s)'%ROOTLABELS[tt][i]
      h.draw()
      #
      h = H(xmin=1e-1, xmax=100, xlog=True, ylog=True, postproc=bottom)
      h.name   = 'vISO%i'%j
      h.params = 'cc_vPT%i/1e3+1e-1'%j
      h.xlabel = 'I_{pT}(%s)'%ROOTLABELS[tt][i]
      h.draw()

filt2 = {
  'e' : ['ISO1 > 0.9', 'ISO2 > 0.9'],
  'h1': ['ISO1 > 0.9', 'ISO2 > 0.9'],
  'h3': ['ISO1 > 0.9', 'ISO2 > 0.9'],
  'mu': ['ISO1 > 0.9', 'ISO2 > 0.9'],
}

#===============================================================================

@skip_except_h3
def gallery1_h3_quality(tt):
  H0 = QHistV3(auto_name=True, xbin=20, postproc=new_postproc(0.13))
  for suffix, trees in trees_queues(tt):
    if suffix=='bkg':
      H0.normalize = new_normalizers(tt)
      H0.filters = filt2[tt]

    H = H0(trees=trees, suffix=suffix, xlog=True)
    H(params='tau_VCHI2PDOF', xmax=20, xmin=1e-2, xlabel='Vertex #chi^{2}/ndf').draw()
    H(params='tau_DRoPT', xmax=1e-1  , xmin=1.1e-4, xlabel='#DeltaR_{max}/p_{T} [GeV^{-1}]').draw()


filt1 = deepcopy(filt2)
filt1['h3'] += ['tau_VCHI2PDOF < 9', 'tau_DRoPT < 0.01']

#===============================================================================

@skip_except_h3
def gallery3_displ_h3(tt):
  H0 = QHistV3(auto_name=True, xbin=20)
  for suffix, trees in trees_queues(tt):
    if suffix=='bkg':
      H0.normalize = new_normalizers(tt)
      H0.filters = filt1[tt]
    
    H = H0(trees=trees, suffix=suffix)
    h = H(params='tau_BPVLTIME*1E6', xlog=True, xmin=1, xmax=5e3)
    h.xlabel   = 'Decay time [fs]'
    h.postproc = new_postproc(0.13)
    h.draw()
    #
    h = H(params='tau_BPVCORRM/1e3', xlog=False, xmin=0, xmax=5)
    h.xlabel   = 'm_{corr} [GeV/c^{2}]'
    h.postproc = new_postproc(0.71)
    h.draw()

filt3a = deepcopy(filt1)
filt3a['h3'] += ['tau_BPVLTIME*1e6', 'tau_BPVCORRM/1e3']

#===============================================================================

def gallery3_displ(tt):
  H0 = QHistV3(prefix=tt, auto_name=True, xmin=2e-3, xmax=1, xbin=20, xlog=True)
  H0.postproc = new_postproc(0.71)
  for suffix, trees in trees_queues(tt):
    if suffix=='bkg':
      H0.normalize = new_normalizers(tt)
      H0.filters = filt3a[tt]

    H = H0(trees=trees, suffix=suffix)
    H(params='IP1', xlabel='IP(%s)'%ROOTLABELS[tt][0]).draw()
    H(params='IP2', xlabel='IP(%s)'%ROOTLABELS[tt][1]).draw()

filt3b = deepcopy(filt3a)
filt3b = {k:filt3b[k]+l for k,l in {
  'e' : ['IP1 > 0', 'IP1 < 0.05', 'IP2 > 0.01'],
  'h1': ['IP1 > 0', 'IP1 < 0.05', 'IP2 > 0.01'],
  'h3': ['IP1 > 0', 'IP1 < 0.05'],
  'mu': ['IP1 > 0', 'IP1 < 0.05', 'IP2 > 0.05'],
}.iteritems()}

#===============================================================================

def gallery4_ditau(tt):
  left = new_postproc(0.13)
  right = new_postproc(0.71)

  H0 = QHistV3(prefix=tt, auto_name=True)
  for suffix, trees in trees_queues(tt):
    if suffix=='bkg':
      H0.normalize = new_normalizers(tt)
      H0.filters = filt3b[tt]

    H = H0(trees=trees, suffix=suffix)
    H(params='APT' , xmin=0, xmax=1.0, xbin=20, postproc=right, xlabel='A_{PT}').draw()
    H(params='DPHI', xmin=0, xmax=3.4, xbin=20, postproc=left, xlabel='\Delta\phi [rad]').draw()
    # H(params='DPT', xmin=-60,xmax=120, xbin=30, postproc=left, xlabel='#Delta_{PT}').draw()

#===============================================================================

def draw_dr_pt():
  tt = 'h3'
  trees = hmt_trees(tt) + bkg_trees(tt)

  def postpc(h):
    h.upperPad.cd()
     # --mask-poly 12,0.05 50,0.25 50,0.5 12,0.5
    x = [12  , 50 , 12 , 12  ]
    y = [0.12, 0.5, 0.5, 0.12]
    box = ROOT.TPolyLine(len(x), x, y)
    box.ownership = False
    box.fillColorAlpha = ROOT.kBlack, 0.3
    box.Draw('f')
    h.box = box
    #
    leg = ROOT.TLegend(0.6, 0.65, 0.88, 0.92)
    for x in h:
      leg.AddEntry(x, x.title, 'l')
    leg.Draw()
    leg.fillStyle = 0
    h.leg = leg

  ## Show corr: before & after
  h = QHist()
  h.auto_name = True
  h.batch   = True
  h.trees   = trees
  h.params  = 'tau_DRTRIOMAX:tau_PT/1000'
  h.options = 'cont3'
  h.xbin    = 10
  h.ybin    = 10
  h.zbin    = 4
  h.xmin    = 12
  h.xmax    = 50
  h.ymax    = 0.5
  h.ymin    = 4e-2
  h.xlog    = True 
  h.ylog    = True
  h.xlabel  = 'p_{T} [GeV/c]'
  h.ylabel  = '#DeltaR_{max}'
  h.draw(postpc=postpc)

#===============================================================================

if __name__ == '__main__':
  ROOT.gROOT.batch = True
  ROOT.gROOT.ProcessLine(".L lhcbstyle.C")
  set_qhist_color_palette()

  if '--redraw' in sys.argv:
    ## Draw all
    for tt in TTYPES:
      gallery0_pt(tt)
      # gallery2_iso(tt)
      # gallery1_h3_quality(tt)
      # gallery3_displ_h3(tt)
      # gallery3_displ(tt)
      # gallery4_ditau(tt)
      ROOT.gROOT.CloseFiles()
    sys.exit()

  ## DEV
  tt = 'mu'
  # print hmt_trees(tt)
  # print bkg_trees(tt)
  gallery0_pt(tt)
  # gallery2_iso(tt)
  # gallery1_h3_quality(tt)
  # gallery3_displ_h3(tt)
  # gallery3_displ(tt)
  # gallery4_ditau(tt)
  # draw_dr_pt()
