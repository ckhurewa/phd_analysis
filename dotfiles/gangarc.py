#=======================================================================

[DIRAC]
DiracLFNBase = /lhcb/user/c/ckhurewa
# ReplicateOutputData = True
# Timeout = 604800

#=======================================================================

[Display]
jobs_columns = ('fqid', 'status', 'name', 'subjobs', 'application', 'backend', 'comment')
jobs_columns_width = {'fqid': 5, 'status': 10, 'name': 18, 'application': 11, 'comment': 60, 'subjobs': 7, 'backend': 9}

#=======================================================================

[Output]

## For LPHE-PBS client only
MassStorageFile = {'defaultProtocol': 'root://eoslhcb.cern.ch',   'fileExtensions': [''],   'uploadOptions': {    'path': '/eos/lhcb/user/c/ckhurewa/ganga',     'cp_cmd': 'eos cp',     'ls_cmd': 'eos ls',     'mkdir_cmd': 'eos mkdir'  },   'backendPostprocess': {'PBS': 'WN', 'Dirac': 'client'}}


#=======================================================================
#  internal PBS command line interface
[PBS]

heartbeat_frequency = 15
jobid_name = SLURM_JOB_ID
jobnameopt = J
kill_res_pattern = (Job <\d+> exceed virtual memory limit )|(^scancel: error: Kill job error on job id \d+: Invalid job id specified)|(Force Terminated job <\d+>)
kill_str = scancel %s

queue_name = SBATCH_PARTITION
submit_res_pattern = ^Submitted (?P<queue>\S*) job (?P<id>\d*)
submit_str = cd %s; sbatch %s %s %s %s
timeout = 300

postexecute = 
  import os
  import re
  import subprocess
  ### Remove scratch
  jid=os.environ["SLURM_JOB_ID"]
  user=os.environ['SLURM_JOB_USER']
  dest='/scratch/{}/{}/'.format(user,jid)
  os.chdir('/scratch')
  os.system('rm -rf %s'%dest)

preexecute = 
  import re
  import os
  import subprocess
  ### Change dir
  orig=os.getcwd()
  jid=os.environ["SLURM_JOB_ID"]
  user=os.environ['SLURM_JOB_USER']
  host=os.environ['SLURM_SUBMIT_HOST']
  dest='/scratch/{user}/{jid}/'.format(**locals())
  os.makedirs(dest)
  os.chdir(dest)
  os.system('cp %s/* .'%orig)
  ### Prepare scp host
  scphost = '{user}@{host}.epfl.ch'.format(**locals())
  ### Link GangaDiracEnv
  gdenv = os.environ['GANGADIRACENVIRONMENT']
  subprocess.call(['scp', scphost+':'+gdenv, gdenv])
  
