
# 
# Main trigger to bind all configurations to appropriate location.
# By default, replace any existing.
# 

## Default shell personalization
ln -sf ${PWD}/login.tcsh ~/.login

## (unused) tcshrc
ln -sf ${PWD}/tcshrc ~/.tcshrc

## Screen
ln -sf ${PWD}/screenrc ~/.screenrc

## Ganga's startup config
ln -sf ${PWD}/ganga.py ~/.ganga.py
ln -sf ${PWD}/gangarc.py ~/.gangarc

## Local GIT config
ln -sf ${PWD}/gitconfig ~/.gitconfig

## GIT ignore
ln -sf ${PWD}/gitignore_global ~/.gitignore_global

## Python startup file
ln -sf ${PWD}/pythonstartup ~/.pythonstartup

## Dropbox shell
ln -sf ${PWD}/dropbox_uploader ~/.dropbox_uploader

echo bind completed!

## Misc cleanup
rm -f ~/.viminfo
rm -f ~/.sqlite_history
rm -f ~/.root_hist
rm -f ~/.rootauthrc
rm -f ~/.pythonhistory
rm -rf ~/.pip
rm -rf ~/.ostap
rm -rf ~/.mozilla
rm -rf ~/.mg5 
rm -rf ~/.matplotlib
rm -f ~/.lesshst
rm -f ~/.dropshell_history
rm -f ~/.eos_history
rm -f ~/.distlib
rm -f ~/.bender_history*
rm -f ~/.bash_history
rm -f ~/.apdisk

echo clean completed!
