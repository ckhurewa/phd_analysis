
if ( $?VO_LHCB_SW_DIR ) then
    source ${VO_LHCB_SW_DIR}/lib/group_login.csh 
else
    source /cvmfs/lhcb.cern.ch/lib/group_login.csh
endif


### PATH
setenv PATH ${PATH}:$HOME/bin
setenv PATH ${PATH}:$HOME/.local/bin
setenv PATH ${PATH}:$HOME/mylib/PythonCK/scripts
setenv PATH ${PATH}:$HOME/mylib/PyrootCK/scripts
setenv PATH ${PATH}:$HOME/mylib/BenderCK/scripts
setenv PATH ${PATH}:$HOME/mylib/GangaCK/scripts
set fignore=(.pyc)  # Exclusion from executable

# local fallback
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:$HOME/.local/lib64

### PYTHONPATH
setenv PYTHONSTARTUP $HOME/.pythonstartup
setenv PYTHONPATH ${PYTHONPATH}:$HOME/mylib/PythonCK
setenv PYTHONPATH ${PYTHONPATH}:$HOME/mylib/pyroot_zen
setenv PYTHONPATH ${PYTHONPATH}:$HOME/mylib/GangaCK
setenv PYTHONPATH ${PYTHONPATH}:$HOME/mylib/PyrootCK
setenv PYTHONPATH ${PYTHONPATH}:$HOME/mylib/PyPythia8/python
setenv PYTHONPATH ${PYTHONPATH}:$HOME/mylib/BenderCK
setenv PYTHONPATH ${PYTHONPATH}:$HOME/analysis/902_fullsim_resources 


## For LHCB_future branches
# setenv CMTPROJECTPATH /home/khurewat/cmtuser/hackathon:${CMTPROJECTPATH}
# setenv CMTPROJECTPATH /home/khurewat/cmtuser/hackathon:/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-future/191:$CMTPROJECTPATH
# setenv CMTPROJECTPATH /home/khurewat/cmtuser/future:$CMTPROJECTPATH
# setenv CMTPROJECTPATH /home/khurewat/cmtuser/future:/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-future/191:$CMTPROJECTPATH
# setenv CMTPROJECTPATH /cvmfs/lhcbdev.cern.ch/nightlies/lhcb-future/Today:$CMTPROJECTPATH

# setenv CMAKE_PREFIX_PATH /home/khurewat/cmtuser/lhcb-head:/cvmfs/lhcbdev.cern.ch/nightlies:/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/Today:${CMAKE_PREFIX_PATH}
# setenv CMTPROJECTPATH /home/khurewat/cmtuser/lhcb-head:/cvmfs/lhcbdev.cern.ch/nightlies:/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/Today:$CMTPROJECTPATH
setenv CCACHE_DIR ~/.ccache
setenv CMAKEFLAGS -DCMAKE_USE_CCACHE=ON
setenv VERBOSE 1

# setenv PATH ${PATH}:/cvmfs/lhcb.cern.ch/lib/contrib/ninja/1.4.0/x86_64-slc6


## CDPATH for quick cd
set cdpath = ($cdpath ~/mylib)

## Main shared-library borrowing from LCG
# Use 79, because 84 is too new for xrootd & EOS
# setenv MYLCGSYS $LCGCMT_HOME/../../LCG_79  # LCGCMT_HOME has gone
# setenv MYLCGSYS $LCG_external_area/../releases/LCG_79
setenv LCGREL /cvmfs/lhcb.cern.ch/lib/lcg/releases
# setenv MYLCGSYS /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87


## TEMP for working on private decfile
# setenv DECFILESDUMMY $HOME/PhD_Analysis/009_bbtautau_generation/H2AA

## Rationale about ~/.local
# http://askubuntu.com/questions/14535/whats-the-local-folder-for-in-my-home-directory

## If -ldpython2.7 is missing...
# setenv LDFLAGS -L/cvmfs/lhcb.cern.ch/lib/lcg/releases/Python/2.7.6-31787/x86_64-slc6-gcc48-opt/lib/
# setenv LDFLAGS -L${LCGREL}/LCG_87/Python/2.7.10/x86_64-slc6-gcc49-opt/lib/

# default editor
setenv EDITOR /usr/bin/nano

## Static grid proxy instead of /tmp
setenv X509_USER_PROXY $HOME/.globus/grid.proxy
setenv KRB5CCNAME $HOME/.globus/krb5.proxy
setenv GETPACK_USER 'ckhurewa'

## For PyrootCK
setenv TFILEPATH ~/gangadir/workspace/$USER/LocalXML:/panfs/khurewat/ganga_storage

## Patch needs from tcsh+Bender
# https://groups.cern.ch/group/lhcb-davinci/Lists/Archive/DispForm.aspx?ID=18817
setenv GCCXML_config_version

## Use all core
setenv MAKEFLAGS "-j 8"

#————————#
# PYTHIA #
#————————#

## Use by PyPythia8
setenv PYTHIA8SYS ${LCGREL}/MCGenerators/pythia8/186-b1d4c/${CMTCONFIG}
setenv PYTHIA8DATA ${PYTHIA8SYS}/xmldoc


#———————#
# BOOST #
#———————#

## Use by PyPythia8, use name BOOST_ROOT to compat with CMake
# setenv BOOST_ROOT ${MYLCGSYS}/Boost/1.55.0_python2.7/${CMTCONFIG}
setenv BOOST_ROOT ${LCGREL}/Boost/1.62.0-83c69/${CMTCONFIG}
setenv BOOST_INCLUDEDIR $BOOST_ROOT/include/boost-1_62

#——————#
# ROOT #
#——————#

# For ROOT: See SetupProject root
# setenv ROOTSYS $LCG_external_area/../releases/ROOT/6.02.03-25a9d/$CMTCONFIG/
# setenv ROOTSYS /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/ROOT/6.02.08/$CMTCONFIG
# setenv ROOTVER `ls -t $MYLCGSYS/ROOT | head -1`

setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${LCGREL}/gcc/4.9.3/x86_64-slc6/lib64
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${LCGREL}/GSL/2.1-36ee5/$CMTCONFIG/lib
setenv ROOTSYS ${LCGREL}/ROOT/6.08.02-99084/$CMTCONFIG
setenv PATH ${PATH}:$ROOTSYS/bin
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:$ROOTSYS/lib
setenv PYTHONPATH ${PYTHONPATH}:$ROOTSYS/lib
alias root0 $ROOTSYS/bin/root -l


## newer GCC for root_numpy
# setenv PATH ${LCGREL}/LCG_87/gcc/4.9.3/x86_64-slc6/bin/:${PATH}

#————————#
# LHAPDF #
#————————#

## TODO: Try this, very active: /cvmfs/sft.cern.ch/lcg/releases/MCGenerators

## Helper for PyPythia8+CMake, use during install
setenv LHAPDF_ROOT ${LCGREL}/MCGenerators/lhapdf/6.1.4-86274/${CMTCONFIG}

## Runtime dependency
# https://lhapdf.hepforge.org/
setenv LHAPDF_DATA_PATH /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${LHAPDF_ROOT}/lib


### For FASTJET
#setenv FASTJET ${HOME}/Generators/FASTJET3.0.2
#setenv PATH ${PATH}:${FASTJET}/bin
#setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${FASTJET}/lib

# #----------#
# # MADGRAPH #
# #----------#

# setenv PATH ${PATH}:$HOME/lib/MG5_aMC_v2_1_1/bin

#--------#
# GOLANG #
#--------#

setenv GOROOT $HOME/lib/go
setenv GOPATH $HOME/mylib/golang
setenv PATH ${PATH}:$GOROOT/bin
setenv PATH ${PATH}:$GOPATH/bin

#----------#
# VALGRIND #
#----------#

setenv PATH ${PATH}:$HOME/lib/valgrind-3.10.1/bin

#------------#
# KERBEROS 5 #
#------------#

setenv KRB5SYS ${HOME}/lib/krb5-1.13.2/usr/local
setenv PATH ${PATH}:${KRB5SYS}/bin
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${KRB5SYS}/lib
setenv KRB5_CONFIG ${HOME}/.ssh/cern.krb5.conf

#------------#
# XROOTD-EOS #
#------------#

## Stolen from /afs/cern.ch/project/eos/installation/0.3.15/bin
## Stolen from lxplus:/usr/bin/eos (check version with --version)
# ganga's
# setenv XROOTDSYS /cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.2.7/$CMTCONFIG
# setenv XROOTDSYS $MYLCGSYS/xrootd/3.3.6/$CMTCONFIG
# setenv XROOTDVER `ls -t $MYLCGSYS/xrootd | head -1`
# setenv XROOTDSYS $MYLCGSYS/xrootd/3.3.6/x86_64-slc6-gcc48-opt
# setenv XROOTDSYS $MYLCGSYS/xrootd/$XROOTDVER/$CMTCONFIG

setenv XROOTDSYS ${LCGREL}/xrootd/4.4.1-752cf/$CMTCONFIG
setenv LD_LIBRARY_PATH ${XROOTDSYS}/lib64:${LD_LIBRARY_PATH} # dependency
setenv PATH ${PATH}:${XROOTDSYS}/bin
setenv PATH ${PATH}:${HOME}/bin/eoslhcb  

## EOS-LHCb ( use simlink too to let Ganga pickup correct exe)
setenv EOS_HOME /eos/lhcb/user/c/ckhurewa
setenv EOS_MGM_URL root://eoslhcb.cern.ch
# shortcut 
setenv EH ${EOS_MGM_URL}/${EOS_HOME}


# alias eos1 'setenv EOS_HOME /eos/lhcb/user/c/ckhurewa && printf $EOS_HOME > .eos_pwd && eos-0.3.15'
# alias eos2 'setenv EOS_HOME /eos/user/c/ckhurewa && printf $EOS_HOME > .eos_pwd && eos-0.3.84-aquamarine.user root://eosuser.cern.ch'
# alias eos eos1  # Default to use LHCb's space


#===============================================================================
# ALIASES 
#===============================================================================

#------------#
# BASIC BASH #
#------------#

alias pl 'nano ~/.login'
alias sl 'source ~/.login'
alias l ls -lav
alias cd 'cd \!:1*; ls -v'   #  * for optional args
alias grep 'grep --color=auto --exclude-dir=.svn'
alias k 'killall -9'
alias cx 'chmod a+x'
alias psk 'ps -ef | grep khurewat'
# alias gitlog 'git log --pretty=oneline'
alias p 'python'
alias m 'more'
alias d 'du -bsh'
alias g "grep -I \!:1 -r ."

alias ga 'git add . -n'
alias gb 'git branch -v'
alias gc 'git checkout'
alias gd 'git diff .'
alias gr 'git remote -v'
alias gitclean 'git ls-files --ignored --exclude-standard | xargs git rm --cached'

alias clean 'rm -vf .bender_history*; rm -vf .root_hist; rm -vf .eos_history; rm -vf .bash_history; rm -vf .pythonhistory; rm -vf *.pyc; rm -vf *.out; rm -vf ._*;'

### Screen 
alias sc 'screen tcsh -l'
alias scl 'screen -ls'
alias scr 'screen -r'
alias scd 'screen -d'

### Quick cd upward
alias .. 'cd ..'
alias ... 'cd ../..'
alias .... 'cd ../../..'
alias ..... 'cd ../../../..'

# Set MATH alias - takes an arithmetic assignment statement
# as argument, e.g., newvar = var1 + var2
# Separate all items and operators in the expression with blanks
alias MATH 'set \!:1 = `echo "\!:3-$" | bc -l`'

alias cpyc "find . -name '*.pyc' -delete"

## Python profiling
alias kprof 'kernprof.py -l -v'

## SVn diff in color
alias svndiff "svn diff \!:1 | vim -R -"

#-----------#
# WORKSPACE #
#-----------#

setenv HOMEP /panfs/khurewat ### PANASAS
setenv GANGADIR $HOME/gangadir
setenv DIR09 $HOME/analysis/009_bbtautau_generation
setenv DIR11 $HOME/analysis/011_StrippingDitau
setenv DIR12 $HOME/analysis/012_Hlt2Ditau
setenv DIR13 $HOME/analysis/013_Tau_Identification
setenv DIR15 $HOME/analysis/015_Higgs_mutau
setenv DIR16 $HOME/analysis/016_stripdoc

### IOCHAIN
# setenv SIMCOND $HOME/analysis/902_fullsim_resources
# setenv IOCHAINROOT $HOME/analysis/902_fullsim_resources


alias wdp 'eval "if (! \!*) then \\
  cd $HOMEP \\
else \\
  cd $HOMEP/ganga_storage/\!* \\
endif"'
alias wdp0 'cd `(ls -t -d $HOMEP/ganga_storage/*/ | head -n 1)`'

## Ganga jumper
alias wdg  'cd $GANGADIR/workspace/khurewat/LocalXML/\!:1*'
alias wdg0 'cd `(ls -rv -d $GANGADIR/workspace/khurewat/LocalXML/*/ | head -n 1)`'
alias wdr 'source ~/bin/f_wdr.tcsh'  ## Jump to Ganga's repo


# alias wdgp cd $HOMEP/gangadir/workspace/khurewat/LocalXML
alias wdt  cd $HOME/mylib/PythonCK/tests

alias wd2  cd $HOME/analysis/002_MyAnalysis_Z0/v3/
alias wd5  cd $HOME/analysis/005_ParticleGun/p8jp_v2
alias wd7  cd $HOME/analysis/007_bbtautau_CMS_LHCb/v3
alias wd9  cd $DIR09/H2AA
alias wd10 cd $HOME/analysis/010_Ztautau
alias wd11 cd $DIR11/S21rXp1
alias wd12 cd $DIR12
alias wd13 cd $DIR13
alias wd15 cd $DIR15
alias wd16 cd $DIR16

alias wd  cd ~/temp/stripping-nightlies  # Most active 
alias wds cd '$HOME/cmtuser/DaVinci_v39r0p1/Phys/StrippingSelections/tests/liaisons/QEE'  
alias wdf cd '~/cmtuser/future/CaloReco/BrunelDev_future'


# Hack for WD12 dev
# setenv PYTHONPATH ${DIR12}:${PYTHONPATH}

#------#
# LPHE #
#------#

alias sq      'watch -d -n 2 gsqueue'
alias sqq     'watch -d -n 2 squeue -l'
alias sqo     'squeue -o "%.18i %.9P %.8u %.2t %.10M %.6D %R %m %c %C %D"'
alias sqa     'squeue -o "%all"'
alias sdebug  'srun --mem=2048 -A debug -p debug --pty tcsh -l' 
# alias qsub    'sbatch -N 1 -n 1 -c 1 -A batch -p batch --mem=8000 -t 2-0:0:0 -o job.out -e job.err --wrap="srun \!*"'
alias TMVAGui '$ROOTSYS/bin/root -l $ROOTSYS/tmva/test/TMVAGui.C\(\"\!:1\"\)'
alias sshlx   'ssh -Y lxplus'
alias root    '~/mylib/PyrootCK/scripts/root_browse.py'
alias q       'quota -s; eos quota | grep ^ckhurewa -B2'

alias me 'eval "if ( \!* == '' ) then \\
  more output/stderr \\
else \\
  more \!*/output/stderr \\
endif"'
alias mo 'eval "if ( \!* == '' ) then \\
  more output/stdout \\
else \\
  more \!*/output/stdout \\
endif"'


alias stcsh 'eval "if (! \!*) then \\
  srun --mem=5000 -t 2-0:0:0 --pty tcsh -l \\
else  \\
  srun --mem=\!* -t 2-0:0:0 --pty tcsh -l \\
endif"'

## MUST BE CALLED only from ./jid/output/
## Usage: eosgpush tuple.root
alias eosgpush 'eoscp -v \!:1 ${EOS_MGM_URL}/${EOS_HOME}/ganga/`dirname ${PWD} | xargs basename`/\!:1'

#------#
# LHCb #
#------#

alias ss 'svn status -u'

## Setup Projects
## PATCHING; only PythonCK lost in PATH, strange!!
# Cannot have comment after too. It breakdown in subshell (like stcsh).
alias SetupProject 'source `which SetupProject.csh` \!*; source ~/bin/set_prompt.csh; setenv PATH ${PATH}:$HOME/mylib/PythonCK/scripts'
alias sd  'SetupProject DaVinci v39r0p1'
alias sgs 'SetupProject Gauss'
# alias sb  'SetupProject Bender v29r5' # v25r7, v28r3
alias sb  'lb-run Bender/v29r4 bash'
alias sp  'SetupProject Panoramix'
alias sm  'SetupProject Moore v23r0 --runtime-project=Hlt v23r0'
alias sg  'source ~/bin/setup_ganga.tcsh'
alias sld 'lb-run LHCbDirac/prod bash'
# alias sld 'SetupProject LHCbDirac'
# alias sg  'LbLogin -c x86_64-slc6-gcc48-opt; SetupProject Ganga v600r44; setenv LD_LIBRARY_PATH $XROOTDSYS/lib64:${LD_LIBRARY_PATH}'

# alias de 'dst-explorer root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/'

## Ganga
alias cgc 'rm -f ${GANGADIR}/repository/khurewat/LocalXML/6.0/sessions/*'
# alias gangap 'ganga --option="[Configuration]gangadir=/panfs/khurewat/gangadir"'

# Bender
alias ibender 'ipython -i $BENDERROOT/python/Bender/MainMC.py'

alias kc 'kinit ckhurewa@CERN.CH'

## LHCbDirac shortcuts
alias qd 'lb-run LHCbDirac prod dirac-dms-storage-usage-summary --Users=ckhurewa'
alias url 'lb-run LHCbDirac prod dirac-dms-lfn-accessURL'

## Force longer timeout
alias lhcb-proxy-init 'lhcb-proxy-init -v 168:00'


## Gaudi future
alias brdump '~/cmtuser/BrunelDev_future/run gaudirun.py -n -o dump.opts'

test -e "${HOME}/.iterm2_shell_integration.tcsh" && source "${HOME}/.iterm2_shell_integration.tcsh"
