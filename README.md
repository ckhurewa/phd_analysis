Z->tautau and Higgs->MuTau at 8TeV
==================================

The analysis code repository for both Z->tautau 
([LHCb-ANA-2016-054](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/Ztautau8TeV))
and Higgs->MuTau 
([LHCb-ANA-2017-035](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/HiggsMuTau8TeV)) analyses. The sources from two projects
are highly intertwined so they are placed in the same repo.


Important files/folders
-----------------------

~~~
file/dir                    Description
--------------------------- ----------------------------------------------------
013_Tau_Identification/     Entire Z -> tau tau 8TeV analysis.
  - decorators.py           Collection of decorators.
  - ditau_utils.py          Centralized constants & imporant functions.
  - drawers.py              Centralized drawing functions for consistent style.
  - efficiencies/           All efficiencies calculations.
    - acceptance.py         Visualize acceptance at 4-vec level using Pythia8.
    - purity.py             Compute signal purity/cross-feed.
    - nlo-acceptance/       Ganga job & compute NLO acceptance using POWHEG-BOX.
    - reconstruction/       Reconstruction efficiency modules.
    - selection/            Compute offline selection eff with corrections & systematics.
      - sel.py              Main handle combining correction & syst from each vars..
    - sfarry/               Muon reco efficiencies result from Steve+Michel.
  - generation/             Gauss (DECFILE) script to generate MC samples.
  - identification/         All signal & background offline selection.
    - DV-bender/            Bender script to make ntuples of candidates.
    - plot/                 Compute numbers of expected backgrounds/signal.
      - bkg_context.py      Compute misc quantities for bkg.
      - bkg_samesign.py     Fit same-sign bkg = QCD+Vj and compute for OS/SS ratio.
      - bkg_zll.py          Compute on-shell Z->ll bkg.
      - candidates.py       Summary of all bkgs and thus compute number of signal.
      - cross_feed.py       Compute fraction of cross-feed for each signal channel.
      - ditau_cuts.py       Offline signal cuts.
      - ditau_prep.py       Load all input trees.
      - gallery.py          Loop draw all selection variables.
      - id_utils.py         Collection of utils in this subpackage.
      - lepton_misid.py     Compute and plot lepton misidentification rate.
      - plot_signal.py      Use the final numbers of expected bkg/sig to plot stacked histogram.
      - samesign_utils.py   Actual algorithms splitting samesign=QCD+Vj
      - selected.py         Transform large input TTrees into smaller ntuples after selection.
      - varbin.py           Use final result to plot equiprobable bin.
      - zmumu_misid.py      Validation of lepton misidentification rate.
    - tmva/                 Use TMVA to compute for suggest optimal cut.
  - results/                Computing cross-section & systematics.
    - beam_syst.py          Syst from beam energy
    - combine_csc.py        Combine csc from all channels, and other derivative results.
    - compare_csc.py        Compare final csc with theoretical's
    - csc_egen.py           Const csc, generator eff for MC samples.
    - debug_changes.py      For reviewers.
    - fast_csc.py           Compute csc = nsig/(lumi * br * eff)
    - fit_br.py             Fix observed csc and compute tau BR as POI.
    - lumi.py               Double-check lumi values from online.
    - normalizer.py         Compute candidate yields from MC samples.

015_Higgs_mutau             Entire Higgs -> mu tau 8TeV analysis.
  - HMT_utils.py            Centralized constants & imporant functions.
  - packages.py             Helper module to import frequently-used packages.
  - efficiencies/           All efficiencies calculations.
    - punzi.py              Compute & draw punzi FoM.
    - rec_detailed.py       Compute reco eff using slower-precise method via Ztautau.
    - rec_direct.py         Compute reco eff using faster method from MC.
    - selection.py          Compute offline selection efficiency.
    - acceptance/           Compute acceptance
      - gallery_gen.py      Gallery of generator-level variables.
      - read_scan.py        Parsed results from Pythia at different mH,PDF and compute for acc.
      - generators/         Generator scripts for events before acceptance.
        - shower_powheg.py  Take events from POWHEG-BOX and shower using pythia8.
  - future_sensitivity/     Computation for signal yield in Run-II.
  - gen/                    Gauss DECFILE description of HMT process.
  - selection/              All signal & background offline selection.
    - bkg_ztautau.py        Prepare vars to compute number of expected Ztautau bkg.
    - ditau_cuts.py         Preselection cuts (before regime).
    - gallery.py            Loop draw all selection variables.
    - HMT_prep.py           Load all input trees.
    - preselection.py       Transform large input TTrees into smaller ntuples after preselection.
    - bkg_mva/              Use MVA to optimize for signal selection.
    - bkg_rectangular/      Use rectangular cut for signal selection.
      - plot_signal.py      Use the final numbers of expected bkg/sig to plot stacked histogram. 
      - rect_context.py     Compute misc quantities for bkg.
      - rect_samesign.py    Fit same-sign bkg = QCD+Vj and compute for OS/SS ratio.
      - rect_selected.py    Transform large input TTrees into smaller ntuples after selection.
      - rect_utils.py       Helper modules to load trees with selection aliases.
      - rect_zll.py         Compute Z->ll bkg. 
      - rect_ztautau.py     Compute Z->tautau bkg.
      - spec_lowmass.py     Offline signal selection at low-mass regime.
      - spec_nominal.py     Offline signal selection at central regime.
      - spec_tight.py       Offline signal selection at high-mass regime.
      - summary.py          Summary of all bkgs and thus compute number of signal.
      - varbin.py           Use final result to plot equiprobable bin.
  - upperlim/               Determine signal upper limits.
    - branching_fraction.py Cast final limits to BR.
    - collectors.py         Collect all inputs (nsig, eff, ...) to single place.
    - coupling.py           Cast final limits to Yukawa coupling.
    - cscbr.py              Cast final limits to cscbr.
    - lim_utils.py          Collection of utils used in this subpackage.
    - load_bestfit.py       Collect all best fit results from all hypotest runs.
    - load_pull.py          Draw the pull plot.
    - load_upperlim.py      Collect all obs/exp limits results from all hypotest runs.
    - massfit.py            Draw mass fit plot.
    - model[1-12].py        Different variation of extended-MLL to do hypotest.
    - nuisance_gaussian.py  Helper module to inject nuisance param with correlation.
    - prep_workspace.py     Preparing inputs selected TTrees into RooWorkspace.
    - prod_csc.py           Const production csc of Higgs boson.
    - signal_yield.py       Cast final limits to signal yields.
    - test_suite.py         Collection of statistical test for each hypotest run.
    - validate_bestfit.py   Draw & observe best fit result from each hypotest run.
    - validate_upperlim.py  Draw & observe upperlim result from each hypotest run.
    - validate_workspace.py Draw & observe PDF in the prepared workspace before hypotest.

902_fullsim_resources       Helper scripts used during full gen.
904_cross_section_checker   Helper scripts to run Gauss and determine prod csc.
~~~

Dataset
-------

Large input/output DST/roots are stored on my LHCb-EOS area 
(`root://eoslhcb.cern.ch//eos/lhcb/user/c/ckhurewa/ganga/...`)
where they are indexed by the ganga job ID that worked on it.
See the job dump [here](./eos.html)


Dependencies
------------

Apart from the usual LHCb projects (e.g., Gauss for generation, Bender for crunching
DST to ROOT), the following python packages are needed. They are all written by
me and published on gitlab and PyPi, so they can be installed via `pip install ...`.

- [pyroot_zen](https://gitlab.com/ckhurewa/pyroot-zen): to simplify the PyROOT syntax.
- [qhist](https://gitlab.com/ckhurewa/qhist)          : quickly draw histograms.
- [PyPythia8](https://gitlab.com/ckhurewa/PyPythia8)  : executing Pythia8 in python.
- [PythonCK](https://gitlab.com/ckhurewa/PythonCK)    : my base python utilities/algos,
- [PyrootCK](https://gitlab.com/ckhurewa/PyrootCK)    : my base PyROOT utilities.
- [BenderCK](https://gitlab.com/ckhurewa/BenderCK)    : my Bender utilities, providing core Tau algorithms.
- [GangaCK](https://gitlab.com/ckhurewa/GangaCK)      : my Ganga utilities.
